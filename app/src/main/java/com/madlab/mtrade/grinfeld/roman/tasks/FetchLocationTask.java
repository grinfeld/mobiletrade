package com.madlab.mtrade.grinfeld.roman.tasks;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyLocationListener;
import com.madlab.mtrade.grinfeld.roman.iface.IFetchLocationTask;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public class FetchLocationTask extends AsyncTask<Void, Integer, Location> {

    private final static float BOUND = 300f;
    private final static short MAX_TIME = 180;
    private static final String TAG = "#FetchLocationTask";
    private Location result = null;
    private int seconds = 0;

    private MyLocationListener network;
    private MyLocationListener gps;
    private LocationManager locManager;
    private Activity context;
    private IFetchLocationTask listener;

    public FetchLocationTask(Activity context, IFetchLocationTask listener) {
        network = new MyLocationListener();
        gps = new MyLocationListener();
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        locManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        //locManager = LocationServices.getFusedLocationProviderClient(this);
        requestLocationUpdates(network, LocationManager.NETWORK_PROVIDER, 10);
        requestLocationUpdates(gps, LocationManager.GPS_PROVIDER, 10);
    }

    @Override
    protected Location doInBackground(Void... params) {
        // Если мы не получили координату с нормальной точностью - ищем
        // дальше
        while ((result == null || result.getAccuracy() > BOUND) && seconds <= MAX_TIME) {
            Log.d(TAG, String.valueOf(seconds) +" "+String.valueOf(result));
            result = network.lastLocation();
            if (isCancelled())
                break;
            if (result == null) {
                result = gps != null ? gps.lastLocation() : null;
            } else {
                Location last = gps != null ? gps.lastLocation() : null;
                if (last != null
                        && result.getAccuracy() > last.getAccuracy()) {
                    result = last;
                }
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            seconds++;
        }
        if (seconds >= MAX_TIME) {
            if (gps.lastLocation() != null) {
                result = gps.lastLocation();
            } else if (network.lastLocation() != null) {
                result = network.lastLocation();
            }
        }
        return result;
    }


    @Override
    protected void onPostExecute(Location result) {
        if (locManager != null) {
            if (network != null)
                locManager.removeUpdates(network);
            if (gps != null)
                locManager.removeUpdates(gps);
        }
        listener.onLocation(this);
    }

    private void requestLocationUpdates(MyLocationListener listener, String provider, float minDistance) {
        try {
            if (locManager == null) {
                locManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
            }
            if (ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, listener);
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, listener);
            //locManager.requestLocationUpdates(provider, 0, minDistance, listener);
            Log.d(TAG, String.valueOf(locManager) +" requestLocationUpdates");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
