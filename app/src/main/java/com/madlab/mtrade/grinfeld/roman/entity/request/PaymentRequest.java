package com.madlab.mtrade.grinfeld.roman.entity.request;

/**
 * Created by grinfeldra
 */
public class PaymentRequest {

    private String Agent;
    private String Client;
    private String DocNumber;
    private String DocDate;
    private float Amount;

    public PaymentRequest(String agent, String client, String docNumber, String docDate, float amount) {
        Agent = agent;
        Client = client;
        DocNumber = docNumber;
        DocDate = docDate;
        Amount = amount;
    }
}
