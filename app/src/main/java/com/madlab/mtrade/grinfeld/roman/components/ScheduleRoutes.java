package com.madlab.mtrade.grinfeld.roman.components;


import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;


public class ScheduleRoutes extends LinearLayout {

	/**
	 * График посещений
	 */
	private String mRoute = null;

	private TextView tvDescription;

	/**
	 * Ссылка на картинку для галочки
	 */
	private int checked;
	/**
	 * Ссылка на картинку без галочки
	 */
	private int unchecked = R.drawable.button_shape_normal;
	/**
	 * Показывать подписи на кнопках
	 */
	private boolean mShowText = false;

	public ScheduleRoutes(Context context) {
		super(context);
		ctor(context);
	}

	public ScheduleRoutes(Context context, AttributeSet attrs) {
		super(context, attrs);
		ctor(context);
	}

	/**
	 * Заполняем элемент управления String route - график посещений
	 */
	public void setRoute(String route) {
		if (route == null)
			return;

		mRoute = route;

		// Загружаем подписи для кнопок
		String[] days = getResources().getStringArray(R.array.days_of_weeks);

		// Контейнер для надписи и кнопок
		LinearLayout ll = (LinearLayout) getChildAt(0);
		// Нулевой элемент - надпись, первый - tableRow
		TableRow row = (TableRow) ll.getChildAt(1);
		if (row == null)
			return;
		for (int i = 0; i < row.getChildCount(); i++) {
			if (i >= mRoute.length())
				break;
			ViewGroup child = (ViewGroup) row.getChildAt(i);
			ImageButton imageButton = (ImageButton)child.getChildAt(0);
			Button button = (Button) child.getChildAt(1);
			button.setBackgroundResource(unchecked);
			if (mRoute.charAt(i) == '1') {
				imageButton.setBackgroundResource(checked);
			} else {
				imageButton.setBackgroundResource(unchecked);
				if (mShowText)
					button.setText(days[i]);
			}
		}
	}

	private void ctor(Context context) {
		String infService = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li = (LayoutInflater) context.getSystemService(infService);
		li.inflate(R.layout.schedule_routes, this, true);

		checked = R.mipmap.check_g;
		unchecked = 0;

		tvDescription = (TextView) findViewById(R.id.sr_tvDescription);
	}

	public void setShowText(boolean showText) {
		mShowText = showText;
	}

	public void setDescriptionText(String text) {
		if (tvDescription != null) {
			tvDescription.setText(text);
		}
	}

	public void setDescriptionWidth(int pixels) {
		if (tvDescription != null) {
			tvDescription.setWidth(pixels);
		}
	}
}
