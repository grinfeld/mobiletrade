package com.madlab.mtrade.grinfeld.roman.connectivity;

/**
 * Created by GrinfeldRA on 12.07.2018.
 */

public class ReportResponse extends DalimoResponse {

    public ReportResponse() {
    }

    public ReportResponse(Exception error) {
        super(error);
    }

    public boolean isSuccess(){
        return error == null;
    }
}
