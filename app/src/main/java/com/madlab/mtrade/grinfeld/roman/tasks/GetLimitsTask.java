package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.iface.IGetLimitTask;

import java.util.Locale;

/**
 * Created by GrinfeldRA on 26.04.2018.
 */

public class GetLimitsTask extends AsyncTask<String,Void,String> {

    private static final String TAG = "!->GetLimitsIntentService";
    private final String PROC_NAME = "ПроверкаОстаткаТоварногоЛимита";

    public static final String EXTRA_AGENT = "AgentCode";
    public static final String EXTRA_CLIENT = "ClientCode";

    public static final String EXTRA_STATUS = "TaskStatus";
    public static final String EXTRA_MESSAGE = "ErrorMessage";

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.asklimits";

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;

    private byte status = STATUS_OK;
    private String mResult = null;

    //Сведения для подключения
    private DalimoClient mClient;
    private Credentials mConnectInfo;

    private String mCodeAgent;
    private String mCodeClient;
    private Context context;
    private IGetLimitTask callback;

    public GetLimitsTask(Context context, IGetLimitTask callback, String mCodeAgent, String mCodeClient){
        this.context = context;
        this.mCodeAgent = mCodeAgent;
        this.mCodeClient = mCodeClient;
        this.callback = callback;
    }


    @Override
    protected String doInBackground(String... voids) {
        if (voids!=null){
            mResult = voids[0];
        }
        if (mResult == null){
            mConnectInfo = Credentials.load(context);
            StringBuilder data = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;",
                    Const.COMMAND, mCodeAgent, PROC_NAME, mCodeAgent, mCodeClient));
            mClient = new DalimoClient(mConnectInfo);
            if (mClient.connect()) {
                mClient.send(data + Const.END_MESSAGE);
                mResult = mClient.receive();
                status = STATUS_OK;
            } else {
                status = STATUS_ERROR;
                //GlobalProc.mToast(context, mClient.LastError());
            }
            Log.d("#OrderGoodsFragment", String.valueOf(mResult));
            mClient.disconnect();
        }

        return mResult;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callback.onTaskComplete(this);
    }
}
