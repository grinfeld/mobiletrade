package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.ArrayList;
import java.util.Date;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;

public class Arrear implements Parcelable {
    public final static byte DOC_P = 0;
    public final static byte DOC_N = 1;

    private final static float DEFAULT_MONEY = 0f;
    private final String UNKNOWN = "<Неизвестный>";

    private String mAgentFIO;
    private String mAgentCode;
    private String mClient;

    private String mDocNumber;
    private float mDocSum;
    private float mDocCredit;
    private float mDocPayment;
    private Date mDocDate;

    private String mExpeditor;
    private String mExpeditorPhone;
    /**
     * 0 - П, 1 - Н
     */
    private byte mDocType;

    private String firma;

    /**
     * Список долгов
     */
    private static ArrayList<Arrear> mList;

    public static final Creator<Arrear> CREATOR = new Creator<Arrear>() {
        @Override
        public Arrear createFromParcel(Parcel in) {
            return new Arrear(in);
        }

        @Override
        public Arrear[] newArray(int size) {
            return new Arrear[size];
        }
    };

    public void setAgentCode(String agentCode) {
        mAgentCode = agentCode;
    }

    public String getAgentCode() {
        return mAgentCode;
    }

    public void setAgentFIO(String agentFIO) {
        mAgentFIO = agentFIO;
    }

    public String getAgentFIO() {
        return mAgentFIO;
    }

    public void setClient(String client) {
        mClient = client;
    }

    public String getClient() {
        return mClient;
    }

    public void setDocNumber(String number) {
        if (number == null)
            mDocNumber = "";
        else
            mDocNumber = number;
    }

    public String getDocNumber() {
        return mDocNumber;
    }

    public void setDocSum(float sum) {
        mDocSum = sum;
    }

    public float getDocSum() {
        return mDocSum;
    }

    public void setDocCredit(float credit) {
        mDocCredit = credit;
    }

    public float getDocCredit() {
        return mDocCredit;
    }

    public void setDocPayment(Float payment) {
        if (payment == null)
            mDocPayment = 0f;
        else
            mDocPayment = payment;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public Float getDocPayment() {
        return mDocPayment;
    }

    public void setDocDate(Date docDate) {
        if (docDate != null) {
            mDocDate = docDate;
        } else {
            mDocDate = new Date(System.currentTimeMillis());
        }
    }

    public Date getDocDate() {
        return mDocDate;
    }

    public void setExpeditor(String expeditorFIO) {
        if (expeditorFIO == null)
            mExpeditor = UNKNOWN;
        else
            mExpeditor = expeditorFIO;
    }

    public String getExpeditor() {
        return mExpeditor;
    }

    public void setExpeditorPhone(String expeditorPhone) {
        if (expeditorPhone == null)
            mExpeditorPhone = UNKNOWN;
        else
            mExpeditorPhone = expeditorPhone;
    }

    public String getExpeditorPhone() {
        return mExpeditorPhone;
    }

    public byte getDocType() {
        return mDocType;
    }

    public void setDocType(byte docType) {
        this.mDocType = docType;
    }

    public Arrear(String agentCode, String agentFIO, String client,
                  String docNumber, float docSum, float docCredit, float docPay,
                  Date docDate, String expeditorFIO, String expeditorPhone,
                  byte docType, String firma) {
        mAgentCode = agentCode;
        mAgentFIO = agentFIO;
        mClient = client;
        mDocNumber = docNumber;

        mDocSum = docSum;
        mDocCredit = docCredit;
        mDocPayment = docPay;
        mDocDate = docDate;

        setExpeditor(expeditorFIO);
        setExpeditorPhone(expeditorPhone);

        mDocType = docType;
        this.firma = firma;
    }

    public Arrear(Parcel src) {
        mAgentCode = src.readString();
        mAgentFIO = src.readString();
        mClient = src.readString();
        mDocNumber = src.readString();

        mDocSum = src.readFloat();
        mDocCredit = src.readFloat();
        mDocPayment = src.readFloat();
        mDocDate = (Date)src.readSerializable();

        setExpeditor(src.readString());
        setExpeditorPhone(src.readString());

        mDocType = src.readByte();
        firma = src.readString();
    }

    public Arrear() {
        this("", "", "", "", DEFAULT_MONEY, DEFAULT_MONEY, DEFAULT_MONEY, new Date(System.currentTimeMillis()),
                null, null, DOC_N, "");
    }

    public static ArrayList<Arrear> getList() {
        return mList;
    }

    public static Arrear getItem(int index) {
        Arrear res = null;

        if (index < mList.size())
            return mList.get(index);

        return res;
    }

    public static void loadList(SQLiteDatabase db, String codeAgent,
                                String codeClient) {
        mList = new ArrayList<Arrear>();

        String sql = String
                .format("SELECT ClientCreditInfo.NumDoc as NumDocT,"
                                + "ClientCreditInfo.FIO as AgentFIO, "
                                + "ClientCreditInfo.CreditDoc as CreditDocT, "
                                + "ClientCreditInfo.SumDoc as SumDocT, "
                                + "Payment.Amount as AmountT, "
                                + "ClientCreditInfo.DateDoc as DateDocT, "
                                + "Payment.RecType as RecTypeT, "
                                + "ClientCreditInfo.Expeditor as Expeditor, "
                                + "ClientCreditInfo.ExpeditorPhone as ExpeditorPhone, "
                                + "ClientCreditInfo.DocType as DocType, "
                                + "ClientCreditInfo.Firma as Firma "
                                + "FROM ClientCreditInfo LEFT JOIN Payment ON "
                                + "(ClientCreditInfo.CodeAgent=Payment.CodeAgent) AND "
                                + "(ClientCreditInfo.CodeCli=Payment.CodeCli) AND "
                                + "(ClientCreditInfo.NumDoc=Payment.NumDoc) AND "
                                + "(ClientCreditInfo.DateDoc=Payment.DateDoc) "
                                + "WHERE ClientCreditInfo.CodeAgent='%s' AND ClientCreditInfo.CodeCli='%s' AND (Payment.RecType=1 or Payment.RecType is NULL) "
                                + "UNION ALL "
                                +

                                "SELECT Payment.NumDoc as NumDocT, "
                                + "ClientCreditInfo.FIO as AgentFIO, "
                                + "ClientCreditInfo.CreditDoc as CreditDocT, "
                                + "ClientCreditInfo.SumDoc as SumDocT, "
                                + "Payment.Amount as AmountT, "
                                + "Payment.DateDoc as DateDocT, "
                                + "Payment.RecType as RecTypeT, "
                                + "ClientCreditInfo.Expeditor as Expeditor, "
                                + "ClientCreditInfo.ExpeditorPhone as ExpeditorPhone, "
                                + "ClientCreditInfo.DocType as DocType, "
                                + "ClientCreditInfo.Firma as Firma "
                                + "FROM Payment LEFT JOIN ClientCreditInfo ON "
                                + "(Payment.CodeAgent=ClientCreditInfo.CodeAgent) AND "
                                + "(Payment.CodeCli=ClientCreditInfo.CodeCli) AND "
                                + "(Payment.NumDoc=ClientCreditInfo.NumDoc) AND "
                                + "(Payment.DateDoc=ClientCreditInfo.DateDoc) "
                                + "WHERE (CreditDoc is NULL) AND Payment.CodeAgent='%s' AND Payment.CodeCli='%s' AND (Payment.RecType=2 OR Payment.RecType is NULL) "
                                + "ORDER BY DateDocT, RecTypeT, NumDocT ", codeAgent,
                        codeClient, codeAgent, codeClient);

        Cursor queryResult = null;

        try {
            queryResult = db.rawQuery(sql, null);

            if (queryResult.getCount() < 1) {
                queryResult.close();
                return;
            }

            int numDocT = queryResult.getColumnIndex("NumDocT");
            int agentFIO = queryResult.getColumnIndex("AgentFIO");
            int creditDocT = queryResult.getColumnIndex("CreditDocT");
            int sumDocT = queryResult.getColumnIndex("SumDocT");
            int amountT = queryResult.getColumnIndex("AmountT");
            int dateDocT = queryResult.getColumnIndex("DateDocT");
            int expeditor = queryResult.getColumnIndex("Expeditor");
            int expeditorPhone = queryResult.getColumnIndex("ExpeditorPhone");
            int docTypeColumn = queryResult.getColumnIndex(ClientCreditInfoMetaData.FIELD_DOC_TYPE);
            int firmaColumn = queryResult.getColumnIndex(ClientCreditInfoMetaData.FIELD_FIRMA);
            while (queryResult.moveToNext()) {
                Arrear credit = new Arrear();
                credit.setClient(codeClient);
                credit.setAgentCode(codeAgent);


                credit.setDocNumber(queryResult.getString(numDocT));

                credit.setAgentFIO(queryResult.getString(agentFIO));

                credit.setDocCredit(queryResult.getFloat(creditDocT));

                credit.setDocSum(queryResult.getFloat(sumDocT));

                credit.setDocPayment(queryResult.getFloat(amountT));

                Date date = TimeFormatter.parseSqlDate(queryResult.getString(dateDocT));
                credit.setDocDate(date);

                credit.setExpeditor(queryResult.getString(expeditor));

                credit.setExpeditorPhone(queryResult.getString(expeditorPhone));

                String typeTmp = queryResult.getString(docTypeColumn);
                byte docType = DOC_N;
                if (typeTmp != null && typeTmp.charAt(0) == 'П') {
                    docType = DOC_P;
                }
                credit.setDocType(docType);
                String firma = queryResult.getString(firmaColumn);
                credit.setFirma(firma);
                mList.add(credit);
            }
        } catch (Exception e) {
            Log.e("!->Arrear.loadList()", e.toString());
        } finally {
            queryResult.close();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAgentCode);
        dest.writeString(mAgentFIO);
        dest.writeString(mClient);
        dest.writeString(mDocNumber);

        dest.writeFloat(mDocSum);
        dest.writeFloat(mDocCredit);
        dest.writeFloat(mDocPayment);
        dest.writeSerializable(mDocDate);

        dest.writeString(mExpeditor);
        dest.writeString(mExpeditorPhone);

        dest.writeByte(mDocType);
        dest.writeString(firma);
    }
}
