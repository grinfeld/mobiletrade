package com.madlab.mtrade.grinfeld.roman.entity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.ShipmentMustListMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA on 11.05.2018.
 */

public class ShipmentMustList {

    private static final String TAG = "#ShipmentMustList";
    String codeGoods;
    String isShipmentMustList;
    String codeAgent;
    String codeClient;

    public ShipmentMustList(String codeGoods, String isShipmentMustList, String codeAgent, String codeClient) {
        this.codeGoods = codeGoods;
        this.isShipmentMustList = isShipmentMustList;
        this.codeAgent = codeAgent;
        this.codeClient = codeClient;
    }

    public String getCodeGoods() {
        return codeGoods;
    }

    public void setCodeGoods(String codeGoods) {
        this.codeGoods = codeGoods;
    }

    public String getIsShipmentMustList() {
        return isShipmentMustList;
    }

    public void setIsShipmentMustList(String isShipmentMustList) {
        this.isShipmentMustList = isShipmentMustList;
    }

    public String getCodeAgent() {
        return codeAgent;
    }

    public void setCodeAgent(String codeAgent) {
        this.codeAgent = codeAgent;
    }

    public String getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(String codeClient) {
        this.codeClient = codeClient;
    }

    public static List<String> loadCodeGoodsIsShipmentMustList(Context ctx, String codeAgent, String codeClient){
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return loadGoodsShipmentMustList(app.getDB(),codeAgent,codeClient);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    private static List<String> loadGoodsShipmentMustList(SQLiteDatabase db, String codeAgent, String codeClient) {
        List<String> result = new ArrayList<>();
        String where = String.format("%s = '%s' AND %s = '%s'", ShipmentMustListMetaData.CODE_CLIENT, codeClient, ShipmentMustListMetaData.CODE_AGENT, codeAgent);
        Cursor cursor = null;
        try {
            cursor = db.query(ShipmentMustListMetaData.TABLE_NAME, null, where, null, null,null,null);
            while (cursor.moveToNext()){
                String code = cursor.getString(cursor.getColumnIndex(ShipmentMustListMetaData.IS_SHIPMENT_MUST_LIST));
                if (code.equals("1")){
                    String codeGoods  = cursor.getString(cursor.getColumnIndex(ShipmentMustListMetaData.CODE_GOODS));
                    result.add(codeGoods);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

}
