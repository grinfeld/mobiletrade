package com.madlab.mtrade.grinfeld.roman.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.TicketClient;

import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 25.12.2017.
 */

public class TicketClientListAdapter extends BaseAdapter{

    private Activity activity;
    private ArrayList<TicketClient> ticketClientArrayList;

    public TicketClientListAdapter(Activity activity, ArrayList<TicketClient> ticketClientArrayList) {
        this.activity = activity;
        this.ticketClientArrayList = ticketClientArrayList;
    }

    @Override
    public int getCount() {
        return ticketClientArrayList.size();
    }

    @Override
    public TicketClient getItem(int i) {
        return ticketClientArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = view;
        ViewHolder holder;
        if (rowView == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.item_ticket_client, null);
            holder = new ViewHolder();
            holder.txtTicketAmount = rowView.findViewById(R.id.txtTicketAmount);
            holder.txtTicketManager = rowView.findViewById(R.id.txtTicketManager);
            holder.txtTicketName = rowView.findViewById(R.id.txtTicketName);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder)rowView.getTag();
        }
        TicketClient ticketClient = getItem(i);
        holder.txtTicketName.setText(ticketClient.getNameTicket());
        holder.txtTicketManager.setText(ticketClient.getManager());
        holder.txtTicketAmount.setText(ticketClient.getAmount()+" Р");
        return rowView;
    }

    private static class ViewHolder {
        TextView txtTicketName;
        TextView txtTicketManager;
        TextView txtTicketAmount;
    }
}
