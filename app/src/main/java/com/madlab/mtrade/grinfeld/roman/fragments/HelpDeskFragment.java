package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by GrinfeldRA
 */

public class HelpDeskFragment extends Fragment {

    Spinner spinner;
    EditText et_theme, et_description;
    Activity context;
    Handler mHandler;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        context = getActivity();
        mHandler = new Handler(Looper.getMainLooper());
        ((AppCompatActivity) context).getSupportActionBar().setTitle("Заявка в HelpDesk");
        View rootView = inflater.inflate(R.layout.fragment_help_deck, container, false);

        et_theme = rootView.findViewById(R.id.et_theme);
        et_description = rootView.findViewById(R.id.et_description);

//        spinner = rootView.findViewById(R.id.spinner);
//        List<String> list = new ArrayList<>();
//        list.add("MTrade и моб. устройства");
//        list.add("Вопрос по работе 1С");
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list);
//        adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
//        spinner.setAdapter(adapter);
//        spinner.setTag(list);
//
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
//                if (city != null) {
//                    switch (city) {
//                        case "SM":
//                            switch (position) {
//                                case 0:
//                                    mCategoryID = "22";
//                                    break;
//                                case 1:
//                                    mCategoryID = "49";
//                                    break;
//                            }
//                            break;
//                    }
//
//                } else {
//                    Toast.makeText(context, "Вы не авторизованы", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.task_add_edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_task_add_edit_send) {
            GlobalProc.closeKeyboard(context);
            String theme = et_theme.getText().toString();
            String description = et_description.getText().toString();
            if (theme.isEmpty() || description.isEmpty()) {
                Toast.makeText(context, "Не все поля заполнены", Toast.LENGTH_SHORT).show();
            } else {
                String code_agent = App.get(context).getCodeAgent();
                String domain = App.getDomain(context);
                String city = App.getRegion(context);
                String catId = App.getCatIdHelpDesk(context);
                if (city != null && code_agent != null && domain != null && catId != null) {
                    Objects.requireNonNull(OkHttpClientBuilder.buildOrderHelpDesk(catId, theme, description, code_agent, city, domain)).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NonNull Call call, @NonNull final IOException e) {
                            mHandler.post(() -> Toast.makeText(context, "Не отправлена " + e.getMessage(), Toast.LENGTH_SHORT).show());
                        }

                        @Override
                        public void onResponse(@NonNull Call call, @NonNull final Response response) throws IOException {
                            mHandler.post(() -> {
                                if (response.message().equals("OK")) {
                                    Toast.makeText(context, "Успешно отправлена", Toast.LENGTH_SHORT).show();
                                    et_theme.setText("");
                                    et_description.setText("");
                                } else {
                                    Toast.makeText(context, "Не отправлена", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                } else {
                    Toast.makeText(context, "Вы не авторизованы", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
