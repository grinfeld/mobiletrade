package com.madlab.mtrade.grinfeld.roman.eventbus;

import android.view.View;

/**
 * Created by GrinfeldRa
 */
public class OnLongClickItem {

   private View view;

    public OnLongClickItem(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }
}
