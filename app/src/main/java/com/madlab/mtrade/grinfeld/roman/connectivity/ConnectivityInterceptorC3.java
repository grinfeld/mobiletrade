package com.madlab.mtrade.grinfeld.roman.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by grinfeldra
 */
public class ConnectivityInterceptorC3 implements Interceptor {

    private Context mContext;
    private Handler handler;

    public ConnectivityInterceptorC3(Context context) {
        mContext = context;
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!isOnline(mContext)) {
            handler.post(() -> Toast.makeText(mContext, "Нет интернет соединения для отправки фото", Toast.LENGTH_SHORT).show());
        }

        Request.Builder builder = chain.request().newBuilder();
        builder.header("x-app-id", "8rRYkPqv4rMm9XLnK9Mt");
        builder.header("x-app-key", "E2DKtaxAWBpufa6xZgC8bhnwjBY6URXe4vkjHQhXQExdMLzw");
        return chain.proceed(builder.build());
    }

    private  boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (connectivityManager != null) {
            netInfo = connectivityManager.getActiveNetworkInfo();
        }
        return (netInfo != null && netInfo.isConnected());
    }

}
