package com.madlab.mtrade.grinfeld.roman.eventbus;

import android.view.View;

/**
 * Created by GrinfeldRA
 */
public class DateChanged {

    private View view;
    private int year;
    private int month;
    private int day;

    public DateChanged(View view, int year, int month, int day) {
        this.view = view;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public View getView() {
        return view;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
