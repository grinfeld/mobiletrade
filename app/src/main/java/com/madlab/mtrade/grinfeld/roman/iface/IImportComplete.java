package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.ImportFileTask;

public interface IImportComplete {
	void onTaskComplete(ImportFileTask task);
}
