package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.SetReservPaymentTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservReturnTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservOrderTask;

/**
 * Created by GrinfeldRA on 14.11.2017.
 */

public interface ISetReservComplete {

    void onTaskComplete(SetReservOrderTask task);

    void onTaskComplete(SetReservReturnTask task);

    void onTaskComplete(SetReservPaymentTask task);

}
