package com.madlab.mtrade.grinfeld.roman.connectivity;

public class DalimoResponse {

    protected Exception error;

    public DalimoResponse(){
    }

    public DalimoResponse(Exception error){
        this.error = error;
    }

    public Exception getError(){
        return error;
    }

    public void setError(Exception error){
        this.error = error;
    }
}
