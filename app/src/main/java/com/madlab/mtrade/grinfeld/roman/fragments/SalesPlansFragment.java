package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.PlanSalesAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.PlanSales;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by GrinfeldRa on 13.04.2018.
 */


public class SalesPlansFragment extends Fragment {


    private static final String TAG = "#SalesPlansFragment";
    private Context context;
    private Handler mHandler;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sales_plans, container, false);
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        setHasOptionsMenu(true);
        final ListView listView = rootView.findViewById(android.R.id.list);
        TextView txtClient = rootView.findViewById(R.id.txt_client);
        context = getActivity();
        mHandler = new Handler(Looper.getMainLooper());
        Client client = getArguments().getParcelable(Client.KEY);
        txtClient.setText(client.mName);
        final ProgressDialog mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Пожалуйста подождите...");
        mProgress.setCancelable(true);
        mProgress.show();
        OkHttpClientBuilder.buildCall("ПолучитьИсполнениеПланаПродаж", client.getCode(), App.getRegion(context)).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(mProgress::dismiss);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    final List<PlanSales> planSalesResult = new ArrayList<>();
                    String strResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(strResponse);
                    String data = jsonObject.getString("data");
                    JSONArray jsonArray = new JSONArray(data);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject json = jsonArray.getJSONObject(i);
                        int skuPlan = json.getInt("SKU_План");
                        int skuFact = json.getInt("SKU_Факт");
                        int percentageCompletion = json.getInt("ПроцентВыполнения");
                        String groupGoodsName = json.getString("ТоварнаяГруппаНазвание");
                        planSalesResult.add(new PlanSales(skuPlan, skuFact, percentageCompletion, groupGoodsName));
                    }
                    mHandler.post(() -> {
                        listView.setAdapter(new PlanSalesAdapter(getActivity(), planSalesResult));
                        mProgress.dismiss();
                    });
                } catch (JSONException e) {
                    mProgress.dismiss();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }
}
