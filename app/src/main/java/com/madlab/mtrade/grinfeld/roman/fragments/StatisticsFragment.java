package com.madlab.mtrade.grinfeld.roman.fragments;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.AuthManager;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.ContactMetaData;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaymentsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionBonusesMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionItemMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionProductMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;
import com.madlab.mtrade.grinfeld.roman.entity.Document;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.PaybackPhoto;
import com.madlab.mtrade.grinfeld.roman.entity.Payment;
import com.madlab.mtrade.grinfeld.roman.entity.Post;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.entity.request.PaymentRequest;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IGetFilesComplete;
import com.madlab.mtrade.grinfeld.roman.iface.IImportComplete;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;
import com.madlab.mtrade.grinfeld.roman.iface.IUpdateComplete;
import com.madlab.mtrade.grinfeld.roman.services.AlarmReceiver;
import com.madlab.mtrade.grinfeld.roman.services.GeolocationService;
import com.madlab.mtrade.grinfeld.roman.services.GetParamsIntentService;
import com.madlab.mtrade.grinfeld.roman.tasks.DocumentsReserver;
import com.madlab.mtrade.grinfeld.roman.tasks.GetFilesFTPTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetFilesFromServerTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetPromotionRx;
import com.madlab.mtrade.grinfeld.roman.tasks.ImportFileTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservPaymentTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservReturnTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservOrderTask;
import com.madlab.mtrade.grinfeld.roman.tasks.UpdateProgrammTask;
import com.madlab.mtrade.grinfeld.roman.tasks.UpdateRestFrom1CTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;


/**
 * Created by GrinfeldRA on 30.09.2017.
 */

public class StatisticsFragment extends Fragment implements View.OnClickListener, ISetReservComplete, IGetFilesComplete, IImportComplete, IUpdateComplete, GetPromotionRx.IPromotionEvent {

    private static final String TAG = "#StatisticsFragment";
    private static final String ARCHIVE_FILE_TRADE = "Files.zip";
    private static final String ARCHIVE_FILE_MERCH = "FILES_MONITORING.zip";
    private View viewSendAll, viewMoreInfo;
    private ProgressBar progressBarVisit, progressBarTicket, progressBarPhoto, progressBarRevert;
    private TextView txt_ticket_count, txt_ticket_cost, txt_ticket_weight, txt_invoices,
            txt_visit_count, txt_photo_cost, txt_revert_weight, txt_revert_cost, txt_revert_count, txt_total_cost;
    private ProgressBar progressBar;
    private DocumentsReserver<Visit> mDocumentReserverThread;
    private static final int MESSAGE_UPDATE_LIST = 0;
    private List<Document> listVisit;
    private List<Document> listOrder;
    private List<Document> listReturn;
    private List<Document> listPayment;
    private static final int PERMISSIONS = 1;
    private Activity context;
    private Handler handler;
    public static boolean deleteDocuments = true;
    private GetPromotionRx getPromotionRx;
    private ProgressDialog mProgress;

    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_UPDATE_LIST:
                    if (mDocumentReserverThread != null && mDocumentReserverThread.getItemsCount() < 1) {
                        waitCursor(false);
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    });


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        setHasOptionsMenu(true);
        context = getActivity();
        handler = new Handler(Looper.getMainLooper());
        ((IDynamicToolbar) getActivity()).onChangeToolbar(null);
        progressBar = view.findViewById(R.id.progress_bar_statistic);
        viewSendAll = view.findViewById(R.id.send_all);
        viewMoreInfo = view.findViewById(R.id.more_info);
        txt_total_cost = view.findViewById(R.id.txt_total_cost);
        progressBarVisit = view.findViewById(R.id.pb_visit);
        progressBarTicket = view.findViewById(R.id.pb_ticket);
        progressBarPhoto = view.findViewById(R.id.pb_photo);
        progressBarRevert = view.findViewById(R.id.pb_revert);
        txt_ticket_count = view.findViewById(R.id.txt_ticket_count);
        txt_ticket_weight = view.findViewById(R.id.txt_ticket_weight);
        txt_ticket_cost = view.findViewById(R.id.txt_ticket_cost);
        txt_visit_count = view.findViewById(R.id.txt_visit_count);
        txt_photo_cost = view.findViewById(R.id.txt_photo_cost);
        txt_revert_weight = view.findViewById(R.id.txt_revert_weight);
        txt_revert_cost = view.findViewById(R.id.txt_revert_cost);
        txt_revert_count = view.findViewById(R.id.txt_revert_count);
        txt_invoices = view.findViewById(R.id.txt_invoices);
        viewSendAll.setOnClickListener(this);
        viewMoreInfo.setOnClickListener(this);
//        ProgressBar[] progressBars = new ProgressBar[]{progressBarVisit,progressBarTicket,progressBarPhoto,progressBarRevert};
//        onAnimatorStart(progressBars);
        if (verifyPermissions(context)) {
            fillListAndBasement();
        }
        if (impexFileLastModify(24000)) {
            deleteDocuments = false;
            String codeAgent = App.get(context).getCodeAgent();
            String server = App.get(context).getFTPServer();
            String connectionServer = App.get(context).connectionServer;
            if (!codeAgent.equals(getString(R.string.lb_zero_code_manager)) && !server.isEmpty()) {
                waitCursor(true);
                Intent startGetLimitsTask = new Intent(context, GetParamsIntentService.class);
                startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_AGENT, codeAgent);
                startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_TYPE_SERVER, connectionServer);
                context.startService(startGetLimitsTask);
                MainActivity.flag = false;
            } else {
                Toast.makeText(context, "Вы не указали в настройках код менеджера или не выбрали сервер", Toast.LENGTH_LONG).show();
            }
        }
        return view;
    }

    //С01201
    MenuItem menuItemGPSTracker;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.update_menu, menu);
        menuItemGPSTracker = menu.findItem(R.id.action_start_gps_tracker);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (GeolocationService.getServiceState() == GeolocationService.State.RUN) {
            Drawable ic_media_pause = ContextCompat.getDrawable(context, android.R.drawable.ic_media_pause);
            if (ic_media_pause != null) {
                menuItemGPSTracker.setIcon(ic_media_pause);
            }
        } else {
            Drawable ic_media_play = ContextCompat.getDrawable(context, android.R.drawable.ic_media_play);
            if (ic_media_play != null) {
                menuItemGPSTracker.setIcon(ic_media_play);
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_update:
                ((AppCompatActivity) context).getSupportActionBar().setSubtitle("");
                m_import().show();
                return true;
            case R.id.action_select_import:
                showDialogImportSelect();
                return true;
            case R.id.action_start_gps_tracker:
                if (AuthManager.getInstance().isAuthenticate()) {
                    if (GeolocationService.getServiceState() == GeolocationService.State.RUN) {
                        Toast.makeText(context, "Остановка службы определения местоположения.", Toast.LENGTH_LONG).show();
                        context.startService(new Intent(context, GeolocationService.class).putExtra("COMMAND", "STOP"));
                        Drawable ic_media_play = ContextCompat.getDrawable(context, android.R.drawable.ic_media_play);
                        if (ic_media_play != null) {
                            item.setIcon(ic_media_play);
                        }
                        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
                        PackageManager pm = context.getPackageManager();
                        pm.setComponentEnabledSetting(receiver,
                                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                                PackageManager.DONT_KILL_APP);
                    } else {
                        Toast.makeText(context, "Запуск службы определения местоположения.", Toast.LENGTH_LONG).show();
                        context.startService(new Intent(context, GeolocationService.class));
                        Drawable ic_media_pause = ContextCompat.getDrawable(context, android.R.drawable.ic_media_pause);
                        if (ic_media_pause != null) {
                            item.setIcon(ic_media_pause);
                        }
                        ComponentName receiver = new ComponentName(context, AlarmReceiver.class);
                        PackageManager pm = context.getPackageManager();
                        pm.setComponentEnabledSetting(receiver,
                                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                PackageManager.DONT_KILL_APP);
                    }
                } else {
                    Toast.makeText(context, "Вы не авторизованы.", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void showDialogImportSelect() {
        final Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_select_import);
        TextView btn_cancel = builder.findViewById(R.id.bt_dialog_import_cancel);
        TextView btn_import = builder.findViewById(R.id.bt_dialog_import_ok);
        final ListView listView = builder.findViewById(R.id.lv_select_import);
        String[] arrayListFilesDescriptions = getResources().getStringArray(R.array.list_files_description);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context, R.layout.item_list_dialog_import_select, arrayListFilesDescriptions);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        btn_cancel.setOnClickListener(view -> builder.dismiss());
        btn_import.setOnClickListener(view -> {
            SparseBooleanArray checked = listView.getCheckedItemPositions();
            String[] listFiles = getResources().getStringArray(R.array.list_files);
            List<String> checkedList = new ArrayList<>();
            for (int i = 0; i < checked.size(); i++) {
                if (checked.valueAt(i)) {
                    checkedList.add(listFiles[checked.keyAt(i)]);
                }
            }
            String[] checkedArrayFiles = checkedList.toArray(new String[checkedList.size()]);
            if (checkedArrayFiles.length > 0) {
                waitCursor(true);
                ImportFileTask importFileTask = new ImportFileTask(context, StatisticsFragment.this);
                importFileTask.deleteDocuments(false);
                importFileTask.setSelectedImport(true);
                importFileTask.execute(checkedArrayFiles);
                builder.dismiss();
            } else {
                Toast.makeText(getActivity(), "Ничего не выбрано", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }


    private static boolean verifyPermissions(Activity activity) {
        // Check if we have write permission
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_PHONE_STATE};
            int permissionStorage = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionFineLocale = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionCoarseLocale = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
            int permissionReadPhoneState = ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE);
            if (permissionStorage != PackageManager.PERMISSION_GRANTED ||
                    permissionCoarseLocale != PackageManager.PERMISSION_GRANTED ||
                    permissionFineLocale != PackageManager.PERMISSION_GRANTED ||
                    permissionReadPhoneState != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission
                // so prompt the user
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, PERMISSIONS);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fillListAndBasement();
                } else {
                    verifyPermissions(context);
                }
                break;
        }

    }

    private void onAnimatorStart(ProgressBar[] progressBar) {
        for (ProgressBar bar : progressBar) {
            int random = new Random().nextInt(100) + 1;
            ObjectAnimator animation = ObjectAnimator.ofInt(bar, "progress", 0, random);
            animation.setDuration(3000);
            animation.start();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_all:
                viewSendAll.setEnabled(false);
                MyApp app = (MyApp) MyApp.getContext();
                SQLiteDatabase db = app.getDB();
                String region = App.getRegion(context);
                String codeAgent = App.get(context).getCodeAgent();

//                List<Contact> contacts = Contact.loadAll(db, codeAgent);
//                StringBuilder query = new StringBuilder();
//                for (Contact contact : contacts) {
//                    Post post = Post.getPostWhereName(db, contact.getPost());
//                    int postId = 0;
//                    if (post != null) {
//                        postId = post.getId();
//                    }
//                    query.append(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;\n", codeAgent,
//                            contact.getCodeClient(), postId, contact.getFio(), contact.getTel(), contact.getEmail(),
//                            contact.getResponsible(), contact.getDateBirth(), contact.getNote()));
//                }
//                if (!query.toString().isEmpty()) {
//                    String substring = query.toString().substring(0, query.lastIndexOf("\n"));
//                    OkHttpClientBuilder.buildCall("ПринятьФайлСКонтактами", substring, region).enqueue(new Callback() {
//                        @Override
//                        public void onFailure(Call call, IOException e) {
//
//                        }
//
//                        @Override
//                        public void onResponse(Call call, Response response) throws IOException {
//                            if (response.isSuccessful()) {
//                                ResponseBody responseBody = response.body();
//                                if (responseBody != null) {
//                                    String string1 = responseBody.string();
//                                    if (string1.contains("OK<EOF>")) {
//                                        for (Contact contact : contacts) {
//                                            Contact.updateReservedContact(db, contact.getId());
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    });
//                }
                createReserver();
                startAllVisitsReservTask();
                Short[] orderReservItems = getItemsForReserv(listOrder);

                Short[] paymentReservItems = getItemsForReserv(listPayment);
                if (orderReservItems != null) {
                    setReserv(orderReservItems);
                }
                Short[] returnReservItems = getItemsForReserv(listReturn);
                if (returnReservItems != null) {
                    setReservReturn(returnReservItems);
                }
                if (paymentReservItems != null) {
                    setReservPayment(paymentReservItems);
                }
                break;
            case R.id.more_info:
                getFragmentManager().beginTransaction()
                        .replace(R.id.contentMain, new WorkModeFragment())
                        .addToBackStack(null)
                        .commit();
        }
    }


    private Short[] getItemsForReserv(List<Document> document) {
        ArrayList<Short> itemsForReserv = new ArrayList<>();
        if (document != null) {
            for (Document doc : document) {
                if (doc.isHighLight == Document.NORMAL) {
                    itemsForReserv.add(doc.getID());
                }
            }
            Short[] items = new Short[itemsForReserv.size()];
            if (itemsForReserv.size() > 0) {
                items = new Short[itemsForReserv.size()];
                return itemsForReserv.toArray(items);
            }
            return items;
        }
        return null;
    }

    private void setReserv(Short[] items) {
        SetReservOrderTask mSetReservTask = new SetReservOrderTask(context, this);
        mSetReservTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, items);
        waitCursor(true);
    }

    private void setReservReturn(Short[] items) {
        SetReservReturnTask task = new SetReservReturnTask(context, this);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, items);
        waitCursor(true);
    }

    private void setReservPayment(Short[] items) {
        try {
            waitCursor(true);
            MyApp app = (MyApp) context.getApplicationContext();
            SQLiteDatabase db = app.getDB();
            JSONArray jsonArray = new JSONArray();
            for (Short current : items) {
                Payment data = Payment.load(db, current);
                PaymentRequest request = new PaymentRequest(data.getCodeAgent(), data.getCodeClient(),
                        data.getDocNum(), TimeFormatter.sdf1C.format(data.getDateDoc()), data.payment());
                Gson gson = new GsonBuilder().create();
                String strJson = gson.toJson(request);

                JSONObject jsonObj = new JSONObject(strJson);
                jsonArray.put(jsonObj);

            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Payments", jsonArray);
            String region = App.getRegion(MyApp.getContext());
            OkHttpClientBuilder.buildCall("СформироватьФайлОплат", jsonObject.toString(), region).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    handler.post(() -> {
                        waitCursor(false);
                        viewSendAll.setEnabled(true);
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (response.isSuccessful()) {
                        handler.post(() -> {
                            waitCursor(false);
                            viewSendAll.setEnabled(true);
                            if (response.message().contains("OK")) {
                                Toast.makeText(context, "Оплаты успешно отправлены", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                            }

                        });

                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void waitCursor(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


    private void fillListAndBasement() {
        try {
            MyApp app = (MyApp) context.getApplication();
            SQLiteDatabase db = app.getDB();
            DBProp.LoadDBProp(db);
            listVisit = Document.loadList(db, Const.DOCUMENTS_TYPE.Visit);
            listOrder = Document.loadList(db, Const.DOCUMENTS_TYPE.Order);
            listReturn = Document.loadList(db, Const.DOCUMENTS_TYPE.Return);
            listPayment = Document.loadList(db, Const.DOCUMENTS_TYPE.Payment);
            List<String> listImages = getImagesInFolder(App.get(context).getPhotosPath());
            if (listVisit != null) {
                txt_visit_count.setText(String.valueOf(listVisit.size()));
            }
            if (listOrder != null) {
                txt_ticket_count.setText(String.valueOf(listOrder.size()));
                float[] weightAndSumOrder = getTotalWeightAndSum(db, listOrder, Const.DOCUMENTS_TYPE.Order);
                txt_ticket_weight.setText(String.format("%.02f", weightAndSumOrder[0]) + " кг");
                txt_ticket_cost.setText(String.format("%.02f", weightAndSumOrder[1]) + " Р");
            }
            if (listReturn != null) {
                txt_revert_count.setText(String.valueOf(listReturn.size()));
                float[] weightAndSumReturn = getTotalWeightAndSum(db, listReturn, Const.DOCUMENTS_TYPE.Return);
                txt_revert_weight.setText(String.format("%.02f", weightAndSumReturn[0]) + " кг");
                txt_revert_cost.setText(String.format("%.02f", weightAndSumReturn[1]) + " Р");
            }
            if (listPayment != null) {
                float[] weightAndSumPayment = getTotalWeightAndSum(db, listPayment, Const.DOCUMENTS_TYPE.Payment);
                txt_total_cost.setText(String.format("%.02f", weightAndSumPayment[1]) + " Р");
                txt_invoices.setText(String.valueOf("Накладные: " + listPayment.size()));
            }

            if (listImages != null) {
                //txt_photo_cost.setText(String.valueOf(listImages.size()));
            }
        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
    }

    private float[] getTotalWeightAndSum(SQLiteDatabase db, List<Document> list, Const.DOCUMENTS_TYPE type) {
        float totalWeight = 0.0f;
        float totalSum = 0.0f;
        try {
            for (Document document : list) {
                switch (type) {
                    case Return:
                        Returns returnsData = Returns.load(db, document.getID());
                        totalSum += returnsData.getTotalAmount();
                        totalWeight += returnsData.getTotalWeight();
                        break;
                    case Payment:
                        Payment paymentData = Payment.load(db, document.getID());
                        totalSum += paymentData.payment();
                        break;
                    case Order:
                        if (document.getID() != Order.UNCONFIRMED_ORDER_NUMBER) {
                            Order orderData = Order.load(db, document.getID());
                            totalSum += orderData != null ? orderData.getTotalAmount() : 0;
                            totalWeight += orderData != null ? orderData.getTotalWeight() : 0;
                        }
                        break;
                }
            }
        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
        return new float[]{totalWeight, totalSum};
    }

    public List<String> getImagesInFolder(String photosPath) {
        File parentDir = new File(photosPath);
        if (!parentDir.exists() || !parentDir.isDirectory()) {
            throw new IllegalArgumentException("Parent directory cannot exist.");
        }
        List<String> imagesPath = new ArrayList<>();
        for (String path : parentDir.list()) {
            if (path.substring(path.lastIndexOf('.') + 1).equals("jpg")) {
                imagesPath.add(parentDir.getAbsolutePath() + "/" + path);
            }
        }
        return imagesPath;
    }


    protected Dialog m_import() {
        Date lastModify = null;
        File ostSkl = new File(App.get(context).getImpExpPath()
                + File.separator + getString(R.string.file_goods));
        if (ostSkl.exists()) {
            lastModify = new Date(ostSkl.lastModified());
        }
        String txt = null;
        if (MyApp.getUser() != null && !impexFileLastModify(23000)) {
            txt = "<font color='red'>У вас актуальные данные</font>";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String title = txt == null ? "Обновить данные" : txt;

        builder.setTitle(Html.fromHtml(title));
        builder.setIcon(R.mipmap.main_icon);
        String[] items = getResources().getStringArray(R.array.import_variants);
        String KEY = "text";
        String VALUE = "value";
        List<Map<String, String>> data = new ArrayList<>();
        String text;
        String description = "";
        for (int i = 0; i < items.length; i++) {
            text = items[i];
            switch (i) {
                case 0:// rest only
                    description = "последнее обновление: " + GlobalProc.getPref(context, R.string.pref_lastRestUpdate);
                    break;
                case 1:// internet
                    String s = "Рекомендуемый.";
                    if (txt != null) {
                        s = "С момента последнего обновления прошло менее суток.\nОбновление не требуется.";
                    }
                    description = lastModify != null ? String.format(
                            Locale.getDefault(), s + " Файлы от %s",
                            TimeFormatter.sdfDT.format(lastModify)) : s;
                    break;
            }
            Map<String, String> item = new HashMap<>(2);
            item.put(KEY, text);
            item.put(VALUE, description);
            data.add(item);
        }
        SimpleAdapter adapter = new SimpleAdapter(context, data,
                R.layout.dialog_import, new String[]{KEY, VALUE},
                new int[]{R.id.tv_cliName, R.id.tv_cliPost});
        builder.setAdapter(adapter, (dialog, which) -> {
            switch (which) {
                case 0:// Остатки
                    importRestOnly();
                    break;
                case 1:// Server
                    //Загружаем параметры для работы
                    String codeAgent = App.get(context).getCodeAgent();
                    String server = App.get(context).getFTPServer();
                    String connectionServer = App.get(context).connectionServer;
                    if (!codeAgent.equals(getString(R.string.lb_zero_code_manager)) && !server.isEmpty()) {
                        waitCursor(true);
                        Intent startGetLimitsTask = new Intent(context, GetParamsIntentService.class);
                        startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_AGENT, codeAgent);
                        startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_TYPE_SERVER, connectionServer);
                        context.startService(startGetLimitsTask);
                        MainActivity.flag = false;
                    } else {
                        Toast.makeText(context, "Вы не указали в настройках код менеджера или не выбрали сервер", Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    break;
            }
        });
        builder.setCancelable(true);
        return builder.create();
    }

    private void startUpdateRestFrom1CTask(String[] values) {
        UpdateRestFrom1CTask updateRestOnly = new UpdateRestFrom1CTask(context, this);
        updateRestOnly.execute(values);
    }

    private void importRestOnly() {
        if (App.get(context).connectionServer.equals("1")) {
            startUpdateRestFrom1CTask(null);
        } else {
            waitCursor(true);
            OkHttpClientBuilder.buildCall("ПолучитьОстатки", App.get(context).getCodeAgent(), App.getRegion(context)).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    handler.post(() -> {
                        waitCursor(false);
                        startUpdateRestFrom1CTask(null);
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        final String data = jsonObject.getString("data");
                        handler.post(() -> {
                            waitCursor(false);
                            startUpdateRestFrom1CTask(new String[]{data});
                        });
                    } catch (JSONException e) {
                        handler.post(() -> {
                            waitCursor(false);
                            startUpdateRestFrom1CTask(null);
                        });
                    }
                }
            });
        }
    }


    private void importFromServer() {
        GetFilesFromServerTask task = new GetFilesFromServerTask(context, this);
        if (App.isMerch()) {
            task.execute("FILES_MONITORING");
        } else {
            task.execute("FILES");
        }
    }

    protected void m_import_from_card() {
        if (deleteDocuments) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(getString(R.string.cap_confirm));
            builder.setIcon(R.mipmap.main_icon);
            builder.setMessage(getString(R.string.alert_message_import));
            builder.setPositiveButton(getString(R.string.bt_update),
                    (dialog, which) -> importFromCard());
            builder.setNegativeButton(getString(R.string.bt_no_update),
                    (dialog, which) -> dialog.cancel());
            builder.setCancelable(true);
            builder.create();
            builder.show();
        } else {
            importFromCard();
        }
    }

    private void importFromCard() {
        waitCursor(true);
        ImportFileTask importFileTask = new ImportFileTask(context, this);
        String[] files = getResources().getStringArray(R.array.list_files);
        importFileTask.deleteDocuments(deleteDocuments);
        importFileTask.execute(files);
    }


    private Boolean impexFileLastModify(int hour) {
        String fileName;
        if (App.isMerch()) {
            fileName = ARCHIVE_FILE_MERCH;
        } else {
            fileName = ARCHIVE_FILE_TRADE;
        }
        File ostSkl = new File(App.get(context).getImpExpPath()
                + File.separator + fileName);
        if (ostSkl.exists()) {
            Date lastModify = new Date(ostSkl.lastModified());
            Date date = new Date(System.currentTimeMillis() - 3600 * hour);
            Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Log.d(TAG, formatter.format(lastModify) + "  " + formatter.format(date));
            if (lastModify.getTime() <= date.getTime()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public void startAllVisitsReservTask() {
        if (mDocumentReserverThread != null && mDocumentReserverThread.getItemsCount() > 0) {
            return;
        }
        ArrayList<Visit> docs = Visit.loadList(context.getApplicationContext(), Visit.STATE_NORMAL);
        // Сразу после загрузки добавляем документы из списка в поток
        // для отправки
        if (docs.size() > 0) {
            waitCursor(true);

            for (Visit visit : docs) {
                String id = visit.getID().toString();
                try {
                    MyApp app = (MyApp) context.getApplication();
                    SQLiteDatabase db = app.getDB();

                    String sql = String.format("SELECT %s, COUNT(%s) as ordersCnt FROM %s WHERE %s = '%s' ",
                            OrderMetaData.FIELD_NUM,
                            OrderMetaData.FIELD_NUM,
                            OrderMetaData.TABLE_NAME,
                            OrderMetaData.FIELD_VISIT_FK, id);

                    Cursor rows = db.rawQuery(sql, null);
                    if (rows.moveToFirst()) {
                        visit.hasOrder(rows.getInt(1) > 0);
                    }
                    rows.close();

                    sql = String.format("SELECT %s, COUNT(%s) as returnsCnt FROM %s WHERE %s = '%s' ",
                            ReturnMetaData.FIELD_NUM,
                            ReturnMetaData.FIELD_NUM,
                            ReturnMetaData.TABLE_NAME,
                            ReturnMetaData.FIELD_VISIT_FK, id);
                    rows = db.rawQuery(sql, null);
                    if (rows.moveToFirst()) {
                        visit.hasReturns(rows.getInt(1) > 0);
                    }
                    rows.close();
                    sql = String.format("SELECT COUNT(%s) as paymentsCnt FROM %s WHERE %s = '%s' ",
                            PaymentsMetaData.FIELD_CODE_CLIENT,
                            PaymentsMetaData.TABLE_NAME,
                            PaymentsMetaData.FIELD_VISIT_FK, id);
                    rows = db.rawQuery(sql, null);
                    if (rows.moveToFirst()) {
                        visit.hasPayments(rows.getInt(0) > 0);
                    }
                    rows.close();

                    sql = String.format(Locale.US, "select * from %s where %s = '%s'",
                            PaybackPhotoMetaData.TABLE_NAME, PaybackPhotoMetaData.FIELD_UUID, visit.getID().toString());

                    rows = db.rawQuery(sql, null);
                    while (rows.moveToNext()) {
                        visit.addPaybackPhotos(new PaybackPhoto(rows.getInt(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_ID)),
                                rows.getString(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_URL))));
                    }
                    rows.close();

                } catch (Exception e) {
                    Log.e(TAG, "Ошибка при выборке подчиненных документов");
                }
                mDocumentReserverThread.queueVisit(visit, id);
            }
        } else {
            Log.d(TAG, "empty visit");
        }
    }


    private void createReserver() {
        // Создаем резервер, подписываемся на события
        mDocumentReserverThread = new DocumentsReserver<>(
                App.get(context).getCodeAgent(), mHandler,
                Credentials.load(context));
        mDocumentReserverThread.setListener((visit, result, typeReserveServer) -> {
            // Вносим изменения в БД
            if (result == DocumentsReserver.RESULT_SUCCESS) {
                visit.updateState(context, Visit.STATE_SENDED, typeReserveServer, true);
            }
            if (mHandler != null) {
                mHandler.sendEmptyMessage(MESSAGE_UPDATE_LIST);
            }
            Log.d(TAG, "reserve visit");
        });
        mDocumentReserverThread.getLooper();
        mDocumentReserverThread.start();
        Log.i(TAG, "Background thread started");
    }


    /**
     CALLBACKS -----------------------------------------|
     ****************************************************/

    /**
     * Получение файлов FTP завершено
     */
    @Override
    public void onTaskComplete(GetFilesFTPTask ftpTask) {

    }

    /**
     * Получение архива с файлами завершено
     */
    @Override
    public void onTaskComplete(GetFilesFromServerTask serverTask) {
        boolean res;
        try {
            res = serverTask.get();
        } catch (Exception e) {
            res = false;
        }
        if (serverTask.isCancelled()) {
            GlobalProc.mToast(context, "Отменено пользователем");
        } else if (res) {
            if (isAdded()) {
                // Теперь файл надо распаковать и запустить обновление
                if (GlobalProc.unpack(App.get(context)
                        .getImpExpPath(), App.get(context)
                        .getImpExpPath(), App.isMerch() ? ARCHIVE_FILE_MERCH : ARCHIVE_FILE_TRADE)) {
                    m_import_from_card();
                    //showReport();
                } else {
                    GlobalProc.mToast(context, "Ошибка при распаковке архива");
                }
            }
        } else {
            GlobalProc.mToast(context, "Ошибка при получении файла: " + serverTask.getMessage());
        }
    }

    @Override
    public void onTaskComplete(ImportFileTask task) {
        boolean res;
        try {
            res = task.get();
        } catch (Exception e) {
            res = false;
        }
        String message;
        if (task.isCancelled()) {
            message = "Отменено пользователем";
        } else {
            if (res) {
                message = "Импорт завершен";
                //sendBroadcast(new Intent(AppWidget.FORCE_WIDGET_UPDATE));
                MyApp myApp = (MyApp) MyApp.getContext();
                DBHelper.execMultipleSQLTXN(myApp.getDB(), new String[]{PromotionMetaData.CLEAR,
                        PromotionItemMetaData.CLEAR, PromotionBonusesMetaData.CLEAR,
                        PromotionProductMetaData.CLEAR});
                if (!task.selectedImport) {
                    getPromotionRx = new GetPromotionRx(this);
                    getPromotionRx.onStart(1);
                    showProgress(context, "Идет загрузка Акций");
                }
            } else {
                message = "Ошибка при обновлении данных";
            }
        }
        waitCursor(false);
        fillListAndBasement();
        GlobalProc.mToast(context, message);
        deleteDocuments = true;

    }

    @Override
    public void onTaskComplete(SetReservPaymentTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            txt = context.getString(R.string.mes_reserv_success);
        } else {
            txt = getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getMessage();
        }

        GlobalProc.mToast(context, txt);
        waitCursor(false);
    }

    @Override
    public void onTaskComplete(SetReservOrderTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            txt = getString(R.string.mes_reserv_success);
        } else {
            txt = getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getMessage();
        }

        GlobalProc.mToast(context, txt);
        waitCursor(false);
    }

    @Override
    public void onTaskComplete(SetReservReturnTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            txt = getString(R.string.mes_reserv_success);
        } else {
            txt = getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getMessage();
        }

        GlobalProc.mToast(context, txt);
        waitCursor(false);
    }

    @Override
    public void onTaskComplete(UpdateProgrammTask updateTask) {
        waitCursor(false);

        byte res = UpdateProgrammTask.FAIL;
        try {
            res = updateTask.get();
        } catch (Exception e) {
            res = UpdateProgrammTask.FAIL;
        }

        switch (res) {
            case UpdateProgrammTask.ACTUAL:
                GlobalProc.mToast(context, updateTask.getResultMessage());
                break;
            case UpdateProgrammTask.DOWNLOADED:
                String path = App.get(context.getApplicationContext()).getImpExpPath()
                        + File.separator + Const.APK;
                File file = new File(path);

                if (file.exists()) {
                    Intent startAPK = new Intent(Intent.ACTION_VIEW);
                    startAPK.setDataAndType(Uri.fromFile(file),
                            "application/vnd.android.package-archive");
                    startActivity(startAPK);
                }
                break;
            case UpdateProgrammTask.FAIL:
                GlobalProc.mToast(context, updateTask.getResultMessage());
                break;
        }
    }

    @Override
    public void onTaskComplete(UpdateRestFrom1CTask updateRestFrom1CTask) {
        boolean res;
        try {
            res = updateRestFrom1CTask.get();
        } catch (Exception e) {
            res = false;
        }
        if (res) {
            GlobalProc.setPref(context, R.string.pref_lastRestUpdate,
                    TimeFormatter.sdfDT.format(Calendar.getInstance().getTime()));
        }
        GlobalProc.mToast(context, updateRestFrom1CTask.getMessage());
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCompleteGetParam(String s) {
        waitCursor(false);
        if (s.equals("OK")) {
            importFromServer();
        }
    }

    @Override
    public void onNext(int page) {
        getPromotionRx.onStart(page);
    }

    @Override
    public void onError(Throwable t) {
        new Handler(Looper.getMainLooper()).post(() -> {
            dismiss();
            GlobalProc.mToast(context, t.getMessage());
            getListAutoOrder();
        });
    }

    @Override
    public void onComplete() {
        new Handler(Looper.getMainLooper()).post(() -> {
            dismiss();
            GlobalProc.mToast(context, "Загрузка акций завершена");
            getListAutoOrder();
        });
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.dismiss();
        }
    }

    private void showProgress(Context context, String message) {
        mProgress = new ProgressDialog(context);
        String text = context.getString(R.string.mes_wait_for_load);
        mProgress.setMessage(message);
        mProgress.setTitle(text);
        mProgress.setCancelable(false);
        mProgress.show();
    }

    private void getListAutoOrder() {
        showProgress(context, "Идет загрузка списка автозаказа");
        OkHttpClientBuilder.buildCall("ПолучитьСписокАвтозаказа", "", App.getRegion(MyApp.getContext())).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                dismiss();
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String dataBean = jsonObject.getString("data");
                        String autoOrderPref = dataBean.replaceAll("[\"\r\n\\[\\]]", "").replaceAll(",", "|");
                        if (!autoOrderPref.isEmpty()) {
                            GlobalProc.setPref(context, R.string.pref_autoOrder, autoOrderPref);
                            App.Reload(context);
                        }
                        dismiss();
                    } else {
                        dismiss();
                    }
                } catch (Exception e) {
                    dismiss();
                }
            }
        });
    }

}
