package com.madlab.mtrade.grinfeld.roman.connectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by GrinfeldRA
 */

public class ConnectivityInterceptor implements Interceptor {

    private Context mContext;
    private Handler handler;

    public ConnectivityInterceptor(Context context) {
        mContext = context;
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!isOnline(mContext)) {
            handler.post(() -> Toast.makeText(mContext, "Нет интернет соединения для обновления Акций", Toast.LENGTH_SHORT).show());
        }

        Request.Builder builder = chain.request().newBuilder();
        builder.header("Accept", "application/json");
        builder.header("X-CMC_PRO_API_KEY", "fbbed4bf-3a78-44a3-aed8-eadada18b32f");
        return chain.proceed(builder.build());
    }

    private  boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (connectivityManager != null) {
            netInfo = connectivityManager.getActiveNetworkInfo();
        }
        return (netInfo != null && netInfo.isConnected());
    }

}