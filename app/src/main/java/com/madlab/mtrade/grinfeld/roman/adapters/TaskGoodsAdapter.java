package com.madlab.mtrade.grinfeld.roman.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;

import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 12.03.2018.
 */

public class TaskGoodsAdapter extends ArrayAdapter<GoodsInTask> {
    private ArrayList<GoodsInTask> mItems;
    private int mSelected = -1;

    public TaskGoodsAdapter(Context context, ArrayList<GoodsInTask> items) {
        super(context, R.layout.subtask_item, items);
        mItems = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.goods_in_task_item, null);
        }

        if (mItems == null)
            return convertView;

        GoodsInTask git = mItems.get(position);
        if (git != null) {
            LinearLayout rl = (LinearLayout) v;
            if (rl != null) {
                if (mSelected > -1 && mSelected == position) {
                    rl.setBackgroundColor(Color.DKGRAY);
                } else {
                    rl.setBackgroundColor(Color.TRANSPARENT);
                }
            }

            TextView tvDescr = v.findViewById(R.id.goodsInTask_description);
            tvDescr.setText(git.name());

            int status = git.percent() < 80 ? 0 : git.percent() > 95 ? 2 : 1;
            switch (status) {
                case 0:
                    tvDescr.setTextColor(Color.RED);
                    break;
                case 1:
                    tvDescr.setTextColor(Const.COLOR_GOLD);
                    break;
                case 2:
                    tvDescr.setTextColor(Color.GREEN);
                    break;
                default:
                    break;
            }

            TextView tvPercent =  v
                    .findViewById(R.id.goodsInTask_lbPercent);
            tvPercent.setText(Integer.toString(git.percent()));

            TextView tvTotalCount =  v
                    .findViewById(R.id.goodsInTask_lbCountTotal);
            tvTotalCount.setText(Integer.toString(git.count()));

            TextView tvSold =  v
                    .findViewById(R.id.goodsInTask_lbCount);
            tvSold.setText(Integer.toString(git.sold()));
        }
        return v;
    }

    public void setSelected(int index) {
        mSelected = index;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }
}
