package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by grinfeldra
 */
public class PaybackPhotoMetaData {



    public static final String TABLE_NAME = "PaybackPhoto";

    public static final String FIELD_ID = "id";
    public static final String FIELD_URL = "url";
    public static final String FIELD_UUID = "uuid";
    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;


    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "%s	text	NOT NULL DEFAULT '',"
                    + "%s	text	NOT NULL DEFAULT '')",
            TABLE_NAME, FIELD_ID, FIELD_URL, FIELD_UUID);


    public static String insertQuery(String url, String uuid) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s) "
                        + "VALUES "
                        + "('%s', '%s')",
                TABLE_NAME, FIELD_URL, FIELD_UUID, url, uuid);
    }

    public static String deleteQuery(int id) {
        return String.format(Locale.ENGLISH, "DELETE FROM %s WHERE %s = %d", TABLE_NAME,
                FIELD_ID, id);
    }

}
