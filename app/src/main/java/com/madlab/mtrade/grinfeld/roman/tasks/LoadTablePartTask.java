package com.madlab.mtrade.grinfeld.roman.tasks;

import android.app.Activity;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadTablePartCompleteListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import okhttp3.Response;

/**
 * Created by GrinfeldRA on 27.12.2017.
 */

public class LoadTablePartTask extends AsyncTask<String, Integer, Boolean> {
    public static final String GOODS_NAME = "goodsName";
    public static final String COUNT = "count";
    public static final String AMOUNT = "amount";
    public static final String IS_EXIST = "is_exist";

    public static String COMMAND = "GET_ORDER_TABLE_PART";

    private Activity base;
    private Credentials connectInfo;
    private ILoadTablePartCompleteListener taskCompleteListener;

    private DalimoClient client;
    private String lastError;
    private List<Map<String, String>> resultList;

    public LoadTablePartTask(Activity base, Credentials info,
                             ILoadTablePartCompleteListener listener) {
        this.base = base;
        connectInfo = info;
        taskCompleteListener = listener;
    }

    @Override
    protected Boolean doInBackground(String... arg0) {
        boolean wasError = false;
        for (String docID : arg0) {
            String answer = "";
            if (App.get(base).connectionServer.equals("1")) {
                client = new DalimoClient(connectInfo);
                if (client.connect()) {
                    String query = String.format("%s;%s;%s", COMMAND, docID, Const.END_MESSAGE);
                    client.send(query);
                    answer = client.receive();
                } else {
                    lastError = "Не удалось подключиться";
                    wasError = true;
                    continue;
                }
                if (answer.contains(DalimoClient.ERROR)) {
                    wasError = true;
                    continue;
                }
            }else {
                try {
                    Response response = OkHttpClientBuilder.buildCall("СодержаниеВОперативке", docID, App.getRegion(base)).execute();
                    if (response.isSuccessful()){
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        answer = jsonObject.getString("data");
                    }

                } catch (IOException e) {
                    wasError = true;
                    lastError = e.getMessage();
                    continue;
                } catch (JSONException e) {
                    wasError = true;
                    lastError = e.getMessage();
                    continue;
                }
            }


            resultList = parseAnswer(answer);
        }

        return !wasError;
    }

    private List<Map<String, String>> parseAnswer(String answer) {
        while (base == null) {
            try {
                wait(100);
            } catch (Exception e) {
            }
        }
        MyApp app = (MyApp) base.getApplication();

        List<Map<String, String>> result;
        result = new ArrayList<Map<String, String>>();
        String[] list = answer.split(";");
        StringTokenizer token;
        for (String item : list) {
            try {
                token = new StringTokenizer(item, Const.DELIMITTER);
                if (token.countTokens() > 1) {
                    String codeGoods = token.nextToken();
                    Goods goodsInfo = Goods.load(app.getDB(), codeGoods);
                    if (goodsInfo != null && token.hasMoreTokens()) {
                        String name = goodsInfo.getNameFull();
                        Map<String, String> data = new HashMap<String, String>(
                                2);
                        data.put(GOODS_NAME, name);
                        String cnt = String.format(
                                "%s %s",
                                token.nextToken(),
                                goodsInfo.isWeightGood() ? "гол." : goodsInfo
                                        .getBaseMeash());
                        data.put(COUNT, cnt);
                        if (token.hasMoreTokens()) {
                            float amount = GlobalProc.parseFloat(token
                                    .nextToken());
                            data.put(AMOUNT, GlobalProc.formatMoney(amount));
                        } else {
                            data.put(AMOUNT, "0,00 руб.");
                        }
                        if (token.hasMoreTokens()){
                            data.put(IS_EXIST, token.nextToken());
                        }
                        result.add(data);
                    }
                }
            } catch (Exception e) {
                continue;
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (taskCompleteListener != null) {
            taskCompleteListener.onTaskComplete(this);
        }
    }

    public void link(Activity newActivity) {
        base = newActivity;
    }

    public void unlink() {
        base = null;
    }

    public String getLastError() {
        return lastError;
    }

    public List<Map<String, String>> getList() {
        return resultList;
    }
}