package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsGroupTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsOnTreeClickTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGroupMatrixTask;


public interface ILoadGoodsComplete {

	void onTaskComplete(LoadGoodsGroupTask task);
	void onTaskComplete(LoadGoodsOnTreeClickTask task);
	void onTaskComplete(LoadGroupMatrixTask task);

}
