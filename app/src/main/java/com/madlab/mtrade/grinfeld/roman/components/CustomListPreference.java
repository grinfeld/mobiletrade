package com.madlab.mtrade.grinfeld.roman.components;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA on 25.04.2018.
 */

public class CustomListPreference extends ListPreference {

    private static final String TAG = "#CustomListPreference";
    private CharSequence[] entries;
    private CharSequence[] entryValues;
    private Context mContext;
    private List<RadioButton> rButtonList;
    private SharedPreferences.Editor editor;
    private SharedPreferences prefs;
    private String APP_PREFERENCES_ENTRIES = "entries";
    private String APP_PREFERENCES_OTHER_SERVER = "other_server";

    public CustomListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        prefs = getSharedPreferences();
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        entries = getEntries();
        entryValues = getEntryValues();
        rButtonList = new ArrayList<>();
        if (entries == null || entryValues == null || entries.length != entryValues.length )
        {
            throw new IllegalStateException(
                    "ListPreference requires an entries array and an entryValues array which are both the same length");
        }

        CustomListPreferenceAdapter customListPreferenceAdapter = new CustomListPreferenceAdapter(mContext);

        builder.setAdapter(customListPreferenceAdapter, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                    Log.d(TAG, "onClickDel");
            }
        });

    }

    private class CustomListPreferenceAdapter extends BaseAdapter {


        CustomListPreferenceAdapter(Context context) {
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
        }

        @Override
        public int getCount() {
            return 17;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public View getView(final int position, View mInflater, ViewGroup parent) {
            View row = mInflater;
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            if(row == null) {
                //If it's not the last one: use a normal holder...
                if(position < 16) {
                    final NormalHolder holder;

                    row = vi.inflate(R.layout.normal_list_preference_row, null);
                    if(prefs.getString(APP_PREFERENCES_ENTRIES,"").equals(entries[position].toString())) {
                        holder = new NormalHolder(row, position,true);
                    } else {
                        holder = new NormalHolder(row, position,false);
                    }

                    row.setTag(holder);
                    row.setClickable(true);
                    row.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            for(RadioButton rb : rButtonList) {
                                if(rb.getId() != position)
                                    rb.setChecked(false);
                            }
                            String key = entries[position].toString();
                            String value = entryValues[position].toString();
                            Log.v("Editor", "putting string" + value);
                            editor = prefs.edit();
                            editor.putString(mContext.getString(R.string.pref_ftp_server), value);
                            editor.putString(APP_PREFERENCES_ENTRIES, key);
                            editor.apply();
                            Dialog mDialog = getDialog();
                            mDialog.dismiss();
                        }
                    });
                } else {
                    //Use the custom row
                    getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    row = vi.inflate(R.layout.custom_list_preference_row, null);
                    String fromPref = prefs.getString(APP_PREFERENCES_ENTRIES,"");
                    boolean flag = false;
                    for(CharSequence entry  : entries) {
                        if(entry.toString().equals(fromPref)) {
                            flag=true;
                        }
                    }
                    //And use a "custom holder"
                    CustomHolder holder;
                    if(!flag) {
                        holder = new CustomHolder(row, position, fromPref, true);
                    } else {
                        holder = new CustomHolder(row, position, prefs.getString(APP_PREFERENCES_OTHER_SERVER,""), false);

                    }
                    row.setTag(holder);
                }
            }
            return row;
        }
        /*
         * This class just shows the information in row from the position and the PreferenceList entries
         */
        class NormalHolder {
            private TextView text = null;
            private RadioButton rButton = null;

            NormalHolder(View row, final int position, boolean isChecked) {
                text = row.findViewById(R.id.custom_list_view_row_text_view);
                text.setText(entries[position]);
                rButton = row.findViewById(R.id.custom_list_view_row_radio_button);
                rButton.setId(position);
                rButton.setChecked(isChecked);

                // also need to do something to check your preference and set the right button as checked
                rButtonList.add(rButton);
                rButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked) {
                            for(RadioButton rb : rButtonList) {
                                if(rb.getId() != position)
                                    rb.setChecked(false);
                            }
                            String key = entries[position].toString();
                            String value = entryValues[position].toString();
                            Log.v("Editor", "putting string" + value);
                            editor = prefs.edit();
                            editor.putString(mContext.getString(R.string.pref_ftp_server), value);
                            editor.putString(APP_PREFERENCES_ENTRIES, key);
                            editor.apply();
                            Dialog mDialog = getDialog();
                            mDialog.dismiss();
                        }
                    }
                });
            }

        }
        /*
         * This class display the text within the EditText
         */
        class CustomHolder {
            private EditText text = null;
            private RadioButton rButton = null;

            CustomHolder(View row, final int position, String pref, boolean checked) {
                text = row.findViewById(R.id.ET_prefs_customText);
                text.setText(pref);
                rButton = row.findViewById(R.id.custom_list_view_row_radio_button);
                rButton.setId(position);
                rButton.setChecked(checked);
                rButtonList.add(rButton);
                rButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(isChecked) {
                            String value = text.getText().toString();
                            for(RadioButton rb : rButtonList) {
                                if(rb.getId() != position)
                                    rb.setChecked(false);
                            }
                            Log.v("Editor", "putting string" + value);
                            editor = prefs.edit();
                            editor.putString(mContext.getString(R.string.pref_ftp_server), value);
                            editor.putString(APP_PREFERENCES_ENTRIES, value);
                            editor.putString(APP_PREFERENCES_OTHER_SERVER, value);
                            editor.apply();
                            Dialog mDialog = getDialog();
                            mDialog.dismiss();
                        }
                    }
                });
            }

        }
    }
}