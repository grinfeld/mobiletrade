package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.CamActivity;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.adapters.ReturnGoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.iface.IGetReturnItemsPricesComplete;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;
import com.madlab.mtrade.grinfeld.roman.services.UploadFileIntentService;
import com.madlab.mtrade.grinfeld.roman.tasks.GetReturnItemPricesTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservOrderTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservPaymentTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static com.madlab.mtrade.grinfeld.roman.fragments.ReturnCommonFragment.RETURN_MODE;
import static com.madlab.mtrade.grinfeld.roman.fragments.VisitContainFragment.CLIENT_CODE;
import static com.madlab.mtrade.grinfeld.roman.fragments.VisitContainFragment.IDD_NEW_PHOTO;

/**
 * Created by GrinfeldRA on 22.11.2017.
 */

public class ReturnGoodsFragment extends ListFragment implements View.OnClickListener, IGetReturnItemsPricesComplete {

    public static final String TAG = "#ReturnGoodsFragment";

    private final static byte DOC_NEW = 21;
    private final static byte DOC_EDIT = 22;

    private final static byte IDD_ADD = 0;
    private final static byte IDD_MODIFY_ITEM = 1;

    public final static byte GOODS_ADDED = 65;
    public static final String TYPE_PHOTO_REPORT = "type_photo_report";
    public static final int RETURN_PHOTO_REPORT = 12;
    public static final String KEY_UUID_DOC = "uuid_doc";
    public static final String KEY_RETURN_FOREIGN = "key_foreign";


    byte currentDocType = DOC_NEW;

    ListView lv;
    TextView tvCliName;
    TextView tvCliAddress;
    TextView tvTotAmount;
    TextView tvTotCount;
    TextView tvTotWeight;
    ProgressBar progressBar;
    Button btOk, btCancel, btAddGoods;
    private Context context;
    private BroadcastReceiver uploadTask;
    private ReturnGoodsAdapter mAdapter;
    private MenuItem photoReturn;

    private Returns.ItemAddListener mAddListener = new Returns.ItemAddListener() {
        @Override
        public void onItemAdd(float totalSum, short count, float totalWeight) {
            fillBasement(totalSum, count, totalWeight);
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_return_goods, container, false);
        setHasOptionsMenu(true);
        context = getActivity();
        progressBar = rootView.findViewById(R.id.progress_bar_return_goods);
        tvCliName = rootView.findViewById(R.id.rg_lbName);
        tvCliAddress = rootView.findViewById(R.id.rg_lbAddress);
        tvTotAmount = rootView.findViewById(R.id.rg_lbTotalAmount);
        tvTotCount = rootView.findViewById(R.id.rg_lbTotalCount);
        tvTotWeight = rootView.findViewById(R.id.rg_lbTotalWeight);
        btOk = rootView.findViewById(R.id.rg_btOk);
        btCancel = rootView.findViewById(R.id.rg_btCancel);
        btAddGoods = rootView.findViewById(R.id.rg_btAddGoods);
        lv = rootView.findViewById(android.R.id.list);
        registerForContextMenu(lv);
        btOk.setOnClickListener(this);
        btAddGoods.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("Список товаров");
            supportActionBar.setTitle("Новый возврат");
        }
        setMode();
        Returns retData = Returns.getReturn();
        if (retData != null) {
            retData.setItemAddListener(mAddListener);
            if (savedInstanceState == null) {
                printClientData();
                setAdapter(retData);
                fillBasement(retData.getTotalAmount(),
                        retData.getItemsCount(), retData.getTotalWeight());
            }
        } else {
            //_setResult(RESULT_CANCELED);
        }
        uploadTask = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte status = intent.getByteExtra(
                        UploadFileIntentService.EXTRA_STATUS, (byte) 0);
                switch (status) {
                    case UploadFileIntentService.STATUS_ERROR:
                        GlobalProc.mToast(context, "Не удалось отправить");
                        break;
                    case UploadFileIntentService.STATUS_OK:
                        GlobalProc.mToast(context, "Успешно отправлено");
                        break;
                }
            }
        };
        IntentFilter filterUpload = new IntentFilter(UploadFileIntentService.BROADCAST_ACTION);
        context.registerReceiver(uploadTask, filterUpload);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            context.unregisterReceiver(uploadTask);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remainder_and_discounts:
                Context context = getActivity();
                Returns data = Returns.getReturn();
                if (data != null && data.getItems() != null
                        && data.getItemsCount() > 0 && data.getClient() != null) {
                    String codeCli = data.getClient().getCode();
                    waitCursor(true);
                    Credentials connectInfo = Credentials.load(context);
                    GetReturnItemPricesTask getPrices = new GetReturnItemPricesTask(
                            connectInfo, App.get(context.getApplicationContext()).getCodeAgent(), codeCli,
                            data.getItems(), this);
                    getPrices.execute();
                }
                break;
            case R.id.action_return_photo:
                Intent intent = new Intent(getActivity(), CamActivity.class);
                intent.putExtra(CLIENT_CODE, Returns.getReturn().getClient().getCode());
                intent.putExtra(TYPE_PHOTO_REPORT, RETURN_PHOTO_REPORT);
                if (Returns.getReturn().getUuidDoc() != null) {
                    intent.putExtra(KEY_UUID_DOC, Returns.getReturn().getUuidDoc().toString());
                }
                UUID uuid = Returns.getReturn().visitFK();
                String uuidStr = "payback";
                if (uuid != null) {
                    uuidStr = uuid.toString();
                }

                intent.putExtra(VisitContainFragment.UUID_KEY, uuidStr);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (intent.resolveActivity(getActivity().getApplicationContext().getPackageManager()) != null) {
                    startActivityForResult(intent, IDD_NEW_PHOTO);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void waitCursor(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.return_goods_context_menu, menu);
        MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onContextItemSelected(item);
                return true;
            }
        };
        for (int i = 0, n = menu.size(); i < n; i++) {
            menu.getItem(i).setOnMenuItemClickListener(listener);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.return_goods_menu, menu);
        photoReturn = menu.findItem(R.id.action_return_photo);
        if (Returns.getReturn().getmTypeAct() == 2) {
            photoReturn.setVisible(true);
            Drawable drawable = ContextCompat.getDrawable(context, R.mipmap.photo_return);
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN));
            }
            photoReturn.setIcon(drawable);
        } else {
            photoReturn.setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Returns returnData = Returns.getReturn();
        if (returnData == null) {
            return false;
        }
        try {
            AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            int position = aMenuInfo.position;
            ReturnItem returnItem = returnData.getItem(position);
            if (returnItem == null)
                return false;
            switch (item.getItemId()) {
                case R.id.menuReturnGoodsList_info: {
                    try {
                        MyApp app = (MyApp) getActivity().getApplication();
                        Goods g = Goods.load(app.getDB(), returnItem.getCodeGoods());
                        Fragment fragment = GoodsInfoContainer.newInstance(g, Returns.getReturn().getClient().isForeignAgent());
                        getFragmentManager().beginTransaction()
                                .replace(R.id.contentMain, fragment)
                                .addToBackStack(null)
                                .commit();
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                        GlobalProc.mToast(getActivity(), e.toString());
                    }
                    return true;
                }
                case R.id.menuReturnGoodsList_edit: {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment fragmentModify = new ReturnQuantityFragment();
                    fragmentModify.setTargetFragment(this, IDD_MODIFY_ITEM);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(ReturnItem.KEY, returnItem);
                    bundle.putBoolean(KEY_RETURN_FOREIGN, Returns.getReturn().getClient().isForeignAgent());
                    fragmentModify.setArguments(bundle);
                    ft.addToBackStack(null);
                    ft.replace(R.id.contentMain, fragmentModify);
                    ft.commit();
                    return true;
                }
                case R.id.menuReturnGoodsList_delete: {
                    mDeleteConfirm(position).show();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(getActivity(), e.toString());
        }
        return super.onContextItemSelected(item);
    }


    protected Dialog mDeleteConfirm(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.cap_confirm));
        builder.setIcon(R.mipmap.main_icon);
        builder.setMessage(getString(R.string.mes_del_item_confirm));
        builder.setPositiveButton(getString(R.string.menu_delete),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (Returns.getReturn() != null) {
                            Returns.getReturn().removeItem(position);
                            notifyAdapter();
                        }
                    }
                });
        builder.setNegativeButton(getString(R.string.bt_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setCancelable(true);
        return builder.create();
    }


    private void setMode() {
        String mode = getArguments().getString(RETURN_MODE);
        if (mode != null) {
            if (mode.contains("edit")) {
                currentDocType = DOC_EDIT;
            } else {
                currentDocType = DOC_NEW;
            }
        }
    }


    private void notifyAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
        if (Returns.getReturn() != null) {
            btOk.setEnabled(Returns.getReturn().getItemsCount() > 0);
        }
    }

    private void setAdapter(Returns returnData) {
        ArrayList<ReturnItem> items = returnData.getItems();
        if (lv != null && items != null) {
            mAdapter = new ReturnGoodsAdapter(getActivity(), R.layout.return_goods_item, items, Returns.getReturn().getClient().isForeignAgent());
            lv.setAdapter(mAdapter);
        }
    }

    private void printClientData() {
        if (Returns.getReturn().getClient() == null) {
            return;
        }
        Client client = Returns.getReturn().getClient();
        try {
            tvCliName.setText(client.mNameFull);
            tvCliAddress.setText(client.mPostAddress);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    private void fillBasement(float totalAmount, short totalCount, float totalWeight) {
        if (tvTotAmount != null) {
            tvTotAmount.setText(GlobalProc.toForeignPrice(totalAmount, Returns.getReturn().getClient().isForeignAgent()));
        }
        if (tvTotCount != null) {
            tvTotCount.setText(Short.toString(totalCount));
        }
        if (tvTotWeight != null) {
            tvTotWeight.setText(GlobalProc.formatWeight(totalWeight) + Const.POSTFIX_WEIGHT);
        }
    }


    protected Dialog dlgNotify() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(android.R.string.dialog_alert_title));
        builder.setIcon(R.mipmap.main_icon);
        builder.setMessage(getString(R.string.mes_checkPrices));
        builder.setPositiveButton(getString(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveReturn(Returns.getReturn());
                    }
                });

        builder.setNegativeButton(getString(android.R.string.cancel),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }

    protected Dialog dlgGoodsFilter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.cap_choose_goods));
        builder.setIcon(R.mipmap.main_icon);
        builder.setItems(R.array.goods_filter_list_return,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        byte currentMode = GoodsList.MODE_ALL;
                        switch (which) {
                            case 0: // All
                                // action = WITH_TREE;
                                currentMode = GoodsList.MODE_ALL;
                                break;
                            case 1:
                                // action = WITH_TREE;
                                currentMode = GoodsList.MODE_MATRIX;
                                break;
                            default:
                                break;
                        }
                        startGoodsSelectFragment(currentMode, null, GoodsAdapter.MODE_RETURN, Returns.getReturn().getClient());
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }

    private void startGoodsSelectFragment(byte typeLoad, Node selected, byte mode, Client client) {
        GoodsTreeFragment fragment = new GoodsTreeFragment();
        Bundle args = new Bundle();
        args.putByte(GoodsTreeFragment.EXTRA_TYPE, typeLoad);
        args.putParcelable(GoodsTreeFragment.EXTRA_NODE, selected);
        args.putByte(GoodsTreeFragment.EXTRA_MODE, mode);
        args.putParcelable(GoodsTreeFragment.EXTRA_CLIENT, client);
        ArrayList<GoodsInDocument> list = createGoodsInDocList();
        args.putParcelableArrayList(GoodsInDocument.KEY, list);
        fragment.setArguments(args);
        fragment.setTargetFragment(this, IDD_ADD);
        getFragmentManager().beginTransaction()
                .replace(R.id.contentMain, fragment)
                .addToBackStack(null)
                .commit();
    }

    private ArrayList<GoodsInDocument> createGoodsInDocList() {
        ArrayList<GoodsInDocument> result = new ArrayList<>();
        for (ReturnItem item : Returns.getReturn().getItems()) {
            GoodsInDocument gid = new GoodsInDocument(item.getCodeGoods(),
                    item.inPack() ? item.getCountPack() : (int) item.getCount(),
                    item.inPack() ? (byte) 1 : 0,
                    1f, item.getPrice(), item.getWeight());
            result.add(gid);
        }
        return result;
    }

    private void saveReturn(Returns returnData) {
        if (returnData == null)
            return;

        SQLiteDatabase db;

        String strRes;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            if (currentDocType == DOC_NEW) {
                returnData.setNumReturn(DBProp.GetNumOrder());
                if (returnData.insert(db)) {
                    DBProp.IncNumOrder();
                    if (DBProp.UpdateDBProp(db)) {
                        strRes = getString(R.string.mes_return_success_save);
                        GlobalProc.mToast(getActivity(), strRes);
                        _setResult(RESULT_OK);
                    } else {
                        strRes = getString(R.string.err_save_doc_numeration);
                        GlobalProc.mToast(getActivity(), strRes);
                    }
                } else {
                    int cnt = GlobalProc.getDocsCount(db,
                            OrderMetaData.TABLE_NAME);
                    cnt += GlobalProc.getDocsCount(db,
                            ReturnMetaData.TABLE_NAME);
                    cnt++;

                    returnData.setNumReturn((short) cnt);
                    if (returnData.insert(db)) {
                        DBProp.IncNumOrder();
                        DBProp.UpdateDBProp(db);

                        strRes = getString(R.string.mes_return_success_save);
                    } else {
                        strRes = getString(R.string.err_return_save);
                    }

                    GlobalProc.mToast(getActivity(), strRes);
                }
            } else {
                if (Returns.getReturn().update(db)) {
                    strRes = getString(R.string.mes_return_success_update);
                    GlobalProc.mToast(getActivity(), strRes);
                    _setResult(RESULT_OK);
                } else {
                    strRes = getString(R.string.err_return_update);
                    GlobalProc.mToast(getActivity(), strRes);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.e(TAG + ".saveReturn", e.toString());
        }
    }

    private void _setResult(int resultOk) {
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultOk, new Intent());
        getFragmentManager().popBackStack();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.rg_btCancel):
                getFragmentManager().popBackStack();
                break;
            case (R.id.rg_btOk):
                if (Returns.getReturn() != null
                        && Returns.getReturn().getItemsCount() > 0) {
                    if (Returns.getReturn().getmTypeAct() == 2) {
                        if (Returns.getReturn().getPhotoIsExist() == 1) {
                            dlgNotify().show();
                        } else {
                            Drawable drawable = ContextCompat.getDrawable(context, R.mipmap.photo_return);
                            if (drawable != null) {
                                drawable.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorRed), PorterDuff.Mode.SRC_IN));
                            }
                            photoReturn.setIcon(drawable);
                            Toast.makeText(context, getString(R.string.need_photo_report), Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        if (isMercury(Returns.getReturn())) {
//                            AlertDialog alertDialog = new AlertDialog.Builder(context)
//                                    .setIcon(android.R.drawable.ic_dialog_info)
//                                    .setTitle(R.string.confirmation)
//                                    .setMessage(getString(R.string.return_mercury))
//                                    .setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss())
//                                    .setPositiveButton("Удалить", (dialog, which) -> {
//                                        Returns.getReturn().removeMercuryItems();
//                                        notifyAdapter();
//                                    })
//                                    .create();
//                            if (!alertDialog.isShowing()) {
//                                alertDialog.show();
//                            }
//                        } else {
                        dlgNotify().show();
                        //}
                    }
                }
                break;
            case (R.id.rg_btAddGoods):
                dlgGoodsFilter().show();
                break;
        }
    }


    private boolean isMercury(Returns returns) {
        if (Returns.getReturn().getmTypeAct() == 0) {
            for (ReturnItem items : returns.getItems()) {
                if (items.getIsMercury() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Returns returnData = Returns.getReturn();
        switch (requestCode) {
            case IDD_NEW_PHOTO:
                if (resultCode == Activity.RESULT_OK) {

                    Drawable drawable = ContextCompat.getDrawable(context, R.mipmap.photo_return);
                    if (drawable != null) {
                        drawable.setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_IN));
                    }
                    photoReturn.setIcon(drawable);
                    List<String> imagesArray = getImagesInFolder(App.get(context).getPhotosPath());
                    if (imagesArray != null && imagesArray.size() > 0) {
                        String[] files = new String[imagesArray.size()];
                        imagesArray.toArray(files);
                        // Запускаем процесс отправки
                        progressBar.setProgress(0);
                        progressBar.setVisibility(View.VISIBLE);
                        Intent startUpload = new Intent(context, UploadFileIntentService.class);
                        startUpload.putExtra(UploadFileIntentService.EXTRA_FILES_LIST, files);
                        startUpload.putExtra("receiver", new DownloadReceiver(new Handler()));
                        context.startService(startUpload);
                    }
                    //Ставим картинку на кнопку
                }
                break;
            case IDD_MODIFY_ITEM:
                if (data == null || returnData == null)
                    return;
                if (resultCode == RESULT_OK) {
                    ReturnItem ri = data.getParcelableExtra(ReturnItem.KEY);

                    if (ri != null) {
                        boolean res = returnData.updateItem(ri);

                        String msg;
                        if (res) {
                            notifyAdapter();
                            msg = getString(R.string.mes_item_edit_success);
                        } else {
                            msg = "Ошибка при изменении элемента";
                        }
                        GlobalProc.mToast(getActivity(), msg);
                    }
                }
                break;
            case IDD_ADD:
                boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                if (tabletSize) {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment goodsListFragment = fragmentManager.findFragmentByTag(GoodsListFragment.TAG);
                    Fragment gMatrixFragment = fragmentManager.findFragmentByTag(GoodsGroupMatrixFragment.TAG);
                    if (goodsListFragment != null && goodsListFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                    if (gMatrixFragment != null && gMatrixFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                }
                if (data == null || returnData == null)
                    return;
                ArrayList<GoodsInDocument> newItems = data.getParcelableArrayListExtra(GoodsInDocument.KEY);
                if (newItems != null) {
                    MyApp app = (MyApp) getActivity().getApplicationContext();
                    for (GoodsInDocument item : newItems) {
                        ReturnItem newItem = returnData.find(item.getCode());
                        //Если товара нет в заявке - добавляем
                        if (newItem == null) {
                            Goods goods = Goods.load(app.getDB(), item.getCode());
                            ReturnItem ri = new ReturnItem(goods);
                            ri.setIsMercury(goods.getIsMercury());
                            if (item.getMeashPosition() == 0) {
                                ri.inPack(false);
                                float count = item.getCountPosition();
                                ri.setCount(count);
                                ri.setPrice(item.getPrice());
                                ri.setWeight(item.getWeight());
                            } else {
                                ri.inPack(true);
                                ri.setCountPack(item.getCountPosition());
                                ri.setWeight(item.getWeight());
                            }
                            returnData.addItem(ri);
                        } else {
                            if (item.getMeashPosition() == 0) {
                                newItem.inPack(false);
                                newItem.setCount(item.getCountPosition());
                                newItem.setWeight(item.getWeight());
                                //float count = getCount(newItem); //* item.getCountPosition();
                                //newItem.setCount(count);
                            } else {
                                newItem.inPack(true);
                                newItem.setCountPack(item.getCountPosition());
                                newItem.setWeight(item.getWeight());
                            }
                        }
                    }
                    notifyAdapter();
                }

            default:
                break;
        }
        fillBasement(returnData.getTotalAmount(), returnData.getItemsCount(), returnData.getTotalWeight());
    }


    private float getCount(ReturnItem returnItem) {
        Log.d(TAG, returnItem.mIsWeightGoods + ", " + returnItem.getWeight() + ", " + returnItem.inPack() + ", " + returnItem.getCountPack() + ", " + returnItem.getCount());
        return returnItem.mIsWeightGoods ? returnItem.getWeight() : returnItem.inPack() ? returnItem.getCountPack() : returnItem.getCount();
    }

    public List<String> getImagesInFolder(String photosPath) {
        File parentDir = new File(photosPath);
        if (!parentDir.exists() || !parentDir.isDirectory()) {
            throw new IllegalArgumentException("Parent directory cannot exist.");
        }
        List<String> imagesPath = new ArrayList<>();
        for (String path : parentDir.list()) {
            if (path.substring(path.lastIndexOf('.') + 1).equals("jpg")) {
                imagesPath.add(parentDir.getAbsolutePath() + "/" + path);
            }
        }
        return imagesPath;
    }


    @Override
    public void onTaskComplete(GetReturnItemPricesTask task) {
        boolean res;
        try {
            res = task.get();
        } catch (Exception e) {
            res = false;
        }
        if (res) {
            notifyAdapter();
            Returns data = Returns.getReturn();
            if (data != null) {
                //data.recalcTotals();
            }
        } else {
            GlobalProc.mToast(context, task.getMessage());
        }
        waitCursor(false);
    }


    private class DownloadReceiver extends ResultReceiver {

        DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            switch (resultCode) {
                case 1:
                    progressBar.setProgress(resultData.getInt("progress"));
                    break;
                case 2:
                    progressBar.setVisibility(View.INVISIBLE);
                    break;
            }

        }
    }
}
