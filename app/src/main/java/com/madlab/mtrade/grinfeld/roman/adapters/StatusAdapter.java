package com.madlab.mtrade.grinfeld.roman.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.Status;


public class StatusAdapter extends ArrayAdapter<Status> {

    private ArrayList<Status> items;
    String fromSite = "";
    Context context;


    public StatusAdapter(Context context, int textViewResourceId,
                         ArrayList<Status> items) {
        super(context, textViewResourceId, items);
        fromSite = context.getString(R.string.from_site);
        this.context = context;
        this.items = items;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        ViewHolder holder = null;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.status_item, null);

            holder = new ViewHolder();

            holder.image =  v.findViewById(R.id.si_image);

            holder.tvClient =  v.findViewById(R.id.si_tvClient);

            holder.tvLeft2Line =  v.findViewById(R.id.si2_tvLeft);
            holder.tvRight2Line =  v.findViewById(R.id.si2_tvRight);

            holder.tvLeft3Line =  v.findViewById(R.id.si3_tvLeft);
            holder.tvRight3Line =  v.findViewById(R.id.si3_tvRight);

            holder.thirdLine =  v.findViewById(R.id.si_thirdLine);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        final Status item = items.get(position);
        if (item != null) {
            if (holder.tvClient != null) {
                holder.tvClient.setText(item.getClientName());
            }

            if (holder.tvLeft2Line != null) {
                holder.tvLeft2Line.setText(item.getLeft2Line());
            }

            if (holder.tvRight2Line != null) {
                String right2Line = item.getRight2Line();
                if (!right2Line.contains("руб")){
                    final String phoneNum = item.getRight2Line();
                    holder.tvRight2Line.setText(phoneNum);
                    holder.tvRight2Line.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (phoneNum != null) {
                                String uri = "tel:" + normalizePhones(phoneNum);
                                context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                            }
                        }
                    });
                }else {
                    holder.tvRight2Line.setText(item.getRight2Line());
                }

            }

            if (item.getLeft3Line() == "" && item.getRight3Line() == "") {
                if (holder.thirdLine != null) {
                    holder.thirdLine.setVisibility(View.GONE);
                }
            } else {
                if (holder.tvLeft3Line != null) {
                    holder.tvLeft3Line.setText(item.getLeft3Line());
                }

                if (holder.tvRight3Line != null) {
                    holder.tvRight3Line.setText(item.getRight3Line());
                    if (item.getRight3Line().contains(fromSite)) {
                        holder.image.setVisibility(View.VISIBLE);
                    } else {
                        holder.image.setVisibility(View.GONE);
                    }
                }
            }
            if (item.isOutOfStock()){
                holder.tvClient.setTextColor(Color.RED);
            }else {
                holder.tvClient.setTextColor(Color.WHITE);
            }
        }
        return v;
    }


    /**
     * Нормализует строку, которая может быть номером телефона
     *
     * @param phones
     *            исходная строка
     * @return нормализованная строка
     */
    public String normalizePhones(String phones) {
        String res = phones.replaceAll("[^0-9,]", "");
        return res;
    }

    private static class ViewHolder {
        public ImageView image;

        public TextView tvClient;
        public TextView tvLeft2Line;
        public TextView tvRight2Line;
        public TextView tvLeft3Line;
        public TextView tvRight3Line;

        public LinearLayout thirdLine;
    }
}
