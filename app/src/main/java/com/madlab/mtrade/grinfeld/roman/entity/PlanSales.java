package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRa on 16.04.2018.
 */

public class PlanSales {

    private int skuPlan;
    private int skuFact;
    private int percentageCompletion;
    private String groupGoodsName;

    public PlanSales(int skuPlan, int skuFact, int percentageCompletion, String groupGoodsName) {
        this.skuPlan = skuPlan;
        this.skuFact = skuFact;
        this.percentageCompletion = percentageCompletion;
        this.groupGoodsName = groupGoodsName;
    }


    public int getSkuPlan() {
        return skuPlan;
    }

    public int getSkuFact() {
        return skuFact;
    }

    public int getPercentageCompletion() {
        return percentageCompletion;
    }

    public String getGroupGoodsName() {
        return groupGoodsName;
    }
}
