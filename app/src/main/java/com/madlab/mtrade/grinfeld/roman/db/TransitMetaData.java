package com.madlab.mtrade.grinfeld.roman.db;

public class TransitMetaData {
    private TransitMetaData() {	}

    public static final String TABLE_NAME = "Transit";

    public static final DBField FIELD_CODE_GOODS = new DBField("CodeGoods", 0);
    public static final DBField FIELD_DATE = new DBField("DateShip", 1);
    public static final DBField FIELD_COUNT = new DBField("NumItem", 2);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s (" + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	datetime		NOT NULL DEFAULT 0,"
                    + "%s	money			NOT NULL DEFAULT 0)", TABLE_NAME,
            FIELD_CODE_GOODS.Name, FIELD_DATE.Name, FIELD_COUNT.Name);

    public static String insertQuery(String code, String date, String amount) {
        return String.format("INSERT INTO %s (" + "%s, %s, %s) VALUES ("
                        + "'%s', '%s 23:59:59', %s)", TABLE_NAME,
                FIELD_CODE_GOODS.Name, FIELD_DATE.Name, FIELD_COUNT.Name, code,
                date, amount);
    }
}
