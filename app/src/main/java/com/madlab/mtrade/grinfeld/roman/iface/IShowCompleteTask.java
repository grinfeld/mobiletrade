package com.madlab.mtrade.grinfeld.roman.iface;

/**
 * Created by GrinfeldRA on 03.05.2018.
 */

public interface IShowCompleteTask {
    void showCompleteTask(boolean b);
}
