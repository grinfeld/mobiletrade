package com.madlab.mtrade.grinfeld.roman.entity;

import android.location.Location;

import com.madlab.mtrade.grinfeld.roman.db.GeolocationMetaData;


public class Geolocation extends GeolocationMetaData {

    private String provider;
    private long time = 0;
    private double latitude = 0;
    private double longitude = 0;
    private float speed = 0;
    private float accuracy = 0;
    private boolean send = false;

    public Geolocation(){
        this.time = System.currentTimeMillis();
    }

    public Geolocation(String provider){
        this();
        this.provider = provider;
    }

    public Geolocation(Location location){
        this.provider = location.getProvider();
        this.time = location.getTime();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        if (location.hasSpeed()) this.speed = location.getSpeed();
        if (location.hasAccuracy()) this.accuracy = location.getAccuracy();
    }

    public String getProvider(){
        return provider;
    }

    public void setProvider(String provider){
        this.provider = provider;
    }

    public long getTime(){
        return time;
    }

    public void setTime(long time){
        this.time = time;
    }

    public double getLatitude(){
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public void setLongitude(double longitude){
        this.longitude = longitude;
    }

    public float getSpeed(){
        return speed;
    }

    public void setSpeed(float speed){
        this.speed = speed;
    }

    public float getAccuracy(){
        return accuracy;
    }

    public void setAccuracy(float accuracy){
        this.accuracy = accuracy;
    }

    public boolean isSend(){
        return send;
    }

    public void setSend(boolean send){
        this.send = send;
    }


}