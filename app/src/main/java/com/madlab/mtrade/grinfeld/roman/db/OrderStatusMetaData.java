package com.madlab.mtrade.grinfeld.roman.db;


public class OrderStatusMetaData {
	private OrderStatusMetaData() {}

	public static final String TABLE_NAME = "OrderStatus";

	public static final String FIELD_CLIENT = "Client";
	public static final String FIELD_EXPEDITOR = "Expeditor";
	public static final String FIELD_PHONE = "Phone";
	public static final String FIELD_STATUS = "Status";
	public static final String FIELD_ORDER_TYPE = "OrderType";

	public static final byte FIELD_CLIENT_NUM = 0;
	public static final byte FIELD_EXPEDITOR_NUM = 1;
	public static final byte FIELD_PHONE_NUM = 2;
	public static final byte FIELD_STATUS_NUM = 3;
	public static final byte FIELD_ORDER_TYPE_NUM = 4;

	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s ("
					+ "%s	nvarchar(50)    NOT NULL DEFAULT '',"
					+ "%s	nvarchar(30)	NOT NULL DEFAULT '',"
					+ "%s	nvarchar(23)	NOT NULL DEFAULT '',"
					+ "%s	nvarchar(30)	NOT NULL DEFAULT '',"
					+ "%s	nvarchar(12)	NOT NULL DEFAULT '')", TABLE_NAME,
			FIELD_CLIENT, FIELD_EXPEDITOR, FIELD_PHONE, FIELD_STATUS,
			FIELD_ORDER_TYPE);

	public static final String INDEX_STATUS = String.format(
			"CREATE INDEX idxOrderStatusStatus ON %s ( %s )", TABLE_NAME,
			FIELD_STATUS);

	public static final String INDEX_TYPE = String.format(
			"CREATE INDEX idxOrderStatusType ON %s ( %s )", TABLE_NAME,
			FIELD_ORDER_TYPE);

}
