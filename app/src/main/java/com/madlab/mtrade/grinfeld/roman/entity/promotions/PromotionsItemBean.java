package com.madlab.mtrade.grinfeld.roman.entity.promotions;

/**
 * Created by grinfeldra
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromotionsItemBean {
    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Activity {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Exception extends Throwable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("client")
        @Expose
        private Client client;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Client getClient() {
            return client;
        }

        public void setClient(Client client) {
            this.client = client;
        }

    }

    public class Client {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("code")
        @Expose
        private String code;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }

    public class Bonuse {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("counts")
        @Expose
        private Integer counts;
        @SerializedName("quantum")
        @Expose
        private String quantum;
        @SerializedName("product")
        @Expose
        private Product product;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCounts() {
            return counts;
        }

        public void setCounts(Integer counts) {
            this.counts = counts;
        }

        public String getQuantum() {
            return quantum;
        }

        public void setQuantum(String quantum) {
            this.quantum = quantum;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

    }


    public class City {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("initial")
        @Expose
        private String initial;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getInitial() {
            return initial;
        }

        public void setInitial(String initial) {
            this.initial = initial;
        }

    }


    public class CloseAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezoneType() {
            return timezoneType;
        }

        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }


    public class Cover {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("path_url")
        @Expose
        private String pathUrl;
        @SerializedName("domain")
        @Expose
        private String domain;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPathUrl() {
            return pathUrl;
        }

        public void setPathUrl(String pathUrl) {
            this.pathUrl = pathUrl;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

    }


    public class Cover_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("path_url")
        @Expose
        private String pathUrl;
        @SerializedName("domain")
        @Expose
        private String domain;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPathUrl() {
            return pathUrl;
        }

        public void setPathUrl(String pathUrl) {
            this.pathUrl = pathUrl;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

    }


    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("cover")
        @Expose
        private String cover;
        @SerializedName("photo")
        @Expose
        private Photo photo;
        @SerializedName("quantum")
        @Expose
        private String quantum;
        @SerializedName("start_at")
        @Expose
        private StartAt startAt;
        @SerializedName("close_at")
        @Expose
        private CloseAt closeAt;
        @SerializedName("counts")
        @Expose
        private Integer counts;
        @SerializedName("is_stm")
        @Expose
        private Boolean isStm;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("only_required")
        @Expose
        private Boolean onlyRequired;
        @SerializedName("city")
        @Expose
        private City city;
        @SerializedName("bonuses")
        @Expose
        private List<Bonuse> bonuses = null;
        @SerializedName("products")
        @Expose
        private List<Product_> products = null;
        @SerializedName("required")
        @Expose
        private List<Object> required = null;
        @SerializedName("activities")
        @Expose
        private List<Activity> activities = null;
        @SerializedName("exceptions")
        @Expose
        private List<Exception> exceptions = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public Photo getPhoto() {
            return photo;
        }

        public void setPhoto(Photo photo) {
            this.photo = photo;
        }

        public String getQuantum() {
            return quantum;
        }

        public void setQuantum(String quantum) {
            this.quantum = quantum;
        }

        public StartAt getStartAt() {
            return startAt;
        }

        public void setStartAt(StartAt startAt) {
            this.startAt = startAt;
        }

        public CloseAt getCloseAt() {
            return closeAt;
        }

        public void setCloseAt(CloseAt closeAt) {
            this.closeAt = closeAt;
        }

        public Integer getCounts() {
            return counts;
        }

        public void setCounts(Integer counts) {
            this.counts = counts;
        }

        public Boolean getIsStm() {
            return isStm;
        }

        public void setIsStm(Boolean isStm) {
            this.isStm = isStm;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getOnlyRequired() {
            return onlyRequired;
        }

        public void setOnlyRequired(Boolean onlyRequired) {
            this.onlyRequired = onlyRequired;
        }

        public City getCity() {
            return city;
        }

        public void setCity(City city) {
            this.city = city;
        }

        public List<Bonuse> getBonuses() {
            return bonuses;
        }

        public void setBonuses(List<Bonuse> bonuses) {
            this.bonuses = bonuses;
        }

        public List<Product_> getProducts() {
            return products;
        }

        public void setProducts(List<Product_> products) {
            this.products = products;
        }

        public List<Object> getRequired() {
            return required;
        }

        public void setRequired(List<Object> required) {
            this.required = required;
        }

        public List<Activity> getActivities() {
            return activities;
        }

        public void setActivities(List<Activity> activities) {
            this.activities = activities;
        }

        public List<Exception> getExceptions() {
            return exceptions;
        }

        public void setExceptions(List<Exception> exceptions) {
            this.exceptions = exceptions;
        }

    }


    public class Photo {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("path_url")
        @Expose
        private String pathUrl;
        @SerializedName("domain")
        @Expose
        private String domain;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPathUrl() {
            return pathUrl;
        }

        public void setPathUrl(String pathUrl) {
            this.pathUrl = pathUrl;
        }

        public String getDomain() {
            return domain;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

    }


    public class Product {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("origin_id")
        @Expose
        private String originId;
        @SerializedName("cover")
        @Expose
        private Cover cover;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOriginId() {
            return originId;
        }

        public void setOriginId(String originId) {
            this.originId = originId;
        }

        public Cover getCover() {
            return cover;
        }

        public void setCover(Cover cover) {
            this.cover = cover;
        }

    }

    public class Product_ {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("origin_id")
        @Expose
        private String originId;
        @SerializedName("cover")
        @Expose
        private Cover_ cover;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getOriginId() {
            return originId;
        }

        public void setOriginId(String originId) {
            this.originId = originId;
        }

        public Cover_ getCover() {
            return cover;
        }

        public void setCover(Cover_ cover) {
            this.cover = cover;
        }

    }


    public class StartAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezoneType() {
            return timezoneType;
        }

        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }
}
