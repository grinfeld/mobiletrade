package com.madlab.mtrade.grinfeld.roman.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.components.ListViewLazy;
import com.madlab.mtrade.grinfeld.roman.db.PromotionCheckedMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Remainder;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.Bonuse;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionItem;
import com.madlab.mtrade.grinfeld.roman.eventbus.GoodsListEventFilter;
import com.madlab.mtrade.grinfeld.roman.eventbus.GoodsListEventZoom;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnClickItem;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnLongClickItem;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsGroupTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsOnTreeClickTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGroupMatrixTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import uk.co.deanwild.flowtextview.FlowTextView;
import uk.co.deanwild.flowtextview.listeners.OnLinkClickListener;

import static android.content.Context.MODE_PRIVATE;


/**
 * Класс, описывающий фрагмент со списком товаров
 *
 * @author Konovaloff
 */
public class GoodsListFragment extends ListFragment implements
        ILoadGoodsComplete, ListViewLazy.ListViewListener {
    public final static String TAG = "!->GoodsListFragment";

    public final static String EXTRA_TYPE = "LoadGoodsType";
    public final static String EXTRA_MODE = "ShowMode";
    public final static String EXTRA_GOODS_LIST = "GoodsListExtra";
    public final static String EXTRA_SELECTED_NODE = "SelectedNode";
    public final static String EXTRA_POMOTIONS = "PromotionsItem";
    public final static String EXTRA_CLIENT = "SelectedClient";
    public final static String EXTRA_TYPE_LOAD = "typeLoad";

    public final static byte RADIO_ALL = (byte) 0;
    public final static byte RADIO_WITH_REST = (byte) 1;
    public final static byte RADIO_STM = (byte) 2;

    public final static String PREF_SEARCH_QUERY = "searchQuery";

    public static byte modeRadio = RADIO_WITH_REST;
    //private ICallbacks mCallbacks;
    public final static byte IDD_ADD = 12;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    private String query = null;
    private String errorLoad;
    private FlowTextView ftv;
    private ImageView imgPromotion, iv_more;

    int limit = 20;
    int offset = 0;

    /**
     * Режим загрузки товаров: Все, Матрица, Неликвид
     */
    private byte mTypeLoad;


    /**
     * Режим работы диалога: Справочник, Возврат, Заявка
     */
    private byte mListViewMode;

    /**
     * Выбранная корневая группа. Используется только в случае однопанельного
     * интерфейса
     */
    private Node mSelectedNode;

    /**
     * Код клиента (нужен для загрузки матрицы)
     */
    private String mClientCode;


    private boolean isForeignClient = false;

    /**
     * Уровень цен
     */
    private byte mPriceLevel = 1;

    /**
     * Общая скидка
     */
    private byte mDiscount = 0;

    /**
     * Категория маст-листа
     */
    private String mMLCategory;

    /**
     * Список видимых пользователю товаров
     */
    private static GoodsList mGoodsList;

    private static ArrayList<GoodsInDocument> mExistedItems;

    /**
     * Адаптер для товаров
     */
    private GoodsAdapter mGoodsAdapter;

    private GoodsAdapter mGoodsAdapterLazy;

    private int currentSelectedItemIndex = 0;

    private boolean mSearch = false;

    private ViewGroup viewGroup;

    private Activity context;

    private ArrayList<Goods> listLazyGoods = new ArrayList<>();

    private byte loadGoodsTask_MODE;


    public void setExistedItems(ArrayList<GoodsInDocument> existedList) {
        mExistedItems = existedList;
    }


    /**
     * mode = Справочник, Заявка или Возврат typeLoad = режим загрузки: Все,
     * матрица, неликвид node = выбранный узел
     */
    public static GoodsListFragment newInstance(byte mode, byte loadGoodsTask_MODE, Node node,
                                                byte typeLoad, Client selected,
                                                ArrayList<GoodsInDocument> existedItems, String key) {
        Bundle args = new Bundle();
        args.putByte(EXTRA_TYPE, typeLoad);
        args.putByte(EXTRA_GOODS_LIST, loadGoodsTask_MODE);
        args.putByte(EXTRA_MODE, mode);
        args.putParcelable(EXTRA_SELECTED_NODE, node);
        args.putParcelable(EXTRA_CLIENT, selected);
        args.putParcelableArrayList(GoodsInDocument.KEY, existedItems);
        args.putString(MainActivity.PRICE, key);
        GoodsListFragment fragment = new GoodsListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public static GoodsListFragment newInstance(byte mode, byte loadGoodsTask_MODE, int id_promotion,
                                                byte typeLoad, Client selected,
                                                ArrayList<GoodsInDocument> existedItems, String key) {
        Bundle args = new Bundle();
        args.putByte(EXTRA_TYPE, typeLoad);
        args.putByte(EXTRA_GOODS_LIST, loadGoodsTask_MODE);
        args.putByte(EXTRA_MODE, mode);
        args.putInt(EXTRA_POMOTIONS, id_promotion);
        args.putParcelable(EXTRA_CLIENT, selected);
        args.putParcelableArrayList(GoodsInDocument.KEY, existedItems);
        args.putString(MainActivity.PRICE, key);
        GoodsListFragment fragment = new GoodsListFragment();
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onSaveInstanceState(final Bundle outState) {
        setTargetFragment(getTargetFragment(), getTargetRequestCode());
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    ActionBar actionBar;
    CharSequence subtitle;
    SharedPreferences mPrefs;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        context = getActivity();
        actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        errorLoad = getString(R.string.err_load_metadata);
        if (getArguments() != null) {
            mTypeLoad = getArguments().getByte(EXTRA_TYPE);
            mPrefs = context.getPreferences(MODE_PRIVATE);
            mListViewMode = getArguments().getByte(EXTRA_MODE);
            SharedPreferences.Editor editor = mPrefs.edit();
            loadGoodsTask_MODE = getArguments().getByte(EXTRA_GOODS_LIST);
            editor.putInt(EXTRA_TYPE_LOAD, loadGoodsTask_MODE);
            editor.apply();
            mSelectedNode = getArguments().getParcelable(EXTRA_SELECTED_NODE);
            Client selected = getArguments().getParcelable(EXTRA_CLIENT);
            mExistedItems = getArguments().getParcelableArrayList(GoodsInDocument.KEY);
            if (selected != null) {
                mClientCode = selected.getCode();
                mPriceLevel = selected.mPriceType;
                mMLCategory = selected.Category();
                mDiscount = selected.mDiscount;
                isForeignClient = selected.isForeignAgent();
            }
        }
        // Запускаем загрузку списка товаров, если у нас есть последний
        // выбранный узел
        // Он у нас будет только с однопанельным интерфейсом
        if (getPrefsTypeLoad() != GoodsList.MODE_LIST && mSelectedNode != null) {
            startLoadGoodsList(mSelectedNode, mClientCode,
                    mMLCategory, modeRadio, mTypeLoad, menuItemObjects);
        }
    }

    private byte getPrefsTypeLoad() {
        return (byte) mPrefs.getInt(EXTRA_TYPE_LOAD, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewGroup = container;
        View view = inflater.inflate(R.layout.goods_list, container, false);
        ftv = view.findViewById(R.id.ftv);
        iv_more = view.findViewById(R.id.iv_more);
        imgPromotion = view.findViewById(R.id.img_promotion);
        return view;
    }


    View footer;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ftv.setVisibility(View.GONE);
        if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
            ((ListViewLazy) getListView()).setListViewListener(this);
            initData();
            mGoodsAdapterLazy = new GoodsAdapter(getActivity(), listLazyGoods, mExistedItems, mListViewMode, getArguments().getString(MainActivity.PRICE), isForeignClient);
            footer = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);
            getListView().addFooterView(footer);
            getListView().setAdapter(mGoodsAdapterLazy);
        } else if (getPrefsTypeLoad() == GoodsList.MODE_PROMOTIONS) {
            if (viewGroup != null) {
                if (viewGroup.findViewById(R.id.detailFragmentContainer) == null) {
                    int promotionsItemId = getArguments().getInt(EXTRA_POMOTIONS);
                    loadPromotionItem(promotionsItemId);
                }
            }
        }
        super.onActivityCreated(savedInstanceState);
    }

    boolean b = false;

    private void loadPromotionItem(int idPromotion) {
        imgPromotion.setVisibility(View.GONE);
        ftv.setVisibility(View.VISIBLE);
        iv_more.setVisibility(View.VISIBLE);
        ViewGroup.LayoutParams params = ftv.getLayoutParams();
        if (getArguments().getString(MainActivity.PRICE) == null) {
            params.height = (int) getResources().getDimension(R.dimen.min_height);
        } else {
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            b = true;
            iv_more.setVisibility(View.GONE);
        }
        ftv.setLayoutParams(params);
        PromotionItem promotionItem = PromotionItem.load(idPromotion);
        if (promotionItem != null) {
            if (promotionItem.getCover() != null) {
                if (!promotionItem.getCover().isEmpty()) {
                    imgPromotion.setVisibility(View.VISIBLE);
                    Glide.with(context).load(promotionItem.getCover()).into(imgPromotion);
                    imgPromotion.setOnClickListener(view1 -> {
                        final Dialog builder = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                        builder.setContentView(R.layout.dialog_cam_preview);
                        ImageButton button_close = builder.findViewById(R.id.button_close);
                        ImageView img_full_screen = builder.findViewById(R.id.img_full_screen);
                        String domain = promotionItem.getCover().substring(0, 21);
                        String data = promotionItem.getCover().substring(25);
                        Glide.with(context).load(domain + data).into(img_full_screen);
                        button_close.setOnClickListener(view -> builder.dismiss());
                        builder.show();
                    });
                }
            }
            String startDate = promotionItem.getStartDate();
            String endDate = promotionItem.getEndDate();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            try {
                Date start = simpleDateFormat.parse(startDate);
                Date end = simpleDateFormat.parse(endDate);
                Calendar c = Calendar.getInstance();
                c.setTime(start);
                c.add(Calendar.YEAR, 1);
                SimpleDateFormat format;
                if (c.getTime().after(end)) {
                    format = new SimpleDateFormat("dd MMMM", Locale.getDefault());
                } else {
                    format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
                }
                startDate = format.format(start);
                endDate = format.format(end);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Spanned html = Html.fromHtml("Акция действует с " + startDate +
                    " по " + endDate + "<br/>" + promotionItem.getText());
            ftv.setText(html);
            ftv.setTextColor(getResources().getColor(R.color.colorWhite));
            float spTextSize = 16;
            float textSize = spTextSize * getResources().getDisplayMetrics().scaledDensity;
            ftv.setTextSize(textSize);
            ftv.invalidate();
            ftv.setOnClickListener(view -> {
                if (!b) {
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    ftv.setLayoutParams(params);
                    iv_more.setVisibility(View.GONE);
                    b = true;
                } else {
                    params.height = (int) getResources().getDimension(R.dimen.min_height);
                    ftv.setLayoutParams(params);
                    iv_more.setVisibility(View.VISIBLE);
                    b = false;
                }
            });
            MyApp app = (MyApp) context.getApplication();
            SQLiteDatabase db = app.getDB();
            GoodsList result = new GoodsList();
            mGoodsAdapter = new GoodsAdapter(getActivity(), result, mExistedItems, mListViewMode, getArguments().getString(MainActivity.PRICE), isForeignClient);
            for (String originCodeGoods : promotionItem.getOriginCodeGoods()) {
                String where = String.format("%s = '%s'",
                        GoodsMetaData.FIELD_CODE.Name, originCodeGoods);
                GoodsList goods = GoodsList.loadList(db, where, null, null);
                for (Goods good : goods) {
                    good.calcVisiblePrice(mPriceLevel, mDiscount);
                    good.setType(Goods.Type.PROMOTION.getValue());
                    if (promotionItem.getQuantum().equals("шт")) {
                        good.setQuant(promotionItem.getAmount_for_bonus());
                    }
                    if (mExistedItems != null) {
                        for (GoodsInDocument item : mExistedItems) {
                            if (item.getCode().equalsIgnoreCase(good.getCode())) {
                                good.inDocument(true);
                                double countPosition = item.getCountPosition();
                                int count = (int) Math.ceil((countPosition * item.getQuant()) / promotionItem.getAmount_for_bonus());
                                item.setCountPosition(count);
                                item.setQuant(promotionItem.getAmount_for_bonus());
                                break;
                            }
                        }
                    }
                    mGoodsAdapter.addItem(good);
                }
                //result.addAll(goods);
            }


            mGoodsAdapter.addSectionHeaderItem();

            for (Bonuse bonuse : promotionItem.getBonuses()) {
                String where = String.format("%s = '%s'",
                        GoodsMetaData.FIELD_CODE.Name, bonuse.getOrigin_id());
                GoodsList goods = GoodsList.loadList(db, where, null, null);
                for (Goods good : goods) {
                    good.calcVisiblePrice(mPriceLevel, mDiscount);
                    good.setType(Goods.Type.BONUS.getValue());
                    mGoodsAdapter.addItem(good);
                }
                //result.addAll(goods);
            }

            getListView().setAdapter(mGoodsAdapter);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu_goods_info, menu);
        MenuItem.OnMenuItemClickListener listener = item -> {
            onContextItemSelected(item);
            return true;
        };
        for (int i = 0, n = menu.size(); i < n; i++) {
            menu.getItem(i).setOnMenuItemClickListener(listener);
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menuGoods_info) {
            AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            if (mGoodsAdapter != null) {
                Goods goods = mGoodsAdapter.getItem(menuInfo.position);
                if (goods != null) {
                    if (searchItem != null && searchItem.isActionViewExpanded()) {
                        onRestoreStateLazyList();
                    }
                    getFragmentManager().beginTransaction()
                            .replace(R.id.contentMain, GoodsInfoContainer.newInstance(goods, isForeignClient))
                            .addToBackStack(null)
                            .commit();
                }
            }
        }
        return super.onContextItemSelected(item);
    }

    MenuItem filterRest;
    MenuItem filterEverything;
    static MenuItem searchItem;
    MenuItem menuItem;
    public static final int IDM_GET_SCLAD = 404;
    private static final int IDM_ZOOM = 405;
    List<MenuItemObject> menuItemObjects = new ArrayList<>();
    final int[] index = new int[1];
    final int[] top = new int[1];

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (App.isMerch()) {
            return;
        }
        menu.clear();

        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);

        if (getPrefsTypeLoad() != GoodsList.MODE_PROMOTIONS) {
            inflater.inflate(R.menu.goods_menu, menu);
            if (actionBar != null) {
                subtitle = actionBar.getSubtitle();
            }
            if (viewGroup != null) {
                if (viewGroup.findViewById(R.id.detailFragmentContainer) != null) {
                    menu.add(Menu.NONE, IDM_ZOOM, Menu.NONE, "Увеличить")
                            .setIcon(R.drawable.zoom).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }
            SubMenu subMenu = menu.findItem(R.id.action_sclad).getSubMenu();
            Set<String> set = Remainder.loadScladName(getActivity());
            if (set == null || set.size() == 0) {
                menu.removeItem(R.id.action_sclad);
            } else {
                int menuId = 1;
                menuItemObjects = new ArrayList<>();
                for (String nameSclad : set) {
                    if (!nameSclad.equals("")) {
                        menuItemObjects.add(new MenuItemObject(nameSclad, menuId, true));
                        subMenu.add(IDM_GET_SCLAD, menuId, Menu.NONE, nameSclad);
                        subMenu.setGroupCheckable(IDM_GET_SCLAD, true, false);
                        menuId++;
                    }
                }
                for (MenuItemObject itemObject : menuItemObjects) {
                    menu.findItem(itemObject.getId()).setChecked(true);
                }
            }
            searchItem = menu.findItem(R.id.action_search);
            menuItem = menu.findItem(R.id.action_menu);

            filterEverything = menu.findItem(R.id.filter_everything);
            filterRest = menu.findItem(R.id.filter_rest);
            if (modeRadio == RADIO_ALL) {
                filterEverything.setChecked(true);
            } else if (modeRadio == RADIO_WITH_REST) {
                filterRest.setChecked(true);
            }
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) searchItem.getActionView();
            if (searchView != null) {
                if (searchManager != null) {
                    searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                }
                queryTextListener = new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        return true;
                    }

                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        query = s;
                        startSearchGoods(modeRadio, mMLCategory, query, null);
                        return true;
                    }
                };
                searchView.setOnQueryTextListener(queryTextListener);
            }


            searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
                        index[0] = getListView().getFirstVisiblePosition();
                        View v = getListView().getChildAt(0);
                        top[0] = (v == null) ? 0 : (v.getTop() - getListView().getPaddingTop());
                        getListView().removeFooterView(footer);
                    }
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    query = null;
                    if (getPrefsTypeLoad() != GoodsList.MODE_LIST && mSelectedNode != null) {
                        startLoadGoodsList(mSelectedNode, mClientCode,
                                mMLCategory, modeRadio, mTypeLoad, menuItemObjects);
                    }
                    onRestoreStateLazyList();
                    return true;
                }
            });

        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void onRestoreStateLazyList() {
        if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
            getListView().addFooterView(footer);
            getListView().setAdapter(mGoodsAdapterLazy);
            getListView().setSelectionFromTop(index[0], top[0]);
        }
    }

    private void setSubtitle() {
        boolean flag = true;
        if (menuItemObjects != null)
            for (MenuItemObject itemObject : menuItemObjects) {
                if (!itemObject.isChecked()) {
                    flag = false;
                }
            }

        if (actionBar != null) {
            if (flag) {
                actionBar.setSubtitle(subtitle + "");
            } else {
                actionBar.setSubtitle(subtitle + " [Фильтр по складам]");
            }
        }
    }

    void resetIndex() {
        limit = 20;
        offset = 0;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, String.valueOf("onOptionsItemSelected: " + item.getItemId() + " " + mGoodsAdapterLazy));
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.filter_everything:
                item.setChecked(true);
                modeRadio = RADIO_ALL;
                if (getPrefsTypeLoad() == GoodsList.MODE_LIST && query == null) {
                    resetIndex();
                    listLazyGoods.clear();
                    EventBus.getDefault().post(new GoodsListEventFilter(RADIO_ALL));
                    initData();
                } else {
                    EventBus.getDefault().post(new GoodsListEventFilter(RADIO_ALL));
                    startSearchGoods(RADIO_ALL, mMLCategory, query, menuItemObjects);
                }
                break;
            case R.id.filter_rest:
                item.setChecked(true);
                modeRadio = RADIO_WITH_REST;
                if (getPrefsTypeLoad() == GoodsList.MODE_LIST && query == null) {
                    resetIndex();
                    listLazyGoods.clear();
                    EventBus.getDefault().post(new GoodsListEventFilter(RADIO_WITH_REST));
                    initData();
                } else {
                    EventBus.getDefault().post(new GoodsListEventFilter(RADIO_WITH_REST));
                    startSearchGoods(RADIO_WITH_REST, mMLCategory, query, menuItemObjects);
                }
                break;
            case IDM_ZOOM:
                EventBus.getDefault().post(new GoodsListEventZoom());
                break;
            default:
                try {
                    MenuItemObject itemObject = menuItemObjects.get(itemId - 1);
                    if (!itemObject.isChecked()) {
                        if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
                            resetIndex();
                            listLazyGoods.clear();
                            item.setChecked(true);
                            itemObject.setChecked(true);
                            initData();
                            setSubtitle();
                        } else {
                            item.setChecked(true);
                            itemObject.setChecked(true);
                            startSearchGoods(modeRadio, mMLCategory, null, menuItemObjects);
                            setSubtitle();
                        }
                    } else {
                        if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
                            resetIndex();
                            listLazyGoods.clear();
                            item.setChecked(false);
                            itemObject.setChecked(false);
                            initData();
                            setSubtitle();
                        } else {
                            item.setChecked(false);
                            itemObject.setChecked(false);
                            startSearchGoods(modeRadio, mMLCategory, null, menuItemObjects);
                            setSubtitle();
                        }
                    }
                } catch (IndexOutOfBoundsException e) {

                }
                break;
        }
//        for (MenuItemObject itemObject : menuItemObjects) {
//            int itemId = itemObject.getId();
//            if (item.getItemId() == itemId) {
//                if (!item.isChecked()) {
//                    if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
//                        resetIndex();
//                        listLazyGoods.clear();
//                        item.setChecked(true);
//                        itemObject.setChecked(true);
//                        //initData(menuItemObjects);
//                        setSubtitle();
//                    } else {
//                        item.setChecked(true);
//                        itemObject.setChecked(true);
//                        startSearchGoods(modeRadio, mMLCategory, null, menuItemObjects);
//                        setSubtitle();
//                    }
//                } else {
//                    if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
//                        resetIndex();
//                        listLazyGoods.clear();
//                        item.setChecked(false);
//                        itemObject.setChecked(false);
//                        //initData(menuItemObjects);
//                        setSubtitle();
//                    } else {
//                        item.setChecked(false);
//                        itemObject.setChecked(false);
//                        startSearchGoods(modeRadio, mMLCategory, null, menuItemObjects);
//                        setSubtitle();
//                    }
//                }
//            }
//        }
//        initData(menuItemObjects);
//        return true;
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        registerForContextMenu(getListView());
    }

    public void updateItems(Node selectedNode, String codeCli, String mlCat,
                            byte radioMode, byte loadGoodsOnTreeClickTask_mode) {
        mSelectedNode = selectedNode;
        if (mSelectedNode != null)
            startLoadGoodsList(mSelectedNode, codeCli, mlCat,
                    radioMode, loadGoodsOnTreeClickTask_mode, menuItemObjects);
    }

    public void updateItems(int id_promotion) {
        loadPromotionItem(id_promotion);
    }

    /**
     * Загружаем список товаров с ненулевым остатком в выбранной группе
     * radioMode - значение переключателя 0 = Все, 1 = с остатком, 2 = СТМ
     */
    private void startLoadGoodsList(Node parent, String codeCli,
                                    String mlCat, byte radioMode, byte loadGoodsOnTreeClickTask_mode, List<MenuItemObject> menuItemObjects) {
        if (getActivity() != null) {
            //Заменяем скидкой группы скидку клиента
            byte currentDiscount = parent.discount() > 0 ? parent.discount() : mDiscount;
            LoadGoodsOnTreeClickTask task = new LoadGoodsOnTreeClickTask(
                    getActivity(),
                    mExistedItems, this, loadGoodsOnTreeClickTask_mode,
                    radioMode, mPriceLevel, currentDiscount, menuItemObjects);
            task.setClientCode(codeCli);
            task.setMLCategory(mlCat);

            //waitCursor(true);

            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, parent.getCode());
        }
    }

    public void startSearchGoods(byte currentFilter, String mlCat, String query, List<MenuItemObject> menuItemObjects) {
        mSearch = true;
        new SearchItemsTask(currentFilter, mlCat, query, menuItemObjects).execute();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnLongClickItem(OnLongClickItem item) {
        getListView().showContextMenuForChild(item.getView());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onListItemClick(OnClickItem item) {
        currentSelectedItemIndex = item.getPosition();
        Goods g = item.getGoods();
        if (g.getType() != Goods.Type.BONUS.getValue()
                && getTargetRequestCode() != VisitContainFragment.IDD_NEW_MONITORING
                && getTargetRequestCode() != VisitContainFragment.IDD_EDIT_MONITORING) {
            EventBus.getDefault().post(g);
            if (searchItem != null && searchItem.isActionViewExpanded()) {
                onRestoreStateLazyList();
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        currentSelectedItemIndex = position;
        Goods g = (Goods) getListView().getAdapter().getItem(position);
        if (g.getType() != Goods.Type.BONUS.getValue() && !App.isMerch()) {
            EventBus.getDefault().post(g);
            if (searchItem != null && searchItem.isActionViewExpanded()) {
                onRestoreStateLazyList();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        if (getListView() != null) {
            registerForContextMenu(getListView());
        }
        super.onResume();
    }

    private void notifyAdapter() {
        if (mGoodsAdapter != null)
            mGoodsAdapter.notifyDataSetChanged();
    }

    public void updateAdapterAfterAddItemsInOrder() {
        if (mGoodsAdapter != null) {
            mGoodsAdapter.setExistedItems(mExistedItems);
            mGoodsAdapter.notifyAfterExistedListChanged();
        }
        if (mGoodsAdapterLazy != null) {
            mGoodsAdapterLazy.setExistedItems(mExistedItems);
            mGoodsAdapterLazy.notifyAfterExistedListChanged();
        }
    }

    public void updateAdapterAfterChange() {
        if (mGoodsAdapter != null) {
            mGoodsAdapter.setExistedItems(mExistedItems);
            mGoodsAdapter.notifyAfterExistedListChanged();
        }
    }


    /**
     * Устанавливает адаптер для списка элементов
     *
     * @param destination Назначение товара. Заявка или возврат
     */
    private void setListAdapter(byte destination, GoodsList gl) {
        if (isAdded()) {
            ListView lvGoods = getListView();
            lvGoods.setEmptyView(getActivity().findViewById(R.id.empty_view));
            if (mGoodsList != null) {
                mGoodsAdapter = new GoodsAdapter(getActivity(), gl, mExistedItems, destination, getArguments().getString(MainActivity.PRICE), isForeignClient);
                mGoodsAdapter.setRequestCode(getTargetRequestCode());
                lvGoods.setAdapter(mGoodsAdapter);
            } else {
                if (mGoodsAdapter != null)
                    mGoodsAdapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null)
            return;
        switch (requestCode) {
            case IDD_ADD:
                switch (mListViewMode) {
                    case GoodsAdapter.MODE_ORDER:
                        OrderItem oi = data.getParcelableExtra(OrderItem.KEY);
                        if (oi == null)
                            return;
                        if (mExistedItems == null)
                            mExistedItems = new ArrayList<>();
                        int countPosition = oi.inPack() ? oi.getCountPack() : oi.getCount();
                        byte meashPosition = oi.inPack() ? (byte) 1 : 0;
                        mExistedItems.add(new GoodsInDocument(oi.getCodeGoods(), countPosition, meashPosition, oi.getQuant(), oi.getPrice(), oi.getWeight()));
                        // Выделяем элемент
                        if (mGoodsList != null && -1 < currentSelectedItemIndex
                                && currentSelectedItemIndex < mGoodsList.size()) {
                            mGoodsList.get(currentSelectedItemIndex).inDocument(true);
                        }

                        updateAdapterAfterAddItemsInOrder();
                        break;

                    case GoodsAdapter.MODE_RETURN:
                        if (resultCode == Activity.RESULT_OK) {
                            Returns retData = Returns.getReturn();
                            if (retData != null) {
                                ReturnItem item = data
                                        .getParcelableExtra(ReturnItem.KEY);
                                if (item == null)
                                    return;

                                int index = retData.getIndex(item.getCodeGoods());
                                if (index == -1) {
                                    retData.addItem(item);
                                } else {
                                    retData.updateItem(item);
                                }
                                // Выделяем элемент
                                if (mGoodsList != null && -1 < currentSelectedItemIndex
                                        && currentSelectedItemIndex < mGoodsList.size()) {
                                    mGoodsList.get(currentSelectedItemIndex)
                                            .inDocument(true);
                                }
                                notifyAdapter();
                            }
                        }
                        break;
                    case GoodsAdapter.MODE_REFERENCE:
                        // Ничего не делаем
                        break;
                }
                break;

            default:
                break;
        }
    }


    @Override
    public void onTaskComplete(LoadGoodsGroupTask task) {

    }

    @Override
    public void onTaskComplete(LoadGoodsOnTreeClickTask task) {
        try {
            boolean res = task.get();
            // Если активно тыкать в экран - getListView() вернёт exception
            if (res) {
                mGoodsList = task.getList();
                ListView lv = getListView();
                if (lv != null) {
                    setListAdapter(mListViewMode, mGoodsList);
                }
            } else {
                GlobalProc.mToast(context, errorLoad);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        //waitCursor(false);
    }

    @Override
    public void onTaskComplete(LoadGroupMatrixTask task) {

    }

    private boolean isUnCheckedMenu() {
        if (menuItemObjects.size() > 0) {
            for (MenuItemObject menuItem : menuItemObjects) {
                if (!menuItem.isChecked()) {
                    return true;
                }
            }
        }
        return false;
    }

    private GoodsList getGoodsLimit(int limit, int offset) {
        MyApp app = (MyApp) context.getApplication();
        SQLiteDatabase db = app.getDB();
        String whereRest = "";
        Set<String> collectionSetChecked = new LinkedHashSet<>();
        if (isUnCheckedMenu()) {
            for (MenuItemObject itemObject : menuItemObjects) {
                String scladName = itemObject.getScladName();
                if (itemObject.isChecked()) {
                    List<String> listCodeGoodsToSclad = Remainder.loadCodeGoodsToSclad(getActivity(), scladName);
                    if (listCodeGoodsToSclad != null) {
                        collectionSetChecked.addAll(listCodeGoodsToSclad);
                    }
                }
            }
        }
        String inClause = collectionSetChecked.toString();
        if (collectionSetChecked.size() > 0) {
            inClause = inClause.replace("[", "(");
            inClause = inClause.replace("]", ")");
            inClause = " AND " + GoodsMetaData.FIELD_CODE.Name + " in " + inClause;
        } else {
            inClause = "";
        }

        if (modeRadio == RADIO_WITH_REST) {
            whereRest = " AND " + GoodsMetaData.FIELD_REST + " > 0";
        }

        String sql = "SELECT * FROM " + GoodsMetaData.TABLE_NAME + " WHERE "
                + GoodsMetaData.FIELD_RECORD_TYPE.Name + " =\"0\"" + whereRest + inClause + " LIMIT " + limit + " OFFSET " + offset + ";";
        Log.d(TAG, sql);
        GoodsList resList = new GoodsList();
        Cursor results = null;
        try {
            results = db.rawQuery(sql, null);
            while (results.moveToNext()) {
                Goods good = new Goods();
                // results.getString(0), results.getString(2),
                // results.getString(1)
                good.setCode(results.getString(GoodsMetaData.FIELD_CODE.Index));
                good.setName(results.getString(GoodsMetaData.FIELD_NAME.Index));
                good.setParentCode(results.getString(GoodsMetaData.FIELD_PARENT.Index));
                good.setNameFull(results.getString(GoodsMetaData.FIELD_NAME_FULL.Index));
                good.setBaseMeash(results.getString(GoodsMetaData.FIELD_BASE_MEASH.Index));
                good.setPackMeash(results.getString(GoodsMetaData.FIELD_PACK_MEASH.Index));
                good.setNumInPack(results.getFloat(GoodsMetaData.FIELD_NUM_IN_PACK.Index));
                float w = results.getFloat(GoodsMetaData.FIELD_WEIGHT.Index);
                float q = results.getFloat(GoodsMetaData.FIELD_QUANT.Index);
                good.setQuant(q);
                good.setRestOnStore(results.getInt(GoodsMetaData.FIELD_REST.Index));
                good.setPriceContract(results.getFloat(GoodsMetaData.FIELD_PRICE_CONTRACT.Index));
                good.setPriceStandart(results.getFloat(GoodsMetaData.FIELD_PRICE_STANDART.Index));
                good.setPriceShop(results.getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index));
                good.setPriceRegion(results.getFloat(GoodsMetaData.FIELD_PRICE_REGION.Index));
                good.setPriceMarket(results.getFloat(GoodsMetaData.FIELD_PRICE_MARKET.Index));
                good.setPriceOptSection(results.getFloat(GoodsMetaData.FIELD_PRICE_SECTION.Index));
                good.setBestBefore(results.getString(GoodsMetaData.FIELD_BEST_BEFORE.Index));
                good.setNDS((byte) results.getShort(GoodsMetaData.FIELD_NDS.Index));
                good.setCountry(results.getString(GoodsMetaData.FIELD_COUNTRY.Index));
                good.setWeight(w);
                good.setHighLight((byte) results.getShort(GoodsMetaData.FIELD_HIGHLIGHT.Index));
                good.setIsWeightGood(results.getShort(GoodsMetaData.FIELD_IS_WEIGHT.Index) == 1);
                good.isHalfHead(results.getShort(GoodsMetaData.FIELD_IS_HALFHEAD.Index) == 1);
                good.perishable(results.getShort(GoodsMetaData.FIELD_IS_PERISHABLE.Index) == 1);
                good.stm(results.getShort(GoodsMetaData.FIELD_STM.Index) == 1);
                good.manager(results.getString(GoodsMetaData.FIELD_MANAGER_FK.Index));
                good.transitList(Goods.loadTransitList(db, good.getCode()));
                good.setIsMercury(results.getInt(GoodsMetaData.FIELD_IS_MERCURY.Index));
                good.setIsCustom(results.getInt(GoodsMetaData.FIELD_IS_CUSTOM.Index));
                good.setNonCash(results.getInt(GoodsMetaData.FIELD_IS_NON_CASH.Index) == 1);
                good.calcVisiblePrice(mPriceLevel, mDiscount);

                resList.add(good);
            }
            results.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (results != null && !results.isClosed()) {
                results.close();
            }
        }
        Log.d(TAG, "resList.size: " + resList.size());
        return resList;
    }

    @Override
    public void onEndOfList() {
        if (getPrefsTypeLoad() == GoodsList.MODE_LIST) {
            this.offset += 10 + limit;
            this.initData();
        }
    }


    @SuppressLint("StaticFieldLeak")
    private void initData() {

        new AsyncTask<Void, Void, ArrayList<Goods>>() {


            @Override
            protected ArrayList<Goods> doInBackground(Void... voids) {
                return getGoodsLimit(limit, offset);
            }

            @Override
            protected void onPostExecute(ArrayList<Goods> temp) {
                if (temp != null && temp.size() > 0)
                    listLazyGoods.addAll(temp);
                update();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    private void update() {
        if (this.mGoodsAdapterLazy != null) {
            this.mGoodsAdapterLazy.notifyAfterExistedListChanged();
        }
    }


    private class SearchItemsTask extends AsyncTask<Void, Void, GoodsList> {
        private final static byte ONE = (byte) 1;
        private byte mFilter;
        private String mMLCategory;
        private String query;
        private List<MenuItemObject> menuItemObjects;
        private ProgressDialog progressDialog;

        public SearchItemsTask(byte filter, String mlCat, String query, List<MenuItemObject> menuItemObjects) {
            mFilter = filter;
            mMLCategory = mlCat;
            this.query = query;
            this.menuItemObjects = menuItemObjects;
        }


        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(getActivity(), "",
                    "Пожалуйста подождите...", true);
            super.onPreExecute();
        }

        @Override
        protected GoodsList doInBackground(Void... params) {
            Activity activity = getActivity();
            if (activity == null)
                return null;

//            String query = PreferenceManager.getDefaultSharedPreferences(
//                    activity).getString(PREF_SEARCH_QUERY, null);
            MyApp app = (MyApp) getActivity().getApplication();
            GoodsList result = null;
            if (query != null) {
                String where = String.format(Locale.getDefault(), "%s like ?",
                        GoodsMetaData.FIELD_NAME.Name);// UPPER(%s)
                switch (mFilter) {
                    case RADIO_WITH_REST:
                        where += String.format(" AND %s > 0",
                                GoodsMetaData.FIELD_REST.Name);
                        break;
                }
                if (loadGoodsTask_MODE == GoodsList.MODE_STM) {
                    where += String.format(" AND %s = 1",
                            GoodsMetaData.FIELD_STM.Name);
                } else if (loadGoodsTask_MODE == GoodsList.MODE_MATRIX) {
                    where += String.format(" AND %s = 1",
                            GoodsMetaData.FIELD_STM.Name);
                }
                String[] args = new String[]{"%" + query + "%"};// {query.toUpperCase()};
                if (query.length() == 5 && GlobalProc.parseInt(query.substring(1)) > 0) {
                    where = String.format(Locale.getDefault(), "%s = ? AND %s = 0",
                            GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_RECORD_TYPE.Name);
                    args = new String[]{query};
                }

                result = GoodsList.loadList(app.getDB(), where, args, null);
                // Выделяем элементы маст-листа если есть товар и получили
                // категорию
                if (result != null && mMLCategory != null) {
                    ArrayList<String> ml = GoodsList.loadMustList(app.getDB(),
                            App.get(getActivity()).getCodeAgent(), mMLCategory,
                            null);
                    if (ml != null && ml.size() > 0) {
                        for (String code : ml) {
                            for (Goods goods : result) {
                                if (goods.getCode().equalsIgnoreCase(code)) {
                                    goods.setHighLight(ONE);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                if (mSelectedNode != null) {
                    switch (mFilter) {
                        case RADIO_WITH_REST:
                            result = GoodsList.loadChildWithRest(app.getDB(), mSelectedNode.getCode());
                            break;
                        default:
                            result = GoodsList.loadChild(app.getDB(), mSelectedNode.getCode());
                            break;
                    }
                    if (menuItemObjects.size() > 0) {
                        /**
                         это фильтр для складов
                         на множестве складов может быть один и тот же код товара
                         * */
                        Set<String> collectionSetUnchecked = new LinkedHashSet<>();
                        Set<String> collectionSetChecked = new LinkedHashSet<>();
                        for (MenuItemObject itemObject : menuItemObjects) {
                            String scladName = itemObject.getScladName();
                            if (itemObject.isChecked()) {
                                List<String> listCodeGoodsToSclad = Remainder.loadCodeGoodsToSclad(getActivity(), scladName);
                                if (listCodeGoodsToSclad != null) {
                                    collectionSetChecked.addAll(listCodeGoodsToSclad);
                                }
                            } else {
                                List<String> listCodeGoodsToSclad = Remainder.loadCodeGoodsToSclad(getActivity(), scladName);
                                if (listCodeGoodsToSclad != null) {
                                    collectionSetUnchecked.addAll(listCodeGoodsToSclad);
                                }
                            }
                        }
                        Iterator<String> iteratorUnchecked = collectionSetUnchecked.iterator();
                        while (iteratorUnchecked.hasNext()) {
                            String codeUnchecked = iteratorUnchecked.next();
                            for (String codeChecked : collectionSetChecked) {
                                if (codeUnchecked.equals(codeChecked)) {
                                    iteratorUnchecked.remove();
                                    break;
                                }
                            }
                        }
                        Iterator<Goods> goodsIterator = result.iterator();
                        while (goodsIterator.hasNext()) {
                            Goods goods = goodsIterator.next();
                            for (String codeUnchecked : collectionSetUnchecked) {
                                if (goods.getCode().equals(codeUnchecked)) {
                                    goodsIterator.remove();
                                    break;
                                }
                            }
                        }
                        List<Remainder> remainder;
                        if (collectionSetUnchecked.size() > 0)
                            for (Goods goods : result) {
                                remainder = Remainder.load(getActivity(), goods.getCode(), menuItemObjects);
                                if (remainder != null) {
                                    Float totalRemainder = 0.0f;
                                    for (Remainder r : remainder) {
                                        if (!r.getRemainder().isEmpty())
                                            totalRemainder = totalRemainder + Float.valueOf(r.getRemainder());
                                    }
                                    goods.setRestOnStore(totalRemainder);
                                }
                            }
                    }
                }
            }
            //Проставляем цены
            if (result != null)
                for (Goods goods : result) {
                    goods.calcVisiblePrice(mPriceLevel, mDiscount);
                }
            if (mExistedItems != null) {
                for (GoodsInDocument item : mExistedItems) {
                    for (Goods goods : result) {
                        if (item.getCode().equalsIgnoreCase(goods.getCode())) {
                            goods.inDocument(true);
                            break;
                        }
                    }
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(GoodsList items) {

            mGoodsList = items;
            // Boolean withRest = rbRest != null && rbRest.isChecked();
            setListAdapter(mListViewMode, mGoodsList);// withRest ?
            // goodsWithRest(mGoodsList)
            // : mGoodsList
            progressDialog.dismiss();
        }
    }


    public class MenuItemObject {

        private String scladName;
        private int id;
        private boolean checked;

        MenuItemObject(String scladName, int id, boolean checked) {
            this.scladName = scladName;
            this.id = id;
            this.checked = checked;
        }

        public String getScladName() {
            return scladName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isChecked() {
            return checked;
        }

        void setChecked(boolean checked) {
            this.checked = checked;
        }
    }

}
