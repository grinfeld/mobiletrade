package com.madlab.mtrade.grinfeld.roman.iface;


/**
 * Created by GrinfeldRA
 */

public interface IGetSizePromotionGoodsTask {

    void onCompleteGetSizePromotionGoodsTask(Integer integer);

}
