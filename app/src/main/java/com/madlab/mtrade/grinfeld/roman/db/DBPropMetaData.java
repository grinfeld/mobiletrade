package com.madlab.mtrade.grinfeld.roman.db;

import android.net.Uri;
import android.provider.BaseColumns;

public final class DBPropMetaData implements BaseColumns {
    private DBPropMetaData() {
    }

    public static final String TABLE_NAME = "dbProp";

    public static final Uri CONTENT_URU = Uri.parse(String.format(
            "content://%s/%s", DBHelper.AUTHORITY, TABLE_NAME));

    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mtradedb."
            + TABLE_NAME;
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mtradedb."
            + TABLE_NAME;

    public static final String DEFAULT_SORT_ORDER = "modified DESC";

    public static final String FIELD_NUM_DOC = "numDoc";
    public static final String FIELD_NUM_ORDER = "numOrder";

    public static final String CREATE_TABLE = String.format("CREATE TABLE %s ("
                    + "%s int not null default 1, " + "%s int not null default 1)",
            TABLE_NAME, FIELD_NUM_DOC, FIELD_NUM_ORDER);

}