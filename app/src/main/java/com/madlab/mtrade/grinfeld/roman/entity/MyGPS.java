package com.madlab.mtrade.grinfeld.roman.entity;


import java.util.Date;
import java.util.Locale;

import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.LocationMetadata;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

/**
 * Класс, описывающий координату GPS. Есть поле для ссылки на заявку...
 *
 * @author Konovaloff
 *
 */
public class MyGPS {
    private final static String TAG = "!->MyGPS";
    public final static String UNKNOWN_CLIENT_CODE = "XXXXXX";

    public static enum SELECT_MODE {
        All, Sended, Unsended
    }

    ;

    private int id;

    public Date timeStamp;
    private float latitude;
    private float longitude;
    private float accuracy;
    private float speed;

    private String provider;
    private String clientCode;

    short sended;

    public MyGPS(int id, Date timeStamp, float latitude, float longitide,
                 float accuracy, float speed, String provider, String codeClient,
                 short sended) {
        this.id = id;
        this.timeStamp = timeStamp;
        this.latitude = latitude;
        this.longitude = longitide;
        this.speed = speed;
        this.accuracy = accuracy;
        this.provider = provider;
        this.clientCode = codeClient;
        this.sended = sended;
    }

    public MyGPS(Location location) {
        if (location != null) {
            id = -1;
            this.timeStamp = new Date(location.getTime());
            this.latitude = (float) location.getLatitude();
            this.longitude = (float) location.getLongitude();
            this.accuracy = (float) location.getAccuracy();
            this.speed = (float) location.getSpeed();
            this.provider = location.getProvider();
            this.clientCode = UNKNOWN_CLIENT_CODE;
        }
    }

    public int getId() {
        return id;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }

    public void clientCode(String codeClient) {
        this.clientCode = codeClient;
    }

    public String clientCode() {
        return clientCode;
    }

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%.6f|%.6f|%.3f|%.2f|%s|%s",
                latitude, longitude, speed, accuracy, provider, clientCode);
    }

    public static MyGPS[] load(final Cursor rows) {
        MyGPS[] result = null;

        try {
            result = new MyGPS[rows.getCount()];

            int id = -1;

            do {
                id = rows.getInt(LocationMetadata.FIELD_ID_NUM);

                Date date = TimeFormatter.parseSqlDate(rows
                        .getString(LocationMetadata.FIELD_DATE_NUM));

                MyGPS gps = new MyGPS(id, date,
                        rows.getFloat(LocationMetadata.FIELD_LATITUDE_NUM),
                        rows.getFloat(LocationMetadata.FIELD_LONGITUDE_NUM),
                        rows.getFloat(LocationMetadata.FIELD_ACCURACY_NUM),
                        rows.getFloat(LocationMetadata.FIELD_SPEED_NUM),
                        rows.getString(LocationMetadata.FIELD_PROVIDER_NUM),
                        rows.getString(LocationMetadata.FIELD_CLIENT_NUM),
                        rows.getShort(LocationMetadata.FIELD_SENDED_NUM));
                result[rows.getPosition()] = gps;
            } while (rows.moveToNext());
        } catch (Exception e) {
            Log.e(TAG, "Ошибка загрузки координат из БД");
        }
        return result;
    }

    /**
     * Загружает координату по номер (автоинкременту) из базы данных. ОСТОРОЖНО
     * может вернуть null
     *
     * @param db
     *            ссылка на базу данных
     * @param id
     *            номер
     * @return координата
     */
    public static Location load(SQLiteDatabase db, int id) {
        Location result = null;
        String sql = String.format("SELECT * FROM %s WHERE %s = %d",
                LocationMetadata.TABLE_NAME, LocationMetadata._ID, id);
        Cursor rows = db.rawQuery(sql, null);
        while (rows.moveToNext()) {
            result = new Location(
                    rows.getString(LocationMetadata.FIELD_PROVIDER_NUM));
            result.setLatitude(rows
                    .getFloat(LocationMetadata.FIELD_LATITUDE_NUM));
            result.setLongitude(rows
                    .getFloat(LocationMetadata.FIELD_LONGITUDE_NUM));
            result.setAccuracy(rows
                    .getFloat(LocationMetadata.FIELD_ACCURACY_NUM));
            result.setSpeed(rows.getFloat(LocationMetadata.FIELD_SPEED_NUM));
            result.setTime(TimeFormatter.parseSqlDateWithTimeToLong(rows.getString(LocationMetadata.FIELD_DATE_NUM)));
            break;
        }
        rows.close();
        return result;
    }

    // private static void update(final SQLiteDatabase db, MyGPS[] tracking) {
    // ContentValues cv = null;
    // try {
    // String where = String.format("%s = ?", LocationMetaData.FIELD_ID);
    // for (MyGPS myGPS : tracking) {
    // if (myGPS.getId() == -1)
    // continue;
    //
    // cv = new ContentValues();
    // cv.put(LocationMetaData.FIELD_SENDED, true);
    //
    // String[] args = { Integer.toString(myGPS.getId()) };
    // int res = db.update(LocationMetaData.TABLE_NAME, cv, where,
    // args);
    // if (res < 1) {
    // Log.e(TAG,
    // String.format("update for row %d failed",
    // myGPS.getId()));
    // }
    // }
    // } catch (Exception e) {
    // Log.e(TAG, e.toString());
    // } finally {
    // }
    // }

    public static MyGPS getLastLocation(Context context) {
        MyGPS result;
        Location locGPS = null;
        Location locCell = null;
        LocationManager manager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
                locGPS = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                locCell = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        long timeGPS = 0;
        if (locGPS != null) {
            timeGPS = locGPS.getTime();
        }

        long timeCell = 0;
        if (locCell != null) {
            timeCell = locCell.getTime();
        }

        if (0 == timeGPS && timeCell == 0) {
            result = null;
        } else {
            if (timeGPS > timeCell) {
                result = new MyGPS(locGPS);
            } else {
                result = new MyGPS(locCell);
            }
        }

        return result;
    }
}
