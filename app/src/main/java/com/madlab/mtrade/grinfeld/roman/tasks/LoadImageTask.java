package com.madlab.mtrade.grinfeld.roman.tasks;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

public class LoadImageTask extends AsyncTask<String, Void, Boolean> {
    private final static String TAG = "!->LoadImageTask";

    public final static byte RES_SUCCESS = 0;
    public final static byte RES_FAIL = -1;

    private Bitmap mImage;
    private String mUnswer;
    private IListener mListener;

    public interface IListener {
        void onImageDownloaded(Bitmap thumbnail, String message);
    }

    public void setListener(IListener listener) {
        mListener = listener;
    }

    public String getMessage() {
        return mUnswer;
    }

    public LoadImageTask() {
        mUnswer = "";
    }


    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(String... urlString) {
        try {
            trustEveryone();
            byte[] bitmapBytes = getUrlBytes(urlString[0]);
            mImage = decodeSampledBitmapFromResource(bitmapBytes, 100,100);
            //BitmapFactory.decodeByteArray(bitmapBytes, 0, bitmapBytes.length, options);
        } catch (UnknownHostException e) {
            mUnswer = "Не удается подключиться к электронному каталогу";
            return false;
        } catch (ConnectException e) {
            mUnswer = "Не удается подключиться к электронному каталогу";
            return false;
        } catch (Exception e) {
            mUnswer = "Не удается загрузить изображение";
            Log.e(TAG, e.getMessage());
            return false;
        }
        if (mImage == null) {
            mUnswer = "Не удалось загрузить изображение";
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mListener != null) {
            mListener.onImageDownloaded(mImage, mUnswer);
        }
    }


    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private Bitmap decodeSampledBitmapFromResource(byte[] data, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //options.inPurgeable = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, imageWidth, imageHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

    private byte[] getUrlBytes(String urlSpec) throws IOException {
        //кудирект на http://dalimoshop/get_images.php?secret=NTgyOTI=&id=58292&is_iframe=1&size=320x320
        URL url = new URL(urlSpec);
        HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
        connection.setInstanceFollowRedirects(false);   // Make the logic below easier to detect redirections
        connection.setRequestProperty("User-Agent", "Mozilla/5.0...");
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }});
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager(){
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {}
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }}}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(
                    context.getSocketFactory());
        } catch (Exception e) { // should never happen
            e.printStackTrace();
        }
    }

}
