package com.madlab.mtrade.grinfeld.roman.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by GrinfeldRA
 */

public class ProgressPhotoIntentService extends IntentService {

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.progressPhoto";

    public ProgressPhotoIntentService() {
        super(BROADCAST_ACTION);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Intent result = new Intent(BROADCAST_ACTION);
        if (intent != null) {
            result.putExtra("extra", intent.getIntExtra("extra", 0));
        }
        sendBroadcast(result);
    }
}
