package com.madlab.mtrade.grinfeld.roman.entity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.PhotoFilterMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class PhotoFilter {

    private static final String TAG = "#PhotoFilter";
    String code;
    String value;

    public PhotoFilter(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

    public static List<PhotoFilter> load(Context ctx) {
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return load(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    @Override
    public String toString() {
        return value;
    }

    private static List<PhotoFilter> load(SQLiteDatabase db) {
        List<PhotoFilter> res = new ArrayList<>();
        Cursor rows = null;
        try {
            rows = db.query(PhotoFilterMetaData.TABLE_NAME, null, null, null, null, null, null);
            PhotoFilter r;
            while (rows.moveToNext()) {
                String code = rows.getString(PhotoFilterMetaData.FIELD_CODE.Index);
                String val = rows.getString(PhotoFilterMetaData.FIELD_VALUE.Index);
                r = new PhotoFilter(code,val);
                res.add(r);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }
}
