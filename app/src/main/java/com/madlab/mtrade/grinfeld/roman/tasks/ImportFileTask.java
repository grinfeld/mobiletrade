package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.AutoorderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ClientsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ContactMetaData;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.DiscountsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ExcludedMetaData;
import com.madlab.mtrade.grinfeld.roman.db.FirmsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ManagerMerchMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ManagerMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MatrixMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MustListMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PhotoFilterMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PostMetaData;
import com.madlab.mtrade.grinfeld.roman.db.RemainderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TransitMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;
import com.madlab.mtrade.grinfeld.roman.fragments.StatisticsFragment;
import com.madlab.mtrade.grinfeld.roman.iface.IImportComplete;


public class ImportFileTask extends AsyncTask<String, Integer, Boolean> {
    private final String TAG = "!->ImportFile";

    public final static byte FINISHED = 5;
    public final static byte INTERRUPTED = 6;

    private final byte MODE_CLIENTS = 0;
    private final byte MODE_GOODS = 1;
    private final byte MODE_CREDIT = 2;
    private final byte MODE_FIRMS = 3;
    private final byte MODE_MATRIX = 4;
    private final byte MODE_MUSTLIST = 5;
    private final byte MODE_SMTP = 6;
    private final byte MODE_TRANSIT = 7;
    private final byte MODE_EXCLUDED = 8;
    private final byte MODE_AUTOORDER = 9;
    private final byte MODE_DISCOUNTS = 10;
    private final byte MODE_MANAGERS = 11;
    private final byte MODE_OSTATKY = 12;
    private final byte MODE_PHOTO_FILTER = 13;
    private final byte MODE_POSTS = 14;

    private final short MAX_QUERYES_ARRAY = 1000;

    private ProgressDialog mProgress;
    private Activity context;
    private byte mCurrentStep;
    private boolean mDeleteDocuments = false;
    public boolean selectedImport = false;
    private IImportComplete mTaskCompleteListener;

    private final static String ZERO_PRICE = "0.00";
    private final static String ZERO_COUNT = "0";
    private final static String NOT_DATA = "Нет данных";

    /**
     * Эталонный список файлов
     */
    private String[] mFilesList;

    private String[] mDescriptionsList;
    private String[] mClearDocsList;

    private String mAgent; // код сотрудника
    private String mExclusiveCommand; // Эксклюзивная команда
    private String mImpExpPath; // Путь к папке обмена

    // переменные для списка исключенния
    private String mLastName;
    private byte mIndex;

    /**
     * Запускаем обновление данных из файлов для импорта
     *
     * @param context , базовый контекст
     * @param ,       шаг, на котором импорт был остановлен. Если новый, то 0
     */
    public ImportFileTask(Activity context,
                          IImportComplete taskCompleteListener) {
        this.context = context;
        mTaskCompleteListener = taskCompleteListener;

        mLastName = "";
        mIndex = -1;
    }

    /**
     * Признак необходимости удаления документов
     */
    public void deleteDocuments(boolean data) {
        mDeleteDocuments = data;
    }

    public void setSelectedImport(boolean isSelected){
        selectedImport = isSelected;
    }

    @Override
    protected void onPreExecute() {
        showProgress(context);

        if (App.isMerch()) {
            mFilesList = context.getResources().getStringArray(
                    R.array.list_files_merch);
        } else {
            mFilesList = context.getResources().getStringArray(
                    R.array.list_files);
        }

        mDescriptionsList = context.getResources().getStringArray(
                R.array.list_files_description);

        mClearDocsList = DBHelper.CLEAR_DOC_QUERYS_LIST;

        mAgent = GlobalProc.getPref(context, R.string.pref_codeManager);
        mExclusiveCommand = GlobalProc.getPref(context,
                R.string.pref_exclusive);

        mImpExpPath = App.get(context.getApplicationContext())
                .getImpExpPath();
    }

    @Override
    protected Boolean doInBackground(String... paths) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            db = app.getDB();
            if (mDeleteDocuments) {
                DeleteAllDocs(db);
            }

            MyApp.setUser(App.get(context).getCodeAgent());
            mProgress.setProgress(0);
            mProgress.setMax(paths.length * 2);
            for (byte i = 0; i < paths.length; i++) {
                String fileName = paths[i];
                String currentFile = fileName.contains("OST_SKL") ? mExclusiveCommand + fileName : fileName;

                if (App.isMerch()) {
                    if (fileName.contains("KLIENTS")) {
                        currentFile = "KLIENTS_M.TXT";
                    } else if (fileName.contains("MANAGERS")) {
                        currentFile = "MANAG_M.TXT";
                    } else if (fileName.contains("MATRIX")) {
                        currentFile = "MATRIX_M.TXT";
                    }
                }
                File file = new File(mImpExpPath, currentFile);
                if (!file.exists()) {
                    // Пробуем заводскую директорию
                    String impExpPath = Environment
                            .getExternalStorageDirectory().getAbsolutePath()
                            + File.separator + Const.IMPEXP_FOLDER;
                    file = new File(impExpPath, currentFile);
                    if (!file.exists()) {
                        Log.e(TAG, "Файл не существует: " + mImpExpPath
                                + File.separator + currentFile);
                        //if (i == paths.length - 1) {
                        //    return false;
                        // } else {
                        continue;
                        //}
                    }
                }

                for (byte j = 0; j < mFilesList.length; j++) {
                    if (mFilesList[j].equalsIgnoreCase(fileName)) {
                        mCurrentStep = j;
                        break;
                    }
                }

                String deleteQuery = DBHelper.CLEAR_TABLES_LIST[mCurrentStep];
                String[] query;
                if (deleteQuery.equals(ManagerMetaData.CLEAR)) {
                    query = new String[]{ManagerMerchMetaData.CLEAR, ManagerMetaData.CLEAR};
                } else if (deleteQuery.equals(ClientsMetaData.CLEAR)) {
                    query = new String[]{ClientsMetaData.CLEAR, ContactMetaData.CLEAR};
                } else {
                    query = new String[]{deleteQuery};
                }
                if (deleteQuery != null) {
                    try {
                        DBHelper.execMultipleSQLTXN(db, query);
                    } catch (Exception e) {
                        Log.e(TAG, "Clear table error: " + e.toString());
                    }
                }

                final FileInputStream stream = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(stream,
                        Const.WIN1251);
                final BufferedReader br = new BufferedReader(isr);

                StringBuffer sBuf = new StringBuffer();

                int insertCounter = MAX_QUERYES_ARRAY;
                int count = 0;

                String line;
                while ((line = br.readLine()) != null) {//
                    String sql = null;
                    switch (mCurrentStep) {
                        case MODE_CLIENTS:
                            sql = parseLineClients(line, mAgent);
                            break;
                        case MODE_GOODS:
                            sql = parseLineGoods(line, count);
                            break;
                        case MODE_CREDIT:
                            sql = parseLineCreditInfo(line, mAgent);
                            break;
                        case MODE_FIRMS:
                            sql = parseLineFirms(line);
                            break;
                        case MODE_MATRIX:
                            sql = parseLineMatrix(line, mAgent, count);
                            break;
                        case MODE_MUSTLIST:
                            sql = parseLineMustList(line, mAgent);
                            break;
                        case MODE_SMTP:
                            sql = null;
                            //parseLineSMTP(line, mAgent);
                            break;
                        case MODE_TRANSIT:
                            sql = parseLineTransit(line);
                            break;
                        // case MODE_LAST_SALES:
                        // sql = parseLineLastSales(line, mAgent);
                        // break;
                        case MODE_EXCLUDED:
                            sql = parseLineExcluded(line);
                            break;
                        case MODE_AUTOORDER:
                            sql = parseLineAutoOrder(line, mAgent);
                            break;
                        case MODE_DISCOUNTS:
                            sql = parseLineDiscounts(line, mAgent);
                            break;
                        case MODE_MANAGERS:
                            sql = parseLineManagers(line);
                            break;
                        case MODE_OSTATKY:
                            sql = parseLineOstatky(line);
                            break;
                        case MODE_PHOTO_FILTER:
                            sql = parsePhotoFilter(line);
                            break;
                        case MODE_POSTS:
                            sql = parsePosts(line);
                            break;
                        default:
                            break;
                    }

                    // Добавляем запрос в список
                    if (sql != null) {
                        sBuf.append(sql + Const.NewLine);
                    }
                    count++;

                    // При достижении максимального количества записей в массиве
                    // производим вставку значений в БД
                    if (count >= insertCounter) {
                        DBHelper.execMultipleSQLTXN(db,
                                sBuf.toString().split(Const.NewLine));
                        sBuf = new StringBuffer();
                        insertCounter += MAX_QUERYES_ARRAY;
                    }
                }// while

                br.close();
                publishProgress((mCurrentStep * 2) + 1);

                // Теперь запускаем запросы на выполнение
                if (mCurrentStep != MODE_SMTP && sBuf.length() > 0) {
                    DBHelper.execMultipleSQLTXN(db,
                            sBuf.toString().split(Const.NewLine));
                }

                mCurrentStep++;
                publishProgress(mCurrentStep * 2);
            }// for
        }// try
        catch (Exception e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String parsePosts(String line) {
        String[] arr = line.split(";");
        int id = 0;
        String value = "";
        int j = 0;
        for (String word : arr) {
            switch (j) {
                case 0:
                    id = Integer.parseInt(word);
                    break;
                case 1:
                    value = word;
                    break;
            }
            j++;
        }
        return PostMetaData.insertQuery(id, value);
    }

    private String parsePhotoFilter(String line) {
        String[] arr = line.split(";");
        String code = "";
        String value = "";
        int j = 0;
        for (String word : arr) {
            switch (j) {
                case 0:
                    code = word;
                    break;
                case 1:
                    value = word;
                    break;
            }
            j++;
        }

        return PhotoFilterMetaData.insertQuery(code, value);
    }


    @Override
    protected void onProgressUpdate(Integer... values) {
        if (mDescriptionsList != null && mProgress != null) {
            int posi = (int) (values[0] / 2) >= (mDescriptionsList.length - 1) ? (mDescriptionsList.length - 1)
                    : values[0] / 2;
            String msg = String.format("Обработка файла: %s", mDescriptionsList[posi]);
            mProgress.setMessage(msg);
            mProgress.setProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dismiss();
        App.Reload(context);
        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void showProgress(Context context) {
        mProgress = new ProgressDialog(context);
        String text = context.getString(R.string.mes_wait_for_load);
        if (!StatisticsFragment.deleteDocuments) {
            text = context.getString(R.string.mes_wait_for_load_doc_not_del);
        }
        mProgress.setMessage(context.getString(R.string.mes_wait_for_load));
        mProgress.setTitle(text);
        mProgress.setCancelable(false);
        mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgress.show();
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.hide();
            mProgress.dismiss();
        }
    }


    public void unlink() {
        dismiss();
        context = null;
    }

    private void DeleteAllDocs(SQLiteDatabase db) {
        DBProp.DropNumeration(db);
        DBHelper.execMultipleSQL(db, mClearDocsList);
    }

    /**
     * parseLineGoods
     */
    private String parseLineGoods(String line, int counter) {
        if (line.length() < 27)
            return null;

        String[] arr = line.split(";");

        char rType = '0';
        String parentCode = "";
        String codeGoods = "";
        String goodsName = "";
        String baseMeash = "";
        String packMeash = "";
        String numInPack = "0";
        String minShip = "0";
        String rest = "0";
        String priceContract = ZERO_PRICE;
        String priceStandart = ZERO_PRICE;
        String priceShop = ZERO_PRICE;
        String priceRegion = ZERO_PRICE;
        String priceMarket = ZERO_PRICE;
        String priceOpt = ZERO_PRICE;
        String bestBefore = "";
        String nds = "10";
        String country = "";
        String brutto = "0";
        String fullName = "";
        String highLight = "0";
        String isWeight = "0";
        String halfHead = "0";
        String isPerishable = "0";
        String isHit = "0";
        String isSTM = "0";
        String manager = "";
        String isMercury = "0";
        String isCustom = "0";
        String isNonCash = "0";
        int j = 0;
        for (String word : arr) {
            switch (j) {
                case 0:// Эксклюзивная команда
//                    if (!word.equalsIgnoreCase(mExclusiveCommand)) {
//                        return null;
//                    }
                    break;
                case 1:// Тип записи (1 - группа, 0 - элемент)
                    rType = word.charAt(0);
                    break;
                case 2:
                    parentCode = word;
                    break;
                case 3:
                    codeGoods = word;
                    break;
                case 4:// name
                    String tmp = GlobalProc.normalizeString(word);
                    goodsName = (tmp.length() > 50) ? tmp.substring(0, 50) : tmp;
                    break;
                case 5:// basemeash
                    baseMeash = word;
                    break;
                case 6:// strPackMeash
                    packMeash = word;
                    break;
                case 7:// strNumInPack
                    numInPack = word.length() > 0 ? word.replace(',', '.') : ZERO_COUNT;
                    break;
                case 8:// strMinShip 0.00
                    minShip = word.length() > 0 ? word.replace(',', '.') : "1";
                    minShip = minShip.equals("0") ? "1" : minShip;
                    break;
                case 9:// RestOnStore
                    rest = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 10:// Price Contract
                    priceContract = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 11:// Price Standart
                    priceStandart = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 12:// Price Shop
                    priceShop = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 13:// Price region
                    priceRegion = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 14:// Price market
                    priceMarket = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 15:// bestBefore
                    bestBefore = word.length() > 0 ? word : NOT_DATA;
                    break;
                case 16:
                    nds = word;
                    break;
                case 17:
                    country = word;
                    break;
                case 18:// ГТД
                    break;
                case 19:
                    break;
                case 20:// Brutto
                    brutto = word;
                    break;
                case 21:// Full Name
                    fullName = GlobalProc.normalizeString(word);
                    break;
                case 22:// highLight
                    // highLight = word; //Подсветка берётся из маст-листа. Здесь
                    // больше не нужна
                    break;
                case 23:// Price opt section
                    priceOpt = word.length() > 0 ? word : ZERO_PRICE;
                    break;
                case 24://Yellow reserv
                    break;
                case 25://Red reserv
                    break;
                case 26:// is WG
                    isWeight = word;
                    break;
                case 27:
                    halfHead = word;
                    break;
                case 28:
                    if (word.equals("1")) {
                        Log.d(TAG, codeGoods);
                    }
                    isPerishable = word;
                    break;
                case 29:// Признак Hit
                    isHit = word;
                    break;
                case 30:// Признак СТМ
                    isSTM = word; //word;
                    break;
                case 31:// Код ответственного менеджера
                    manager = word;
                    break;
                case 32:
                    isMercury = word;
                    break;
                case 33:
                    isCustom = word;
                    break;
                case 34:
                    isNonCash = word;
                    break;
            }
            j++;
        }// for j

        return GoodsMetaData.insertQuery(rType, parentCode, codeGoods,
                goodsName, baseMeash, packMeash, numInPack, minShip, rest,
                priceContract, priceStandart, priceShop, priceRegion,
                priceMarket, priceOpt, bestBefore, nds, country, brutto,
                fullName, highLight, isWeight, halfHead,
                isPerishable, isHit, isSTM, manager, counter, isMercury, isCustom, isNonCash);
    }

    private String parseLineClients(String strSQL, String codeAgent) {
        if (strSQL.length() < 30)
            return null;

        String[] arr = strSQL.split(";");

        String codeManager = "";
        String codeCli = "";
        String name = "";
        String nameFull = "";
        String inn = "";
        String address = "";
        String phones = "";
        String director = "";
        String contact = "";
        int typePrice = 2;
        String discount = "0";
        String maxCredit = "0";
        String curCredit = "0";
        String avgOrder = "0";
        String strVisit = "";
        String startDate = "";
        String visitPlan = "";
        char isToday = '0';
        char isStop = '0';
        String strLat = ZERO_PRICE;
        String strLong = ZERO_PRICE;
        String strSKU = ZERO_COUNT;
        String strShipping = "";
        String strFirmsArray = "";
        String strMLCat = "";
        int typeSalesPlan = 0;

        byte typeDoc = 0;
        byte regMercury = 1;
        byte foreignAgent = 0;

        StringBuilder query = new StringBuilder(Const.NewLine);

        String dateLastUpdate = "";

        int i = 0;
        for (String word : arr) {
            if (App.isMerch()) {
                switch (i) {
                    case 0:// код менеджера
                        String s = word;
                        if (!word.equalsIgnoreCase(codeAgent)) {
                            return null;
                        } else {
                            codeManager = word;
                        }
                        break;
                    case 1:// Код клиента
                        codeCli = word;
                        break;
                    case 2:// Название
                        name = word.replace("\"", "").replace("'", "");
                        nameFull = word.replace("\"", "").replace("'", "");
                        break;
                    case 3:// // ИНН/КПП
                        inn = (word.length() > 20) ? word.substring(0, 20) : word;
                        break;
                    case 4:
                        address = (word.length() == 0) ? "<Не указан>" : word.replace(
                                "\"", "").replace("'", "");
                        break;
                    case 5:
                        phones = (word.length() > 50) ? word.substring(0, 50) : word;
                        break;
                    case 6:
                        contact = word;
                        break;
                }
            } else {
                switch (i) {
                    case 0:// код менеджера
                        String s = word;
                        if (!word.equalsIgnoreCase(codeAgent)) {
                            return null;
                        } else {
                            codeManager = word;
                        }
                        break;
                    case 1:// Код клиента
                        codeCli = word;
                        break;
                    case 2:// Название
                        name = word.replace("\"", "").replace("'", "");
                        break;
                    case 3:// Полное наименование
                        nameFull = word.replace("\"", "").replace("'", "");
                        break;
                    case 4:// ИНН/КПП
                        inn = (word.length() > 20) ? word.substring(0, 20) : word;
                        break;
                    case 5:// Адрес
                        address = (word.length() == 0) ? "<Не указан>" : word.replace(
                                "\"", "").replace("'", "");
                        break;
                    case 6:// Phone
                        phones = (word.length() > 50) ? word.substring(0, 50) : word;
                        break;
                    case 7:// Директор
                        director = word;
                        break;
                    case 8:// Контактное лицо
                        contact = word;
                        break;
                    case 9:// Тип клиента
                        // cliTypeCli = word;
                        break;
                    case 10:// Тип цен
                        typePrice = GlobalProc.parseInt(word, 3);
                        switch (typePrice) {
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                typePrice = typePrice - 2;
                                break;
                            default:
                                typePrice = 0;
                                break;
                        }

                        break;
                    case 11:// Скидка
                        discount = (word.length() > 0) ? word : "0";
                        break;
                    case 12:// Максимальный кредит
                        maxCredit = (word.length() > 0) ? word : "0";
                        break;
                    case 13:// Текущая задолженность
                        curCredit = (word.length() > 0) ? word : "0";
                        break;
                    case 14:// Средняя заявка
                        avgOrder = (word.length() > 0) ? word : "0";
                        break;
                    case 15:// неизвестно
                        break;
                    case 16:// strVisit
                        strVisit = word;
                        break;
                    case 17:// strStartDate
                        startDate = word;
                        break;
                    case 18:// strVisitPlan
                        visitPlan = word;
                        break;
                    case 19:// strTodayVisit
                        isToday = word.length() > 0 ? word.charAt(0) : '0';
                        break;
                    case 20:// Цель визита
                        break;
                    case 21:// Стоп-отгрузка
                        isStop = word.length() > 0 ? word.charAt(0) : '0';
                        break;
                    case 22:// П/Н умолчание
                        typeDoc = (byte) (word.equalsIgnoreCase("п") ? 1 : 2);
                        break;
                    case 23:// широта
                        String val = word.trim().replaceAll("\\s", "");
                        strLat = val.length() > 0 ? val.replace(',', '.')
                                : ZERO_PRICE;
                        break;
                    case 24:// долгота
                        String val2 = word.trim().replaceAll("\\s", "");
                        strLong = val2.length() > 0 ? val2.replace(',', '.')
                                : ZERO_PRICE;
                        break;
                    case 25:// СКЮ
                        strSKU = word.length() > 0 ? word : "0";
                        break;
                    case 26:// RoutesPlan
                        strShipping = word;
                        break;
                    case 27:// FirmsArray
                        //strFirmsArray = word;
                        break;
                    case 28: // MustList category
                        strMLCat = word;
                        break;
                    case 29: //признак клиента, если 1, то есть возможность ПолучитьИсполнениеПланаПродаж
                        typeSalesPlan = Integer.parseInt(word);
                        break;
                    case 30:// FirmsArray и тип оплаты
                        strFirmsArray = word;
                        break;
                    case 31:
                        try {
                            regMercury = Byte.parseByte(word);
                        } catch (Exception e) {

                        }
                        break;
                    case 32:
                        try {
                            foreignAgent = Byte.parseByte(word);
                        } catch (Exception e) {

                        }
                        break;
                    case 33:
                        if (!word.isEmpty()) {
                            String post;
                            String fio;
                            String tel;
                            String email;
                            int responsible;
                            String dateBirth;
                            String note;
                            String[] split = word.split("#");
                            for (String temp : split) {
                                temp = temp.substring(0, temp.lastIndexOf("|"));
                                String[] data = temp.split("[|]", 7);
                                if (data.length == 7) {
                                    post = data[0];
                                    fio = data[1];
                                    tel = data[2];
                                    email = data[3];
                                    responsible = data[4].isEmpty() ? 0 : Integer.parseInt(data[4]);
                                    dateBirth = data[5];
                                    if (dateBirth.length() == "01.01.95".length()) {
                                        dateBirth = dateBirth.substring(0, 6).concat("19").concat(dateBirth.substring(6, 8));
                                    }
                                    note = data[6];
                                    query.append(ContactMetaData.insertQuery(codeAgent, codeCli, post, fio, tel, email, responsible, dateBirth, note))
                                            .append(Const.NewLine);
                                }
                            }
                        }
                        break;
                    case 34:
                        dateLastUpdate = word;
                        break;
                }
            }
            i++;
        }

        return ClientsMetaData.insertQuery(codeManager, codeCli, name,
                nameFull, inn, address, phones, director, contact,
                (byte) typePrice, discount, maxCredit, curCredit, avgOrder,
                strVisit, startDate, visitPlan, isToday, isStop, typeDoc,
                strLat, strLong, strSKU, strShipping, strFirmsArray, strMLCat,
                0, typeSalesPlan, regMercury, foreignAgent, dateLastUpdate)
                .concat(query.toString());
    }

    /**
     * parseLineCreditInfo
     *
     * @param data
     * @param codeAgent
     * @return
     */
    private String parseLineCreditInfo(String data, String codeAgent) {
        if (data.length() < 16)
            return null;

        String[] arr = data.split(";");

        String codeCli = "";
        String numDoc = "";
        String dateDoc = "";
        String sumDoc = "";
        String creditDoc = "";
        String fio = "";
        String expeditor = "";
        String phones = "";
        char typeCH = 'Н';
        String firma = "";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0:// код менеджера
                    if (!word.equalsIgnoreCase(codeAgent)) {
                        return null;
                    }
                    break;
                case 1:// Код клиента
                    codeCli = word;
                    break;
                case 2:// Номер документа
                    numDoc = word;
                    break;
                case 3:// Дата документа
                    dateDoc = TimeFormatter.convertDate1CToSQL(word);
                    break;
                case 4:// Сумма документа
                    sumDoc = word;
                    break;
                case 5:// Сумма долга
                    creditDoc = word;
                    break;
                case 6:// Менеджер (хояин накладной)
                    fio = word;
                    break;
                case 7:// Экспедитор
                    expeditor = word;
                    break;
                case 8:// Номер телефона экспедитора
                    phones = GlobalProc.normalizePhones(word);
                    break;
                case 9:// Type
                    if (word != null && word.length() > 0) {
                        typeCH = word.toUpperCase().charAt(0);
                    }
                    break;
                case 10:
                    firma = word;
                    break;
            }
            i++;
        }

        return ClientCreditInfoMetaData.insertQuery(codeAgent, codeCli, numDoc,
                dateDoc, sumDoc, creditDoc, fio, expeditor, phones, typeCH, firma);
    }

    private String parseLineFirms(String data) {
        if (data.length() < 10)
            return null;

        String[] arr = data.split(";");

        String code = "";
        String name = "";
        String inn = "";
        String address = "";
        String postAddr = "";
        String chief = "";
        String account = "";
        String bank = "";
        String phones = "";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0:// Code
                    code = word;
                    break;
                case 1:// Name
                    name = word;
                    break;
                case 2:// Inn
                    inn = word.replace("\\", "");
                    break;
                case 3:// Address
                    address = word;// strLowAddress = words[i];
                    break;
                case 4:// Full adr
                    postAddr = name;
                    break;
                case 5:// Chief
                    chief = word;
                    break;
                case 6:// account
                    account = word;
                    break;
                case 7:// Bank
                    bank = word;
                    break;
                case 8:// Phones
                    phones = word.length() > 0 ? GlobalProc.normalizePhones(word)
                            : "";
                    break;
            }
            i++;
        }

        return FirmsMetaData.insertQuery(code, name, inn, address, postAddr,
                chief, account, bank, phones);
    }

    private String parseLineMatrix(String data, String codeAgent, int sortOrder) {
        if (data.length() < 9)
            return null;

        String[] arr = data.split(";");

        String client = "";
        String goods = "";
        float price = 0f;

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0:// код менеджера
                    if (!word.equalsIgnoreCase(codeAgent)) {
                        return null;
                    }
                    break;
                case 1:// Код клиента
                    client = word;
                    break;
                case 2:// Код товара
                    goods = word;
                    break;
                case 3:// Цена
                    price = GlobalProc.parseFloat(word);
                    break;
            }
            i++;
        }

        return MatrixMetaData.insertQuery(client, goods, price, sortOrder);
    }

    private void parseLineSMTP(String data, String codeAgent) {
        if (data == null || data.length() < 20)
            return;

        String[] arr = data.split(";");
        int i = 0;

        for (String word : arr) {
            switch (i) {
                case 0:// Code
//                    if (!word.equalsIgnoreCase(codeAgent)) {
//                        // values = null;
//                        return;
//                    }
                    break;
                case 1:// SMTP Server
                    GlobalProc.setPref(context, R.string.pref_smtp_server, word);
                    break;
                case 2:// To
                    GlobalProc.setPref(context, R.string.pref_smtp_to, word);
                    break;
                case 3:// Login
                    GlobalProc.setPref(context, R.string.pref_smtp_login,
                            word);
                    GlobalProc
                            .setPref(context, R.string.pref_smtp_from, word);
                    break;
                case 4: {// Password
                    String tmp = GlobalProc.deCode(word);
                    GlobalProc.setPref(context, R.string.pref_smtp_password,
                            tmp);
                    break;
                }
                case 5:// FTPServer
                    GlobalProc.setPref(context, R.string.pref_ftp_server,
                            word);
                    break;
                case 6:// FTPServer alternate
                    GlobalProc.setPref(context, R.string.pref_ftp_serverA,
                            word);
                    break;
                case 7:// FTPLogin
                    GlobalProc.setPref(context, R.string.pref_ftp_login, word);
                    break;
                case 8: {// FTPPassword
                    String tmp = GlobalProc.deCode(word);
                    GlobalProc.setPref(context, R.string.pref_ftp_password,
                            tmp);
                    break;
                }
                case 9:// SMTPPort
                    int port = GlobalProc.parseInt(word, 25);
                    GlobalProc.setPref(context, R.string.pref_smtp_port, port);
                    break;
                case 10:// FTPPort

                    break;
                case 11:// PhotoReport sign
                    int sign = GlobalProc.parseInt(word, 0);
                    GlobalProc.setPref(context, R.string.pref_photoWithOrder,
                            sign == 1);
                    break;
                case 12:// Photos count
                    int count = GlobalProc.parseInt(word, 1);
                    GlobalProc.setPref(context, R.string.pref_photosCount,
                            count);
                    break;
                case 13://
                    GlobalProc.setPref(context, R.string.pref_autoOrder, word);
                    Log.d(TAG, "pref_autoOrder: " + word);
                    break;
                case 14://
                    int sec = GlobalProc.parseInt(word, 600);
                    GlobalProc.setPref(context,
                            R.string.pref_locationUpdatePeriod, sec);
                    break;
            }// switch

            i++;
        }// for
    }

    private String parseLineMustList(String data, String codeAgent) {
        if (data.length() < 6)
            return null;

        String[] arr = data.split(";");

        if (arr == null || arr.length < 3
                || !arr[0].equalsIgnoreCase(codeAgent)) {
            return null;
        }

        return MustListMetaData.insertQuery(arr[0], arr[1], arr[2]);
    }

    private String parseLineDiscounts(String data, String codeAgent) {
        if (data.length() < 13)
            return null;

        String[] arr = data.split(";");

        String client = "";
        String goods = "";
        byte isGroup = 0;
        float discount = 0f;

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0:// код менеджера
                    if (!word.contains(codeAgent)) {
                        return null;
                    }
                    break;
                case 1:// Клиент
                    client = word;
                    break;
                case 2: // Товар
                    goods = word;
                    break;
                case 3:// Признак группы
                    isGroup = (byte) GlobalProc.parseInt(word);
                    break;
                case 4:// Скидка
                    discount = (float) GlobalProc.parseFloat(word);
                    break;
            }
            i++;
        }

        return DiscountsMetaData.insertQuery(codeAgent, client, goods, isGroup, discount);
    }

    private String parseLineTransit(String line) {
        if (line.length() < 9)
            return null;

        String[] arr = line.split(";");

        String code = "";
        String date = "";
        String amount = "0";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0: {// Код товара
                    code = word;
                    break;
                }
                case 1: {// Дата поступления
                    date = TimeFormatter.convertDate1CToSQL(word);
                    break;
                }
                case 2: {// Количество
                    amount = word;
                    break;
                }
            }
            i++;
        }

        return TransitMetaData.insertQuery(code, date, amount);
    }

    private String parseLineManagers(String line) {
        if (line.length() < 9)
            return null;

        String[] arr = line.split(";");

        String code = "";
        String fio = "";
        String phone = "";
        String email = "";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0: // Код сотрудника
                    code = word;
                    break;
                case 1: // фио
                    fio = word;
                    break;
                case 2: // номер телефона
                    phone = word;
                    break;
                case 3: // email
                    email = word;
                    break;
            }
            i++;
        }
        if (App.isMerch()) {
            return ManagerMerchMetaData.insertQuery(code, fio, phone, email);
        } else {
            return ManagerMetaData.insertQuery(code, fio, phone, email);
        }

    }


    private String parseLineOstatky(String line) {
        String[] arr = line.split(";");

        String code = "";
        String sclad = "";
        String remainder = "";
        String reserve = "";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 1: // Код продукта
                    code = word;
                    break;
                case 2: // № склада
                    if (word.isEmpty()) {
                        sclad = "<Не задан>";
                    } else {
                        sclad = word;
                    }
                    break;
                case 3: // Остаток на складе
                    remainder = word;
                    break;
                case 4: // Резерв
                    reserve = word;
                    break;
            }
            i++;
        }
        return RemainderMetaData.insertQuery(code, sclad, remainder, reserve);
    }

    // private String parseLineLastSales(String data, String codeAgent) {
    // if (data.length() < 14)
    // return null;
    // String[] arr = data.split(";");
    //
    // String client = "";
    // String goods = "";
    // String info = "";
    //
    // int i = 0;
    // for (String word : arr) {
    // switch (i) {
    // case 0:// код менеджера
    // if (!word.equalsIgnoreCase(codeAgent)) {
    // return null;
    // }
    // break;
    // case 1:// Код клиента
    // client = word;
    // break;
    // case 2:// Код товара
    // goods = word;
    // break;
    // case 3:// Произвольная информация
    // info = word;
    // break;
    // }
    // i++;
    // }
    //
    // return LastSalesMetaData.insertQuery(client, goods, info, 0);
    // }

    /**
     * Обработка строки из файла с исключениями
     *
     * @param line строка файла
     * @param
     * @return SQL-запрос
     */
    private String parseLineExcluded(String line) {
        if (line.length() < 10)
            return null;
        String[] arr = line.split(";");

        String name = "";
        String goods = "";

        int i = 0;
        for (String word : arr) {
            switch (i) {
                case 0:// Название подгруппы
                    name = word;
                    if (!mLastName.equalsIgnoreCase(word)) {
                        ++mIndex;
                        mLastName = word;
                    }
                    break;
                case 1:// Код товара
                    goods = word;
                    break;
            }
            i++;
        }

        return ExcludedMetaData.insertQuery(name, goods, mIndex);
    }

    private String parseLineAutoOrder(String data, String codeAgent) {
        if (data.length() < 14)
            return null;
        String[] arr = data.split(";");

        String client = "";
        String goods = "";
        String count = "0";
        String mustList = "0";
        String newbee = "0";

        int i = 0;
        for (String word : arr) {
            if (word.length() < 1)
                return null;

            switch (i) {
                case 0:// код менеджера
                    if (!word.equalsIgnoreCase(codeAgent)) {
                        return null;
                    }
                    break;
                case 1:// Код клиента
                    client = word;
                    break;
                case 2:// Код товара
                    goods = word;
                    break;
                case 3:// Количество
                    count = word.replace(',', '.');
                    break;
                case 4:// признак маст-листа
                    mustList = word;// (byte)GlobalProc.parseInt(word);
                    break;
                case 5:// признак новинки
                    newbee = word;// (byte)GlobalProc.parseInt(word);
                    break;
            }
            i++;
        }

        return AutoorderMetaData.insertQuery(codeAgent, client, goods, count,
                mustList, newbee);
    }

}
