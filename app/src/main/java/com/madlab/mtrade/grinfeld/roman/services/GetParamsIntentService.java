package com.madlab.mtrade.grinfeld.roman.services;


import java.io.IOException;
import java.util.Locale;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.LocaleDisplayNames;
import android.preference.PreferenceManager;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.Security;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.LocationMetadata;

import org.json.JSONObject;

import okhttp3.Response;


public class GetParamsIntentService extends IntentService {

    public static final String EXTRA_TYPE_SERVER = "type_server";
    private static final String TAG = "#GetParamsIntentService";
    private final String PROC_NAME = "Авторизация";

    public static final String EXTRA_AGENT = "AgentCode";

    public static final String EXTRA_STATUS = "TaskStatus";
    public static final String EXTRA_MESSAGE = "ErrorMessage";

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.get.parameters.for.work";

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;

    private byte status = STATUS_OK;
    private String mResult = "";

    //Сведения для подключения
    private DalimoClient mClient;

    private String mCodeAgent;

    public GetParamsIntentService() {
        super(BROADCAST_ACTION);
    }


    @Override
    protected void onHandleIntent(Intent extras) {
        //В экстрас нужно получать потому что можем передавать из диалога
        mCodeAgent = extras.getStringExtra(EXTRA_AGENT);
        String connectionServer = extras.getStringExtra(EXTRA_TYPE_SERVER);
        Log.d(TAG, "connectionServer: " + connectionServer);
        StringBuilder data = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;",
                Const.COMMAND, mCodeAgent, PROC_NAME, mCodeAgent));
        Log.d(TAG, String.valueOf(data));

        if (connectionServer.equals("1")) {
            mClient = new DalimoClient(Credentials.load(getApplicationContext()));
            if (mClient.connect()) {
                Log.d(TAG, "send: " + data + Const.END_MESSAGE);
                mClient.send(data + Const.END_MESSAGE);

                mResult = mClient.receive();
                Log.d(TAG, "result app: " + mResult);
                status = STATUS_OK;
            } else {
                status = STATUS_ERROR;
                mResult = mClient.LastError();
            }
            mClient.disconnect();
        } else {
            try {
                Response response = OkHttpClientBuilder.buildCall(PROC_NAME, mCodeAgent, App.getRegion(MyApp.getContext())).execute();
                if (response.isSuccessful()) {
                    String strResponse = response.body().string();
                    Log.d(TAG, "result iis: " + strResponse);
                    JSONObject jsonObject = new JSONObject(strResponse);
                    mResult = jsonObject.getString("data");
                    status = STATUS_OK;
                } else {
                    status = STATUS_ERROR;
                }
            } catch (Exception e) {
                e.printStackTrace();
                mClient = new DalimoClient(Credentials.load(getApplicationContext()));
                if (mClient.connect()) {
                    Log.d(TAG, "send: " + data + Const.END_MESSAGE);
                    mClient.send(data + Const.END_MESSAGE);

                    mResult = mClient.receive();
                    Log.d(TAG, "result app: " + mResult);
                    status = STATUS_OK;
                } else {
                    status = STATUS_ERROR;
                    mResult = mClient.LastError();
                }
                mClient.disconnect();
            }
        }

        if (mResult.contains(DalimoClient.ERROR)) {
            status = STATUS_ERROR;
        } else {
            String[] params = mResult.split(";");

            if (0 < params.length && params.length > 2) {
                String sectionEC = getString(R.string.pref_exclusive);
                String sectionTimeZone = getString(R.string.pref_timezone);
                String sectionFIO = getString(R.string.pref_nameManager);

                //Парсим
                String exclusive = params[0];
                int tz = GlobalProc.parseInt(params[1]);
                String fio = params[2];

                //Сохраняем
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();

                editor.putString(sectionEC, exclusive.trim());
                editor.putInt(sectionTimeZone, tz);
                editor.putString(sectionFIO, fio);

                editor.apply();
            } else {
                status = STATUS_ERROR;
                mResult = "Недостаточно параметров";
            }
        }

        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, status);
        result.putExtra(EXTRA_MESSAGE, mResult);
        sendBroadcast(result);
    }

}
