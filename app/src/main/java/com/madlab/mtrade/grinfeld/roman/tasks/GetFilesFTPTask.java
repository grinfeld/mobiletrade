package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.madlab.mtrade.grinfeld.roman.connectivity.FTP;
import com.madlab.mtrade.grinfeld.roman.iface.IGetFilesComplete;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

public class GetFilesFTPTask extends AsyncTask<String, Integer, Boolean> {
    // private final String TAG = "!->GetFilesFTP";

    private final static String MES_SUCCESS = "Получение файлов успешно завершено";

    private ProgressDialog progress;
    private Context baseContext;
    private IGetFilesComplete mHandler;
    private String mUnswer;

    // Credentials
    private String server;
    private String serverAlternate;
    private String login;
    private String password;

    // dir on the ftp server
    private String dir;

    // path to save
    private String destPath;

    private boolean running = true;

    public String getMessage() {
        return mUnswer;
    }

    public String getDir() {
        return dir;
    }

    /**
     * Устанвливает параметры подключения к ФТП-серверу
     *
     * @param server1
     * @param server2
     * @param login
     * @param password
     */
    public void setCredentials(String server1, String server2, String login,
                               String password) {
        this.server = server1;
        this.serverAlternate = server2;
        this.login = login;
        this.password = password;
    }

    /**
     * После вызова конструктора ОБЯЗАТЕЛЬНО нужно вызвать метод setCredentials
     * для установки параметров подключения к ФТП-серверу
     *
     */
    public GetFilesFTPTask(Context data, String dir, String pathToSave,
                           IGetFilesComplete handler) {
        baseContext = data;
        this.dir = dir;
        destPath = pathToSave;
        mHandler = handler;
    }

    @Override
    protected void onPreExecute() {
        showProgress();
    }

    @Override
    protected Boolean doInBackground(String... paths) {
        FTP ftp = null;
        boolean wasError = false;
        try {
            ftp = new FTP(server, login, password);

            if (!ftp.connect()) {
                ftp.disconnect();
                if (running) {
                    ftp = new FTP(serverAlternate, login, password);
                    if (!ftp.connect()) {
                        ftp.disconnect();
                        mUnswer = "Не удалось подключиться к серверу";
                        return false;
                    }
                }
            }

            if (running && !ftp.login()) {
                ftp.disconnect();
                mUnswer = "Не удалось авторизоваться на сервере";
                return false;
            }

            if (running && !ftp.changeWorkingDirectory(dir)) {
                ftp.disconnect();
                return false;
            }

            if (progress != null) {
                progress.setMax(paths.length);
            }
            int mProgress = 0;

            if (!running)
                return null;

            for (String current : paths) {
                OutputStream os = null;

                try {
                    os = new FileOutputStream(destPath + File.separator
                            + current);

                    if (!ftp.retrieveFile(current, os)) {
                        Log.e("!->retrieveFile", "Ошибка при получении файла");
                        wasError = true;
                    }
                } catch (Exception e) {
                    Log.e("!->retrieveFile", e.getMessage());
                    wasError = true;
                    continue;
                } finally {
                    if (os != null)
                        os.close();
                }
                mProgress++;
                publishProgress(mProgress);

            }// for
        } catch (Exception e) {
            mUnswer = e.toString();
            wasError = true;
        }

        if (ftp != null) {
            ftp.disconnect();
        }

        mUnswer = wasError ? "Ошибка при получении одного или нескольких файлов"
                : MES_SUCCESS;
        return !wasError;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (progress != null)
            progress.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dismiss();

        if (mHandler != null) {
            mHandler.onTaskComplete(this);
        }
    }

    private void showProgress() {
        progress = new ProgressDialog(baseContext);
        progress.setMessage("Дождитесь окончания загрузки");
        progress.setCancelable(true);
        progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // TODO Auto-generated method stub
                cancel(true);
            }
        });
        progress.show();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        running = false;
        mUnswer = "Операция прервана пользователем";
        Log.i("", mUnswer);
        if (mHandler != null) {
            mHandler.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (progress != null) {
            progress.hide();
            progress.dismiss();
        }
    }

    public void unLink() {
        dismiss();
        baseContext = null;
    }

    public void link(Context context) {
        baseContext = context;
        showProgress();
    }
}
