package com.madlab.mtrade.grinfeld.roman;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Created by GrinfeldRA on 16.01.2018.
 */

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Security.createDeviceCode(this, false);
        initDeviceCodeFields();
        initUnswerFields();
        View reg_btAccept = findViewById(R.id.reg_btAccept);
        reg_btAccept.setOnClickListener(this);
        findViewById(R.id.img_btn_copy_code_device).setOnClickListener(this);
        findViewById(R.id.code_device_label).setOnClickListener(this);
    }

    private void initUnswerFields() {
        et1 = findViewById(R.id.reg_unswer1);
        et2 = findViewById(R.id.reg_unswer2);
        et3 = findViewById(R.id.reg_unswer3);
        et4 = findViewById(R.id.reg_unswer4);

//        String unswerCode = App.get(getApplicationContext()).getUnswerCode();
        if (et1 != null) {
//            if (unswerCode != null && unswerCode.length() > 4){
//                et1.setText(unswerCode.substring(0, 5));
//            }
            et1.setNextFocusRightId(R.id.reg_unswer2);
            et1.addTextChangedListener(new TextWatcherE(et1));
        }
        if (et2 != null) {
//            if (unswerCode != null && unswerCode.length() > 9){
//                et2.setText(unswerCode.substring(5, 10));
//            }
            et2.setNextFocusRightId(R.id.reg_unswer3);
            et2.addTextChangedListener(new TextWatcherE(et2));
        }
        if (et3 != null) {
//            if (unswerCode != null && unswerCode.length() > 14){
//                et3.setText(unswerCode.substring(10, 15));
//            }
            et3.setNextFocusRightId(R.id.reg_unswer4);
            et3.addTextChangedListener(new TextWatcherE(et3));
        }
        if (et4 != null) {
//            if (unswerCode != null && unswerCode.length() > 19){
//                et4.setText(unswerCode.substring(15));
//            }
            et4.setNextFocusRightId(R.id.reg_btAccept);
            et4.addTextChangedListener(new TextWatcherE(et4));
        }
    }

    private void initDeviceCodeFields() {
        EditText dev1 = findViewById(R.id.reg_first);
        EditText dev2 = findViewById(R.id.reg_second);
        EditText dev3 = findViewById(R.id.reg_third);
        EditText dev4 = findViewById(R.id.reg_fourth);
        String deviceCode = Security.getDeviceCode();
        if (deviceCode.length() < 15)
            return;
        dev1.setText(deviceCode.substring(0, 5));
        dev2.setText(deviceCode.substring(5, 10));
        dev3.setText(deviceCode.substring(10, 15));
        dev4.setText(deviceCode.substring(15));
    }

    private boolean checkRegistration() {
        String code = GlobalProc.getPref(getApplicationContext(), R.string.pref_unswerCode);
        return code != null && code.equalsIgnoreCase(Security.getEtalonCode());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reg_btAccept:
                String resString = "";
                if (et1 != null) {
                    resString += et1.getText().toString();
                    resString += et2.getText().toString();
                    resString += et3.getText().toString();
                    resString += et4.getText().toString();

                }
                GlobalProc.setPref(getApplicationContext(),
                        R.string.pref_unswerCode, resString);
                if (checkRegistration()) {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                } else {
                    GlobalProc.mToast(this, "Неверный код подтверждения");
                }
                break;
            case R.id.img_btn_copy_code_device:
                if (copyToClipboard(this, Security.getDeviceCode())) {
                    Toast.makeText(this, "Код устройства скопирован в буфер", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.code_device_label:
                if (copyToClipboard(this, Security.getDeviceCode())) {
                    Toast.makeText(this, "Код устройства скопирован в буфер", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    public boolean copyToClipboard(Context context, String text) {
        try {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
                        .getSystemService(CLIPBOARD_SERVICE);
                if (clipboard != null) {
                    clipboard.setText(text);
                }
            } else {
                android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
                android.content.ClipData clip = android.content.ClipData.newPlainText("Код устройства скопирован в буфер", text);
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private class TextWatcherE implements TextWatcher {

        private EditText mET;

        TextWatcherE(EditText et) {
            super();
            mET = et;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 5) {
                if (mET != null) {
                    View v = mET.focusSearch(View.FOCUS_RIGHT);
                    if (v != null)
                        v.requestFocus();
                }
            } else if (s.toString().length() == 0) {
                View v = mET.focusSearch(View.FOCUS_LEFT);
                if (v != null) {
                    v.requestFocus();
                    EditText editText = (EditText) v;
                    editText.setSelection(editText.getText().toString().length());
                }
            }
        }
    }
}
