package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.CamActivity;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.GalleryAdapter;
import com.madlab.mtrade.grinfeld.roman.adapters.JournalAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaymentsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Document;
import com.madlab.mtrade.grinfeld.roman.entity.Firm;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.PaybackPhoto;
import com.madlab.mtrade.grinfeld.roman.entity.Payment;
import com.madlab.mtrade.grinfeld.roman.entity.PaymentTable;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.entity.request.PaymentRequest;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveMonitoringComplete;
import com.madlab.mtrade.grinfeld.roman.services.ProgressPhotoIntentService;
import com.madlab.mtrade.grinfeld.roman.services.UploadFileIntentService;
import com.madlab.mtrade.grinfeld.roman.services.AddPhotoIntentService;
import com.madlab.mtrade.grinfeld.roman.tasks.DocumentsReserver;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservPaymentTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservReturnTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReservOrderTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveMonitoring;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * Created by GrinfeldRA on 29.11.2017.
 */

public class JournalFragment extends Fragment implements ISetReservComplete, View.OnClickListener, AdapterView.OnItemClickListener, ISetReserveMonitoringComplete {


    private static final String TAG = "#JournalFragment";
    public final static int IDD_NEW_ORDER = 10;
    public final static int IDD_NEW_RETURN = 11;
    public final static int IDD_NEW_PAYMENT = 12;
    public final static int IDD_EDIT_ORDER = 15;
    public final static int IDD_EDIT_RETURN = 16;
    public final static int IDD_NEW_PHOTO = 17;

    private final static byte IDD_LOCATION_SETTINGS = 1;

    private ArrayList<Document> mDocumentList;
    private JournalAdapter mAdapter;
    private ListView listView;
    TextView tvLeft;
    ImageView weight;
    TextView tvCenter;
    TextView tvRight;
    static int positionViewPager;
    private int position;
    private DocumentsReserver<Visit> mDocumentReserverThread;
    private static final int MESSAGE_UPDATE_LIST = 0;
    private ProgressBar progressBar;
    public static boolean initFlag = false;
    private FragmentManager fragmentManager;
    private ArrayList<String> images = new ArrayList<>();
    private RecyclerView recyclerView;
    private GalleryAdapter galleryAdapter;
    private static FloatingActionButton floatingActionButton, floatingActionButton2;
    private static Drawable ic_send, ic_new;
    private BroadcastReceiver uploadTask;
    private Context context;
    private TableLayout tableLayout;
    private LinearLayout journal_base;
    public static byte APP_SERVER = 1;
    public static byte IIS_SERVER = 2;
    private ProgressBar progressPhoto;
    private Handler handler;

    Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_UPDATE_LIST:
                    if (mDocumentReserverThread != null && mDocumentReserverThread.getItemsCount() < 1) {
                        waitCursor(false);
                        fillListAndBasement(positionViewPager);
                        return true;
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    });

    @Override
    public void onResume() {
        super.onResume();
        listener.onPageSelected(positionViewPager);
    }

    public static ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            positionViewPager = position;
            Log.d(TAG, "onPageSelected: " + position);
            initFlag = true;
            if (floatingActionButton != null) {
//                if (App.isMerch()) {
//                    if (position == 1) {
//                        floatingActionButton.setVisibility(View.GONE);
//                    } else {
//                        floatingActionButton.setVisibility(View.VISIBLE);
//                    }
//                }
                int i = App.isMerch() ? 3 : 4;
                if (position == i) {
                    floatingActionButton.setImageResource(android.R.drawable.ic_menu_send);
                    floatingActionButton2.setVisibility(View.VISIBLE);
                } else {
                    floatingActionButton.setImageResource(R.mipmap.add_new_white);
                    floatingActionButton2.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragmentManager = getActivity().getFragmentManager();
        } else {
            fragmentManager = getFragmentManager();
        }
        Log.d(TAG, "onCreateView " + initFlag);
        context = getActivity();
        handler = new Handler(Looper.getMainLooper());
        ic_new = ContextCompat.getDrawable(getActivity(), R.mipmap.add_new_white);
        ic_send = ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_send);
        fragmentManager.addOnBackStackChangedListener(getListener());
        position = getArguments().getInt(WorkModeFragment.POSITION);
        floatingActionButton = getActivity().findViewById(R.id.fab);
        floatingActionButton2 = getActivity().findViewById(R.id.fab2);
        int i = App.isMerch() ? 3 : 4;
        if (position == i) {
            if (floatingActionButton2 != null) {
                floatingActionButton2.setOnClickListener(this);
            }
            setupReceiver();
            View photoView = inflater.inflate(R.layout.fragment_photo, container, false);
            recyclerView = photoView.findViewById(R.id.recycler_view);
            progressPhoto = photoView.findViewById(R.id.progress_bar_photo);
            images = getListImage();
            galleryAdapter = new GalleryAdapter(getActivity(), images);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(galleryAdapter);
            recyclerView.addOnItemTouchListener(new GalleryAdapter.RecyclerTouchListener(getActivity(), recyclerView, new GalleryAdapter.ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("images", images);
                    bundle.putInt("position", position);
                    android.support.v4.app.FragmentTransaction ft = ((AppCompatActivity) getActivity()).getSupportFragmentManager().beginTransaction();
                    SlideshowDialogFragment newFragment = SlideshowDialogFragment.newInstance();
                    newFragment.setArguments(bundle);
                    newFragment.show(ft, "slideshow");
                }

                @Override
                public void onLongClick(View view, int position) {
                    new AlertDialog.Builder(context)
                            .setIcon(R.mipmap.main_icon)
                            .setTitle("Удалить фотографию?")
                            .setMessage("Вы уверены что хотите удалить фотографию?")
                            .setNegativeButton("Нет", null)
                            .setPositiveButton("Да", (dialogInterface, i) -> {
                                File file = new File(images.get(position));
                                if (file.exists()) {
                                    file.delete();
                                }
                                images.remove(position);
                                recyclerView.getAdapter().notifyItemRemoved(position);
                                recyclerView.getAdapter().notifyItemRangeChanged(position, images.size());
                            }).show();
                }
            }));
            return photoView;
        } else {
            View v = inflater.inflate(R.layout.fragment_journal, container, false);
            if (floatingActionButton != null) {
                floatingActionButton.setImageResource(R.mipmap.add_new_white);
                floatingActionButton.setOnClickListener(this);
            }
            listView = v.findViewById(android.R.id.list);
            tvLeft = v.findViewById(R.id.journal_tvTextLeft);
            weight = v.findViewById(R.id.journal_imageWeight);
            tvCenter = v.findViewById(R.id.journal_tvTextCenter);
            tvRight = v.findViewById(R.id.journal_tvTextRight);
            tableLayout = v.findViewById(R.id.journal_baseTable);
            journal_base = v.findViewById(R.id.journal_base);
            listView.setOnItemClickListener(this);
            fillListAndBasement(position);
            progressBar = v.findViewById(R.id.progress_bar_journal);
            return v;
        }
    }


    private ArrayList<String> getListImage() {
        ArrayList<String> images = new ArrayList<>();
        File folder = new File("/storage/emulated/0/IMPEXP/Photos/");
        File[] files = folder.listFiles();
        if (files != null)
            for (File file : files) {
                if (file.isFile()) {
                    images.add(file.getAbsolutePath());
                }
            }
        return images;
    }


    private FragmentManager.OnBackStackChangedListener getListener() {
        return () -> {
            FragmentManager manager = getFragmentManager();
            if (manager != null) {
                Fragment fragment = manager.findFragmentById(R.id.contentMain);
                if (fragment instanceof WorkModeFragment) {
                    Log.d(TAG, "onBackStackChanged: " + String.valueOf(position));
                    fillListAndBasement(position);
                    //fixme
                    //fixme
                    //fixme
                    //fixme
                    //fixme
                }
            }
        };
    }

    public Dialog loadBackupDlg(final Activity ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(ctx.getString(R.string.cap_confirm));
        builder.setMessage(ctx.getString(R.string.mes_load_order_from_backup));
        builder.setPositiveButton(ctx.getString(R.string.bt_yes),
                (dialog, which) -> {
                    Order.loadUnsavedOrder(ctx);
                    startNewOrderDoc();
                });
        builder.setNegativeButton(ctx.getString(R.string.bt_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Order.deleteUnsavedOrder(ctx.getApplicationContext());
                        Order.newOrder(App.get(ctx).getCodeAgent());
                        startNewOrderDoc();
                    }
                });
        builder.setCancelable(true);
        return builder.create();
    }


    private void startNewOrderDoc() {
        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
        FragmentTransaction fragmentTransaction;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), IDD_NEW_ORDER);
            fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        } else {
            fragment.setTargetFragment(this, IDD_NEW_ORDER);
            fragmentTransaction = getFragmentManager().beginTransaction();
        }
        fragmentTransaction.addToBackStack(null).replace(R.id.contentMain, fragment).commit();
    }

    private void checkExistOrderDoc() {
        if (Order.isUnsavedOrderExists(getActivity())) {
            loadBackupDlg(getActivity()).show();
        } else {
            Order.newOrder(App.get(getActivity()).getCodeAgent());
            startNewOrderDoc();
        }
    }

    private void startNewPaymentDoc() {
        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), IDD_NEW_PAYMENT);
        } else {
            fragment.setTargetFragment(this, IDD_NEW_PAYMENT);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null).replace(R.id.contentMain, fragment).commit();
    }

    private void startNewReturnDoc() {
        Returns.newReturn(App.get(getActivity()).getCodeAgent(), null);
        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), IDD_NEW_RETURN);
        } else {
            fragment.setTargetFragment(this, IDD_NEW_RETURN);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null).replace(R.id.contentMain, fragment).commit();
    }

    private void startNewVisitDoc() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null)
                .replace(R.id.contentMain, new ClientViewPagerContainer())
                .commit();
    }

    private void startNewPhoto() {
        VisitSelectFragment fragment = new VisitSelectFragment();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), IDD_NEW_PHOTO);
        } else {
            fragment.setTargetFragment(this, IDD_NEW_PHOTO);
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null).replace(R.id.contentMain, fragment).commit();
    }

    private void fillListAndBasement(int position) {
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            SQLiteDatabase db = app.getDB();
            switch (position) {
                case 0:
                    mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Visit);
                    fillBasement(db, Const.DOCUMENTS_TYPE.Visit);
                    setAdapter(Const.DOCUMENTS_TYPE.Visit);
                    break;
                case 1:
                    mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Order);
                    fillBasement(db, Const.DOCUMENTS_TYPE.Order);
                    setAdapter(Const.DOCUMENTS_TYPE.Order);
                    break;
                case 2:
                    if (App.isMerch()) {
                        mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Payment);
                        fillBasement(db, Const.DOCUMENTS_TYPE.Payment);
                        setAdapter(Const.DOCUMENTS_TYPE.Payment);
                    } else {
                        mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Return);
                        fillBasement(db, Const.DOCUMENTS_TYPE.Return);
                        setAdapter(Const.DOCUMENTS_TYPE.Return);
                    }
                    break;
                case 3:
                    if (!App.isMerch()) {
                        mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Payment);
                        fillBasement(db, Const.DOCUMENTS_TYPE.Payment);
                        setAdapter(Const.DOCUMENTS_TYPE.Payment);
                    }
                    break;
            }

        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final Document current = mAdapter.getItem(i);
        switch (positionViewPager) {
            case 0:
                dlgActionsVisit(current).show();
                break;
            case 1:
                if (App.isMerch()) {
                    dlgActionsVisit(current).show();
                } else {
                    dlgActionsOrders(current).show();
                }
                break;
            case 2:
                dlgActionsReturns(current).show();
                break;
            case 3:
                dlgActionsPayments(current).show();
                break;
        }
        Log.d(TAG, String.valueOf(current + " " + positionViewPager));
    }


    private Short[] getItemsForReserv(List<Document> document) {
        ArrayList<Short> itemsForReserv = new ArrayList<>();
        for (Document doc : document) {
            if (doc.isHighLight == Document.NORMAL) {
                itemsForReserv.add(doc.getID());
            }
        }
        Short[] items = new Short[itemsForReserv.size()];
        if (itemsForReserv.size() > 0) {
            items = new Short[itemsForReserv.size()];
            return itemsForReserv.toArray(items);
        }
        return items;
    }

    protected Dialog dlgActionsOrders(final Document currentDoc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.cap_choose_action));
        builder.setItems(R.array.journal_actions,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0://setReserve
                                if (currentDoc != null) {
                                    setReserv(new Short[]{currentDoc.getID()});
                                }
                                break;
                            case 1:// setReserveAll
                                setReserveAll(Const.DOCUMENTS_TYPE.Order);
                                break;
                            case 2:
                                checkExistOrderDoc();
                                break;
                            case 3:
                                editDoc(currentDoc, IDD_EDIT_ORDER);
                                break;
                            case 4:
                                deleteDoc(currentDoc);
                                break;
                            case 5:
                                copyOrder(currentDoc);
                                break;
                        }
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }


    private void copyOrder(Document document) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            Order orderData = Order.load(db, document.getID());
            if (orderData != null) {
                orderData.setNumOrder(Order.UNCONFIRMED_ORDER_NUMBER);
                orderData.mReserved = false;
                orderData.wasAutoOrder(Order.AUTOORDER_NOT_USED);
                orderData.wasCheck(false);
                orderData.firmFK((byte) 0);
                orderData.mCodeAgent = App.get(getActivity()).getCodeAgent();
                Order.setOrder(orderData);
            }

            FragmentManager fragmentManager = getActivity().getFragmentManager();
            ClientViewPagerContainer fragment = new ClientViewPagerContainer();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fragment.setTargetFragment(getParentFragment(), IDD_NEW_ORDER);
            } else {
                fragment.setTargetFragment(this, IDD_NEW_ORDER);
            }
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.contentMain, fragment)
                    .commit();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, e.toString());
        }
    }

    private void editDoc(Document doc, int type) {
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            SQLiteDatabase db = app.getDB();
            switch (type) {
                case IDD_EDIT_ORDER:
                    Order orderData = Order.load(db, doc.getID());
                    if (orderData != null) {
                        orderData.mCodeAgent = App.get(getActivity()).getCodeAgent();
                        Order.setOrder(orderData);
                        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            fragment.setTargetFragment(getParentFragment(), IDD_EDIT_ORDER);
                        } else {
                            fragment.setTargetFragment(this, IDD_EDIT_ORDER);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.addToBackStack(null)
                                .replace(R.id.contentMain, fragment)
                                .commit();
                    } else {
                        GlobalProc.mToast(context, getString(R.string.err_load_order));
                    }
                    break;
                case IDD_EDIT_RETURN:
                    Returns returnData = Returns.load(db, doc.getID());
                    if (returnData != null) {
                        returnData.setCodeAgent(App.get(getActivity()).getCodeAgent());
                        Returns.setReturn(returnData);
                        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            fragment.setTargetFragment(getParentFragment(), IDD_EDIT_RETURN);
                        } else {
                            fragment.setTargetFragment(this, IDD_EDIT_RETURN);
                        }
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.addToBackStack(null)
                                .replace(R.id.contentMain, fragment)
                                .commit();
                    } else {
                        GlobalProc.mToast(context, getString(R.string.err_load_return));
                    }
                    break;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void setReservMonitoring(Short[] items) {
        SetReserveMonitoring mSetReservTask = new SetReserveMonitoring(context, this);
        mSetReservTask.execute(items);
        waitCursor(true);
    }

    private void setReserv(Short[] items) {
        SetReservOrderTask mSetReservTask = new SetReservOrderTask(getActivity(), this);
        mSetReservTask.execute(items);
        waitCursor(true);
    }

    private void setReservReturn(Short[] items) {
        SetReservReturnTask task = new SetReservReturnTask(getActivity(), this);
        task.execute(items);
        waitCursor(true);
    }

    private void setReserveAll(Const.DOCUMENTS_TYPE type) {
        MyApp app = (MyApp) getActivity().getApplication();
        SQLiteDatabase db = app.getDB();
        ArrayList<Document> documents = Document.loadList(db, type);
        Short[] itemsForReserv = getItemsForReserv(documents);
        switch (type) {
            case Order:
                if (App.isMerch()) {
                    setReservMonitoring(itemsForReserv);
                } else {
                    setReserv(itemsForReserv);
                }
                break;
            case Return:
                setReservReturn(itemsForReserv);
                break;
        }

    }

    protected Dialog dlgActionsReturns(final Document currentDoc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.cap_choose_action));
        builder.setItems(R.array.journal_actions,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0: //setReserve
                                if (currentDoc != null) {
                                    setReservReturn(new Short[]{currentDoc.getID()});
                                }
                                break;
                            case 1:// setReserveAll
                                setReserveAll(Const.DOCUMENTS_TYPE.Return);
                                break;
                            case 2:
                                startNewReturnDoc();
                                break;
                            case 3:
                                editDoc(currentDoc, IDD_EDIT_RETURN);
                                break;
                            case 4:
                                deleteDoc(currentDoc);
                                break;
                            case 5://
                                copyReturn(currentDoc);
                                break;
                        }
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }

    private void copyReturn(Document currentDoc) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            Returns returnData = Returns.load(db, currentDoc.getID());
            if (returnData != null) {
                returnData.setNumReturn(Returns.UNCONFIRMED_RETURN_NUMBER);
                returnData.setUuidDoc(UUID.randomUUID());
                returnData.wasSended(false);
                returnData.firmFK((byte) 0);
                returnData.setPhotoIsExist((byte) 0);
                returnData.setCodeAgent(App.get(getActivity()).getCodeAgent());
                Returns.setReturn(returnData);
            }

            FragmentManager fragmentManager = getActivity().getFragmentManager();
            ClientViewPagerContainer fragment = new ClientViewPagerContainer();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fragment.setTargetFragment(getParentFragment(), IDD_NEW_RETURN);
            } else {
                fragment.setTargetFragment(this, IDD_NEW_RETURN);
            }
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.contentMain, fragment)
                    .commit();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, e.toString());
        }
    }


    private void deleteDoc(final Document currentDoc) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            boolean success = false;
            switch (positionViewPager) {
                case 1:
                    success = Order.changeDeleteMark(db, currentDoc.getID());
                    break;
                case 2:
                    success = Returns.changeDeleteMark(db, currentDoc.getID());
                    break;
            }
            if (success) {
                String txt = "";
                if (currentDoc.isHighLight == Document.NORMAL
                        || currentDoc.isHighLight == Document.RESERVED) {
                    currentDoc.isHighLight = Document.DELETED;
                    txt = getString(R.string.mes_mark_del);
                } else if (currentDoc.isHighLight == Document.DELETED) {
                    if (currentDoc.getFieldRight().equalsIgnoreCase("Резерв")) {
                        currentDoc.isHighLight = Document.RESERVED;
                    } else {
                        currentDoc.isHighLight = Document.NORMAL;
                    }
                    txt = getString(R.string.mes_mark_undel);
                }
                GlobalProc.mToast(context, txt);
                notifyAdapter();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    protected Dialog dlgDeleteConfirm(final Document doc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.cap_confirm));
        builder.setMessage(getString(R.string.mes_del_payment_confirm));
        builder.setIcon(R.mipmap.main_icon);
        builder.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deletePay(doc);
            }
        });
        builder.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        return builder.create();
    }

    private void deletePay(Document document) {
        boolean success = true;
        int res;
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            String where = String.format("%s = %d", PaymentsMetaData._ID, document.getID());
            res = db.delete(PaymentsMetaData.TABLE_NAME, where, null);
            Log.i(TAG, String.format("success full deleted %d rows", res));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            success = false;
        }
        String txt;
        if (success) {
            txt = getString(R.string.mes_pay_del_success);
            fillListAndBasement(positionViewPager);
        } else {
            txt = getString(R.string.mes_delete_doc_error);
        }
        GlobalProc.mToast(context, txt);
    }


    protected Dialog dlgActionsPayments(final Document currentDoc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.cap_choose_action));
        builder.setItems(R.array.journal_actions_payment,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Short[] docList = new Short[mDocumentList.size()];
                                for (int i = 0; i < mDocumentList.size(); i++) {
                                    docList[i] = mDocumentList.get(i).getID();
                                }
                                setReservPayment(docList);
                                break;
                            case 1:
                                dlgDeleteConfirm(currentDoc).show();
                                break;
                        }
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }

    private void notifyAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void setReservPayment(Short[] items) {
//        SetReservPaymentTask task = new SetReservPaymentTask(getActivity(), this);
//        task.execute(items);
        try {
            waitCursor(true);
            MyApp app = (MyApp) context.getApplicationContext();
            SQLiteDatabase db = app.getDB();
            JSONArray jsonArray = new JSONArray();
            for (Short current : items) {
                Payment data = Payment.load(db, current);
                PaymentRequest request = new PaymentRequest(data.getCodeAgent(), data.getCodeClient(),
                        data.getDocNum(), TimeFormatter.sdf1C.format(data.getDateDoc()), data.payment());
                Gson gson = new GsonBuilder().create();
                String strJson = gson.toJson(request);

                JSONObject jsonObj = new JSONObject(strJson);
                jsonArray.put(jsonObj);

            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Payments", jsonArray);
            String region = App.getRegion(MyApp.getContext());
            OkHttpClientBuilder.buildCall("СформироватьФайлОплат", jsonObject.toString(), region).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    handler.post(() -> {
                        waitCursor(false);
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (response.isSuccessful()) {
                        handler.post(() -> {
                            waitCursor(false);
                            if (response.message().contains("OK")) {
                                Toast.makeText(context, "Оплаты успешно отправлены", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                            }

                        });

                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected Dialog dlgActionsVisit(final Document currentDoc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.cap_choose_action));
        builder.setItems(R.array.journal_actions_visit,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (currentDoc != null) {
                                    if (App.isMerch() && position != 0) {
                                        setReserveAll(Const.DOCUMENTS_TYPE.Order);
                                    } else {
                                        createReserver();
                                        startAllVisitsReservTask();
                                    }
                                }
                                break;
                            case 1:
                                startNewVisitDoc();
                                break;
                            case 2:
                                MyApp app = (MyApp) getActivity().getApplication();
                                SQLiteDatabase db = app.getDB();
                                Visit visit = Visit.load(db, currentDoc.getID());
                                Intent intent = new Intent(getActivity(), CamActivity.class);
                                intent.putExtra(VisitContainFragment.CLIENT_CODE, visit.client().getCode());
                                intent.putExtra(VisitContainFragment.UUID_KEY, visit.getID().toString());
                                startActivityForResult(intent, IDD_NEW_PHOTO);
                                break;
                        }
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }


    private void fillBasement(SQLiteDatabase db, Const.DOCUMENTS_TYPE typ) {
        if (tvLeft == null || tvCenter == null || tvRight == null)
            return;
        Cursor result = null;
        switch (typ) {
            case Visit:
                weight.setVisibility(View.INVISIBLE);
                tvCenter.setVisibility(View.VISIBLE);

                String sql = String.format(
                        "SELECT Count(%s), SUM(%s), SUM(%s) FROM %s",
                        VisitMetaData._ID, VisitMetaData.FIELD_ORDER_SKU,
                        VisitMetaData.FIELD_ORDER_SUM, VisitMetaData.TABLE_NAME);
                result = db.rawQuery(sql, null);

                if (result.moveToFirst()) {
                    int cnt = result.getInt(0);

                    tvLeft.setText(cnt + " док.");
                    tvCenter.setText(String.format("SKU: %d ", result.getInt(1)));
                    tvRight.setText(GlobalProc.formatMoney(result.getFloat(2)));
                }
                break;
            case Order:
                // Смотрим наличие несохраненной заявки
                boolean unsOrderExists = Order.isUnsavedOrderExists(getActivity());

                weight.setVisibility(View.VISIBLE);
                tvCenter.setVisibility(View.VISIBLE);

                // Select Sum(TotalSum) as tSum From Orders
                result = db.query(OrderMetaData.TABLE_NAME, new String[]{
                                "Count(NumOrder)", "Sum(TotalSum)", "Sum(TotalWeight)"},
                        "NumOrder != 999", null, null, null, null);

                if (result.moveToFirst()) {
                    int cnt = result.getInt(0);//unsOrderExists ? result.getInt(0) - 1 : result.getInt(0);
                    tvLeft.setText(cnt + " док.");
                    tvCenter.setText(GlobalProc.formatWeight(result.getFloat(2))
                            + Const.POSTFIX_WEIGHT);
                    tvRight.setText(GlobalProc.formatMoney(result.getFloat(1)));
                }

                break;
            case Return:
                weight.setVisibility(View.VISIBLE);
                tvCenter.setVisibility(View.VISIBLE);

                // Select Sum(TotalSum) as tSum From Orders
                result = db.query(ReturnMetaData.TABLE_NAME, new String[]{
                                "Count(NumReturn)", "Sum(TotalSum)", "Sum(TotalWeight)"},
                        null, null, null, null, null);

                if (result.moveToFirst()) {
                    tvLeft.setText(result.getInt(0) + " док.");
                    tvCenter.setText(GlobalProc.formatWeight(result.getFloat(2))
                            + Const.POSTFIX_WEIGHT);
                    tvRight.setText(GlobalProc.formatMoney(result.getFloat(1)));
                }
                break;
            case Payment:
                String totalSum = "";
                result = db.query(PaymentsMetaData.TABLE_NAME, new String[]{
                                "Count(CodeAgent)", "Sum(Amount)", "Sum(SumDoc)"}, null,
                        null, null, null, null);

                if (result.moveToFirst()) {
                    tvLeft.setText(result.getInt(0) + " док.");
                    tvRight.setText(GlobalProc.formatMoney(result.getFloat(1)));
                    totalSum = GlobalProc.formatMoney(result.getFloat(1));
                }

                tableLayout.removeAllViews();
                journal_base.setVisibility(View.GONE);
                weight.setVisibility(View.INVISIBLE);
                tvCenter.setVisibility(View.INVISIBLE);
                tvRight.setVisibility(View.VISIBLE);
                TableRow row = new TableRow(context);
                TextView titleFirm = new TextView(context);
                titleFirm = initText(titleFirm, "Фирма");
                TextView titleBezNal = new TextView(context);
                titleBezNal = initText(titleBezNal, "П");
                TextView titleNal = new TextView(context);
                titleNal = initText(titleNal, "Н");
                TextView total = new TextView(context);
                total = initText(total, "");
                row.addView(titleFirm);
                row.addView(titleBezNal);
                row.addView(titleNal);
                row.addView(total);
                tableLayout.addView(row, 0);
                int index = 1;
                ArrayList<PaymentTable> paymentTables = getFirmAndTypePay();
                float totalSumNal = 0.0f;
                float totalSumBezNal = 0.0f;
                for (PaymentTable paymentTable : paymentTables) {
                    row = new TableRow(context);
                    titleFirm = new TextView(context);
                    titleFirm = initText(titleFirm, paymentTable.getNameFirma());
                    titleBezNal = new TextView(context);
                    titleNal = new TextView(context);
                    float bezNal = paymentTable.getPayBezNal();
                    float nal = paymentTable.getPayNal();
                    titleBezNal = initText(titleBezNal, GlobalProc.formatMoney(bezNal));
                    titleNal = initText(titleNal, GlobalProc.formatMoney(nal));
                    totalSumNal += nal;
                    totalSumBezNal += bezNal;
                    total = new TextView(context);
                    total = initText(total, GlobalProc.formatMoney(paymentTable.getPayBezNal() + paymentTable.getPayNal()));
                    row.addView(titleFirm);
                    row.addView(titleBezNal);
                    row.addView(titleNal);
                    row.addView(total);
                    tableLayout.addView(row, index);
                    index++;
                }
                row = new TableRow(context);
                titleFirm = new TextView(context);
                titleFirm = initText(titleFirm, "");
                titleBezNal = new TextView(context);
                titleBezNal = initText(titleBezNal, GlobalProc.formatMoney(totalSumBezNal));
                titleNal = new TextView(context);
                titleNal = initText(titleNal, GlobalProc.formatMoney(totalSumNal));
                total = new TextView(context);
                total = initText(total, totalSum);
                row.addView(titleFirm);
                row.addView(titleBezNal);
                row.addView(titleNal);
                row.addView(total);
                tableLayout.addView(row, paymentTables.size() + 1);
                break;
            default:
                break;
        }
        if (result != null)
            result.close();
    }


    private ArrayList<PaymentTable> getFirmAndTypePay() {
        ArrayList<PaymentTable> paymentTables = new ArrayList<>();
        ArrayList<PaymentTable> result = new ArrayList<>();
        Set<String> nameFirm = new LinkedHashSet<>();
        if (mDocumentList != null)
            for (Document document : mDocumentList) {
                Cursor rows = null;
                try {
                    MyApp app = (MyApp) context.getApplicationContext();
                    String where = String.format("%s = '%s'", ClientCreditInfoMetaData.FIELD_NUMBER, document.mTag);
                    rows = app.getDB().query(ClientCreditInfoMetaData.TABLE_NAME, null, where, null, null, null, null);
                    while (rows.moveToNext()) {
                        String firma = rows.getString(rows.getColumnIndex(ClientCreditInfoMetaData.FIELD_FIRMA));
                        Firm firm = null;
                        if (!firma.equals("")) {
                            firm = Firm.load(context, Short.parseShort(firma));
                        }
                        if (firm != null) {
                            firma = firm.getName();
                        }
                        String typePay = rows.getString(rows.getColumnIndex(ClientCreditInfoMetaData.FIELD_DOC_TYPE));
                        nameFirm.add(firma);
                        if (typePay.equals("Н")) {
                            paymentTables.add(new PaymentTable(firma, Float.parseFloat(document.getFieldCenter()), 0));
                        } else {
                            paymentTables.add(new PaymentTable(firma, 0, Float.parseFloat(document.getFieldCenter())));
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                } finally {
                    if (rows != null) {
                        rows.close();
                    }
                }
            }
        for (String name : nameFirm) {
            PaymentTable paymentTable = new PaymentTable(name, 0, 0);
            result.add(paymentTable);
            for (PaymentTable p : paymentTables) {
                if (name.equals(p.getNameFirma())) {
                    paymentTable.setPayBezNal(paymentTable.getPayBezNal() + p.getPayBezNal());
                    paymentTable.setPayNal(paymentTable.getPayNal() + p.getPayNal());
                }
            }
        }

        return result;
    }

    private TextView initText(TextView textView, String text) {
        TableRow.LayoutParams lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 1f);
        textView.setTextColor(Color.WHITE);
        textView.setLayoutParams(lp);
        textView.setText(text);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    public void startAllVisitsReservTask() {
        if (mDocumentReserverThread != null && mDocumentReserverThread.getItemsCount() > 0) {
            return;
        }
        ArrayList<Visit> docs = Visit.loadList(getActivity().getApplicationContext(), Visit.STATE_NORMAL);
        // Сразу после загрузки добавляем документы из списка в поток
        // для отправки
        if (docs.size() > 0) {
            waitCursor(true);
        }
        for (Visit visit : docs) {
            String id = visit.getID().toString();
            try {
                MyApp app = (MyApp) getActivity().getApplication();
                SQLiteDatabase db = app.getDB();

                String sql = String.format("SELECT %s, COUNT(%s) as ordersCnt FROM %s WHERE %s = '%s' ",
                        OrderMetaData.FIELD_NUM,
                        OrderMetaData.FIELD_NUM,
                        OrderMetaData.TABLE_NAME,
                        OrderMetaData.FIELD_VISIT_FK, id);

                Cursor rows = db.rawQuery(sql, null);
                if (rows.moveToFirst()) {
                    visit.hasOrder(rows.getInt(1) > 0);
                }
                rows.close();

                sql = String.format("SELECT %s, COUNT(%s) as returnsCnt FROM %s WHERE %s = '%s' ",
                        ReturnMetaData.FIELD_NUM,
                        ReturnMetaData.FIELD_NUM,
                        ReturnMetaData.TABLE_NAME,
                        ReturnMetaData.FIELD_VISIT_FK, id);
                rows = db.rawQuery(sql, null);
                if (rows.moveToFirst()) {
                    visit.hasReturns(rows.getInt(1) > 0);
                }
                rows.close();
                sql = String.format("SELECT COUNT(%s) as paymentsCnt FROM %s WHERE %s = '%s' ",
                        PaymentsMetaData.FIELD_CODE_CLIENT,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_VISIT_FK, id);
                rows = db.rawQuery(sql, null);
                if (rows.moveToFirst()) {
                    visit.hasPayments(rows.getInt(0) > 0);
                }
                rows.close();

                sql = String.format(Locale.US, "select * from %s where %s = '%s'",
                        PaybackPhotoMetaData.TABLE_NAME, PaybackPhotoMetaData.FIELD_UUID, visit.getID().toString());

                rows = db.rawQuery(sql, null);
                while (rows.moveToNext()) {
                    visit.addPaybackPhotos(new PaybackPhoto(rows.getInt(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_ID)),
                            rows.getString(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_URL))));
                }
                rows.close();

            } catch (Exception e) {
                Log.e(TAG, "Ошибка при выборке подчиненных документов");
            }
            mDocumentReserverThread.queueVisit(visit, id);
        }
    }

    private void setAdapter(Const.DOCUMENTS_TYPE TypeDoc) {
        if (mDocumentList == null) {
            return;
        }
        mAdapter = new JournalAdapter(getActivity(), R.layout.item_journal,
                mDocumentList, TypeDoc);
        listView.setAdapter(mAdapter);
    }

    private void createReserver() {
        // Создаем резервер, подписываемся на события
        mDocumentReserverThread = new DocumentsReserver<>(
                App.get(getActivity()).getCodeAgent(), mHandler,
                Credentials.load(getActivity()));
        mDocumentReserverThread.setListener((visit, result, typeReserveServer) -> {
            // Вносим изменения в БД
            Log.d(TAG, "reserve visit");
            if (result == DocumentsReserver.RESULT_SUCCESS) {
                visit.updateState(context, Visit.STATE_SENDED, typeReserveServer, true);
            }
            if (mHandler != null) {
                mHandler.sendEmptyMessage(MESSAGE_UPDATE_LIST);
            }
        });
        mDocumentReserverThread.getLooper();
        mDocumentReserverThread.start();
        Log.i(TAG, "Background thread started");
    }

    @Override
    public void onTaskComplete(SetReservOrderTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            fillListAndBasement(positionViewPager);

            txt = context.getString(R.string.mes_reserv_success);
        } else {
            txt = context.getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getMessage();
        }
        GlobalProc.mToast(context, txt);
        waitCursor(false);
    }

    private void waitCursor(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onTaskComplete(SetReservReturnTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }

        String msg;
        if (result) {
            msg = "Возвраты успешно отправлены в 1С";
        } else {
            msg = "Один или несколько возвратов не были отправлены";
            Log.e(TAG, task.getMessage());
        }

        GlobalProc.mToast(context, msg);
        fillListAndBasement(positionViewPager);
        waitCursor(false);
    }

    @Override
    public void onTaskComplete(SetReservPaymentTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }

        String msg;
        if (result) {
            msg = "Оплаты успешно отправлены";
        } else {
            msg = "Одна или несколько оплат не были отправлены:\n"
                    + task.getMessage();
        }

        GlobalProc.mToast(context, msg);
        waitCursor(false);
    }


    void setupReceiver() {
        uploadTask = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte status = intent.getByteExtra(
                        UploadFileIntentService.EXTRA_STATUS, (byte) 0);
                switch (status) {
                    case UploadFileIntentService.STATUS_ERROR:
                        String stringExtra = intent
                                .getStringExtra(UploadFileIntentService.EXTRA_MESSAGE);
                        GlobalProc.mToast(context, stringExtra);
                        break;
                    case UploadFileIntentService.STATUS_OK:
                        GlobalProc.mToast(context, "Успешно отправлено");
                        images = getListImage();
                        galleryAdapter = new GalleryAdapter(context, images);
                        recyclerView.setAdapter(galleryAdapter);
                        break;
                }
                progressPhoto.setVisibility(View.INVISIBLE);
            }
        };
        BroadcastReceiver addPhoto = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                images = getListImage();
                galleryAdapter = new GalleryAdapter(context, images);
                recyclerView.setAdapter(galleryAdapter);
            }
        };
        BroadcastReceiver progressReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int i = intent.getIntExtra("extra", 0);
                if (i == 0) {
                    progressPhoto.setVisibility(View.VISIBLE);
                } else {
                    progressPhoto.setProgress(i);
                }
            }
        };
        IntentFilter filterUpload = new IntentFilter(UploadFileIntentService.BROADCAST_ACTION);
        context.registerReceiver(uploadTask, filterUpload);
        IntentFilter intentFilter = new IntentFilter(AddPhotoIntentService.BROADCAST_ACTION);
        context.registerReceiver(addPhoto, intentFilter);
        IntentFilter intentProgress = new IntentFilter(ProgressPhotoIntentService.BROADCAST_ACTION);
        context.registerReceiver(progressReceiver, intentProgress);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (uploadTask != null)
            context.unregisterReceiver(uploadTask);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IDD_NEW_PHOTO) {
            Intent startUpload = new Intent(context, AddPhotoIntentService.class);
            context.startService(startUpload);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                if (!initFlag) {
                    positionViewPager = 0;
                }
                switch (positionViewPager) {
                    case 0: //visit
                        startNewVisitDoc();
                        break;
                    case 1: //order
                        if (App.isMerch()) {
                            startNewVisitDoc();
                        } else {
                            checkExistOrderDoc();
                        }
                        break;
                    case 2: //return
                        if (App.isMerch()) {
                            startNewPaymentDoc();
                        } else {
                            startNewReturnDoc();
                        }
                        break;
                    case 3: //payment
                        if (App.isMerch()) {
                            startUploadFileService();
                        } else {
                            startNewPaymentDoc();
                        }
                        break;
                    case 4:
                        if (!App.isMerch()) {
                            startUploadFileService();
                        }
                        break;
                }
                break;
            case R.id.fab2:
                if (isAdded()) {
                    startNewPhoto();
                }
                break;
        }
    }

    private void startUploadFileService() {
        if (getListImage().size() != 0) {
            Intent startUpload = new Intent(context, UploadFileIntentService.class);
            String[] array = getListImage().toArray(new String[0]);
            startUpload.putExtra(UploadFileIntentService.EXTRA_FILES_LIST, array);
            startUpload.putExtra("receiver", new DownloadReceiver(new Handler()));
            context.startService(startUpload);
            Intent startProgress = new Intent(context, ProgressPhotoIntentService.class);
            startProgress.putExtra("extra", 0);
            context.startService(startProgress);
        }
    }

    @Override
    public void onTaskComplete(SetReserveMonitoring task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            fillListAndBasement(positionViewPager);

            txt = context.getString(R.string.mes_reserv_success);
        } else {
            txt = context.getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getMessage();
        }
        GlobalProc.mToast(context, txt);
        waitCursor(false);
    }


    private class DownloadReceiver extends ResultReceiver {

        DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            switch (resultCode) {
                case 1:
                    Intent startProgress = new Intent(context, ProgressPhotoIntentService.class);
                    startProgress.putExtra("extra", resultData.getInt("progress"));
                    context.startService(startProgress);
                    break;
                case 2:
                    // progressPhoto.setVisibility(View.INVISIBLE);
                    break;
            }

        }
    }

}
