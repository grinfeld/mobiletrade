package com.madlab.mtrade.grinfeld.roman.fragments;


import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.TaskAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.iface.IGetTasksProgress;
import com.madlab.mtrade.grinfeld.roman.tasks.GetTasksTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class TasksFragment extends ListFragment implements IGetTasksProgress {

    private static final String TAG = "!->TasksFragment";
    public static final String EXTRA_CLIENT_CODE = "extraClientCode";
    public static final String EXTRA_FROM_DATE = "extraFromDate";
    public static final String EXTRA_TO_DATE = "extraToDate";


    private String mCodeClient;
    private ArrayList<Task> mTaskList = new ArrayList<>();
    private TaskAdapter mAdapter;
    private View emptyView;
    private Context context;
    private ProgressBar progressBar;
    private GetTasksTask task;
    public CheckBox cb_show_complete_task;
    private Handler handler;
    private ListView listView;

    @Override
    public void onGetTasksComplete(GetTasksTask task) {
    }

    @Override
    public void onGetTasksProgress(List<Task> taskList) {
        progressBar.setVisibility(View.GONE);
        cb_show_complete_task.setVisibility(View.VISIBLE);
        if (listView != null) {
            listView.setVisibility(View.VISIBLE);
        }
        if (taskList != null) {
            for (Task item : taskList) {
                if (!(TaskLab.get(getActivity())).updateTask(item)) {
                    TaskLab.get(getActivity()).addTask(item);
                }
            }
        }
        fillList(false);
    }


    public static TasksFragment newInstance(String codeClient, Date fromDate, Date toDate) {
        Bundle args = new Bundle();
        args.putString(EXTRA_CLIENT_CODE, codeClient);
        if (fromDate!=null){
            args.putString(EXTRA_FROM_DATE, TimeFormatter.sdfTaskMerch.format(fromDate));
            args.putString(EXTRA_TO_DATE, TimeFormatter.sdfTaskMerch.format(toDate));
        }
        TasksFragment fragment = new TasksFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        setHasOptionsMenu(true);
        mCodeClient = getArguments().getString(EXTRA_CLIENT_CODE);
    }


    @Override
    public void onDestroyView() {
        if (v.getParent() != null) {
            ((ViewGroup) v.getParent()).removeView(v);
        }
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (task != null && task.getStatus() == AsyncTask.Status.RUNNING)
            task.cancel(true);
    }

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_visit_contain_bottom_tasks, container, false);
            context = getActivity();
            handler = new Handler(Looper.getMainLooper());
            progressBar = v.findViewById(R.id.progress_bar_tasks);
            cb_show_complete_task = v.findViewById(R.id.cb_show_complete_task);
            cb_show_complete_task.setOnCheckedChangeListener((compoundButton, b) -> {
                Log.d(TAG, "onCheckedChanged: " + String.valueOf(b));
                if (b) {
                    fillList(true);
                } else {
                    fillList(false);
                }
            });
            mAdapter = new TaskAdapter(context, mTaskList);
            setListAdapter(mAdapter);
            ImageButton btRefresh = v.findViewById(R.id.ev_btRefresh);
            if (btRefresh != null) {
                btRefresh.setOnClickListener(arg0 -> {
                    enableButtonRefresh(false);
                    prepareLoadTaskList();
                });
            }
            prepareLoadTaskList();
        }
        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        MyApp app = (MyApp) getActivity().getApplication();
        //Client client = Client.load(app.getDB(), mCodeClient);
        //fillList();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emptyView = view.findViewById(R.id.empty_view);
        listView = getListView();
    }


    public void fillList(boolean b) {
        if (b) {
            if (App.isMerch()){
                mTaskList = TaskLab.get(context).getTasksMerch(App.get(context).getCodeAgent());
            }else {
                mTaskList = TaskLab.get(context).getTasksOnClient(App.get(context).getCodeAgent(), mCodeClient);
            }
        } else {
            if (App.isMerch()){
                mTaskList = TaskLab.get(context).getTasksMerch(App.get(context).getCodeAgent());
            }else {
                mTaskList = TaskLab.get(context).getTasksOnClient(App.get(context).getCodeAgent(), mCodeClient);
            }
            if (mTaskList != null) {
                Iterator<Task> taskIterator = mTaskList.iterator();
                while (taskIterator.hasNext()) {
                    Task task = taskIterator.next();
                    if (task.currentStatus() == TaskAddEditFragment.TASK_COMPLETE_INDEX) {
                        taskIterator.remove();
                    }
                }
            }
        }
        if (mTaskList != null) {
            mAdapter = new TaskAdapter(context, mTaskList);
        }
        setListAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Task current = mAdapter.getItem(position);
        TaskAddEditFragment mtf = TaskAddEditFragment.newInstance(current, false);
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.visit_bottom, mtf)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void prepareLoadTaskList() {
        App settings = App.get(context);
        if (settings.connectionServer.equals("1")) {
            startLoadTaskList(null);
        } else {
            String procName = App.isMerch() ? GetTasksTask.PROC_NAME_MERCH : GetTasksTask.PROC_NAME;
            String values = App.isMerch() ? String.format("%s;%s;%s;", settings.getCodeAgent(),
                    getArguments().getString(EXTRA_FROM_DATE), getArguments().getString(EXTRA_TO_DATE)) :
                    String.format("%s;%s", settings.getCodeAgent(), mCodeClient);
            OkHttpClientBuilder.buildCall(procName, values, App.getRegion(context)).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    handler.post(() -> {
                        startLoadTaskList(null);
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String strResponse = response.body().string();
                    handler.post(() -> {
                        try {
                            Log.d(TAG, procName+" : " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String param = jsonObject.getString("data");
                            handler.post(() -> startLoadTaskList(param));
                        } catch (JSONException e) {
                            handler.post(() -> startLoadTaskList(null));
                        }
                    });
                }
            });
        }
    }

    private void startLoadTaskList(String param) {
        task = new GetTasksTask(mCodeClient, this, getArguments().getString(EXTRA_FROM_DATE), getArguments().getString(EXTRA_TO_DATE));
        task.execute(param);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void enableButtonRefresh(boolean value) {
        ImageButton btRefresh = getView().findViewById(
                R.id.ev_btRefresh);
        if (btRefresh != null) {
            btRefresh.setEnabled(value);
        }
    }

}
