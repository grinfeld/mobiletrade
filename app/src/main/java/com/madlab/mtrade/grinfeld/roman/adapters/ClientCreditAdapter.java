package com.madlab.mtrade.grinfeld.roman.adapters;

import java.util.ArrayList;
import java.util.Locale;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Arrear;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Firm;


public class ClientCreditAdapter extends ArrayAdapter<Arrear> {

    private Drawable imageP;
    private Drawable imageN;
    private Context context;
    private boolean isForeign;

    public ClientCreditAdapter(Context context, ArrayList<Arrear> items, boolean isForeign) {
        super(context, 0, items);
        imageP = ContextCompat.getDrawable(context, R.mipmap.doc_p);
        imageN = ContextCompat.getDrawable(context, R.mipmap.doc_n);
        this.context = context;
        this.isForeign = isForeign;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.item_credit_client_info, null);

            holder = new ViewHolder();

            holder.tvDocNumber = convertView
                    .findViewById(R.id.cciItem_docNumber);
            holder.tvDocDate =  convertView
                    .findViewById(R.id.cciItem_docDate);
            holder.tvAgent =  convertView
                    .findViewById(R.id.cciItem_agentName);

            holder.tvDocSum =  convertView
                    .findViewById(R.id.cciItem_docSum);
            holder.tvDocCredit =  convertView
                    .findViewById(R.id.cciItem_docCredit);
            holder.tvDocPay =  convertView
                    .findViewById(R.id.cciItem_docPayment);
            holder.tvExpeditor =  convertView
                    .findViewById(R.id.cciItem_expeditor);
            holder.tvPhone =  convertView
                    .findViewById(R.id.cciItem_expeditorPhone);
            holder.imageDocType =  convertView
                    .findViewById(R.id.cciItem_docType);
            holder.imageCall = convertView.findViewById(R.id.cciItem_imageExpeditorPhone);
            holder.tvFirma = convertView.findViewById(R.id.cciItem_Firma);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Arrear credit = getItem(position);
        if (credit != null) {
            // Заголовок
            if (holder.tvDocNumber != null) {
                holder.tvDocNumber.setText(credit.getDocNumber());
            }
            if (holder.tvFirma!=null){
                String textFirmAndTypePay = "";
                if (credit.getFirma()!=null){
                    if (!credit.getFirma().equals("")){
                        Firm item = Firm.load(context, Short.parseShort(credit.getFirma()));
                        if (item != null) {
                            textFirmAndTypePay = item.getName();
                        }
                    }
                }
                holder.tvFirma.setText(textFirmAndTypePay);
            }

            if (holder.tvDocDate != null) {
                holder.tvDocDate
                        .setText(TimeFormatter.sdf1C.format(credit.getDocDate()));
            }

            if (holder.tvAgent != null) {
                holder.tvAgent.setText(credit.getAgentFIO());
            }

            // Данные
            if (holder.tvDocSum != null) {
                holder.tvDocSum.setText(GlobalProc.toForeignPrice(credit.getDocSum(), isForeign));//String.format(Locale.getDefault(), Const.FORMAT_MONEY, credit.getDocSum()));
            }

            if (holder.tvDocCredit != null) {
                holder.tvDocCredit.setText(GlobalProc.toForeignPrice(credit.getDocCredit(), isForeign));//String.format(Locale.getDefault(), Const.FORMAT_MONEY, credit.getDocCredit()));
            }

            if (holder.tvDocPay != null) {
                holder.tvDocPay
                        .setText(GlobalProc.toForeignPrice(credit.getDocPayment(), isForeign));
            }

            // Экспедитор
            if (holder.tvExpeditor != null) {
                holder.tvExpeditor.setText(credit.getExpeditor());
            }
            if (holder.tvPhone != null) {
                holder.tvPhone.setText(credit.getExpeditorPhone());
                holder.tvPhone.setTag(credit.getExpeditorPhone());
            }
            if (holder.imageCall != null){
                holder.imageCall.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String phoneNum = (String) holder.tvPhone.getTag();
                        if (phoneNum != null) {
                            String uri = "tel:" + phoneNum;
                            context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                        }
                    }
                });
            }
            if (holder.imageDocType != null) {
                holder.imageDocType.setImageDrawable(credit.getDocType() == Arrear.DOC_P ? imageP : imageN);
            }
        }
        return convertView;
    }


    private static class ViewHolder {
        TextView tvDocNumber;
        TextView tvDocDate;
        TextView tvAgent;

        TextView tvDocSum;
        TextView tvDocCredit;
        TextView tvDocPay;

        TextView tvExpeditor;
        TextView tvPhone;

        ImageView imageDocType;
        ImageView imageCall;

        TextView tvFirma;
    }
}