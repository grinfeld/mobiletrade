package com.madlab.mtrade.grinfeld.roman;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.graphics.Color;
import android.util.Xml.Encoding;

public class Const {
    public static enum Theme {
        Default, Dark, Light, Transparent
    };

    public final static String DEFAULT_CODE_AGENT = "XXXXXX";
    public final static String COMMAND = "COMMAND";
    public final static String COMMAND_SEND_FILE = "TAKE";

    /**
     * Разделитель для суб-элементов
     */
    public static final String DELIMITTER = "|";
    public static final String END_MESSAGE = "<EOF>";

    public static final String WIN1251 = "windows-1251";
    public static final String UTF8 = Encoding.UTF_8.name();

    public static final String NewLine = System.getProperty("line.separator");
    public static final char CR = 0x0D;
    public static final char LR = 0x0A;
    public static final String CRLR = new String(new char[] { CR, LR });

    public static final NumberFormat formatMoney = DecimalFormat.getCurrencyInstance();
    public static final String POSTFIX_WEIGHT = " кг.";
    public static final String POSTFIX_WEIGHT_GOODS = " гол.";
    public static final String POSTFIX_PIECE_GOODS = " шт.";

    public static final String ORDER_FILE_PREFIX = "Z";
    public static final String RETURN_FILE_PREFIX = "V";
    public static final String PAY_FILE_PREFIX = "O";
    public static final String LOCATION_FILE_PREFIX = "L";

    public static final String EXPORT_FILE_EXT = ".txt";
    public static final String APK = "MTrade2.apk";
    public static final String ZIP = "MTrade2.zip";
    public static final String OrdersListFileName = "OrdersList.txt";
    public static final String ReturnsListFileName = "ReturnsList.txt";

    // Wheels
    public final static byte WHEEL_TEXT_SIZE = 16;

    public static enum DOCUMENTS_TYPE {
        Order, Payment, Return, Visit, Photo
    }

    public static final String COLOR_GOLD_STRING = "#DAA520";
    public static final int COLOR_GOLD = Color.parseColor(COLOR_GOLD_STRING);

    // private static String DEFAULT_FOLDER = Environment.getDataDirectory() +
    // File.separator + "com.dalimo.mtrade";
    public static String IMPEXP_FOLDER = "IMPEXP";
    public static String BACKUP_FOLDER = "BACKUP";

    public static byte NOTIFICATION_ID = 126;

    public final static String DEFAULT_CATALOG_URL = "https://www.dalimo.ru/get_images.php?secret=";

    public static String FORMAT_MONEY = "%,.2f";
    public static String FORMAT_WEIGHT = "%.3f";
}
