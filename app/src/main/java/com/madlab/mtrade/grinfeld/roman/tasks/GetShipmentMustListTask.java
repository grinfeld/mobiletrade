package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.iface.IGetShipmentMustList;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by GrinfeldRA on 10.05.2018.
 */

public class GetShipmentMustListTask extends AsyncTask<Void, Void, String>{

    private final static String COMMAND = "COMMAND";
    private final static String FUNCTION = "ПолучитьОтгрузкиПоМастЛисту";

    private DalimoClient client;
    private Credentials connectInfo;

    private String codeClient;
    private String codeAgent;
    private String mUnswer = null;
    private String _1CUnswer = null;
    private Context context;
    private IGetShipmentMustList callback;
    private ArrayList<Goods> goodsArrayList;

    public GetShipmentMustListTask(Context context, IGetShipmentMustList callback, String codeClient, String codeAgent, ArrayList<Goods> goodsArrayList){
        this.context = context;
        this.callback = callback;
        this.codeClient = codeClient;
        this.codeAgent = codeAgent;
        this.goodsArrayList = goodsArrayList;
    }

    @Override
    protected String doInBackground(Void... voids) {
        StringBuilder stringBuilder = new StringBuilder();
        if (goodsArrayList!=null)
        for (Goods goods : goodsArrayList){
            stringBuilder.append(goods.getCode()).append("|");
        }
        String query = String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s;%s",
                COMMAND, codeAgent, FUNCTION, codeClient, codeAgent, stringBuilder.toString(), Const.END_MESSAGE);
        connectInfo = Credentials.load(context);
        client = new DalimoClient(connectInfo);
        if (!client.connect()) {
            mUnswer = "Не удалось подключиться";
            return null;
        }

        client.send(query);

        _1CUnswer = client.receive();

        client.disconnect();

        if (_1CUnswer == null || _1CUnswer.contains(DalimoClient.ERROR)) {
            mUnswer = _1CUnswer;
            return null;
        }
        return _1CUnswer;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callback.getShipmentMustListComplete(this);
    }
}