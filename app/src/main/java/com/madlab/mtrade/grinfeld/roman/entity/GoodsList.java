package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MatrixMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MustListMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TransitMetaData;

public class GoodsList extends ArrayList<Goods> {
    public static final String KEY = "GoodsListExtra";
    private static final String TAG = "!->GoodsList";

    /**
     * Режим загрузки товаров ВСЕ, МАТРИЦА, НЕЛИКВИД
     * MODE_ALL = 0;
     */
    public final static byte MODE_ALL = 0;
    /**
     * Режим загрузки товаров ВСЕ, МАТРИЦА, НЕЛИКВИД
     * MODE_MATRIX = 1;
     */
    public final static byte MODE_MATRIX = 2;
    /**
     * Режим загрузки товаров ВСЕ, МАТРИЦА, НЕЛИКВИД
     * MODE_STM = 2;
     */
    public final static byte MODE_STM = 3;

    public final static byte MODE_MUST_LIST = 4;

    public final static byte MODE_LIST = 1;

    public final static byte MODE_PROMOTIONS = 5;

    public final static byte MODE_NON_LIQUID = 10;


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public GoodsList() {
        super();
    }

//	public GoodsList(ArrayList<Goods> g){
//		super();
//	}

    public int getPositionVisibleList(Goods goods) {
        int res = -1;
        Goods item;
        for (int i = 0; i < size(); i++) {
            item = get(i);
            if (item != null && item.getCode().equalsIgnoreCase(goods.getCode())) {
                res = i;
                break;
            }
        }
        return res;
    }

    public void sortVisibleList() {
        Collections.sort(this, goodsComparator);
    }

    //-------------------------Статические методы ------------------------------

    public static Comparator<Goods> goodsComparator = new Comparator<Goods>() {

        @Override
        public int compare(Goods lhs, Goods rhs) {
            return lhs.getName().compareToIgnoreCase(rhs.getName());
        }
    };

    /**
     * Элементы подгруппы
     */
    public static GoodsList loadChild(SQLiteDatabase db, String parent) {
        String where = String.format("%s = 0 AND %s = '%s'",
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                GoodsMetaData.FIELD_PARENT.Name, parent);
        return loadList(db, where, null, GoodsMetaData.FIELD_NAME.Name);
    }

    /**
     * Элементы подгруппы, Только СТМ
     */
    public static GoodsList loadChildSTM(SQLiteDatabase db, String parent) {
        String where = String.format("%s = 0 AND %s = '%s' AND %s = 1",
                //
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                //AND
                GoodsMetaData.FIELD_PARENT.Name, parent,
                //AND
                GoodsMetaData.FIELD_STM);
        return loadList(db, where, null, GoodsMetaData.FIELD_NAME.Name);
    }

    /**
     * Только СТМ
     */
    public static GoodsList loadSTM(SQLiteDatabase db) {
        String where = String.format("%s = 0 AND %s = 1",
                //
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                //AND
                GoodsMetaData.FIELD_STM);
        return loadList(db, where, null, GoodsMetaData.FIELD_NAME.Name);
    }

    public static int CountInGroup(SQLiteDatabase db, String codeGroup) {
        String sql = String.format(Locale.getDefault(), "SELECT COUNT(*) FROM %s WHERE %s = \'%s\' AND %s = 0"
                , GoodsMetaData.TABLE_NAME
                , GoodsMetaData.FIELD_PARENT.Name
                , codeGroup
                , GoodsMetaData.FIELD_RECORD_TYPE.Name);
        Cursor rows = db.rawQuery(sql, null);
        int cnt = 0;
        if (rows.moveToFirst()) {
            cnt = rows.getInt(0);
        }
        return cnt;
    }

    /**
     * Загружает список товаров
     */
    public static GoodsList loadList(SQLiteDatabase db, String where, String[] selectionArguments, String orderBy) {
        GoodsList resList = new GoodsList();

        Cursor results = null;

        try {
            results = db.query(GoodsMetaData.TABLE_NAME, null, where, selectionArguments,
                    null, null, orderBy);

            while (results.moveToNext()) {
                Goods good = new Goods();
                // results.getString(0), results.getString(2),
                // results.getString(1)
                good.setCode(results.getString(GoodsMetaData.FIELD_CODE.Index));
                good.setName(results.getString(GoodsMetaData.FIELD_NAME.Index));
                good.setParentCode(results.getString(GoodsMetaData.FIELD_PARENT.Index));
                good.setNameFull(results.getString(GoodsMetaData.FIELD_NAME_FULL.Index));
                good.setBaseMeash(results.getString(GoodsMetaData.FIELD_BASE_MEASH.Index));
                good.setPackMeash(results.getString(GoodsMetaData.FIELD_PACK_MEASH.Index));
                good.setNumInPack(results.getFloat(GoodsMetaData.FIELD_NUM_IN_PACK.Index));
                float w = results.getFloat(GoodsMetaData.FIELD_WEIGHT.Index);
                float q = results.getFloat(GoodsMetaData.FIELD_QUANT.Index);
                good.setQuant(q);
                good.setRestOnStore(results.getInt(GoodsMetaData.FIELD_REST.Index));
                good.setPriceContract(results.getFloat(GoodsMetaData.FIELD_PRICE_CONTRACT.Index));
                good.setPriceStandart(results.getFloat(GoodsMetaData.FIELD_PRICE_STANDART.Index));
                good.setPriceShop(results.getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index));
                good.setPriceRegion(results.getFloat(GoodsMetaData.FIELD_PRICE_REGION.Index));
                good.setPriceMarket(results.getFloat(GoodsMetaData.FIELD_PRICE_MARKET.Index));
                good.setPriceOptSection(results.getFloat(GoodsMetaData.FIELD_PRICE_SECTION.Index));
                good.setBestBefore(results.getString(GoodsMetaData.FIELD_BEST_BEFORE.Index));
                good.setNDS((byte) results.getShort(GoodsMetaData.FIELD_NDS.Index));
                good.setCountry(results.getString(GoodsMetaData.FIELD_COUNTRY.Index));
                good.setWeight(w);
                good.setHighLight((byte) results.getShort(GoodsMetaData.FIELD_HIGHLIGHT.Index));
                good.setIsWeightGood(results.getShort(GoodsMetaData.FIELD_IS_WEIGHT.Index) == 1);
                good.isHalfHead(results.getShort(GoodsMetaData.FIELD_IS_HALFHEAD.Index) == 1);
                good.perishable(results.getShort(GoodsMetaData.FIELD_IS_PERISHABLE.Index) == 1);
                good.stm(results.getShort(GoodsMetaData.FIELD_STM.Index) == 1);
                good.manager(results.getString(GoodsMetaData.FIELD_MANAGER_FK.Index));
                good.transitList(Goods.loadTransitList(db, good.getCode()));
                good.setIsMercury(results.getInt(GoodsMetaData.FIELD_IS_MERCURY.Index));
                good.setIsCustom(results.getInt(GoodsMetaData.FIELD_IS_CUSTOM.Index));
                good.setNonCash(results.getInt(GoodsMetaData.FIELD_IS_NON_CASH.Index) == 1);

                resList.add(good);
            }
            results.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (results != null && !results.isClosed()) {
                results.close();
            }
        }

        return resList;
    }

    /**
     * Элементы подгруппы с остатками. Нам нужно объединить три запроса.
     * Первый - не нулевые остатки за исключением скоропорта
     * Второй - нулевые со списком товаров в пути за исключением скоропорта
     * Третий - скоропорт
     * В результате можем получить дубликаты товаров, с полями "в пути"
     * SELECT * FROM GoodsInStor
     * LEFT JOIN Transit ON GoodsInStor.CodeGoods = Transit.CodeGoods
     * WHERE RecType = 0
     * AND ParentCode = '32311'
     * AND IsPerishable = 0 AND RestOnStor > 0
     * UNION
     * SELECT * FROM GoodsInStor
     * INNER JOIN Transit
     * ON GoodsInStor.CodeGoods = Transit.CodeGoods
     * WHERE RecType = 0 AND ParentCode = '32311' AND IsPerishable = 0 AND RestOnStor = 0
     * UNION
     * SELECT * FROM GoodsInStor
     * LEFT JOIN Transit ON GoodsInStor.CodeGoods = Transit.CodeGoods
     * WHERE RecType = 0 AND ParentCode = '32311' AND IsPerishable = 1
     * ORDER BY Name
     */
    public static GoodsList loadChildWithRest(SQLiteDatabase db, String parent) {
        GoodsList result = new GoodsList();
        //Товары с не нулевым остатком. Не скоропорт
        String query1 = String.format(
                "SELECT * FROM %s LEFT JOIN %s " + "ON %s.%s = %s.%s "
                        + "WHERE %s = 0 " + "AND %s = '%s' "
                        + "AND %s = 0 AND %s > 0",
                // SELECT
                GoodsMetaData.TABLE_NAME,
                TransitMetaData.TABLE_NAME,
                // ON
                GoodsMetaData.TABLE_NAME,
                GoodsMetaData.FIELD_CODE.Name,
                TransitMetaData.TABLE_NAME,
                TransitMetaData.FIELD_CODE_GOODS.Name,
                // WHERE
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                GoodsMetaData.FIELD_PARENT.Name, parent,
                GoodsMetaData.FIELD_IS_PERISHABLE.Name,
                GoodsMetaData.FIELD_REST.Name);
        //Товары в пути с нулевым остатком. Не скоропорт
        String query2 = String.format(
                "SELECT * FROM %s INNER JOIN %s " + "ON %s.%s = %s.%s "
                        + "WHERE %s = 0 " + "AND %s = '%s' "
                        + "AND %s = 0 AND %s = 0",
                // SELECT
                GoodsMetaData.TABLE_NAME,
                TransitMetaData.TABLE_NAME,
                // ON
                GoodsMetaData.TABLE_NAME,
                GoodsMetaData.FIELD_CODE.Name,
                TransitMetaData.TABLE_NAME,
                TransitMetaData.FIELD_CODE_GOODS.Name,
                // WHERE
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                GoodsMetaData.FIELD_PARENT.Name, parent,
                GoodsMetaData.FIELD_IS_PERISHABLE.Name,
                GoodsMetaData.FIELD_REST.Name);
        //Добавляем скоропорт с остатками
        String query3 = String.format(
                "SELECT * FROM %s LEFT JOIN %s " + "ON %s.%s = %s.%s "
                        + "WHERE %s = 0 " + "AND %s = '%s' "
                        + "AND %s = 1 AND %s > 0",
                // SELECT
                GoodsMetaData.TABLE_NAME,
                TransitMetaData.TABLE_NAME,
                // ON
                GoodsMetaData.TABLE_NAME,
                GoodsMetaData.FIELD_CODE.Name,
                TransitMetaData.TABLE_NAME,
                TransitMetaData.FIELD_CODE_GOODS.Name,
                // WHERE
                GoodsMetaData.FIELD_RECORD_TYPE.Name,
                GoodsMetaData.FIELD_PARENT.Name, parent,
                GoodsMetaData.FIELD_IS_PERISHABLE.Name,
                GoodsMetaData.FIELD_REST.Name);
        //Собираем в один
        String sql = String.format("%s UNION %s UNION %s ORDER BY %s",
                query1, query2, query3,
                GoodsMetaData.FIELD_NAME.Name);
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, null);

            byte transitCodeGoodsColumnNumber = (byte) rows.getColumnIndex(TransitMetaData.FIELD_CODE_GOODS.Name);//TransitMetaData.TABLE_NAME+"."+
            byte transitDateColumnNumber = (byte) rows.getColumnIndex(TransitMetaData.FIELD_DATE.Name);//TransitMetaData.TABLE_NAME+"."+
            byte transitCountColumnNumber = (byte) rows.getColumnIndex(TransitMetaData.FIELD_COUNT.Name);//TransitMetaData.TABLE_NAME+"."+
            byte quantColumnNumber = (byte) rows.getColumnIndex(GoodsMetaData.FIELD_QUANT.Name);

            Goods newItem = null;
            while (rows.moveToNext()) {
                String codeGoods = rows
                        .getString(GoodsMetaData.FIELD_CODE.Index);
                // Если код последнего товара совпадает, то добавляем значение в
                // список товара в пути
                if (newItem != null
                        && codeGoods.equalsIgnoreCase(newItem.getCode())) {
                    Shipment transitItem = new Shipment(
                            TimeFormatter.parseSqlDate(rows
                                    .getString(transitDateColumnNumber)),
                            rows.getInt(transitCountColumnNumber));
                    newItem.addTransitItem(transitItem);
                } else {
                    newItem = new Goods();

                    newItem.setCode(codeGoods);
                    newItem.setName(rows
                            .getString(GoodsMetaData.FIELD_NAME.Index));
                    newItem.setParentCode(rows
                            .getString(GoodsMetaData.FIELD_PARENT.Index));
                    newItem.setNameFull(rows
                            .getString(GoodsMetaData.FIELD_NAME_FULL.Index));
                    newItem.setBaseMeash(rows
                            .getString(GoodsMetaData.FIELD_BASE_MEASH.Index));
                    newItem.setPackMeash(rows
                            .getString(GoodsMetaData.FIELD_PACK_MEASH.Index));
                    newItem.setNumInPack(rows
                            .getShort(GoodsMetaData.FIELD_NUM_IN_PACK.Index));

                    float w = rows.getFloat(GoodsMetaData.FIELD_WEIGHT.Index);
                    float q = rows.getFloat(quantColumnNumber);

                    newItem.setQuant(q);

                    newItem.setRestOnStore(rows
                            .getInt(GoodsMetaData.FIELD_REST.Index));

                    newItem.setPriceContract(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_CONTRACT.Index));
                    newItem.setPriceStandart(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_STANDART.Index));
                    newItem.setPriceShop(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index));
                    newItem.setPriceRegion(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_REGION.Index));
                    newItem.setPriceMarket(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_MARKET.Index));
                    newItem.setPriceOptSection(rows
                            .getFloat(GoodsMetaData.FIELD_PRICE_SECTION.Index));

                    newItem.setBestBefore(rows
                            .getString(GoodsMetaData.FIELD_BEST_BEFORE.Index));
                    newItem.setNDS((byte) rows
                            .getShort(GoodsMetaData.FIELD_NDS.Index));
                    newItem.setCountry(rows
                            .getString(GoodsMetaData.FIELD_COUNTRY.Index));

                    newItem.setWeight(w);

                    newItem.setHighLight((byte) rows
                            .getShort(GoodsMetaData.FIELD_HIGHLIGHT.Index));
                    newItem.setIsWeightGood(rows
                            .getShort(GoodsMetaData.FIELD_IS_WEIGHT.Index) == 1);
                    newItem.isHalfHead(rows
                            .getShort(GoodsMetaData.FIELD_IS_HALFHEAD.Index) == 1);
                    newItem.perishable(rows
                            .getShort(GoodsMetaData.FIELD_IS_PERISHABLE.Index) == 1);
                    newItem.stm(rows
                            .getShort(GoodsMetaData.FIELD_STM.Index) == 1);

                    newItem.manager(rows
                            .getString(GoodsMetaData.FIELD_MANAGER_FK.Index));

                    // Смотрим товары в пути
                    if (rows.getString(transitCodeGoodsColumnNumber) != null) {
                        Shipment transitItem = new Shipment(
                                TimeFormatter.parseSqlDate(rows
                                        .getString(transitDateColumnNumber)),
                                rows.getInt(transitCountColumnNumber));
                        newItem.addTransitItem(transitItem);
                    }
                    newItem.setIsMercury(rows.getInt(GoodsMetaData.FIELD_IS_MERCURY.Index));
                    newItem.setIsCustom(rows.getInt(GoodsMetaData.FIELD_IS_CUSTOM.Index));
                    newItem.setNonCash(rows.getInt(GoodsMetaData.FIELD_IS_NON_CASH.Index) == 1);
                    result.add(newItem);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    /**
     * Товарно-ценовая матрица
     */
    public static GoodsList loadMatrix(SQLiteDatabase db, String query, String[] param) {
        if (param == null || param.length == 0)
            return null;

        GoodsList result = null;

        Cursor rows = db.rawQuery(query, param);

        if (rows.getCount() > 0) {
            result = new GoodsList();
        }

        int colPrice = rows.getColumnIndex(MatrixMetaData.FIELD_PRICE);
        while (rows.moveToNext()) {
            Goods good = new Goods();

            good.setCode(rows.getString(GoodsMetaData.FIELD_CODE.Index));
            good.setName(rows.getString(GoodsMetaData.FIELD_NAME.Index));
            good.setParentCode(rows
                    .getString(GoodsMetaData.FIELD_PARENT.Index));

            good.setNameFull(rows
                    .getString(GoodsMetaData.FIELD_NAME_FULL.Index));
            good.setBaseMeash(rows
                    .getString(GoodsMetaData.FIELD_BASE_MEASH.Index));
            good.setPackMeash(rows
                    .getString(GoodsMetaData.FIELD_PACK_MEASH.Index));
            good.setNumInPack(rows
                    .getShort(GoodsMetaData.FIELD_NUM_IN_PACK.Index));
            good.setQuant(rows.getFloat(GoodsMetaData.FIELD_QUANT.Index));
            good.setRestOnStore(rows.getInt(GoodsMetaData.FIELD_REST.Index));

            good.setPriceContract(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_CONTRACT.Index));
            good.setPriceStandart(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_STANDART.Index));
            good.setPriceShop(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index));
            good.setPriceRegion(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_REGION.Index));
            good.setPriceMarket(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_MARKET.Index));
            good.setPriceOptSection(rows
                    .getFloat(GoodsMetaData.FIELD_PRICE_SECTION.Index));
            good.price(rows.getFloat(colPrice));
            good.setBestBefore(rows
                    .getString(GoodsMetaData.FIELD_BEST_BEFORE.Index));
            good.setNDS((byte) rows.getShort(GoodsMetaData.FIELD_NDS.Index));
            good.setCountry(rows
                    .getString(GoodsMetaData.FIELD_COUNTRY.Index));
            good.setWeight(rows.getFloat(GoodsMetaData.FIELD_WEIGHT.Index));
            good.setHighLight((byte) rows
                    .getShort(GoodsMetaData.FIELD_HIGHLIGHT.Index));

            good.setIsWeightGood(rows
                    .getShort(GoodsMetaData.FIELD_IS_WEIGHT.Index) == 1);
            good.isHalfHead(rows
                    .getShort(GoodsMetaData.FIELD_IS_HALFHEAD.Index) == 1);
            good.perishable(rows
                    .getShort(GoodsMetaData.FIELD_IS_PERISHABLE.Index) == 1);
            good.stm(rows
                    .getShort(GoodsMetaData.FIELD_STM.Index) == 1);
            good.setIsMercury(rows.getInt(GoodsMetaData.FIELD_IS_MERCURY.Index));
            good.setIsCustom(rows.getInt(GoodsMetaData.FIELD_IS_CUSTOM.Index));
            good.setNonCash(rows.getInt(GoodsMetaData.FIELD_IS_NON_CASH.Index) == 1);
            result.add(good);
        }

        rows.close();

        return result;
    }

    public static ArrayList<String> loadMustList(SQLiteDatabase db, String agent,
                                                 String category, String parent) {

        //Если категорию маст-листа мы не получили, то нет смысла грузить список
        //if (category == null) return null;

        ArrayList<String> result = null;
        Cursor rows = null;

        //Аргументы для запроса
        String[] selectionArgs = new String[2];
        selectionArgs[0] = agent;
        selectionArgs[1] = category;
        String sql = String.format("SELECT %s.%s FROM %s JOIN %s ON %s.%s = %s.%s "
                        + "WHERE %s = ? AND %s = 0 AND %s = ?",
                //SELECT
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
                //FROM
                GoodsMetaData.TABLE_NAME,
                //JOIN
                MustListMetaData.TABLE_NAME,
                //ON
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
                MustListMetaData.TABLE_NAME, MustListMetaData.FIELD_CODE_GOODS,
                //WHERE
                MustListMetaData.FIELD_CODE_AGENT, GoodsMetaData.FIELD_RECORD_TYPE.Name,
                MustListMetaData.FIELD_CATEGORY
        );

        //В случае поиска товаров родитель будет равен null
        if (parent != null) {
            sql = String.format("%s AND %s = ?", sql, GoodsMetaData.FIELD_PARENT.Name);

            selectionArgs = new String[3];

            selectionArgs[0] = agent;
            selectionArgs[1] = category;
            selectionArgs[2] = parent;
        }

        try {
            rows = db.rawQuery(sql, selectionArgs);

            if (rows.getCount() > 0) result = new ArrayList<>();

            while (rows.moveToNext()) {
                result.add(rows.getString(0));
            }
            rows.close();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null && !rows.isClosed()) {
                rows.close();
            }
        }

        return result;
    }

}
