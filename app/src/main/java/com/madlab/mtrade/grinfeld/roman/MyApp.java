package com.madlab.mtrade.grinfeld.roman;


import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madlab.mtrade.grinfeld.roman.connectivity.ApiC3;
import com.madlab.mtrade.grinfeld.roman.connectivity.ApiPromotions;
import com.madlab.mtrade.grinfeld.roman.connectivity.ConnectivityInterceptor;
import com.madlab.mtrade.grinfeld.roman.connectivity.ConnectivityInterceptorC3;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DatabaseHelperManager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.support.multidex.MultiDexApplication;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyApp extends MultiDexApplication {

    private DBHelper mHelper;
    private SQLiteDatabase mDB;
    @SuppressLint("StaticFieldLeak")
    private static volatile Context context;
    private static final String APP_PREFERENCES = "mysettings2";
    private static final String USER = "user";
    private static SharedPreferences mSettings;
    private static ApiPromotions api;
    private static ApiC3 apiC3;
    String TAG = "#MyAPP";

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    public static final String NOTIFICATION_CHANNEL_ID_SERVICE = "com.madlab.mtrade.grinfeld.roman.service";
    public static final String NOTIFICATION_CHANNEL_ID_INFO = "com.madlab.mtrade.grinfeld.roman.download_info";

    public void initChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (nm != null) {
                nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_SERVICE, "App Service", NotificationManager.IMPORTANCE_DEFAULT));
                nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, "Download Info", NotificationManager.IMPORTANCE_DEFAULT));
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        sAnalytics = GoogleAnalytics.getInstance(this);
        Fabric.with(this, new Crashlytics());
        AuthManager.initialize(this);
        DatabaseHelperManager.releaseDatabaseHelper();
        openDB();
        initChannel();
        initRetrofit();
        updateBaseContextLocale(context);
    }

    private Context updateBaseContextLocale(Context context) {
        Locale myLocale = new Locale("ru","RU");
        Locale.setDefault(myLocale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResourcesLocale(context, myLocale);
        }

        return updateResourcesLocaleLegacy(context, myLocale);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }

    private Context updateResourcesLocaleLegacy(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }


    private void initRetrofit() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ConnectivityInterceptor(this))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).build();
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.dalimo.ru/api/v1/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        api = retrofit.create(ApiPromotions.class);

        OkHttpClient clientС3 = new OkHttpClient.Builder()
                .addInterceptor(new ConnectivityInterceptorC3(this))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).build();
        Retrofit retrofitС3 = new Retrofit.Builder()
                .baseUrl("https://mtrade.dalimo.org/api/v1/")
                .client(clientС3)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiC3 = retrofitС3.create(ApiC3.class);

    }

    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }


    public static ApiPromotions getApi() {
        return api;
    }

    public static ApiC3 getApiC3() {
        return apiC3;
    }

    public static void setUser(String user){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(USER, user);
        editor.apply();
    }

    public static String getUser(){
        return mSettings.getString(USER, null);
    }


    public static Context getContext(){
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }


    private void openDB() {
        mHelper = new DBHelper(this);
        mDB = mHelper.getWritableDatabase();
    }

    public void closeDB() {
        if (mDB != null) {
            mDB.close();
        }
        if (mHelper != null) {
            mHelper.close();
        }
    }

    public synchronized SQLiteDatabase getDB() {
//        if (mDB==null || !mDB.isOpen()){
//            openDB();
//        }
        return mDB;
    }

}