package com.madlab.mtrade.grinfeld.roman.services;



import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.db.TaskMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Task;


public class SendTasksIntentService extends IntentService {
    private static final String TAG = "!->SendTasksIntentService";

    private final String PROC_NAME = "ОбновитьСтатусЗадачи";

    public static final String EXTRA_STATUS = "TaskStatus";
    public static final String EXTRA_MESSAGE = "ErrorMessage";

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.SendTasks";

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;

    private byte status = STATUS_OK;
    private String mMessage = "";
    private ArrayList<Task> mResult = null;

    private DalimoClient mClient;

    public SendTasksIntentService() {
        super(BROADCAST_ACTION);
    }

    @Override
    protected void onHandleIntent(Intent extras) {
        App settings = App.get(getApplicationContext());

        StringBuilder common = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;",
                Const.COMMAND, settings.getCodeAgent(), PROC_NAME));
        Task item = extras.getParcelableExtra(TaskMetaData.TABLE_NAME);
        //mResult = TaskLab.get(getApplicationContext()).getUnsendedTasks(settings.getCodeAgent());

        mClient = new DalimoClient(Credentials.load(getApplicationContext()));
        //for (Task item : mResult) {
            if (mClient.connect()) {
                @SuppressLint("DefaultLocale")
                String current = String.format("%s;%s;%d;" + "%s", item.UUID(), item.agent(), item.currentStatus(), Const.END_MESSAGE);
                mClient.send(common.toString()+current);

                mMessage = mClient.receive();

                if (mMessage.contains(DalimoClient.ERROR)){
                    status = STATUS_ERROR;
                }
            } else {
                status = STATUS_ERROR;
                mMessage = mClient.LastError();
            }

            mClient.disconnect();
        //}

        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, status);
        result.putExtra(EXTRA_MESSAGE, mMessage);
        sendBroadcast(result);
    }

}
