package com.madlab.mtrade.grinfeld.roman.tasks;



import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsMatrix;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;

public class LoadGroupMatrixTask  extends AsyncTask<String, Integer, Boolean>{
    private final String TAG = "!->LoadGroupMatrixTask";
    public static final byte LOAD_SUCCESS = 10;
    public static final byte LOAD_FAIL = 11;
    public static final byte LOAD_EMPTY = 12;

    private Activity baseContext;
    private ILoadGoodsComplete mTaskCompleteListener;
    private String mUnswer = "";
    byte mType;

    public String getMessage(){
        return mUnswer;
    }

    public LoadGroupMatrixTask(Activity context, ILoadGoodsComplete listener, byte mType){
        baseContext = context;
        mTaskCompleteListener = listener;
        this.mType = mType;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... clientCodes) {
        try{
            MyApp app = (MyApp)baseContext.getApplication();

            //формируем дерево товаров
            switch (mType){
                case GoodsList.MODE_MATRIX:
                    GoodsMatrix.loadGroup(app.getDB(), App.get(baseContext).getRootCode(), clientCodes[0]);
                    break;
                case GoodsList.MODE_STM:
                    GoodsMatrix.loadGroupSTM(app.getDB(), App.get(baseContext).getRootCode(), clientCodes[0]);
                    break;
                case GoodsList.MODE_PROMOTIONS:
                    break;
            }

        }
        catch(Exception e){
            Log.e(TAG, e.toString());
            mUnswer = e.toString();
            return false;
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mTaskCompleteListener != null){
            mTaskCompleteListener.onTaskComplete(this);
        }
    }
}

