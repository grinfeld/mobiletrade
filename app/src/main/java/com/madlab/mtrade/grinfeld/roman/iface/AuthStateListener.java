package com.madlab.mtrade.grinfeld.roman.iface;

/**
 * Created by GrinfeldRA on 02.07.2018.
 */

public interface AuthStateListener {

    void onAuthStateChanged(String employeeId);

}
