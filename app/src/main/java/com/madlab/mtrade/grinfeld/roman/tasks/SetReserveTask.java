package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveTaskComplete;

import org.json.JSONObject;

import java.util.Locale;
import java.util.StringTokenizer;

import okhttp3.Response;

/**
 * Created by GrinfeldRa
 */
public class SetReserveTask extends AsyncTask<Void, Void, Boolean> {

    private final static String TAG = "!->SetReserveTask";

    private final static String PROC_NAME = "СоздатьДокументЗадачФормат";

    private final static String SUCCESS = "OK";
    private final static String FAILURE = "ERR";

    private String mUnswer;
    private Credentials connectInfo;
    private Context baseContext;
    private ISetReserveTaskComplete taskComplete;
    private Task mTask;

    public SetReserveTask(Context baseContext, ISetReserveTaskComplete taskComplete, Task task) {
        this.baseContext = baseContext;
        this.taskComplete = taskComplete;
        connectInfo = Credentials.load(baseContext);
        mTask = task;
    }


    public String getmUnswer() {
        return mUnswer;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        App settings = App.get(baseContext);
        boolean success;
        String answer;
        DalimoClient myClient = null;
        try {
            StringBuilder header = new StringBuilder(String.format(Locale.getDefault(),
                    "%s;%s;%s;", Const.COMMAND,
                    mTask.agent(),
                    PROC_NAME));
            if (settings.connectionServer.equals("1")) {
                myClient = new DalimoClient(connectInfo);
                if (!myClient.connect()) {
                    return false;
                }
                String message = header.append(generateDataTask()).append(Const.END_MESSAGE).toString();
                myClient.send(message);
                answer = myClient.receive();
                myClient.disconnect();
            } else {
                String region = App.getRegion(baseContext);
                try {
                    Response response = OkHttpClientBuilder
                            .buildCall(PROC_NAME, generateDataTask(), region).execute();
                    if (response.isSuccessful()) {
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        answer = jsonObject.getString("data");
                    } else {
                        answer = FAILURE;
                    }
                } catch (Exception e) {
                    myClient = new DalimoClient(connectInfo);
                    if (!myClient.connect()) {
                        return false;
                    }
                    String message = header.append(generateDataTask()).append(Const.END_MESSAGE).toString();
                    myClient.send(message);
                    answer = myClient.receive();
                    myClient.disconnect();
                }
            }

            if (answer.contains(FAILURE)) {
                success = false;
                mUnswer += answer;
            } else {
                StringTokenizer st = new StringTokenizer(answer, ";");
                byte paramCount = 0;
                success = false;
                while (st.hasMoreElements()) {
                    String object = st.nextToken();
                    switch (paramCount) {
                        case 0:
                            success = object.contains(SUCCESS);
                            mTask.setReserved(true);
                            break;
                        default:
                            break;
                    }
                    paramCount++;
                }
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            success = false;
        } finally {
            mTask.setUnconfirmed(Task.SAVED_TASK);
            TaskLab.get(baseContext).updateTask(mTask);
            if (myClient != null)
                myClient.disconnect();
        }
        return success;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (taskComplete != null) {
            taskComplete.onTaskComplete(this);
        }
    }

    private String generateDataTask() {
        StringBuilder data = new StringBuilder(String.format(Locale.getDefault(),
                "%s;%s;%s;%s;" +
                        "%s;%s;%s;" +
                        "%s;%s;%s;" +
                        "%s;%s;%s;" +
                        "%s;%s;",
                mTask.UUID(),
                mTask.agent(),
                mTask.clientCode(),
                mTask.getManagerCode(),
                mTask.description(),
                TimeFormatter.sdf1C.format(mTask.dateDue()),
                TimeFormatter.sdf1C.format(mTask.dateCreation()),
                "", "", "",
                "", "", "",
                "", ""));

        for (GoodsInTask goodsInTasks : mTask.goodsList()) {
            int count = (int) (goodsInTasks.count() * goodsInTasks.getQuant());
            data.append(String.format("%s|%s|;", goodsInTasks.code(), count));
        }
        return data.toString();
    }
}
