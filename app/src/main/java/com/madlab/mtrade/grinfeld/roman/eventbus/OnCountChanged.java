package com.madlab.mtrade.grinfeld.roman.eventbus;

import com.madlab.mtrade.grinfeld.roman.entity.Goods;

/**
 * Created by GrinfeldRA
 */

public class OnCountChanged {

    private Goods goods;
    private int newCount;

    public OnCountChanged(Goods goods, int newCount) {
        this.goods = goods;
        this.newCount = newCount;
    }


    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public int getNewCount() {
        return newCount;
    }

    public void setNewCount(int newCount) {
        this.newCount = newCount;
    }
}
