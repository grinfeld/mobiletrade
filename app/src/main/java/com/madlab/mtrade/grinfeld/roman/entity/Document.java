package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRA on 14.11.2017.
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.ClientsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MonitoringMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaymentsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitMetaData;
import com.madlab.mtrade.grinfeld.roman.tasks.InitTime;

public class Document implements Parcelable {
    private static final String TAG = "!->DocumentsList";

    public static final byte NORMAL = 0;
    public static final byte RESERVED = 1;
    public static final byte DELETED = 2;

    public static final String KEY = "document";

    private byte typeMoney;

    /**
     * ID документа
     */
    private short mID;

    public short getID() {
        return mID;
    }

    public void setID(short data) {
        mID = data;
    }

    /**
     * Заголовок
     */
    private String mCaption;

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String info) {
        mCaption = info;
    }

    /**
     * Поле 1
     */
    private String mFieldLeft;

    public String getFieldLeft() {
        return mFieldLeft;
    }

    public void setFieldLeft(String info) {
        mFieldLeft = info;
    }

    /**
     * Поле 2
     */
    private String mFieldCenter;

    public String getFieldCenter() {
        return mFieldCenter;
    }

    public void setFieldCenter(String info) {
        mFieldCenter = info;
    }

    /**
     * Поле 3
     */
    private String mFieldRight;

    public String getFieldRight() {
        return mFieldRight;
    }

    public void setFieldRight(String info) {
        mFieldRight = info;
    }

    public String mCodeCli;

    public byte isHighLight;

    // Произвольная информация
    public String mTag;

    private byte typeReserveServer;

    public byte getTypeMoney() {
        return typeMoney;
    }

    public void setTypeMoney(byte typeMoney) {
        this.typeMoney = typeMoney;
    }

    // Конструктор
    public Document(short id, String caption, String left, String center,
                    String right, String tag, boolean highLight, byte typeReserveServer, byte typeMoney) {
        mID = id;
        mCaption = caption;

        mFieldLeft = left;
        mFieldCenter = center;
        mFieldRight = right;

        mTag = tag;

        isHighLight = 0;
        this.typeReserveServer = typeReserveServer;
        this.typeMoney = typeMoney;
    }

    // Конструктор
    public Document() {
        this((short) 0, "", "", "", "", "", false, (byte) 0, (byte) 0);
    }

    private Document(Parcel parce) {
        mID = (short) parce.readInt();

        mCaption = parce.readString();
        mFieldLeft = parce.readString();
        mFieldCenter = parce.readString();
        mFieldRight = parce.readString();

        mTag = parce.readString();
        typeReserveServer = parce.readByte();
        typeMoney = parce.readByte();
    }

    public static final Parcelable.Creator<Document> CREATOR = new Parcelable.Creator<Document>() {

        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        public Document[] newArray(int size) {
            return new Document[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mID);

        parcel.writeString(mCaption);

        parcel.writeString(mFieldLeft);
        parcel.writeString(mFieldCenter);
        parcel.writeString(mFieldRight);

        parcel.writeString(mTag);
        parcel.writeByte(typeReserveServer);
        parcel.writeByte(typeMoney);
    }

    private static final String PAYMENT_SEPARATOR = " док.№ ";

    private static ArrayList<Document> mList = new ArrayList<Document>();
    private static short mCount = 0;

    /**
     * Акссесор, дает доступ к списку документов
     */
    public static ArrayList<Document> getList() {
        return mList;
    }

    /**
     * Добавляет документ в конец списка
     */
    private static void Add(Document doc) {
        if (doc == null)
            return;

        if (mList.add(doc)) {
            mCount++;
        }
    }

    public byte getTypeReserveServer() {
        return typeReserveServer;
    }

    public void setTypeReserveServer(byte typeReserveServer) {
        this.typeReserveServer = typeReserveServer;
    }

    /**
     * Возвращает количество записей в списке
     */
    public static short getCount() {
        return mCount;
    }

    public static Document getItem(int index) {
        return mList.get(index);
    }

    /**
     * Загружает список документов из БД
     */
    public static ArrayList<Document> loadList(SQLiteDatabase db, Const.DOCUMENTS_TYPE TypeDoc) {
        /*
         * SELECT NumOrder, Name, TotalSum, TotalWeight, OrderState FROM Orders
         * INNER JOIN Clients ON (Clients.CodeCli = Orders.CodeCli)
         */
        String sql = "";
        switch (TypeDoc) {
            case Visit:
                sql = String
                        .format("SELECT %s, %s, %s," + "%s, %s, %s, " + "%s, %s "
                                        + "FROM %s " + "INNER JOIN %s " + "ON (%s = %s) "
                                        + "Group By %s",
                                // SELECT
                                VisitMetaData._ID,
                                VisitMetaData.FIELD_STATE,
                                VisitMetaData.FIELD_ORDER_SKU,
                                VisitMetaData.FIELD_ORDER_SUM,
                                VisitMetaData.FIELD_START_TIME,
                                ClientsMetaData.FIELD_CODE,
                                ClientsMetaData.FIELD_NAME,
                                VisitMetaData.FIELD_TYPE_SERVER,
                                // FROM
                                VisitMetaData.TABLE_NAME,
                                // INNER JOIN
                                ClientsMetaData.TABLE_NAME,
                                // ON
                                ClientsMetaData.FIELD_CODE,
                                VisitMetaData.FIELD_CLIENT_CODE,
                                // GROUP BY
                                VisitMetaData.FIELD_LOCATION_FK);
                break;
            case Order:
                sql = String.format(
                        "SELECT %s.%s, %s, " + "%s, %s, %s, %s, %s, %s, %s, %s "
                                + "FROM %s " + "INNER JOIN %s "
                                + "ON (%s.%s = %s.%s) " + "GROUP BY %s " + "ORDER BY %s ",
                        // Select
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_CODE,
                        ClientsMetaData.FIELD_NAME,

                        OrderMetaData.FIELD_NUM, OrderMetaData.FIELD_TOTAL_SUM,
                        OrderMetaData.FIELD_TOTAL_WEIGHT,
                        OrderMetaData.FIELD_STATE, OrderMetaData.FIELD_RESERVED,
                        OrderMetaData.FIELD_DATE_SHIP, OrderMetaData.FIELD_TYPE_SERVER,
                        OrderMetaData.FIELD_TYPE_DOC,
                        // From
                        OrderMetaData.TABLE_NAME,
                        // INNER JOIN
                        ClientsMetaData.TABLE_NAME,
                        // ON
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_CODE,
                        OrderMetaData.TABLE_NAME, OrderMetaData.FIELD_CLIENT,
                        // ORDER BY
                        OrderMetaData.FIELD_NUM,
                        // GROUP BY
                        OrderMetaData.FIELD_NUM);
                break;
            case Payment:
                sql = String.format(
                        "SELECT " + "%s.%s, " + // Client.CodeCli,
                                "%s.%s, " + // ID
                                "%s.%s, " + // Amount
                                "%s.%s, " + // Doc_number
                                "%s.%s, " + // doc_date
                                "%s.%s, " + // doc_sum
                                "%s.%s " + // sended sign
                                "FROM %s " + "INNER JOIN %s " + "ON %s.%s = %s.%s",
                        // ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_CODE,
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_NAME,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData._ID,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_AMOUNT,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_DOC_NUMBER,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_DOC_DATE,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_DOC_SUM,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_SENDED,
                        // from
                        PaymentsMetaData.TABLE_NAME,
                        // inner join
                        ClientsMetaData.TABLE_NAME,
                        // on
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_CODE,
                        PaymentsMetaData.TABLE_NAME,
                        PaymentsMetaData.FIELD_CODE_CLIENT);
                break;
            case Return:
                sql = String.format(
                        "SELECT %s, %s, %s, %s, %s, %s, %s, %s " + "FROM %s "
                                + "INNER JOIN %s " + "ON (%s.%s = %s.%s) "
                                + "ORDER BY %s",
                        // Select
                        ClientsMetaData.FIELD_NAME, ReturnMetaData.FIELD_NUM,
                        ReturnMetaData.FIELD_TOTAL_SUM,
                        ReturnMetaData.FIELD_TOTAL_WEIGHT,
                        ReturnMetaData.FIELD_STATE, ReturnMetaData.FIELD_DATE_SHIP,
                        ReturnMetaData.FIELD_SENDED, ReturnMetaData.FIELD_TYPE_SERVER,
                        // From
                        ReturnMetaData.TABLE_NAME,
                        // INNER JOIN
                        ClientsMetaData.TABLE_NAME,
                        // ON
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_CODE,
                        ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_CLIENT,
                        // ORDER BY
                        ReturnMetaData.FIELD_NUM);
                break;
            default:
                break;
        }

        Cursor results = null;
        try {
            mList = new ArrayList<>();
            mCount = 0;
            if (App.isMerch() && TypeDoc.equals(Const.DOCUMENTS_TYPE.Order)) {
                List<Monitoring> monitoringList = Monitoring.load(db, String.format(Locale.getDefault(),
                        "%s = %d", MonitoringMetaData.FIELD_UNCONFIRMED_MONITORING, 1));
                for (Monitoring monitoring : monitoringList){
                    Document doc = new Document();
                    doc.setID((short) monitoring.getId());
                    doc.mCodeCli = monitoring.getCodeClient();
                    doc.setCaption(monitoring.getClient().mName);
                    short reserved = (short) (monitoring.isReserved() ? 1 : 0);
//                    doc.setFieldLeft(String.format(Const.FORMAT_MONEY, sum,
//                            Locale.ENGLISH));
//                    doc.setFieldCenter(String.format(Const.FORMAT_WEIGHT,
//                            weight, Locale.ENGLISH));

                    doc.mTag = TimeFormatter.sdfDT.format(monitoring.getDate());
                    doc.setTypeReserveServer((byte) monitoring.getTypeServer());
                    byte colorHL = Document.NORMAL;
                    String txt = "";
                    if (monitoring.getState().contains(Order.STATE_NORMAL)) {
                        // Если зарезервирована - выделяем
                        if (reserved == 1) {
                            colorHL = Document.RESERVED;
                            txt = "Резерв";
                        } else {
                            txt = "Создана";
                        }
                    } else {
                        txt = "Удалена";
                        colorHL = Document.DELETED;
                    }
                    doc.setFieldRight(txt);
                    doc.isHighLight = colorHL;
                    doc.Add(doc);
                }
            } else {
                results = db.rawQuery(sql, null);
                while (results.moveToNext()) {
                    Document doc = new Document();
                    switch (TypeDoc) {
                        case Visit: {
                            short id = results.getShort(0);
                            doc.isHighLight = (byte) results.getShort(1);
                            short sku = results.getShort(2);
                            float amount = results.getFloat(3);
                            Date startTime = TimeFormatter.parseSqlDateWithTime(results.getString(4));

                            // String clientCode = results.getString(5);
                            String clientName = results.getString(6);
                            int i = results.getInt(7);
                            doc.setTypeReserveServer((byte) i);
                            doc.setID(id);
                            SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'  'HH:mm:ss");
                            isoFormat.setTimeZone(TimeZone.getTimeZone(GlobalProc.getTimeZone(MyApp.getContext())));
                            String date = isoFormat.format(startTime);
                            doc.mTag = date;

                            doc.setCaption(clientName);
                            doc.setFieldLeft(String.format(Const.FORMAT_MONEY, amount));
                            doc.setFieldCenter(String.format("SKU: %d ", sku));
                            switch (doc.isHighLight) {
                                case Document.NORMAL:
                                    doc.setFieldRight("Создан");
                                    break;
                                case Document.RESERVED:
                                    doc.setFieldRight("Резерв");
                                    break;
                                case Document.DELETED:
                                    doc.setFieldRight("Удалён");
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                        case Order: {

                            short id = results.getShort(2);

                            if (id == Order.UNCONFIRMED_ORDER_NUMBER) {
                                continue;
                            }

                            String codeCli = results.getString(0);
                            String nameCli = results.getString(1);

                            float sum = results.getFloat(3);
                            float weight = results.getFloat(4);

                            String state = results.getString(5);
                            short reserved = results.getShort(6);

                            String dateShip = results.getString(7);

                            int i = results.getInt(8);
                            doc.setTypeReserveServer((byte) i);

                            byte b = (byte) results.getInt(9);
                            doc.setTypeMoney(b);

                            doc.setID(id);
                            doc.mCodeCli = codeCli;
                            doc.setCaption(nameCli);
                            doc.setFieldLeft(String.format(Const.FORMAT_MONEY, sum,
                                    Locale.ENGLISH));
                            doc.setFieldCenter(String.format(Const.FORMAT_WEIGHT,
                                    weight, Locale.ENGLISH));

                            doc.mTag = dateShip;

                            byte colorHL = Document.NORMAL;
                            String txt = "";
                            if (state.contains(Order.STATE_NORMAL)) {
                                // Если зарезервирована - выделяем
                                if (reserved == 1) {
                                    colorHL = Document.RESERVED;
                                    txt = "Резерв";
                                } else {
                                    txt = "Создана";
                                }
                            } else {
                                txt = "Удалена";
                                colorHL = Document.DELETED;
                            }
                            doc.setFieldRight(txt);
                            doc.isHighLight = colorHL;
                            break;
                        }
                        case Payment:
                            doc.setID(results.getShort(1));

                            doc.mTag = results.getString(3);

                            doc.setCaption(results.getString(0) + PAYMENT_SEPARATOR
                                    + doc.mTag);

                            doc.setFieldLeft(TimeFormatter.parseSqlTo1CDate(results
                                    .getString(4)));
                            doc.setFieldCenter(results.getString(2));
                            doc.setFieldRight(results.getString(5));

                            doc.isHighLight = (byte) results.getShort(6);
                            break;
                        case Return: {
                            doc.setID(results.getShort(1));
                            doc.setCaption(results.getString(0));
                            doc.setFieldLeft(String.format(Const.FORMAT_MONEY,
                                    results.getFloat(2), Locale.ENGLISH));
                            doc.setFieldCenter(String.format(Const.FORMAT_WEIGHT,
                                    results.getFloat(3), Locale.ENGLISH));
                            String state = results.getString(4);
                            String ds = results.getString(5);
                            doc.mTag = ds;

                            short reserved = results.getShort(6);

                            int i = results.getInt(7);
                            doc.setTypeReserveServer((byte) i);

                            byte colorHL = Document.NORMAL;
                            String txt;
                            if (state.contains(Order.STATE_NORMAL)) {
                                // Если зарезервирована - выделяем
                                if (reserved == 1) {
                                    colorHL = Document.RESERVED;
                                    txt = "Резерв";
                                } else {
                                    txt = "Создана";
                                }
                            } else {
                                txt = "Удалена";
                                colorHL = Document.DELETED;
                            }
                            doc.setFieldRight(txt);
                            doc.isHighLight = colorHL;
                            break;
                        }
                        default:
                            break;
                    }

                    Add(doc);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (results != null) {
                results.close();
            }
        }

        return mList;
    }
}
