package com.madlab.mtrade.grinfeld.roman.fragments;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;


public class SimpleGoodsListFragment extends ListFragment {
	public final static String PREF_SEARCH_QUERY = "SearchQuery";
	private final static byte IDD_MODIFY = 0;
	private static final String TAG = "#SimpleGoodsListFragment";

	private ArrayList<GoodsInDocument> mExistedItems;
	private ArrayList<Goods> mFullList;
	private ArrayList<Goods> mFilteredList;
	private byte mCurrentViewMode = GoodsAdapter.MODE_ORDER;
	private Client client;

	public static SimpleGoodsListFragment newInstance(ArrayList<GoodsInDocument> existedItems, Client client) {
		Bundle args = new Bundle();

		args.putParcelableArrayList(GoodsInDocument.KEY, existedItems);
		args.putParcelable(Client.KEY, client);
		// args.putByte(EXTRA_MODE, mode);

		SimpleGoodsListFragment result = new SimpleGoodsListFragment();
		result.setArguments(args);

		return result;
	}

	public GoodsAdapter mAdapter;

	public SimpleGoodsListFragment() {
		super();
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);

		if (getArguments() != null) {
			mExistedItems = getArguments().getParcelableArrayList(
					GoodsInDocument.KEY);
			client = getArguments().getParcelable(Client.KEY);

		}
	}

	public void setExistedItems(ArrayList<GoodsInDocument> list){
		mExistedItems = list;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		//inflater.inflate(R.menu.simple_goods, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		registerForContextMenu(getListView());
	}


	public void updateItems(ArrayList<Goods> goodsList, byte currentViewMode, boolean isForeign) {
		mFullList = goodsList;
		mCurrentViewMode = currentViewMode;

		if (goodsList != null) {
			mFilteredList = new ArrayList<>();
			for (Goods goods : goodsList) {
				if (goods.transitList() != null || goods.getRestOnStore() > 0) {
					mFilteredList.add(goods);
				}
			}
			mAdapter = new GoodsAdapter(getActivity(), goodsList, mExistedItems,
					currentViewMode, null, isForeign);
			setListAdapter(mAdapter);
		}else {
			setListAdapter(null);
		}
	}


	public void updateShipmentMustList(LinkedHashMap<String, String> mapResult) {
		if (mFullList!=null){
			for (Goods goods : mFullList){
				if (mapResult.containsKey(goods.getCode())){
					String key = mapResult.get(goods.getCode());
					boolean b = key.equals("1");
					goods.setSnipmentMustList(b);
				}
			}
			mAdapter.notifyAfterExistedListChanged();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Goods current = (Goods)getListAdapter().getItem(position);
		OrderItem orderItem = null;
		if (mExistedItems != null && current.inDocument()) {
			for (GoodsInDocument git : mExistedItems) {
				if (git.getCode().equalsIgnoreCase(current.getCode())){
					orderItem = new OrderItem(current);
					orderItem.setPrice(git.getPrice());
					//признаки
					orderItem.mIsSpecialPrice = git.isSpecPrice();
					orderItem.isSertificateNeed(git.isSertificate());
					orderItem.isQualityDocNeed(git.isQualityDoc());
					//Если штуки
					if (git.getMeashPosition() == 0){
						orderItem.inPack(false);
						orderItem.setCount(git.getCountPosition());
						orderItem.setCountPack(0);
					}else{//Если упаковки
						orderItem.inPack(true);
						orderItem.setCount(0);
						orderItem.setCountPack(git.getCountPosition());
					}
					break;
				}
			}
		}
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		Fragment fragment = new GoodsQuantityFragment();
		fragment.setTargetFragment(this, GoodsTreeFragment.IDD_ORDER_QUANTITY);
		Bundle bundle = new Bundle();
		bundle.putParcelable(Goods.KEY, current);
		bundle.putParcelable(OrderItem.KEY, orderItem);
		fragment.setArguments(bundle);
		ft.addToBackStack(null);
		ft.replace(R.id.contentMain, fragment);
		ft.commit();

//		if (mCurrentViewMode == GoodsAdapter.MODE_ORDER) {
//			Intent gqa = new Intent(getActivity(), GoodsQuantityActivity.class);
//			gqa.putExtra(Goods.KEY, current);
//			gqa.putExtra(OrderItem.KEY, orderItem);
//
//			startActivityForResult(gqa, IDD_MODIFY);
//		}
//		else if (mCurrentViewMode == GoodsAdapter.MODE_NON_LIQUID) {
//			if (orderItem != null) {
//				Intent gqa = new Intent(getActivity(),
//						GoodsQuantityActivity.class);
//				gqa.putExtra(Goods.KEY, current);
//				gqa.putExtra(OrderItem.KEY, orderItem);
//
//				startActivityForResult(gqa, IDD_MODIFY);
//			}
//		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
			case IDD_MODIFY:
				if (resultCode == Activity.RESULT_OK && data != null) {
					OrderItem orderItem = data.getParcelableExtra(OrderItem.KEY);

					GoodsInDocument gid = null;
					if (mExistedItems == null)
					{
						mExistedItems = new ArrayList<>();
					}
					else {
						for (GoodsInDocument item : mExistedItems) {
							if (orderItem.getCodeGoods().equalsIgnoreCase(item.getCode())){
								gid = item;

								gid.isSpecPrice(orderItem.mIsSpecialPrice);
								gid.setPrice(orderItem.getPrice());

								gid.isSertificate(orderItem.isSertificateNeed());
								gid.isQualityDoc(orderItem.isQualityDocNeed());

								//pieces
								if (!orderItem.inPack()){
									gid.setMeashPosition((byte)0);
									gid.setCountPosition(orderItem.getCount());
								}else{//pack meash
									gid.setMeashPosition((byte)1);
									gid.setCountPosition(orderItem.getCountPack());
								}
								break;
							}
						}
					}

					if (gid == null){//Добавляем
						gid = new GoodsInDocument(orderItem.getCodeGoods(),
								orderItem.inPack() ? orderItem.getCount() : orderItem.getCountPack(),
								orderItem.inPack() ? (byte) 1 : 0,
								orderItem.getQuant(), orderItem.getPrice(), orderItem.getWeight());

						gid.isSertificate(Order.getOrder().mSertificates);
						gid.isQualityDoc(Order.getOrder().mQualityDocs);

						mExistedItems.add(gid);
					}

					updateAdapterAfterAddItemsInOrder();
				}
				break;
			case GoodsTreeFragment.IDD_ORDER_QUANTITY:
				if (resultCode == Activity.RESULT_OK && data != null) {
					OrderItem orderItem = data.getParcelableExtra(OrderItem.KEY);
					Log.d(TAG, String.valueOf(orderItem));
					GoodsInDocument gid = null;
					if (mExistedItems == null) {
						mExistedItems = new ArrayList<>();
					} else {
						for (GoodsInDocument item : mExistedItems) {
							if (orderItem.getCodeGoods().equalsIgnoreCase(item.getCode())) {
								gid = item;
								gid.setMeashPosition(orderItem.inPack() ? (byte)1 : 0);
								gid.setCountPosition(orderItem.inPack() ? orderItem
										.getCountPack() : orderItem
										.getCount());
								gid.isSpecPrice(orderItem.mIsSpecialPrice);
								gid.setPrice(orderItem.getPrice());
								gid.isSertificate(orderItem.isSertificateNeed());
								gid.isQualityDoc(orderItem.isQualityDocNeed());
								break;
							}
						}
					}
					if (gid == null) {
						byte meash = orderItem.inPack() ? (byte)1 : 0;
						int count = orderItem.inPack() ? orderItem
								.getCountPack() : orderItem.getCount();
						gid = new GoodsInDocument(orderItem.getCodeGoods(), count,
								meash, orderItem.getQuant(), orderItem.getPrice(), orderItem.getWeight());
						gid.isSpecPrice(orderItem.mIsSpecialPrice);
						gid.setPrice(orderItem.getPrice());
						gid.isSertificate(orderItem.isSertificateNeed());
						gid.isQualityDoc(orderItem.isQualityDocNeed());
						mExistedItems.add(gid);
					}
					updateAdapterAfterAddItemsInOrder();
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.context_menu_goods_info, menu);
		MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				onContextItemSelected(item);
				return true;
			}
		};
		for (int i = 0, n = menu.size(); i < n; i++){
			menu.getItem(i).setOnMenuItemClickListener(listener);
		}
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menuGoods_info){
			AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			if (getListAdapter() != null){
				Goods goods = (Goods) getListAdapter().getItem(menuInfo.position);
				if (goods!=null){
					getFragmentManager().beginTransaction()
							.replace(R.id.contentMain, GoodsInfoContainer.newInstance(goods, client.isForeignAgent()))
							.addToBackStack(null)
							.commit();
				}
			}
		}
		return super.onContextItemSelected(item);
	}



	public void updateAdapterAfterAddItemsInOrder(){
		if (mAdapter != null) {
			mAdapter.setExistedItems(mExistedItems);
			mAdapter.notifyAfterExistedListChanged();
		}
	}


}
