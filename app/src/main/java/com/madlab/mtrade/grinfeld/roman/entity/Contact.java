package com.madlab.mtrade.grinfeld.roman.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.db.ContactMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grinfeldra
 */
public class Contact implements Parcelable {


    private static final String TAG = "#Contacts";
    private int id;
    private String codeClient;
    private String post;
    private String fio;
    private String tel;
    private String email;
    private int responsible;
    private String dateBirth;
    private String note;


    protected Contact(Parcel in) {
        id = in.readInt();
        codeClient = in.readString();
        post = in.readString();
        fio = in.readString();
        tel = in.readString();
        email = in.readString();
        responsible = in.readInt();
        dateBirth = in.readString();
        note = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public Contact(int id, String codeClient, String post, String fio, String tel, String email, int responsible, String dateBirth, String note) {
        this.id = id;
        this.codeClient = codeClient;
        this.post = post;
        this.fio = fio;
        this.tel = tel;
        this.email = email;
        this.responsible = responsible;
        this.dateBirth = dateBirth;
        this.note = note;
    }

    public static void deleteReserved(SQLiteDatabase db, String codeAgent) {
        db.delete(ContactMetaData.TABLE_NAME, ContactMetaData.FIELD_RESERVED.Name + " =? and "
                + ContactMetaData.FIELD_CODE_AGENT.Name + " = ?", new String[]{"1", codeAgent});
    }

    public static void updateContact(SQLiteDatabase db, ContentValues values, int id) {
        db.update(ContactMetaData.TABLE_NAME, values, ContactMetaData.FIELD_ID + "=" + id, null);
    }

    public static void updateReservedContact(SQLiteDatabase db, int id) {
        ContentValues values = new ContentValues();
        values.put(ContactMetaData.FIELD_RESERVED.Name, 1);
        db.update(ContactMetaData.TABLE_NAME, values, ContactMetaData.FIELD_ID + "=" + id, null);
    }

    public static List<Contact> loadContacts(SQLiteDatabase db, String codeClient, String codeAgent) {
        List<Contact> res = new ArrayList<>();
        Cursor rows = null;
        try {
            rows = db.rawQuery("select * from " + ContactMetaData.TABLE_NAME + " where "
                    + ContactMetaData.FIELD_CODE_AGENT.Name + " = ? and "
                    + ContactMetaData.FIELD_CODE_CLIENT.Name + " = ? ", new String[]{codeAgent, codeClient});
            Contact r;
            while (rows.moveToNext()) {
                int id = rows.getInt(ContactMetaData.FIELD_ID.Index);
                String codeCli = rows.getString(ContactMetaData.FIELD_CODE_CLIENT.Index);
                String post = rows.getString(ContactMetaData.FIELD_POST.Index);
                String fio = rows.getString(ContactMetaData.FIELD_FIO.Index);
                String tel = rows.getString(ContactMetaData.FIELD_TEL.Index);
                String email = rows.getString(ContactMetaData.FIELD_EMAIL.Index);
                int responsible = rows.getInt(ContactMetaData.FIELD_RESPONSIBLE.Index);
                String dateBirth = rows.getString(ContactMetaData.FIELD_DATE_BIRTH.Index);
                String note = rows.getString(ContactMetaData.FIELD_NOTE.Index);
                r = new Contact(id, codeCli, post, fio, tel, email, responsible, dateBirth, note);
                res.add(r);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

    public static List<Contact> loadAll(SQLiteDatabase db, String codeAgent) {
        List<Contact> res = new ArrayList<>();
        Cursor rows = null;
        try {
            rows = db.rawQuery("select * from " + ContactMetaData.TABLE_NAME + " where "
                    + ContactMetaData.FIELD_CODE_AGENT.Name + " = ?", new String[]{codeAgent});
            Contact r;
            while (rows.moveToNext()) {
                int id = rows.getInt(ContactMetaData.FIELD_ID.Index);
                String codeClient = rows.getString(ContactMetaData.FIELD_CODE_CLIENT.Index);
                String post = rows.getString(ContactMetaData.FIELD_POST.Index);
                String fio = rows.getString(ContactMetaData.FIELD_FIO.Index);
                String tel = rows.getString(ContactMetaData.FIELD_TEL.Index);
                String email = rows.getString(ContactMetaData.FIELD_EMAIL.Index);
                int responsible = rows.getInt(ContactMetaData.FIELD_RESPONSIBLE.Index);
                String dateBirth = rows.getString(ContactMetaData.FIELD_DATE_BIRTH.Index);
                String note = rows.getString(ContactMetaData.FIELD_NOTE.Index);
                r = new Contact(id, codeClient, post, fio, tel, email, responsible, dateBirth, note);
                res.add(r);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

    public static boolean deleteContact(SQLiteDatabase db, int id) {
        return db.delete(ContactMetaData.TABLE_NAME, ContactMetaData.FIELD_ID + "=" + id, null) > 0;
    }

    public int getId() {
        return id;
    }

    public void setCodeClient(String codeClient) {
        this.codeClient = codeClient;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setResponsible(int responsible) {
        this.responsible = responsible;
    }

    public void setDateBirth(String dateBirth) {
        this.dateBirth = dateBirth;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCodeClient() {
        return codeClient;
    }

    public String getPost() {
        return post;
    }

    public String getFio() {
        return fio;
    }

    public String getTel() {
        return tel;
    }

    public String getEmail() {
        return email;
    }

    public int getResponsible() {
        return responsible;
    }

    public String getDateBirth() {
        return dateBirth;
    }

    public String getNote() {
        return note;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(codeClient);
        dest.writeString(post);
        dest.writeString(fio);
        dest.writeString(tel);
        dest.writeString(email);
        dest.writeInt(responsible);
        dest.writeString(dateBirth);
        dest.writeString(note);
    }


}
