package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;
import android.util.Log;

import java.util.Locale;

public final class ClientsMetaData implements BaseColumns {
    public static final String TABLE_NAME = "Clients";

    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CODE = "CodeCli";
    public static final String FIELD_NAME = "Name";
    public static final String FIELD_FULL_NAME = "FullName";
    public static final String FIELD_INN = "Inn";
    public static final String FIELD_ADDRESS = "PostAddress";
    public static final String FIELD_PHONES = "Phone";
    public static final String FIELD_DIRECTOR = "Chief";
    public static final String FIELD_CONTACT = "Manager";
    public static final String FIELD_PRICE_TYPE = "TypePrice";
    public static final String FIELD_DISCOUNT = "Discount";
    public static final String FIELD_MAX_ARREAR = "MaxCredit";
    public static final String FIELD_CUR_ARREAR = "CurCredit";
    public static final String FIELD_AVG_ORDER = "AvgOrder";
    public static final String FIELD_VISIT = "Visit";
    public static final String FIELD_START_DATE = "StartDate";
    public static final String FIELD_VISIT_PLAN = "VisitPlan";
    public static final String FIELD_IS_TODAY = "TodayVisit";
    public static final String FIELD_IS_STOP = "StopShipment";
    public static final String FIELD_TYPE_PAY = "TypePay";
    public static final String FIELD_LATITUDE = "Latitude";
    public static final String FIELD_LONGITUDE = "Longitude";
    public static final String FIELD_SKU = "SKU";
    public static final String FIELD_SHIPPING = "ShippingPlan";
    public static final String FIELD_FIRMS = "FirmsArray";
    public static final String FIELD_TYPE_SALES_PLAN = "TypeSalesPlan";
    public static final String FIELD_SORT_ORDER = "SortOrder";
    public static final String FIELD_CATEGORY = "MLCategory";
    public static final String FIELD_UPPER_NAME = "UpperName";
    public static final String FIELD_REG_MERCURY = "RegMercury";
    public static final String FIELD_FOREIGN_AGENT = "ForeignAgent";
    public static final String FIELD_LAST_UPDATE_CONTACT = "last_update_contact";

    public static final byte FIELD_CODE_AGENT_INDEX = 0;
    public static final byte FIELD_CODE_INDEX = 1;
    public static final byte FIELD_NAME_INDEX = 2;
    public static final byte FIELD_FULL_NAME_INDEX = 3;
    public static final byte FIELD_INN_INDEX = 4;
    public static final byte FIELD_ADDRESS_INDEX = 5;
    public static final byte FIELD_PHONES_INDEX = 6;
    public static final byte FIELD_DIRECTOR_INDEX = 7;
    public static final byte FIELD_CONTACT_INDEX = 8;
    public static final byte FIELD_PRICE_TYPE_INDEX = 9;
    public static final byte FIELD_DISCOUNT_INDEX = 10;
    public static final byte FIELD_MAX_ARREAR_INDEX = 11;
    public static final byte FIELD_CUR_ARREAR_INDEX = 12;
    public static final byte FIELD_AVG_ORDER_INDEX = 13;
    public static final byte FIELD_VISIT_INDEX = 14;
    public static final byte FIELD_START_DATE_INDEX = 15;
    public static final byte FIELD_VISIT_PLAN_INDEX = 16;
    public static final byte FIELD_IS_TODAY_INDEX = 17;
    public static final byte FIELD_IS_STOP_INDEX = 18;
    public static final byte FIELD_TYPE_PAY_INDEX = 19;
    public static final byte FIELD_LATITUDE_INDEX = 20;
    public static final byte FIELD_LONGITUDE_INDEX = 21;
    public static final byte FIELD_SKU_INDEX = 22;
    public static final byte FIELD_SHIPPINGPLAN_INDEX = 23;
    public static final byte FIELD_FIRMS_INDEX = 24;
    public static final byte FIELD_SORT_ORDER_INDEX = 25;
    public static final byte FIELD_CATEGORY_INDEX = 26;
    public static final byte FIELD_SALES_PLAN_INDEX = 27;
    public static final byte FIELD_REG_MERCURY_INDEX = 29;
    public static final byte FIELD_FOREIGN_AGENT_INDEX = 30;
    public static final byte FIELD_LAST_UPDATE_CONTACT_INDEX = 31;

    public static final String INDEX_TODAY_VISIT = String.format(
            "CREATE INDEX idxClientTodayVisit ON %s ( %s )", TABLE_NAME,
            FIELD_IS_TODAY);

    public static final String INDEX_SORT_ORDER = String.format(
            "CREATE INDEX idxSortOrder ON %s ( %s )", TABLE_NAME,
            FIELD_SORT_ORDER);

    public static final String CREATE_COMMAND = String
            .format("CREATE TABLE %s ("
                            + "%s	nvarchar(6)		NOT NULL DEFAULT '',"
                            + "%s	nvarchar(6)		NOT NULL CONSTRAINT PK_ClientCode PRIMARY KEY,"
                            + "%s	nvarchar(32)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(50)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(20)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(100)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(50)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(60)	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(60)	NOT NULL DEFAULT '',"
                            + "%s	smallint		NOT NULL DEFAULT 1,"
                            + "%s	int				NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"    // макс. долг
                            + "%s	money			NOT NULL DEFAULT 0,"    // тек. долг
                            + "%s	money			NOT NULL DEFAULT 0,"    // Средняя заявка
                            + "%s	nchar(7)		NOT NULL DEFAULT '0000000',"// график визитов
                            + "%s	nchar(8)		NOT NULL DEFAULT '',"
                            + "%s	nchar(62)		NOT NULL DEFAULT '00000000000000000000000000000000000000000000000000000000000000',"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	nchar(1)		NOT NULL DEFAULT '0'," // Стоп-отгрузка
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	REAL	    	NOT NULL DEFAULT 0," // latitude
                            + "%s	REAL	    	NOT NULL DEFAULT 0," // longitude
                            + "%s	int		    	NOT NULL DEFAULT 0," // SKU counts
                            + "%s	nvarchar(38)   	NOT NULL DEFAULT ''," // Shipping plan
                            + "%s	nvarchar(20)   	NOT NULL DEFAULT '',"  // Firms array
                            + "%s	smallint		NOT NULL DEFAULT 0," // sortOrder
                            + "%s	nvarchar(30)   	NOT NULL DEFAULT ''," //MustList category
                            + "%s   int NOT NULL DEFAULT 0, "
                            + "%s	nvarchar(32)	NOT NULL DEFAULT '',"
                            + "%s	smallint		NOT NULL DEFAULT 1,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	text		NOT NULL DEFAULT '')", // SALES PLAN TYPE
                    TABLE_NAME, FIELD_CODE_AGENT, FIELD_CODE, FIELD_NAME,
                    FIELD_FULL_NAME, FIELD_INN, FIELD_ADDRESS, FIELD_PHONES,
                    FIELD_DIRECTOR, FIELD_CONTACT, FIELD_PRICE_TYPE,
                    FIELD_DISCOUNT, FIELD_MAX_ARREAR, FIELD_CUR_ARREAR,
                    FIELD_AVG_ORDER, FIELD_VISIT, FIELD_START_DATE,
                    FIELD_VISIT_PLAN, FIELD_IS_TODAY, FIELD_IS_STOP,
                    FIELD_TYPE_PAY, FIELD_LATITUDE, FIELD_LONGITUDE, FIELD_SKU,
                    FIELD_SHIPPING, FIELD_FIRMS, FIELD_SORT_ORDER, FIELD_CATEGORY,
                    FIELD_TYPE_SALES_PLAN, FIELD_UPPER_NAME, FIELD_REG_MERCURY, FIELD_FOREIGN_AGENT,
                    FIELD_LAST_UPDATE_CONTACT);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    /**
     * Возвращает текст параметризованного запроса к БД. Требуется указать код
     * клиента
     */
    public static String SELECT = String.format(
            "SELECT * FROM %s WHERE %s = ?", TABLE_NAME, FIELD_CODE);

    public static String insertQuery(String codeAgent, String codeCli,
                                     String name, String nameFull, String inn, String address,
                                     String phones, String director, String contact, byte typePrice,
                                     String discount, String maxCredit, String curCredit,
                                     String avgOrder, String visit, String startDate, String visitPlan,
                                     char isToday, char isStop, byte typeDoc, String lat, String lon,
                                     String sku, String shippingPlan, String firms, String category,
                                     int sortOrder, int typeSalesPlan, byte regMercury, byte foreignAgent,
                                     String lastDateUpdateContact) {
        return String.format(Locale.getDefault(),
                "INSERT INTO %s (" + "%s, %s, %s, " + // 1
                        "%s, %s, %s, " + // 2
                        "%s, %s, %s, " + // 3
                        "%s, %s, %s, " + // 4
                        "%s, %s, %s, " + // 5
                        "%s, %s, %s, " + // 6
                        "%s, %s, %s," + // 7
                        "%s, %s, %s," + // 8
                        "%s, %s, %s, %s, %s, %s, %s, %s" + // 9
                        ") VALUES (" + "'%s', '%s', '%s', " + // 1
                        "'%s', '%s', '%s', " + // 2
                        "'%s', '%s', '%s', " + // 3
                        "%s, %s, %s, " + // 4
                        "%s, %s, '%s', " + // 5
                        "'%s', '%s', %s, " + // 6
                        "'%s', %d, %s, " + // 7
                        "%s, %s, '%s', " + // 8
                        "'%s', '%s', %d, %d, '%s', %d, %d, '%s')", // 9
                TABLE_NAME,
                // Fields
                FIELD_CODE_AGENT,
                FIELD_CODE,
                FIELD_NAME, // 1
                FIELD_FULL_NAME,
                FIELD_INN,
                FIELD_ADDRESS, // 2
                FIELD_PHONES,
                FIELD_DIRECTOR,
                FIELD_CONTACT, // 3
                FIELD_PRICE_TYPE,
                FIELD_DISCOUNT,
                FIELD_MAX_ARREAR, // 4
                FIELD_CUR_ARREAR,
                FIELD_AVG_ORDER,
                FIELD_VISIT, // 5
                FIELD_START_DATE,
                FIELD_VISIT_PLAN,
                FIELD_IS_TODAY, // 6
                FIELD_IS_STOP,
                FIELD_TYPE_PAY,
                FIELD_LATITUDE, // 7
                FIELD_LONGITUDE,
                FIELD_SKU,
                FIELD_SHIPPING, // 8
                FIELD_FIRMS,
                FIELD_CATEGORY,
                FIELD_SORT_ORDER,// 9
                FIELD_TYPE_SALES_PLAN,
                FIELD_UPPER_NAME,
                FIELD_REG_MERCURY,
                FIELD_FOREIGN_AGENT,
                FIELD_LAST_UPDATE_CONTACT,
                // Values
                codeAgent, codeCli, name, nameFull, inn, address, phones,
                director, contact, typePrice, discount, maxCredit, curCredit,
                avgOrder, visit, startDate, visitPlan, isToday, isStop,
                typeDoc, lat, lon, sku, shippingPlan, firms, category, sortOrder, typeSalesPlan,
                name.toUpperCase(), regMercury, foreignAgent, lastDateUpdateContact);
    }
}
