package com.madlab.mtrade.grinfeld.roman;

import android.content.Context;
import android.telephony.TelephonyManager;

public class PhoneInfo {

    private String mIMEI;
    private String mPhoneNum;
    private String mManufacturer;
    private String mModel;
    private String mOSVersion;

    public String getIMEI() {
        return mIMEI;
        // StringBuilder res = new StringBuilder();
        // for (int i = 0; i < mIMEI.length(); i += 5)
        // {
        // int end = i + 5;
        // if (end > mIMEI.length()) end = mIMEI.length() - 1;
        // res.append(String.format("%s ", mIMEI.substring(i, end)));
        // }
        // return res.toString();
    }

    public String getPhoneNum() {
        return mPhoneNum;
    }

    public String getManufacturer() {
        return mManufacturer;
    }

    public String getModel() {
        return mModel;
    }

    public String getOSVersion() {
        return mOSVersion;
    }

    public PhoneInfo() {
        mIMEI = null;
        mPhoneNum = null;

        mManufacturer = android.os.Build.MANUFACTURER.toLowerCase();
        mModel = android.os.Build.MODEL.toLowerCase();
        mOSVersion = android.os.Build.VERSION.RELEASE;
    }

    public static PhoneInfo getInfo(Context ctx) {
        String defID = ctx.getString(R.string.defaultDeviceCode);
        String defPhoneNum = ctx.getString(R.string.defaultPhoneNum);

        PhoneInfo res = new PhoneInfo();
        res.mIMEI = defID;
        res.mPhoneNum = defPhoneNum;

        TelephonyManager telephonyManager = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            res.mIMEI = telephonyManager.getDeviceId();
            String number = telephonyManager.getLine1Number();
            if (number != null && number.length() > 0) {
                res.mPhoneNum = number;
            }
        }

        return res;
    }
}
