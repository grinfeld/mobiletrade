package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.barteksc.pdfviewer.PDFView;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;


/**
 * Created by GrinfeldRA
 */
public class PDFViewerFragment extends Fragment {

    private static final String KEY_TYPE_LOAD_DOC = "KEY_TYPE_LOAD_DOC";
    public static final String FAQ = "faq";
    public static final String NETWORKING_STANDARD = "network";

    public static PDFViewerFragment newInstance(String type) {

        Bundle args = new Bundle();
        args.putString(KEY_TYPE_LOAD_DOC, type);
        PDFViewerFragment fragment = new PDFViewerFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pdf_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PDFView pdfView = view.findViewById(R.id.pdfView);
        if (getArguments() != null) {
            String loadType = getArguments().getString(KEY_TYPE_LOAD_DOC);
            if (loadType != null) {
                ((IDynamicToolbar)getActivity()).onChangeToolbar(loadType);
                switch (loadType) {
                    case FAQ:
                        pdfView.fromAsset("faq.pdf").load();
                        break;
                    case NETWORKING_STANDARD:
                        pdfView.fromAsset("networking_standards.pdf").load();
                        break;
                }
            }
        }
    }

}
