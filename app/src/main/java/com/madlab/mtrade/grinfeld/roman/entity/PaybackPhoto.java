package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by grinfeldra
 */
public class PaybackPhoto {

    private int id;
    private String url;


    public PaybackPhoto(int id, String url) {
        this.id = id;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

}
