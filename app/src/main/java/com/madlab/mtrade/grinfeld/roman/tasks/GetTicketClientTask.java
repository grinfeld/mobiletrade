package com.madlab.mtrade.grinfeld.roman.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.TicketClient;
import com.madlab.mtrade.grinfeld.roman.iface.IGetTicketClientTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public class GetTicketClientTask extends AsyncTask<String, Void, ArrayList<TicketClient>>{

    private final String PROC_NAME = "ПолучитьЗаявкиПоКлиенту";
    public final static byte RES_SUCCESS = 0;
    public final static byte RES_FAIL = -1;
    private ProgressDialog progress;
    Activity context;
    private String codeCli;
    private DalimoClient mClient;
    private String mUnswer = "";
    private String date;
    private IGetTicketClientTask listener;

    public GetTicketClientTask(Activity context, String codeCli, String date, IGetTicketClientTask listener) {
        this.context = context;
        this.codeCli = codeCli;
        this.date = date;
        this.listener = listener;
    }

    @Override
    protected ArrayList<TicketClient> doInBackground(String... params) {

        String message = null;
        if (params != null){
            message = params[0];
        }
        if (message == null){
            StringBuilder data = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s",
                    Const.COMMAND, App.get(context).getCodeAgent(), PROC_NAME, codeCli, date, Const.END_MESSAGE));
            Credentials connectInfo = Credentials.load(context);
            mClient = new DalimoClient(connectInfo);
            mClient.connect();
            mClient.send(data.toString());
            message = mClient.receive();
            mClient.disconnect();
        }
        ArrayList<TicketClient> ticketClientsList = new ArrayList<>();
        // А теперь обрабатываем полученное сообщение
        if (message.contains(DalimoClient.ERROR)) {
            int index = message.indexOf(DalimoClient.ERROR);
            if (index > 0) {
                mUnswer = message.substring(0, index);
            }
            return null;
        }
        if (!message.equals("")){
            String[] split = message.split(";");
            for (String stringData : split){
                StringTokenizer token = new StringTokenizer(stringData, "<>");
                String nameTicket = token.nextToken();
                String manager  = token.nextToken();
                String amount  = token.nextToken();
                ticketClientsList.add(new TicketClient(nameTicket,manager,amount));
            }
        }
        return ticketClientsList;
    }

    @Override
    protected void onPostExecute(ArrayList<TicketClient> clients) {
        listener.onGetTicketClientTaskComplete(this);
    }

}


