package com.madlab.mtrade.grinfeld.roman.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;

public class OrderItem implements Parcelable, Cloneable {
    public static final String KEY = "order_item";


    /**
     * Признак скоропорта
     */
    private boolean isPerishable;

    public void perishable(boolean value) {
        isPerishable = value;
    }

    public boolean perishable() {
        return isPerishable;
    }

    public boolean isNonCash() {
        return isNonCash;
    }

    public void setNonCash(boolean nonCash) {
        isNonCash = nonCash;
    }

    public int getIsMercury() {
        return isMercury;
    }

    public void setIsMercury(int isMercury) {
        this.isMercury = isMercury;
    }

    public interface ICheckedChanged{
        void onCheckedChanged(OrderItem item);
    }

    private static ICheckedChanged mListener;

    public static void setCheckedListener(ICheckedChanged listener){
        mListener = listener;
    }

    public int getIsCustom() {
        return isCustom;
    }

    public void setIsCustom(int isCustom) {
        this.isCustom = isCustom;
    }

    private int isCustom = 0;

    private int isMercury = 0;

    private boolean isNonCash = false;

    /**
     * Если не нужно выделять цветом
     */
    public static final byte H_NOTHING = 0;

    /**
     * Позиция принадлежит маст-листу
     */
    public static final byte H_MUSTLIST = 1;

    /**
     * Новинка
     */
    public static final byte H_NEWBEE = 2;

    /**
     * Цена снижена
     */
    public static final byte H_DISCOUNT = 3;

    /**
     * Нет на остатках
     */
    public static final byte H_ZERO_COUNT = 99;

    // TODO: Поля
    // ---------------------------------------------------------------------------------------------------
    /**
     * Номер заявки
     */
    private Short mNumOrder;

    public void setNumOrder(short number) {
        mNumOrder = number;
    }

    public short getNumOrder() {
        return mNumOrder;
    }

    /**
     * Код товара
     */
    private String mCodeGoods;

    public void setCodeGoods(String codeGoods) {
        mCodeGoods = codeGoods;
    }

    public String getCodeGoods() {
        return mCodeGoods;
    }

    /**
     * Название товара
     */
    private String mNameGoods;

    public String getNameGoods() {
        return mNameGoods;
    }

    public void setNameGoods(String name) {
        mNameGoods = name;
    }

    /**
     * Количество
     */
    private int mCount;

    public int getCount() {
        return mCount;
    }

    public float getCountInPieces() {
        float cntInPcs = 1f;
        if (mInPack){
            cntInPcs = mCountPack * mNumInPack;
        }else{
            if (mIsWeightGoods){//Счёт в головках
                cntInPcs = mCount;
            }else{
                //Если квант дробный, то в колесиках у нас кол-во умножается на квант
                if (isFractional()){
                    cntInPcs = mCount;
                }else{
                    cntInPcs = mCount * mQuant;
                }
            }
        }
        return cntInPcs;
    }

    /**
     * Количество товара. Виртуальное. Итоговое будет складываться из умножения
     * на количество упаковок и квант
     *
     * @param cnt
     */
    public void setCount(int cnt) {
        mCount = cnt;
        recalcAmount();
    }

    /**
     * Количество упаковок (множитель)
     */
    private int mCountPack;

    public int getCountPack() {
        return mCountPack;
    }

    public void setCountPack(int cnt) {
        mCountPack = cnt;
        recalcAmount();
    }

    /**
     * Количество в упаковке (справочно)
     */
    private float mNumInPack;

    /**
     * Геттер для количества в упаковке (справочник)
     */
    public float numInPack() {
        return mNumInPack;
    }

    /**
     * Сеттер для количества в упаковке (справочник)
     */
    public void numInPack(float numInPack) {
        mNumInPack = numInPack;
    }

    /**
     * Цена
     */
    private float mPrice;

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
        recalcAmount();
    }

    /**
     * Квант
     */
    private float mQuant = 1f;

    public float getQuant() {
        return mQuant;
    }

    public void setQuant(float quant) {
        mQuant = quant;
    }

    /**
     * Проверяет, дробный квант или нет...
     *
     * @return
     */
    public boolean isFractional() {
        return (mQuant - (int) mQuant) > 0;
    }

    /**
     * Вес единицы товара
     */
    private float mWeight;

    /**
     * Справочный вес
     * @return
     */
    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        mWeight = GlobalProc.round(weight, 3);
    }

    /**
     * Итоговый вес.
     * Рассчитывается в зависимости от количества.
     */
    public float getTotalWeight(){
        float res = mWeight * getCountInPieces();
        return mIsHalfHead ? res / 2 : res;
    }

    /**
     * Итоговая сумма
     */
    private float mAmount;

    public float getAmount() {
        return mAmount;
    }

    /**
     * Произвольная информация о товаре
     */
    private String mInfo;

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    /**
     * Признак необходимости качественного удостоверения
     */
    private boolean mQualityDocNeed;

    public boolean isQualityDocNeed() {
        return mQualityDocNeed;
    }

    public void isQualityDocNeed(boolean data) {
        mQualityDocNeed = data;
    }

    /**
     * Признак необходимости сертификата
     */
    private boolean mSertificateNeed;

    public boolean isSertificateNeed() {
        return mSertificateNeed;
    }

    public void isSertificateNeed(boolean data) {
        mSertificateNeed = data;
    }

    /**
     * Специальная цена (признак)
     */
    public boolean mIsSpecialPrice;

    /**
     * Признак весового товара
     */
    public boolean mIsWeightGoods;

    /**
     * Признак половины головки
     */
    public boolean mIsHalfHead;

    /**
     * Подсветить
     */
    public byte mHighLight;

    /**
     * Признак отсутствия на складе
     */
    private boolean mNotInStock;

    /**
     * Сеттер. Признак отсутствия на складе
     */
    public void notInStock(boolean notInStock) {
        mNotInStock = notInStock;
    }

    /**
     * Геттер. Признак отсутствия на складе
     */
    public boolean notInStock() {
        return mNotInStock;
    }

    /**
     * Признак счета в упаковках
     */
    private boolean mInPack;

    /**
     * Меняет штуки на упаковки. При этом пересчитываются итоги
     * TRUE - упаковки, FALSE - штуки
     * @param newValue
     *            значение
     */
    public void changePack(boolean newValue) {
        mInPack = newValue;
        if (newValue) {
            mCount = 0;
            mCountPack = 1;
        } else {
            mCount = 1;
            mCountPack = 0;
        }
        recalcAmount();
    }

    public void inPack(boolean newValue) {
        mInPack = newValue;
    }

    public boolean inPack() {
        return mInPack;
    }

    /**
     * Отметка элемента заявки (например, для удаления)
     */
    private boolean mIsChecked;

    public boolean isChecked() {
        return mIsChecked;
    }

    /**
     * Устанавливает отметку для элемента
     *
     * @param value
     *            новое значение
     */
    public void isChecked(boolean value) {
        mIsChecked = value;
    }

    /**
     * Остаток на складе, нужен для адаптера
     */
    private float mRestOnStore;

    public float restOnStore() {
        return mRestOnStore;
    }

    public void restOnStore(float r) {
        mRestOnStore = r;
    }

    // Конструктор
    public OrderItem(short numOrder, String codeGoods, String nameGoods,
                     int count, int countPack, float numInPack, float price,
                     float weight, float quant, String info, boolean qualityDoc,
                     boolean sertificatDoc, boolean specialPrice, boolean weightGoods,
                     boolean halfHead, byte highLight, boolean inPack, float restOnStor) {
        mNumOrder = numOrder;
        mCodeGoods = codeGoods;
        mNameGoods = nameGoods;

        mCount = count;
        mCountPack = countPack;
        mNumInPack = numInPack;
        mPrice = price;
        mWeight = weight;
        mQuant = quant;

        mInfo = info;

        mHighLight = highLight;

        mQualityDocNeed = qualityDoc;
        mSertificateNeed = sertificatDoc;
        mIsSpecialPrice = specialPrice;
        mIsWeightGoods = weightGoods;
        mIsHalfHead = halfHead;

        mInPack = inPack;

        mRestOnStore = restOnStor;

        recalcAmount();
    }

    public OrderItem() {
        this((short) 0, "", "", 0, 0, 0, 0f, 0f, 1f, "", false, false, false,
                false, false, H_NOTHING, false, 0f);
    }

    /**
     * Конструктор
     *
     * @param goods
     */
    public OrderItem(Goods goods) {
        this((short) 0, goods.getCode(), goods.getNameFull(), 0, 0, goods
                .getNumInPack(), goods.price(), goods
                .getWeight(), goods.getQuant(), "", false, false, false, goods
                .isWeightGood(), false, goods.getHighLight(), false, goods.getRestOnStore());
        recalcAmount();
        // mTransitList = goods.getTransitList();
    }

    /**
     *  Увеличивает количество на квант
     */
    public void inc() {
        ++mCount;
        recalcAmount();
    }

    /**
     *  Уменьшает количество на квант
     */
    public void dec() {
        if (mCount >= 1) {
            --mCount;
        }
        recalcAmount();
    }

    /*
     * Пересчитывает итоговую стоимость позиции при изменении показателей
     */
    public void recalcAmount() {
        if (mIsWeightGoods) {
            if (mIsHalfHead) {
                mAmount = (mWeight / 2) * mCount * mPrice;
            } else {
                mAmount = mWeight * mCount * mPrice;
            }
        } else {
            if (mInPack) {
                mAmount = mCountPack * mNumInPack * mPrice;
            } else {
                mAmount = mCount * mQuant * mPrice;
            }
        }
    }

    /**
     * Меняем текущую отметку на противоположную
     */
    public void toggle(){
        mIsChecked = !mIsChecked;
        if (mListener != null){
            mListener.onCheckedChanged(this);
        }
    }

    private OrderItem(Parcel parce) {
        mNumOrder = (short) parce.readInt();
        mCodeGoods = parce.readString();
        mNameGoods = parce.readString();

        mCount = parce.readInt();
        mCountPack = parce.readInt();
        mNumInPack = parce.readFloat();
        mPrice = parce.readFloat();
        mWeight = parce.readFloat();
        mQuant = parce.readFloat();

        mInfo = parce.readString();

        mHighLight = parce.readByte();

        mRestOnStore = parce.readFloat();

        boolean[] bb = new boolean[6];
        parce.readBooleanArray(bb);

        mQualityDocNeed = bb[0];
        mSertificateNeed = bb[1];
        mIsSpecialPrice = bb[2];
        mIsWeightGoods = bb[3];
        mIsHalfHead = bb[4];
        mInPack = bb[5];

        recalcAmount();
    }

    public static final Parcelable.Creator<OrderItem> CREATOR = new Parcelable.Creator<OrderItem>() {

        public OrderItem createFromParcel(Parcel in) {
            return new OrderItem(in);
        }

        public OrderItem[] newArray(int size) {
            return new OrderItem[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mNumOrder);
        parcel.writeString(mCodeGoods);
        parcel.writeString(mNameGoods);

        parcel.writeInt(mCount);
        parcel.writeInt(mCountPack);
        parcel.writeFloat(mNumInPack);
        parcel.writeFloat(mPrice);
        parcel.writeFloat(mWeight);
        parcel.writeFloat(mQuant);

        parcel.writeString(mInfo);

        parcel.writeByte(mHighLight);

        parcel.writeFloat(mRestOnStore);

        boolean[] b = new boolean[] { mQualityDocNeed, mSertificateNeed,
                mIsSpecialPrice, mIsWeightGoods, mIsHalfHead, mInPack };

        parcel.writeBooleanArray(b);
    }
}
