package com.madlab.mtrade.grinfeld.roman.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TreeSet;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.components.WheelView;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnClickItem;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnCountChanged;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnLongClickItem;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnMeashChanged;
import com.madlab.mtrade.grinfeld.roman.fragments.VisitContainFragment;
import com.madlab.mtrade.grinfeld.roman.wheel.OnWheelScrollListener;
import com.madlab.mtrade.grinfeld.roman.wheel.adapters.ArrayWheelAdapter;

import org.greenrobot.eventbus.EventBus;

import cn.refactor.library.SmoothCheckBox;

public class GoodsAdapter extends ArrayAdapter<Goods> {
    // private final static String TAG = "!->GoodsAdapter";

    private int requestCode;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private TreeSet<Integer> sectionHeader = new TreeSet<>();

    /**
     * Режим для подбора товаров в заявку
     */
    public final static byte MODE_ORDER = 0;

    /**
     * Режим для подбора товаров в возрат
     */
    public final static byte MODE_RETURN = 1;

    /**
     * Режим справочника
     */
    public final static byte MODE_REFERENCE = 2;

    /**
     * Режим подбора со склада брака
     */
    public final static byte MODE_NON_LIQUID = 3;
    private static final String TAG = "#GoodsAdapter";

    private ArrayList<Goods> mFullList;
    private ArrayList<Goods> mFilteredList;
    private GoodsFilter mFilter;

//    private ICallbacks mCallback;
//
//    public interface ICallbacks {
//        void onCountChanged(Goods goods, int newCount);
//
//        void onMeashChanged(Goods goods, byte newValue);
//    }

    /**
     * Настройка приложения Штуки/упаковки
     */
    private boolean mDefaultMeashValue;
    /**
     * Значение штуки/упаковки
     */
    private boolean mInPieces;

    private short mMaxItemsCount = 30;

    private static Drawable IMG_COMPASS;
    private static Drawable IMG_INFO;
    private static Drawable IMG_STAR;
    private String priceFlag = null;
    private boolean isForeignClient = false;

    private byte mCurrentMode;

    ArrayList<GoodsInDocument> mExistedPositionList;

    public void setExistedItems(ArrayList<GoodsInDocument> existed) {
        mExistedPositionList = existed;
    }

    /**
     * При изменении списка существующих товаров, чтобы не загружать из БД
     * заново, уведомляем адаптер
     */
    public void notifyAfterExistedListChanged() {
        if (mFullList != null) {
            if (mExistedPositionList == null) {
                for (Goods goods : mFullList) {
                    goods.inDocument(false);
                }
            } else {
                for (Goods goods : mFullList) {
                    for (GoodsInDocument item : mExistedPositionList) {
                        if (!(goods.getType() == Goods.Type.BONUS.getValue()) && goods.getCode().contains(item.getCode())) {
                            goods.inDocument(true);
                            break;
                        }
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    /**
     * Адаптер для подбора товаров
     *
     * @param context     Контекст
     * @param items       список элементов
     * @param currentMode
     */
    public GoodsAdapter(Activity context, ArrayList<Goods> items,
                        ArrayList<GoodsInDocument> existedItems, byte currentMode, String priceFlag, boolean isForeignClient) {
        super(context, R.layout.goods_list_item, items);

        //mCallback = (ICallbacks) context.getFragmentManager().findFragmentById(R.id.contentMain);
        this.priceFlag = priceFlag;
        mFullList = items;//new ArrayList<>();
        //mFullList.addAll(items);
        mExistedPositionList = existedItems;
        mCurrentMode = currentMode;
        this.isForeignClient = isForeignClient;
        mMaxItemsCount = App.get(getContext()).getMaxWheelItemsCount();

        IMG_COMPASS = ContextCompat.getDrawable(context, R.mipmap.compass);
        IMG_INFO = ContextCompat.getDrawable(context, R.mipmap.information);
        IMG_STAR = ContextCompat.getDrawable(context, R.mipmap.star);

        mFilteredList = mFullList;
        mFilter = new GoodsFilter();

        mDefaultMeashValue = App.get(getContext()).getMeash()
                .equalsIgnoreCase(context.getString(R.string.pieces));
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    public void addItem(Goods goods) {
        mFullList.add(goods);
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem() {
        sectionHeader.add(mFullList.size());
        mFullList.add(new Goods());
        notifyDataSetChanged();
    }


    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        int listViewItemType = getItemViewType(position);
        LayoutInflater vi = (LayoutInflater) MyApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (listViewItemType == TYPE_SEPARATOR) {
            convertView = vi.inflate(R.layout.goods_list_separator, parent, false);
            return convertView;
        }
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.goods_list_item, parent, false);
            holder.icon = convertView.findViewById(R.id.gli_image);
            holder.tvName = convertView.findViewById(R.id.gli_tvGoodsName);
            holder.tvCount = convertView.findViewById(R.id.gli_tvRest);
            holder.tvPrice = convertView.findViewById(R.id.gli_tvPrice);
            holder.tvInfo = convertView.findViewById(R.id.gli_tvInfo);
            holder.tvInOrder = convertView.findViewById(R.id.gli_tvInOrder);
            holder.wheelCount = convertView.findViewById(R.id.gli_wheelCount);
            holder.wheelMeash = convertView.findViewById(R.id.gli_wheelMeash);
            holder.tvCodeGoods = convertView.findViewById(R.id.gli_tvCodeGoods);
            holder.txt_reserve = convertView.findViewById(R.id.txt_reserve);
            holder.checkBox = convertView.findViewById(R.id.cb_merch);
            holder.frame_check = convertView.findViewById(R.id.frame_check);
            holder.gli_imagePrice = convertView.findViewById(R.id.gli_imagePrice);
            holder.gli_imageStore = convertView.findViewById(R.id.gli_imageStore);
            holder.txt_mercury = convertView.findViewById(R.id.txt_mercury);
            holder.txt_is_custom = convertView.findViewById(R.id.txt_is_custom);
            holder.txt_non_cash = convertView.findViewById(R.id.txt_non_cash);
            holder.txt_perishable = convertView.findViewById(R.id.txt_perishable);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mFullList == null && mFilteredList == null) {
            return convertView;
        }

        Goods g = mFilteredList.get(position);
        int index = mFullList.indexOf(g);

        Goods goods = null;
        if (-1 < index && index < mFullList.size()) {
            goods = mFullList.get(index);
        }
        if (goods != null) {
            if (holder.icon != null) {
                if (goods.getHighLight() == 4) {
                    holder.icon.setImageDrawable(IMG_STAR);
                    if (holder.tvInfo != null) {
                        holder.tvInfo.setVisibility(View.VISIBLE);
                        holder.tvInfo.setText("ХИТ");
                    }
                } else {
                    if (holder.tvInfo != null) {
                        holder.tvInfo.setVisibility(View.GONE);
                    }
                    if (goods.transitList() != null) {
                        holder.icon.setImageDrawable(IMG_COMPASS);
                    } else {
                        holder.icon.setImageDrawable(IMG_INFO);
                    }
                }
                holder.icon.setTag(goods);
            }

            if (holder.tvName != null) {
                int color = Const.COLOR_GOLD;
                switch (g.getHighLight()) {
                    case 1: // MustList
                        color = Color.GREEN;
                        break;
                    default:
                        break;
                }

                holder.tvName.setText(goods.getName());
                holder.tvName.setTextColor(color);
                if (goods.isShipmentMustList()) {
                    holder.tvName.setTextColor(Color.GREEN);
                }
            }
            holder.tvCodeGoods.setText("[" + goods.getCode() + "]");
            holder.tvCount.setText(goods.getRestOnStore() + " "
                    + goods.getBaseMeash());
//            List<Remainder> remainderList = Remainder.load(getContext(),goods.getCode());
            Float totalReserve = null;
//            if (remainderList != null) {
//                for (Remainder r : remainderList){
//                    if (!r.getReserve().isEmpty()){
//                        totalReserve =+ Float.valueOf(r.getReserve());
//                    }
//                }
//            }
            if (totalReserve != null) {
                holder.txt_reserve.setVisibility(View.VISIBLE);
                holder.txt_reserve.setText("Резерв: " + totalReserve);
            } else {
                holder.txt_reserve.setVisibility(View.GONE);
            }
            //TODO проставить реальную цену
            String tmp = GlobalProc.formatMoney(goods.price());

            holder.tvPrice.setText(GlobalProc.toForeignPrice(goods.price(), isForeignClient));

            if (goods.inDocument() && goods.getType() != Goods.Type.BONUS.getValue()) {
                holder.tvInOrder.setVisibility(View.VISIBLE);
            } else {
                holder.tvInOrder.setVisibility(View.GONE);
            }
            if (requestCode == VisitContainFragment.IDD_NEW_TASK) {
                holder.tvPrice.setVisibility(View.GONE);
                holder.tvCount.setVisibility(View.GONE);
                holder.gli_imageStore.setVisibility(View.GONE);
                holder.gli_imagePrice.setVisibility(View.GONE);
            }
            if (App.isMerch() && priceFlag == null && requestCode != VisitContainFragment.IDD_NEW_TASK) {
                holder.tvInOrder.setVisibility(View.GONE);
                holder.tvPrice.setVisibility(View.GONE);
                holder.tvCount.setVisibility(View.GONE);
                holder.gli_imageStore.setVisibility(View.GONE);
                holder.gli_imagePrice.setVisibility(View.GONE);
                if (priceFlag == null) {
                    holder.frame_check.setVisibility(View.VISIBLE);
                    if (goods.inDocument()) {
                        holder.checkBox.setChecked(true, false);
                    } else {
                        holder.checkBox.setChecked(false, false);
                    }
                    Goods finalGoods = goods;
                    holder.checkBox.setOnClickListener(view -> {
                        if (holder.checkBox.isChecked()) {
                            EventBus.getDefault().post(new OnCountChanged(finalGoods, 0));
                        } else {
                            EventBus.getDefault().post(new OnCountChanged(finalGoods, 1));
                        }
                    });
                }
            } else {
                switch (mCurrentMode) {
                    case MODE_ORDER:
                        initWheelMeash(holder.wheelMeash, goods, priceFlag);
                        initWheel(holder.wheelCount, goods, true, priceFlag);
                        break;
                    case MODE_RETURN:
                        initWheelMeash(holder.wheelMeash, goods, priceFlag);
                        initWheel(holder.wheelCount, goods, false, priceFlag);
                        break;
                    case MODE_NON_LIQUID:
                        holder.tvInfo.setVisibility(View.VISIBLE);
                        holder.tvInfo.setText(goods.getInfo());
                        // initWheelMeash(holder.wheelMeash, goods);
                        initWheel(holder.wheelCount, goods, false, priceFlag);
                        break;
                    default:
                        break;
                }
            }
            if (goods.getIsMercury() == 1) {
                holder.txt_mercury.setVisibility(View.VISIBLE);
            } else {
                holder.txt_mercury.setVisibility(View.GONE);
            }

            if (goods.getIsCustom() == 1) {
                holder.txt_is_custom.setVisibility(View.VISIBLE);
            } else {
                holder.txt_is_custom.setVisibility(View.GONE);
            }

            if (goods.isNonCash()) {
                holder.txt_non_cash.setVisibility(View.VISIBLE);
            } else {
                holder.txt_non_cash.setVisibility(View.GONE);
            }

            if (goods.perishable()) {
                holder.txt_perishable.setVisibility(View.VISIBLE);
            } else {
                holder.txt_perishable.setVisibility(View.GONE);
            }
        }
        if (App.isMerch()) {
            Goods finalGoods1 = goods;
            convertView.setOnClickListener(view ->
                    EventBus.getDefault().post(new OnClickItem(finalGoods1, position)));
            View finalConvertView = convertView;
            convertView.setOnLongClickListener(view -> {
                EventBus.getDefault().post(new OnLongClickItem(finalConvertView));
                return true;
            });
        }
        return convertView;

    }

    private void initWheel(WheelView wheel, final Goods goods, boolean quanted, String priceFlag) {
        if (wheel == null)
            return;
        if (priceFlag != null || goods.getType() == Goods.Type.BONUS.getValue()) {
            wheel.setVisibility(View.GONE);
            return;
        }
        wheel.setTag(goods);
        wheel.setCyclic(true);
        wheel.setVisibleItems(App.get(getContext()).getVisibleWheelItems() + 1);
        wheel.setVisibility(View.VISIBLE);

        /**
         * переменная содержит шаг для адапетра, в том случае, если у нас штуки
         * или упаковки
         */

        float quant = 1;
        if (quanted)
            if (mDefaultMeashValue)
                quant = goods.isWeightGood() ? 1f : goods.getQuant();
            else
                quant = 1f;

        // Для сыров (весового товара) делаем исключение
        boolean isFractional = !goods.isWeightGood() && goods.isFractional();

        int currentItemPosition = 0;
        short max = mMaxItemsCount;

        // Если товар есть в документе, то нам нужно сформировать свой адаптер,
        // так
        // как могут быть упаковки
        if (mExistedPositionList != null && goods.inDocument()) {
            for (GoodsInDocument item : mExistedPositionList) {
                if (goods.getCode().contains(item.getCode())) {
                    if (item.getMeashPosition() == 0) {
                        if (quanted) {
                            quant = goods.isWeightGood() ? 1f : goods.getQuant();
                        }
                    } else {
                        quant = 1;
                    }
                    currentItemPosition = item.getCountPosition();
                    break;
                }
            }

            if (currentItemPosition > mMaxItemsCount) {
                max = (short) (currentItemPosition + 1);
            }
        } else {
            currentItemPosition = 0;
        }

        ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<>(
                getContext(), createAdapter(quant, max, isFractional));
        awa.setTextSize(Const.WHEEL_TEXT_SIZE);

        wheel.setViewAdapter(awa);

        wheel.setCurrentItem(currentItemPosition);

        // Здесь, потому что ширина рассчитывется при установке адаптера
        // wheel.setMinimumWidth(WHEEL_WIDTH);

        // Слушатели
        wheel.removeScrollingListener(scrollingListener);
        wheel.addScrollingListener(scrollingListener);
    }

    private void initWheelMeash(WheelView wheelMeash, final Goods goods, String priceFlag) {

        if (wheelMeash == null)
            return;
        if (priceFlag != null || goods.getType() == Goods.Type.BONUS.getValue() || requestCode == VisitContainFragment.IDD_NEW_TASK) {
            wheelMeash.setVisibility(View.GONE);
            return;
        }

        wheelMeash.setTag(goods);
        mInPieces = mDefaultMeashValue;

        // Если товар есть в документе, то проверяем, штуки у нас или упаковки
        int currentValue = mInPieces ? 0 : 1;
        if (mExistedPositionList != null && goods.inDocument()) {
            for (GoodsInDocument item : mExistedPositionList) {
                if (goods.getCode().equalsIgnoreCase(item.getCode())) {
                    currentValue = item.getMeashPosition();
                    mInPieces = item.getMeashPosition() == 0;
                    break;
                }
            }
        }

        if (goods.isWeightGood()) {
            if (currentValue >= 1) {
                wheelMeash.setVisibility(View.VISIBLE);
            } else {
                wheelMeash.setVisibility(View.GONE);
                return;
            }
        } else {
            wheelMeash.setVisibility(View.VISIBLE);
        }

        wheelMeash.setVisibility(View.VISIBLE);

        String[] list = goods.isWeightGood() ?
                new String[]{"гол.", "кор."} : new String[]{goods.getBaseMeash(), goods.getPackMeash()};

        ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<String>(
                getContext(), list);

        awa.setTextSize(Const.WHEEL_TEXT_SIZE);
        wheelMeash.setViewAdapter(awa);

        wheelMeash
                .setVisibleItems(App.get(getContext()).getVisibleWheelItems() + 1);

        wheelMeash.setCurrentItem(mInPieces ? 0 : 1);

        //Он нужен ЗДЕСЬ
        //wheelMeash.setMinimumWidth(WHEEL_WIDTH);

        // Слушатель изменения
        wheelMeash.removeScrollingListener(scrollingListener);
        wheelMeash.addScrollingListener(scrollingListener);
    }

    private String[] createAdapter(float quant, short items_count,
                                   boolean isFractional) {
        String[] result = new String[items_count];
        for (int i = 0; i < items_count; i++) {
            float res = GlobalProc.round(i * quant, 2);
            result[i] = isFractional ? new DecimalFormat("#.##").format(res).replace(",", ".") : Integer
                    .toString((int) res);// Integer.toString((int) res);
        }
        return result;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private final OnWheelScrollListener scrollingListener = new OnWheelScrollListener() {
        @Override
        public void onScrollingStarted(WheelView wheel) {
        }

        @Override
        public void onScrollingFinished(WheelView wheel) {
            Goods goods = (Goods) wheel.getTag();
            if (goods == null) {
                return;
            }
            int newValue = wheel.getCurrentItem();
            switch (wheel.getId()) {
                case R.id.gli_wheelCount:
                    EventBus.getDefault().post(new OnCountChanged(goods, newValue));
                    break;
                case R.id.gli_wheelMeash:
                    EventBus.getDefault().post(new OnMeashChanged(goods, (byte) newValue));
                    break;
            }
        }
    };

    public void setRequestCode(int targetRequestCode) {
        requestCode = targetRequestCode;
    }

    private static class ViewHolder {
        public ImageView icon;

        TextView tvName;
        TextView tvCount;
        TextView tvPrice;
        TextView tvInfo;
        TextView tvInOrder;
        TextView tvCodeGoods;
        TextView txt_reserve;
        TextView txt_mercury;
        TextView txt_is_custom;
        TextView txt_non_cash;
        TextView txt_perishable;
        ImageView gli_imageStore, gli_imagePrice;

        FrameLayout frame_check;

        SmoothCheckBox checkBox;

        WheelView wheelMeash;
        WheelView wheelCount;
    }

    private class GoodsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            constraint = constraint.toString().toLowerCase();
            if (constraint != null && constraint.length() >= 0) {
                ArrayList<Goods> filteredItemsList = new ArrayList<>();
                for (Goods current : mFullList) {
                    if (current.getName().toLowerCase().contains(constraint)) {
                        filteredItemsList.add(current);
                    }
                }
                result.values = filteredItemsList;
                result.count = filteredItemsList.size();
            } else {
                synchronized (this) {
                    result.values = mFullList;
                    result.count = mFullList.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            mFilteredList = (ArrayList<Goods>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0; i < mFilteredList.size(); i++) {
                add(mFilteredList.get(i));
            }
            notifyDataSetInvalidated();
        }

    }

    @Override
    public int getCount() {
        return mFullList == null ? 0 : mFullList.size();
    }
}
