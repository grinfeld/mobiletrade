package com.madlab.mtrade.grinfeld.roman.adapters;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.Datum;



public class GoodsGroupMatrixAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "#GoodsGroupMatrixAdapter";
    private List<Map<String, Node>> mNodes;
    private List<List<Map<String, Node>>> mSubNodes;

    private Context mContext;
    private Drawable expanded;
    private Drawable collapsed;
    private byte mode;
    List<Datum> data;

    public GoodsGroupMatrixAdapter(Context context,
                                   List<Map<String, Node>> groupData,
                                   List<List<Map<String, Node>>> childData, byte mode) {
        mContext = context;
        mNodes = groupData;
        mSubNodes = childData;
        this.mode = mode;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            collapsed = mContext.getResources().getDrawable(R.mipmap.add, context.getTheme());
            expanded = mContext.getResources().getDrawable(R.mipmap.remove, context.getTheme());
        } else {
            if (mContext!=null){
                collapsed = mContext.getResources().getDrawable(R.mipmap.add);
                expanded = mContext.getResources().getDrawable(R.mipmap.remove);
            }
        }
    }

    public GoodsGroupMatrixAdapter(Activity context, byte mType, List<Datum> data) {
        mContext = context;
        this.mode = mType;
        this.data = data;
    }


    public List<Datum> getAllGroups(){
        return data;
    }

    @Override
    public int getGroupCount() {
        switch (mode) {
            case GoodsList.MODE_PROMOTIONS:
                return data.size();
            default:
                return mNodes.size();
        }
    }



    @Override
    public int getChildrenCount(int groupPosition) {
        switch (mode) {
            case GoodsList.MODE_PROMOTIONS:
                return 0;
            default:
                return mSubNodes.get(groupPosition).size();
        }

    }

    @Override
    public Object getGroup(int groupPosition) {
        switch (mode) {
            case GoodsList.MODE_PROMOTIONS:
                return data.get(groupPosition);
            default:
                return mNodes.get(groupPosition);
        }

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        switch (mode) {
            case GoodsList.MODE_PROMOTIONS:
                return null;
            default:
                return mSubNodes.get(childPosition);
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (mode == GoodsList.MODE_PROMOTIONS) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.goods_item_promotions, null);

            }
            TextView itemId = v.findViewById(R.id.ggi_title);
            if (data.get(groupPosition).isChecked()){
                v.setBackgroundColor(mContext.getResources().getColor(R.color.colorGrey));
            }else {
                v.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
            }
            if (data.get(groupPosition).isRead()){
                itemId.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
            }else {
                itemId.setTextColor(mContext.getResources().getColor(R.color.colorGreen));
            }
            itemId.setText(data.get(groupPosition).getTitle());
            return v;
        } else {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.goods_group_item, null);
            }
            Map<String, Node> m = mNodes.get(groupPosition);
            Node node = m.get(Node.NODE_KEY);

            if (node != null) {
                TextView itemId = v.findViewById(R.id.ggi_title);
                if (itemId != null) {
                    Drawable drawable = isExpanded ? expanded : collapsed;
                    itemId.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                    String txt = String.format(Locale.getDefault(), "%s (%d)", node.getName(), node.childCount());
                    itemId.setText(txt);
                    itemId.setTextColor(Const.COLOR_GOLD);
                }

                if (node.isSelected()) {
                    v.setBackgroundColor(Color.GRAY);
                } else {
                    v.setBackgroundColor(Color.TRANSPARENT);
                }

                if (node.isExpanded()) {
                    ExpandableListView eLV = (ExpandableListView) parent;
                    if (eLV != null) {
                        eLV.expandGroup(groupPosition);
                    }
                }
            }

            return v;
        }
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_goods_child_matrix, null);
            }

            Map<String, Node> m = mSubNodes.get(groupPosition).get(childPosition);
            Node node = m.get(Node.SUBNODE_KEY);

            if (node != null) {
                TextView itemId = v.findViewById(R.id.ggi_title);
                if (itemId != null) {
                    String txt = String.format("%s (%d)", node.getName(), node.childCount());
                    itemId.setText(txt);
                }
            }
            return v;
    }




    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
