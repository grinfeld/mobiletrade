package com.madlab.mtrade.grinfeld.roman.entity;



import java.util.ArrayList;
import java.util.Locale;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TaskItemMetaData;

public class GoodsInTask implements Parcelable{
    private final static String TAG = "!->GoodsInTask";

    private String mCode;
    private String mName;

    private byte mPercentOfComplete;
    private short mTotalCount;
    private short mSold;
    private float quant;

    public String code(){
        return mCode;
    }

    public String name(){
        return mName;
    }

    public byte percent(){
        return mPercentOfComplete;
    }

    public short count(){
        return mTotalCount;
    }

    public short sold(){
        return mSold;
    }

    public GoodsInTask(String code, String name, byte percent, short count, short sold, float quant){
        mCode = code;
        mName = name;

        mTotalCount = count;
        mPercentOfComplete = percent;
        mSold = sold;
        this.quant = quant;
    }

    public static ArrayList<GoodsInTask> loadList(SQLiteDatabase db, String task_uuid_fk){
        ArrayList<GoodsInTask> result = null;

        String sql = String.format(Locale.ENGLISH, "SELECT * FROM %s INNER JOIN %s ON %s = %s WHERE %s = ?",
                TaskItemMetaData.TABLE_NAME,
                GoodsMetaData.TABLE_NAME,
                TaskItemMetaData.FIELD_GOODS,
                GoodsMetaData.FIELD_CODE.Name,
                TaskItemMetaData.FIELD_TASK_FK);
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, new String[] {task_uuid_fk});
            if (rows.moveToFirst()){
                result = new ArrayList<>();
                int goodsNameColumnIndex = rows.getColumnIndex(GoodsMetaData.FIELD_NAME.Name);
                do{
                    GoodsInTask git = new GoodsInTask(
                            rows.getString(TaskItemMetaData.FIELD_GOODS_INDEX),
                            rows.getString(goodsNameColumnIndex),
                            (byte)rows.getShort(TaskItemMetaData.FIELD_PERCENT_INDEX),
                            rows.getShort(TaskItemMetaData.FIELD_TOTAL_COUNT_INDEX),
                            rows.getShort(TaskItemMetaData.FIELD_SOLD_INDEX),
                            rows.getShort(TaskItemMetaData.FIELD_QUANT_INDEX));
                    result.add(git);

                    rows.moveToNext();
                }while(!rows.isAfterLast());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }finally{
            if (rows != null)
                rows.close();
        }
        return result;
    }

    public static final Parcelable.Creator<GoodsInTask> CREATOR = new Parcelable.Creator<GoodsInTask>() {

        public GoodsInTask createFromParcel(Parcel in) {
            return new GoodsInTask(in);
        }

        public GoodsInTask[] newArray(int size) {
            return new GoodsInTask[size];
        }
    };

    public GoodsInTask(Parcel parcel){
        mCode = parcel.readString();
        mName = parcel.readString();

        mTotalCount = (short)parcel.readInt();
        mPercentOfComplete = parcel.readByte();
        mSold = (short)parcel.readInt();
        quant = parcel.readInt();
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeString(mCode);
        parcel.writeString(mName);

        parcel.writeInt(mTotalCount);
        parcel.writeByte(mPercentOfComplete);
        parcel.writeInt(mSold);
        parcel.writeInt((int) quant);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public float getQuant() {
        return quant;
    }

    public void setQuant(float quant) {
        this.quant = quant;
    }
}
