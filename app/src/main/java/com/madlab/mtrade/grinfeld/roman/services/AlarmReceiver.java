package com.madlab.mtrade.grinfeld.roman.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;


/**
 * Created by GrinfeldRA
 */

public class AlarmReceiver extends BroadcastReceiver{


    private static final String TAG = "#AlarmReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "AlarmReceiver: "+GeolocationService.getServiceState().name());
        if (GeolocationService.getServiceState() == GeolocationService.State.STOP) {
            try {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    context.startService(new Intent(context, GeolocationService.class));
                }
            }catch (Exception e){
                Log.d(TAG, e.getMessage());
            }
        }
    }
}
