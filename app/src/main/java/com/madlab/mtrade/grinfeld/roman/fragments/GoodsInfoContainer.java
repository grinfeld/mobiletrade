package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA on 30.01.2018.
 */

public class GoodsInfoContainer extends Fragment {

    private static final String TAG = "#FragmentInfoContainer";
    public static final String POSITION = "position";
    public static final String KEY_FOREIGN = "key_foreign";

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View _rootView;
    Goods goods;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("");
            supportActionBar.setTitle("Информация о товаре");
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            goods = bundle.getParcelable(Goods.KEY);
        }
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.fragment_client_view_pager, container, false);
            viewPager = _rootView.findViewById(R.id.viewpager_visit);
            tabLayout = _rootView.findViewById(R.id.tabs_visit);
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
        return _rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupViewPager(ViewPager viewPager) {
        GoodsInfoContainer.ViewPagerAdapter adapter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            adapter = new GoodsInfoContainer.ViewPagerAdapter(getChildFragmentManager());
        } else {
            adapter = new GoodsInfoContainer.ViewPagerAdapter(getFragmentManager());
        }
        adapter.addFragmentTitle("Информация");
        adapter.addFragmentTitle("Изображение");
        if (goods != null && goods.transitList() != null) {
            adapter.addFragmentTitle("В пути");
        }
        viewPager.setAdapter(adapter);
    }

    public static Fragment newInstance(Goods goods, boolean isForeign) {
        GoodsInfoContainer container = new GoodsInfoContainer();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Goods.KEY, goods);
        bundle.putBoolean(KEY_FOREIGN, isForeign);
        container.setArguments(bundle);
        return container;
    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(POSITION, position);
            GoodsInfoFragment fragment = new GoodsInfoFragment();
            if (goods != null) {
                bundle.putParcelable(Goods.KEY, goods);
                bundle.putBoolean(KEY_FOREIGN, getArguments().getBoolean(KEY_FOREIGN));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fragment.setTargetFragment(getParentFragment(), getTargetRequestCode());
            } else {
                fragment.setTargetFragment(fragment, getTargetRequestCode());
            }
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        private void addFragmentTitle(String title) {
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
