package com.madlab.mtrade.grinfeld.roman;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.appupdater.AppUpdater;
import com.github.javiersantos.appupdater.AppUpdaterUtils;
import com.github.javiersantos.appupdater.enums.AppUpdaterError;
import com.github.javiersantos.appupdater.objects.Update;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.security.ProviderInstaller;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.PromotionCheckedMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;

import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.fragments.ClientListFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.ClientViewPagerContainer;
import com.madlab.mtrade.grinfeld.roman.fragments.GoodsInfoContainer;
import com.madlab.mtrade.grinfeld.roman.fragments.LocationFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.OrderCommonFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.OrderGoodsFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.PDFViewerFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.PaymentClientListFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.SalesPlansFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.SimpleGoodsListContainer;
import com.madlab.mtrade.grinfeld.roman.fragments.StatDocFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.SummaryFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.GoodsTreeFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.SettingsFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.StatisticsFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.TicketClientFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.VisitContainFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.VisitSelectFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.WorkModeFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.contacts.ContactEditorFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.contacts.ContactListFragment;
import com.madlab.mtrade.grinfeld.roman.help.HelpFragment;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IOnChangeSetting;
import com.madlab.mtrade.grinfeld.roman.services.GetParamsIntentService;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IOnChangeSetting, ProviderInstaller.ProviderInstallListener, DrawerLayout.DrawerListener, IDynamicToolbar {


    private static final String TAG = "#MainActivity";
    private static final String ARCHIVE_FILE = "Files.zip";
    private static final int IDD_REGISTER = 2;
    private boolean mRetryProviderInstall;
    public Toolbar toolbar;
    private boolean mToolBarNavigationListenerIsRegistered = false;
    private NavigationView navigationView;

    //флаг для включения списка товаров с возможностью только просмотра, - это в меню навигации "товары"
    public static final String PRICE = "price";
    boolean doubleBackToExitPressedOnce = false;
    private ActionBarDrawerToggle toggle;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    public TextView txt_fio;
    private MyBroadcastReceiver mMyBroadcastReceiver;
    TextView txt_version_name;
    public static volatile boolean flag = true;
    private Activity activity;
    String versionName;
    public static final int IDD_TICKET_ClIENT = 30;
    public static final int IDD_SALES_PLANS = 31;
    private static final int ERROR_DIALOG_REQUEST_CODE = 1;
    private DrawerLayout drawer;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String unswerCode = GlobalProc.getPref(getApplicationContext(), R.string.pref_unswerCode);
        boolean b = unswerCode != null && !unswerCode.isEmpty() && !unswerCode.equals("0123456789ABCDEFFFFF");
        Log.d(TAG, "onCreate: " + unswerCode + "  " + Security.getEtalonCode());
        Security.createDeviceCode(this, b);
        if (!checkRegistration()) {
            showRegisterActivity();
            return;
        }
        forcedTranslationToIIS();
        App.onChangeSetting = this;
        activity = this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        txt_version_name = header.findViewById(R.id.txt_version_name);
        txt_fio = header.findViewById(R.id.txt_fio);
        versionName = GlobalProc.getVersionName(this);
        txt_version_name.setText(String.format("Версия: %s", versionName));
        FragmentManager fragmentManager = getFragmentManager();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(2).setChecked(true);
        View inflate = getLayoutInflater().inflate(R.layout.menu_counter, null);
        navigationView.getMenu().getItem(2).setActionView(inflate);
        actionBar = getSupportActionBar();
//        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//        getFragmentManager().beginTransaction()
//                .replace(R.id.contentMain, new StatisticsFragment())
//                .commit();
        firsStart();
        startGoodsSelectFragment(GoodsList.MODE_PROMOTIONS, null, GoodsAdapter.MODE_ORDER, null);
        mMyBroadcastReceiver = new MyBroadcastReceiver();
        // регистрируем BroadcastReceiver
        IntentFilter intentFilter = new IntentFilter(GetParamsIntentService.BROADCAST_ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(mMyBroadcastReceiver, intentFilter);
        String fio = GlobalProc.getPref(this, R.string.pref_nameManager);
        if (fio != null && !fio.isEmpty()) {
            txt_fio.setText(fio + "\n[" + App.get(this).getCodeAgent() + "]");
        }
        String region = App.getRegion(this);
        String code = App.get(this).getCodeAgent();
        if (fio != null && !fio.isEmpty() &&
                region != null && !region.isEmpty() &&
                code != null && !code.isEmpty() && !code.equals(getString(R.string.lb_zero_code_manager))) {
            initFireBase(region, fio, code);
            AuthManager.getInstance().signInExplicitly(code);
        }
        AppUpdaterUtils appUpdaterUtils = new AppUpdaterUtils(this)
                .withListener(new AppUpdaterUtils.UpdateListener() {
                    @Override
                    public void onSuccess(Update update, Boolean isUpdateAvailable) {
                        new AppUpdater(MainActivity.this)
                                .setContentOnUpdateAvailable(getString(R.string.update_message, update.getLatestVersion()))
                                .setButtonDoNotShowAgain("")
                                .setButtonDoNotShowAgainClickListener(null)
                                .start();
                    }

                    @Override
                    public void onFailed(AppUpdaterError error) {
                        Log.d("AppUpdater Error", "Something went wrong");
                    }
                });
        appUpdaterUtils.start();
        getInstalledApplications();
        ProviderInstaller.installIfNeededAsync(this, this);
        drawer.addDrawerListener(this);

    }


    private void firsStart() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("FirstStart", 1);
        editor.apply();
    }


    private void forcedTranslationToIIS() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.pref_connection_server_boolean), true)) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(getString(R.string.pref_connection_server), "2");
            editor.putBoolean(getString(R.string.pref_connection_server_boolean), false);
            editor.apply();
        }
    }


    @Override
    protected void onResume() {
        getInstalledApplications();
        super.onResume();
    }

    private void getInstalledApplications() {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            String packageName = packageInfo.packageName.toLowerCase();
            if (packageName.contains("fakegps") || packageName.contains("fakeloc") || packageName.equals("org.hola.gpslocation") || packageName.equals("com.fly.gps")) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", packageName, null);
                intent.setData(uri);
                startActivity(intent);
            }
        }
    }


    private boolean checkRegistration() {
        String code = GlobalProc.getPref(getApplicationContext(), R.string.pref_unswerCode);
        Log.d(TAG, code + "  " + Security.getEtalonCode());
        return code != null && code.equalsIgnoreCase(Security.getEtalonCode());
    }

    private void showRegisterActivity() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        IDynamicToolbar iDynamicToolbar = this;
        iDynamicToolbar.onChangeToolbar(null);
        FragmentManager fragmentManager = getFragmentManager();
        int backStackCount = fragmentManager.getBackStackEntryCount();
        Fragment fragment = getFragmentManager().findFragmentById(R.id.contentMain);
        Log.d(TAG, "backStackCount1: " + backStackCount);
        if (fragment instanceof SettingsFragment) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction()
                    .replace(R.id.contentMain, new StatisticsFragment())
                    .commit();
        } else if (fragment instanceof VisitContainFragment) {
            //fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            FragmentManager manager;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                manager = fragment.getChildFragmentManager();
//            } else {
//                manager = getFragmentManager();
//            }
//            if (manager.findFragmentById(R.id.visit_bottom) instanceof TaskAddEditFragment) {
//                TaskAddEditFragment taskAddEditFragment = (TaskAddEditFragment) manager.findFragmentById(R.id.visit_bottom);
//                if (!App.isMerch()) {
//                    taskAddEditFragment.onTaskUpdated();
//                }
//                manager.popBackStack();
//                //fragmentManager.popBackStack();
//            } else {
//                fragmentManager.beginTransaction().remove(fragment).commit();
//                fragmentManager.popBackStack();
//                fragmentManager.beginTransaction()
//                        .replace(R.id.contentMain, new ClientViewPagerContainer())
//                        .commit();
//            }
            ((VisitContainFragment) fragment).completeVisitDialog();
        } else if (fragment instanceof GoodsTreeFragment && backStackCount > 0) {
            GoodsTreeFragment goodsTreeFragment = (GoodsTreeFragment) fragment;
            Fragment targetFragment = fragment.getTargetFragment();
            if (targetFragment != null) {
                Intent extras = new Intent();
                ArrayList<GoodsInDocument> mExistedItems = goodsTreeFragment.mExistedItems;
                extras.putExtra(GoodsInDocument.KEY, mExistedItems);
                targetFragment.onActivityResult(fragment.getTargetRequestCode(), Activity.RESULT_OK, extras);

            }
            fragmentManager.popBackStack();
        } else if (fragment instanceof SimpleGoodsListContainer) {
            SimpleGoodsListContainer simpleGoodsListContainer = (SimpleGoodsListContainer) fragment;
            if (fragment.getTargetFragment() != null) {
                Intent extras = new Intent();
                ArrayList<GoodsInDocument> mExistedItems = simpleGoodsListContainer.mExistedItems;
                extras.putExtra(GoodsInDocument.KEY, mExistedItems);
                fragment.getTargetFragment().onActivityResult(fragment.getTargetRequestCode(), Activity.RESULT_OK, extras);
            }
            fragmentManager.popBackStack();
        } else if (fragment instanceof ClientViewPagerContainer && backStackCount > 0) {
            fragmentManager.beginTransaction().remove(fragment).commit();
            fragmentManager.popBackStack();
        } else if (fragment instanceof SalesPlansFragment) {
            if (fragment.getTargetFragment() != null) {
                fragmentManager.beginTransaction().remove(fragment).commit();
                fragmentManager.popBackStack();
                ClientViewPagerContainer clientViewPagerContainer = new ClientViewPagerContainer();
                clientViewPagerContainer.setTargetFragment(clientViewPagerContainer, fragment.getTargetRequestCode());
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, clientViewPagerContainer)
                        .commit();
            } else {
                fragmentManager.popBackStack();
            }
        } else if (fragment instanceof ContactListFragment) {
            if (((ContactListFragment) fragment).isNotActualData()) {
                ((ContactListFragment) fragment).showDialog();
            } else {
                fragmentManager.popBackStack();
            }
        } else if (fragment instanceof PaymentClientListFragment) {
//            if (fragment.getTargetFragment()!=null){
//                fragmentManager.beginTransaction().remove(fragment).commit();
//                fragmentManager.popBackStack();
//                ClientViewPagerContainer clientViewPagerContainer = new ClientViewPagerContainer();
//                clientViewPagerContainer.setTargetFragment(clientViewPagerContainer, fragment.getTargetRequestCode());
//                fragmentManager.beginTransaction()
//                        .replace(R.id.contentMain, clientViewPagerContainer)
//                        .commit();
//            }else {
            fragmentManager.popBackStack();
            //}
        } else {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (backStackCount > 0) {
                    super.onBackPressed();
                } else {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                    }
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Для выхода из приложения нажмите еще раз", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ERROR_DIALOG_REQUEST_CODE) {
            // Adding a fragment via GoogleApiAvailability.showErrorDialogFragment
            // before the instance state is restored throws an error. So instead,
            // set a flag here, which will cause the fragment to delay until
            // onPostResume.
            mRetryProviderInstall = true;
        }
    }

    /**
     * On resume, check to see if we flagged that we need to reinstall the
     * provider.
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mRetryProviderInstall) {
            // We can now safely retry installation.
            ProviderInstaller.installIfNeededAsync(this, this);
        }
        mRetryProviderInstall = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMyBroadcastReceiver != null) {
            unregisterReceiver(mMyBroadcastReceiver);
            mMyBroadcastReceiver = null;
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fragmentManager = getFragmentManager();
        switch (item.getItemId()) {
            case R.id.nav_stat:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new StatisticsFragment())
                        .commit();
                break;
            case R.id.nav_clients:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new ClientViewPagerContainer())
                        .commit();
                break;
            case R.id.nav_promotion:
                startGoodsSelectFragment(GoodsList.MODE_PROMOTIONS, null, GoodsAdapter.MODE_ORDER, null);
                break;
            case R.id.nav_price:
                GoodsTreeFragment goodsTreeFragment = new GoodsTreeFragment();
                Bundle bundle = new Bundle();
                //fixme
                bundle.putString(PRICE, "ok");
                goodsTreeFragment.setArguments(bundle);
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, goodsTreeFragment)
                        .commit();
                break;
            case R.id.nav_settings:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new SettingsFragment())
                        .commit();
                break;
            case R.id.nav_summary:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new SummaryFragment())
                        .commit();
                break;
            case R.id.nav_stat_doc:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new StatDocFragment())
                        .commit();
                break;
            case R.id.nav_ticket_client:

                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                ClientListFragment fragmentTicketClient = new ClientListFragment();
                //fragmentTicketClient.setTargetFragment(fragmentTicketClient, IDD_TICKET_ClIENT);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, fragmentTicketClient)
                        .commit();
                break;
            case R.id.nav_messages:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                startActivity(new Intent(this, NotificationMessageActivity.class));
                break;
            case R.id.nav_update_programm:
                m_update().show();
                break;
            case R.id.nav_help:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new HelpFragment())
                        .commit();
                break;
            case R.id.nav_exit:
                finish();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void startActivityForUpgrade() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    protected Dialog m_update() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.cap_confirm));
        builder.setMessage(getString(R.string.mes_load_update));
        builder.setIcon(R.mipmap.main_icon);
        builder.setPositiveButton(getString(R.string.bt_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForUpgrade();
                    }
                });
        builder.setNegativeButton(getString(R.string.bt_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setCancelable(false);
        return builder.create();
    }


    private void setHomeButtonToolbar() {
        actionBar.setDisplayHomeAsUpEnabled(false);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.setToolbarNavigationClickListener(null);
        mToolBarNavigationListenerIsRegistered = false;
    }

    private void setBackButtonToolbar() {
        toggle.setDrawerIndicatorEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (!mToolBarNavigationListenerIsRegistered) {
            toggle.setToolbarNavigationClickListener(v -> onBackPressed());
            mToolBarNavigationListenerIsRegistered = true;
        }
    }

    @Override
    public void onChangeToolbar(String value) {
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.contentMain);
        int backStackCount = fm.getBackStackEntryCount();
        if (fragment instanceof GoodsTreeFragment) {
            GoodsTreeFragment goodsTreeFragment = (GoodsTreeFragment) fragment;
            int priceLvl = 1;
            byte discount = 0;
            String[] prices = getResources().getStringArray(R.array.list_prices);
            Client mClient = goodsTreeFragment.getArguments().getParcelable(GoodsTreeFragment.EXTRA_CLIENT);
            byte mode = goodsTreeFragment.getArguments().getByte(GoodsTreeFragment.EXTRA_TYPE);
            if (mClient != null) {
                priceLvl = prices.length > mClient.mPriceType ? mClient.mPriceType : priceLvl;
                discount = mClient.mDiscount;
            }
            String subtitle = "Цена: " + prices[priceLvl];
            if (discount != 0) {
                subtitle += String.format(Locale.US, ". Скидка %d%%", discount);
            }
            if (mode == GoodsList.MODE_PROMOTIONS) {
                actionBar.setSubtitle("");
                actionBar.setTitle("Акции");
            } else {
                actionBar.setSubtitle(subtitle);
                actionBar.setTitle("Список товаров");
            }

            if (backStackCount > 0) {
                setBackButtonToolbar();
            } else {
                setHomeButtonToolbar();
            }
        } else if (fragment instanceof GoodsInfoContainer) {
            toolbar.setTitle("Информация о товаре");
            toolbar.setSubtitle("");
            //setMenuItemVisibility(false);
            setBackButtonToolbar();
            Log.d(TAG, "GoodsInfoContainer init toolbar");
        } else if (fragment instanceof ClientViewPagerContainer) {
            if (backStackCount > 0) {
                toolbar.setTitle("Клиенты");
                toolbar.setSubtitle("");
                setBackButtonToolbar();
            } else {
                toolbar.setTitle("Новый визит");
                toolbar.setSubtitle("Клиенты");
                setHomeButtonToolbar();
            }
        } else if (fragment instanceof StatisticsFragment) {
            toolbar.setTitle("Статистика");
            toolbar.setSubtitle("");
            setHomeButtonToolbar();
        } else if (fragment instanceof WorkModeFragment) {
            toolbar.setTitle("Статистика");
            toolbar.setSubtitle("Подробнее");
            setBackButtonToolbar();
        } else if (fragment instanceof LocationFragment) {
            toolbar.setTitle("Новый визит");
            toolbar.setSubtitle("Определение местоположения");
            setHomeButtonToolbar();
        } else if (fragment instanceof VisitContainFragment) {
            toolbar.setTitle("Новый визит");
            toolbar.setSubtitle("Выберите действие");
            setBackButtonToolbar();
        } else if (fragment instanceof OrderCommonFragment) {
            toolbar.setTitle("Новая Заявка");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof OrderGoodsFragment) {
            if (App.isMerch()) {
                if (fragment.getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK) {
                    toolbar.setTitle("Новая задача");
                } else {
                    toolbar.setTitle("Новый Мониторинг");
                }
            } else {
                toolbar.setTitle("Новая Заявка");
            }
            toolbar.setSubtitle("Список товаров");
            setBackButtonToolbar();
        } else if (fragment instanceof TicketClientFragment) {
            toolbar.setTitle("Заявки клиента");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof ClientListFragment) {
            toolbar.setTitle("Заявки клиента");
            toolbar.setSubtitle("Выберите клиета");
            setHomeButtonToolbar();
        } else if (fragment instanceof SettingsFragment) {
            toolbar.setTitle("Настройки");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof PaymentClientListFragment) {
            toolbar.setTitle("Оплаты");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof SalesPlansFragment) {
            toolbar.setTitle("Спецификация содружества");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof PDFViewerFragment) {
            String title;
            if (value != null && value.equals(PDFViewerFragment.FAQ)) {
                title = "Инструкция";
            } else {
                title = "Стандарты работы с сетями";
            }
            toolbar.setTitle(title);
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof HelpFragment) {
            toolbar.setTitle("Помощь и поддержка");
            toolbar.setSubtitle("");
            setHomeButtonToolbar();
        } else if (fragment instanceof VisitSelectFragment) {
            toolbar.setTitle("Выберите визит");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof ContactListFragment) {
            toolbar.setTitle("Контакты");
            toolbar.setSubtitle("");
            setBackButtonToolbar();
        } else if (fragment instanceof ContactEditorFragment) {
            toolbar.setTitle("Контакты");
            String subTitle = "Новый";
            if (value != null) {
                subTitle = value;
            }
            toolbar.setSubtitle(subTitle);
            setBackButtonToolbar();
        }
    }


    private void initFireBase(final String region, final String name, final String code) {
        try {
            firebaseAuth = FirebaseAuth.getInstance();
            Task<AuthResult> authResultTask = firebaseAuth.signInAnonymously();
            authResultTask.addOnCompleteListener(this, task -> {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    user = firebaseAuth.getCurrentUser();
                    Log.d(TAG, "signInAnonymously:success");
                    final String tokenFirebase = FirebaseInstanceId.getInstance().getToken();
                    Log.d(TAG, "token " + tokenFirebase);
                    OkHttpClientBuilder.buildLogin().enqueue(new Callback() {
                        @Override
                        public void onFailure(@NonNull Call call, IOException e) {
                            Log.d(TAG, "onFailure" + e.getMessage());
                        }

                        @Override
                        public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                            try {
                                String strResponse = response.body().string();
                                Log.d(TAG, strResponse);
                                JSONObject jsonObject = new JSONObject(strResponse);
                                String token = jsonObject.getString("token");
                                OkHttpClientBuilder.buildNewClient(region, code, tokenFirebase, name, token).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(@NonNull Call call, @NonNull IOException e) {

                                    }

                                    @Override
                                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInAnonymously:failure", task.getException());
                    Toast.makeText(MainActivity.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            //Toast.makeText(this, "Обновите сервисы гугл плей", Toast.LENGTH_LONG).show();
        }
    }


    private void killProcess() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        if (am != null) {
            runningAppProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                String processName = this.getPackageName() + ":GetParamsIntentService";
                if (next.processName.equals(processName)) {
                    //останавливаем сервис,если он уже запущен
                    Process.killProcess(next.pid);
                    break;
                }
            }
        }
    }

    ProgressDialog progressDialog;

    @Override
    public void onChangeSetting() {
        killProcess();
        AuthManager.getInstance().signOutExplicitly();
        Intent startGetLimitsTask = new Intent(this, GetParamsIntentService.class);
        String codeAgent = App.get(this).getCodeAgent();
        String server = App.get(this).getFTPServer();
        if (!codeAgent.equals(activity.getString(R.string.lb_zero_code_manager)) && server != null && !server.isEmpty()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Идет загрузка параметров с сервера");
            progressDialog.setMessage("Пожалуйста подождите...");
            progressDialog.setButton(Dialog.BUTTON_NEGATIVE, "Отмена", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    killProcess();
                    progressDialog.dismiss();
                }
            });
            progressDialog.show();
            flag = true;
            App app = App.get(this);
            String codeAgent1 = app.getCodeAgent();
            String connectionServer = app.connectionServer;
            startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_AGENT, codeAgent1);
            startGetLimitsTask.putExtra(GetParamsIntentService.EXTRA_TYPE_SERVER, connectionServer);
            startService(startGetLimitsTask);
        } else {
            Log.d(TAG, "Toast1");
            Toast.makeText(this, "Неверный код менеджера или неправильно выбран сервер", Toast.LENGTH_LONG).show();
        }
    }

    private void onProviderInstallerNotAvailable() {
        Log.d(TAG, "This is reached if the provider cannot be updated for some reason App should consider all HTTP communication to be vulnerable, and take appropriate action.");

    }

    @Override
    public void onProviderInstalled() {
        Log.d(TAG, "Provider is up-to-date, app can make secure network calls.");
    }


    @Override
    public void onProviderInstallFailed(int errorCode, Intent intent) {
        GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
        if (availability.isUserResolvableError(errorCode)) {
            // Recoverable error. Show a dialog prompting the user to
            // install/update/enable Google Play services.
            availability.showErrorDialogFragment(
                    this,
                    errorCode,
                    ERROR_DIALOG_REQUEST_CODE,
                    dialog -> {
                        // The user chose not to take the recovery action
                        onProviderInstallerNotAvailable();
                    });
        } else {
            // Google Play services is not available.
            onProviderInstallerNotAvailable();
        }
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {
        if (newState == DrawerLayout.STATE_SETTLING && !drawer.isDrawerOpen(GravityCompat.START)) {
            MyApp app = (MyApp) MyApp.getContext();
            SQLiteDatabase db = app.getDB();
            try {
                Cursor cursor = db.rawQuery("select * from " + PromotionCheckedMetaData.TABLE_NAME + " where "
                        + PromotionCheckedMetaData.FIELD_CHECK + " = 0 and "
                        + PromotionCheckedMetaData.FIELD_REGION + " = " + App.getRegionId(activity), null);
                if (cursor.getCount() > 0) {
                    setMenuCounter(R.id.nav_promotion, cursor.getCount());
                }
                cursor.close();
            } catch (SQLiteException e) {

            }

        }
    }


    public class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null) {
                switch (intent.getAction()) {
                    case GetParamsIntentService.BROADCAST_ACTION:
                        if (intent.getByteExtra(GetParamsIntentService.EXTRA_STATUS, GetParamsIntentService.STATUS_OK) != GetParamsIntentService.STATUS_ERROR) {
                            String message = intent.getStringExtra(GetParamsIntentService.EXTRA_MESSAGE);
                            Log.d(TAG, message);
                            String[] messageArray = message.split(";");
                            String sectionEC = getString(R.string.pref_exclusive);
                            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString(sectionEC, messageArray[0].trim());
                            editor.apply();
                            String fio = messageArray[2];
                            String tagUserDeleteForDataBase = messageArray[3];

                            String codeRegion = null;
                            if (messageArray.length >= 5) {
                                codeRegion = messageArray[4];
                            }
                            String tagIsMerch = null;
                            if (messageArray.length >= 6) {
                                tagIsMerch = messageArray[5];
                            }

                            if (messageArray.length >= 7) {
                                GlobalProc.setPref(activity, R.string.pref_foreign_price, messageArray[6]);
                            }

                            if (tagUserDeleteForDataBase.equals("0") && !fio.isEmpty()) {
                                Security.deleteFile(context);
                                showRegisterActivity();
                            } else if (fio.isEmpty()) {
                                txt_fio.setText(fio + "\n[" + App.get(context).getCodeAgent() + "]");
                                Toast.makeText(context, "Неверный код менеджера или неправильно выбран сервер", Toast.LENGTH_LONG).show();
                            } else {
                                if (flag) {
                                    txt_fio.setText(fio + "\n[" + App.get(context).getCodeAgent() + "]");
                                    Toast.makeText(context, "Добро пожаловать " + fio, Toast.LENGTH_LONG).show();
                                    GlobalProc.setPref(activity, R.string.pref_nameManager, fio);
                                    if (tagIsMerch != null) {
                                        GlobalProc.setPref(activity, R.string.pref_is_merch, tagIsMerch);
                                    }
                                    if (codeRegion != null && !codeRegion.isEmpty()) {
                                        App.setRegion(activity, codeRegion);
                                        initFireBase(codeRegion, fio, App.get(activity).getCodeAgent());
                                        AuthManager.getInstance().signInExplicitly(App.get(activity).getCodeAgent());
                                        MyApp.setUser(null);
                                    }
                                } else {
                                    EventBus.getDefault().post("OK");
                                    GlobalProc.setPref(activity, R.string.pref_nameManager, fio);
                                }
                            }
                        } else {
                            Log.d(TAG, "Toast3" + String.valueOf(intent.getStringExtra(GetParamsIntentService.EXTRA_MESSAGE)));
                            Toast.makeText(context, String.valueOf(intent.getStringExtra(GetParamsIntentService.EXTRA_MESSAGE)), Toast.LENGTH_LONG).show();
                            EventBus.getDefault().post("NEOK");
                        }
                        break;
                }
            }
            if (progressDialog != null)
                progressDialog.dismiss();
        }
    }


    private void startGoodsSelectFragment(byte typeLoad, Node selected, byte mode, Client client) {
        //супер костыль
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("StartPagePromotion", 1);
        editor.apply();
        Bundle goodsSelect = new Bundle();
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_TYPE, typeLoad);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_NODE, selected);
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_MODE, mode);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_CLIENT, client);
        goodsSelect.putString(PRICE, "ok");
        GoodsTreeFragment fragment = new GoodsTreeFragment();
        fragment.setArguments(goodsSelect);
        getFragmentManager().beginTransaction()
                .replace(R.id.contentMain, fragment, GoodsTreeFragment.TAG)
                .commit();
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        view.setText(count > 0 ? String.valueOf(count) : null);
    }

}
