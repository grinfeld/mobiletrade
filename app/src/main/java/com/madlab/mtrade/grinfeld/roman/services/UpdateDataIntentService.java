package com.madlab.mtrade.grinfeld.roman.services;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by grinfeldra
 */
public class UpdateDataIntentService extends IntentService {

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.updatedata";
    public static final String EXTRA_STATUS = "TaskStatus";
    public static final byte STATUS_OK = Byte.MAX_VALUE;

    public UpdateDataIntentService() {
        super(BROADCAST_ACTION);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, STATUS_OK);
        sendBroadcast(result);
    }
}
