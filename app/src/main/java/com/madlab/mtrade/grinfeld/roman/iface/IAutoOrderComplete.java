package com.madlab.mtrade.grinfeld.roman.iface;


import com.madlab.mtrade.grinfeld.roman.tasks.AutoOrderTask;

public interface IAutoOrderComplete {
	void onTaskComplete(AutoOrderTask task);
}
