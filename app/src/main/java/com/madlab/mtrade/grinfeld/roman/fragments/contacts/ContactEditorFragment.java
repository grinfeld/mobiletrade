package com.madlab.mtrade.grinfeld.roman.fragments.contacts;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.content.res.AppCompatResources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.components.DatePickerFragment;
import com.madlab.mtrade.grinfeld.roman.db.ContactMetaData;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;
import com.madlab.mtrade.grinfeld.roman.entity.Post;
import com.madlab.mtrade.grinfeld.roman.eventbus.DateChanged;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.sapereaude.maskedEditText.MaskedEditText;

/**
 * Created by grinfeldra
 */
public class ContactEditorFragment extends Fragment implements View.OnClickListener {


    private static final String KEY_CONTACT = "key_contacts";
    private static final int SELECT_PHONE_NUMBER = 12;
    private static final String TAG = "#ContactEditorFragment";

    private Contact contact = null;

    private EditText et_email, et_fio,
            et_note;
    private MaskedEditText et_phone;
    private CheckBox cb_responsible;
    private Button btn_birth_date;
    private Spinner spinner_post;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.edit_contact, menu);
    }

    public static Fragment newInstance(Contact contacts, String codeClient) {
        ContactEditorFragment fragment = new ContactEditorFragment();
        Bundle bundle = new Bundle();
        if (contacts != null) {
            bundle.putParcelable(KEY_CONTACT, contacts);
        }
        bundle.putString(Client.KEY, codeClient);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.contact_editor_fragment, container, false);
        if (getArguments() != null) {
            contact = getArguments().getParcelable(KEY_CONTACT);
        }
        String title = null;
        if (contact != null) {
            title = contact.getFio();
        }
        IDynamicToolbar iDynamicToolbar = (IDynamicToolbar) getActivity();
        iDynamicToolbar.onChangeToolbar(title);
        bindView(view, contact);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GlobalProc.closeKeyboard(getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_contact:
                if (validate()) {
                    saveOrUpdate();
                    getFragmentManager().popBackStack();
                }
                break;
            case R.id.action_get_contact:
                Permissions.check(getActivity(), Manifest.permission.READ_CONTACTS, null, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        Intent i = new Intent(Intent.ACTION_PICK);
                        i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                        startActivityForResult(i, SELECT_PHONE_NUMBER);
                    }
                });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveOrUpdate() {
        String codeAgent = App.get(getActivity()).getCodeAgent();
        MyApp app = (MyApp) getActivity().getApplication();
        String codeClient = getArguments().getString(Client.KEY);
        SQLiteDatabase db = app.getDB();
        Post post = (Post) spinner_post.getSelectedItem();
        String fio = et_fio.getText().toString();
        String phone = et_phone.getRawText();
        String email = et_email.getText().toString();
        int responsible = cb_responsible.isChecked() ? 1 : 0;
        String dateBirth = btn_birth_date.getText().toString();
        if (dateBirth.equals("Выберите дату")) {
            dateBirth = "";
        }
        String postName = "Генеральный директор";

        if (post != null) {
            postName = post.getName();
        }

        String note = et_note.getText().toString();
        if (contact == null) {
            String q = ContactMetaData.insertQuery(codeAgent, codeClient, postName,
                    fio, "+7".concat(phone), email, responsible, dateBirth, note);
            DBHelper.execMultipleSQL(db, new String[]{q});
        } else {
            ContentValues values = new ContentValues();
            values.put(ContactMetaData.FIELD_POST.Name, postName);
            values.put(ContactMetaData.FIELD_FIO.Name, fio);
            values.put(ContactMetaData.FIELD_TEL.Name, phone);
            values.put(ContactMetaData.FIELD_EMAIL.Name, email);
            values.put(ContactMetaData.FIELD_RESPONSIBLE.Name, responsible);
            values.put(ContactMetaData.FIELD_DATE_BIRTH.Name, dateBirth);
            values.put(ContactMetaData.FIELD_NOTE.Name, note);
            Contact.updateContact(db, values, contact.getId());
        }
        String dateUpdate = new SimpleDateFormat("dd.MM.yy", Locale.getDefault()).format(new Date());
        Client.updateLastModifyContact(db, dateUpdate, codeClient);
    }

    private boolean validate() {
        if (et_fio.getText().toString().trim().isEmpty()) {
            et_fio.setError("введите обязательное поле");
            return false;
        }
        if (!et_email.getText().toString().trim().isEmpty()) {
            if (!isValidEmail(et_email.getText())) {
                et_email.setError("email не валидный");
                return false;
            }
        }
        if (et_phone.getRawText().trim().isEmpty()) {
            et_phone.setError("введите обязательное поле");
            return false;
        } else if (!isValidMobile(et_phone.getRawText())) {
            et_phone.setError("номер телефона не валидный");
            return false;
        }
        return true;
    }


    private void bindView(View view, Contact contact) {
        et_phone = view.findViewById(R.id.et_phone);
        et_email = view.findViewById(R.id.et_email);
        et_fio = view.findViewById(R.id.et_fio);
        et_note = view.findViewById(R.id.et_note);
        cb_responsible = view.findViewById(R.id.cb_responsible);
        btn_birth_date = view.findViewById(R.id.btn_birth_date);
        spinner_post = view.findViewById(R.id.spinner_post);
        btn_birth_date.setOnClickListener(this);
        setData(contact);
    }

    private void setData(Contact contact) {
        MyApp app = (MyApp) getActivity().getApplication();
        SQLiteDatabase db = app.getDB();
        List<Post> posts = Post.getPosts(db);
        if (posts.size() == 0) {
            posts.add(new Post(0, "Генеральный директор"));
        }
        ArrayAdapter<Post> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, posts);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
        spinner_post.setAdapter(adapter);
        if (contact != null) {
            et_note.setText(contact.getNote());
            if (!contact.getTel().isEmpty()) {
                et_phone.setText(formatPhone(contact.getTel()));
            }
            et_email.setText(contact.getEmail());
            et_fio.setText(contact.getFio());
            cb_responsible.setChecked(contact.getResponsible() == 1);
            if (!contact.getDateBirth().isEmpty()) {
                btn_birth_date.setText(contact.getDateBirth());
            }
            String currentPost = contact.getPost();
            int currentPostIndex = getCurrentPostIndex(posts, currentPost);
            spinner_post.setSelection(currentPostIndex);
        }
    }

    private String formatPhone(String phone) {
        if (phone != null && !phone.isEmpty()) {
            if (phone.substring(0, 2).equals("+7")) {
                return phone.substring(2);
            } else if (phone.substring(0, 1).equals("8") || phone.substring(0, 1).equals("7")) {
                return phone.substring(1);
            } else {
                return phone;
            }
        } else {
            return "";
        }
    }


    private int getCurrentPostIndex(List<Post> posts, String currentPost) {
        int id = 0;
        for (Post post : posts) {
            if (post.getName().equalsIgnoreCase(currentPost)) {
                return id;
            }
            id++;
        }
        return 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_PHONE_NUMBER && resultCode == Activity.RESULT_OK) {
            Uri contactUri = data.getData();
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID};
            Cursor cursor = getActivity().getContentResolver().query(contactUri, projection,
                    null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int displayNameIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_PRIMARY);
                int idIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID);
                int id = cursor.getInt(idIndex);
                String email = getEmail(getActivity(), id);
                String number = cursor.getString(numberIndex);
                String fio = cursor.getString(displayNameIndex);
                et_phone.setText(formatPhone(number));
                et_fio.setText(fio);
                et_email.setText(email);
            }
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static String getEmail(Context c, long id) {
        Uri uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Email._ID,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.TYPE};
        String selection = ContactsContract.CommonDataKinds.Email.CONTACT_ID
                + " = '" + id + "'";
        String sortOrder = ContactsContract.CommonDataKinds.Email.ADDRESS
                + " COLLATE LOCALIZED ASC";

        Cursor cursor = c.getContentResolver().query(uri, projection,
                selection, null, sortOrder);

        int index = cursor
                .getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
        if (cursor.moveToNext()) {
            return cursor.getString(index);
        }
        return "";
    }


    private void showDatePickerDialog(View view, Date start) {
        DialogFragment newFragment = new DatePickerFragment(view, start);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_birth_date:
                showDatePickerDialog(v, new Date(0));
                break;
        }
    }

    private boolean isValidEmail(CharSequence target) {
        return Pattern.compile(".+@.+\\.[a-z]+").matcher(target).matches();
    }

    private boolean isValidMobile(String phone) {
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            Log.d(TAG, String.valueOf(phone.length()));
            return phone.length() == 10;
        }
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateChanged(DateChanged dateChanged) {
        int year = dateChanged.getYear();
        int month = dateChanged.getMonth();
        int day = dateChanged.getDay();
        Calendar dateShip = Calendar.getInstance();
        dateShip.set(Calendar.YEAR, year);
        dateShip.set(Calendar.MONTH, month);
        dateShip.set(Calendar.DAY_OF_MONTH, day);
        dateShip.set(Calendar.HOUR, 0);
        dateShip.set(Calendar.MINUTE, 0);
        btn_birth_date.setText(String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year));
//        String format = String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year);
//        try {
//            Date date = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).parse(format);
//            String result = new SimpleDateFormat("dd.MM.yy", Locale.getDefault()).format(date);
//            btn_birth_date.setText(result);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
    }


    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

}
