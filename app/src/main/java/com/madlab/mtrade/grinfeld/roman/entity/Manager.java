package com.madlab.mtrade.grinfeld.roman.entity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.db.ManagerMetaData;

import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 15.01.2018.
 */

public class Manager implements Parcelable {
    private final static String TAG = "!->Manager";

    public final static String KEY = "ManagerExtra";

    private String mCode;
    private String mFIO;
    private String mPhone;
    private String mEmail;

    public Manager(String code, String fio, String phone, String email){
        mCode = code;
        mFIO = fio;
        mPhone = phone;
        mEmail = email;
    }

    public Manager(){this("", "", "", "");}

    public String Code(){return mCode;}
    public void Code(String code){mCode = code;}

    public String Fio(){return mFIO;}
    public void Fio(String fio){mFIO = fio;}

    public String Phone(){return mPhone;}
    public void Phone(String phone){mPhone = phone;}

    public String Email(){return mEmail;}
    public void Email(String email){mEmail = email;}

    public static Manager load(SQLiteDatabase db, String code){
        Manager result = null;

        String sql = String.format("SELECT * FROM %s WHERE %s = ?", ManagerMetaData.TABLE_NAME, ManagerMetaData.FIELD_CODE);
        ManagerCursorWrapper rows = null;
        try {
            rows = new ManagerCursorWrapper(db.rawQuery(sql, new String[] {code}));

            if (rows.moveToFirst())
                result = rows.getManager();
        }
        catch (Exception e) {
            GlobalProc.log_e(TAG, e.toString());
        }finally{
            if (rows != null){
                rows.close();
            }
        }

        return result;
    }


    public static ArrayList<Manager> loadList(SQLiteDatabase db){
        ArrayList<Manager> result = null;

        String sql = String.format("SELECT * FROM %s", ManagerMetaData.TABLE_NAME);
        ManagerCursorWrapper rows = null;
        try {
            rows = new ManagerCursorWrapper(db.rawQuery(sql, null));

            if (rows.moveToFirst())
                result = new ArrayList<Manager>();

            while (rows.moveToNext()) {
                result.add(rows.getManager());
            }
        } catch (Exception e) {
            GlobalProc.log_e(TAG, e.toString());
        }finally{
            if (rows != null){
                rows.close();
            }
        }

        return result;
    }

    public Manager(Parcel parcel){
        mCode = parcel.readString();
        mFIO = parcel.readString();
        mPhone = parcel.readString();
        mEmail = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int pArg1) {
        parcel.writeString(mCode);
        parcel.writeString(mFIO);
        parcel.writeString(mPhone);
        parcel.writeString(mEmail);
    }
}
