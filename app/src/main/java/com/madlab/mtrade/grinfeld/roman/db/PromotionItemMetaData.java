package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */

public class PromotionItemMetaData {

    public static final String TABLE_NAME = "PromotionItem";

    public static final String FIELD_ID = "id_promotion";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_TEXT = "text";
    public static final String FIELD_COVER = "cover";
    public static final String FIELD_QUANTUM = "quantum";
    public static final String FIELD_START_DATE = "start_date";
    public static final String FIELD_END_DATE = "end_date";
    public static final String FIELD_AMOUNT_FOR_BONUS = "amount_for_bonus";


    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s integer NOT NULL DEFAULT 0,"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s integer NOT NULL DEFAULT 0)",
            TABLE_NAME, FIELD_ID, FIELD_TITLE, FIELD_TEXT, FIELD_COVER, FIELD_QUANTUM,
            FIELD_START_DATE, FIELD_END_DATE, FIELD_AMOUNT_FOR_BONUS);

    public static String insertQuery(int id, String title, String text, String cover, String quantum, String startDate, String endDate, int amount_for_bonus) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s, %s, %s, %s, %s) "
                        + "VALUES "
                        + "(%d, '%s', '%s', '%s', '%s', '%s', '%s', %d)",
                TABLE_NAME, FIELD_ID, FIELD_TITLE, FIELD_TEXT, FIELD_COVER, FIELD_QUANTUM, FIELD_START_DATE, FIELD_END_DATE, FIELD_AMOUNT_FOR_BONUS,
                id, title, text, cover, quantum, startDate, endDate, amount_for_bonus);
    }


}
