package com.madlab.mtrade.grinfeld.roman.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA on 28.04.2018.
 */

public class ImageAdapter extends BaseAdapter {

    private static final String TAG = "#ImageAdapter";
    private Context mContext;
    private ArrayList<String> imgUrl;
    public  List<Integer> selectedPositions = new ArrayList<>();

    public ImageAdapter(Context mContext, ArrayList<String> imgUrl) {
        this.mContext = mContext;
        this.imgUrl = imgUrl;
    }

    public int getCount() {
        return imgUrl.size();
    }

    public Object getItem(int position) {
        return imgUrl.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = vi.inflate(R.layout.galery_item, null);
        ImageView imageView = v.findViewById(R.id.ImageView01);
        imageView.setImageBitmap(decodeURI(imgUrl.get(position)));
        return v;
    }

    public Bitmap decodeURI(String filePath){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Only scale if we need to
        // (16384 buffer for img processing)
        Boolean scaleByHeight = Math.abs(options.outHeight - 200) >= Math.abs(options.outWidth - 200);
        if(options.outHeight * options.outWidth * 2 >= 16384){
            // Load, scaling to smallest power of 2 that'll get it <= desired dimensions
            double sampleSize = scaleByHeight
                    ? options.outHeight / 200
                    : options.outWidth / 200;
            options.inSampleSize =
                    (int)Math.pow(2d, Math.floor(
                            Math.log(sampleSize)/Math.log(2d)));
        }

        // Do the actual decoding
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[512];
        Bitmap output = BitmapFactory.decodeFile(filePath, options);

        return output;
    }
}