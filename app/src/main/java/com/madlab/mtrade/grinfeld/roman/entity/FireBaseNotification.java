package com.madlab.mtrade.grinfeld.roman.entity;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by GrinfeldRA on 22.02.2018.
 */

public class FireBaseNotification implements Parcelable{

    public int id;
    public String title;
    public String body;
    public String tag;
    public int read;
    public long date;


    public FireBaseNotification(int id, String title, String body, String tag, int read, long date) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.tag = tag;
        this.read = read;
        this.date = date;
    }

    public FireBaseNotification(Parcel parcel) {
        String[] data = new String[6];
        parcel.readStringArray(data);
        this.id = Integer.parseInt(data[0]);
        this.title = data[1];
        this.body = data[2];
        this.tag = data[3];
        this.read = Integer.parseInt(data[4]);
        this.date = Long.parseLong(data[5]);
    }

    public static final Creator<FireBaseNotification> CREATOR = new Creator<FireBaseNotification>() {
        @Override
        public FireBaseNotification createFromParcel(Parcel in) {
            return new FireBaseNotification(in);
        }

        @Override
        public FireBaseNotification[] newArray(int size) {
            return new FireBaseNotification[size];
        }
    };

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRead() {
        return read;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return body;
    }

    public String getTag() {
        return tag;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setRead(int read) {
        this.read = read;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{String.valueOf(id), title, body, tag, String.valueOf(read), String.valueOf(date)});
    }

}
