package com.madlab.mtrade.grinfeld.roman.eventbus;

import com.madlab.mtrade.grinfeld.roman.entity.Goods;

/**
 * Created by GrinfeldRA
 */

public class OnMeashChanged {

    private Goods goods;
    private byte  newValue;


    public OnMeashChanged(Goods goods, byte newValue) {
        this.goods = goods;
        this.newValue = newValue;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public byte getNewValue() {
        return newValue;
    }

    public void setNewValue(byte newValue) {
        this.newValue = newValue;
    }
}
