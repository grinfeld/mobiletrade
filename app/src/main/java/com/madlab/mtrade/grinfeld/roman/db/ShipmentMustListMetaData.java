package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

import java.util.Locale;

/**
 * Created by GrinfeldRA on 11.05.2018.
 */

public class ShipmentMustListMetaData implements BaseColumns{


    public static final String TABLE_NAME = "ShipmentMustList";

    public static final String CODE_GOODS = "code_goods";

    public static final String  CODE_AGENT = "code_agent";

    public static final String  CODE_CLIENT = "code_client";

    public static final String  IS_SHIPMENT_MUST_LIST = "isShipmentMustList";



    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s (" + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '', " +
                    "%s	text NOT NULL DEFAULT ''," +
                    "%s	text NOT NULL DEFAULT '')",
            TABLE_NAME, CODE_GOODS, IS_SHIPMENT_MUST_LIST, CODE_AGENT, CODE_CLIENT);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;


    public static String insertQuery(String codeAgent, String codeCli,
                                     String isShipmentMustList, String codeGoods) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s) "
                        + "VALUES "
                        + "('%s', '%s', '%s', '%s')",
                TABLE_NAME, CODE_AGENT, CODE_CLIENT, IS_SHIPMENT_MUST_LIST, CODE_GOODS,
                codeAgent, codeCli, isShipmentMustList, codeGoods);
    }



}
