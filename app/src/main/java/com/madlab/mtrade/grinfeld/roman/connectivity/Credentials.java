package com.madlab.mtrade.grinfeld.roman.connectivity;

import android.content.Context;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;

public class Credentials {

    private String server1;
    private String server2;

    private String login;
    private String password;

    private int port;

    public String getServer1() {
        return server1;
    }

    public String getServer2() {
        return server2;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Credentials(String server1, String server2, String login,
                       String password, int port) {
        this.server1 = server1;
        this.server2 = server2;
        this.login = login;
        this.password = password;
        this.port = port;
    }

    public static Credentials load(Context context) {
        String server1 = GlobalProc.getPref(context, R.string.pref_ftp_server);
        String server2 = GlobalProc.getPref(context, R.string.pref_ftp_serverA);

        String login = GlobalProc.getPref(context, R.string.pref_ftp_login);
        String tmp = GlobalProc.getPref(context, R.string.pref_ftp_password);
        String password = GlobalProc.deCode(tmp);

        int port = GlobalProc.getPrefInt(context, R.string.pref_serverPort,0);

        return new Credentials(server1, server2, login, password, port);
    }
}