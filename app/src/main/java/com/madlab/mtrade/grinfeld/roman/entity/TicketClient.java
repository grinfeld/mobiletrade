package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public class TicketClient {

    private String nameTicket;
    private String manager;
    private String amount;

    public TicketClient(String nameTicket, String manager, String amount) {
        this.nameTicket = nameTicket;
        this.manager = manager;
        this.amount = amount;
    }

    public String getNameTicket() {
        return nameTicket;
    }

    public String getManager() {
        return manager;
    }

    public String getAmount() {
        return amount;
    }
}
