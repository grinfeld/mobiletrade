package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.GetLimitsTask;

/**
 * Created by GrinfeldRA on 26.04.2018.
 */

public interface IGetLimitTask {

    void onTaskComplete(GetLimitsTask task);

}
