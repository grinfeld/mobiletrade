package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class ReturnItemMetaData implements BaseColumns {
	private ReturnItemMetaData() {}

	public static final String TABLE_NAME = "ReturnItem";

		/**
	 * ����� �������� (FK)
	 */
	public static final String FIELD_NUM = "NumReturn";

	/**
	 * ��� ������
	 */
	public static final String FIELD_CODE = "CodeGoods";

	/**
	 * ����������
	 */
	public static final String FIELD_COUNT = "NumItem";

	/**
	 * ���-�� � �������� (���������)
	 */
	public static final String FIELD_COUNT_PACK = "NumInPack";

	/**
	 * ����
	 */
	public static final String FIELD_PRICE = "Price";

	/**
	 * ���
	 */
	public static final String FIELD_WEIGHT = "Weight";

	/**
	 * �������� ���������
	 */
	public static final String FIELD_AMOUNT = "Amount";

	/**
	 * ������������ ����������
	 */
	public static final String FIELD_INFO = "Info";

	/**
	 * ������� �������� ������ (��������)
	 */
	public static final String FIELD_WEIGHING = "Weighing";

	/**
	 * ���� ��������
	 */
	public static final String FIELD_BEST_BEFORE = "BestBefore";

	/**
	 * ������� ������������� ������ ��������� ������ � ����������� �� ����������
	 */
	public static final String FIELD_NEED_CALC_PRICE = "calcPrice";

	/**
	 * ������� ����� � ���������
	 */
	public static final String FIELD_IN_PACK = "inPack";

	public static final String INDEX = String.format(
			"CREATE INDEX idxNumReturn ON %s ( %s )", TABLE_NAME, FIELD_NUM);

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s (" + // TABLE_NAME
					"%s	int			NOT NULL DEFAULT 0, " + // FIELD_NUM
					"%s	nvarchar(6)	NOT NULL DEFAULT '', " + // FIELD_CODE
					"%s	integer		NOT NULL DEFAULT 0, " + // FIELD_COUNT
					"%s	smallint	NOT NULL DEFAULT 0, " + // FIELD_COUNT_PACK
					"%s	money		NOT NULL DEFAULT 0, " + // FIELD_PRICE
					"%s	money		NOT NULL DEFAULT 0, " + // FIELD_WEIGHT
					"%s	money		NOT NULL DEFAULT 0, " + // FIELD_AMOUNT
					"%s datetime	NOT NULL DEFAULT 0, " + // FIELD_BEST_BEFORE
					"%s	nvarchar(20)	NOT NULL DEFAULT ''," + // FIELD_INFO
					"%s	smallint	NOT NULL DEFAULT 0, " + // �������
															// �������������
															// ������� ����
					"%s	smallint	NOT NULL DEFAULT 0)", // FIELD_IN_PACK
			TABLE_NAME, FIELD_NUM, FIELD_CODE, FIELD_COUNT, FIELD_COUNT_PACK,
			FIELD_PRICE, FIELD_WEIGHT, FIELD_AMOUNT, FIELD_BEST_BEFORE,
			FIELD_INFO, FIELD_NEED_CALC_PRICE, FIELD_IN_PACK);

	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;
}
