package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.IOException;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.iface.IGetReturnItemsPricesComplete;

import org.json.JSONObject;

import okhttp3.Response;


public class GetReturnItemPricesTask extends AsyncTask<Void, Integer, Boolean> {
    private final String PROC_NAME = "ПолучитьЦеныВозврата";
    private final static String TAG = "!->GetReturnItemPricesTask";

    private String codeMan;
    private String codeCli;
    private ArrayList<ReturnItem> mItems;
    private IGetReturnItemsPricesComplete mTaskCompleteListener;

    private Credentials connectInfo;
    private DalimoClient mClient;

    private String mResult = "";

    public String getMessage() {
        return mResult;
    }

    public GetReturnItemPricesTask(Credentials info, String codeMan,
                                   String codeCli, ArrayList<ReturnItem> items,
                                   IGetReturnItemsPricesComplete taskCompleteListener) {
        connectInfo = info;
        this.codeMan = codeMan;
        this.codeCli = codeCli;
        mItems = items;
        mTaskCompleteListener = taskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        if (mItems == null) {
            mResult = "Ошибка получения элементов возврата";
            return false;
        }
        String header = String.format("%s;%s;%s;", Const.COMMAND, codeMan, PROC_NAME);
        StringBuilder data = new StringBuilder();
        data.append(codeMan).append(";");
        data.append(codeCli).append(";");
        for (ReturnItem item : mItems) {
            data.append(item.getCodeGoods());
            data.append(";");
        }
        App settings = App.get(MyApp.getContext());
        String message;
        if (settings.connectionServer.equals("1")) {
            // А теперь всё это дело запускаем
            mClient = new DalimoClient(connectInfo);

            if (!mClient.connect()) {
                mResult = "Не удалось подключиться";
                return false;
            }

            mClient.send(header + data + Const.END_MESSAGE);

            message = mClient.receive();

            mClient.disconnect();
        } else {
            String region = App.getRegion(MyApp.getContext());
            try {
                data = data.replace(data.lastIndexOf(";"), data.lastIndexOf(";") + 1, "");
                Response response = OkHttpClientBuilder.buildCall(PROC_NAME, data.toString(), region).execute();
                if (response.isSuccessful()){
                    String strResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(strResponse);
                    message = jsonObject.getString("data");
                }else {
                    message = DalimoClient.ERROR;
                }
            } catch (Exception e) {
                mClient = new DalimoClient(connectInfo);

                if (!mClient.connect()) {
                    mResult = "Не удалось подключиться";
                    return false;
                }

                mClient.send(header + data + Const.END_MESSAGE);

                message = mClient.receive();

                mClient.disconnect();
            }
        }


        // А теперь обрабатываем полученное сообщение
        if (message.contains(DalimoClient.ERROR)) {
            int index = message.indexOf(DalimoClient.ERROR);
            if (index > 0) {
                mResult = message.substring(0, index);
            }
            return false;
        }

        parseMessage(mItems, message);

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void parseMessage(ArrayList<ReturnItem> items, String data) {
        String[] priceList = data.split(";");


        String codeGoods = null;
        String priceStr = null;
        float price;
        for (String item : priceList) {
            String[] split = item.split("[|]");
            try {
                codeGoods = split[0];
                priceStr = split[1];
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                priceStr = "0";
            }

            price = GlobalProc.parseFloat(priceStr);

            // теперь надо обновить данные в списке товаров
            for (ReturnItem ri : items) {
                if (ri.getCodeGoods().equalsIgnoreCase(codeGoods) && !ri.getInfo().equals("1")) {
                    ri.setPrice(price);
                    break;
                }
            }
        }
    }
}
