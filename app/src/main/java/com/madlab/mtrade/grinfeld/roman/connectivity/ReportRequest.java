package com.madlab.mtrade.grinfeld.roman.connectivity;

import android.os.Build;
import android.util.Xml;


import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.entity.Geolocation;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public final class ReportRequest extends DalimoRequest<ReportResponse> {

    public static class Builder {

        private InetSocketAddress address;
        private String employeeId;

        private List<Geolocation> geolocations = new ArrayList<>();

        public Builder() {
        }

        public Builder address(InetSocketAddress address) {
            this.address = address;
            return this;
        }

        public Builder employeeId(String employeeId) {
            this.employeeId = employeeId;
            return this;
        }

        public Builder geolocations(Collection<Geolocation> geolocations) {
            this.geolocations.addAll(geolocations);
            return this;
        }


        public ReportRequest build() throws IOException {
            ReportRequest request = new ReportRequest();
            request.address = address;

            XmlSerializer serializer = Xml.newSerializer();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                serializer.setOutput(output, "windows-1251");
                serializer.startDocument("windows-1251", false);

                serializeReport(serializer);

                serializer.endDocument();
                output.flush();
                request.body = output.toByteArray();
            } finally {
                try {
                    output.close();
                } catch (IOException e) {
                    // do nothing
                }
            }
            request.head = String.format("%s;Data;%s;%s\r\n", "MTRADE "+GlobalProc.getVersionName(MyApp.getContext()),
                    employeeId, request.body.length).getBytes("UTF-8");

            return request;
        }

        private void serializeReport(XmlSerializer serializer) throws IOException {
            String model = String.format("%s-%s", Build.MANUFACTURER, Build.MODEL);
            serializer.startTag("", "ClientMessage");
            serializer.attribute("", "CodeAgent", employeeId);
            serializer.attribute("", "Version", GlobalProc.getVersionName(MyApp.getContext()));
            serializer.attribute("", "Model", model);
            serializer.attribute("", "IMEI",GlobalProc.getIMEI());
            serializer.attribute("", "PhoneNumber", GlobalProc.getPhoneNumber());
            serializer.startTag("", "Points");
            serializer.attribute("", "class", "java.util.ArrayList");

            if (geolocations != null) {
                for (Geolocation geolocation : geolocations) {
                    serializeGeolocation(serializer, geolocation);
                }
            }
            serializer.endTag("", "Points");

            serializer.startTag("", "Events");
            serializer.attribute("", "class", "java.util.ArrayList");

            serializer.endTag("", "Events");
            serializer.endTag("", "ClientMessage");
        }

        private void serializeGeolocation(XmlSerializer serializer,
                                          Geolocation geolocation) throws IOException {
            serializer.startTag("", "GeoPoint");

            serializer.startTag("", "Provider");
            serializer.text(geolocation.getProvider());
            serializer.endTag("", "Provider");

            serializer.startTag("", "TimeStamp");
            serializer.text(TimeFormatter.formatDateTime(geolocation.getTime()));
            serializer.endTag("", "TimeStamp");

            serializer.startTag("", "Latitude");
            serializer.text(String.valueOf(geolocation.getLatitude()));
            serializer.endTag("", "Latitude");

            serializer.startTag("", "Longitude");
            serializer.text(String.valueOf(geolocation.getLongitude()));
            serializer.endTag("", "Longitude");

            serializer.startTag("", "Speed");
            serializer.text(String.valueOf(geolocation.getSpeed()));
            serializer.endTag("", "Speed");

            serializer.startTag("", "Accuracy");
            serializer.text(String.valueOf(geolocation.getAccuracy()));
            serializer.endTag("", "Accuracy");

            serializer.endTag("", "GeoPoint");
        }

    }

    private byte[] head;
    private byte[] body = new byte[0];

    private ReportRequest() {
    }

    @Override
    public void onRequest(OutputStream output) throws IOException {
        output.write(head);
        output.write(body);
    }

    @Override
    public ReportResponse onReceive(String response) {
        if (response == null || response.length() == 0){
            return new ReportResponse(new DalimoRemoteException("Сервер не отвечает."));
        } else if (response.contains("ERR")){
            return new ReportResponse(new DalimoRemoteException(response));
        }
        return new ReportResponse();
    }

}