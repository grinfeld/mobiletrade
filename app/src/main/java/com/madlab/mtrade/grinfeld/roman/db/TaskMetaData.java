package com.madlab.mtrade.grinfeld.roman.db;

public class TaskMetaData {
    public final static byte STATE_CREATED = 0;
    public final static byte STATE_ACCEPTED = 1;
    public final static byte STATE_COMPLETED = 2;
    public final static byte STATE_DECLINED = 3;
    public final static byte STATE_CANCELED = 4;

    public final static byte IMPORTANCE_LOW = -1;
    public final static byte IMPORTANCE_DEFAULT = 0;
    public final static byte IMPORTANCE_HIGH = 1;

    public static final String TABLE_NAME = "Tasks";

    public static final String FIELD_UUID = "UUID";
    public static final String FIELD_TITLE = "Title";
    public static final String FIELD_STATE = "TaskCurrentState";
    public static final String FIELD_AGENT = "CodeAgentTask";
    public static final String FIELD_CLIENT = "CodeClientTask";
    public static final String FIELD_DATE_CREATION = "CreationDate";
    public static final String FIELD_DATE_DUE = "DateDue";
    public static final String FIELD_IMPORTANCE = "Importance";
    public static final String FIELD_SENDED = "TaskSended";
    public static final String FIELD_CODE_MANAGER = "TaskCodeManager";
    public static final String FIELD_UNCONFIRMED_TASK = "UNCONFIRMED_TASK";

    public static final byte FIELD_UUID_INDEX = 0;
    public static final byte FIELD_TITLE_INDEX = 1;
    public static final byte FIELD_STATE_INDEX = 2;
    public static final byte FIELD_AGENT_INDEX = 3;
    public static final byte FIELD_CLIENT_INDEX = 4;
    public static final byte FIELD_DATE_CREATION_INDEX = 5;
    public static final byte FIELD_DATE_DUE_INDEX = 6;
    public static final byte FIELD_IMPORTANCE_INDEX = 7;
    public static final byte FIELD_SENDED_INDEX = 8;
    public static final byte FIELD_CODE_MANAGER_INDEX = 9;
    public static final byte FIELD_UNCONFIRMED_TASK_INDEX = 10;

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s 	nvarchar(36)	PRIMARY KEY,"//0 - UUID
                    + "%s	nvarchar(140)	NOT NULL DEFAULT '',"//Title
                    + "%s	short		    NOT NULL DEFAULT 0,"//current state
                    + "%s	nvarchar(6)		NOT NULL DEFAULT '',"//agent
                    + "%s	nvarchar(6)		NOT NULL DEFAULT '',"//client
                    + "%s	datetime	    NOT NULL DEFAULT 0,"//creation date
                    + "%s	datetime		NOT NULL DEFAULT 0,"//due date
                    + "%s	short			NOT NULL DEFAULT 0,"//importance
                    + "%s	short			NOT NULL DEFAULT 0, "//sended sign
                    + "%s text '',"
                    + "%s	short			NOT NULL DEFAULT 0)",
            TABLE_NAME,
            FIELD_UUID,
            FIELD_TITLE,
            FIELD_STATE,
            FIELD_AGENT,
            FIELD_CLIENT,
            FIELD_DATE_CREATION,
            FIELD_DATE_DUE,
            FIELD_IMPORTANCE,
            FIELD_SENDED,
            FIELD_CODE_MANAGER,
            FIELD_UNCONFIRMED_TASK
    );

}

