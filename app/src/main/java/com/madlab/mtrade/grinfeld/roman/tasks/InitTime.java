package com.madlab.mtrade.grinfeld.roman.tasks;

import android.os.AsyncTask;
import android.util.Log;


import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.net.InetAddress;
import java.util.Date;


/**
 * Created by GrinfeldRA
 */

public class InitTime extends AsyncTask<Void, Void, Date> {

    String TAG = "#InitTime";

    @Override
    protected Date doInBackground(Void... voids) {
        return getRealTime();
    }


    private String[] NTP_SERVERS = new String[]{"0.ru.pool.ntp.org", "1.ru.pool.ntp.org", "2.ru.pool.ntp.org", "3.ru.pool.ntp.org"};

    private Date getRealTime() {
        try {
            NTPUDPClient timeClient = new NTPUDPClient();
            timeClient.setDefaultTimeout(5000);
            for (String server : NTP_SERVERS){
                InetAddress inetAddress = InetAddress.getByName(server);
                TimeInfo timeInfo = timeClient.getTime(inetAddress);
                long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
                Date time = new Date(returnTime);
                Log.d(TAG, String.valueOf(time));
                return time;
            }
        } catch (Exception e) {
            Log.d("#InitTime", e.toString());
        }
        return null;
    }

}
