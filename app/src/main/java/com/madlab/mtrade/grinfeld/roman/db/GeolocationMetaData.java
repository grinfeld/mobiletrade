package com.madlab.mtrade.grinfeld.roman.db;

/**
 * Created by GrinfeldRA on 12.07.2018.
 */

public class GeolocationMetaData {

    public static final String TABLE_NAME = "geolocations";

    public static final String COLUMN_PROVIDER = "provider";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_SPEED = "speed";
    public static final String COLUMN_ACCURACY = "accuracy";
    public static final String COLUMN_SEND = "send";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT NOT NULL, " +
                    "%s REAL NOT NULL, %s REAL NOT NULL, " +
                    "%s REAL NOT NULL DEFAULT 0, %s REAL NOT NULL DEFAULT 0, " +
                    "%s INTEGER NOT NULL DEFAULT 0);",
            TABLE_NAME, COLUMN_TIME,
            COLUMN_PROVIDER, COLUMN_LATITUDE,
            COLUMN_LONGITUDE, COLUMN_SPEED,
            COLUMN_ACCURACY, COLUMN_SEND
    );

}
