package com.madlab.mtrade.grinfeld.roman.iface;


import com.madlab.mtrade.grinfeld.roman.tasks.GetOrderListTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetOrderStatusTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetReturnsListTask;

public interface IGetStatus {

	void onTaskComplete(GetOrderStatusTask orderStatusTask);

	void onTaskComplete(GetOrderListTask orderListTask);

	void onTaskComplete(GetReturnsListTask returnListTask);
}
