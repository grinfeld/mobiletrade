package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

public final class MatrixMetaData{
    public static final String TABLE_NAME = "Matrix";

    public static final String FIELD_CODE_CLIENT = "CodeCli";
    public static final String FIELD_CODE_GOODS = "CodeGoods";
    public static final String FIELD_PRICE = "MatrixPrice";
    public static final String FIELD_SORT_ORDER = "SortOrder";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	money			NOT NULL DEFAULT 0,"
                    + "%s	int	            NOT NULL DEFAULT 0)", TABLE_NAME,
            FIELD_CODE_CLIENT, FIELD_CODE_GOODS, FIELD_PRICE, FIELD_SORT_ORDER);

    public static final String INDEX = String.format(
            "CREATE INDEX idxMatrixSortOrder ON %s ( %s )", TABLE_NAME,
            FIELD_SORT_ORDER);

    public static String insertQuery(String client, String goods, float price,
                                     int sortOrder) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s ("
                        + "%s, %s, %s, %s) VALUES ("
                        + "'%s', '%s', %.2f, %d)",
                TABLE_NAME,
                FIELD_CODE_CLIENT, FIELD_CODE_GOODS, FIELD_PRICE, FIELD_SORT_ORDER,
                client, goods, price, sortOrder);
    }
}
