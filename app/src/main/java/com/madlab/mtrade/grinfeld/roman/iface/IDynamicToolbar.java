package com.madlab.mtrade.grinfeld.roman.iface;

/**
 * Created by grinfeldra
 */
public interface IDynamicToolbar {

    void onChangeToolbar(String value);
}
