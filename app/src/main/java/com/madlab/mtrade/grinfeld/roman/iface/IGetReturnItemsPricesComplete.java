package com.madlab.mtrade.grinfeld.roman.iface;


import com.madlab.mtrade.grinfeld.roman.tasks.GetReturnItemPricesTask;

public interface IGetReturnItemsPricesComplete {
	void onTaskComplete(GetReturnItemPricesTask task);
}
