package com.madlab.mtrade.grinfeld.roman.services;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;

import android.app.IntentService;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.GoodsNLMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;
import okhttp3.ResponseBody;


public class LoadNonLiquidGoodsListIntentService extends IntentService {
    private final String TAG = "!->LoadNonLiquidTask";
    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.loadnonliquidgoodslisttask";

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;
    public static final String EXTRA_STATUS = "TaskStatus";
    public static final String EXTRA_MESSAGE = "ErrorMessage";
    public static final String EXTRA_GOODS = "GoodsList";

    private final String FAILURE = "<ERR>";
    private final String COMMAND = "COMMAND";
    private final String PROC_NAME = "ОстаткиБрака";
    private final String GOODS_DIVIDER = ";";
    private final String GOODS_SUB_DIVIDER = "|";

    private byte status = STATUS_OK;
    private String mUnswer = "Ошибка";
    private String mCodeAgent = "XXXXXX";
    private ArrayList<Goods> mGoodsList;
    private Credentials connectInfo;

    public LoadNonLiquidGoodsListIntentService() {
        super(BROADCAST_ACTION);
    }

    @Override
    protected void onHandleIntent(Intent arg0) {
        connectInfo = Credentials.load(getApplicationContext());
        mCodeAgent = GlobalProc.getPref(getApplicationContext(), R.string.pref_codeManager);

        String message = FAILURE;
        App settings = App.get(getApplicationContext());
        if (settings.connectionServer.equals("1")) {
            String data = String.format("%s;%s;%s;", COMMAND, mCodeAgent, PROC_NAME);
            DalimoClient myClient = new DalimoClient(connectInfo);

            // А теперь всё это дело запускаем
            if (!myClient.connect()) {
                mUnswer = "Не удалось подключиться";
                status = STATUS_ERROR;
                setResult();
                return;
            }

            myClient.send(data + Const.END_MESSAGE);

            message = myClient.receive();

            // отключаемся
            myClient.disconnect();
        } else {
            try {
                String region = App.getRegion(getApplicationContext());
                Response response = OkHttpClientBuilder.buildCall(PROC_NAME, mCodeAgent, region).execute();
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        message = jsonObject.getString("data");
                    }
                }
            } catch (Exception e) {
                status = STATUS_ERROR;
            }
        }


        // А теперь обрабатываем полученное сообщение
        if (!message.contains(FAILURE)) {
            if (!parseMessage(message)) {
                mUnswer = "Ошибка при обработке входящего сообщения";
                status = STATUS_ERROR;
                setResult();
                return;
            }
        } else {
            int index = message.indexOf(FAILURE);
            if (index > 0) {
                mUnswer = message.substring(0, index);
            }
            status = STATUS_ERROR;
            setResult();
            return;
        }

        setResult();
    }

    /**
     * Отправляем результат
     */
    private void setResult() {
        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, status);
        result.putExtra(EXTRA_MESSAGE, mUnswer);
        if (mGoodsList != null) {
            Collections.sort(mGoodsList, (goods, t1) -> goods.getName().compareToIgnoreCase(t1.getName()));
        }
        result.putParcelableArrayListExtra(EXTRA_GOODS, mGoodsList);
        sendBroadcast(result);
    }

    private boolean parseMessage(String data) {
        String[] goodsList = data.split(GOODS_DIVIDER);
        SQLiteDatabase db = null;
        try {
            MyApp app = (MyApp) getApplicationContext();
            db = app.getDB();

            db.execSQL(GoodsNLMetaData.CLEAR);

            mGoodsList = new GoodsList();

            for (String item : goodsList) {
                StringTokenizer st = new StringTokenizer(item,
                        GOODS_SUB_DIVIDER);

                if (st.countTokens() < 5)
                    continue;

                String code = st.nextToken();
                String priceStr = st.nextToken();
                String comment = st.nextToken();
                String dateOfChangeStr = st.nextToken();
                String countStr = st.nextToken();

                Goods goods = Goods.load(db, code);
                if (goods != null) {
                    float price = GlobalProc.parseFloat(priceStr);

                    goods.price(price);
//					goods.setPriceContract(price);
//					goods.setPriceMarket(price);
//					goods.setPriceShop(price);
//					goods.setPriceOptSection(price);
//					goods.setPriceRegion(price);
//					goods.setPriceStandart(price);
                    String info = String.format("%s %s", comment,
                            dateOfChangeStr);
                    goods.setInfo(info);
                    float count = Float.parseFloat(countStr.replace(",", "."));

                    goods.setRestOnStore(count);

                    // пишем в базу
//					Date dateOfChange = TimeFormatter.parse1CDate(dateOfChangeStr);
//
//					ContentValues nl = new ContentValues();
//					nl.put(GoodsNLMetaData.FIELD_CODE, goods.getCode());
//					nl.put(GoodsNLMetaData.FIELD_REST, count);
//					nl.put(GoodsNLMetaData.FIELD_PRICE, price);
//					nl.put(GoodsNLMetaData.FIELD_COMMENT, comment);
//					nl.put(GoodsNLMetaData.FIELD_DATE_OF_CHANGE,
//							TimeFormatter.sdfSQL.format(dateOfChange));
//
//					db.insert(GoodsNLMetaData.TABLE_NAME, null, nl);

                    // Добавляем в список товаров
                    mGoodsList.add(goods);
                }
            }

            Order orderData = Order.getOrder();
            if (orderData != null) {
                for (OrderItem item : orderData.getItems()) {
                    for (Goods goods : mGoodsList) {
                        if (item != null
                                && goods != null
                                && item.getCodeGoods().equalsIgnoreCase(
                                goods.getCode())) {
                            goods.inDocument(true);
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return true;
    }
}
