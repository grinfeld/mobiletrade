package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.GetMinAmountOrder;

/**
 * Created by GrinfeldRA on 26.04.2018.
 */

public interface IGetMinAmountOrder {

    void onTaskComplete(GetMinAmountOrder task);

}
