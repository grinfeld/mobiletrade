package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.components.DatePickerFragment;
import com.madlab.mtrade.grinfeld.roman.components.ScheduleRoutes;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Firm;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.ShippingPlan;
import com.madlab.mtrade.grinfeld.roman.eventbus.DateChanged;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.services.UpdateDataIntentService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_EDIT_ORDER;


/**
 * Created by GrinfeldRA on 26.10.2017.
 */

public class OrderCommonFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "#OrderCommonFragment";
    private final static byte STEP_ORDER_GOODS = 11;
    private final static byte DOC_NEW = 21;
    private final static byte DOC_EDIT = 22;
    private final static byte DOC_BACK = 23;
    public static final String ACTIONS = "actions";
    private byte currentDocType = 0;
    private byte docBack = 0;
    private Client mClient;
    private TextView tvName, tv_reg_mercury;
    private TextView tvAddress, oc_btOk, oc_btCancel;
    private Spinner spinnerListFirms;
    private Order data;
    private EditText etSumToGet;
    private RadioButton rbQualityNorm, rbQualityBrak, rbNal, rbBezNal;
    private CheckBox cbWarranty, cbSelf, cbDontShip, cbGetMoney, oc_checkPrintBill, cbSert;//cbQualDoc
    private ImageButton oc_btAdvantage, oc_btCalm;
    EditText etNote;
    Button btDateShip;
    private Activity context;

    public static OrderCommonFragment newInstance(Client mClient) {
        Bundle args = new Bundle();
        args.putParcelable(Client.KEY, mClient);
        OrderCommonFragment fragment = new OrderCommonFragment();
        fragment.setArguments(args);
        return fragment;
    }

    BroadcastReceiver receiver;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null)
            context.unregisterReceiver(receiver);
    }

    void setupReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getByteExtra(UpdateDataIntentService.EXTRA_STATUS, (byte) 0) == UpdateDataIntentService.STATUS_OK) {
                    updateData(false);
                }
            }
        };
        IntentFilter filter = new IntentFilter(UpdateDataIntentService.BROADCAST_ACTION);
        context.registerReceiver(receiver, filter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        setHasOptionsMenu(true);
        context = getActivity();
        View v = inflater.inflate(R.layout.fragment_order_common, container, false);
        tvName = v.findViewById(R.id.oc_lbName);
        spinnerListFirms = v.findViewById(R.id.oc_listFirms);
        tvAddress = v.findViewById(R.id.oc_lbAddress);
        tv_reg_mercury = v.findViewById(R.id.tv_reg_mercury);
        rbQualityNorm = v.findViewById(R.id.oc_radioQualityNorm);
        rbQualityBrak = v.findViewById(R.id.oc_radioQualityNonLiquid);
        btDateShip = v.findViewById(R.id.btn_get_date);
        rbNal = v.findViewById(R.id.oc_radioNal);
        rbBezNal = v.findViewById(R.id.oc_radioBeznal);
        cbWarranty = v.findViewById(R.id.oc_checkWarrant);
        oc_btAdvantage = v.findViewById(R.id.oc_btAdvantage);
        oc_btCalm = v.findViewById(R.id.oc_btClaim);
        //cbQualDoc = v.findViewById(R.id.oc_checkQualityDoc);
        cbSert = v.findViewById(R.id.oc_checkSertificate);
        cbSelf = v.findViewById(R.id.oc_checkSelfDelivery);
        cbDontShip = v.findViewById(R.id.oc_checkDontShip);
        cbGetMoney = v.findViewById(R.id.oc_checkGetMoney);
        oc_checkPrintBill = v.findViewById(R.id.oc_checkPrintBill);
        etNote = v.findViewById(R.id.oc_tbNote);
        etSumToGet = v.findViewById(R.id.oc_etSumToGet);
        oc_btOk = v.findViewById(R.id.oc_btOk);
        oc_btCancel = v.findViewById(R.id.oc_btCancel);
        v.findViewById(R.id.routes_list_click_view).setOnClickListener(v1 -> showDialog());
        setupReceiver();
        if (currentDocType == 0) {
            if (getTargetRequestCode() == IDD_EDIT_ORDER) {
                currentDocType = DOC_EDIT;
            } else {
                currentDocType = DOC_NEW;
            }
        }
        mClient = getArguments().getParcelable(Client.KEY);
        if (mClient != null) {
            if (mClient.isRegMercury()) {
                tv_reg_mercury.setVisibility(View.GONE);
            } else {
                tv_reg_mercury.setVisibility(View.VISIBLE);
            }
        }
        data = Order.getOrder();
        if (data != null && data.getClient() != null
                && data.getClient().currentFirm() != null) {


            ArrayList<Firm> list = new ArrayList<>();
            int selectedIndex = -1;
            int i = 0;
            for (Map.Entry<String, String> entry : data.getClient().currentFirm().entrySet()) {
                short firmID = -1;
                try {
                    firmID = Short.parseShort(entry.getKey());
                } catch (Exception e) {

                }
                Firm item = Firm.load(context, firmID);
                if (item != null) {
                    list.add(item);
                }
                if (data.firmFK() == firmID) {
                    selectedIndex = i;

                }
                i++;
            }
            if (data.getClient().currentFirm().size() > 1) {
                spinnerListFirms.setVisibility(View.VISIBLE);
                ArrayAdapter<Firm> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, list);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
                spinnerListFirms.setAdapter(adapter);
                spinnerListFirms.setTag(list);
                try {
                    if (currentDocType == DOC_EDIT) {
                        if (list.size() > 0)
                            spinnerListFirms.setSelection(selectedIndex);
                        data.firmFK(list.get(selectedIndex).getCode());
                    } else {
                        if (list.size() > 0) {
                            short code = list.get(0).getCode();
                            data.firmFK(code);
                        }

                    }
                } catch (ArrayIndexOutOfBoundsException e) {

                }

                spinnerListFirms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            Firm selected = (Firm) parent.getAdapter().getItem(position);
                            Order.getOrder().firmFK(selected.getCode());
                            updateData(false);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }else {
                data.firmFK(list.get(0).getCode());
            }
        }
        initListeners();
        //Client client = Order.getOrder().getClient();
        Date date = null;
        if (data != null) {
            date = data.getDateShip();
        }
        if (date != null) {
            setButtonDateShipText(TimeFormatter.sdfDate.format(date));
        } else {
            GregorianCalendar gc = new GregorianCalendar();
            gc.add(Calendar.DATE, 1);
            setButtonDateShipText(TimeFormatter.sdfDate.format(gc.getTime()));
        }
        tvName.setText(mClient.mName);
        tvAddress.setText(mClient.mPostAddress);
        updateData(false);
        return v;
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GlobalProc.closeKeyboard(context);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case STEP_ORDER_GOODS:
                if (resultCode == RESULT_OK) {
                    updateData(true);
                    getTargetFragment().onActivityResult(requestCode, resultCode, data);
                    getFragmentManager().popBackStack();
                }
                break;
            case DOC_BACK:
                currentDocType = DOC_EDIT;
                docBack = (byte) requestCode;
                Intent startUpload = new Intent(context, UpdateDataIntentService.class);
                context.startService(startUpload);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    private void printClientData(Dialog v) {
        if (Order.getOrder() == null || Order.getOrder().getClient() == null) {
            return;
        }
        Client client = Order.getOrder().getClient();
        Display display = context.getWindowManager().getDefaultDisplay();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        int descrWidth = (dm.widthPixels / 6) + 12;
        ScheduleRoutes dummy = v.findViewById(R.id.oc_srDummy);
        if (dummy != null) {
            dummy.setShowText(true);
            dummy.setDescriptionText("");
            dummy.setDescriptionWidth(descrWidth);
            dummy.setRoute("0000000");
        }
        if (client.mShippingPlan != null) {
            for (ShippingPlan plan : client.mShippingPlan) {
                ScheduleRoutes sr = null;
                switch (plan.mCode) {
                    case 1:
                        sr = v.findViewById(R.id.oc_srCommon);
                        break;
                    case 2:
                        sr = v.findViewById(R.id.oc_srFast);
                        break;
                    case 3:
                        sr = v.findViewById(R.id.oc_srFreeze);
                        break;
                    case 4:
                        sr = v.findViewById(R.id.oc_srMars);
                        break;
                }
                showShippingPlan(plan, sr, descrWidth);
            }
        }
    }

    private void showShippingPlan(ShippingPlan shippingPlan,
                                  ScheduleRoutes current, int descriptionWidth) {
        if (current != null) {
            String description = "";
            switch (shippingPlan.mCode) {
                case 1:
                    description = ShippingPlan.ROUTE_COMMON;
                    break;
                case 2:
                    description = ShippingPlan.ROUTE_PERISHABLE;
                    break;
                case 3:
                    description = ShippingPlan.ROUTE_FREEZE;
                    break;
                case 4:
                    description = ShippingPlan.ROUTE_MARS;
                    break;
                default:
                    break;
            }
            current.setDescriptionText(description);
            current.setDescriptionWidth(descriptionWidth);
            current.setRoute(shippingPlan.mPlan);
        }
    }

    private void initListeners() {
        rbNal.setOnClickListener(this);
        rbBezNal.setOnClickListener(this);
        oc_btOk.setOnClickListener(this);
        oc_btCancel.setOnClickListener(this);
        btDateShip.setOnClickListener(this);
        rbQualityNorm.setOnClickListener(this);
        rbQualityBrak.setOnClickListener(this);
        oc_btCalm.setOnClickListener(this);
        oc_btAdvantage.setOnClickListener(this);
        cbDontShip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Order data = Order.getOrder();
                if (data != null) {
                    data.mTypeVisit = isChecked ? (byte) 3 : (byte) 0;
                }
            }
        });
        cbGetMoney.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Order data = Order.getOrder();
                if (data != null) {
                    data.mTakeMoney = isChecked;
                }
                if (etSumToGet != null) {
                    etSumToGet.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                }
            }
        });
        cbSert.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Order data = Order.getOrder();
                if (data != null) {
                    data.mSertificates = isChecked;
                    data.setSertificateSignForAllItems(isChecked);
                }
            }
        });

//        cbQualDoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                Order data = Order.getOrder();
//                if (data != null) {
//                    data.mQualityDocs = isChecked;
//                    data.setQualityDocSignForAllItems(isChecked);
//                }
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_date:
                showDatePickerDialog(view, Order.getOrder().getDateShip());
                break;
            case (R.id.oc_btCancel): {
                getFragmentManager().popBackStack();
                break;
            }
            case (R.id.oc_btOk): {
                if (Order.getOrder() == null) {
                    Order.newOrder(App.get(context).getCodeAgent());
                    Order.getOrder().setClient(mClient);
                }
                Order order = Order.getOrder();
                order.quality();
                if (cbGetMoney != null && etSumToGet != null) {
                    if (cbGetMoney.isChecked()) {
                        float sum = GlobalProc.parseFloat(etSumToGet.getText().toString());
                        if (sum <= 0) {
                            etSumToGet.requestFocus();
                            GlobalProc.mToast(context, getString(R.string.mes_input_sum));
                            return;
                        } else {
                            Order.getOrder().sumToGet(sum);
                        }
                    } else {
                        Order.getOrder().sumToGet(0f);
                    }
                }

                updateData(true);

                Calendar shippingDate = Calendar.getInstance();
                shippingDate.setTime(Order.getOrder().getDateShip());
                shippingDate.set(Calendar.HOUR_OF_DAY, 23);
                shippingDate.set(Calendar.MINUTE, 59);
                Calendar now = Calendar.getInstance();
                if (shippingDate.before(now)) {
                    GlobalProc.mToast(context, "Проверьте дату поставки");
                    return;
                }
                String action;
                // Если документ новый, то сохраняем данные заявки, чтобы в
                // случае падения не потерять
                if (currentDocType == DOC_NEW) {
                    Order.saveUnconfirmedOrder(context);
                    action = getString(R.string.intent_new_order_goods);
                } else {
                    if (docBack == 0) {
                        action = getString(R.string.intent_edit_order_goods);
                    } else {
                        action = getString(R.string.intent_new_order_goods);
                    }
                }
                Fragment fragment = new OrderGoodsFragment();
                Bundle bundle = new Bundle();
                bundle.putString(ACTIONS, action);
                fragment.setArguments(bundle);
                fragment.setTargetFragment(this, STEP_ORDER_GOODS);
                getFragmentManager().beginTransaction()
                        .replace(R.id.contentMain, fragment, OrderGoodsFragment.TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            }
            case (R.id.oc_radioQualityNorm):
                if (Order.getOrder().quality() != Order.QUALITY_NORMAL && Order.getOrder().getItemsCount() > 0) {
                    dlgConfirmQualityChange().show();
                } else {
                    Order.getOrder().quality(Order.QUALITY_NORMAL);
                }
                break;
            case R.id.oc_radioQualityNonLiquid:
                if (Order.getOrder().quality() != Order.QUALITY_NON_LIQUID && Order.getOrder().getItemsCount() > 0) {
                    dlgConfirmQualityChange().show();
                } else {
                    Order.getOrder().quality(Order.QUALITY_NON_LIQUID);
                }
                break;
            case R.id.oc_radioBeznal:
                oc_checkPrintBill.setChecked(true);
                oc_checkPrintBill.setEnabled(false);
                break;
            case R.id.oc_radioNal:
                oc_checkPrintBill.setEnabled(true);
                break;
            case R.id.oc_btAdvantage:
                showAdvantageDiaolg();
                break;
            case R.id.oc_btClaim:
                try {
                    String url = String.format(
                            "http://clients.dalimo.ru/m/new.php?c=%s&k=%s", App
                                    .get(context.getApplicationContext()).getCodeAgent(),
                            mClient.getCode());
                    Intent newClaim = new Intent(Intent.ACTION_VIEW);
                    newClaim.setData(Uri.parse(url));
                    startActivity(newClaim);
                } catch (Exception e) {
                    GlobalProc.mToast(context, e.toString());
                }
                break;
        }
    }

    private void showAdvantageDiaolg() {
        final Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_advantage_order_common);
        Button imageButton = builder.findViewById(R.id.cancel_dialog);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });
        builder.show();
    }

    private void showDatePickerDialog(View view, Date start) {
        DialogFragment newFragment = new DatePickerFragment(view, start);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    protected Dialog dlgConfirmQualityChange() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.cap_confirm));
        builder.setMessage(getString(R.string.mes_confirm_change_quality));
        builder.setIcon(R.mipmap.main_icon);
        builder.setPositiveButton(getString(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Order data = Order.getOrder();
                        if (data != null) {
                            data.clear();
                            if (data.quality() == Order.QUALITY_NORMAL) {
                                data.quality(Order.QUALITY_NON_LIQUID);
                            } else {
                                data.quality(Order.QUALITY_NORMAL);
                            }
                        }
                    }
                });
        builder.setNegativeButton(getString(android.R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Order data = Order.getOrder();
                        if (data.quality() == Order.QUALITY_NORMAL && rbQualityNorm != null) {
                            rbQualityNorm.setChecked(true);
                        } else {
                            if (data.quality() == Order.QUALITY_NON_LIQUID && rbQualityBrak != null)
                                rbQualityBrak.setChecked(true);
                        }
                        dialog.cancel();
                    }
                });
        builder.setCancelable(false);
        return builder.create();
    }

    private void updateData(boolean direction) {
        Order data = Order.getOrder();
        if (data == null) {
            return;
        }
        try {
            if (direction) {
                data.mTypeMoney = (byte) (rbNal.isChecked() ? 1 : 2);
                data.mPrintBill = (byte) (oc_checkPrintBill.isChecked() ? 1 : 2);
                data.mWarranty = cbWarranty.isChecked();
                //data.mSertificates = cbSert.isChecked();
                //TODO удалить поле
                //data.mQualityDocs = cbQualDoc.isChecked();
                data.selfDelivery(cbSelf.isChecked());

                data.mNote = GlobalProc.normalizeString(etNote.getText()
                        .toString());
            } else {
                setButtonDateShipText(TimeFormatter.sdfDate.format(data.getDateShip()));

                if (data.quality() == Order.QUALITY_NORMAL) {
                    rbQualityNorm.setChecked(true);
                } else {
                    rbQualityBrak.setChecked(true);
                }

                String typePay = mClient.currentFirm().get("0" + data.mFirmFK);
                if (currentDocType == DOC_NEW) {
                    if (typePay != null && !typePay.equals("")) {
                        if (typePay.equals("П")) {
                            data.mTypeMoney = 2;
                        } else {
                            data.mTypeMoney = 1;
                        }
                    } else {
                        //если клиент по умолчанию работает по безналу
                        if (mClient.mTypeDoc == 1) {
                            data.mTypeMoney = 2;
                        } else {
                            data.mTypeMoney = 1;
                        }
                    }
                }
                /**
                 * Тип оплаты (нал = 1/безнал = 2)
                 * Печатать чек (да = 1/нет =2)
                 * */
                if (data.mTypeMoney == 2) {
                    oc_checkPrintBill.setChecked(true);
                    oc_checkPrintBill.setEnabled(false);
                    rbBezNal.setChecked(true);
                } else {
                    rbBezNal.setChecked(false);
                    rbNal.setChecked(true);
                    oc_checkPrintBill.setEnabled(true);
                    if (data.mPrintBill == 2) {
                        oc_checkPrintBill.setChecked(false);
                    } else {
                        oc_checkPrintBill.setChecked(true);
                    }
                }

                cbWarranty.setChecked(data.mWarranty);
                data.checkAllItemsHaveSertificatesChecked();

                cbSert.setChecked(data.mSertificates);
                //cbQualDoc.setChecked(data.mQualityDocs);
                cbSelf.setChecked(data.selfDelivery());

                cbDontShip.setChecked(data.mTypeVisit == 3);
                cbGetMoney.setChecked(data.mTakeMoney);
                etSumToGet.setVisibility(data.mTakeMoney ? View.VISIBLE
                        : View.GONE);
                etSumToGet.setText(Float.toString(data.sumToGet()));

                etNote.setText(data.mNote);

            }
        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
    }

    public void setButtonDateShipText(String newText) {
        btDateShip.setText("Дата поставки: " + newText);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateChanged(DateChanged dateChanged) {
        int year = dateChanged.getYear();
        int month = dateChanged.getMonth();
        int day = dateChanged.getDay();
        Calendar dateShip = Calendar.getInstance();
        dateShip.set(Calendar.YEAR, year);
        dateShip.set(Calendar.MONTH, month);
        dateShip.set(Calendar.DAY_OF_MONTH, day);
        dateShip.set(Calendar.HOUR, 0);
        dateShip.set(Calendar.MINUTE, 0);
        Order.getOrder().setDateShip(dateShip.getTime());
        setButtonDateShipText(String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year));
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_transport_routes);
        printClientData(dialog);
        Button dialogButton = dialog.findViewById(R.id.oc_btDialogDismiss);
        dialogButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }
}
