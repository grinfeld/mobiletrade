package com.madlab.mtrade.grinfeld.roman.connectivity;


import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public abstract class DalimoRequest<Response extends DalimoResponse> {

    protected InetSocketAddress address;

    public DalimoRequest(){
    }

    public DalimoRequest(InetSocketAddress address){
        this.address = address;
    }

    public DalimoRequest address(InetSocketAddress address){
        this.address = address;
        return this;
    }

    /* package */ InetSocketAddress getAddress(){
        return address;
    }

    public abstract void onRequest(OutputStream output) throws IOException;

    public abstract Response onReceive(String response);

    public final Response execute() throws DalimoClientException {
        return DalimoClient.execute(this);
    }
}
