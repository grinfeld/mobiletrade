package com.madlab.mtrade.grinfeld.roman.tasks;


import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.db.DiscountsMetaData;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;


public class LoadGoodsGroupTask extends AsyncTask<Void, Integer, Boolean> {
    private final static String TAG = "!->LoadGoodsGroupTask";
    public final static byte RES_SUCCESS = 0;
    public final static byte RES_FAIL = 1;

    private Context mBaseContext;
    private ILoadGoodsComplete mLoadGoodsCompleteListener;
    private String mUnswer;

    /**
     * вариант загрузки списка групп
     * MODE_ALL = 0
     * MODE_MATRIX = 1
     */
    private byte mMode = GoodsList.MODE_ALL;

    private ArrayList<Node> mGoodsTree;
    private Node mSelectedNode;
    private String mClient;

    public String getMessage() {
        return mUnswer;
    }

    public void setTree(ArrayList<Node> tree){
        mGoodsTree = tree;
    }

    /**
     * Задача для загрузки товаров из БД
     *
     * @param baseContext
     *            контекст формы для отображения индикатора выполнения
     * @param listener
     *            обработчик событий
     * @param mode
     *            режим загрузки
     * @param codeClient
     *            код клиента (нужен для загрузки ТЦ матрицы и последних продаж)
     */
    public LoadGoodsGroupTask(Context baseContext, ILoadGoodsComplete listener,
                              byte mode, Node selected, String codeClient) {
        mBaseContext = baseContext;
        mLoadGoodsCompleteListener = listener;
        mMode = mode;
        mSelectedNode = selected;
        mClient = codeClient;
    }

    @Override
    protected void onPreExecute() {
        // mProgress = new ProgressDialog(mBaseContext);
        // mProgress.setCancelable(true);
        // mProgress.setIcon(R.drawable.dalimo);
        // mProgress.setMessage(mBaseContext.getText(R.string.mes_wait_for_load));
        // mProgress.show();
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) mBaseContext.getApplicationContext();
            db = app.getDB();
            switch (mMode) {
                case GoodsList.MODE_ALL:
                    if (mGoodsTree == null){
                        mGoodsTree = Node.loadSubGroups(db, App.get(mBaseContext).getRootCode(), (byte)0);
                        Log.d(TAG, "mGoodsTree: "+ String.valueOf(mGoodsTree));
                        setPrices(db, App.get(mBaseContext).getRootCode(), mClient, mGoodsTree);
                    } else{
                        //Ищем узел, который разворачиваем и загружаем для него подгруппы
                        if (mSelectedNode != null){
                            ArrayList<Node> child = Node.loadSubGroups(db, mSelectedNode.getCode(), (byte)(mSelectedNode.getLevel()+1));
                            for (Node node : child) {
                                node.discount(mSelectedNode.discount());
                            }

                            if (mClient != null && mClient.length() > 0)
                                setPrices(db, mSelectedNode.getCode(), mClient, child);

                            if (child != null) mSelectedNode.setChild(child);
                        }
                    }
                    break;
                case GoodsList.MODE_MATRIX:
				     //GoodsList goodsList = GoodsList.loadMatrix(db, DBHelper.MATRIX_QUERY, new String[]{mClient});
                    //А теперь строим дерево

                    break;
				case GoodsList.MODE_STM:
                    if (mGoodsTree == null){
                        mGoodsTree = Node.loadSubGroupsSTM(db, App.get(mBaseContext).getRootCode(), (byte)0);
                        Log.d(TAG, "mGoodsTree: "+ String.valueOf(mGoodsTree));
                        setPrices(db, App.get(mBaseContext).getRootCode(), mClient, mGoodsTree);
                    } else{
                        //Ищем узел, который разворачиваем и загружаем для него подгруппы
                        if (mSelectedNode != null){
                            ArrayList<Node> child = Node.loadSubGroupsSTM(db, mSelectedNode.getCode(), (byte)(mSelectedNode.getLevel()+1));
                            for (Node node : child) {
                                node.discount(mSelectedNode.discount());
                            }

                            if (mClient != null && mClient.length() > 0)
                                setPrices(db, mSelectedNode.getCode(), mClient, child);

                            if (child != null) mSelectedNode.setChild(child);
                        }
                    }
				    break;
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            Log.e(TAG, mUnswer);
            return false;
        }
        return true;
    }

    /**
     * Рекурсивный алгоритм исключения пустых групп. Работает медленно (25 сек.), требует дополнительных итераций.
     * @param db
     * @param nodes
     * @param cliCode
     * @return
     */
//	private ArrayList<Node> startRecreateTree(SQLiteDatabase db, ArrayList<Node> nodes, String cliCode){
//		for (int i = 0; i < nodes.size(); i++) {
//			Node subNode = nodes.get(i);
//			if (subNode.getChild() != null && subNode.getChild().size() > 0){
//				ArrayList<Node> newChild = startRecreateTree(db, subNode.getChild(), cliCode);
//				subNode.setChild(newChild);
//				nodes.set(i, subNode);
//			}
//			else{
//				Cursor rows = db.rawQuery(DBHelper.MATRIX_QUERY_GROUP, new String[]{subNode.getCode(), cliCode});
//
//				if (rows.getCount() == 0){
//					nodes.remove(i);
//				}
//
//				rows.close();
//			}
//		}
//		return nodes;
//	}

    /**
     * Проставляем цены на одном уровне. Поскольку подгруппы мы подгружаем, рекурсия нам не нужна
     *
     * @param db
     * @param rootCode
     * @return
     */
    public void setPrices(SQLiteDatabase db, String rootCode, String codeClient, ArrayList<Node> nodes) {
        if (nodes == null) return;

        String[] values = null;
        Cursor rows = null;
        try
        {
            values = new String[] { App.get(mBaseContext).getCodeAgent()};
            //Выбираем группы с нужным кодом агента
            String where = String.format("%s = 1 AND %s = ? ",
                    DiscountsMetaData.FIELD_IS_GROUP,
                    DiscountsMetaData.FIELD_CODE_AGENT);

            if (codeClient != null){
                values = new String[] { App.get(mBaseContext).getCodeAgent(), codeClient};
                where = String.format("%s AND %s = ?", where, DiscountsMetaData.FIELD_CODE_CLIENT);
            }

            rows = db.query(DiscountsMetaData.TABLE_NAME, null, where, values,
                    null, null, null);

            int colGoods = rows.getColumnIndex(DiscountsMetaData.FIELD_CODE_GOODS);
            int colDiscount = rows.getColumnIndex(DiscountsMetaData.FIELD_DISCOUNT);
            while (rows.moveToNext()) {
                for (Node node : nodes) {
                    if (node.getCode().equalsIgnoreCase(rows.getString(colGoods))){
                        Log.d(TAG, node.getCode() +" "+ rows.getString(colGoods));
                        node.discount((byte)rows.getInt(colDiscount));
                        break;
                    }
                }
            }
        }
        catch (Exception e){
            Log.e(TAG, e.toString());
        }
        finally {
            rows.close();
        }
    }

//	private Node find(ArrayList<Node> nodes, String code){
//		for (Node node : nodes) {
//			if (node.getCode() == code) return node;
//
//			if (node.getChild() != null && node.getChild().size() > 0){
//				Node subnode = find(node.getChild(), code);
//				if (subnode == null)
//					continue;
//				else
//					return subnode;
//			}
//		}
//		return null;
//	}

    @Override
    protected void onPostExecute(Boolean result) {
        // dismiss();

        if (mLoadGoodsCompleteListener != null) {
            mLoadGoodsCompleteListener.onTaskComplete(this);
        }
    }

    public ArrayList<Node> getGoodsTree(){
        return mGoodsTree;
    }
}