package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.GetShipmentMustListTask;

/**
 * Created by GrinfeldRA on 10.05.2018.
 */

public interface IGetShipmentMustList {

    void getShipmentMustListComplete(GetShipmentMustListTask task);

}
