package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.ArrayList;
import java.util.Date;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TransitMetaData;

public class Goods implements Parcelable {
    public static final float MIN_WEIGHT = (float) 0.01;
    public static final String DEFAULT_NAME_FULL = "<Название не указано>";
    public static final String KEY = "goods";

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public enum Type implements Parcelable {

        DEFAULT(0),
        PROMOTION(1),
        BONUS(2);

        private int value;

        private Type(int value) {
            this.value = value;
        }

        Type(Parcel in) {
            value = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(value);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Type> CREATOR = new Creator<Type>() {
            @Override
            public Type createFromParcel(Parcel in) {
                return Type.values()[in.readInt()];
            }

            @Override
            public Type[] newArray(int size) {
                return new Type[size];
            }
        };

        public int getValue() {
            return value;
        }
    }

    public int getIsMercury() {
        return isMercury;
    }

    public void setIsMercury(int isMercury) {
        this.isMercury = isMercury;
    }

    public int getIsCustom() {
        return isCustom;
    }

    public void setIsCustom(int isCustom) {
        this.isCustom = isCustom;
    }

    public boolean isNonCash() {
        return isNonCash;
    }

    public void setNonCash(boolean nonCash) {
        isNonCash = nonCash;
    }

    private int isMercury = 0;

    private int isCustom = 0;

    private boolean isNonCash = false;

    //private static final String TAG = "!->Goods";
    private int type = Type.DEFAULT.getValue();

    /**
     * Код товара
     */
    private String mCode;// mCode

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    /**
     * Краткое название (для списка)
     */
    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    /**
     * Код родителя
     */
    private String mParentCode;

    public String getParentCode() {
        return mParentCode;
    }

    public void setParentCode(String code) {
        mParentCode = code;
    }

    /**
     * Полное наименование товара
     */
    private String mNameFull;

    public String getNameFull() {
        return mNameFull;
    }

    public void setNameFull(String nameFull) {
        if (nameFull.length() < 1)
            nameFull = DEFAULT_NAME_FULL;
        else
            mNameFull = nameFull;
    }

    /**
     * Базовая единица измерения
     */
    private String mBaseMeash;

    public String getBaseMeash() {
        return mBaseMeash;
    }

    public void setBaseMeash(String name) {
        mBaseMeash = name;
    }

    /**
     * Единица измерения упаковки
     */
    private String mPackMeash;

    public String getPackMeash() {
        return mPackMeash;
    }

    public void setPackMeash(String name) {
        mPackMeash = name;
    }

    /**
     * Количество в упаковке
     */
    private float mNumInPack;

    public float getNumInPack() {
        return mNumInPack;
    }

    public void setNumInPack(float numInPack) {
        if (numInPack < 1)
            mNumInPack = 1;
        else
            mNumInPack = numInPack;
    }

    /**
     * Квант отгрузки
     */
    private float mQuant;

    public float getQuant() {
        return mQuant;
    }

    public void setQuant(float quant) {
        if (quant < 0.001f)
            this.mQuant = 1;
        else
            this.mQuant = quant;
    }

    /**
     * Проверяет, дробный квант или нет...
     *
     * @return
     */
    public boolean isFractional() {
        return (mQuant - (int) mQuant) > 0f;
    }

    /**
     * Остаток на складе
     */
    private float mRestOnStore;

    public float getRestOnStore() {
        return mRestOnStore;
    }

    public void setRestOnStore(float r) {
        mRestOnStore = r;
    }

    /**
     * Цена контракт (price 0)
     */
    private float mPriceContract;

    public float getPriceContract() {
        return mPriceContract;
    }

    public void setPriceContract(float price) {
        mPriceContract = price;
    }

    /**
     * Цена стандарт (price 1)
     */
    private float mPriceStandart;

    public float getPriceStandart() {
        return mPriceStandart;
    }

    public void setPriceStandart(float price) {
        mPriceStandart = price;
    }

    /**
     * Цена магазины (price2)
     */
    private float mPriceShop;

    public float getPriceShop() {
        return mPriceShop;
    }

    public void setPriceShop(float price) {
        mPriceShop = price;
    }

    /**
     * Цена для рынков (price 3)
     */
    private float mPriceMarket;

    public float getPriceMarket() {
        return mPriceMarket;
    }

    public void setPriceMarket(float price) {
        mPriceMarket = price;
    }

    /**
     * Цена для области (price 4)
     */
    private float mPriceRegion;

    public float getPriceRegion() {
        return mPriceRegion;
    }

    public void setPriceRegion(float price) {
        mPriceRegion = price;
    }

    /**
     * Цена для оптовых секций (price 5)
     */
    private float mPriceOptSection;

    public float getPriceOptSection() {
        return mPriceOptSection;
    }

    public void setPriceOptSection(float price) {
        mPriceOptSection = price;
    }

    /**
     * Видимая цена (после расчёта)
     */
    private float mPrice;

    public float price() {
        return mPrice;
    }

    public void price(float price) {
        mPrice = price;
    }

    /**
     * Срок годности
     */
    private String mBestBefore;

    public String getBestBefore() {
        return mBestBefore;
    }

    public void setBestBefore(String bb) {
        mBestBefore = bb;
    }

    /**
     * НДС
     */
    private byte mNDS;

    public Byte getNDS() {
        return mNDS;
    }

    public void setNDS(byte nds) {
        mNDS = nds;
    }

    /**
     * Страна происхождения
     */
    private String mCountry;

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    /**
     * Номер ГТД
     */
    private String mGTD;

    public String getGTD() {
        return mGTD;
    }

    public void setGTD(String gtd) {
        mGTD = gtd;
    }

    /**
     * Вес единицы
     */
    private float mWeight;

    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float weight) {
        if (weight <= MIN_WEIGHT)
            mWeight = MIN_WEIGHT;
        else
            mWeight = weight;
    }

    /**
     * 0 - Ничего 1 - Мастлист 2 - Новинка 3 - Понижение цены
     */
    private byte mHighLight;

    public byte getHighLight() {
        return mHighLight;
    }

    public void setHighLight(byte hl) {
        mHighLight = hl;
    }

    /**
     * Можем отгружать по половине головки (только для весового товара)
     */
    private boolean mIsHalfHead;

    public boolean isHalfHead() {
        return mIsHalfHead;
    }

    public void isHalfHead(boolean halfHead) {
        mIsHalfHead = halfHead;
    }

    /**
     * Признак весового товара
     */
    private boolean mIsWeightGood;

    public boolean isWeightGood() {
        return mIsWeightGood;
    }

    public void setIsWeightGood(boolean wg) {
        mIsWeightGood = wg;
    }

    /**
     * Признак СТМ
     */
    private boolean mIsSTM;

    public boolean stm() {
        return mIsSTM;
    }

    public void stm(boolean stm) {
        mIsSTM = stm;
    }

    /**
     * Отметка
     */
    private boolean mInOrder;

    /**
     * Изменяет признак наличия товара в документе на противополжный
     */
    public void toggle() {
        mInOrder = !mInOrder;
    }

    /**
     * Признак наличия товара в документе
     */
    public boolean inDocument() {
        return mInOrder;
    }

    /**
     * Изменяет признак наличия товара в документе
     *
     * @param check признак. True - есть, False - нет
     */
    public void inDocument(boolean check) {
        mInOrder = check;
    }

    // /**
    // * Маст-Лист
    // */
    // private boolean mInMustList;
    //
    // public boolean inMustList(){
    // return mInMustList;
    // }
    //
    // public void inMustList(boolean inMustList){
    // mInMustList = inMustList;
    // }
    //
    // /**
    // * Новинка
    // */
    // private boolean mIsNewbee;
    //
    // public boolean isNewbee(){
    // return mIsNewbee;
    // }
    //
    // public void isNewbee(boolean isNewbee){
    // mIsNewbee = isNewbee;
    // }

    /**
     * Произвольная информация
     */
    private String mInfo;

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    /**
     * Признак скоропорта
     */
    private boolean isPerishable;

    public void perishable(boolean value) {
        isPerishable = value;
    }

    public boolean perishable() {
        return isPerishable;
    }

    /**
     * Код ответственного менеджера
     */
    private String mManager;

    /**
     * Возвращает код ответственного менеджера
     */
    public String manager() {
        return mManager;
    }

    /**
     * Устанавливает код ответственного менеджера
     */
    public void manager(String value) {
        mManager = value;
    }

    private ArrayList<Shipment> mTransitList;

    public void transitList(ArrayList<Shipment> list) {
        mTransitList = list;
    }

    public ArrayList<Shipment> transitList() {
        return mTransitList;
    }

    private boolean isShipmentMustList = false;

    public boolean isShipmentMustList() {
        return isShipmentMustList;
    }

    public void setSnipmentMustList(boolean shipmentMustList) {
        isShipmentMustList = shipmentMustList;
    }

    /**
     * Конструктор
     */
    public Goods(String code, String name, String parent, String nameFull,
                 String baseMeash, String packMeash, short numInPack, short quant,
                 int restOnStore, float priceContract, float priceStandart,
                 float priceShop, float priceRegion, float priceMarket,
                 float priceOptSection, String bestBefore, byte nds, String gtd,
                 float weight, String yellowReserv, String redReserv,
                 byte highLight, String info, String manager,
                 boolean halfHead, boolean weightGood, boolean inOrder,
                 boolean perishable, boolean stm, int type) {
        mCode = code;
        mName = name;
        mParentCode = parent;
        setNameFull(nameFull);
        mBaseMeash = baseMeash;
        mPackMeash = packMeash;

        setNumInPack(numInPack);
        setQuant(quant);
        mRestOnStore = restOnStore;

        mPrice = priceShop;
        mPriceContract = priceContract;
        mPriceStandart = priceStandart;
        mPriceShop = priceShop;
        mPriceRegion = priceRegion;
        mPriceMarket = priceMarket;
        mPriceOptSection = priceOptSection;

        mBestBefore = bestBefore;
        mNDS = nds;
        mGTD = gtd;
        setWeight(weight);

        mHighLight = highLight;

        mInfo = info;
        mManager = manager;

        mIsHalfHead = halfHead;
        mIsWeightGood = weightGood;
        mInOrder = inOrder;
        isPerishable = perishable;
        mIsSTM = stm;

        mTransitList = null;
    }

    /**
     * Конструктор без параметров
     */
    public Goods() {
        this("", "", "", "", "", "", (short) 1, (short) 1, 0, (float) 0,
                (float) 0, (float) 0, (float) 0, (float) 0, (float) 0, "",
                (byte) 10, "", MIN_WEIGHT, "0", "0", (byte) 0, "", "",
                false, false, false, false, false, 0);
    }

    /**
     * Конструктор с необходимыми параметрами
     */
    public Goods(String code, String parent, String name) {
        this(code, name, parent, "", "", "", (short) 1, (short) 1, 0,
                (float) 0, (float) 0, (float) 0, (float) 0, (float) 0,
                (float) 0, "", (byte) 10, "", MIN_WEIGHT, "0", "0", (byte) 0,
                "", "", false, false, false, false, false, 0);
    }

    /**
     * Конструктор для парселизации
     */
    @SuppressWarnings("unchecked")
    public Goods(Parcel parcel) {
        mCode = parcel.readString();
        mName = parcel.readString();
        mParentCode = parcel.readString();
        mNameFull = parcel.readString();
        mBaseMeash = parcel.readString();
        mPackMeash = parcel.readString();
        mNumInPack = parcel.readFloat();
        mQuant = parcel.readFloat();
        mRestOnStore = parcel.readFloat();

        mPrice = parcel.readFloat();
        mPriceContract = parcel.readFloat();
        mPriceStandart = parcel.readFloat();
        mPriceShop = parcel.readFloat();
        mPriceRegion = parcel.readFloat();
        mPriceMarket = parcel.readFloat();
        mPriceOptSection = parcel.readFloat();

        mBestBefore = parcel.readString();
        mNDS = parcel.readByte();
        mGTD = parcel.readString();
        mCountry = parcel.readString();
        mWeight = parcel.readFloat();
        mHighLight = parcel.readByte();

        mInfo = parcel.readString();
        mManager = parcel.readString();

        mTransitList = parcel.readArrayList(getClass().getClassLoader());

        boolean[] tmp = new boolean[5];
        parcel.readBooleanArray(tmp);
        mIsHalfHead = tmp[0];
        mIsWeightGood = tmp[1];
        mInOrder = tmp[2];
        isPerishable = tmp[3];
        mIsSTM = tmp[4];
        type = parcel.readInt();
        isMercury = parcel.readInt();
        isCustom = parcel.readInt();
        isNonCash = parcel.readInt() == 1;
    }

    /**
     * Добавляем Транзит в список
     */
    public void addTransitItem(Shipment item) {
        if (mTransitList == null) {
            mTransitList = new ArrayList<Shipment>();
            mTransitList.add(item);
        } else {
            mTransitList.add(item);
        }
    }

    /**
     * Очищаем список транзитов
     */
    public void clearTransitList() {
        mTransitList.clear();
        mTransitList = null;
    }

    /**
     * Загружаем список транзитов из БД
     */
    public static ArrayList<Shipment> loadTransitList(SQLiteDatabase database,
                                                      String codeGoods) {
        String selection = String.format("%s = ?",
                TransitMetaData.FIELD_CODE_GOODS.Name);

        Cursor query = database.query(TransitMetaData.TABLE_NAME, null,
                selection, new String[]{codeGoods}, null, null, null);

        ArrayList<Shipment> result = null;
        int columnDate = query.getColumnIndex(TransitMetaData.FIELD_DATE.Name);

        if (query.getCount() > 0) {
            result = new ArrayList<Shipment>();
        }

        while (query.moveToNext()) {
            Shipment item = new Shipment(TimeFormatter.parseSqlDate(query.getString(columnDate)),
                    query.getInt(TransitMetaData.FIELD_COUNT.Index));

            result.add(item);
        }

        query.close();

        return result;
    }

    /**
     * Загружает информацию о товаре из БД
     *
     * @param db
     * @param code
     * @return
     */
    public static Goods load(SQLiteDatabase db, String code) {
        Goods goods = null;

        String where = String.format("%s = '%s'",
                GoodsMetaData.FIELD_CODE.Name, code);// GoodsMetaData.FIELD_PARENT
        Cursor results = db.query(GoodsMetaData.TABLE_NAME, null, where, null,
                null, null, null);

        while (results.moveToNext()) {
            goods = new Goods();

            goods.mCode = results.getString(GoodsMetaData.FIELD_CODE.Index);
            goods.mName = results.getString(GoodsMetaData.FIELD_NAME.Index);
            goods.setParentCode(results
                    .getString(GoodsMetaData.FIELD_PARENT.Index));
            String tmp = results.getString(GoodsMetaData.FIELD_NAME_FULL.Index)
                    .length() == 0 ? DEFAULT_NAME_FULL : results
                    .getString(GoodsMetaData.FIELD_NAME_FULL.Index);
            goods.mNameFull = tmp;
            goods.mBaseMeash = results
                    .getString(GoodsMetaData.FIELD_BASE_MEASH.Index);
            goods.mPackMeash = results
                    .getString(GoodsMetaData.FIELD_PACK_MEASH.Index);
            goods.mNumInPack = results
                    .getFloat(GoodsMetaData.FIELD_NUM_IN_PACK.Index);

            float w = results.getFloat(GoodsMetaData.FIELD_WEIGHT.Index);
            float q = results.getFloat(GoodsMetaData.FIELD_QUANT.Index);

            goods.setQuant(q);

            goods.mRestOnStore = results.getInt(GoodsMetaData.FIELD_REST.Index);

            goods.mPriceContract = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_CONTRACT.Index);
            goods.mPriceStandart = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_STANDART.Index);
            goods.mPriceShop = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index);
            goods.mPriceRegion = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_REGION.Index);
            goods.mPriceMarket = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_MARKET.Index);
            goods.mPriceOptSection = results
                    .getFloat(GoodsMetaData.FIELD_PRICE_SECTION.Index);

            goods.mBestBefore = results
                    .getString(GoodsMetaData.FIELD_BEST_BEFORE.Index);
            goods.mNDS = (byte) results.getShort(GoodsMetaData.FIELD_NDS.Index);
            goods.mCountry = results
                    .getString(GoodsMetaData.FIELD_COUNTRY.Index);
            goods.setWeight(w);
            goods.mHighLight = (byte) results
                    .getShort(GoodsMetaData.FIELD_HIGHLIGHT.Index);

            goods.mIsWeightGood = results
                    .getShort(GoodsMetaData.FIELD_IS_WEIGHT.Index) == 1 ? true
                    : false;
            goods.mIsHalfHead = results
                    .getShort(GoodsMetaData.FIELD_IS_HALFHEAD.Index) == 1 ? true
                    : false;
            goods.isPerishable = results
                    .getShort(GoodsMetaData.FIELD_IS_PERISHABLE.Index) == 1 ? true
                    : false;
            goods.mIsSTM = results
                    .getShort(GoodsMetaData.FIELD_STM.Index) == 1 ? true
                    : false;
            goods.mManager = results
                    .getString(GoodsMetaData.FIELD_MANAGER_FK.Index);

            goods.isMercury = results.getInt(GoodsMetaData.FIELD_IS_MERCURY.Index);
            goods.isCustom = results.getInt(GoodsMetaData.FIELD_IS_CUSTOM.Index);
            goods.isNonCash = results.getInt(GoodsMetaData.FIELD_IS_NON_CASH.Index) == 1;
        }

        results.close();

        if (goods != null) {
            goods.mTransitList = loadTransitList(db, code);
        }

        return goods;
    }

    /**
     * Загружает список НЕЛИКВИДНЫХ товаров из БД
     *
     * @param db БД
     * @return ArrayList<Goods> или null
     */
    public static ArrayList<Goods> loadNLList(SQLiteDatabase db) {
        ArrayList<Goods> result = null;

        String sql = "SELECT GoodsNonLiquid.CodeGoods as Code, Name, NameFull, BaseMeash, PackMeash, NumInPack, "
                + "MinShip, GoodsNonLiquid.RestOnStor as rest, "
                + "Price, "
                + "Comment, DateOfChange, "
                + "WeightBrutto, isWeightGood "
                + "FROM GoodsInStor "
                + "INNER JOIN GoodsNonLiquid "
                + "ON GoodsInStor.CodeGoods = GoodsNonLiquid.CodeGoods";
        Cursor rows = db.rawQuery(sql, null);

        if (rows.moveToFirst()) {
            result = new ArrayList<Goods>();

            while (rows.moveToNext()) {
                Goods goods = new Goods();

                goods.mCode = rows.getString(0);
                goods.mName = rows.getString(1);
                goods.mNameFull = rows.getString(2);
                goods.mBaseMeash = rows.getString(3);
                goods.mPackMeash = rows.getString(4);
                goods.mNumInPack = rows.getFloat(5);

                goods.mQuant = 1;// results.getFloat(6);
                goods.mRestOnStore = rows.getInt(7);
                goods.mPriceContract = rows.getFloat(8);
                goods.mPriceStandart = rows.getFloat(8);
                goods.mPriceShop = rows.getFloat(8);
                goods.mPriceRegion = rows.getFloat(8);
                goods.mPriceMarket = rows.getFloat(8);
                goods.mPriceOptSection = rows.getFloat(8);

                String tmp = rows.getString(9);
                Date date = TimeFormatter.parseSqlDate(rows.getString(10));
                goods.setInfo(String.format("%s от %s", tmp,
                        TimeFormatter.sdf1C.format(date)));

                goods.setWeight(rows.getFloat(11));

                goods.mIsWeightGood = rows.getShort(12) == 1 ? true : false;

                result.add(goods);
            }
        }

        rows.close();

        return result;
    }

    public static Goods loadNLGoods(SQLiteDatabase db, String codeGoods) {
        Goods result = null;

        String sql = "SELECT GoodsNonLiquid.CodeGoods as Code, Name, NameFull, BaseMeash, PackMeash, NumInPack, "
                + "MinShip, GoodsNonLiquid.RestOnStor as rest, "
                + "Price, "
                + "Comment, DateOfChange, "
                + "WeightBrutto, isWeightGood "
                + "FROM GoodsInStor "
                + "INNER JOIN GoodsNonLiquid "
                + "ON GoodsInStor.CodeGoods = GoodsNonLiquid.CodeGoods "
                + "WHERE GoodsInStor.CodeGoods = ?";
        Cursor rows = db.rawQuery(sql, new String[]{codeGoods});

        if (rows.moveToFirst()) {
            result = new Goods();

            result.mCode = rows.getString(0);
            result.mName = rows.getString(1);
            result.mNameFull = rows.getString(2);
            result.mBaseMeash = rows.getString(3);
            result.mPackMeash = rows.getString(4);
            result.mNumInPack = rows.getFloat(5);

            result.mQuant = 1;// results.getFloat(6);
            result.mRestOnStore = rows.getInt(7);

            result.mPriceContract = rows.getFloat(8);
            result.mPriceStandart = rows.getFloat(8);
            result.mPriceShop = rows.getFloat(8);
            result.mPriceRegion = rows.getFloat(8);
            result.mPriceMarket = rows.getFloat(8);
            result.mPriceOptSection = rows.getFloat(8);

            String tmp = rows.getString(9);
            Date date = TimeFormatter.parseSqlDate(rows.getString(10));
            result.setInfo(String.format("%s от %s", tmp,
                    TimeFormatter.sdf1C.format(date)));

            result.setWeight(rows.getFloat(11));

            result.mIsWeightGood = rows.getShort(12) == 1 ? true : false;
        }

        rows.close();

        return result;
    }

    public float getPrice(byte lvl) {
        switch (lvl) {
            case 0:
                return mPriceStandart;
            case 1:
                return mPriceShop;
            case 2:
                return mPriceRegion;
            case 3:
                return mPriceMarket;
            case 4:
                return mPriceContract;
            case 5:
                return mPriceOptSection;
        }
        return mPriceStandart;
    }

    public void calcVisiblePrice(byte level, float discount) {
        Float basePrice = getPrice(level);
        mPrice = basePrice;
        if (discount != 0) {
            //mPrice = GlobalProc.round(basePrice - (basePrice/100)*discount, 2);
            float price = GlobalProc.round(basePrice - (basePrice / 100) * discount, 2);
            if (price >= mPriceContract) {
                mPrice = price;
            } else {
                mPrice = mPriceContract;
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Goods> CREATOR = new Parcelable.Creator<Goods>() {

        public Goods createFromParcel(Parcel in) {
            return new Goods(in);
        }

        public Goods[] newArray(int size) {
            return new Goods[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mCode);
        parcel.writeString(mName);
        parcel.writeString(mParentCode);
        parcel.writeString(mNameFull);
        parcel.writeString(mBaseMeash);
        parcel.writeString(mPackMeash);

        parcel.writeFloat(mNumInPack);
        parcel.writeFloat(mQuant);
        parcel.writeFloat(mRestOnStore);

        parcel.writeFloat(mPrice);
        parcel.writeFloat(mPriceContract);
        parcel.writeFloat(mPriceStandart);
        parcel.writeFloat(mPriceShop);
        parcel.writeFloat(mPriceRegion);
        parcel.writeFloat(mPriceMarket);
        parcel.writeFloat(mPriceOptSection);

        parcel.writeString(mBestBefore);
        parcel.writeByte(mNDS);
        parcel.writeString(mGTD);
        parcel.writeString(mCountry);
        parcel.writeFloat(mWeight);
        parcel.writeByte(mHighLight);

        parcel.writeString(mInfo);
        parcel.writeString(mManager);

        parcel.writeList(mTransitList);

        parcel.writeBooleanArray(new boolean[]{mIsHalfHead, mIsWeightGood,
                mInOrder, isPerishable, mIsSTM});
        parcel.writeInt(type);
        parcel.writeInt(isMercury);
        parcel.writeInt(isCustom);
        parcel.writeInt(isNonCash ? 1 : 0);
    }
}

