package com.madlab.mtrade.grinfeld.roman.iface;

/**
 * Created by GrinfeldRA on 02.04.2018.
 */

public interface IOnChangeSetting {

    void onChangeSetting();

}
