package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.FireBaseMessageExpAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.FireBaseNotification;
import com.madlab.mtrade.grinfeld.roman.iface.IChildClickFireBaseFragment;
import com.madlab.mtrade.grinfeld.roman.services.MyFirebaseMessagingService;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder.buildNotificationChangeRead;

/**
 * Created by GrinfeldRA on 16.02.2018.
 */

public class FireBaseMessageContainer extends Fragment  implements IChildClickFireBaseFragment{


    private View rootView;
    public final static String NOTIFICATION = "notification";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (rootView==null){
            rootView = inflater.inflate(R.layout.fragment_goods_tree_contener, container, false);
            FragmentFireBaseMessageTreeView fragmentFireBaseMessageTreeView = new FragmentFireBaseMessageTreeView();
            if (getArguments()!=null){
                String id = (String) getArguments().get(MyFirebaseMessagingService.ID_MESSAGE);
                String tag = (String) getArguments().get(MyFirebaseMessagingService.TAG_MESSAGE);
                Bundle bundle = new Bundle();
                bundle.putString(MyFirebaseMessagingService.ID_MESSAGE, id);
                bundle.putString(MyFirebaseMessagingService.TAG_MESSAGE, tag);
                fragmentFireBaseMessageTreeView.setArguments(bundle);
            }
            getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragmentFireBaseMessageTreeView).commit();
            if (rootView.findViewById(R.id.detailFragmentContainer) != null){
                FireBaseMessageDetail fireBaseMessageDetail = new FireBaseMessageDetail();
                getFragmentManager().beginTransaction().replace(R.id.detailFragmentContainer, fireBaseMessageDetail).commit();
            }
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        if (rootView.getParent() != null) {
            ((ViewGroup)rootView.getParent()).removeView(rootView);
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(FireBaseNotification fireBaseNotification, String token, FireBaseMessageExpAdapter adapter) {
        FireBaseMessageDetail fireBaseMessageDetail = new FireBaseMessageDetail();
        buildNotificationChangeRead(fireBaseNotification.getId(), token).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
        fireBaseNotification.setRead(1);
        adapter.notifyDataSetChanged();
        Bundle bundle = new Bundle();
        bundle.putParcelable(NOTIFICATION, fireBaseNotification);
        fireBaseMessageDetail.setArguments(bundle);
        if (rootView.findViewById(R.id.detailFragmentContainer) != null){
            getFragmentManager().beginTransaction().replace(R.id.detailFragmentContainer, fireBaseMessageDetail).commit();
        }else {
            getFragmentManager().beginTransaction().replace(R.id.contentMain, fireBaseMessageDetail).addToBackStack(null).commitAllowingStateLoss();
        }
    }
}
