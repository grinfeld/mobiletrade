package com.madlab.mtrade.grinfeld.roman.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.AuthManager;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.TransitAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.ReportRequest;
import com.madlab.mtrade.grinfeld.roman.connectivity.ReportResponse;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DatabaseHelperManager;
import com.madlab.mtrade.grinfeld.roman.db.GeolocationEntityMapper;
import com.madlab.mtrade.grinfeld.roman.db.GeolocationMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Geolocation;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Created by GrinfeldRA on 11.07.2018.
 */

public class ReportingService  extends IntentService {

    private DBHelper dbHelper;

    private AuthManager authManager = AuthManager.getInstance();
    private String employeeId = authManager.getEmployeeId();

    public ReportingService() {
        super(ReportingService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        dbHelper = DatabaseHelperManager.getDatabaseHelper(this);
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (employeeId == null) return;
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            while (true) {
                List<Geolocation> geolocations = GeolocationEntityMapper.get(db,
                        String.format("%s = %s", GeolocationMetaData.COLUMN_SEND,
                                0), null, GeolocationMetaData.COLUMN_TIME, "500");


                if (geolocations.size() == 0) {
                    Log.d("#GeolocationService ","geolocations.size() == 0" );
                    break;
                }
                String server1 = GlobalProc.getPref(this, R.string.pref_ftp_server);
                int gpsPort = GlobalProc.getPrefInt(this, R.string.pref_gpsPort, 1001);
                ReportRequest report = new ReportRequest.Builder()
                        .address(new InetSocketAddress(server1, gpsPort))
                        .employeeId(employeeId)
                        .geolocations(geolocations)
                        .build();

                ReportResponse response = report.execute();
                if (!response.isSuccess()) {
                    Log.d("#GeolocationService 1", String.valueOf(response.getError()));
                    throw response.getError();
                }else {
                    Log.d("#GeolocationService response success", response.toString());
                }
                ContentValues gValues = new ContentValues(1);
                gValues.put(GeolocationMetaData.COLUMN_SEND, 1);
                for (Geolocation geolocation : geolocations) {
                    db.update(GeolocationMetaData.TABLE_NAME, gValues,
                            String.format("%s = %s",
                                    GeolocationMetaData.COLUMN_TIME,
                                    String.valueOf(geolocation.getTime())), null);

                }
            }


            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.cancel(R.id.notifications_reporting_service_error);
            }

        } catch (Exception e) {
            Log.d("#GeolocationService 2", String.valueOf(e.getMessage()));
            Notification notification = new NotificationCompat.BigTextStyle(
                    new NotificationCompat.Builder(this, MyApp.NOTIFICATION_CHANNEL_ID_INFO)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("MTRADE")
                            .setContentText(getString(R.string.reporting_error))
                            .setPriority(2)
                            .setDefaults(Notification.DEFAULT_ALL))
                    .bigText(getString(R.string.failed_to_connect_to_the_server))
                    .build();
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(R.id.notifications_reporting_service_error, notification);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbHelper = null;
        DatabaseHelperManager.releaseDatabaseHelper();
    }
}