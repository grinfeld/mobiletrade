package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRA on 20.06.2018.
 */

public class Image {

    private String medium;


    public Image(String medium) {
        this.medium = medium;
    }

    public String getMedium() {
        return medium;
    }
}
