package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */

public class PromotionMetaData {


    public static final String TABLE_NAME = "Promotion";

    public static final String FIELD_ID= "id_promotion";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_TEXT = "text";
    public static final String FIELD_COVER = "cover";
    public static final String FIELD_TIMESTAMP = "fieldTimeStamp";


    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s integer NOT NULL DEFAULT 0,"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s text NOT NULL DEFAULT '')",
            TABLE_NAME, FIELD_ID, FIELD_TITLE, FIELD_TEXT, FIELD_COVER, FIELD_TIMESTAMP);

    public static String insertQuery(int id, String title, String text, String cover, String timeStamp) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s, %s) "
                        + "VALUES "
                        + "(%d, '%s', '%s', '%s', '%s')",
                TABLE_NAME, FIELD_ID, FIELD_TITLE, FIELD_TEXT, FIELD_COVER, FIELD_TIMESTAMP,
                id, title, text, cover, timeStamp);
    }


}
