package com.madlab.mtrade.grinfeld.roman.entity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnItemMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by GrinfeldRA on 21.11.2017.
 */

public class Returns {

    private final static String TAG = "!->Returns";

    /**
     * Номер для неподтвержденной заявки
     */
    public static short UNCONFIRMED_RETURN_NUMBER = 998;

    public static final String KEY = "returns";

    private ArrayList<ReturnItem> mItemsList;

    private ArrayList<PaybackPhoto> paybackPhotos;

    private float mTotalWeight = 0f;
    private float mTotalSum = 0f;

    private int typeServer = 0;

    public static void deleteUnsavedReturn(SQLiteDatabase db) {

    }

    public static enum State {
        Normal, Deleted
    }

    public final static String STATE_NORMAL = "V";
    public final static String STATE_DELETED = "X";

    public final static String FORMAT_TIME = "HH:mm";

    /**
     * Событие, генерируемое при добавлении новой позиции
     *
     * @author Konovaloff
     */
    public interface ItemAddListener {
        void onItemAdd(float totalSum, short count, float totalWeight);
    }

    /**
     * Событие, генерируемое при удалении позиции
     *
     * @author Konovaloff
     */
    public interface ItemRemoveListener {
        void onItemRemove(float totalSum, short count, float totalWeight);
    }

    private ItemAddListener mAddItemListener;
    private ItemRemoveListener mRemoveItemListener;

    /**
     * Уведомляет слушатели об изменении
     */
    private void notifyListener() {
        if (mAddItemListener != null) {
            mAddItemListener
                    .onItemAdd(mTotalSum, getItemsCount(), mTotalWeight);
        }
        if (mRemoveItemListener != null) {
            mAddItemListener
                    .onItemAdd(mTotalSum, getItemsCount(), mTotalWeight);
        }
    }

    public void setItemAddListener(ItemAddListener listener) {
        mAddItemListener = listener;
    }

    private static Handler mHandler;

    public static void setHandler(Handler _handler) {
        mHandler = _handler;
    }

    /**
     * Возвращает сумму
     */
    public float getTotalAmount() {
        return mTotalSum;
    }

    /**
     * Возвращает вес
     */
    public float getTotalWeight() {
        return mTotalWeight;
    }

    /**
     * Возвращает количество элементов
     */
    public short getItemsCount() {
        if (mItemsList != null)
            return (short) mItemsList.size();
        else
            return 0;
    }

    public int getTypeServer() {
        return typeServer;
    }

    public void setTypeServer(int typeServer) {
        this.typeServer = typeServer;
    }

    /**
     * Номер заявки (идентификатор)
     */
    private short mNumReturn;

    public short getNumReturn() {
        return mNumReturn;
    }

    public void setNumReturn(short value) {
        mNumReturn = value;
        for (ReturnItem item : mItemsList) {
            item.setNum(value);
        }
    }

    public ArrayList<PaybackPhoto> getPaybackPhotos() {
        return paybackPhotos;
    }

    /**
     * Код менеджера
     */
    private String mCodeAgent;

    /**
     * Код менеджера
     */
    public void setCodeAgent(String codeAgent) {
        mCodeAgent = codeAgent;
    }

    /**
     * Код менеджера
     */
    public String getCodeAgent() {
        return mCodeAgent;
    }

    /**
     * Клиент
     */
    private Client mClient;

    /**
     * Сеттер для клиента
     */
    public void setClient(Client client) {
        if (client == null) {
            Log.e(KEY + ".setClient", "client is null");
        }
        mClient = client;
    }

    public Client getClient() {
        return mClient;
    }

    /**
     * Комментарий
     */
    public String mNote;

    /**
     * Дата возврата
     */
    private Date mDateReturn;

    public void setDateReturn(Date data) {
        mDateReturn = data;
    }

    public Date getDateReturn() {
        return mDateReturn;
    }

    /**
     * Время принятия возврата
     */
    private Date mGetReturnTime;

    private String getReturnTime() {
        if (mGetReturnTime == null) {
            mGetReturnTime = new Date(System.currentTimeMillis());
        }
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIME,
                Locale.getDefault());
        return sdf.format(mGetReturnTime);
    }

    public String mDocNum;

    public void setDocNum(String docNum) {
        mDocNum = docNum.replace('\'', '"').replace('\n', ' ');
    }

    public Date mDocDate;

    /**
     * выкуп.. флаг для фотоотчета
     */
    private byte repayment;

    /**
     * выкуп.. флаг делал ли пользователь фото
     */
    private byte photoIsExist;

    /**
     * Состояние заявки: V - нормальное Х - помечена на удаление
     */
    public State mState;

    /**
     * Тип оплаты (нал/безнал)
     */
    public byte mTypeMoney;

    /**
     * Тип акта (возврат/потеря)
     */
    public byte mTypeAct;

    /**
     * Печатать чек (да/нет) НЕ ИСПОЛЬЗУЕТСЯ
     */
    private byte mTypeDoc;

    /**
     * Признак отправки в 1С. для статуса документа
     */
    private boolean mWasSended;


    /**
     * Признак отправки в 1С. для фотоотчета
     */
    private boolean isReserved;


    public boolean isReserved() {
        return isReserved;
    }

    public void setReserved(boolean reserved) {
        isReserved = reserved;
    }

    public boolean wasSended() {
        return mWasSended;
    }

    public void wasSended(boolean wasSended) {
        this.mWasSended = wasSended;
    }

    /**
     * Номер накладной
     */
    private String invoiceNumber;

    public void setInvoice(String value) {
        invoiceNumber = value;
    }

    public String getInvoice() {
        return invoiceNumber;
    }

    private Date dateGetGoodsBack;

    public void setDateGetGoods(Date dateGetGoods) {
        this.dateGetGoodsBack = dateGetGoods;
    }

    public Date getDateGoodsBack() {
        return dateGetGoodsBack;
    }

    public byte getmTypeAct() {
        return mTypeAct;
    }

    public void setmTypeAct(byte mTypeAct) {
        this.mTypeAct = mTypeAct;
    }

    /**
     * Признак "Везу сам"
     */
    private boolean pickup;

    public void setPickup(boolean pickup) {
        this.pickup = pickup;
    }

    public boolean getPickup() {
        return pickup;
    }

    /**
     * Ссылка на визит
     */
    private UUID uuidDoc;

    public void setUuidDoc(UUID uuidDoc) {
        this.uuidDoc = uuidDoc;
    }

    public UUID getUuidDoc() {
        return uuidDoc;
    }

    /**
     * Ссылка на визит
     */
    private UUID mVisit_fk;

    public void visitFK(UUID id) {
        mVisit_fk = id;
    }

    public UUID visitFK() {
        return mVisit_fk;
    }

    private short mFirmFK;

    public void firmFK(short value) {
        mFirmFK = value;
    }

    public short firmFK() {
        return mFirmFK;
    }


    public void setRepayment(byte repayment) {
        this.repayment = repayment;
    }


    public byte getPhotoIsExist() {
        return photoIsExist;
    }

    public void setPhotoIsExist(byte photoIsExist) {
        this.photoIsExist = photoIsExist;
    }

    /**
     * Конструктор
     */
    public Returns(short num, State retState, String codeAgent, Client client,
                   String docNum, Date docDate, String invoice, Date getGoodsDate,
                   byte typeMoney, byte typeDoc, boolean pickup, String note, byte typeAct, int typeServer) {
        mNumReturn = num;
        mState = retState;
        mCodeAgent = codeAgent;
        mClient = client;
        mTypeAct = typeAct;

        Calendar today = Calendar.getInstance();

        Calendar tommorow = Calendar.getInstance();
        tommorow.add(Calendar.DATE, 1);

        mDateReturn = today.getTime();

        if (getGoodsDate == null) {
            dateGetGoodsBack = tommorow.getTime();
        } else {
            dateGetGoodsBack = getGoodsDate;
        }

        if (docDate == null) {
            mDocDate = today.getTime();
        } else {
            mDocDate = docDate;
        }

        mTypeMoney = typeMoney;
        mTypeDoc = typeDoc;

        mNote = note;

        mWasSended = false;

        isReserved = false;

        mItemsList = new ArrayList<>();

        paybackPhotos = new ArrayList<>();

        invoiceNumber = invoice;
        this.pickup = pickup;

        mFirmFK = 0;

        this.typeServer = typeServer;
    }

    // Конструктор без параметров
    public Returns() {
        this((short) 0, State.Normal, "", null, "", null, "", null, (byte) 1,
                (byte) 2, false, "", (byte) 0, 0);
    }

    public Returns(short numReturn) {
        this(numReturn, State.Normal, "", null, "", null, "", null, (byte) 1,
                (byte) 2, false, "", (byte) 0, 0);
    }

    /**
     * Добавляет элемент в конец списка
     */
    public void addItem(ReturnItem returnItem) {
        if (returnItem == null)
            return;

        returnItem.setNum(mNumReturn);

        if (mItemsList != null) {
            mTotalSum += returnItem.getAmount();

            mTotalWeight += returnItem.getTotalWeight();

            mItemsList.add(returnItem);
            notifyListener();
        }
    }

    /**
     * Удаляет элемент из списка по его индексу
     */
    public void removeItem(int index) {
        if (mItemsList != null && ((-1 < index) && (index < mItemsList.size()))) {
            ReturnItem oi = mItemsList.get(index);

            mTotalSum -= oi.getAmount();
            mTotalWeight -= oi.getTotalWeight();

            mItemsList.remove(index);
            notifyListener();
        }
    }


    public void removeMercuryItems() {
        if (mItemsList != null) {
            for (Iterator<ReturnItem> it = mItemsList.iterator(); it.hasNext(); ) {
                ReturnItem returnItem = it.next();
                if (returnItem.getIsMercury() == 1) {
                    mTotalSum -= returnItem.getAmount();
                    mTotalWeight -= returnItem.getTotalWeight();
                    it.remove();
                }
            }
            notifyListener();
        }
    }

    /**
     * Обновляет данные элемента с номером index в соответствии с данными oi
     */
    private void updateItem(int index, ReturnItem returnItem) {
        if (mItemsList != null) {
            if (-1 < index && index < mItemsList.size()) {
                ReturnItem old = mItemsList.get(index);
                mTotalSum -= old.getAmount();
                mTotalWeight -= old.getWeight();

                mItemsList.set(index, returnItem);

                mTotalSum += returnItem.getAmount();
                mTotalWeight += returnItem.getWeight();

                notifyListener();
            }
        }
    }

    /**
     * Ищет требуемый элемент в списке, а затем обновляет данные
     *
     * @param returnItem новое значение
     * @return TRUE если нашли, FALSE - иначе
     */
    public boolean updateItem(ReturnItem returnItem) {
        int index = getIndex(returnItem.getCodeGoods());
        if (index > -1) {
            updateItem(index, returnItem);
            recalcTotals();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает элемент по индексу, может вернуть null
     */
    public ReturnItem getItem(int index) {
        ReturnItem res = null;
        if (mItemsList == null) {
            res = null;
        }
        if (-1 < index && index < mItemsList.size()) {
            res = mItemsList.get(index);
        }

        return res;
    }

    /**
     * Аксессор для списка товаров
     */
    public ArrayList<ReturnItem> getItems() {
        return mItemsList;
    }

    /**
     * Пересчитывает итоги
     */
    public void recalcTotals() {
        mTotalSum = 0;
        mTotalWeight = 0;

        if (mItemsList == null)
            return;

        for (ReturnItem oi : mItemsList) {
            mTotalSum += oi.getAmount();
            mTotalWeight += oi.getWeight();
        }
        notifyListener();
    }

    /**
     * Ищет элемент по коду и возвращает если найден.
     *
     * @param code
     * @return Если не найден, то NULL
     */
    public ReturnItem find(String code) {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).getCodeGoods().equalsIgnoreCase(code)) {
                return mItemsList.get(i);
            }
        }

        return null;
    }

    /**
     * Ищет элемент по коду
     *
     * @param item
     * @return
     */
    // public int getIndex(ReturnItem item) {
    // int position = getIndex(item.getCodeGoods());
    // return position;
    // }

    /**
     * Ищет элемент по коду
     *
     * @param code код
     * @return индекс элемента или -1
     */
    public short getIndex(String code) {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).getCodeGoods().equalsIgnoreCase(code)) {
                return (short) i;
            }
        }
        return -1;
    }

    public static Returns load(final SQLiteDatabase db, short index)
            throws ParseException {
        String where = String
                .format("%s = %d", ReturnMetaData.FIELD_NUM, index);
        return load(db, where);
    }

    /**
     * Load Загружает данные из БД
     *
     * @param db
     * @throws ParseException
     */
    public static Returns load(final SQLiteDatabase db, String where)
            throws ParseException {
        Cursor rows = db.query(ReturnMetaData.TABLE_NAME, null, where, null, null, null, null);
        if (!rows.moveToFirst()) {
            return null;
        }
        short index = rows.getShort(ReturnMetaData.FIELD_NUM_INDEX);
        Returns res = new Returns(index);
        // 1 - State
        int columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_STATE);
        res.mState = (rows.getString(columnNum).contains(STATE_NORMAL)) ? State.Normal : State.Deleted;
        // 2 - CodeAgent
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_CODE_AGENT);
        res.mCodeAgent = rows.getString(columnNum);
        // 3 - CodeCli
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_CLIENT);
        String code = rows.getString(columnNum);
        res.mClient = Client.load(db, code);
        // 4 - DateShip
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_DATE_SHIP);
        String ds = rows.getString(columnNum);
        res.mDateReturn = TimeFormatter.parseSqlDate(ds);
        // 5 - DocNum
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_DOC_NUM);
        res.mDocNum = rows.getString(columnNum);
        // 6 - DocDate
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_DOC_DATE);
        ds = rows.getString(columnNum);
        res.mDocDate = TimeFormatter.parseSqlDate(ds);
        // 8 - TypePay2
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_TYPE_MONEY);
        res.mTypeMoney = (byte) rows.getShort(columnNum);

        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_TYPE_ACT);
        res.mTypeAct = (byte) rows.getShort(columnNum);
        // 7 - TypePay1
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_TYPE_DOC);
        res.mTypeDoc = (byte) rows.getShort(columnNum);

        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_TYPE_SERVER);
        res.typeServer = rows.getInt(columnNum);
        // 9 - TotalWeight
        // res.mTotalWeight = results.getFloat(9);
        // 10 - TotalSum
        // columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_TOTAL_SUM);
        // res.mTotalSum = rows.getFloat(columnNum);
        // 11 - Note
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_NOTE);
        res.mNote = rows.getString(columnNum);
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_SENDED);
        res.wasSended(rows.getInt(columnNum) == 1);
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_INVOICE);
        res.setInvoice(rows.getString(columnNum));
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_PICKUP);
        res.setPickup(rows.getShort(columnNum) == 1);
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_VISIT_FK);
        String uuid = rows.getString(columnNum);
        try {
            UUID visit = UUID.fromString(uuid);
            res.visitFK(visit);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_DATE_GET_GOODS);
        if (columnNum > -1) {
            ds = rows.getString(columnNum);
            res.dateGetGoodsBack = TimeFormatter.parseSqlDate(ds);
        }
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_FIRM_FK);
        res.firmFK(rows.getShort(columnNum));
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_REPAYMENT);
        res.repayment = (byte) rows.getShort(columnNum);
        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_PHOTO_IS_EXIST);
        res.photoIsExist = (byte) rows.getShort(columnNum);

        columnNum = rows.getColumnIndex(ReturnMetaData.FIELD_UUID_DOC);
        String uuidDocStr = rows.getString(columnNum);
        try {
            UUID uuidDoc = UUID.fromString(uuidDocStr);
            res.setUuidDoc(uuidDoc);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        rows.close();

        // А теперь табличная часть
        // Псевдонимы
        String goodsNumInPack = "goodsNumInPack";
        String riCountPack = "countPack";

        @SuppressLint("DefaultLocale") String sql = String.format(
                "SELECT %s, %s, %s.%s as %s, %s, %s.%s, "
                        + "%s, %s.%s as %s, %s, %s, %s, "
                        + "%s, %s.%s, %s, %s " + "FROM %s "
                        + "INNER JOIN %s ON (%s.%s = %s.%s) "
                        + "WHERE %s.%s = %d",
                // Select
                GoodsMetaData.FIELD_NAME_FULL.Name, // 0
                GoodsMetaData.FIELD_IS_WEIGHT.Name, // 1
                GoodsMetaData.TABLE_NAME,
                GoodsMetaData.FIELD_NUM_IN_PACK.Name, // 2
                // as
                goodsNumInPack,
                ReturnItemMetaData.FIELD_NUM, // 3
                ReturnItemMetaData.TABLE_NAME,
                ReturnItemMetaData.FIELD_CODE, // 4
                ReturnItemMetaData.FIELD_COUNT, // 5
                ReturnItemMetaData.TABLE_NAME,
                ReturnItemMetaData.FIELD_COUNT_PACK, // 6
                // as
                riCountPack,
                ReturnItemMetaData.FIELD_PRICE, // 7
                ReturnItemMetaData.FIELD_AMOUNT, // 8
                ReturnItemMetaData.FIELD_WEIGHT, // 9
                ReturnItemMetaData.FIELD_INFO, // 10
                ReturnItemMetaData.TABLE_NAME,
                ReturnItemMetaData.FIELD_BEST_BEFORE, // 11
                ReturnItemMetaData.FIELD_NEED_CALC_PRICE, // 12
                ReturnItemMetaData.FIELD_IN_PACK, // 13
                // FROM
                GoodsMetaData.TABLE_NAME,
                // INNER JOIN
                ReturnItemMetaData.TABLE_NAME,
                // ON
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
                ReturnItemMetaData.TABLE_NAME, ReturnItemMetaData.FIELD_CODE,
                // WHERE
                ReturnItemMetaData.TABLE_NAME, ReturnItemMetaData.FIELD_NUM,
                index);

        rows = db.rawQuery(sql, null);

        while (rows.moveToNext()) {
            ReturnItem item = new ReturnItem();// index, results.getString(3)

            item.setNum(index);
            // Название товара
            int columnIndex = rows
                    .getColumnIndex(GoodsMetaData.FIELD_NAME_FULL.Name);
            item.setNameGoods(rows.getString(columnIndex));

            // Признак весового товара
            columnIndex = rows
                    .getColumnIndex(GoodsMetaData.FIELD_IS_WEIGHT.Name);
            item.mIsWeightGoods = (rows.getShort(columnIndex) == 1);

            // Справочно количество в упаковке
            columnIndex = rows.getColumnIndex(goodsNumInPack);
            item.setNumInPack(rows.getFloat(columnIndex));

            // Код товара
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_CODE);
            item.setCodeGoods(rows.getString(columnIndex));

            // Количество
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_COUNT);
            item.setCount(rows.getFloat(columnIndex));

            // Множитель, для упаковок
            columnIndex = rows.getColumnIndex(riCountPack);
            item.setCountPack(rows.getInt(columnIndex));

            // Цена
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_PRICE);
            item.setPrice(rows.getFloat(columnIndex));

            // Итоговая сумма
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_AMOUNT);
            item.setAmount(rows.getFloat(columnIndex));

            // Вес
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_WEIGHT);
            item.setWeight(rows.getFloat(columnIndex));

            // Информация
            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_INFO);
            item.setInfo(rows.getString(columnIndex));

            // Срок годности
            // columnIndex = rows
            // .getColumnIndex(ReturnItemMetaData.FIELD_BEST_BEFORE);

            // Признак необходимости расчета цены
//			columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_NEED_CALC_PRICE);
//			item.calcPrice(rows.getShort(columnIndex) == 1);

            columnIndex = rows.getColumnIndex(ReturnItemMetaData.FIELD_IN_PACK);
            item.inPack(rows.getShort(columnIndex) == 1);

            Goods goods = Goods.load(db, item.getCodeGoods());

            item.setIsMercury(goods.getIsMercury());

            res.addItem(item);
        }

        rows.close();

        sql = String.format(Locale.US, "select * from %s where %s = '%s'",
                PaybackPhotoMetaData.TABLE_NAME, PaybackPhotoMetaData.FIELD_UUID, uuidDocStr);

        rows = db.rawQuery(sql, null);
        while (rows.moveToNext()) {
            res.paybackPhotos.add(new PaybackPhoto(rows.getInt(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_ID)),
                    rows.getString(rows.getColumnIndex(PaybackPhotoMetaData.FIELD_URL))));
        }
        rows.close();

        return res;
    }

    /**
     * Записывает данные возврата в БД
     *
     * @param db
     * @return TRUE - если операция завершена успешно, FALSE - иначе.
     */
    public boolean insert(SQLiteDatabase db) {
        if (mItemsList.size() < 1 || mClient == null)
            return false;

        // Общая часть
        ContentValues common = new ContentValues();

        common.put(ReturnMetaData.FIELD_NUM, mNumReturn);
        common.put(ReturnMetaData.FIELD_STATE,
                (mState == State.Normal) ? STATE_NORMAL : STATE_DELETED);
        common.put(ReturnMetaData.FIELD_CODE_AGENT, mCodeAgent);
        common.put(ReturnMetaData.FIELD_CLIENT, mClient.getCode());
        common.put(ReturnMetaData.FIELD_DATE_SHIP, TimeFormatter.sdfSQL.format(mDateReturn));
        common.put(ReturnMetaData.FIELD_TYPE_MONEY, mTypeMoney);
        common.put(ReturnMetaData.FIELD_TYPE_DOC, mTypeDoc);

        common.put(ReturnMetaData.FIELD_NOTE, mNote == null ? " " : mNote);

        common.put(ReturnMetaData.FIELD_DOC_NUM, mDocNum.replace('\n', ' ')
                .replace('\'', '"'));
        common.put(ReturnMetaData.FIELD_DOC_DATE, TimeFormatter.sdfSQL.format(mDocDate));

        common.put(ReturnMetaData.FIELD_TOTAL_WEIGHT, mTotalWeight);
        common.put(ReturnMetaData.FIELD_TOTAL_SUM, mTotalSum);
        common.put(ReturnMetaData.FIELD_GET_RETURN_TIME, getReturnTime());
        common.put(ReturnMetaData.FIELD_SENDED, mWasSended);
        common.put(ReturnMetaData.FIELD_IS_RESERVED, isReserved);
        common.put(ReturnMetaData.FIELD_INVOICE, invoiceNumber);
        common.put(ReturnMetaData.FIELD_DATE_GET_GOODS, TimeFormatter.sdfSQL.format(dateGetGoodsBack));
        common.put(ReturnMetaData.FIELD_PICKUP, pickup);
        common.put(ReturnMetaData.FIELD_TYPE_ACT, mTypeAct);
        common.put(ReturnMetaData.FIELD_REPAYMENT, repayment);
        common.put(ReturnMetaData.FIELD_PHOTO_IS_EXIST, photoIsExist);
        if (mVisit_fk != null) {
            common.put(ReturnMetaData.FIELD_VISIT_FK, mVisit_fk.toString());
        }
        common.put(ReturnMetaData.FIELD_FIRM_FK, mFirmFK);
        if (uuidDoc != null) {
            common.put(ReturnMetaData.FIELD_UUID_DOC, uuidDoc.toString());
        }
        db.beginTransaction();

        try {
            if (db.insert(ReturnMetaData.TABLE_NAME, null, common) == -1) {
                db.endTransaction();
                return false;
            }
        } catch (Exception e) {
            Log.e("ReturnInsertError", e.toString());
            db.endTransaction();
            return false;
        }

        // А теперь табличная часть
        for (ReturnItem ri : mItemsList) {
            ContentValues item = new ContentValues();

            item.put(ReturnItemMetaData.FIELD_NUM, ri.getNum());
            item.put(ReturnItemMetaData.FIELD_CODE, ri.getCodeGoods());
            item.put(ReturnItemMetaData.FIELD_COUNT_PACK, ri.getCountPack());
            item.put(ReturnItemMetaData.FIELD_COUNT, ri.getCount());
            item.put(ReturnItemMetaData.FIELD_PRICE, ri.getPrice());
            item.put(ReturnItemMetaData.FIELD_AMOUNT, ri.getAmount());
            item.put(ReturnItemMetaData.FIELD_WEIGHT, ri.getWeightAtPiece());
            item.put(ReturnItemMetaData.FIELD_INFO, ri.getInfo());
            // item.put(ReturnItemMetaData.FIELD_BEST_BEFORE,
            // ri.getBestBeforeFormattedSQL());
//			item.put(ReturnItemMetaData.FIELD_NEED_CALC_PRICE, ri.calcPrice());
            item.put(ReturnItemMetaData.FIELD_IN_PACK, ri.inPack());

            try {
                if (db.insert(ReturnItemMetaData.TABLE_NAME, null, item) == -1) {
                    db.endTransaction();
                    return false;
                }
            } catch (Exception e) {
                Log.e("ReturnItemInsertError", "item: " + ri.getCodeGoods()
                        + " " + e.toString());
                db.endTransaction();
            }
        }


        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    /**
     * Обновляет данные возврата
     *
     * @param db
     * @return
     */
    public boolean update(SQLiteDatabase db) {
        if (mItemsList.size() < 1 || mClient == null)
            return false;

        // Общая часть
        ContentValues common = new ContentValues();

        common.put(ReturnMetaData.FIELD_NUM, mNumReturn);
        common.put(ReturnMetaData.FIELD_STATE,
                (mState == Returns.State.Normal) ? STATE_NORMAL : STATE_DELETED);
        common.put(ReturnMetaData.FIELD_CODE_AGENT, mCodeAgent);
        common.put(ReturnMetaData.FIELD_CLIENT, mClient.getCode());
        common.put(ReturnMetaData.FIELD_DATE_SHIP, TimeFormatter.sdfSQL.format(mDateReturn));
        common.put(ReturnMetaData.FIELD_TYPE_DOC, mTypeDoc);
        common.put(ReturnMetaData.FIELD_TYPE_MONEY, mTypeMoney);
        common.put(ReturnMetaData.FIELD_TYPE_ACT, mTypeAct);
        common.put(ReturnMetaData.FIELD_NOTE, mNote == null ? " " : mNote);

        common.put(ReturnMetaData.FIELD_DOC_NUM, mDocNum.replace('\n', ' ')
                .replace('\'', '"'));
        common.put(ReturnMetaData.FIELD_DOC_DATE, TimeFormatter.sdfSQL.format(mDocDate));

        common.put(ReturnMetaData.FIELD_TOTAL_WEIGHT, mTotalWeight);
        common.put(ReturnMetaData.FIELD_TOTAL_SUM, mTotalSum);
        common.put(ReturnMetaData.FIELD_GET_RETURN_TIME, getReturnTime());
        common.put(ReturnMetaData.FIELD_INVOICE, invoiceNumber);
        common.put(ReturnMetaData.FIELD_DATE_GET_GOODS, TimeFormatter.sdfSQL.format(dateGetGoodsBack));
        common.put(ReturnMetaData.FIELD_PICKUP, pickup);
        common.put(ReturnMetaData.FIELD_VISIT_FK, mVisit_fk == null ? ""
                : mVisit_fk.toString());
        common.put(ReturnMetaData.FIELD_FIRM_FK, mFirmFK);
        common.put(ReturnMetaData.FIELD_REPAYMENT, repayment);
        common.put(ReturnMetaData.FIELD_PHOTO_IS_EXIST, photoIsExist);
        common.put(ReturnMetaData.FIELD_UUID_DOC, uuidDoc == null ? ""
                : uuidDoc.toString());
        db.beginTransaction();

        String where = String.format("%s = %d", ReturnMetaData.FIELD_NUM,
                mNumReturn);
        try {
            if (db.update(ReturnMetaData.TABLE_NAME, common, where, null) == -1) {// (table,
                // values,
                // whereClause,
                // whereArgs)
                db.endTransaction();
                return false;
            }

            // Удаляем табличную часть
            String query = String.format("DELETE FROM %s WHERE %s",
                    ReturnItemMetaData.TABLE_NAME, where);
            db.execSQL(query);

        } catch (Exception e) {
            Log.e("!->ReturnUpdateError", e.toString());
            db.endTransaction();
            return false;
        }

        // А теперь табличная часть
        for (ReturnItem ri : mItemsList) {
            ContentValues item = new ContentValues();

            item.put(ReturnItemMetaData.FIELD_NUM, ri.getNum());
            item.put(ReturnItemMetaData.FIELD_CODE, ri.getCodeGoods());
            item.put(ReturnItemMetaData.FIELD_COUNT_PACK, ri.getCountPack());
            item.put(ReturnItemMetaData.FIELD_COUNT, ri.getCount());
            item.put(ReturnItemMetaData.FIELD_PRICE, ri.getPrice());
            item.put(ReturnItemMetaData.FIELD_AMOUNT, ri.getAmount());
            item.put(ReturnItemMetaData.FIELD_WEIGHT, ri.getWeightAtPiece());
            item.put(ReturnItemMetaData.FIELD_INFO, ri.getInfo());
            // item.put(ReturnItemMetaData.FIELD_BEST_BEFORE,
            // ri.getBestBeforeFormattedSQL());
//			item.put(ReturnItemMetaData.FIELD_NEED_CALC_PRICE, ri.calcPrice());
            item.put(ReturnItemMetaData.FIELD_IN_PACK, ri.inPack());

            try {
                if (db.insert(ReturnItemMetaData.TABLE_NAME, null, item) == -1) {
                    db.endTransaction();
                    return false;
                }
            } catch (Exception e) {
                Log.e("!->ReturnItemInsertError", "item: " + ri.getCodeGoods()
                        + " " + e.toString());
                db.endTransaction();
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    /**
     * Изменяет признак удаления
     *
     * @param db
     * @param orderID
     * @return
     */
    public static boolean changeDeleteMark(SQLiteDatabase db, short orderID) {
        String sql = String.format("SELECT %s " + "FROM %s " + "WHERE %s = %d",
                ReturnMetaData.FIELD_STATE, ReturnMetaData.TABLE_NAME,
                ReturnMetaData.FIELD_NUM, orderID);

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);

            if (cursor == null) {
                return false;
            }

            if (!cursor.moveToFirst()) {
                return false;
            }

            State lastState = cursor.getString(0)
                    .equalsIgnoreCase(STATE_NORMAL) ? State.Normal
                    : State.Deleted;

            ContentValues common = new ContentValues();

            common.put(ReturnMetaData.FIELD_STATE,
                    (lastState == State.Normal) ? STATE_DELETED : STATE_NORMAL);

            String where = String.format("%s = %d", ReturnMetaData.FIELD_NUM,
                    orderID);
            try {
                db.update(ReturnMetaData.TABLE_NAME, common, where, null);

            } catch (Exception e) {
                Log.e("!->ReturnMarkError", e.toString());
                return false;
            }

            if (mHandler != null) {
                mHandler.sendEmptyMessage(2);
            }
        } catch (Exception e) {
            Log.e("!->changeDeleteMark", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return true;
    }

    public boolean updateSendSign(final SQLiteDatabase db, boolean isReserved, int typeServer) {
        ContentValues common = new ContentValues();
        if (isReserved) {
            common.put(ReturnMetaData.FIELD_SENDED, true);
            common.put(ReturnMetaData.FIELD_TYPE_SERVER, typeServer);
        }
        common.put(ReturnMetaData.FIELD_IS_RESERVED, true);
        String where = String.format("%s = ?", ReturnMetaData.FIELD_NUM);
        int res = db.update(ReturnMetaData.TABLE_NAME, common, where,
                new String[]{Short.toString(mNumReturn)});

        return res > 0;
    }

    public static Boolean isReserved(UUID id) {
        Boolean res = null;
        String sql = String.format("SELECT %s FROM %s WHERE %s = '%s'",
                ReturnMetaData.FIELD_IS_RESERVED, ReturnMetaData.TABLE_NAME,
                ReturnMetaData.FIELD_UUID_DOC, id);
        Cursor rows = null;
        try {
            Context context = MyApp.getContext();
            MyApp app = (MyApp) context;
            SQLiteDatabase db = app.getDB();
            rows = db.rawQuery(sql, null);
            if (rows.moveToFirst()) {
                int columnIndex = rows.getColumnIndex(ReturnMetaData.FIELD_IS_RESERVED);
                res = rows.getInt(columnIndex) == 1;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

    /**
     * Возврат
     */
    private static Returns mReturn;

    public static Returns getReturn() {
        return mReturn;
    }

    public static void setReturn(Returns value) {
        mReturn = value;
    }

    public static void newReturn(String codeAgent, UUID visit) {
        mReturn = new Returns(DBProp.GetNumOrder());
        mReturn.setCodeAgent(codeAgent);
        mReturn.setUuidDoc(UUID.randomUUID());
        mReturn.visitFK(visit);
    }
}