package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.madlab.mtrade.grinfeld.roman.CamActivity;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.JournalAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.Document;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.util.ArrayList;

import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_NEW_PHOTO;

/**
 * Created by grinfeldra
 */
public class VisitSelectFragment extends Fragment {

    private ArrayList<Document> mDocumentList;
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        return inflater.inflate(R.layout.fragment_journal, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.journal_base).setVisibility(View.GONE);
        listView = view.findViewById(android.R.id.list);
        listView.setEmptyView(view.findViewById(R.id.emptyElement));
        MyApp app = (MyApp) getActivity().getApplication();
        SQLiteDatabase db = app.getDB();
        mDocumentList = Document.loadList(db, Const.DOCUMENTS_TYPE.Visit);
        setAdapter();
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            Document document = (Document) parent.getItemAtPosition(position);
            Visit visit = Visit.load(db, document.getID());
            Intent intent = new Intent(getActivity(), CamActivity.class);
            intent.putExtra(VisitContainFragment.CLIENT_CODE, visit.client().getCode());
            intent.putExtra(VisitContainFragment.UUID_KEY, visit.getID().toString());
            startActivityForResult(intent, IDD_NEW_PHOTO);
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IDD_NEW_PHOTO) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.contentMain);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //getParentFragment().getTargetFragment().onActivityResult(requestCode, resultCode, data);
            } else {
                fragment.getTargetFragment().onActivityResult(requestCode, resultCode, data);
            }
            getActivity().getFragmentManager().popBackStack();
        }
    }

    private void setAdapter() {
        if (mDocumentList == null) {
            return;
        }
        JournalAdapter mAdapter = new JournalAdapter(getActivity(), R.layout.item_journal,
                mDocumentList, Const.DOCUMENTS_TYPE.Visit);
        listView.setAdapter(mAdapter);
    }
}
