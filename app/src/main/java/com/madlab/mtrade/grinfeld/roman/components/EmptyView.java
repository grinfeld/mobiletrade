package com.madlab.mtrade.grinfeld.roman.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;


public class EmptyView extends RelativeLayout {

	private TextView tvText;
	private ImageButton btAction;

	public EmptyView(Context context) {
		super(context);

		ctor(context);
	}

	public EmptyView(Context context, AttributeSet attrs) {
		super(context, attrs);

		ctor(context);
	}

	public EmptyView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		ctor(context);
	}

	private void ctor(Context context) {
		String infService = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li = (LayoutInflater) context.getSystemService(
				infService);
		li.inflate(R.layout.empty_view, this, true);

		btAction = (ImageButton) findViewById(R.id.ev_btRefresh);
		tvText = (TextView) findViewById(R.id.ev_tvText);
	}

	public void showActionButton(boolean visible) {
		if (btAction != null) {
			btAction.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

	public void setText(String message) {
		if (tvText != null) {
			tvText.setText(message);
		}
	}
}
