package com.madlab.mtrade.grinfeld.roman.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import android.app.Fragment;
import android.app.FragmentManager;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.db.PromotionCheckedMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsMatrix;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsGroupMatrixAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.Datum;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsBean;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsGroupTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsOnTreeClickTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGroupMatrixTask;


public class GoodsGroupMatrixFragment extends Fragment implements ILoadGoodsComplete {

    public static final String TAG = "#GGMFragment";
    public static String EXTRA_CLIENT = "Client";
    private Client mClient;
    private ExpandableListView mTree;
    private GoodsGroupMatrixAdapter mAdapter;
    private ArrayList<GoodsInDocument> mExistedItems;
    private boolean isTablet = false;
    private byte mListViewMode = GoodsAdapter.MODE_REFERENCE;
    private static final String EXTRA_LIST_VIEW_MODE = "listViewMode";
    private byte mType;
    private ProgressBar progress_matrix;


    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.d(TAG, "onSaveInstanceState onSaveInstanceState onSaveInstanceState onSaveInstanceState onSaveInstanceState");
        setTargetFragment(getTargetFragment(), getTargetRequestCode());
    }

    public static GoodsGroupMatrixFragment newInstance(byte mType, Client client, ArrayList<GoodsInDocument> mExistedItems, byte mListViewMode, String TAG) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_CLIENT, client);
        args.putByte(EXTRA_LIST_VIEW_MODE, mListViewMode);
        args.putParcelableArrayList(GoodsInDocument.KEY, mExistedItems);
        args.putByte(GoodsTreeFragment.EXTRA_TYPE, mType);
        args.putString(MainActivity.PRICE, TAG);
        GoodsGroupMatrixFragment result = new GoodsGroupMatrixFragment();
        result.setArguments(args);
        return result;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);
        mClient = getArguments().getParcelable(EXTRA_CLIENT);
        mExistedItems = getArguments().getParcelableArrayList(GoodsInDocument.KEY);
        mListViewMode = getArguments().getByte(EXTRA_LIST_VIEW_MODE);
        mType = getArguments().getByte(GoodsTreeFragment.EXTRA_TYPE);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    View rootView;

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.goods_group_matrix, container, false);
        progress_matrix = rootView.findViewById(R.id.progress_matix);
        setHasOptionsMenu(true);
        if (container != null && container.getParent() != null) {
            ViewGroup viewGroup = (ViewGroup) container.getParent();
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View view = viewGroup.getChildAt(i);
                if (view.getId() == R.id.detailFragmentContainer) {
                    isTablet = true;
                }
            }
        }
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTree = view.findViewById(R.id.list);
        setOnClickListener(mTree);
        switch (mType) {
            case GoodsList.MODE_PROMOTIONS:
                PromotionsBean promotions = PromotionsBean.load();//loadClientExclusion(mClient.getCode());
                List<Datum> data = new ArrayList<>();
                if (promotions != null) {
                    data = promotions.getData();
                    Collections.sort(data, (datum, t1) -> datum.getTitle().compareToIgnoreCase(t1.getTitle()));
                }
                mAdapter = new GoodsGroupMatrixAdapter(getActivity(), mType, data);
                mTree.setAdapter(mAdapter);
                if (isTablet) {
                    for (Datum datum : mAdapter.getAllGroups()) {
                        datum.setChecked(false);
                    }
                    if (data.size() != 0) {
                        Datum datum = (Datum) (mAdapter.getGroup(0));
                        datum.setChecked(true);
                        datum.setRead(true);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(PromotionCheckedMetaData.FIELD_CHECK, 1);
                        MyApp app = (MyApp) getActivity().getApplicationContext();
                        SQLiteDatabase db = app.getDB();
                        db.update(PromotionCheckedMetaData.TABLE_NAME, contentValues, PromotionCheckedMetaData.FIELD_ID + "=" + datum.getId(), null);
                        mAdapter.notifyDataSetChanged();
                        Fragment old = getFragmentManager().findFragmentByTag(GoodsListFragment.TAG);
                        try {
                            if (old != null)
                                ((GoodsListFragment) old).updateItems(datum.getId());
                            else {
                                old = getFragmentManager().findFragmentById(R.id.fragmentContainer);
                                ((GoodsListFragment) old).updateItems(datum.getId());
                            }
                        } catch (Exception ignored) {

                        }
                    }
                }
                break;
            default:
                startLoadMatrixGroupTask(mClient);
        }
    }


    private void setOnClickListener(ExpandableListView tree) {
        tree.setOnGroupClickListener((expandableListView, view, i, l) -> {
            if (mType == GoodsList.MODE_PROMOTIONS) {
                GoodsGroupMatrixAdapter goodsGroupMatrixAdapter = (GoodsGroupMatrixAdapter) tree.getExpandableListAdapter();
                for (Datum datum : goodsGroupMatrixAdapter.getAllGroups()) {
                    datum.setChecked(false);
                }
                Datum datum = (Datum) (goodsGroupMatrixAdapter.getGroup(i));
                datum.setChecked(true);
                datum.setRead(true);
                ContentValues contentValues = new ContentValues();
                contentValues.put(PromotionCheckedMetaData.FIELD_CHECK, 1);
                MyApp app = (MyApp) getActivity().getApplicationContext();
                SQLiteDatabase db = app.getDB();
                db.update(PromotionCheckedMetaData.TABLE_NAME, contentValues, PromotionCheckedMetaData.FIELD_ID + "=" + datum.getId(), null);
                goodsGroupMatrixAdapter.notifyDataSetChanged();
                if (!isTablet) {
                    GoodsListFragment glf = GoodsListFragment.newInstance(
                            mListViewMode, mType, datum.getId(), mType, mClient, mExistedItems, getArguments().getString(MainActivity.PRICE));
                    FragmentManager fm = getFragmentManager();
                    fm.beginTransaction().replace(R.id.fragmentContainer, glf)
                            .addToBackStack(GoodsListFragment.TAG).commit();
                } else {
                    Fragment old = getFragmentManager().findFragmentById(R.id.detailFragmentContainer);
                    try {
                        if (old != null)
                            ((GoodsListFragment) old).updateItems(datum.getId());
                        else {
                            old = getFragmentManager().findFragmentById(R.id.fragmentContainer);
                            ((GoodsListFragment) old).updateItems(datum.getId());
                        }
                    } catch (Exception ignored) {

                    }
                }
                return true;
            } else {
                return false;
            }
        });
        tree.setOnChildClickListener((parent, view, groupPosition, childPosition, id) -> {

            if (GoodsMatrix.getSubNodes() != null) {
                Map<String, Node> m = GoodsMatrix.getSubNodes()
                        .get(groupPosition)
                        .get(childPosition);
                Node selected = m.get(Node.SUBNODE_KEY);
                selected.setSelection();
                if (!isTablet) {
                    MyApp app = (MyApp) getActivity().getApplicationContext();
                    if (GoodsList.CountInGroup(app.getDB(), selected.getCode()) > 0) {
                        // Заменяем фрагмент списка групп на список товаров
                        GoodsListFragment glf = GoodsListFragment.newInstance(
                                mListViewMode, mType, selected, mType, mClient, mExistedItems, null);
                        glf.setTargetFragment(this, getTargetRequestCode());
                        FragmentManager fm = getFragmentManager();
                        fm.beginTransaction().replace(R.id.fragmentContainer, glf, GoodsListFragment.TAG)
                                .addToBackStack(GoodsListFragment.TAG).commit();
                    }
                } else {
                    Fragment old = getFragmentManager().findFragmentById(R.id.detailFragmentContainer);
                    String codeCli = mClient != null ? mClient.getCode() : "";
                    String mlCat = mClient != null ? mClient.Category() : "";
                    try {
                        if (old != null)
                            ((GoodsListFragment) old).updateItems(selected, codeCli,
                                    mlCat, GoodsListFragment.modeRadio, mType);
                        else {
                            old = getFragmentManager().findFragmentById(R.id.fragmentContainer);
                            ((GoodsListFragment) old).updateItems(selected, codeCli,
                                    mlCat, GoodsListFragment.modeRadio, mType);
                        }

                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                }
            }
            return true;
        });
    }


    private void startLoadMatrixGroupTask(Client client) {
        if (client != null) {
            progress_matrix.setVisibility(View.VISIBLE);
            LoadGroupMatrixTask lgmt = new LoadGroupMatrixTask(getActivity(), this, mType);
            lgmt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, client.getCode());
        }
    }

    @Override
    public void onTaskComplete(LoadGoodsGroupTask task) {

    }

    @Override
    public void onTaskComplete(LoadGoodsOnTreeClickTask task) {
    }

    @Override
    public void onTaskComplete(LoadGroupMatrixTask task) {
        setListAdapter();
        progress_matrix.setVisibility(View.GONE);
    }

    private void setListAdapter() {
        if (mTree != null && GoodsMatrix.getNodes() != null && GoodsMatrix.getSubNodes() != null) {
            mAdapter = new GoodsGroupMatrixAdapter(getActivity(), GoodsMatrix.getNodes(), GoodsMatrix.getSubNodes(), mType);
            mTree.setAdapter(mAdapter);
        }
    }

}
