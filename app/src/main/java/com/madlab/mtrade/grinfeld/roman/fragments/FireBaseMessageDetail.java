package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.FireBaseNotification;

/**
 * Created by GrinfeldRA on 26.02.2018.
 */

public class FireBaseMessageDetail extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView  = inflater.inflate(R.layout.fragment_firebase_message_detail, container, false);
        TextView tvDetail = rootView.findViewById(R.id.tv_detail);
        TextView tvTitle = rootView.findViewById(R.id.tv_title);
        if (getArguments()!=null){
            FireBaseNotification notification = getArguments().getParcelable(FireBaseMessageContainer.NOTIFICATION);
            if (notification != null) {
                tvTitle.setText(notification.getTitle());
            }
            if (notification != null) {
                tvDetail.setText(notification.getMessage());
            }
        }

        return rootView;
    }
}
