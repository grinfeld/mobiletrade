package com.madlab.mtrade.grinfeld.roman.entity;



import java.util.Date;
import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;

public class ReturnItem implements Parcelable {
    public static final String KEY = "return_item";

    /**
     * Номер
     */
    private Short mNumReturn;
    private int isMercury;

    /**
     * Устанавливает принадлежность элемента возврата к документу
     *
     * @param number
     *            номер документа
     */
    public void setNum(short number) {
        mNumReturn = number;
    }

    public short getNum() {
        return mNumReturn;
    }


    public int getIsMercury() {
        return isMercury;
    }

    public void setIsMercury(int isMercury) {
        this.isMercury = isMercury;
    }

    /**
     * Код товара
     */
    private String mCodeGoods;

    public void setCodeGoods(String codeGoods) {
        mCodeGoods = codeGoods;
    }

    public String getCodeGoods() {
        return mCodeGoods;
    }

    /**
     * Название товара
     */
    private String mNameGoods;

    public String getNameGoods() {
        return mNameGoods;
    }

    public void setNameGoods(String name) {
        mNameGoods = name;
    }

    /**
     * Количество
     */
    private float mCount;

    public float getCount() {
        return mCount;
    }

    public void setCount(float cnt) {
        mCount = cnt;
        recalcAmount();
    }

    /**
     * Количество упаковок (множитель)
     */
    private int mCountPack;

    public int getCountPack() {
        return mCountPack;
    }

    public void setCountPack(int cnt) {
        mCountPack = cnt;
        recalcAmount();
    }

    /**
     * Цена
     */
    private float mPrice = 0;

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
        recalcAmount();
    }

    /**
     * вес
     */
    private float mWeight;

    public float getWeight() {
        return mWeight;
    }

    public float getTotalWeight() {
        return GlobalProc.round(mWeight * (inPack ? mCountPack * mNumInPack : mCount), 3);// ;
    }

    /**
     * Возвращает вес единицы товара
     *
     * @return вес одной штуки
     */
    public float getWeightAtPiece() {
        return mWeight;
    }

    public void setWeight(float weight) {
        mWeight = weight;
        recalcAmount();
    }

    /**
     * Итоговая сумма
     */
    private float mAmount;

    public float getAmount() {
        return mAmount;
    }

    public void setAmount(float amount) {
        mAmount = amount;
    }

    /**
     * Произвольная информация о товаре
     */
    private String mInfo;

    public String getInfo() {
        return mInfo;
    }

    public void setInfo(String info) {
        mInfo = info;
    }

    /**
     * Срок годности. НЕ ИСПОЛЬЗУЕТСЯ
     */
    // private Date mBestBefore;
    //
    // public void setBestBefore(Date bestBefore){
    // mBestBefore = bestBefore;
    // }
    //
    // public Date getBestBefore(){
    // return mBestBefore;
    // }
    //
    // public String getBestBeforeFormattedSQL(){
    // return Const.sdfSQL.format(mBestBefore);
    // }

    /**
     * Признак счета в упаковках
     */
    private boolean inPack;

    public boolean inPack() {
        return inPack;
    }

    public void inPack(boolean value) {
        inPack = value;
    }

    /**
     * Переключает штуки/упаковки
     */
    public void inPieces(boolean pieces) {
        inPack = !pieces;
        if (pieces) {
            mCount = 1;
            mCountPack = 1;
        } else {
            mCount = (int) mNumInPack;
            mCountPack = 1;
        }
        recalcAmount();
    }

    /**
     * Справочное количество в упаковке
     */
    private float mNumInPack;

    public void setNumInPack(float value) {
        mNumInPack = value;
    }

    public float numInPack() {
        return mNumInPack;
    }

    /**
     * Признак весового товара
     */
    public boolean mIsWeightGoods;

    /**
     * Конструктор
     *
     * @param numOrder
     *            номер возврата
     * @param codeGoods
     *            код товара
     * @param nameGoods
     *            название товара
     * @param count
     *            количество
     * @param countPack
     *            количество упаковок
     * @param price
     *            цена
     * @param weight
     *            вес
     * @param info
     *            информация
     * @param bestBefore
     *            срок годности
     * @param weightGoods
     *            признак весового товара
     */
    public ReturnItem(short numOrder, String codeGoods, String nameGoods,
                      float count, int countPack, float price, float weight, String info,
                      Date bestBefore, float numInPack, boolean weightGoods,
                      boolean needCalcPrice) {
        mNumReturn = numOrder;
        mCodeGoods = codeGoods;
        mNameGoods = nameGoods;

        mCount = count;
        mCountPack = countPack;
        mPrice = price;
        mWeight = weight;

        mNumInPack = numInPack;

        mInfo = info;

        mIsWeightGoods = weightGoods;

        inPack = false;

        // В противном случае мы устанавливаем
        recalcAmount();
    }

    public ReturnItem() {
        this((short) 0, "", "", 0, 1, 0, 0, "", null, 1f, false, false);
    }

    // public ReturnItem clone(){
    // return new ReturnItem(mNumReturn, mCodeGoods, mNameGoods,
    // mCount, mCountPack, mPrice, mWeight, mInfo, null, mIsWeightGoods);
    // }

    public ReturnItem(Goods goods) {
        this((short) 0, goods.getCode(), goods.getNameFull(), 1, 1, 0f, goods
                .isWeightGood() ? 1 : goods.getWeight(), "", null, goods
                .getNumInPack(), goods.isWeightGood(), false);

        //Костыль для псевдовесового товара
        if (goods.getBaseMeash().contains("кг(уп")) mIsWeightGoods = true;
    }

    /**
     * Пересчитывает итоговую стоимость позиции при изменении показателей
     */
    private void recalcAmount() {
        float tmp = 0f;
        if (mIsWeightGoods) {
            tmp = mWeight * mCount * mPrice;
        } else {
            if (inPack) {
                tmp = mCountPack * mNumInPack * mPrice;
            } else {
                tmp = mCount * mPrice;
            }
        }
        mAmount = tmp;
    }

    public void recalcPrice() {
        float count = mIsWeightGoods ? mWeight : getCountInPieces();
        mPrice = count > 1 ? mAmount / count : mAmount;
    }

    public float getCountInPieces() {
        if (mIsWeightGoods)
            return mCount;
        else
            return mCount * mCountPack;
    }

    private ReturnItem(Parcel parce) {
        mNumReturn = (short) parce.readInt();
        mCodeGoods = parce.readString();
        mNameGoods = parce.readString();

        mCount = parce.readFloat();
        mCountPack = parce.readInt();

        mNumInPack = parce.readFloat();
        mPrice = parce.readFloat();
        mWeight = parce.readFloat();
        mAmount = parce.readFloat();

        mInfo = parce.readString();

        boolean[] bb = new boolean[2];
        parce.readBooleanArray(bb);

        mIsWeightGoods = bb[0];
        inPack = bb[1];
    }

    public static final Parcelable.Creator<ReturnItem> CREATOR = new Parcelable.Creator<ReturnItem>() {

        public ReturnItem createFromParcel(Parcel in) {
            return new ReturnItem(in);
        }

        public ReturnItem[] newArray(int size) {
            return new ReturnItem[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mNumReturn);
        parcel.writeString(mCodeGoods);
        parcel.writeString(mNameGoods);

        parcel.writeFloat(mCount);
        parcel.writeInt(mCountPack);

        parcel.writeFloat(mNumInPack);
        parcel.writeFloat(mPrice);
        parcel.writeFloat(mWeight);
        parcel.writeFloat(mAmount);

        parcel.writeString(mInfo);

        parcel.writeBooleanArray(new boolean[] { mIsWeightGoods, inPack });
    }
}
