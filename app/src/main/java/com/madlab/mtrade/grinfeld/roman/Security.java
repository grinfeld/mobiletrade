package com.madlab.mtrade.grinfeld.roman;

import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

public class Security {

    public static String DEFAULT_DEVICE_CODE = "0123456789ABCDE";
    private final static String DIR = "hash.KEY_TYPE";
    public static final String HASH = "HZ1WxHh9tV0KiJrAXT9Y";

    private final static byte CODE_LENGTH = 20;

    protected static String mEtalonCode;

    private static String mDeviceCode = "";

    public static String getEtalonCode() {
        return mEtalonCode;
    }

    public static String getDeviceCode() {
        return mDeviceCode.toString();
    }

    public static void createDeviceCode(Context ctx, boolean isMain) {
        Log.d("#MainActivity", "createDeviceCode   " + isMain + " " + GlobalProc.getPref(ctx, R.string.pref_unswerCode));
        if (!isMain) {
            mDeviceCode = createInstallationCode(ctx);

            byte i = 0;
            int len = mDeviceCode.length();
            while (len < CODE_LENGTH) {
                int tmp = 0;
                char ch = mDeviceCode.charAt(i);
                try {
                    tmp = Integer.parseInt("" + ch);
                } catch (Exception e) {
                    tmp = 0;
                }
                int newChar = (tmp * i) % 10;
                mDeviceCode += newChar;
                i++;
                len = mDeviceCode.length();
            }
            mEtalonCode = generateCode(mDeviceCode);
        } else {
            mEtalonCode = GlobalProc.getPref(ctx, R.string.pref_unswerCode);
        }
    }

    private static String createInstallationCode(Context ctx) {
        String res = "";
        try {
            File installation = new File(ctx.getFilesDir(), DIR);
            try {
                if (!installation.exists()) {
                    Log.d("#MainActivity", "!installation.exists()");
                    createFile(installation);
                }
                res = readFile(installation);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return res;
    }


    private static String generateCode(String _txt) {
        String res = "";
        String tmpstr = "";
        int len = _txt.length();

        if (len < 10)
            return "";

        int tmp = 0;

        for (int i = 1; i <= 4; i++) {
            tmp = (_txt.charAt(i - 1) | i);
            res += Math.abs(tmp);
        }

        for (int i = 5; i <= 8; i++) {
            tmp = (_txt.charAt(i - 1) ^ i);
            tmpstr += Math.abs(tmp);
        }

        if (tmpstr.length() > 5) {
            int start = tmpstr.length() - 5;
            String subStr = tmpstr.substring(start, tmpstr.length());
            res += subStr;
        } else {
            res += tmpstr;
        }
        tmpstr = "";

        for (int i = 9; i <= len; i++) {
            tmp = ~(_txt.charAt(i - 1) & i);
            tmpstr += Math.abs(tmp);
        }

        int symbolNeeded = CODE_LENGTH - res.length();
        while (res.length() < CODE_LENGTH) {
            if (tmpstr.length() > symbolNeeded) {
                res += tmpstr.substring(tmpstr.length() - symbolNeeded,
                        tmpstr.length());
            } else {
                res += tmpstr;
            }
        }
        return res;
    }

    public static void deleteFile(Context ctx) {
        GlobalProc.setPref(ctx,
                R.string.pref_unswerCode, "");
        File installation = new File(ctx.getFilesDir(), DIR);
        if (installation.exists()) {
            installation.delete();
        }
    }

    public static boolean isExistFile(Context ctx) {
        File installation = new File(ctx.getFilesDir(), DIR);
        return installation.exists();
    }

    private static void createFile(File newFile) throws Exception {
        FileOutputStream out = new FileOutputStream(newFile);
        String id = null;
        String res = "";
        while (res.length() < CODE_LENGTH) {
            id = UUID.randomUUID().toString();
            res = GlobalProc.normalizePhones(id);
        }
        out.write(res.substring(0, CODE_LENGTH).getBytes());
        out.close();
    }

    private static String readFile(File newFile) {
        String res = null;
        try {
            RandomAccessFile f = new RandomAccessFile(newFile, "r");
            byte[] bytes = new byte[(int) f.length()];
            f.readFully(bytes);
            f.close();
            res = new String(bytes);
        } catch (Exception e) {
            res = null;
        }
        return res;
    }


    private static String MD5_Hash(String s) {
        final String MD5 = "MD5";
        String result = "";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            result = hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            GlobalProc.log_e("Security", e.getMessage());
        }
        return result;
    }

    public static String imageURL(String codeGoods)
    {
        String hash = String.format("%s.jpg%s", codeGoods, Security.HASH);
        String md5 = MD5_Hash(hash);
        return String.format("https://img.dalimo.ru/%s/products/%s-%s.jpg", "320x320", codeGoods, md5);
    }

}
