package com.madlab.mtrade.grinfeld.roman.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.fragments.TaskAddEditFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

/**
 * Created by GrinfeldRA on 06.03.2018.
 */

public class TaskAdapter extends ArrayAdapter<Task> {

    private ArrayList<Task> items;
    private int mSelected = -1;

    public TaskAdapter(Context context, ArrayList<Task> items) {
        super(context, R.layout.task_item, items);
        Collections.sort(items, (task, t1) -> Long.compare(t1.dateDue(), task.dateDue()));
        this.items = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.task_item, null);
        }

        if (items == null)
            return convertView;

        Task task = items.get(position);
        if (task != null) {
            LinearLayout rl = (LinearLayout)v;
            if (rl != null) {
                if (mSelected > -1 && mSelected == position) {
                    rl.setBackgroundColor(Color.DKGRAY);
                } else {
                    rl.setBackgroundColor(Color.TRANSPARENT);
                }
            }

            TextView descr = v.findViewById(R.id.taskItem_description);
            descr.setText(task.description());

//            switch (task.lastStatusIndex()) {
//                case 0:
//                    descr.setTextColor(Color.CYAN);
//                    break;
//                case 1:
//                    descr.setTextColor(Const.COLOR_GOLD);
//                    break;
//                case 2:
//                    descr.setTextColor(Color.RED);
//                    break;
//                default:
//                    break;
//            }

            TextView tvPercent =  v.findViewById(R.id.taskItem_lbPercent);
            int percent = 0;
            if (task.currentStatus() == TaskAddEditFragment.TASK_COMPLETE_INDEX){
                percent = 100;
            }
            tvPercent.setText(String.valueOf(percent));

            TextView taskItem_send = v.findViewById(R.id.taskItem_send);
            if (task.isReserved()) {
                taskItem_send.setVisibility(View.GONE);
            } else {
                taskItem_send.setVisibility(View.VISIBLE);
            }

//            byte percentColorSwitch = (byte)(task.avgPercent() < 80 ? 0 : task.avgPercent() >= 95 ? 2 : 1);
//            switch (percentColorSwitch) {
//                case 0:
//                    tvLabelPercent.setTextColor(Color.RED);
//                    break;
//                case 1:
//                    tvLabelPercent.setTextColor(Const.COLOR_GOLD);
//                    break;
//                case 2:
//                    tvLabelPercent.setTextColor(Color.GREEN);
//                    break;
//                default:
//                    break;
//            }


            TextView tvCount = v.findViewById(R.id.taskItem_lbCountGoods);
            int count = task.goodsList() == null ? 0 : task.goodsList().size();
            tvCount.setText(Integer.toString(count));
            TextView dateDue = v.findViewById(R.id.taskItem_dateDue);
            dateDue.setText(TimeFormatter.sdf1C.format(task.dateDue()));
        }
        return v;
    }

    public void setSelected(int index) {
        mSelected = index;
    }

    static class ViewHolder{

        TextView tvPercent;
        TextView tvCount;
        TextView tvLabelPercent;
        TextView descr;
    }

}
