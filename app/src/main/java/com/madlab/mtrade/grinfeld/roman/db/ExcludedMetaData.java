package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class ExcludedMetaData implements BaseColumns {
    private ExcludedMetaData() {}

    public static final String TABLE_NAME = "Excluded";

    public static final String FIELD_NAME = "Name";
    public static final String FIELD_CODE_GOODS = "CodeGoods";
    public static final String FIELD_SORT_ORDER = "SortOrder";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s (" + "%s	nvarchar(30)    NOT NULL DEFAULT '', "
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '', "
                    + "%s	smallint		NOT NULL DEFAULT 0)", TABLE_NAME,
            FIELD_NAME, FIELD_CODE_GOODS, FIELD_SORT_ORDER);

    public final static String INSERT_QUERY = String.format("INSERT INTO %s "
                    + "(%s, %s, %s) VALUES (?, ?, ?)", TABLE_NAME, FIELD_NAME,
            FIELD_CODE_GOODS, FIELD_SORT_ORDER);

    public static String insertQuery(String nameGroup, String codeGoods,
                                     byte index) {
        return String.format("INSERT INTO %s " + "(%s, %s, %s) VALUES "
                        + "('%s', '%s', %d)", TABLE_NAME, FIELD_NAME, FIELD_CODE_GOODS,
                FIELD_SORT_ORDER, nameGroup, codeGoods, index);
    }

    public static final String INDEX = String.format(
            "CREATE INDEX idxExcludedNameSortOrder ON %s ( %s, %s )",
            TABLE_NAME, FIELD_NAME, FIELD_SORT_ORDER);
}
