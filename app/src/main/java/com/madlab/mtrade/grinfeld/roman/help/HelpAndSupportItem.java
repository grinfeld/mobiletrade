package com.madlab.mtrade.grinfeld.roman.help;

import android.support.annotation.DrawableRes;

public class HelpAndSupportItem implements HelpAndSupportTypes {
    @DrawableRes
    private int drawable;
    private String title;
    private HelpFragment.Actions actions;

    public HelpAndSupportItem(@DrawableRes int drawable, String title, HelpFragment.Actions actions){
        this.drawable = drawable;
        this.title = title;
        this.actions = actions;
    }

    public int getDrawable() {
        return drawable;
    }

    public String getTitle() {
        return title;
    }

    public HelpFragment.Actions getActions() {
        return actions;
    }
}
