package com.madlab.mtrade.grinfeld.roman.db;


import java.util.Locale;

public class ManagerMetaData{
    public static final String TABLE_NAME = "Managers";

    /**
     * Код сотрудника
     */
    public static final String FIELD_CODE = "ManagerCode";
    /**
     * Ответственный менеджер
     */
    public static final String FIELD_FIO = "ManagerName";
    /**
     * Номер телефона ответственного менеджера
     */
    public static final String FIELD_PHONE = "ManagerPhone";
    /**
     * Адрес электронной почты ответственного менеджера
     */
    public static final String FIELD_EMAIL = "ManagerEmail";

    public static final byte FIELD_CODE_INDEX = 0;
    public static final byte FIELD_FIO_INDEX = 1;
    public static final byte FIELD_PHONE_INDEX = 2;
    public static final byte FIELD_EMAIL_INDEX = 3;

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)		NOT NULL CONSTRAINT PK_ManagerCode PRIMARY KEY," 	// FIELD_CODE
                    + "%s	nvarchar(25)	NOT NULL DEFAULT ''," 	// FIELD_FIO
                    + "%s	nvarchar(25)	NOT NULL DEFAULT ''," 	// FIELD_PHONE
                    + "%s	nvarchar(20)	NOT NULL DEFAULT '')", 	// FIELD_EMAIL
            TABLE_NAME,
            FIELD_CODE, FIELD_FIO, FIELD_PHONE, FIELD_EMAIL);

    public static String insertQuery(String code, String fio, String phone, String email) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s) "
                        + "VALUES "
                        + "('%s', '%s', '%s', '%s')\n",
                TABLE_NAME,
                FIELD_CODE, FIELD_FIO, FIELD_PHONE, FIELD_EMAIL,
                code, fio, phone, email);
    }

}
