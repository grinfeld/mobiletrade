package com.madlab.mtrade.grinfeld.roman.entity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.db.PostMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grinfeldra
 */
public class Post {

    private static final String TAG = "#Post";
    private int id;
    private String name;

    public Post(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static List<Post> getPosts(SQLiteDatabase db) {
        List<Post> res = new ArrayList<>();
        Cursor rows = null;
        try {
            rows = db.rawQuery("Select * from " + PostMetaData.TABLE_NAME, null);
            while (rows.moveToNext()) {
                int id = rows.getInt(PostMetaData.FIELD_ID.Index);
                String name = rows.getString(PostMetaData.FIELD_VALUE.Index);
                res.add(new Post(id, name));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }


    public static Post getPostWhereName(SQLiteDatabase db, String whereName) {
        Post res = null;
        Cursor rows = null;
        try {
            rows = db.rawQuery("Select * from " + PostMetaData.TABLE_NAME + " where " + PostMetaData.FIELD_VALUE.Name + " = ?", new String[]{whereName});
            while (rows.moveToNext()) {
                int id = rows.getInt(PostMetaData.FIELD_ID.Index);
                String name = rows.getString(PostMetaData.FIELD_VALUE.Index);
                res = new Post(id, name);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

}