package com.madlab.mtrade.grinfeld.roman.entity.promotions;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.PromotionBonusesMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionItemMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionProductMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA
 */

public class PromotionItem {

    private static final String TAG = "#PromotionItem";
    private int idPromotion;
    private String title;
    private String text;
    private String cover;
    private String quantum;
    private String startDate;
    private String endDate;
    private int amount_for_bonus;

    private List<Bonuse> bonuses;
    private List<String> originCodeGoods;

    public PromotionItem(int idPromotion, String title, String text, String cover, String quantum, String startDate, String endDate, int amount_for_bonus, List<Bonuse> bonuses, List<String> originCodeGoods) {
        this.idPromotion = idPromotion;
        this.title = title;
        this.text = text;
        this.cover = cover;
        this.quantum = quantum;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amount_for_bonus = amount_for_bonus;
        this.bonuses = bonuses;
        this.originCodeGoods = originCodeGoods;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getAmount_for_bonus() {
        return amount_for_bonus;
    }

    public void setAmount_for_bonus(int amount_for_bonus) {
        this.amount_for_bonus = amount_for_bonus;
    }

    public List<Bonuse> getBonuses() {
        return bonuses;
    }

    public void setBonuses(List<Bonuse> bonuses) {
        this.bonuses = bonuses;
    }

    public List<String> getOriginCodeGoods() {
        return originCodeGoods;
    }

    public void setOriginCodeGoods(List<String> originCodeGoods) {
        this.originCodeGoods = originCodeGoods;
    }

    public static PromotionItem load(int idPromotion) {
        MyApp app = (MyApp) MyApp.getContext();
        return load(app.getDB(),idPromotion);
    }


    private static PromotionItem load(SQLiteDatabase db, int idPromotion) {
        PromotionItem res = null;
        List<String> originCodeGoods = new ArrayList<>();
        List<Bonuse> bonuseList = new ArrayList<>();
        Cursor rows = null;
        try {
            rows = db.query(PromotionProductMetaData.TABLE_NAME, null, PromotionProductMetaData.FIELD_ID_PROMOTION+" = "+idPromotion, null, null, null, null);
            while (rows.moveToNext()){
                String origiId = rows.getString(0);
                originCodeGoods.add(origiId);
            }
            rows = db.query(PromotionBonusesMetaData.TABLE_NAME, null, PromotionBonusesMetaData.FIELD_ID_PROMOTION+" = "+idPromotion, null, null, null, null);
            while (rows.moveToNext()){
                String origin_id = rows.getString(0);
                String quantum = rows.getString(1);
                int count = rows.getInt(2);
                bonuseList.add(new Bonuse(origin_id,quantum,count));
            }
            rows = db.query(PromotionItemMetaData.TABLE_NAME, null, PromotionItemMetaData.FIELD_ID+" = "+idPromotion, null, null, null, null);
            rows.moveToFirst();
            int id = rows.getInt(0);
            String title = rows.getString(1);
            String text = rows.getString(2);
            String cover = rows.getString(3);
            String quantum = rows.getString(4);
            String startDate = rows.getString(5);
            String endDate = rows.getString(6);
            int amount_for_bonus = rows.getInt(7);
            res = new PromotionItem(id, title, text, cover, quantum, startDate, endDate, amount_for_bonus, bonuseList, originCodeGoods);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }



}