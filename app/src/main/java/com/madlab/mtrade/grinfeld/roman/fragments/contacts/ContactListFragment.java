package com.madlab.mtrade.grinfeld.roman.fragments.contacts;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;
import com.madlab.mtrade.grinfeld.roman.entity.Post;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by grinfeldra
 */
public class ContactListFragment extends ListFragment implements IOnClickItem {

    private Context context;
    private ListView listView;
    private ContactAdapter mClientAdapter;
    private String codeClient;
    private TextView txtWarning;
    private List<Contact> contacts;

    public static ContactListFragment newInstance(String codeClient) {
        Bundle args = new Bundle();
        args.putString(Client.KEY, codeClient);
        ContactListFragment fragment = new ContactListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.new_contact, menu);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        context = getActivity();
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        return inflater.inflate(R.layout.contact_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        codeClient = getArguments().getString(Client.KEY);
        contacts = loadContactList(codeClient);
        txtWarning = view.findViewById(R.id.txt_update_contact);
        if (contacts.size() == 0){
            txtWarning.setVisibility(View.VISIBLE);
            txtWarning.setText(getString(R.string.warning_mes_update_contact2));
        }else if (isNotMainContactFace(contacts)){
            txtWarning.setVisibility(View.VISIBLE);
            txtWarning.setText(getString(R.string.warning_mes_update_contact3));
        }else if (isLastUpdateSixMonthsAgo()) {
            txtWarning.setVisibility(View.VISIBLE);
            txtWarning.setText(getString(R.string.warning_mes_update_contact1));
        }
        listView = view.findViewById(android.R.id.list);
        listView.setEmptyView(view.findViewById(R.id.emptyElement));
        mClientAdapter = new ContactAdapter(context, contacts, this);
        listView.setAdapter(mClientAdapter);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        MyApp app = (MyApp) MyApp.getContext();
        SQLiteDatabase db = app.getDB();
        String region = App.getRegion(context);
        String codeAgent = App.get(context).getCodeAgent();
        StringBuilder query = new StringBuilder();
        for (Contact contact : contacts) {
            Post post = Post.getPostWhereName(db, contact.getPost());
            int postId = 0;
            if (post != null) {
                postId = post.getId();
            }
            query.append(String.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;\n", codeAgent,
                    contact.getCodeClient(), postId, contact.getFio(), contact.getTel(), contact.getEmail(),
                    contact.getResponsible(), contact.getDateBirth(), contact.getNote()));
        }
        if (!query.toString().isEmpty()) {
            String substring = query.toString().substring(0, query.lastIndexOf("\n"));
            OkHttpClientBuilder.buildCall("ПринятьФайлСКонтактами", substring, region).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        ResponseBody responseBody = response.body();
                        if (responseBody != null) {
                            String string1 = responseBody.string();
                            if (string1.contains("OK<EOF>")) {
                                for (Contact contact : contacts) {
                                    Contact.updateReservedContact(db, contact.getId());
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onClickDel(Contact contact) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage("Удалить контакт: " + contact.getFio())
                .setPositiveButton("OK", (dialog, which) -> {
                    MyApp app = (MyApp) getActivity().getApplication();
                    SQLiteDatabase db = app.getDB();
                    if (Contact.deleteContact(db, contact.getId())) {
                        List<Contact> contacts = loadContactList(codeClient);
                        mClientAdapter = new ContactAdapter(context, contacts, this);
                        listView.setAdapter(mClientAdapter);
                        if (contacts.size() == 0){
                            txtWarning.setVisibility(View.VISIBLE);
                            txtWarning.setText(getString(R.string.warning_mes_update_contact2));
                        }else if (isNotMainContactFace(contacts)){
                            txtWarning.setVisibility(View.VISIBLE);
                            txtWarning.setText(getString(R.string.warning_mes_update_contact3));
                        }else if (isLastUpdateSixMonthsAgo()) {
                            txtWarning.setVisibility(View.VISIBLE);
                            txtWarning.setText(getString(R.string.warning_mes_update_contact1));
                        }
                    }
                })
                .setNegativeButton("Отмена", null);
        alertDialog.create();
        alertDialog.show();
    }

    public boolean isNotActualData(){
        List<Contact> contacts = loadContactList(codeClient);
        if (contacts.size() == 0){
           return true;
        }else if (isNotMainContactFace(contacts)){
            return true;
        }else return isLastUpdateSixMonthsAgo();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener((v, keyCode, event) -> {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (isNotActualData()) {
                        showDialog();
                    } else {
                        getActivity().onBackPressed();
                    }
                    return true;
                }
                return false;
            });
        }
    }



    private boolean isNotMainContactFace(List<Contact> contacts) {
        for (Contact contact : contacts) {
            if (contact.getResponsible() == 1) {
                return false;
            }
        }
        return true;
    }

    private boolean isLastUpdateSixMonthsAgo(){
        String date = Client.getLastUpdateContact(getActivity(), codeClient);
        try {
            if (date.length() == "27.01.20".length()) {
                date = date.substring(0, 6).concat("20").concat(date.substring(6, 8));
            }
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            Date lastModify = format.parse(date);
            Date referenceDate = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(referenceDate);
            c.add(Calendar.MONTH, - 6);
            Date timeSixMonthsAgo = c.getTime();
            if (lastModify.getTime() <= timeSixMonthsAgo.getTime()) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void showDialog() {
        AlertDialog alertDialog = new
                AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.confirmation))
                .setMessage("Актуализируйте сведения")
                .setPositiveButton("OK", null)
                .create();
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }
    }

    private void startContactEditorFragment(Contact contacts) {
        getFragmentManager().beginTransaction()
                .replace(R.id.contentMain, ContactEditorFragment.newInstance(contacts, codeClient))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.confirmation))
                    .setMessage("Добавить новый контакт")
                    .setPositiveButton("OK", (dialog, which) -> {
                        startContactEditorFragment(null);
                    })
                    .setNegativeButton("Отмена", null);
            alertDialog.create();
            alertDialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Contact> loadContactList(String codeClient) {
        List<Contact> mFullClientList = new ArrayList<>();
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            mFullClientList = Contact.loadContacts(db, codeClient, App.get(getActivity()).getCodeAgent());
        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
        return mFullClientList;
    }



    @Override
    public void onClick(Contact client) {
        startContactEditorFragment(client);
    }

}
