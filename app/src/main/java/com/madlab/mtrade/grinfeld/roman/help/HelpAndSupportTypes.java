package com.madlab.mtrade.grinfeld.roman.help;

public interface HelpAndSupportTypes {
    int HEADER = 0;
    int ITEM = 1;
}
