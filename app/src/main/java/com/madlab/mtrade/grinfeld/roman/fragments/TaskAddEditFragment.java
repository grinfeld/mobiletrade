package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.TaskGoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveTaskComplete;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveTask;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

/**
 * Created by GrinfeldRA on 06.03.2018.
 */

public class TaskAddEditFragment extends ListFragment {

    private static final String TAG = "!->TaskAddEditFragment";
    public static final String EXTRA_TASK = "extraTask";
    public static final String EXTRA_READONLY = "viewonly";
    public static final byte TASK_COMPLETE_INDEX = 3;

    private Task mTask;
    private boolean mViewOnly;

    private EditText etDescription;
    //	private TextView tvDateDue;
    private CheckBox cbDone;
    private Spinner mSpinnerStatuses;
    private boolean wasChanged;
//	private EmptyView mEmptyView;


    private String[] mStatuses;
    private Context context;

    private TextView txt_stat;


    public static TaskAddEditFragment newInstance(Task selected, boolean readonly) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_TASK, selected);
        args.putInt(EXTRA_READONLY, readonly ? 1 : 0);
        TaskAddEditFragment fragment = new TaskAddEditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRetainInstance(true);

        mTask = getArguments().getParcelable(EXTRA_TASK);
        mViewOnly = getArguments().getInt(EXTRA_READONLY) == 1;

        mStatuses = getResources().getStringArray(R.array.task_statuses);

        context = getActivity();

        if (savedInstanceState == null) {
            if (mTask != null) {
                //getActivity().getActionBar().setSubtitle(String.format(Locale.ENGLISH, "Ñðîê äî: %s", TimeFormatter.sdf1C.format(mTask.dateDue())));
            }
        } else {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.task_new_edit, container, false);
        etDescription = v.findViewById(R.id.task_new_edit_etDescription);
        if (App.isMerch()) {
            setHasOptionsMenu(true);
        }
        return v;
    }


    public void onTaskUpdated() {
        if (!App.isMerch()) {
            if (wasChanged) {
                if (mTask.currentStatus() == TASK_COMPLETE_INDEX) {
                    mTask.avgPercent((byte) 100);
                }
                TaskLab.get(getActivity()).updateTask(mTask);
                EventBus.getDefault().post(mTask);
            }
        }else {
            EventBus.getDefault().post(mTask);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        if (!App.isMerch()){
                            onTaskUpdated();
                        }
                        getFragmentManager().popBackStack();
                        return true;
                    }
                    return false;
                }
            });
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        fillForm();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cbDone = view.findViewById(R.id.task_new_edit_cbDone);
        txt_stat = view.findViewById(R.id.txt_stat);
        cbDone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                wasChanged = true;
                if (arg1) {
                    mTask.changeStatus(TASK_COMPLETE_INDEX);
                    mSpinnerStatuses.setEnabled(false);
                } else {
                    mSpinnerStatuses.setEnabled(true);
                    mTask.changeStatus((byte) mSpinnerStatuses.getSelectedItemPosition());
                }
            }
        });
        setupStatusList();

        TextView evText = view.findViewById(R.id.ev_tvText);
        if (evText != null) {
            evText.setText("Нет товаров");
        }

        TaskGoodsAdapter adapter = new TaskGoodsAdapter(getActivity(), mTask != null ? mTask.goodsList() : null);
        Log.d(TAG, String.valueOf(adapter.getCount()));
        setListAdapter(adapter);
        if (App.isMerch()) {
            cbDone.setEnabled(false);
            mSpinnerStatuses.setVisibility(View.GONE);
            txt_stat.setVisibility(View.VISIBLE);
            txt_stat.setText(mStatuses[mTask.currentStatus()]);
        } else {
            if (adapter.getCount() > 0 || mTask.currentStatus() == TASK_COMPLETE_INDEX) {
                mSpinnerStatuses.setVisibility(View.GONE);
                cbDone.setVisibility(View.GONE);
                setHasOptionsMenu(false);
                txt_stat.setVisibility(View.VISIBLE);
                try {
                    txt_stat.setText(mStatuses[mTask.currentStatus()]);
                } catch (Exception e) {
                }
            } else {
                txt_stat.setVisibility(View.GONE);
                mSpinnerStatuses.setVisibility(View.VISIBLE);
                cbDone.setVisibility(View.VISIBLE);
                setHasOptionsMenu(true);
            }
        }
    }

    private void setupStatusList() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context, R.layout.item_spiner,
                mStatuses);
        mSpinnerStatuses = getView()
                .findViewById(R.id.task_new_edit_spinnerManagers);
        mSpinnerStatuses.setAdapter(adapter);
        mSpinnerStatuses.setTag(mStatuses);
        Log.d(TAG, String.valueOf(mTask.currentStatus()));
        mSpinnerStatuses.setSelection(mTask.currentStatus());
        mSpinnerStatuses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                if (position != mTask.lastStatusIndex()) {
                    Log.d(TAG, String.valueOf(position));
                    mTask.changeStatus((byte) position);
                    wasChanged = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void fillForm() {
        if (mTask == null)
            return;
        etDescription.setText(mTask.description());
        cbDone.setChecked(mTask.lastStatusIndex() == mStatuses.length - 1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (App.isMerch()) {
            if (mTask != null && !mTask.isReserved()) {
                inflater.inflate(R.menu.task_add_edit, menu);
            }
        } else {
            if (!mViewOnly)
                inflater.inflate(R.menu.task_add_edit, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_task_add_edit_send:
                onTaskUpdated();
                getFragmentManager().popBackStack();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}

