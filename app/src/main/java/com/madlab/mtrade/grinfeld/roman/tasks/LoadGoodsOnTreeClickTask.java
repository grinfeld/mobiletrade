package com.madlab.mtrade.grinfeld.roman.tasks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.MenuItem;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DiscountsMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Remainder;
import com.madlab.mtrade.grinfeld.roman.fragments.GoodsListFragment;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;

public class LoadGoodsOnTreeClickTask extends
        AsyncTask<String, Integer, Boolean> {
    private final static String TAG = "!->LoadGoodsOnTreeClickTask";
    public final static byte LOAD_COMPLETE = 10;
    public final static byte LOAD_FAIL = 11;

    // private ProgressDialog mProgress;

    private Context mBaseContext;
    private byte mMode = GoodsList.MODE_ALL;
    private String mUnswer;

    /**
     * 0 - Все, 1 - с остатками, 2 - СТМ
     */
    private byte filter;

    /**
     * Код клиента
     */
    private String mClientCode;

    /**
     * Категория Маст-листа, зависит от клиента
     */
    private String mMLCategory;
    private GoodsList mGoodsList;
    private ProgressDialog mProgress;
    private ArrayList<GoodsInDocument> mExistedItems;
    private byte mPriceLvl;
    private byte mDiscount;
    private List<GoodsListFragment.MenuItemObject> menuItemObjects;

    private ILoadGoodsComplete mTaskCompleteListener;

    public void setClientCode(String code) {
        mClientCode = code;
    }

    /**
     * Устанавливаем категорию маст-листа
     *
     * @param cat
     */
    public void setMLCategory(String cat) {
        mMLCategory = cat;
    }

    public String getMessage() {
        return mUnswer;
    }

    public LoadGoodsOnTreeClickTask(Context baseContext,
                                    ArrayList<GoodsInDocument> existedItems,
                                    ILoadGoodsComplete taskCompleteListener,
                                    byte mode, byte filter,
                                    byte priceLvl, byte discount, List<GoodsListFragment.MenuItemObject> menuItemObjects) {
        mBaseContext = baseContext;
        mTaskCompleteListener = taskCompleteListener;
        mMode = mode;
        this.filter = filter;
        mExistedItems = existedItems;
        mPriceLvl = priceLvl;
        mDiscount = discount;
        this.menuItemObjects = menuItemObjects;
    }

    @Override
    protected void onPreExecute() {
        mProgress = new ProgressDialog(mBaseContext);
        mProgress.setCancelable(false);
        mProgress.setMessage(mBaseContext.getText(R.string.mes_wait_for_load));
        mProgress.show();
    }

    @SuppressLint("LongLogTag")
    @Override
    protected Boolean doInBackground(String... params) {
        String parentCode = params != null ? params[0] : App.get(mBaseContext).getRootCode();
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) mBaseContext.getApplicationContext();
            db = app.getDB();
            switch (mMode) {
                case GoodsList.MODE_ALL:
                    switch (filter) {
                        case 0:
                            mGoodsList = GoodsList.loadChild(db, parentCode);
                            break;
                        case 1:
                            mGoodsList = GoodsList.loadChildWithRest(db, parentCode);
                            break;
                        case 2:
                            mGoodsList = GoodsList.loadChildSTM(db, parentCode);
                            break;
                        default:
                            mGoodsList = GoodsList.loadChild(db, parentCode);
                    }
                    break;
                case GoodsList.MODE_MATRIX:
                    String sql = filter == 1 ? DBHelper.MATRIX_QUERY_GROUP_REST : DBHelper.MATRIX_QUERY_GROUP;
                    mGoodsList = GoodsList.loadMatrix(db, sql, new String[]{parentCode, mClientCode});
                    break;
                case GoodsList.MODE_STM:
                    mGoodsList = GoodsList.loadChildSTM(db, parentCode);
                    break;
                case GoodsList.MODE_MUST_LIST:
                    ArrayList<String> ml = GoodsList.loadMustList(db, App.get(mBaseContext).getCodeAgent(), mMLCategory, null);
                    if (ml != null && ml.size() > 0) {
                        GoodsList goodsList = new GoodsList();
                        for (String code : ml) {
                            Goods load = Goods.load(db, code);
                            goodsList.add(load);
                        }
                        mGoodsList = goodsList;
                    }
                    break;
                case GoodsList.MODE_PROMOTIONS:

                    break;
                default:
                    break;
            }

            if (mGoodsList != null) {
                if (mMode != GoodsList.MODE_MATRIX) {
                    ArrayList<Discount> discountList = getDiscountedGoods(db, App.get(mBaseContext).getCodeAgent(), mClientCode);
                    for (Goods goods : mGoodsList) {
                        Discount forGoods = getDiscountForGoods(discountList, goods.getCode());
                        if (forGoods != null) {
                            goods.calcVisiblePrice(mPriceLvl, forGoods.discount());
                        } else
                            goods.calcVisiblePrice(mPriceLvl, mDiscount);
                    }
                }
                ArrayList<String> ml = GoodsList.loadMustList(db, App.get(mBaseContext).getCodeAgent(),
                        mMLCategory, parentCode);
                if (ml != null && ml.size() > 0) {
                    for (String code : ml) {
                        for (Goods goods : mGoodsList) {
                            if (goods.getCode().equalsIgnoreCase(code)) {
                                goods.setHighLight((byte) 1);
                                break;
                            }
                        }
                    }
                }
                if (mExistedItems != null) {
                    for (GoodsInDocument item : mExistedItems) {
                        for (Goods goods : mGoodsList) {
                            if (item.getCode().equalsIgnoreCase(goods.getCode())) {
                                goods.inDocument(true);
                                break;
                            }
                        }
                    }
                }
                if (menuItemObjects != null && menuItemObjects.size() > 0) {
                    /**
                     это фильтр для складов
                     на множестве складов может быть один и тот же код товара
                     * */
                    Set<String> collectionSetUnchecked = new LinkedHashSet<>();
                    Set<String> collectionSetChecked = new LinkedHashSet<>();
                    for (GoodsListFragment.MenuItemObject itemObject : menuItemObjects) {
                        String scladName = itemObject.getScladName();
                        if (itemObject.isChecked()) {
                            List<String> listCodeGoodsToSclad = Remainder.loadCodeGoodsToSclad(mBaseContext, scladName);
                            if (listCodeGoodsToSclad != null) {
                                collectionSetChecked.addAll(listCodeGoodsToSclad);
                            }
                        } else {
                            List<String> listCodeGoodsToSclad = Remainder.loadCodeGoodsToSclad(mBaseContext, scladName);
                            if (listCodeGoodsToSclad != null) {
                                collectionSetUnchecked.addAll(listCodeGoodsToSclad);
                            }
                        }
                    }
                    Iterator<String> iteratorUnchecked = collectionSetUnchecked.iterator();
                    while (iteratorUnchecked.hasNext()) {
                        String codeUnchecked = iteratorUnchecked.next();
                        for (String codeChecked : collectionSetChecked) {
                            if (codeUnchecked.equals(codeChecked)) {
                                iteratorUnchecked.remove();
                                break;
                            }
                        }
                    }
                    Iterator<Goods> goodsIterator = mGoodsList.iterator();
                    while (goodsIterator.hasNext()) {
                        Goods goods = goodsIterator.next();
                        for (String codeUnchecked : collectionSetUnchecked) {
                            if (goods.getCode().equals(codeUnchecked)) {
                                goodsIterator.remove();
                                break;
                            }
                        }
                    }
                    List<Remainder> remainder;
                    if (collectionSetUnchecked.size() > 0)
                        for (Goods goods : mGoodsList) {
                            remainder = Remainder.load(mBaseContext, goods.getCode(), menuItemObjects);
                            if (remainder != null) {
                                Float totalRemainder = 0.0f;
                                for (Remainder r : remainder) {
                                    if (!r.getRemainder().isEmpty())
                                        totalRemainder = totalRemainder + Float.valueOf(r.getRemainder());
                                }
                                goods.setRestOnStore(totalRemainder);
                            }
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            mUnswer = e.toString();
            return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mTaskCompleteListener != null)
            mProgress.dismiss();
        mTaskCompleteListener.onTaskComplete(this);
    }

    public GoodsList getList() {
        return mGoodsList;
    }

    @SuppressLint("LongLogTag")
    private ArrayList<Discount> getDiscountedGoods(SQLiteDatabase db, String agent, String client) {
        ArrayList<Discount> result = new ArrayList<Discount>();

        //Загружаем список товаров с ценами
        String sql = String.format("SELECT * FROM %s WHERE %s = 0 AND %s = ? AND %s = ?",
                DiscountsMetaData.TABLE_NAME,
                DiscountsMetaData.FIELD_IS_GROUP,
                DiscountsMetaData.FIELD_CODE_AGENT,
                DiscountsMetaData.FIELD_CODE_CLIENT);
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, new String[]{agent, client});

            int columnCodeGoods = rows.getColumnIndex(DiscountsMetaData.FIELD_CODE_GOODS);
            int columnDiscount = rows.getColumnIndex(DiscountsMetaData.FIELD_DISCOUNT);
            while (rows.moveToNext()) {
                Discount item = new Discount(rows.getString(columnCodeGoods), rows.getFloat(columnDiscount));
                result.add(item);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) rows.close();
        }

        return result;
    }

    private Discount getDiscountForGoods(ArrayList<Discount> list, String goods) {
        for (Discount discount : list) {
            if (discount.goods().equalsIgnoreCase(goods)) return discount;
        }
        return null;
    }

    private class Discount {

        private String mCode;
        private float mDiscount;

        public String goods() {
            return mCode;
        }

        public float discount() {
            return mDiscount;
        }

        public Discount(String goods, float discount) {
            mCode = goods;
            mDiscount = discount;
        }
    }
}
