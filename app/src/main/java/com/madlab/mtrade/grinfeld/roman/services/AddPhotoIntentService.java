package com.madlab.mtrade.grinfeld.roman.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

/**
 * Created by GrinfeldRA
 */

public class AddPhotoIntentService extends IntentService {

    public static final String EXTRA_STATUS = "TaskStatus";
    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.addphoto";

    public AddPhotoIntentService() {
        super(BROADCAST_ACTION);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, STATUS_OK);
        sendBroadcast(result);
    }
}
