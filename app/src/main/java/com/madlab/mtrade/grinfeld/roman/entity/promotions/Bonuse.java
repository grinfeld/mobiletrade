package com.madlab.mtrade.grinfeld.roman.entity.promotions;

/**
 * Created by grinfeldra
 */
public class Bonuse {
    private String origin_id;
    private String quantum;
    private int count;


    public Bonuse(String origin_id, String quantum, int count) {
        this.origin_id = origin_id;
        this.quantum = quantum;
        this.count = count;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
    }

    public String getQuantum() {
        return quantum;
    }

    public void setQuantum(String quantum) {
        this.quantum = quantum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
