package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.components.WheelView;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.wheel.OnWheelChangedListener;
import com.madlab.mtrade.grinfeld.roman.wheel.adapters.ArrayWheelAdapter;

import java.util.Locale;

import static com.madlab.mtrade.grinfeld.roman.fragments.ReturnGoodsFragment.KEY_RETURN_FOREIGN;

/**
 * Created by GrinfeldRA on 23.11.2017.
 */

public class ReturnQuantityFragment extends Fragment implements View.OnClickListener {


    private int discount = 0;
    private static final String TAG = "#ReturnQuantityFragment";
    private final static byte TEXT_SIZE = 18;
    private final static byte VISIBLE_ITEMS = 3;
    private EditText etPrice,etCount,etAmount;
    private WheelView wvDiscount;
    private OnWheelChangedListener mWheelListener;
    private Goods goodsData;
    private ReturnItem returnItem;
    private TextView tvAmount,tv_goodsName,tv_numInPack,tv_weight,
            tv_quant,tv_priceStandart,tv_rest,tv_message,rq_btCancel, rq_btOK;
    WheelView wheelMeash;
    private boolean isForeign = false;

    /**
     * True - в штуках, False - упаковки
     */
    private boolean mDefaultValueInPieces;
    private float standartPrice;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_return_quantity,container,false);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("Добавление товара");
            supportActionBar.setTitle("Новый возврат");
        }
        setHasOptionsMenu(true);
        etCount = rootView.findViewById(R.id.rq_etCount);
        etCount.addTextChangedListener(mEditCountListener);
        etPrice = rootView.findViewById(R.id.rq_etPrice);
        etPrice.addTextChangedListener(mEditPriceListener);
        etAmount = rootView.findViewById(R.id.rq_etAmount);
        tvAmount = rootView.findViewById(R.id.rq_tvTotalSum);
        etAmount.addTextChangedListener(mEditAmountListener);
        wvDiscount = rootView.findViewById(R.id.rq_wheelDiscount);
        tv_numInPack = rootView.findViewById(R.id.rq_tvNumInPack);
        tv_weight = rootView.findViewById(R.id.rq_tvWeight);
        tv_quant = rootView.findViewById(R.id.rq_tvQuant);
        tv_priceStandart = rootView.findViewById(R.id.rq_tvPriceStandart);
        tv_rest = rootView.findViewById(R.id.rq_tvRest);
        tv_message = rootView.findViewById(R.id.rq_tvMessage);
        tv_goodsName = rootView.findViewById(R.id.rq_tvNameGoods);
        wheelMeash = rootView.findViewById(R.id.rq_wheelMeash);
        rq_btCancel = rootView.findViewById(R.id.rq_btCancel);
        rq_btOK = rootView.findViewById(R.id.rq_btOK);
        mWheelListener = new MyOnWheelChangedListener();
        rq_btCancel.setOnClickListener(this);
        rq_btOK.setOnClickListener(this);
        mDefaultValueInPieces = App.get(getActivity()).getMeash().equalsIgnoreCase(getString(R.string.pieces));

        if (savedInstanceState == null) {
            returnItem = getArguments().getParcelable(ReturnItem.KEY);
            goodsData = getArguments().getParcelable(Goods.KEY);
            isForeign = getArguments().getBoolean(KEY_RETURN_FOREIGN);
            if (returnItem != null && goodsData == null) {
                goodsData = loadGoodsFromDB(returnItem.getCodeGoods());
            }else if (returnItem == null && goodsData != null) {
                returnItem = new ReturnItem(goodsData);
            }

            standartPrice = goodsData.getPriceStandart();
            //Заполняем колесо скидкой от -100 до 100 и ставим в 0.
            fillWheelDiscount(0);
            updateData(false);
        }
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GlobalProc.closeKeyboard(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    /**
     * updateData обновляем данные в возврате
     *
     * @param direction
     *            TRUE - в возврат, FALSE - из возврата на форму
     */
    private void updateData(boolean direction) {
        try {
            if (direction) {
//                float newValue;
//                try {
//                    newValue = Float.parseFloat(etCount.getText().toString());
//                } catch (Exception e) {
//                    Log.e(TAG, e.toString());
//                    newValue = 0f;
//                }
//
//                int newValueInt = (int) newValue;
//                if (returnItem.mIsWeightGoods) {
//                    returnItem.setWeight(newValue);
//                } else {
//                    if (returnItem.inPack()) {
//                        returnItem.setCountPack(newValueInt);
//                    } else {
//                        returnItem.setCount(newValueInt);
//                    }
//                }
            } else {
                // заполняем форму данными
                if (goodsData != null) {
                    tv_message.setVisibility(goodsData.isWeightGood() ? View.VISIBLE : View.INVISIBLE);
                    tv_goodsName.setText(goodsData.getNameFull());
                    tv_numInPack.setText(Float.toString(goodsData
                            .getNumInPack()));
                    tv_weight.setText(GlobalProc.formatWeight(goodsData.getWeight()));
                    tv_quant.setText(Float.toString(goodsData.getQuant()));

                    tv_priceStandart.setText(GlobalProc.toForeignPrice(goodsData.getPriceStandart(), isForeign));
                    tv_rest.setText(Float.toString(goodsData.getRestOnStore()));

                    // isPieces = !goodsData.isWeightGood();
                }

                if (returnItem != null) {
                    // Заполняем ед. измерения
                    // Здесь, потому что нам нужно знать в штуках товар или в
                    // упаковках
                    fillWheelMeash(goodsData, returnItem, mDefaultValueInPieces);
                    discount = recalcDiscount();
                    // Если товар весовой - пишем вес
                    // Если штуки - количество
                    // Если упаковки - умножаем на количество в упаковке
                    String txt;
                    if (returnItem.mIsWeightGoods || goodsData.isFractional()) {
                        txt = GlobalProc.formatWeight(returnItem.getWeight());
                    } else {
                        if (!returnItem.inPack()) {
                            txt = GlobalProc.formatFloat("%.1f", returnItem.getCount());
                        } else {
                            txt = Integer.toString(returnItem.getCountPack());
                        }
                    }
                    etCount.setText(txt);

                    printDiscount();
                    printPrice();
                    printAmount();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void fillWheelMeash(Goods goods, ReturnItem returnItem, boolean defaultValue) {

        if (goods != null && wheelMeash != null) {
            String list[];
            boolean isWeight = goods.isWeightGood();
            //Костыль для псевдовесового товара
            if (goods.getBaseMeash().contains("кг(уп")) isWeight = true;

            if (!isWeight) {
                list = new String[] { goods.getBaseMeash(),
                        goods.getPackMeash() };
            } else {
                list = new String[] { "кг." };
                wheelMeash.setEnabled(false);
            }
            ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<>(getActivity(), list);
            awa.setTextSize(TEXT_SIZE);
            wheelMeash.setViewAdapter(awa);
            wheelMeash.setVisibleItems(VISIBLE_ITEMS);
            if (returnItem != null) {
                returnItem.mIsWeightGoods = isWeight;
                wheelMeash.setCurrentItem(returnItem.inPack() ? 1 : defaultValue ? 0 : 1);
            }else{
                wheelMeash.setCurrentItem(defaultValue ? 0 : 1);
            }
            // Слушатель изменения
            wheelMeash.addChangingListener(mWheelListener);
        }
    }

    private void fillWheelDiscount(int position) {
        if (wvDiscount != null) {
            String[] list = new String[201];

            for (int i = -100; i < 101; i++){
                list[i+100] = Integer.toString(i)+"%";
            }
            ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<>(getActivity(), list);
            awa.setTextSize(TEXT_SIZE);
            wvDiscount.setViewAdapter(awa);

            wvDiscount.setVisibleItems(VISIBLE_ITEMS);
            wvDiscount.setCurrentItem(position + 100);

            // Слушатель изменения
            wvDiscount.addChangingListener(mWheelListener);
        }
    }


    private int recalcDiscount() {
        int result = 0;
        if (returnItem != null) {
            float currentPrice = returnItem.getPrice();

            float delta = 0;

            if (standartPrice != 0) {
                if (currentPrice >= standartPrice) {
                    delta = currentPrice - standartPrice;
                    delta *= 1000;
                    delta /= standartPrice;
                } else {
                    delta = standartPrice - currentPrice;
                    delta *= 1000;
                    delta /= standartPrice;
                    delta = 0 - delta;
                }
            }
            result = (int)Math.floor(delta / 10);
        }

        return result;
    }

    private Goods loadGoodsFromDB(String codeGoods) {
        Goods result = null;

        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            result = Goods.load(db, codeGoods);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return result;
    }

    private final TextWatcher mEditAmountListener = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {
            try {
                float newValue = Float.parseFloat(s.toString());
                returnItem.setAmount(newValue);
                returnItem.recalcPrice();

                discount = recalcDiscount();

                printPrice();
                printDiscount();
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    };

    private final TextWatcher mEditPriceListener = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {
            try {
                float newValue = Float.parseFloat(s.toString());
                returnItem.setPrice(newValue);

                discount = recalcDiscount();

                printAmount();
                printDiscount();
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
        }
    };

    private final TextWatcher mEditCountListener = new TextWatcher() {

        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        public void afterTextChanged(Editable s) {
            float newValue;
            try {
                newValue = Float.parseFloat(s.toString());
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                newValue = 0f;
            }

            int newValueInt = (int) newValue;
            if (returnItem.mIsWeightGoods) {
                returnItem.setWeight(newValue);
                returnItem.setCount(1);
            } else {
                if (returnItem.inPack()) {
                    returnItem.setCountPack(newValueInt);
                } else {
                    returnItem.setCount(newValueInt);
                }
            }

            printPrice();
            printAmount();
        }
    };

    private final void printAmount() {
        if (returnItem == null)
            return;


        if (tvAmount != null) {
            tvAmount.setText(GlobalProc.toForeignPrice(returnItem.getAmount(), isForeign));
        }
        if (etAmount != null) {
            etAmount.removeTextChangedListener(mEditAmountListener);
            etAmount.setText(GlobalProc.toForeignPrice(returnItem.getAmount(), isForeign));
            etAmount.addTextChangedListener(mEditAmountListener);
        }
    }

    private final void printDiscount() {
        if (wvDiscount != null) {
            wvDiscount.setTag(false);
            wvDiscount.setCurrentItem(100 + discount);
            wvDiscount.setTag(true);
        }
    }

    private final void printPrice() {
        if (returnItem == null)
            return;

        if (etPrice != null) {
            etPrice.removeTextChangedListener(mEditPriceListener);
            etPrice.setText(GlobalProc.toForeignPrice(returnItem.getPrice(), isForeign));
            etPrice.addTextChangedListener(mEditPriceListener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rq_btCancel:
                getFragmentManager().popBackStack();
                break;
            case R.id.rq_btOK:
                updateData(true);
                Intent extras = new Intent();
                returnItem.setInfo("1");
                extras.putExtra(ReturnItem.KEY, returnItem);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, extras);
                getFragmentManager().popBackStack();
                break;
        }
    }

    private class MyOnWheelChangedListener implements OnWheelChangedListener {
        // private Goods goods;

        private MyOnWheelChangedListener() {
            // this.goods = goods;
        }
        private void recalcPrice() {
            float delta = standartPrice / 100 * discount;
            returnItem.setPrice(standartPrice + delta);
        }

        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {

            switch (wheel.getId()) {
                case R.id.rq_wheelMeash:
                    if (returnItem != null) {
                        returnItem.inPieces(newValue == 0);
                        printAmount();
                        if (etCount != null) {
                            if (returnItem.inPack()) {
                                etCount.setText(Integer.toString(returnItem.getCountPack()));
                            } else {
                                etCount.setText(GlobalProc.formatFloat("%.1f", returnItem.getCount()));
                            }
                        }
                    }
                    break;
                case R.id.rq_wheelDiscount:
                    discount = newValue - 100;
                    boolean needRecalc = (boolean)wheel.getTag();
                    if (returnItem != null && needRecalc) {
                        recalcPrice();
                        printAmount();
                        printPrice();
                    }
                    break;
            }
        }
    }
}
