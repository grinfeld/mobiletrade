package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;

import java.io.File;

/**
 * Created by GrinfeldRA on 17.11.2017.
 */

public class SummaryFragment extends Fragment {

    private WebView wv;
    private String DALIMO_URL =  "http://dalimo.com";
    private String FILE_PREFIX = "file:///";
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_summary, container,false);
        progressBar = rootView.findViewById(R.id.progress_bar_summary);
        progressBar.setVisibility(View.VISIBLE);
        wv = rootView.findViewById(R.id.wv);
        setHasOptionsMenu(true);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("");
            supportActionBar.setTitle("Сводные показатели");
        }
        setWebView();
        File rep = new File(App.get(getActivity()).fullPathOfReport());
        long size = rep.length();
        if (wv != null) {
            String address = DALIMO_URL;
            if (size > 0) {
                address = FILE_PREFIX + App.get(getActivity()).fullPathOfReport();
            }
            wv.loadUrl(address);
        }
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setWebView() {
        if (wv != null) {
            final Activity activity = getActivity();
            wv.setWebChromeClient(new WebChromeClient() {
                @SuppressWarnings("deprecation")
                public void onProgressChanged(WebView view, int progress) {
                    // Activities and WebViews measure progress with different
                    // scales.
                    // The progress meter will automatically disappear when we
                    // reach 100%
                    progressBar.setProgress(progress);
                    if (progress == 100){
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            });
            wv.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode,
                                            String description, String failingUrl) {
                    GlobalProc.mToast(activity, "Ошибка: " + description);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    // setHelpButtonImage(url);
                }
            });

            wv.getSettings().setBuiltInZoomControls(true);
            wv.getSettings().setSupportZoom(true);
            wv.getSettings().setLoadWithOverviewMode(true);
            wv.getSettings().setJavaScriptEnabled(true);
        }
    }
}
