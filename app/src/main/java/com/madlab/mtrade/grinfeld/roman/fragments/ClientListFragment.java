package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.CamActivity;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.components.CustomViewPager;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.ClientAdapter;
import com.madlab.mtrade.grinfeld.roman.db.ClientsMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.util.ArrayList;
import java.util.Locale;

import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_EDIT_ORDER;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_EDIT_RETURN;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_NEW_ORDER;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_NEW_PAYMENT;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_NEW_PHOTO;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_NEW_RETURN;
import static com.madlab.mtrade.grinfeld.roman.help.HelpFragment.IDD_NEW_CLAIM;

public class ClientListFragment extends ListFragment {

    public final static String PREF_SEARCH_QUERY = "SearchQuery";
    private final static String TAG = "!->ClientListFragment";
    private final static String TODAY_KEY = "today";
    private final static String CLIENTS_KEY = "clients";

    private final static byte MODE_ORDER = 11;
    private final static byte MODE_RETURN = 12;
    private final static byte MODE_PAYMENT = 13;
    private final static byte MODE_PHOTO = 14;
    private final static byte MODE_VISIT = 15;

    private final static byte DOC_NEW = 21;
    private final static byte DOC_EDIT = 22;

    private final static byte IDD_PHOTOREPORT = 1;

    private ArrayList<Client> mFullClientList;
    private ArrayList<Client> mFilteredClientList;

    private byte currentMode;
    private byte currentDoc;
    private ListView listView;


    public static int positionViewPager = 0;
    private ClientAdapter mClientAdapter;
    public static boolean searchFlag = false;
    private ArrayList<Client> clientsSearchList;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Context context;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setHasOptionsMenu(true);
    }


    public static ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            Log.d(TAG, "positionViewPager: " + String.valueOf(position));
            positionViewPager = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private final int FLAG_SALES_PLAN_IDD = 999;

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.client_list_context_menu, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Client client = (Client) getListView().getItemAtPosition(info.position);
        if (client.typeSalesPlan == 1) {
            menu.add(0, FLAG_SALES_PLAN_IDD, 0, "Спецификация содружества");
        }
        MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                onContextItemSelected(item);
                return true;
            }
        };
        for (int i = 0, n = menu.size(); i < n; i++) {
            menu.getItem(i).setOnMenuItemClickListener(listener);
        }
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(TAG, String.valueOf(item.getItemId()));
        if (mFullClientList == null)
            return super.onContextItemSelected(item);
        try {
            AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            ArrayList<Client> clients;
            if (searchFlag) {
                clients = clientsSearchList;
            } else if (getArguments() == null) {
                clients = loadClientsList(false);
            } else {
                boolean isToday = viewPager.getCurrentItem() == 0;
                clients = loadClientsList(isToday);
            }

            Client client = clients.get(aMenuInfo.position);
            int action = getTargetRequestCode();
            switch (item.getItemId()) {
                case R.id.menuClientList_info:
                    showClientInfo(client);
                    return true;
                case R.id.menuClientList_arrear:
                    PaymentClientListFragment fragment = new PaymentClientListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Client.KEY, client);
                    fragment.setArguments(bundle);
                    if (getArguments() != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            fragment.setTargetFragment(getParentFragment(), action);
                        } else {
                            fragment.setTargetFragment(this, action);
                        }
                    }
                    getActivity().getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.contentMain, fragment)
                            .commit();
                    return true;
                case FLAG_SALES_PLAN_IDD:
                    SalesPlansFragment salesPlansFragment = new SalesPlansFragment();
                    Bundle args = new Bundle();
                    args.putParcelable(Client.KEY, client);
                    salesPlansFragment.setArguments(args);
                    if (getArguments() != null) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            salesPlansFragment.setTargetFragment(getParentFragment(), action);
                        } else {
                            salesPlansFragment.setTargetFragment(this, action);
                        }
                    }
                    getActivity().getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.contentMain, salesPlansFragment)
                            .commit();
                    return true;
                default:
                    return true;
            }
        } catch (Exception e) {
            Log.e("!->Client", e.getMessage());
        }
        return super.onContextItemSelected(item);
    }


    public void initPage(int positionViewPager) {
        boolean isToday = positionViewPager == 0;
        loadClientsList(isToday);
        setAdapter();
    }


    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.client_list, container, false);
        listView = rootView.findViewById(android.R.id.list);
        context = getActivity();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(getString(R.string.pref_init_monitoring), true);
        editor.apply();
        if (getArguments() != null) {
            positionViewPager = getArguments().getInt(ClientViewPagerContainer.POSITION);
            viewPager = getActivity().findViewById(R.id.viewpager_visit);
            tabLayout = getActivity().findViewById(R.id.tabs_visit);
        } else {
            ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
            positionViewPager = 1;
        }
        registerForContextMenu(listView);
        initPage(positionViewPager);
        return rootView;
    }


    private void setClickableTablayout(boolean b) {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(b);
        }
    }


    SearchView searchView;
    SearchView.OnQueryTextListener queryTextListener;
    MenuItem searchItem;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.client_search_menu, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                Log.d(TAG, "onMenuItemActionExpand");
                if (getArguments() != null) {
                    ((CustomViewPager) viewPager).disableScroll(true);
                    setClickableTablayout(false);
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                if (getArguments() != null) {
                    ((CustomViewPager) viewPager).disableScroll(false);
                    setClickableTablayout(true);
                }
                searchFlag = false;
                initPage(positionViewPager);
                return true;
            }
        });
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchItem.getActionView();
        if (searchView != null) {
            if (searchManager != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            }
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.i("onQueryTextChange", newText);
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {
                    Log.i("onQueryTextSubmit", query);
                    clientsSearchList = loadClientsList(query.toUpperCase());
                    mClientAdapter = new ClientAdapter(getActivity(), clientsSearchList);
                    listView.setAdapter(mClientAdapter);
                    searchFlag = true;
                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        searchView.setOnQueryTextListener(queryTextListener);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IDD_NEW_PHOTO) {
            Fragment fragment = getFragmentManager().findFragmentById(R.id.contentMain);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getParentFragment().getTargetFragment().onActivityResult(requestCode, resultCode, data);
            } else {
                fragment.getTargetFragment().onActivityResult(requestCode, resultCode, data);
            }
            getActivity().getFragmentManager().popBackStack();
        } else {
            getActivity().getFragmentManager().popBackStack();
        }
    }

    private void startActionOrderFragment(Client mClient, int action) {
        OrderCommonFragment fragment = new OrderCommonFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Client.KEY, mClient);
        fragment.setArguments(bundle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), action);
        } else {
            fragment.setTargetFragment(this, action);
        }
        getActivity().getFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.contentMain, fragment)
                .commit();
    }


    private void startActionReturnFragment(int action) {
        ReturnCommonFragment fragment = new ReturnCommonFragment();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), action);
        } else {
            fragment.setTargetFragment(this, action);
        }
        getActivity().getFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.contentMain, fragment)
                .commit();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Client curClient;
        if (searchFlag) {
            curClient = clientsSearchList.get(position);
        } else {
            mClientAdapter.setSelected(position);
            mClientAdapter.notifyDataSetInvalidated();
            curClient = mClientAdapter.getItem(position);
        }
        // Выделяем выбранного клиента

        if (getArguments() != null) {
            switch (getTargetRequestCode()) {
                case IDD_NEW_CLAIM:
                    try {
                        String url = String.format(
                                "http://clients.dalimo.ru/m/new.php?c=%s&k=%s", App
                                        .get(context.getApplicationContext()).getCodeAgent(),
                                curClient.getCode());
                        Intent newClaim = new Intent(Intent.ACTION_VIEW);
                        newClaim.setData(Uri.parse(url));
                        startActivity(newClaim);
                    } catch (Exception e) {
                        GlobalProc.mToast(context, e.toString());
                    }
                    break;
                case IDD_NEW_ORDER:
                    if (Order.getOrder() != null) {
                        Order.getOrder().setClient(curClient);
                        Order.getOrder().mPrintBill = curClient.mTypeDoc;
                        startActionOrderFragment(curClient, IDD_NEW_ORDER);
                    }
                    break;
                case IDD_EDIT_ORDER:
                    if (Order.getOrder() != null) {
                        Order.getOrder().setClient(curClient);
                        startActionOrderFragment(curClient, IDD_EDIT_ORDER);
                    }
                    break;
                case IDD_NEW_RETURN:
                    if (Returns.getReturn() != null) {
                        Returns.getReturn().setClient(curClient);
                        startActionReturnFragment(IDD_NEW_RETURN);
                    }
                    break;
                case IDD_EDIT_RETURN:
                    if (Returns.getReturn() != null) {
                        Returns.getReturn().setClient(curClient);
                        startActionReturnFragment(IDD_EDIT_RETURN);
                    }
                    break;
                case IDD_NEW_PAYMENT:
                    PaymentClientListFragment fragment = PaymentClientListFragment.newInstance(curClient, null);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        fragment.setTargetFragment(getParentFragment(), IDD_NEW_PAYMENT);
                    } else {
                        fragment.setTargetFragment(fragment, IDD_NEW_PAYMENT);
                    }
                    getActivity().getFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.contentMain, fragment)
                            .commit();
                    break;
                default:
                    FragmentManager fragmentManager = getActivity().getFragmentManager();
                    fragmentManager.beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.contentMain, LocationFragment.newInstance(curClient))
                            .commitAllowingStateLoss();
                    break;
            }
        } else {
            getActivity().getFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.contentMain, TicketClientFragment.newInstance(curClient))
                    .commitAllowingStateLoss();
//            switch (getTargetRequestCode()){
////                case MainActivity.IDD_SALES_PLANS:
////                    getActivity().getFragmentManager().beginTransaction()
////                            .addToBackStack(null)
////                            .replace(R.id.contentMain, SalesPlansFragment.newInstance(curClient))
////                            .commit();
////                    break;
//                case MainActivity.IDD_TICKET_ClIENT:
//                    getActivity().getFragmentManager().beginTransaction()
//                            .addToBackStack(null)
//                            .replace(R.id.contentMain, TicketClientFragment.newInstance(curClient))
//                            .commit();
//                    break;
//            }

        }
        if (searchItem.isActionViewExpanded()) {
            searchItem.collapseActionView();
        }
    }


    private void setAdapter() {
        if (mFullClientList != null) {
            mClientAdapter = new ClientAdapter(getActivity(), mFullClientList);
        }
//        if (Order.getOrder() != null) {
//            Order.getOrder().getClient().getCode();
//
//        }
        listView.setAdapter(mClientAdapter);
        // Выбранный клиент
        int index = getClientIndex();
        if (mClientAdapter != null && index > -1) {
            mClientAdapter.setSelected(index);
            listView.setSelection(index);
        }
    }


    private int getClientIndex() {
        int res = -1;
        Client client = null;
        switch (getTargetRequestCode()) {
            case IDD_EDIT_ORDER:
                if (Order.getOrder() != null)
                    client = Order.getOrder().getClient();
                break;
            case IDD_EDIT_RETURN:
                if (Returns.getReturn() != null)
                    client = Returns.getReturn().getClient();
                break;
        }
        if (client != null) {
            res = Client.find(mFullClientList, client);
        }
        return res;
    }

    private ArrayList<Client> loadClientsList(boolean today) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();

            mFullClientList = Client.loadList(db, today);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, e.toString());
        }
        return mFullClientList;
    }

    private ArrayList<Client> loadClientsList(String searchRequest) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            String where = String.format("%s like ?",
                    ClientsMetaData.FIELD_UPPER_NAME, searchRequest);
            return Client.loadList(db, where, new String[]{"%" + searchRequest + "%"});
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, e.toString());
            return null;
        }
    }


    private String getPriceType(byte pType) {
        String[] types = getResources().getStringArray(R.array.list_prices);
        return types[pType];
    }

    private String getColoredSpanned(String text, String color) {
        return "<font color=" + color + ">" + text + "</font>";
    }

    public void showClientInfo(Client client) {
        final Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_client_info);
        TextView tvName = builder.findViewById(R.id.cliInfo_lbName);
        TextView tvAddress = builder.findViewById(R.id.cliInfo_lbAddress);
        TextView tvDirector = builder.findViewById(R.id.cliInfo_lbDirector);
        TextView tvManager = builder.findViewById(R.id.cliInfo_lbManager);
        TextView tvPhones = builder.findViewById(R.id.cliInfo_lbPhones);
        TextView tvCredit = builder.findViewById(R.id.cliInfo_lbCurCredit);
        TextView tvMaxCredit = builder.findViewById(R.id.cliInfo_lbMaxCredit);
        TextView tvPriceType = builder.findViewById(R.id.cliInfo_lbPriceType);
        TextView tvDiscount = builder.findViewById(R.id.cliInfo_lbDiscount);
        ImageButton imageButton = builder.findViewById(R.id.button_close);
        ImageView callBt = builder.findViewById(R.id.cliInfo_imgPhone);
        String colorDark = "#626d7f";
        String colorWhite = "#FFFFFF";
        if (client != null) {
            tvName.setText(client.mNameFull);
            tvAddress.setText(client.mPostAddress);
            if (!client.mDirector.isEmpty()) {
                String label = getColoredSpanned(getString(R.string.lb_director), colorDark);
                String val = getColoredSpanned(client.mDirector, colorWhite);
                tvDirector.setVisibility(View.VISIBLE);
                tvDirector.setText(Html.fromHtml(label + " " + val));
            }
            if (!client.mContact.isEmpty()) {
                String label = getColoredSpanned(getString(R.string.lb_manager), colorDark);
                String val = getColoredSpanned(client.mContact, colorWhite);
                tvManager.setVisibility(View.VISIBLE);
                tvManager.setText(Html.fromHtml(label + " " + val));
            }
            if (!client.mPhones.isEmpty()) {
                String label = getColoredSpanned(getString(R.string.lb_phones), colorDark);
                String val = getColoredSpanned(client.mPhones, colorWhite);
                tvPhones.setVisibility(View.VISIBLE);
                callBt.setVisibility(View.VISIBLE);
                tvPhones.setText(Html.fromHtml(label + " " + val));
            }
            if (!client.mCurArrear.isEmpty()) {
                String label = getColoredSpanned(getString(R.string.lb_curcredit), colorDark);
                String val = getColoredSpanned(client.mCurArrear, colorWhite);
                tvCredit.setVisibility(View.VISIBLE);
                tvCredit.setText(Html.fromHtml(label + " " + val));
            }
            if (!client.mMaxArrear.isEmpty()) {
                String label = getColoredSpanned(getString(R.string.lb_maxcredit), colorDark);
                String val = getColoredSpanned(client.mMaxArrear, colorWhite);
                tvMaxCredit.setVisibility(View.VISIBLE);
                tvMaxCredit.setText(Html.fromHtml(label + " " + val));
            }
            String labelPriceType = getColoredSpanned(getString(R.string.lb_typeprice), colorDark);
            String valPriceType = getColoredSpanned(getPriceType(client.mPriceType), colorWhite);
            tvPriceType.setText(Html.fromHtml(labelPriceType + " " + valPriceType));

            String labelDiscount = getColoredSpanned(getString(R.string.lb_discount), colorDark);
            String valDiscount = getColoredSpanned(Byte.toString(client.mDiscount), colorWhite);
            tvDiscount.setText(Html.fromHtml(labelDiscount + " " + valDiscount));

            ImageView tr = builder.findViewById(R.id.cliInfo_showOnMap);
            tr.setTag(client);
            tr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Client client = (Client) v.getTag();
                        if (client != null) {
                            String uri = client.mLatitude != 0f
                                    ? String.format(Locale.ENGLISH, "geo:%f,%f",
                                    client.mLatitude, client.mLongitude)
                                    : "geo:0,0?q=" + client.mPostAddress;
                            Intent geo = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                            startActivity(geo);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                        GlobalProc.mToast(context, e.toString());
                    }
                }
            });

            callBt.setTag(client);
            callBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Client client = (Client) v.getTag();
                        if (client != null) {
                            if (client.mPhones.trim().length() > 2) {
                                String uri = "tel:" + client.mPhones.trim();
                                startActivity(new Intent(
                                        android.content.Intent.ACTION_VIEW,
                                        Uri.parse(uri)));
                            }
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                }
            });
        } else {
            GlobalProc.mToast(context, getString(R.string.err_load_metadata));
            builder.dismiss();
        }
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });
        builder.show();
    }

}
