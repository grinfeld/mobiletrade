package com.madlab.mtrade.grinfeld.roman.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.OrderStatusMetaData;
import com.madlab.mtrade.grinfeld.roman.iface.IGetStatus;

public class GetOrderStatusTask extends AsyncTask<String, Integer, Boolean> {
	private final static String TAG = "!->GetOrderStatusTask";

	private final String COMMAND = "ПолучитьМаршруты";//"ORDERS_STATUS";
	private final String FAILURE = "ERR";

	private final static String SEPARATOR_ROWS = ";";
	private final static String SEPARATOR_ITEMS = "|";

	// private ProgressDialog mProgress;
	private Context baseContext;

	private IGetStatus mTaskCompleteListener;
	private DalimoClient mClient;
	private String mUnswer = "";

	// private String waitMessage;
	private String curDate;

	public String getMessage() {
		return mUnswer;
	}

	public GetOrderStatusTask(Context context, IGetStatus taskCompleteListener,
							  Date dateForRequest) {
		baseContext = context;
		mTaskCompleteListener = taskCompleteListener;

		Credentials info = Credentials.load(baseContext);
		mClient = new DalimoClient(info);
		curDate = TimeFormatter.sdf1C.format(dateForRequest);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		// mProgress = new ProgressDialog(baseContext);
		// mProgress.setMessage(waitMessage);
		// mProgress.setCancelable(true);
		// mProgress.show();
	}

	@Override
	protected Boolean doInBackground(String... params) {

		String message = null;
		if (params!=null){
			message = params[0];
		}
		if (message == null){
			String data = "";
			data = String.format("%s;%s;%s;%s;%s;","COMMAND", App.get(baseContext).getCodeAgent(), COMMAND, // эти
					// два
					// для
					// робота
					App.get(baseContext).getCodeAgent(), curDate); // эти два для 1С
			// А теперь всё это дело запускаем
			if (!mClient.connect()) {
				return false;
			}
			mClient.send(data + Const.END_MESSAGE);
			message = mClient.receive();
			mClient.disconnect();
		}

		// А теперь обрабатываем полученное сообщение
		if (message.contains(FAILURE)) {
			int index = message.indexOf(FAILURE);
			if (index > 0) {
				mUnswer = message.substring(0, index);
			}
			return false;
		}
		SQLiteDatabase db = null;
		try {
			MyApp app = (MyApp) baseContext.getApplicationContext();
			db = app.getDB();
			// Очищаем таблицу
			db.execSQL(OrderStatusMetaData.CLEAR);
			String[] querys = parseMessage(message);
			if (querys != null) {
				// Заполняем
				DBHelper.execMultipleSQLTXN(db, querys);
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}

		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		// dismiss();

		if (mTaskCompleteListener != null) {
			mTaskCompleteListener.onTaskComplete(this);
		}
	}

	// private void dismiss() {
	// if (mProgress != null) {
	// mProgress.hide();
	// mProgress.dismiss();
	// }
	// }

	/**
	 * Формируем массив запросов для БД
	 *
	 * @param data
	 */
	private String[] parseMessage(String data) {
		String[] rows = data.split(SEPARATOR_ROWS);
		if (rows.length < 1 || data.equals("")) {
			return null;
		}

		ArrayList<String> result = new ArrayList<String>();

		StringTokenizer token = null;

		String client = "";
		String expeditor = "";
		String phone = "";
		String status = "";
		String orderType = "";

		for (String line : rows) {
			token = new StringTokenizer(line, SEPARATOR_ITEMS);
			int cnt = token.countTokens();
			for (int i = 0; i < cnt; i++) {
				switch (i) {
					case 0:
						client = token.nextToken();
						break;
					case 1:
						expeditor = token.nextToken();
						break;
					case 2:
						String tmp = token.nextToken();
						phone = GlobalProc.normalizePhones(tmp);
						break;
					case 3:
						status = token.nextToken();
						break;
					case 4:
						orderType = token.nextToken();
						break;
				}
			}
			// А теперь создаем запрос
			String sql = String.format("INSERT INTO %s ("
							+ "%s, %s, %s, %s, %s) " + "VALUES ("
							+ "'%s', '%s', '%s', '%s', '%s')",
					OrderStatusMetaData.TABLE_NAME,
					OrderStatusMetaData.FIELD_CLIENT,
					OrderStatusMetaData.FIELD_EXPEDITOR,
					OrderStatusMetaData.FIELD_PHONE,
					OrderStatusMetaData.FIELD_STATUS,
					OrderStatusMetaData.FIELD_ORDER_TYPE, client, expeditor,
					phone, status, orderType);
			result.add(sql);
		}

		String[] arr = new String[result.size()];
		arr = result.toArray(arr);

		return arr;
	}
}
