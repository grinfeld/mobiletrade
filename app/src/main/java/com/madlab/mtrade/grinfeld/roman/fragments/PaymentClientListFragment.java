package com.madlab.mtrade.grinfeld.roman.fragments;

import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ListFragment;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.ClientCreditAdapter;
import com.madlab.mtrade.grinfeld.roman.components.InputMoneyDialog;
import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.PaymentsMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Arrear;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;


public class PaymentClientListFragment extends ListFragment {

    private final static String TAG = "!->ClientCreditActivity";

    private final static String EXTRA_CLIENT = "extraClient";
    private final static String EXTRA_VISIT_ID = "extraVisitID";

    private static final int IDD_PARTIAL_PAY = 0;
    private static final int IDD_PAY_WITHOUT_DOC = 1;

    private ArrayList<Arrear> creditList;

    private Client currentClient = null;
    private UUID visitFK;
    private BasementInfo basement;
    private Activity context;

    public static PaymentClientListFragment newInstance(Client current, UUID visit) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_CLIENT, current);
        args.putSerializable(EXTRA_VISIT_ID, visit);
        PaymentClientListFragment fragment = new PaymentClientListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        //setRetainInstance(true);
        // Показываем стрелку для возврата сверху, если есть родительская
        // активность
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//            if (NavUtils.getParentActivityName(context) != null) {
//                context.getActionBar().setDisplayHomeAsUpEnabled(true);
//            }
//        }
        setHasOptionsMenu(true);

        context.setTitle(R.string.cap_clientCreaditInfo);
        if (getArguments().getParcelable(EXTRA_CLIENT) != null){
            currentClient = getArguments().getParcelable(EXTRA_CLIENT);
            visitFK = (UUID) getArguments().getSerializable(EXTRA_VISIT_ID);
        }else {
            currentClient = getArguments().getParcelable(Client.KEY);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        loadCreditData();
        // Подвал
        fillBasement(context);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        View v = inflater.inflate(R.layout.fragment_credit_client_info, container, false);
        TextView tvClient =  v.findViewById(R.id.cci_lbName);
        TextView tvAddress =  v.findViewById(R.id.cci_lbAddress);
        tvAddress.setText(currentClient.mPostAddress);
        tvClient.setText(currentClient.mNameFull);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //((ClientCreditAdapter) getListAdapter()).notifyDataSetChanged();
        Arrear currentDoc = data.getParcelableExtra(InputMoneyDialog.EXTRA_RESULT);
        switch (requestCode) {
            case IDD_PARTIAL_PAY:
                partialPay(currentDoc, true);
                break;
            case IDD_PAY_WITHOUT_DOC:
                partialPay(currentDoc, false);
                break;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        final Arrear credit = ((ClientCreditAdapter) getListAdapter()).getItem(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.cap_choose_action));
        String[] actions = getResources().getStringArray(R.array.pay_doc_actions);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.select_dialog_item, actions);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:// полная оплата
                        fullPay(credit);
                        break;
                    case 1:// Частичная оплата
                        partialPay(credit);
                        break;
                    case 2:// Оплата без документа
                        payWithoutDoc();
                        break;
                    case 3:// Удалить оплату
                        m_delete(credit).show();
                        break;
                }
            }
        });
        builder.setCancelable(true);
        builder.create().show();
    }


    protected Dialog m_delete(final Arrear doc) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Подтверждение");
        String msg = String.format(
                "Удалить платеж документа \"%s\" безвозвратно?",
                doc.getDocNumber());
        builder.setMessage(msg);
        builder.setIcon(R.mipmap.main_icon);
        builder.setPositiveButton(
                context.getString(android.R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deletePay(doc);
                    }
                });
        builder.setCancelable(true);
        return builder.create();
    }

    private void partialPay(Arrear mCredit) {
        DialogFragment dlg = InputMoneyDialog.newInstance(mCredit);
        dlg.setTargetFragment(this, IDD_PARTIAL_PAY);
        dlg.show(context.getFragmentManager(), TAG);
    }

    private void payWithoutDoc() {
        String number = String.format(Locale.getDefault(), "<б.д.%04d>", DBProp.GetNumDoc());
        Arrear payWODoc = new Arrear();
        payWODoc.setAgentCode(App.get(context).getCodeAgent());
        payWODoc.setDocNumber(number);
        payWODoc.setClient(currentClient.getCode());
        DialogFragment dlg = InputMoneyDialog.newInstance(payWODoc);
        dlg.setTargetFragment(PaymentClientListFragment.this,
                IDD_PAY_WITHOUT_DOC);
        dlg.show(context.getFragmentManager(), TAG);
    }


    protected void partialPay(Arrear creditDoc, boolean withDoc) {
        SQLiteDatabase db = null;

        boolean success = true;

        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();

            String sql = String.format(Locale.ENGLISH, "INSERT INTO %s ("
                            + "%s, %s, " + "%s, %s, " + "%s, %s, " + "%s, %s) "
                            + "VALUES (" + "'%s', '%s', " + "'%s', '%s', "
                            + "'%s', %.3f," + " %d, '%s')", PaymentsMetaData.TABLE_NAME,
                    PaymentsMetaData.FIELD_CODE_AGENT,
                    PaymentsMetaData.FIELD_CODE_CLIENT,
                    PaymentsMetaData.FIELD_DOC_NUMBER,
                    PaymentsMetaData.FIELD_DOC_DATE,
                    PaymentsMetaData.FIELD_DOC_SUM,
                    PaymentsMetaData.FIELD_AMOUNT,
                    PaymentsMetaData.FIELD_REC_TYPE,
                    PaymentsMetaData.FIELD_VISIT_FK,
                    // Данные
                    creditDoc.getAgentCode(), creditDoc.getClient(), creditDoc
                            .getDocNumber(), TimeFormatter.sdfSQL.format(creditDoc
                            .getDocDate()), creditDoc.getDocSum(), creditDoc
                            .getDocPayment(), withDoc ? 1 : 2,
                    visitFK == null ? "''" : visitFK);

            if (withDoc) {
                ContentValues cv = new ContentValues();
                cv.put(PaymentsMetaData.FIELD_AMOUNT, creditDoc.getDocPayment());

                String where = String.format(Locale.ENGLISH, "%s = '%s' AND "
                                + "%s = '%s' AND " + "%s = '%s'",
                        PaymentsMetaData.FIELD_CODE_AGENT,
                        creditDoc.getAgentCode(),
                        PaymentsMetaData.FIELD_DOC_NUMBER,
                        creditDoc.getDocNumber(),
                        PaymentsMetaData.FIELD_DOC_DATE,
                        TimeFormatter.sdfSQL.format(creditDoc.getDocDate()));

                // Если обновить не удалось - вставляем
                if (db.update(PaymentsMetaData.TABLE_NAME, cv, where, null) == 0) {
                    db.execSQL(sql);
                }
            } else {// без документа
                db.execSQL(sql);
            }

            if (!withDoc) {
                DBProp.IncNumDoc();
                DBProp.UpdateDBProp(db);
            }
        } catch (Exception e) {
            Log.e("!->partialPayError", e.getMessage());
            success = false;
            GlobalProc.mToast(context, "Ошибка оплаты документа");
        }

        if (success) {
            //loadCreditData();
            fillBasement(context);

        }
    }

    /**
     * Загружаем список долгов и подвал из БД. Создаем новый адаптер
     */
    private void loadCreditData() {
        SQLiteDatabase db = null;

        Cursor queryRes = null;
        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();

            // загрузка данных из БД

            // Список документов
            Arrear.loadList(db, App.get(context).getCodeAgent(), currentClient.getCode());

            // Теперь выводим данные на форму
            setAdapter();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            GlobalProc.mToast(context, "Ошибка загрузки долгов");
        } finally {
            if (queryRes != null) {
                queryRes.close();
            }
        }
    }

    // выводим данные подвала на форму
    private void fillBasement(Activity act) {
        basement = loadBasement(context, currentClient.getCode());
        TextView tvCliDocCount = (TextView) act
                .findViewById(R.id.cci_lbTotalDocCount);
        if (tvCliDocCount != null && creditList != null) {
            tvCliDocCount.setText(String.format("Всего %d док.",
                    creditList.size()));
        }

        // долг по клиенту
        TextView tvCliCredit = (TextView) act
                .findViewById(R.id.cci_lbClientCredit);

        if (tvCliCredit != null && basement != null) {
            tvCliCredit.setText(GlobalProc.toForeignPrice(basement.getCurClientCredit(), currentClient.isForeignAgent()));
        }

        // оплата по клиенту
        TextView tvCliPayment = (TextView) act
                .findViewById(R.id.cci_lbClientPayment);

        if (tvCliPayment != null && basement != null) {
            tvCliPayment.setText(GlobalProc.toForeignPrice(basement
                    .getCurClientPaymentWithDoc()
                    + basement.getCurClientPaymentWithoutDoc(), currentClient.isForeignAgent()));
        }

        // Общий долг
        TextView tvTotalCredit = (TextView) act
                .findViewById(R.id.cci_lbTotalCredit);

        if (tvTotalCredit != null && basement != null) {
            tvTotalCredit.setText(GlobalProc.toForeignPrice(basement.getTotalCredit(), currentClient.isForeignAgent()));
        }

        // Общая оплата
        TextView tvTotalPayment = (TextView) act
                .findViewById(R.id.cci_lbTotalPayment);

        if (tvTotalPayment != null && basement != null) {
            tvTotalPayment.setText(GlobalProc.toForeignPrice(basement.getTotalPayment(), currentClient.isForeignAgent()));
        }
    }

    protected void fullPay(Arrear creditDoc) {
        /**
         * Здесь нужно рассмотреть вариант с существующей частичной оплатой
         */
        SQLiteDatabase db;
        boolean success = true;
        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();
            Cursor cursor = db.rawQuery("SELECT * FROM Payment WHERE NumDoc = '"+creditDoc.getDocNumber()+"';", null);
            String sql;
            if (cursor.getCount()==0){
                sql = String.format(
                        Locale.ENGLISH,
                        "INSERT INTO Payment ("
                                + "%s, %s, %s, %s, %s, %s, %s, %s) VALUES ("
                                + "'%s', '%s', " + "'%s', '%s', "
                                + "'%s', %s, 1, '%s')",
                        PaymentsMetaData.FIELD_CODE_AGENT,
                        PaymentsMetaData.FIELD_CODE_CLIENT,
                        PaymentsMetaData.FIELD_DOC_NUMBER,
                        PaymentsMetaData.FIELD_DOC_DATE,
                        PaymentsMetaData.FIELD_DOC_SUM,
                        PaymentsMetaData.FIELD_AMOUNT,
                        PaymentsMetaData.FIELD_REC_TYPE,
                        PaymentsMetaData.FIELD_VISIT_FK,
                        // данные
                        creditDoc.getAgentCode(), creditDoc.getClient(),
                        creditDoc.getDocNumber(),
                        TimeFormatter.sdfSQL.format(creditDoc.getDocDate()),
                        creditDoc.getDocSum(), creditDoc.getDocCredit(),
                        visitFK == null ? "''" : visitFK);
            } else {
                sql = String.format(
                        Locale.ENGLISH,
                        "UPDATE Payment SET %s = '%s', %s = '%s', %s = '%s', %s = '%s' WHERE %s = '%s'",
                        PaymentsMetaData.FIELD_DOC_DATE,
                        TimeFormatter.sdfSQL.format(creditDoc.getDocDate()),
                        PaymentsMetaData.FIELD_DOC_SUM,
                        creditDoc.getDocSum(),
                        PaymentsMetaData.FIELD_AMOUNT,
                        creditDoc.getDocCredit(),
                        PaymentsMetaData.FIELD_VISIT_FK,
                        visitFK == null ? "''" : visitFK,
                        PaymentsMetaData.FIELD_DOC_NUMBER,
                        creditDoc.getDocNumber());
            }
            cursor.close();
            db.execSQL(sql);
        } catch (Exception e) {
            Log.e("!->FullPayError", e.getMessage());
            success = false;
            GlobalProc.mToast(context, "Ошибка оплаты документа");
        }

        if (success) {
            //loadCreditData();
            creditDoc.setDocPayment(creditDoc.getDocCredit());
            ClientCreditAdapter cca = (ClientCreditAdapter) getListAdapter();
            cca.notifyDataSetChanged();
            fillBasement(context);
        }
    }

    protected void deletePay(Arrear doc) {
        SQLiteDatabase db = null;
        boolean success = true;
        int res = 0;

        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();

            String where = String.format(Locale.ENGLISH, "%s = '%s' AND "
                            + "%s = '%s' AND " + "%s = '%s' AND " + "%s = %s",
                    PaymentsMetaData.FIELD_CODE_AGENT, doc.getAgentCode(),
                    PaymentsMetaData.FIELD_CODE_CLIENT, doc.getClient(),
                    PaymentsMetaData.FIELD_DOC_NUMBER, doc.getDocNumber(),
                    PaymentsMetaData.FIELD_AMOUNT, doc.getDocPayment());

            res = db.delete(PaymentsMetaData.TABLE_NAME, where, null);
            Log.i(TAG, String.format("successfull deleted %d rows", res));
        } catch (Exception e) {
            GlobalProc.mToast(context,
                    "Ошибка удаления оплаты: " + e.getMessage());
            Log.e("!->PaymentDeleteError", e.getMessage());
            success = false;
        }

        if (success) {
            // уведомляем адаптер об изменении
            ClientCreditAdapter cca = (ClientCreditAdapter) getListAdapter();
            cca.remove(doc);
            cca.notifyDataSetChanged();
            fillBasement(context);
            // fillBasement(context.getLayoutInflater().inflate(R.layout.client_credit_info,
            // null));

        }
    }

    private void setAdapter() {
        if (Arrear.getList() == null) {
            SQLiteDatabase db;
            try {
                MyApp app = (MyApp) context.getApplication();
                db = app.getDB();

                // загрузка данных из БД, Список документов
                Arrear.loadList(db, App.get(context).getCodeAgent(), currentClient.getCode());
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }

        creditList = Arrear.getList();

        if (creditList != null) {
            ClientCreditAdapter cca = new ClientCreditAdapter(context, creditList, currentClient.isForeignAgent());
            setListAdapter(cca);
        }
    }

    public BasementInfo loadBasement(Activity ctx, String codeClient) {
        SQLiteDatabase db = null;
        BasementInfo basement = new BasementInfo();

        Cursor queryRes = null;
        try {
            MyApp app = (MyApp) ctx.getApplication();
            db = app.getDB();

            // Долги по клиенту-------------------------------------
            String strSQL = String.format(Locale.ENGLISH,
                    "SELECT Sum( %s.%s ) as CreditSum " + "FROM %s "
                            + "WHERE %s.%s='%s' AND " + "%s.%s = '%s' ",
                    ClientCreditInfoMetaData.TABLE_NAME,
                    ClientCreditInfoMetaData.FIELD_CREDIT,
                    ClientCreditInfoMetaData.TABLE_NAME,
                    ClientCreditInfoMetaData.TABLE_NAME,
                    ClientCreditInfoMetaData.FIELD_CODE_AGENT,
                    App.get(context).getCodeAgent(), ClientCreditInfoMetaData.TABLE_NAME,
                    ClientCreditInfoMetaData.FIELD_CLIENT, codeClient);

            queryRes = db.rawQuery(strSQL, null);

            if (queryRes.moveToFirst()) {
                basement.setCurClientCredit(queryRes.getFloat(0));
            }
            queryRes.close();

            // Оплата по клиенту------------------------------------

            // Без документа
            strSQL = String.format(Locale.ENGLISH,
                    "SELECT Sum( %s ) as PaySum " + "FROM %s "
                            + "WHERE %s='%s' AND " + "%s='%s' AND RecType = 2",
                    PaymentsMetaData.FIELD_AMOUNT, PaymentsMetaData.TABLE_NAME,
                    PaymentsMetaData.FIELD_CODE_AGENT, App.get(context).getCodeAgent(),
                    PaymentsMetaData.FIELD_CODE_CLIENT, codeClient);

            queryRes = db.rawQuery(strSQL, null);

            if (queryRes.moveToFirst()) {
                basement.setCurClientPaymentWithoutDoc(queryRes.getFloat(0));
            }
            queryRes.close();

            // С документом
            strSQL = String.format(Locale.ENGLISH,
                    "SELECT Sum( %s ) as PaySum " + "FROM %s "
                            + "WHERE %s='%s' AND " + "%s='%s' AND RecType = 1",
                    PaymentsMetaData.FIELD_AMOUNT, PaymentsMetaData.TABLE_NAME,
                    PaymentsMetaData.FIELD_CODE_AGENT, App.get(context).getCodeAgent(),
                    PaymentsMetaData.FIELD_CODE_CLIENT, codeClient);

            queryRes = db.rawQuery(strSQL, null);

            if (queryRes.moveToFirst()) {
                basement.setCurClientPaymentWithDoc(queryRes.getFloat(0));
            }
            queryRes.close();

            // Общий долг--------------------------------------
            strSQL = String.format(Locale.ENGLISH,
                    "SELECT Sum( %s ) as CreditSum " + "FROM %s "
                            + "WHERE %s = '%s'",
                    ClientCreditInfoMetaData.FIELD_CREDIT,
                    ClientCreditInfoMetaData.TABLE_NAME,
                    ClientCreditInfoMetaData.FIELD_CODE_AGENT,
                    App.get(context).getCodeAgent());

            queryRes = db.rawQuery(strSQL, null);

            if (queryRes.moveToFirst()) {
                basement.setTotalCredit(queryRes.getFloat(0));
            }
            queryRes.close();

            // Общая оплата--------------------------------------
            strSQL = String.format(Locale.ENGLISH,
                    "SELECT Sum( %s ) as CreditSum " + "FROM %s "
                            + "WHERE %s = '%s'", PaymentsMetaData.FIELD_AMOUNT,
                    PaymentsMetaData.TABLE_NAME,
                    PaymentsMetaData.FIELD_CODE_AGENT, App.get(context).getCodeAgent());

            queryRes = db.rawQuery(strSQL, null);

            if (queryRes.moveToFirst()) {
                basement.setTotalPayment(queryRes.getFloat(0));
            }
            queryRes.close();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            GlobalProc.mToast(ctx, "Ошибка загрузки долгов");
        } finally {
            if (queryRes != null) {
                queryRes.close();
            }
        }

        return basement;
    }

    /**
     * подвал
     *
     * @author konovaloff
     *
     */
    private class BasementInfo implements Parcelable {
        // public static final String KEY = "basement_key";

        private float mCurClientCredit;
        private float mCurClientPaymentWithDoc;
        private float mCurClientPaymentWithoutDoc;
        private float mTotalCredit;
        private float mTotalPayment;

        public void setCurClientCredit(float curClientCredit) {
            mCurClientCredit = curClientCredit;
        }

        public float getCurClientCredit() {
            return mCurClientCredit;
        }

        public void setCurClientPaymentWithDoc(float curClientPaymentWithDoc) {
            mCurClientPaymentWithDoc = curClientPaymentWithDoc;
        }

        public float getCurClientPaymentWithDoc() {
            return mCurClientPaymentWithDoc;
        }

        public void setCurClientPaymentWithoutDoc(
                float curClientPaymentWithoutDoc) {
            mCurClientPaymentWithoutDoc = curClientPaymentWithoutDoc;
        }

        public float getCurClientPaymentWithoutDoc() {
            return mCurClientPaymentWithoutDoc;
        }

        public void setTotalCredit(float totalCredit) {
            mTotalCredit = totalCredit;
        }

        public float getTotalCredit() {
            return mTotalCredit;
        }

        public void setTotalPayment(float totalPayment) {
            mTotalPayment = totalPayment;
        }

        public float getTotalPayment() {
            return mTotalPayment;
        }

        public BasementInfo() {
            mCurClientCredit = 0;
            mCurClientPaymentWithDoc = 0;
            mCurClientPaymentWithoutDoc = 0;
            mTotalCredit = 0;
            mTotalPayment = 0;
        }

        public BasementInfo(Parcel data) {
            mCurClientCredit = data.readFloat();
            mCurClientPaymentWithDoc = data.readFloat();
            mCurClientPaymentWithoutDoc = data.readFloat();
            mTotalCredit = data.readFloat();
            mTotalPayment = data.readFloat();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel arg0, int arg1) {
            arg0.writeFloat(mCurClientCredit);
            arg0.writeFloat(mCurClientPaymentWithDoc);
            arg0.writeFloat(mCurClientPaymentWithoutDoc);
            arg0.writeFloat(mTotalCredit);
            arg0.writeFloat(mTotalPayment);
        }

        @SuppressWarnings("unused")
        public final Parcelable.Creator<BasementInfo> CREATOR = new Parcelable.Creator<BasementInfo>() {

            public BasementInfo createFromParcel(Parcel in) {
                return new BasementInfo(in);
            }

            public BasementInfo[] newArray(int size) {
                return new BasementInfo[size];
            }
        };
    }
}
