package com.madlab.mtrade.grinfeld.roman.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Monitoring;
import com.madlab.mtrade.grinfeld.roman.entity.MonitoringProduct;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.eventbus.GoodsListEventFilter;
import com.madlab.mtrade.grinfeld.roman.eventbus.GoodsListEventZoom;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnCountChanged;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnCountChangedTotalSize;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnMeashChanged;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.madlab.mtrade.grinfeld.roman.db.DBHelper.MATRIX_QUERY_M;
import static com.madlab.mtrade.grinfeld.roman.fragments.GoodsInfoContainer.KEY_FOREIGN;
import static com.madlab.mtrade.grinfeld.roman.fragments.ReturnGoodsFragment.KEY_RETURN_FOREIGN;


public class GoodsTreeFragment extends Fragment implements View.OnClickListener {
    public final static String TAG = "!->GoodsTreeFragment";

    private Context context;
    /**
     * Тип загрузки. Все товары, матрица и т. п.
     */
    public final static String EXTRA_TYPE = "LoadType";

    /**
     * Режим: Справочник, заявка, возврат
     */
    public final static String EXTRA_MODE = "Mode";

    /**
     * Последний выбранный узел
     */
    public final static String EXTRA_NODE = "Node";

    /**
     * Выбранный клиент
     */
    public final static String EXTRA_CLIENT = "SelectedClient";

    public final static byte QUALITY_NON_LIQUID = 99;

    public final static byte IDD_GOODS_INFO = 0;
    public final static byte IDD_ORDER_QUANTITY = 1;
    public final static byte IDD_RETURN_QUANTITY = 2;

    /**
     * Тип загрузки: Все, матрица,
     */
    private byte mType;

    /**
     * Код выбранного узла
     */
    private Node mSelectedNode;

    /**
     * Режим отображения списка товаров (используемый адаптер): Справочник,
     * Заявка, Возврат
     */
    private byte mListViewMode = GoodsAdapter.MODE_REFERENCE;

    /**
     * Фильтр элементов списка товаров. 0 - ВСЕ, 1 - с остатками, 2 - СТМ.
     */
    private byte mCurrentFilter = GoodsListFragment.RADIO_WITH_REST;

    private Client mClient;
    private byte mDefaultMeashValue;

    public ArrayList<GoodsInDocument> mExistedItems;

    private View _rootView;

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        setTargetFragment(getTargetFragment(), getTargetRequestCode());
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        Order data = Order.getOrder();
        if (data != null) {
            //data.setTotalsChangedListener(null);
        }
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        Log.d(TAG, "onCreate");
        String[] prices = getResources().getStringArray(R.array.list_prices);
        int priceLvl = 1;
        byte discount = 0;
        int backStackEntryCount = getFragmentManager().getBackStackEntryCount();
        // если в штуках, то 0, если в упаковках, то 1
        mDefaultMeashValue = App.get(getActivity()).getMeash()
                .equalsIgnoreCase(getString(R.string.pieces)) ? (byte) 0 : 1;
        Bundle args = getArguments();
        if (args != null) {
            mType = args.getByte(GoodsTreeFragment.EXTRA_TYPE);
            mSelectedNode = args.getParcelable(GoodsTreeFragment.EXTRA_NODE);
            mListViewMode = args.getByte(GoodsTreeFragment.EXTRA_MODE);
            mClient = args.getParcelable(GoodsTreeFragment.EXTRA_CLIENT);
            mExistedItems = args.getParcelableArrayList(GoodsInDocument.KEY);
            if (mClient != null) {
                priceLvl = prices.length > mClient.mPriceType ? mClient.mPriceType : priceLvl;
                discount = mClient.mDiscount;
            }
            String subtitle = "Цена: " + prices[priceLvl];
            if (discount != 0) {
                subtitle += String.format(Locale.US, ". Скидка %d%%", discount);
            }
//            if (mType == GoodsList.MODE_PROMOTIONS) {
//                ((MainActivity) getActivity()).setTitleAndSubTitleAndIsBackButtonOrHome("Акции", "", backStackEntryCount > 0);
//            } else {
//                ((MainActivity) getActivity()).setTitleAndSubTitleAndIsBackButtonOrHome("Список товаров", subtitle, backStackEntryCount > 0);
//            }

        }
        initRootView();
    }

    private void replace(Fragment fragment, String tag) {
        getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, fragment, tag).commit();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener((v, keyCode, event) -> {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    if (getArguments().getString(MainActivity.PRICE) != null) {
                        getFragmentManager().popBackStack();
                    } else {
                        Intent extras = new Intent();
                        extras.putExtra(GoodsInDocument.KEY, mExistedItems);
                        FragmentManager fragmentManager = getFragmentManager();
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, extras);
                        fragmentManager.popBackStack();
                        return true;
                    }
                }
                return false;
            });
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
            if (preferences.getInt("FirstStart", 0) == 1) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("FirstStart", 0);
                editor.apply();
            }
            int startPagePromotion = preferences.getInt("StartPagePromotion", 0);
            if (startPagePromotion == 1) {
                FragmentManager fragmentManager = getFragmentManager();
                boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                if (tabletSize) {
                    Fragment goodsListFragment = fragmentManager.findFragmentByTag(GoodsListFragment.TAG);
                    Fragment gMatrixFragment = fragmentManager.findFragmentByTag(GoodsGroupMatrixFragment.TAG);
                    if (goodsListFragment != null && goodsListFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                    if (gMatrixFragment != null && gMatrixFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                }
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("StartPagePromotion", 0);
                editor.apply();
            }
        } catch (Exception ignored) {

        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onZoomClicked(GoodsListEventZoom goodsListEventZoom) {
        View view = _rootView.findViewById(R.id.fragmentContainer);
        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        if (_rootView.getParent() != null) {
            ((ViewGroup) _rootView.getParent()).removeView(_rootView);
        }
        super.onDestroyView();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            // Inflate the layout for this fragment
            _rootView = inflater.inflate(R.layout.fragment_goods_tree_contener, container, false);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
            int firstStart = preferences.getInt("FirstStart", 0);
            if (firstStart == 1) {
                _rootView.findViewById(R.id.menu_bottom).setVisibility(View.VISIBLE);
                _rootView.findViewById(R.id.btn_visit).setOnClickListener(this);
                _rootView.findViewById(R.id.btn_stat).setOnClickListener(this);
                _rootView.findViewById(R.id.btn_info).setOnClickListener(this);
            }

            //Если у нас есть фрагмент со списком товаров - рисуем, загружаем
            if (mType != GoodsList.MODE_LIST) {
                if (App.isMerch() && getArguments().getString(MainActivity.PRICE) == null) {
                    if (getTargetRequestCode() == VisitContainFragment.IDD_NEW_MONITORING) {
                        MyApp app = (MyApp) getActivity().getApplication();
                        SQLiteDatabase db = app.getDB();
                        Cursor cursor = db.rawQuery(MATRIX_QUERY_M, new String[]{mClient.getCode()});
                        int indexMonitoringFromUnsaved = Monitoring.getCurrentMonitoring().getId();
                        List<MonitoringProduct> monitoringProducts = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            String codeGoods = cursor.getString(GoodsMetaData.FIELD_CODE.Index);
                            monitoringProducts.add(new MonitoringProduct(indexMonitoringFromUnsaved, codeGoods, 1));
                        }
                        Monitoring.getCurrentMonitoring().addProductItemAll(monitoringProducts);
                        Monitoring.getCurrentMonitoring().update(MyApp.getContext());
                        cursor.close();
                    }
                    if (getTargetRequestCode() != VisitContainFragment.IDD_NEW_TASK) {
                        ArrayList<MonitoringProduct> itemsList = Monitoring.getCurrentMonitoring().getItemsList();
                        for (MonitoringProduct monitoringProduct : itemsList) {
                            if (monitoringProduct.getCheck() == 1) {
                                mExistedItems.add(new GoodsInDocument(monitoringProduct.getCodeProduct(), 1, (byte) 1, 1, 1, 1));
                            }
                        }
                        EventBus.getDefault().post(new OnCountChangedTotalSize(mExistedItems.size()));
                    } else {
                        Task currentTask = Task.getCurrentTask();
                        for (GoodsInTask taskProduct : currentTask.goodsList()) {
                            mExistedItems.add(new GoodsInDocument(taskProduct.code(), taskProduct.count(), mDefaultMeashValue, taskProduct.getQuant(), 1, 1));
                        }
                        EventBus.getDefault().post(new OnCountChangedTotalSize(mExistedItems.size()));
                    }
                }
                if (_rootView.findViewById(R.id.detailFragmentContainer) != null) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment newList = GoodsListFragment.newInstance(mListViewMode, mType,//FIXME
                            mSelectedNode, mCurrentFilter, mClient, mExistedItems, getArguments().getString(MainActivity.PRICE));
                    newList.setTargetFragment(this, getTargetRequestCode());
                    ft.replace(R.id.detailFragmentContainer, newList, GoodsListFragment.TAG);
                    ft.commit();
                }
            } else {
                if (_rootView.findViewById(R.id.detailFragmentContainer) != null) {
                    View view = _rootView.findViewById(R.id.detailFragmentContainer);
                    view.setVisibility(View.GONE);
                }
            }
        }
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (GoodsList.MODE_PROMOTIONS == mType && tabletSize) {
            replace(GoodsGroupMatrixFragment.newInstance(mType, mClient, mExistedItems, mListViewMode, getArguments().getString(MainActivity.PRICE)), GoodsGroupMatrixFragment.TAG);
        }
        return _rootView;
    }


    private void initRootView() {
        switch (mType) {
            case GoodsList.MODE_PROMOTIONS:
                boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                if (!tabletSize) {
                    replace(GoodsGroupMatrixFragment.newInstance(mType, mClient, mExistedItems, mListViewMode, getArguments().getString(MainActivity.PRICE)), GoodsGroupMatrixFragment.TAG);
                }
                break;
            case GoodsList.MODE_ALL:
                replace(GoodsGroupListFragment.newInstance(mType, mSelectedNode, mClient, mExistedItems, mListViewMode, getArguments().getString(MainActivity.PRICE)), GoodsGroupListFragment.TAG);
                break;
            case GoodsList.MODE_LIST:
                replace(GoodsListFragment.newInstance(mListViewMode, mType, mSelectedNode, (byte) 0, mClient, mExistedItems, getArguments().getString(MainActivity.PRICE)), GoodsListFragment.TAG);
                break;
            case GoodsList.MODE_MATRIX:
                GoodsGroupMatrixFragment goodsGroupMatrixFragment = GoodsGroupMatrixFragment.newInstance(mType, mClient, mExistedItems, mListViewMode, null);
                if (App.isMerch()) {
                    goodsGroupMatrixFragment.setTargetFragment(this, getTargetRequestCode());
                }
                replace(goodsGroupMatrixFragment, GoodsGroupMatrixFragment.TAG);
                break;
            case GoodsList.MODE_STM:
                replace(GoodsGroupMatrixFragment.newInstance(mType, mClient, mExistedItems, mListViewMode, null), GoodsGroupMatrixFragment.TAG);
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case IDD_ORDER_QUANTITY:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    OrderItem orderItem = data.getParcelableExtra(OrderItem.KEY);
                    Log.d(TAG, String.valueOf(orderItem));
                    GoodsInDocument gid = null;
                    if (mExistedItems == null) {
                        mExistedItems = new ArrayList<>();
                    } else {
                        for (GoodsInDocument item : mExistedItems) {
                            if (orderItem.getCodeGoods().equalsIgnoreCase(item.getCode())) {
                                gid = item;
                                gid.setMeashPosition(orderItem.inPack() ? (byte) 1 : 0);
                                gid.setCountPosition(orderItem.inPack() ? orderItem
                                        .getCountPack() : orderItem
                                        .getCount());
                                gid.isSpecPrice(orderItem.mIsSpecialPrice);
                                gid.setPrice(orderItem.getPrice());
                                gid.setmIsHalfHead(orderItem.mIsHalfHead);
                                gid.isSertificate(orderItem.isSertificateNeed());
                                gid.isQualityDoc(orderItem.isQualityDocNeed());
                                break;
                            }
                        }
                    }
                    if (gid == null) {
                        byte meash = orderItem.inPack() ? (byte) 1 : 0;
                        int count = orderItem.inPack() ? orderItem
                                .getCountPack() : orderItem.getCount();
                        gid = new GoodsInDocument(orderItem.getCodeGoods(), count,
                                meash, orderItem.getQuant(), orderItem.getPrice(), orderItem.getWeight());
                        gid.isSpecPrice(orderItem.mIsSpecialPrice);
                        gid.setPrice(orderItem.getPrice());
                        gid.setmIsHalfHead(orderItem.mIsHalfHead);
                        gid.isSertificate(orderItem.isSertificateNeed());
                        gid.isQualityDoc(orderItem.isQualityDocNeed());
                        mExistedItems.add(gid);
                    }
                    updateAdapterAfterItemsAdd();
                }
                break;
            case IDD_RETURN_QUANTITY:
//                // Добавляем товар в возврат
                if (resultCode == Activity.RESULT_OK && data != null) {
                    ReturnItem returnItem = data.getParcelableExtra(ReturnItem.KEY);
                    if (Returns.getReturn().find(returnItem.getCodeGoods()) == null) {
                        Returns.getReturn().addItem(returnItem);
                    } else {
                        Returns.getReturn().updateItem(returnItem);
                    }
                    GoodsInDocument gid = null;
                    if (mExistedItems == null) {
                        mExistedItems = new ArrayList<>();
                    } else {
                        for (GoodsInDocument item : mExistedItems) {
                            if (returnItem.getCodeGoods().equalsIgnoreCase(item.getCode())) {
                                gid = item;
                                gid.setMeashPosition(returnItem.inPack() ? (byte) 1 : 0);
                                gid.setCountPosition(returnItem.inPack() ? (byte) returnItem
                                        .getCountPack() : (byte) returnItem
                                        .getCount());
                                gid.setWeight(returnItem.getWeight());
                                break;
                            }
                        }
                    }

                    if (gid == null) {
                        byte meash = returnItem.inPack() ? (byte) 1 : 0;
                        int count = returnItem.inPack() ? (byte) returnItem
                                .getCountPack() : (byte) returnItem.getCount();
                        gid = new GoodsInDocument(returnItem.getCodeGoods(), count,
                                meash, 1, returnItem.getPrice(), returnItem.getWeight());
                        mExistedItems.add(gid);
                    }

                    updateAdapterAfterItemsAdd();
                }
                break;
        }
    }

    /**
     * Реакция на клик товара. Здесь в зависимости от точки вход нужно перейти
     * либо в информацию о товаре, либо в диалог добавления товара
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGoodsListEventSelected(Goods goods) {
        if (getArguments().getString(MainActivity.PRICE) != null) {
            getFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.contentMain, GoodsInfoContainer.newInstance(goods, mClient != null && mClient.isForeignAgent()))
                    .commit();
            return;
        }
        switch (mListViewMode) {
            case GoodsAdapter.MODE_ORDER:
                // Сначала проверяем, есть ли такой товар в списке существующих
                // позиций
                GoodsInDocument gid = null;
                if (mExistedItems != null) {
                    for (GoodsInDocument item : mExistedItems) {
                        if (item.getCode().equalsIgnoreCase(goods.getCode())) {
                            gid = item;
                        }
                    }
                }
                OrderItem oi = new OrderItem(goods);
                if (gid == null) {
                    if (mDefaultMeashValue == 0) {// Штуки
                        oi.inPack(false);
                        oi.setCount(1);
                    } else {// Упаковки
                        oi.inPack(true);
                        oi.setCountPack(1);
                    }
                    //Если не указано ранее - берём из заявки
                    if (!App.isMerch()) {
                        oi.isSertificateNeed(Order.getOrder().mSertificates);
                        oi.isQualityDocNeed(Order.getOrder().mQualityDocs);
                    }
                } else {
                    oi.setPrice(gid.getPrice());
                    oi.inPack(gid.getMeashPosition() == 1);
                    if (gid.getMeashPosition() == 0) {// Штуки
                        oi.setCount(gid.getCountPosition());
                    } else {
                        oi.setCountPack(gid.getCountPosition());
                    }
                    //Если указано ранее - берём из значения
                    oi.mIsSpecialPrice = gid.isSpecPrice();
                    oi.isSertificateNeed(gid.isSertificate());
                    oi.isQualityDocNeed(gid.isQualityDoc());
                }
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment fragment = new GoodsQuantityFragment();
                fragment.setTargetFragment(this, IDD_ORDER_QUANTITY);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Goods.KEY, goods);
                bundle.putParcelable(OrderItem.KEY, oi);
                fragment.setArguments(bundle);
                ft.addToBackStack(null);
                ft.replace(R.id.contentMain, fragment);
                ft.commit();
                break;
            case GoodsAdapter.MODE_RETURN:
                // Сначала проверяем, есть ли такой товар в возврате.
                // Если есть, передаём элемент возврата, в противном случае - товар
                gid = null;
                if (mExistedItems != null) {
                    for (GoodsInDocument item : mExistedItems) {
                        if (item.getCode().equalsIgnoreCase(goods.getCode())) {
                            gid = item;
                        }
                    }
                }
                ReturnItem ri = new ReturnItem(goods);

                if (gid == null) {
                    ri.setCount(1);
                } else {
                    //ri.setPrice(gid.getPrice());
                    ri.inPack(gid.getMeashPosition() == 1);
                    if (gid.getMeashPosition() == 0) {// Штуки
                        ri.setCount(gid.getCountPosition());
                    } else {
                        ri.setCountPack(gid.getCountPosition());
                    }
                }
                ft = getFragmentManager().beginTransaction();
                fragment = new ReturnQuantityFragment();
                fragment.setTargetFragment(this, IDD_RETURN_QUANTITY);
                bundle = new Bundle();
                bundle.putParcelable(Goods.KEY, goods);
                bundle.putParcelable(ReturnItem.KEY, ri);
                bundle.putBoolean(KEY_RETURN_FOREIGN, Returns.getReturn().getClient().isForeignAgent());
                fragment.setArguments(bundle);
                ft.addToBackStack(null);
                ft.replace(R.id.contentMain, fragment);
                ft.commit();
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onGoodsListEventFilter(GoodsListEventFilter newValue) {
        mCurrentFilter = newValue.getNewValue();
        updateAdapterAfterItemsAdd();
    }

    /**
     * Обновляем список товаров в существующем фрагменте
     */
    private void startLoadGoodsInGoodsListFragment() {
        byte modeLoadGoods = GoodsList.MODE_ALL;
        switch (mType) {
            case GoodsList.MODE_MATRIX:
                modeLoadGoods = GoodsList.MODE_MATRIX;
                break;
        }
        Fragment old = getFragmentManager().findFragmentById(R.id.detailFragmentContainer);
        String codeCli = mClient != null ? mClient.getCode() : "";
        String mlCat = mClient != null ? mClient.Category() : "";
        try {
            if (old != null)
                ((GoodsListFragment) old).updateItems(mSelectedNode, codeCli,
                        mlCat, GoodsListFragment.modeRadio, modeLoadGoods);
            else {
                old = getFragmentManager().findFragmentById(R.id.fragmentContainer);
                ((GoodsListFragment) old).updateItems(mSelectedNode, codeCli,
                        mlCat, GoodsListFragment.modeRadio, modeLoadGoods);
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCountChanged(OnCountChanged data) {
        Goods goods = data.getGoods();
        int newCount = data.getNewCount();
        if (mExistedItems == null) {
            mExistedItems = new ArrayList<>();
        }
        if (newCount > 0) {
            goods.inDocument(true);

            GoodsInDocument gid = null;
            for (int i = 0; i < mExistedItems.size(); i++) {
                GoodsInDocument item = mExistedItems.get(i);

                if (goods.getCode().contains(item.getCode())) {
                    gid = item;
                    gid.setQuant(goods.getQuant());
                    gid.setCountPosition(newCount);
                    break;
                }
            }
            if (gid == null) {
                gid = new GoodsInDocument(goods.getCode(), newCount,
                        mDefaultMeashValue, goods.getQuant(), goods.price(), goods.getWeight());
                mExistedItems.add(gid);

                //Признаки берём из заявки
                if (Order.getOrder() != null) {
                    gid.isSertificate(Order.getOrder().mSertificates);
                    gid.isQualityDoc(Order.getOrder().mQualityDocs);
                }
            }
        } else {// Если количество равно 0 - удаляем из списка
            goods.inDocument(false);

            for (int i = 0; i < mExistedItems.size(); i++) {
                GoodsInDocument item = mExistedItems.get(i);

                if (goods.getCode().contains(item.getCode())) {
                    mExistedItems.remove(i);
                    break;
                }
            }
        }
        if (App.isMerch()) {
            if (getTargetRequestCode() != VisitContainFragment.IDD_NEW_TASK) {
                ArrayList<MonitoringProduct> itemsList = Monitoring.getCurrentMonitoring().getItemsList();
                for (MonitoringProduct monitoringProduct : itemsList) {
                    if (goods.getCode().equals(monitoringProduct.getCodeProduct())) {
                        monitoringProduct.setCheck(newCount);
                    }
                }
                Monitoring monitoring = Monitoring.getCurrentMonitoring();
                monitoring.addProductItemAll(itemsList);
                monitoring.update(MyApp.getContext());
                EventBus.getDefault().post(new OnCountChangedTotalSize(mExistedItems.size()));
            } else {
                Task currentTask = Task.getCurrentTask();
                ArrayList<GoodsInTask> taskProducts = currentTask.goodsList();
                for (GoodsInDocument inDocument : mExistedItems) {
                    if (inDocument.getCode().equals(goods.getCode())) {
                        taskProducts.add(new GoodsInTask(inDocument.getCode(), goods.getName(), (byte) 0, (short) inDocument.getCountPosition(), (short) 0, inDocument.getQuant()));
                    }
                }
                currentTask.goodsList(taskProducts);
                TaskLab.get(getActivity()).updateTask(currentTask);
                EventBus.getDefault().post(new OnCountChangedTotalSize(mExistedItems.size()));
            }
        }
        updateAdapterAfterItemsAdd();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMeashChanged(OnMeashChanged data) {
        Goods goods = data.getGoods();
        byte newValue = data.getNewValue();
        GoodsInDocument gid = null;
        if (mExistedItems == null) {
            mExistedItems = new ArrayList<>();
        } else {
            for (GoodsInDocument item : mExistedItems) {
                if (goods.getCode().contains(item.getCode())) {
                    gid = item;
                    gid.setMeashPosition(newValue);
                    break;
                }
            }
        }
        if (gid == null) {// Добавляем
            goods.inDocument(true);

            gid = new GoodsInDocument(goods.getCode(), 1, (byte) newValue,
                    goods.getQuant(), goods.price(), goods.getWeight());

            switch (mListViewMode) {
                case GoodsAdapter.MODE_ORDER:
                    gid.isSertificate(Order.getOrder().mSertificates);
                    gid.isQualityDoc(Order.getOrder().mQualityDocs);
                    break;
                case GoodsAdapter.MODE_RETURN:

                    break;
            }

            mExistedItems.add(gid);
        }

        updateAdapterAfterChange();
    }

    private void updateAdapterAfterChange() {
        try {
            Fragment f = getFragmentManager().findFragmentById(
                    R.id.detailFragmentContainer);
            if (f == null) {
                f = getFragmentManager().findFragmentById(
                        R.id.fragmentContainer);
            }
            GoodsListFragment glf = (GoodsListFragment) f;
            if (glf != null) {
                glf.setExistedItems(mExistedItems);
                glf.updateAdapterAfterChange();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void updateAdapterAfterItemsAdd() {
        try {
            Fragment f = getFragmentManager().findFragmentById(
                    R.id.detailFragmentContainer);
            if (f == null) {
                f = getFragmentManager().findFragmentById(
                        R.id.fragmentContainer);
            }
            GoodsListFragment glf = (GoodsListFragment) f;
            if (glf != null) {
                glf.setExistedItems(mExistedItems);
                glf.updateAdapterAfterAddItemsInOrder();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    @Override
    public void onClick(View v) {
        FragmentManager fragmentManager = getFragmentManager();
        switch (v.getId()) {
            case R.id.btn_stat:
//                String fio = GlobalProc.getPref(getActivity(), R.string.pref_nameManager);
//                FirebaseAnalytics.getInstance( getActivity() ).setUserProperty("BOTTOM_BUTTON", fio == null || fio.isEmpty() ? "not authorized" : fio);
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new StatisticsFragment())
                        .commit();
                break;
            case R.id.btn_visit:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new ClientViewPagerContainer())
                        .commit();
                break;
            case R.id.btn_info:
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, new SummaryFragment())
                        .commit();

                break;
        }
    }
}
