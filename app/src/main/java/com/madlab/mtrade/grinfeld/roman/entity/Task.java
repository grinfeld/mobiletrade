package com.madlab.mtrade.grinfeld.roman.entity;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import android.os.Parcel;
import android.os.Parcelable;


public class Task extends TaskObject implements Parcelable{
    private static final String TAG = "#Task";
//	public static String KEY = "com.dalimo.mmerch.task";

    public final static int UNCONFIRMED_TASK = 0;
    public final static int SAVED_TASK = 1;

    public UUID mUUID;
    public String mAgent;
    public String mClientCode;
    public String mClientName;
    public long mCreationDate;
    public byte mCurrentStatus;
    public byte mAvgPercent;
    private Client client;
    private String managerCode;
    public static Task currentTask;
    private int Unconfirmed;

    private boolean reserved;
    private ArrayList<GoodsInTask> mGoods;

    public static Task getCurrentTask() {
        return currentTask;
    }

    public static void setCurrentTask(Task currentTask) {
        Task.currentTask = currentTask;
    }

    public Task(UUID uuid, String agentCode, String clientCode, String description, long dateDue, byte mCurrentStatus, int unconfirmed, boolean reserved) {
        super(0, description, dateDue);

        mUUID = uuid;

        mAgent = agentCode;
        mClientCode = clientCode;
        mCreationDate = System.currentTimeMillis();
        this.mCurrentStatus = mCurrentStatus;
        Unconfirmed = unconfirmed;
        this.reserved = reserved;
        mGoods = new ArrayList<>();
    }

    public Task() {
        this(UUID.randomUUID(), "", "", "", Calendar.getInstance().getTimeInMillis(), (byte) 0, UNCONFIRMED_TASK, false);
    }

    @SuppressWarnings("unchecked")
    public Task(Parcel parcel) {
        //TaskObject
        mDBID = parcel.readInt();
        mDescription = parcel.readString();
        mDateDue = parcel.readLong();
        mImportance = parcel.readByte();
        mStatusHistory = parcel.readArrayList(StatusHistory.class.getClassLoader());

        //Task
        mUUID = UUID.fromString(parcel.readString());
        mAgent = parcel.readString();
        mClientCode = parcel.readString();
        mClientName = parcel.readString();
        mCreationDate = parcel.readLong();
        mCurrentStatus = parcel.readByte();
        mAvgPercent = parcel.readByte();
        Unconfirmed = parcel.readInt();
        //Goods in task list
        reserved =  parcel.readByte() != 0;
        mGoods = parcel.readArrayList(GoodsInTask.class.getClassLoader());
    }

    public ArrayList<GoodsInTask> goodsList() {
        return mGoods;
    }

    public void goodsList(ArrayList<GoodsInTask> list) {
        mGoods = list;
		//recalcAvgPercent();
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public int getUnconfirmed() {
        return Unconfirmed;
    }

    public void setUnconfirmed(int unconfirmed) {
        Unconfirmed = unconfirmed;
    }

    public String getManagerCode() {
        return managerCode;
    }

    public void setManagerCode(String managerCode) {
        this.managerCode = managerCode;
    }

    public byte avgPercent(){
        return mAvgPercent;
    }

    public void avgPercent(byte value){
        mAvgPercent = value;
    }

//	public void recalcAvgPercent(){
//		if (mGoods != null){
//			int sum = 0;
//			for (GoodsInTask item : mGoods) {
//				sum += item.percent();
//			}
//			mAvgPercent = (byte)(sum / mGoods.size());
//		}
//	}

    public String UUID(){
        return mUUID.toString();
    }

    public String agent() {
        return mAgent;
    }

    public void agent(String agentCode) {
        mAgent = agentCode;
    }

    public String description() {
        return mDescription;
    }

    public void description(String text) {
        mDescription = text;
    }

    public String clientCode() {
        return mClientCode;
    }

    public void clientCode(String code) {
        mClientCode = code;
    }

    public String clientName() {
        return mClientName;
    }

    public void clientName(String name) {
        mClientName = name;
    }

    public long dateDue() {
        return mDateDue;
    }

    public byte currentStatus() {
        return mCurrentStatus;
    }

    /**
     * Возвращает дату создания задачи.
     * @return
     */
    public long dateCreation(){
        return mCreationDate;
    }

    public void importance(byte newValue){
        mImportance = newValue;
    }

    public byte importance(){
        return mImportance;
    }

    public void changeStatus(byte newStatusIndex){
        mCurrentStatus = newStatusIndex;

        StatusHistory sh = new StatusHistory();
        sh.mStatusIndex = newStatusIndex;
        mStatusHistory.add(sh);
    }

    public void addCreationDate(long creation){
        mCreationDate = creation;

        StatusHistory sh = new StatusHistory(creation, (byte)0);
        mStatusHistory.add(sh);
    }

    public void dueDate(long newValue) {
        mDateDue = newValue;
    }

    public StatusHistory lastStatus(){
        if (mStatusHistory != null && mStatusHistory.size() > 0){
            return mStatusHistory.get(mStatusHistory.size() - 1);
        }

        return null;
    }

    public byte lastStatusIndex(){
        if (mStatusHistory != null && mStatusHistory.size() > 0){
            return mStatusHistory.get(mStatusHistory.size() - 1).mStatusIndex;
        }

        return StatusHistory.ZERO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        //TaskObject
        parcel.writeInt(mDBID);
        parcel.writeString(mDescription);
        parcel.writeLong(mDateDue);
        parcel.writeByte(mImportance);
        parcel.writeList(mStatusHistory);

        //Task
        parcel.writeString(mUUID.toString());
        parcel.writeString(mAgent);
        parcel.writeString(mClientCode);
        parcel.writeString(mClientName);
        parcel.writeLong(mCreationDate);
        parcel.writeByte(mCurrentStatus);
        parcel.writeByte(mAvgPercent);
        parcel.writeInt(Unconfirmed);
        parcel.writeByte((byte)( reserved ?  1 : 0));
        //Goods in task
        parcel.writeList(mGoods);
    }

    public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>() {

        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
