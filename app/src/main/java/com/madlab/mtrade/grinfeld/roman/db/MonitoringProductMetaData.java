package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */
public class MonitoringProductMetaData {


    public static final String TABLE_NAME = "MonitoringProduct";

    public static final String FIELD_ID_MONITORING = "id_monitoring";
    public static final String FIELD_CODE_PRODUCT = "CodeProduct";
    public static final String FIELD_CHECK_PRODUCT = "ProductCheck";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	integer,"
                    + "%s	nvarchar(6) NOT NULL DEFAULT '',"
                    + "%s	integer default 1)", TABLE_NAME,
            FIELD_ID_MONITORING, FIELD_CODE_PRODUCT, FIELD_CHECK_PRODUCT);

    public static final String INDEX = String.format(
            "CREATE INDEX idxMonitoringId ON %s ( %s )", TABLE_NAME,
            FIELD_ID_MONITORING);

    public static String Clear(int numOrder) {
        return String.format(Locale.getDefault(),"DELETE FROM %s WHERE %s = %d", TABLE_NAME,
                FIELD_ID_MONITORING, numOrder);
    }

}
