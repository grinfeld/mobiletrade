package com.madlab.mtrade.grinfeld.roman.connectivity;

/**
 * Created by GrinfeldRA on 12.07.2018.
 */

public class DalimoRemoteException extends Exception {
    public DalimoRemoteException(String message) {
        super(message);
    }
}
