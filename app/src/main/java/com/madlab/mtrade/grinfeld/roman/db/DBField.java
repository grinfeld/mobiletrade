package com.madlab.mtrade.grinfeld.roman.db;

public final class DBField {

	public DBField(String name, int index) {
		Name = name;
		Index = index;
	}

	public String Name;
	public int Index;

	@Override
	public String toString() {
		return Name;
	}
}
