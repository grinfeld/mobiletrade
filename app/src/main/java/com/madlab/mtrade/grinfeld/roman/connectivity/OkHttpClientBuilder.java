package com.madlab.mtrade.grinfeld.roman.connectivity;

import android.util.Log;
import android.util.Pair;

import org.apache.commons.net.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.TlsVersion;

/**
 * Created by GrinfeldRa on 12.04.2018.
 */

public class OkHttpClientBuilder {

    private static final String TAG = "#OkHttpClientBuilder";
    private static final String URL_CHAT = "https://push.dalimo.ru/api/v1/";
    private static final String LOGIN = "mobile_dev@dalimo.ru";
    private static final String PASSWORD = "mobile_dev!23";
    private static final String URL_MTRADE_PROXY = "https://mtrade.dalimo.org/api/v1/";//call/

    public static Call buildCall(String function, String values, String region) {
        Log.d(TAG, "function: " + function + ", values: " + values + ", region: " + region);
//        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                .tlsVersions(TlsVersion.TLS_1_2)
//                .cipherSuites(CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256)
//
//                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                //.connectionSpecs(Collections.singletonList(spec))
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS).build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("command", "COMMAND")
                .add("function", function)
                .add("query", values);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url("https://mtrade.dalimo.org/api/v1/call/" + region)
                .addHeader("x-app-id", "8rRYkPqv4rMm9XLnK9Mt")
                .addHeader("x-app-KEY_TYPE", "E2DKtaxAWBpufa6xZgC8bhnwjBY6URXe4vkjHQhXQExdMLzw")
                .addHeader("Content-type", "application/json")
                .post(formBody)
                .build();

        return client.newCall(request);
    }


    public static Call buildLogin() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS).build();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("email", LOGIN)
                .add("password", PASSWORD);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url(URL_CHAT + "login/")
                .post(formBody)
                .build();
        return client.newCall(request);
    }

    private static OkHttpClient getOkHttpClient() {
//        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                .tlsVersions(TlsVersion.TLS_1_1, TlsVersion.TLS_1_2)
//                .cipherSuites(CipherSuite.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
//                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
//                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
//                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
//                        CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384)
//                .build();
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .addHeader("x-app-id", "8rRYkPqv4rMm9XLnK9Mt")
                            .addHeader("x-app-KEY_TYPE", "E2DKtaxAWBpufa6xZgC8bhnwjBY6URXe4vkjHQhXQExdMLzw")
                            .addHeader("Content-type", "application/json")
                            //.method(original.method(), original.body())
                            .build();
                    return chain.proceed(request);
                })
                //.connectionSpecs(Collections.singletonList(spec))
                .connectTimeout(10, TimeUnit.SECONDS).build();

        return client;
    }

    public static Call getFiles(String city, String manager) {
        Log.d(TAG, URL_MTRADE_PROXY + "file/manager/" + city + "/" + manager);
        Request request = new Request.Builder()
                .url(URL_MTRADE_PROXY + "file/manager/" + city + "/" + manager)
                .get()
                .build();
        return getOkHttpClient().newCall(request);
    }


    public static Call buildNotifications(String token, String code, String city) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();
        Request request = new Request.Builder()
                .url(URL_CHAT + "notifications?code=" + code + "&city=" + city)
                .addHeader("Authorization", "Bearer " + token)
                .addHeader("Content-type", "application/json")
                .get()
                .build();
        return client.newCall(request);
    }

    public static Call buildNotificationChangeRead(int id, String token) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS).build();
        String json = "{\"read\": true}";
        RequestBody formBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(URL_CHAT + "notifications/" + id)
                .addHeader("Authorization", "Bearer " + token)
                .addHeader("Content-type", "application/json")
                .patch(formBody)
                .build();
        return client.newCall(request);
    }

    public static Call buildNewClient(String city, String code, String tokenFirebase, String name, String token) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS).build();
        String json = "{\"city\": \"" + city + "\",\"code\": \"" + code + "\",\"tokenId\": \"" + tokenFirebase + "\",\"name\": \"" + name + "\"}";
        RequestBody formBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(URL_CHAT + "clients")
                .addHeader("Authorization", "Bearer " + token)
                .addHeader("Content-type", "application/json")
                .post(formBody)
                .build();
        return client.newCall(request);
    }

    public static Call buildOrderHelpDesk(String cat, String theme, String description, String codeAgent, String city, String domain) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS).build();
        JSONObject ticketData = new JSONObject();
        try {
            ticketData.put("category", cat);
            ticketData.put("subject", theme);
            ticketData.put("message", description);
            JSONObject query = new JSONObject();
            query.put("method", "helpdesk.ticket.new");
            query.put("user", String.format(domain + "_%s", codeAgent));
            query.put("ticket", ticketData);
            query.put("city", city);
            RequestBody formBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), query.toString());
            byte[] encodedBytes = Base64.encodeBase64("pda:O*^&ID*Xcukuslba:libKVTI&^FGO>LWFncbp d4o87f;".getBytes());
            String USER_PASS = new String(encodedBytes);
            Request request = new Request.Builder()
                    .url("https://intranet.dalimo.ru/api/mobile/")
                    .addHeader("Content-type", "application/json")
                    .addHeader("Authorization", "Basic " + USER_PASS)
                    .post(formBody)
                    .build();
            return client.newCall(request);
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
        return null;
    }

}
