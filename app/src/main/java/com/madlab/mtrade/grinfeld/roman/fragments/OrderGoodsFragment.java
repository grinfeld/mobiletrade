package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.adapters.OrderGoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.components.DatePickerFragment;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.DBProp;
import com.madlab.mtrade.grinfeld.roman.db.ExcludedMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ReturnMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.ManagersMerch;
import com.madlab.mtrade.grinfeld.roman.entity.Monitoring;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;

import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.eventbus.DateChanged;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnCountChangedTotalSize;
import com.madlab.mtrade.grinfeld.roman.iface.IAskDiscountComplete;
import com.madlab.mtrade.grinfeld.roman.iface.IAutoOrderComplete;
import com.madlab.mtrade.grinfeld.roman.iface.ICheckRestComplete;

import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IGetLimitTask;
import com.madlab.mtrade.grinfeld.roman.iface.IGetMinAmountOrder;
import com.madlab.mtrade.grinfeld.roman.iface.IGetSizePromotionGoodsTask;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveTaskComplete;
import com.madlab.mtrade.grinfeld.roman.tasks.AskDiscountTask;
import com.madlab.mtrade.grinfeld.roman.tasks.AutoOrderTask;
import com.madlab.mtrade.grinfeld.roman.tasks.CheckRestTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetLimitsTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetMinAmountOrder;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadImageTask;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveTask;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static android.app.Activity.RESULT_OK;
import static com.madlab.mtrade.grinfeld.roman.fragments.OrderCommonFragment.ACTIONS;


/**
 * Created by GrinfeldRA on 08.11.2017.
 */

public class OrderGoodsFragment extends ListFragment implements View.OnClickListener, IAutoOrderComplete, IAskDiscountComplete, ICheckRestComplete, IGetLimitTask, IGetMinAmountOrder, IGetSizePromotionGoodsTask, ISetReserveTaskComplete {


    public static final String TAG = "#OGFragment";
    private static final int IDD_ADD = 1;
    private static final int IDD_MODIFY = 2;

    private final static byte DOC_NEW = 21;
    private final static byte DOC_EDIT = 22;
    private final static byte DOC_BACK = 23;
    private short docNumForEdit = 0;
    private Handler mHandler;
    private byte currentDocMode = DOC_NEW;
    private boolean flagForCompleteAskDiscount = false;

    BroadcastReceiver mReceiver;
    ProgressBar progressBar;
    ListView lv;
    TextView tvTotAmount;
    TextView tvTotCount;
    TextView tvTotWeight;
    TextView tvLimit;
    EditText etDescriptionTask;
    private OrderGoodsAdapter mAdapter;
    Button btOk, btn_get_date;
    private String[] mGroupToExcludeList = null;
    private boolean[] choices = null;
    private ArrayList<String> mExcludeList = null;
    private AutoOrderTask mAutoOrderTask = null;
    private short currentSelectedItemIndex = -1;
    private Activity context;
    View view_traffic_light;
    TextView txt_min_amount_order;
    TextView txt_amount_order_today;
    Spinner spinner_managers;
    TextView tvCurCredit;


    @Override
    public void onStart() {
        super.onStart();
        Order data = Order.getOrder();
        if (data != null) {
            data.setTotalsChangedListener(mTotalsChangedListener);
            fillBasement(data.getUncheckedCount(), data.getTotalSumUnchecked(), data.getTotalWeightUnchecked());
        }
    }

    @Override
    public void onResume() {
        EventBus.getDefault().register(this);
        super.onResume();
    }

    private void showDatePickerDialog(View view, Date start) {
        DialogFragment newFragment = new DatePickerFragment(view, start);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        GlobalProc.closeKeyboard(context);
        super.onDestroyView();
    }


    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ((IDynamicToolbar) getActivity()).onChangeToolbar(null);
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_order_goods, container, false);
            context = getActivity();
            setHasOptionsMenu(true);
            mHandler = new Handler(Looper.getMainLooper());
            tvTotAmount = v.findViewById(R.id.og_lbTotalAmount);
            tvTotCount = v.findViewById(R.id.og_lbTotalCount);
            tvTotWeight = v.findViewById(R.id.og_lbTotalWeight);
            progressBar = v.findViewById(R.id.progress_bar_order_goods);
            lv = v.findViewById(android.R.id.list);
            registerForContextMenu(lv);
            tvLimit = v.findViewById(R.id.og_lbLimit);
            view_traffic_light = v.findViewById(R.id.view_traffic_light);
            txt_min_amount_order = v.findViewById(R.id.txt_min_amount_order);
            txt_amount_order_today = v.findViewById(R.id.txt_amount_order_today);
            tvCurCredit = v.findViewById(R.id.og_lbCurCredit);
            Button btCancel = v.findViewById(R.id.og_btCancel);
            Button btAdd = v.findViewById(R.id.og_btAddGoods);
            btOk = v.findViewById(R.id.og_btOk);
            btOk.setOnClickListener(this);
            btCancel.setOnClickListener(this);
            if (App.isMerch()) {
                Monitoring monitoring = Monitoring.getCurrentMonitoring();
                Task task = Task.getCurrentTask();
                Client client = getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK ? task.getClient() : monitoring.getClient();
                String codeManager = getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK ?
                        task.getManagerCode() : monitoring.getCodeManager();
                Date date = getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK ?
                        new Date(task.dateDue()) : monitoring.getDate();
                int visibility = getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK ? View.VISIBLE : View.GONE;
                ((TextView) v.findViewById(R.id.tvClientName)).setText(client.mNameFull);
                v.findViewById(R.id.textLayoutInputTask).setVisibility(visibility);
                etDescriptionTask = v.findViewById(R.id.etDescriptionTask);
                v.findViewById(R.id.contentMainMerch).setVisibility(View.VISIBLE);
                v.findViewById(R.id.merch_view).setVisibility(View.VISIBLE);
                v.findViewById(R.id.mtrade_view).setVisibility(View.GONE);
                lv.setVisibility(View.GONE);
                TextView textView = v.findViewById(R.id.txt_label_total);
                textView.setText("Выбрано: ");
                v.findViewById(R.id.weight_view).setVisibility(View.GONE);
                v.findViewById(R.id.amount_view).setVisibility(View.GONE);
                btn_get_date = v.findViewById(R.id.btn_get_date);
                spinner_managers = v.findViewById(R.id.spinner_managers);
                btn_get_date.setOnClickListener(this);
                btAdd.setVisibility(View.GONE);
                MyApp app = (MyApp) context.getApplication();
                SQLiteDatabase db = app.getDB();
                List<ManagersMerch> managersMerchList = ManagersMerch.load(db, client.getCode());
                ArrayAdapter<ManagersMerch> adapter =
                        new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, managersMerchList);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
                spinner_managers.setAdapter(adapter);
                spinner_managers.setTag(managersMerchList);
                if (codeManager != null) {
                    int i;
                    for (i = 0; i < managersMerchList.size(); i++) {
                        if (managersMerchList.get(i).getmCode().equals(codeManager)) {
                            try {
                                spinner_managers.setSelection(i);
                            } catch (Exception e) {

                            }
                        }
                    }
                } else {
                    try {
                        if (getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK) {
                            task.setManagerCode(managersMerchList.get(0).getmCode());
                        } else {
                            monitoring.setCodeManager(managersMerchList.get(0).getmCode());
                        }
                    } catch (Exception e) {

                    }
                }
                spinner_managers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        ManagersMerch managersMerch = (ManagersMerch) adapterView.getAdapter().getItem(i);
                        if (getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK) {
                            task.setManagerCode(managersMerch.getmCode());
                            TaskLab.get(context).updateTask(task);
                        } else {
                            monitoring.setCodeManager(managersMerch.getmCode());
                            monitoring.update(context);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                if (date != null) {
                    setButtonDateShipText(TimeFormatter.sdfDate.format(date));
                } else {
                    String time = TimeFormatter.sdfDate.format(new Date());
                    setButtonDateShipText(time);
                }
                startMerchMatrix(GoodsList.MODE_MATRIX, null, GoodsAdapter.MODE_ORDER, client);
            } else {
                v.findViewById(R.id.contentMainMerch).setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                v.findViewById(R.id.mtrade_view).setVisibility(View.VISIBLE);
                v.findViewById(R.id.merch_view).setVisibility(View.GONE);
                try {
                    if (Order.getOrder() == null) {
                        Order.loadUnsavedOrder(context);
                    }

                    btCancel.setOnClickListener(this);
                    btAdd.setOnClickListener(this);
                    setMode();
                    printClientData(v);
                    setAdapter();
                    mGroupToExcludeList = loadNames();
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                    GlobalProc.mToast(context, e.toString());
                    //_setResult(RESULT_OK);
                }
            }
        }
        return v;
    }

    GetMinAmountOrder getMinAmountOrder;
    GetLimitsTask getLimitsTask;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prepareGetLimitsTask();
        prepareGetMinAmountOrderTask();
        loadDebt();
    }

    private void loadDebt() {
        String code = Order.getOrder().getClient().getCode();
        OkHttpClientBuilder.buildCall("ПолучитьЗадолженностьПоКонтрагенту", code, App.getRegion(context)).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String param = jsonObject.getString("data");
                        String s = param.split(":")[1].replaceAll("[\"}]", "");
                        Activity activity = getActivity();
                        if (activity != null && isAdded()) {
                            mHandler.post(() -> {
                                double debt = 0;
                                if (!s.isEmpty()) {
                                    debt = Double.parseDouble(s.replace(",", "."));
                                }
                                String s1 = GlobalProc.toForeignPrice((float) debt, Order.getOrder().getClient().isForeignAgent());
                                if (debt > 0) {
                                    tvCurCredit.setTextColor(Color.parseColor("#ff0000"));
                                    tvCurCredit.setText(getString(R.string.stop_ship, s1));
                                } else {
                                    tvCurCredit.setText(getString(R.string.debt, s1));
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        if (getMinAmountOrder != null && getMinAmountOrder.getStatus() == AsyncTask.Status.RUNNING) {

            getMinAmountOrder.cancel(true);
        }
        if (getLimitsTask != null && getLimitsTask.getStatus() == AsyncTask.Status.RUNNING) {
            getLimitsTask.cancel(true);
        }
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    private void prepareGetMinAmountOrderTask() {
        Order data = Order.getOrder();
        if (data != null && data.getClient() != null) {
            App app = App.get(context);
            if (app.connectionServer.equals("1")) {
                startGetMinAmountOrderTask(data, null);
            } else {
                Date orderTime = Order.getOrder().getDateShip();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
                String date = format.format(orderTime);
                OkHttpClientBuilder.buildCall("МинимальнаяСуммаЗаказа",
                        date + ";" + data.getClient().getCode(), App.getRegion(context)).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        mHandler.post(() -> startGetMinAmountOrderTask(data, null));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String strResponse = response.body().string();
                        mHandler.post(() -> {
                            try {
                                Log.d(TAG, "МинимальнаяСуммаЗаказа " + strResponse);
                                JSONObject jsonObject = new JSONObject(strResponse);
                                String param = jsonObject.getString("data");
                                mHandler.post(() -> startGetMinAmountOrderTask(data, param));
                            } catch (JSONException e) {
                                mHandler.post(() -> startGetMinAmountOrderTask(data, null));
                            }
                        });
                    }
                });
            }
        }
    }

    private void startGetMinAmountOrderTask(Order data, String param) {
        getMinAmountOrder = new GetMinAmountOrder(context, this, data.getClient().getCode(), App.get(context).getCodeAgent());
        getMinAmountOrder.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, param);
    }

    private void prepareGetLimitsTask() {
        Order data = Order.getOrder();
        if (data != null && data.getClient() != null) {
            if (App.get(MyApp.getContext()).connectionServer.equals("1")) {
                startGetLimitTask(data, null);
            } else {
                OkHttpClientBuilder.buildCall("ПроверкаОстаткаТоварногоЛимита",
                        App.get(context).getCodeAgent() + ";" + data.getClient().getCode() + ";", App.getRegion(context)).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        mHandler.post(() -> {
                            startGetLimitTask(data, null);
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        String strResponse = response.body().string();
                        mHandler.post(() -> {
                            try {
                                Log.d(TAG, "ПроверкаОстаткаТоварногоЛимита " + strResponse);
                                JSONObject jsonObject = new JSONObject(strResponse);
                                String param = jsonObject.getString("data");
                                mHandler.post(() -> startGetLimitTask(data, param));
                            } catch (JSONException e) {
                                mHandler.post(() -> startGetLimitTask(data, null));
                            }
                        });
                    }
                });
            }
        }
    }


    private void startGetLimitTask(Order data, String param) {
        getLimitsTask = new GetLimitsTask(context, this, App.get(context).getCodeAgent(), data.getClient().getCode());
        getLimitsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, param);
    }

    private void setMode() {
        String caller = getArguments().getString(ACTIONS);
        if (caller == null)
            return;
        String newInt = getString(R.string.intent_new_order_goods);
        if (caller.equalsIgnoreCase(newInt)) {
            currentDocMode = DOC_NEW;
        } else {
            currentDocMode = DOC_EDIT;
            docNumForEdit = Order.getOrder().getNumOrder();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = context.getMenuInflater();
        inflater.inflate(R.menu.order_goods_context_menu, menu);
        MenuItem.OnMenuItemClickListener listener = item -> {
            onContextItemSelected(item);
            return true;
        };
        for (int i = 0, n = menu.size(); i < n; i++) {
            menu.getItem(i).setOnMenuItemClickListener(listener);
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        currentSelectedItemIndex = (short) position;
        if (Order.getOrder() != null) {
            OrderItem item = Order.getOrder().getItem(position);
            if (item != null) {
                item.toggle();
                notifyAdapter();
            }
        }
    }

    private String[] loadNames() {
        String sql = String.format("SELECT DISTINCT %s " + "FROM %s",
                ExcludedMetaData.FIELD_NAME, ExcludedMetaData.TABLE_NAME);

        SQLiteDatabase db;
        Cursor rows = null;
        String[] result = null;
        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();

            rows = db.rawQuery(sql, null);

            if (rows.getCount() > 0) {
                result = new String[rows.getCount()];
                choices = new boolean[rows.getCount()];
                int columnIndexName = rows
                        .getColumnIndex(ExcludedMetaData.FIELD_NAME);
                byte i = 0;
                while (rows.moveToNext()) {
                    result[i] = rows.getString(columnIndexName);
                    choices[i] = true;
                    i++;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = null;
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (App.isMerch()) {
            return;
        }
        inflater.inflate(R.menu.order_gods, menu);
        if (Order.getOrder() != null && Order.getOrder().quality() == Order.QUALITY_NON_LIQUID) {
            menu.findItem(R.id.action_remainder_and_discounts).setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_auto_ticket:
                if (Order.getOrder() != null) {
//                    if (Order.getOrder().getItemsCount() > 0) {
//                        autoOrderConfirmDlg().show();
//                    } else {
                    if (App.get(context).mAutoOrder != null && App.get(context).mAutoOrder.length > 0) {
                        m_auto_order_type().show();
                    } else {
                        if (mGroupToExcludeList != null && mGroupToExcludeList.length > 0) {
                            m_auto_order(mGroupToExcludeList).show();
                        } else {
                            startAutoOrder();
                        }
                    }
                    //}
                }
                return true;
            case R.id.action_remainder_and_discounts:
                prepareAskDiscountTaskStart(true);
                break;
            case R.id.action_goods_limit:
                prepareGetLimitsTask();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        try {
            AdapterView.AdapterContextMenuInfo aMenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            currentSelectedItemIndex = (short) aMenuInfo.position;
            OrderItem orderItem;
            if (Order.getOrder() != null) {
                orderItem = Order.getOrder().getItem(currentSelectedItemIndex);
            } else {
                return false;
            }
            if (orderItem == null)
                return false;
            switch (item.getItemId()) {
                case R.id.menuOrderGoodsList_auto:
                    autoOrderConfirmDlg().show();
                    break;
                // ---------------------------------
                case R.id.menuOrderGoodsList_askDiscount:
                    prepareAskDiscountTaskStart(true);
                    break;
                case R.id.menuOrderGoodsList_choose:
                    Node selected = null;
                    try {
                        MyApp app = (MyApp) context.getApplicationContext();
                        Goods g = Goods.load(app.getDB(), orderItem.getCodeGoods());
                        selected = Node.load(app.getDB(), g.getParentCode());
                    } catch (Exception e) {
                        Log.e(TAG, e.toString());
                    }
                    startGoodsSelectFragment(GoodsList.MODE_ALL, selected, GoodsAdapter.MODE_ORDER, Order.getOrder().getClient());
                    break;
                // ---------------------------------
                case R.id.menuOrderGoodsList_edit:
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    Fragment fragmentModify = new GoodsQuantityFragment();
                    fragmentModify.setTargetFragment(this, IDD_MODIFY);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(OrderItem.KEY, orderItem);
                    MyApp app = (MyApp) context.getApplicationContext();
                    Goods g = Goods.load(app.getDB(), orderItem.getCodeGoods());
                    //bundle.putParcelable(Goods.KEY, g);
                    fragmentModify.setArguments(bundle);
                    ft.addToBackStack(null);
                    ft.replace(R.id.contentMain, fragmentModify);
                    ft.commit();
                    break;
                // ---------------------------------
                case R.id.menuOrderGoodsList_delete: {
                    if (Order.getOrder() != null) {
                        Order.getOrder().deleteChecked(false);
                        notifyAdapter();
                        if (Order.getOrder().getItems().size() == 0) {
                            btOk.setEnabled(false);
                        }
                    }
                }
                break;
                // ---------------------------------
                case R.id.menuOrderGoodsList_info: {
                    app = (MyApp) context.getApplication();
                    g = Goods.load(app.getDB(), orderItem.getCodeGoods());
                    Fragment fragment = GoodsInfoContainer.newInstance(g, Order.getOrder().getClient().isForeignAgent());
                    getFragmentManager().beginTransaction()
                            .replace(R.id.contentMain, fragment)
                            .addToBackStack(null)
                            .commit();
                }
                break;
                // ---------------------------------
                case R.id.menuOrderGoodsList_image: {
                    app = (MyApp) context.getApplication();
                    g = Goods.load(app.getDB(), orderItem.getCodeGoods());
                    showImage(g);
                }
                break;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, e.toString());
        }
        return true;
    }


    private void checkOrderTaskStart() {
        Order data = Order.getOrder();
        if (data != null && data.getClient() != null && data.getItemsCount() > 0) {
            data.clearMark();
            waitCursor(true);
            CheckRestTask checkRestTask = new CheckRestTask(context, data.getClient().getCode(), data.getItems(), this);
            checkRestTask.execute();
        }
    }

    public void showImage(Goods goods) {
        final Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_image);
        final ImageView iView = builder.findViewById(R.id.imageViewDialog);
        final ImageButton imageButton = builder.findViewById(R.id.button_close);
        final ProgressBar progressBarDialog = builder.findViewById(R.id.progress_bar_dialog);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });
        String code = goods.getCode();
        StringBuilder sb = new StringBuilder(GlobalProc.getPref(context,
                R.string.pref_catalog_url, Const.DEFAULT_CATALOG_URL));
        if (code.length() > 0) {
            String base = new String(Base64.encode(code.getBytes(), Base64.NO_WRAP));
            sb.append(String.format("%s&id=%s&is_iframe=1", base, code));
            progressBarDialog.setVisibility(View.VISIBLE);
            LoadImageTask loadImageTask = new LoadImageTask();
            loadImageTask.setListener((thumbnail, error) -> {
                if (thumbnail != null) {
                    if (iView != null) {
                        iView.setImageBitmap(thumbnail);
                    }
                } else {
                    GlobalProc.mToast(context, "7 " + error);
                }
                progressBarDialog.setVisibility(View.GONE);
            });
            loadImageTask.execute(sb.toString());
        }
        builder.show();
    }


    private String generateQueryAskDiscount(Order data) {
        StringBuilder query = new StringBuilder(String.format(Locale.ENGLISH, "%d;%s;", 999, data.getClient().getCode()));
        for (OrderItem oi : data.getItems()) {
            //if (oi.getIsCustom() != 1 && !oi.perishable()) {
            float cnt;
            // Если товар весовой, то переводим в кг
            // если штучный, переводим коробки в штуки
            if (oi.mIsWeightGoods) {
                cnt = oi.getTotalWeight();
            } else {
                cnt = oi.getCountInPieces();
            }
            query.append(String.format(Locale.ENGLISH, "%s#%.2f|",
                    oi.getCodeGoods(), cnt));
            //}
        }
        query.append(";");
        return query.toString();
    }

    private void prepareAskDiscountTaskStart(boolean startIsMenu) {
        Order data = Order.getOrder();
        if (data != null && data.getClient() != null && data.getItemsCount() > 0) {
            data.clearMark();
            waitCursor(true);
            if (App.get(MyApp.getContext()).connectionServer.equals("1")) {
                startAskDiscountTaskStart(data, null, startIsMenu);
            } else {
                String param = generateQueryAskDiscount(data);
                Log.d(TAG, "param: " + param);
                OkHttpClientBuilder.buildCall("РасчетСкидки", param,
                        App.getRegion(MyApp.getContext())).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        mHandler.post(() -> startAskDiscountTaskStart(data, null, startIsMenu));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            String strResponse = response.body().string();
                            Log.d(TAG, "РасчетСкидки " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String param = jsonObject.getString("data");
                            mHandler.post(() -> startAskDiscountTaskStart(data, param, startIsMenu));
                        } catch (JSONException e) {
                            mHandler.post(() -> startAskDiscountTaskStart(data, null, startIsMenu));
                        }
                    }
                });
            }
        }
    }

    private void startAskDiscountTaskStart(Order data, String param, boolean startIsMenu) {
        AskDiscountTask askDiscountTask = new AskDiscountTask(context, data.getClient().getCode(), data.getItems(), this);
        askDiscountTask.setStartIsMenu(startIsMenu);
        askDiscountTask.execute(param);
    }

    protected Dialog autoOrderConfirmDlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.cap_confirm));
        builder.setIcon(R.mipmap.main_icon);
        builder.setMessage(getString(R.string.mes_clear_order_confirm));
        builder.setPositiveButton(getString(R.string.bt_continue),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        m_auto_order_type().show();
                    }
                });
        builder.setNegativeButton(getString(R.string.bt_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.setCancelable(true);
        return builder.create();
    }


    protected Dialog m_auto_order_type() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.cap_autoOrder_type));
        builder.setIcon(R.mipmap.main_icon);

        String[] array = App.get(context).mAutoOrder;
        builder.setItems(array, (dialog, which) -> {
            App.get(context).mAutoOrderSelectedIndex = (byte) which;
            if (mGroupToExcludeList != null && mGroupToExcludeList.length > 0) {
                m_auto_order(mGroupToExcludeList).show();
            } else {
                startAutoOrder();
            }
        });
        return builder.create();
    }

    protected Dialog m_auto_order(String[] array) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.cap_exclude_groups));
        builder.setIcon(R.mipmap.main_icon);

        builder.setMultiChoiceItems(array, choices, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                choices[which] = isChecked;
            }
        });

        builder.setPositiveButton(getString(R.string.bt_autoOrder), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startAutoOrder();
            }
        });

        return builder.create();
    }

    private void startAutoOrder() {
        mExcludeList = loadExcludeList(mGroupToExcludeList, choices);
        Order data = Order.getOrder();
        if (data != null) {
            prepareAutoorderTask(mExcludeList,
                    data.mSertificates, data.mQualityDocs);
        }
    }

    private void startAutoorderTask(App settings, ArrayList<String> excludeList, boolean sert, boolean qualDoc, String param) {
        mAutoOrderTask = new AutoOrderTask(context,
                settings.getCodeAgent(), Order.getOrder().getClient().getCode(),
                settings.mAutoOrder[settings.mAutoOrderSelectedIndex], sert, qualDoc);
        mAutoOrderTask.setListener(this);
        mAutoOrderTask.setExcludeList(excludeList);
        mAutoOrderTask.execute(param);
    }

    private void prepareAutoorderTask(final ArrayList<String> excludeList,
                                      boolean sert, boolean qualDoc) {
        waitCursor(true);
        App settings = App.get(context);
        if (settings.mAutoOrder != null && settings.mAutoOrderSelectedIndex > -1
                && Order.getOrder() != null
                && Order.getOrder().getClient() != null) {
            waitCursor(true);
            if (settings.connectionServer.equals("1")) {
                startAutoorderTask(settings, excludeList, sert, qualDoc, null);
            } else {
                String command = String.format("%s;%s;%s;",
                        settings.getCodeAgent(), Order.getOrder().getClient().getCode(),
                        settings.mAutoOrder[settings.mAutoOrderSelectedIndex]);
                String region = App.getRegion(MyApp.getContext());
                OkHttpClientBuilder.buildCall("ПолучитьАвтозаказ", command, region).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        mHandler.post(() ->
                                startAutoorderTask(settings, excludeList, sert, qualDoc, null));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            String strResponse = response.body().string();
                            Log.d(TAG, "Автозаказ " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String data = jsonObject.getString("data");
                            mHandler.post(() -> startAutoorderTask(settings, excludeList, sert, qualDoc, data));
                        } catch (JSONException e) {
                            mHandler.post(() -> startAutoorderTask(settings, excludeList, sert, qualDoc, null));
                        }
                    }
                });
            }
        } else {
            GlobalProc.mToast(context, "Не удалось загрузить элементы автозаказа, попройбуйте еще раз");
        }
    }


    private ArrayList<String> loadExcludeList(final String[] groupsNameList, final boolean[] choices) {
        ArrayList<String> result = null;
        boolean first = true;
        boolean second = true;
        boolean flag = false;
        String where = "";
        if (choices != null && choices.length > 0) {
            for (int i = 0; i < groupsNameList.length; i++) {
                boolean item = choices[i];
                first = first && !item;
                second = second && item;
                if (item) {
                    if (flag) {
                        where = where.concat(String.format("OR %s = %d ", ExcludedMetaData.FIELD_SORT_ORDER, i));
                    } else {
                        flag = true;
                        where = String.format(Locale.getDefault(), "WHERE %s = %d ",
                                ExcludedMetaData.FIELD_SORT_ORDER, i);
                    }
                }
            }
        } else {
            first = false;
            second = false;
        }

        String sql;
        if (first) {
            // ok
            sql = "";
        } else {
            if (second) {
                // load all records from table Exclude
                sql = String.format("SELECT * FROM %s",
                        ExcludedMetaData.TABLE_NAME);
            } else {
                // load custom records from table Exclude
                sql = String.format("SELECT * FROM %s ",
                        ExcludedMetaData.TABLE_NAME);
                sql = sql.concat(where);
            }
        }

        Cursor rows = null;
        try {
            MyApp app = (MyApp) context.getApplication();
            SQLiteDatabase db = app.getDB();

            if (sql.length() > 0) {
                rows = db.rawQuery(sql, null);
                Log.d(TAG, "codeColumnNum: " + sql);
                if (rows != null && rows.getCount() > 0) {
                    int codeColumnNum = rows
                            .getColumnIndex(ExcludedMetaData.FIELD_CODE_GOODS);
                    result = new ArrayList<>();
                    while (rows.moveToNext()) {
                        result.add(rows.getString(codeColumnNum));
                        Log.d(TAG, "codeColumnNum: " + rows.getString(codeColumnNum));
                    }
                }
            }
        } catch (Exception e) {
            Log.e("loadExcludeList", e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }



    private void setAdapter() {
        if (App.isMerch())
            return;
        try {
            Order data = Order.getOrder();
            if (lv != null && data != null) {
                mAdapter = new OrderGoodsAdapter(context, R.layout.item_order_goods, data);
                lv.setAdapter(mAdapter);
//                if (data.hasChecked() == -1) {
//                    btOk.setEnabled(false);
//                } else {
//                    btOk.setEnabled(true);
//                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            GlobalProc.mToast(context, "setAdapter " + e.toString());
        }
    }

    // При изменении итогов - выводим на форму
    private Order.ITotalsChangedListener mTotalsChangedListener = new Order.ITotalsChangedListener() {
        @Override
        public void onTotalsChanged(short count, float totalSum,
                                    float totalWeight, float totalSumUncheked,
                                    float totalWeightUncheked) {
            if (currentDocMode == DOC_NEW) {
                // Если документ новый, то сохраняем данные заявки, чтобы в
                // случае падения не потерять
                Order.updateUnsavedOrder(context);
            }
            flagForCompleteAskDiscount = false;
            fillBasement(count, totalSumUncheked, totalWeightUncheked);
        }
    };


    private void fillBasement(short totalCount, float totalSum,
                              float totalWeight) {
        tvTotAmount.setText(GlobalProc.toForeignPrice(totalSum, Order.getOrder().getClient().isForeignAgent()));
        tvTotCount.setText(String.valueOf(totalCount));
        tvTotWeight.setText(String.format("%.1f кг.", totalWeight));
        btOk.setEnabled(totalCount > 0);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCountChanged(OnCountChangedTotalSize size) {
        fillBasement((short) size.getSize(), 0, 0);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateChanged(DateChanged dateChanged) {
        int year = dateChanged.getYear();
        int month = dateChanged.getMonth();
        int day = dateChanged.getDay();
        Calendar dateShip = Calendar.getInstance();
        dateShip.set(Calendar.YEAR, year);
        dateShip.set(Calendar.MONTH, month);
        dateShip.set(Calendar.DAY_OF_MONTH, day);
        dateShip.set(Calendar.HOUR, 0);
        dateShip.set(Calendar.MINUTE, 0);
        setButtonDateShipText(String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year));
        if (getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK) {
            Task currentTask = Task.getCurrentTask();
            currentTask.dueDate(dateShip.getTime().getTime());
            TaskLab.get(context).updateTask(currentTask);
        } else {
            Monitoring monitoring = Monitoring.getCurrentMonitoring();
            monitoring.setDate(dateShip.getTime());
            monitoring.update(context);
        }
    }

    public void setButtonDateShipText(String newText) {
        String title = getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK ? "Срок до: " : "Дата мониторинга: ";
        btn_get_date.setText(title.concat(newText));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_get_date:
                showDatePickerDialog(v, new Date());
                break;
            case (R.id.og_btAddGoods):
                Order order = Order.getOrder();
                byte mTypeMoney = order.mTypeMoney;
                byte mPrintBill = order.mPrintBill;
                Log.d(TAG, "mPrintBill: " + mPrintBill + ", mTypeMoney: " + mTypeMoney);
                byte mode = 0;
                if (Order.getOrder() != null) {
                    mode = Order.getOrder().quality();
                }
                switch (mode) {
                    case Order.QUALITY_NORMAL:
                        m_goods_filter().show();
                        //new GetSizePromotionGoodsTask(context, this).execute();
                        break;
                    case Order.QUALITY_NON_LIQUID:
                        Bundle bundle = new Bundle();
                        bundle.putByte(SimpleGoodsListContainer.EXTRA_LOAD_GOODS, SimpleGoodsListContainer.LOAD_GOODS_NON_LIQUID);
                        bundle.putByte(SimpleGoodsListContainer.EXTRA_VIEW_MODE, GoodsAdapter.MODE_ORDER);
                        bundle.putParcelable(SimpleGoodsListContainer.EXTRA_CLIENT, Order.getOrder().getClient());
                        ArrayList<GoodsInDocument> list = createGoodsInDocList();
                        bundle.putParcelableArrayList(GoodsInDocument.KEY, list);
                        SimpleGoodsListContainer fragment = new SimpleGoodsListContainer();
                        fragment.setTargetFragment(OrderGoodsFragment.this, IDD_ADD);
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction()
                                .replace(R.id.contentMain, fragment)
                                .addToBackStack(null)
                                .commit();
                        break;
                }
                break;
            case (R.id.og_btOk):
                btOk.setEnabled(false);
                if (App.isMerch()) {
                    if (getTargetRequestCode() == VisitContainFragment.IDD_NEW_TASK) {
                        String description = etDescriptionTask.getText().toString().trim();
                        if (!description.isEmpty()) {
                            Task currentTask = Task.getCurrentTask();
                            currentTask.description(description);
                            waitCursor(true);
                            SetReserveTask task = new SetReserveTask(context, this, currentTask);
                            task.execute();
                        } else {
                            GlobalProc.mToast(context, getString(R.string.empty_description));
                            btOk.setEnabled(true);
                        }
                    } else {
                        Monitoring data = Monitoring.getCurrentMonitoring();
                        if (data != null) {
                            saveMonitoring(data);
                        }
                    }
                } else {
                    Order data = Order.getOrder();
                    if (data != null) {
                        if (data.getClient().isForeignAgent()) {
                            data.deleteChecked(true);
                            if (data.getItems().size() > 0) {
                                saveOrder();
                            }
                        } else {
                            if (!isExistMercuryOrder(data)) {
                                if (!isExistNonCashOrder(data)) {
                                    data.deleteChecked(true);
                                    if (data.getItems().size() > 0) {
                                        saveOrder();
                                    }
                                } else {
                                    AlertDialog alertDialog = new AlertDialog.Builder(context)
                                            .setIcon(android.R.drawable.ic_dialog_info)
                                            .setTitle(R.string.confirmation)
                                            .setMessage(getString(R.string.order_non_cash))
                                            .setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss())
                                            .setNeutralButton("Сменить тип оплаты", (dialog, which) -> {
                                                Order.changeTypeMoney(context);
                                            })
                                            .setPositiveButton("Удалить", (dialog, which) -> {
                                                Order.getOrder().removeNonCashItems();
                                                notifyAdapter();
                                            })
                                            .create();
                                    if (!alertDialog.isShowing()) {
                                        alertDialog.show();
                                    }
                                    btOk.setEnabled(true);
                                }
                            } else {
                                AlertDialog alertDialog = new AlertDialog.Builder(context)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle(R.string.confirmation)
                                        .setMessage(getString(R.string.order_mercury))
                                        .setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss())
                                        .setPositiveButton("Удалить", (dialog, which) -> {
                                            Order.getOrder().removeMercuryItems();
                                            notifyAdapter();
                                        })
                                        .create();
                                if (!alertDialog.isShowing()) {
                                    alertDialog.show();
                                }
                                btOk.setEnabled(true);
                            }
                        }
                    }
                }
                break;
            case R.id.og_btCancel:
                if (currentDocMode == DOC_NEW) {
                    getTargetFragment().onActivityResult(DOC_BACK, RESULT_OK, new Intent());
                    getFragmentManager().popBackStack();
                } else {
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }

    private boolean isExistNonCashOrder(Order order) {
        if (order.mPrintBill == 1) {
            for (OrderItem orderItem : order.getItems()) {
                if (!orderItem.isChecked() && orderItem.isNonCash()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isExistMercuryOrder(Order order) {
        boolean regMercury = Order.getOrder().getClient().isRegMercury();
        byte mPrintBill = Order.getOrder().mPrintBill;
        if (!regMercury && mPrintBill == 1) {
            for (OrderItem items : order.getItems()) {
                if (!items.isChecked() && items.getIsMercury() == 1) {
                    return true;
                }
            }
        }
        return false;
    }

    private void saveMonitoring(Monitoring data) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            db = app.getDB();
            saveMonitoring(data, db);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }
    }

    private void saveMonitoring(Monitoring data, SQLiteDatabase db) {
        if (data == null)
            return;
        data.setUnconfirmed(1);
        if (data.update(context)) {
            GlobalProc.mToast(context, "Успешно");
        }
        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, new Intent());
        FragmentManager fragmentManager = getFragmentManager();
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            fragmentManager.popBackStack();
        } else {
            if (fragmentManager.getBackStackEntryCount() == 3) {
                fragmentManager.popBackStack();
                fragmentManager.popBackStack();
            } else {
                fragmentManager.popBackStack();
            }
        }
    }

    private void saveOrder() {
        if (flagForCompleteAskDiscount) {
            SQLiteDatabase db;
            try {
                MyApp app = (MyApp) context.getApplicationContext();
                db = app.getDB();
                Order data = Order.getOrder();
                saveOrder(data, db);
            } catch (Exception e) {
                Log.e(TAG + ".saveOrder", e.toString());
                e.printStackTrace();
            }
        } else {
            prepareAskDiscountTaskStart(false);
        }
    }

    private void saveOrder(Order data, SQLiteDatabase db) {
        if (data == null)
            return;
        String res;
        if (currentDocMode == DOC_NEW) {
            short docNum = DBProp.GetNumOrder();
            data.setNumOrder(docNum);
            if (data.insert(db)) {
                DBProp.IncNumOrder();
                if (DBProp.UpdateDBProp(db)) {
                    res = getString(R.string.mes_order_success_save);
                } else {
                    res = "Ошибка1";
                }
                Order.deleteUnsavedOrder(db);
            } else {
                int cnt = GlobalProc.getDocsCount(db, OrderMetaData.TABLE_NAME);
                cnt += GlobalProc.getDocsCount(db, ReturnMetaData.TABLE_NAME);
                docNum = (short) (cnt + 1);
                data.setNumOrder(docNum);
                if (data.insert(db)) {
                    DBProp.IncNumOrder();
                    DBProp.UpdateDBProp(db);
                    res = getString(R.string.mes_order_success_save);
                } else {
                    res = "Ошибка 2";
                }
            }
        } else {
            if (data.update(db)) {
                res = getString(R.string.mes_order_success_update);
            } else {
                res = "Ошибка 3";
            }
        }
        GlobalProc.mToast(context, res);
        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, new Intent());
        getFragmentManager().popBackStack();
    }


    protected Dialog m_goods_filter() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.cap_choose_goods));
        builder.setIcon(R.mipmap.main_icon);
        String[] array = getResources().getStringArray(R.array.goods_filter_list_order);
        List<String> list = new ArrayList<>(Arrays.asList(array));
//        if (sizePromotions == null || sizePromotions == 0) {
//            list.remove("Акции");
//        }
        array = list.toArray(new String[0]);
        builder.setItems(array,
                (dialog, which) -> {
                    Client selected = Order.getOrder().getClient();
                    switch (which) {
                        case GoodsList.MODE_ALL: // All
                            startGoodsSelectFragment(GoodsList.MODE_ALL, null, GoodsAdapter.MODE_ORDER, selected);
                            break;
                        case GoodsList.MODE_MATRIX:
                            startGoodsSelectFragment(GoodsList.MODE_MATRIX, null, GoodsAdapter.MODE_ORDER, Order.getOrder().getClient());
                            break;
                        case GoodsList.MODE_STM:
//                            Bundle bundle = new Bundle();
//                            bundle.putByte(SimpleGoodsListContainer.EXTRA_LOAD_GOODS, SimpleGoodsListContainer.LOAD_GOODS_STM);
//                            bundle.putByte(SimpleGoodsListContainer.EXTRA_VIEW_MODE, GoodsAdapter.MODE_ORDER);
//                            bundle.putParcelable(SimpleGoodsListContainer.EXTRA_CLIENT, selected);
//                            ArrayList<GoodsInDocument> list = createGoodsInDocList();
//                            bundle.putParcelableArrayList(GoodsInDocument.KEY, list);
//                            SimpleGoodsListContainer fragment = new SimpleGoodsListContainer();
//                            fragment.setTargetFragment(OrderGoodsFragment.this, IDD_ADD);
//                            fragment.setArguments(bundle);
//                            getFragmentManager().beginTransaction()
//                                    .replace(R.id.contentMain, fragment)
//                                    .addToBackStack(null)
//                                    .commit();
                            startGoodsSelectFragment(GoodsList.MODE_STM, null, GoodsAdapter.MODE_ORDER, selected);
                            break;
                        case GoodsList.MODE_MUST_LIST:
                            Bundle args = new Bundle();
                            args.putByte(SimpleGoodsListContainer.EXTRA_LOAD_GOODS, SimpleGoodsListContainer.LOAD_GOODS_MUST_LIST);
                            args.putByte(SimpleGoodsListContainer.EXTRA_VIEW_MODE, GoodsAdapter.MODE_ORDER);
                            args.putParcelable(SimpleGoodsListContainer.EXTRA_CLIENT, selected);
                            ArrayList<GoodsInDocument> list1 = createGoodsInDocList();
                            args.putParcelableArrayList(GoodsInDocument.KEY, list1);
                            SimpleGoodsListContainer fragment1 = new SimpleGoodsListContainer();
                            fragment1.setTargetFragment(OrderGoodsFragment.this, IDD_ADD);
                            fragment1.setArguments(args);
                            getFragmentManager().beginTransaction()
                                    .replace(R.id.contentMain, fragment1)
                                    .addToBackStack(null)
                                    .commit();
                            break;
                        case GoodsList.MODE_LIST:
                            startGoodsSelectFragment(GoodsList.MODE_LIST, null, GoodsAdapter.MODE_ORDER, selected);
                            break;
                        case GoodsList.MODE_PROMOTIONS:
                            startGoodsSelectFragment(GoodsList.MODE_PROMOTIONS, null, GoodsAdapter.MODE_ORDER, selected);
                            break;
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }


    private void startMerchMatrix(byte typeLoad, Node selected, byte mode, Client client) {
        Bundle goodsSelect = new Bundle();
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_TYPE, typeLoad);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_NODE, selected);
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_MODE, mode);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_CLIENT, client);
        ArrayList<GoodsInDocument> list = new ArrayList<>();
        goodsSelect.putParcelableArrayList(GoodsInDocument.KEY, list);
        GoodsTreeFragment fragment = new GoodsTreeFragment();
        fragment.setTargetFragment(this, getTargetRequestCode());
        fragment.setArguments(goodsSelect);
        getFragmentManager().beginTransaction()
                .replace(R.id.contentMainMerch, fragment, GoodsTreeFragment.TAG)
                .commit();
    }


    /**
     * Запускает активность с выбранными параметрами
     *
     * @param typeLoad - тип Все, матрица и т. п.
     * @param selected - выбранный узел
     * @param mode     - справочник, заявка и т. п.
     */
    private void startGoodsSelectFragment(byte typeLoad, Node selected, byte mode, Client client) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        GoodsTreeFragment fragment = new GoodsTreeFragment();
        fragment.setTargetFragment(this, IDD_ADD);
        Bundle goodsSelect = new Bundle();
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_TYPE, typeLoad);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_NODE, selected);
        goodsSelect.putByte(GoodsTreeFragment.EXTRA_MODE, mode);
        goodsSelect.putParcelable(GoodsTreeFragment.EXTRA_CLIENT, client);
        ArrayList<GoodsInDocument> list = createGoodsInDocList();
        goodsSelect.putParcelableArrayList(GoodsInDocument.KEY, list);
        fragment.setArguments(goodsSelect);
        ft.addToBackStack(null)
                .replace(R.id.contentMain, fragment, GoodsTreeFragment.TAG)
                .commit();
    }


    private void loadOrderForEdit(short docNum) {
        SQLiteDatabase db = null;
        try {
            MyApp app = (MyApp) context.getApplication();
            db = app.getDB();
            Order orderData = Order.load(db, docNum);
            Order.setOrder(orderData);
        } catch (Exception e) {
            GlobalProc.mToast(context, "Ошибка обновления заявки");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        flagForCompleteAskDiscount = false;
        Order orderData = Order.getOrder();
        if (orderData == null) {
            if (currentDocMode == DOC_NEW) {
                Order.loadUnsavedOrder(context);
            }
            if (currentDocMode == DOC_EDIT) {
                loadOrderForEdit(docNumForEdit);
            }
        }
        switch (requestCode) {
            case IDD_ADD:
                boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
                if (tabletSize) {
                    FragmentManager fragmentManager = getFragmentManager();
                    Fragment goodsListFragment = fragmentManager.findFragmentByTag(GoodsListFragment.TAG);
                    Fragment gMatrixFragment = fragmentManager.findFragmentByTag(GoodsGroupMatrixFragment.TAG);
                    if (goodsListFragment != null && goodsListFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                    if (gMatrixFragment != null && gMatrixFragment.isAdded()) {
                        fragmentManager.beginTransaction().remove(goodsListFragment).commit();
                    }
                }
                if (data != null) {
                    ArrayList<GoodsInDocument> newItems = data.getParcelableArrayListExtra(GoodsInDocument.KEY);
                    if (newItems != null && newItems.size() > 0) {
                        MyApp app = (MyApp) context.getApplicationContext();
                        ArrayList<OrderItem> orderItems = new ArrayList<>();
                        if (orderData != null) {
                            for (OrderItem orderItem : orderData.getItems()) {
                                for (GoodsInDocument inDocument : newItems) {

                                    if (orderItem.getCodeGoods().equals(inDocument.getCode())) {
                                        orderItems.add(orderItem);
                                    }
                                }
                            }
                            orderData.clear();
                            orderData.addItems(orderItems);
                        }
                        for (GoodsInDocument item : newItems) {
                            OrderItem oi = null;
                            if (orderData != null) {
                                oi = orderData.find(item.getCode());
                            }
                            Goods goods = Goods.load(app.getDB(), item.getCode());
                            if (oi == null) {
                                oi = new OrderItem(goods);
                                oi.setIsMercury(goods.getIsMercury());
                                oi.setNonCash(goods.isNonCash());
                                oi.setIsCustom(goods.getIsCustom());
                                oi.perishable(goods.perishable());
                                if (orderData != null) {
                                    orderData.addItem(oi);
                                }
                            }
                            if (item.getQuant() > goods.getQuant()) {
                                int count = (int) Math.ceil(item.getQuant() / goods.getQuant() * item.getCountPosition());
                                item.setCountPosition(count);
                            }
                            oi.mIsHalfHead = item.ismIsHalfHead();

                            if (item.getMeashPosition() == 1) {
                                oi.inPack(true);
                                oi.setCountPack(item.getCountPosition());
                            } else {
                                oi.setCount(item.getCountPosition());
                                oi.inPack(false);
                            }
                            oi.mIsSpecialPrice = item.isSpecPrice();
                            oi.mIsHalfHead = item.ismIsHalfHead();
                            oi.setPrice(item.getPrice());
                            oi.isSertificateNeed(item.isSertificate());
                            oi.isQualityDocNeed(item.isQualityDoc());
                        }
                    } else {
                        if (orderData != null) {
                            orderData.clear();
                        }
                    }
                }
                Order.updateUnsavedOrder(context);
                if (!App.isMerch()) {
                    notifyAdapter();
                    if (orderData != null) {
                        orderData.recalcAmount(false);
                        fillBasement(orderData.getItemsCount(), orderData.getTotalSumUnchecked(),
                                orderData.getTotalWeightUnchecked());
                    }
                }
                break;
            case IDD_MODIFY:
                if (data != null && resultCode == RESULT_OK) {
                    OrderItem oi = data.getParcelableExtra(OrderItem.KEY);
                    if (oi != null) {
                        if (orderData != null) {
                            orderData.updateItem(oi);
                            orderData.recalcAmount(false);
                            notifyAdapter();
                            fillBasement(orderData.getItemsCount(), orderData.getTotalSumUnchecked(), orderData.getTotalWeightUnchecked());
                            GlobalProc.mToast(context, getString(R.string.mes_item_edit_success));
                        } else {
                            GlobalProc.mToast(context, getString(R.string.mes_oi_add_error));
                        }
                    }
                }
                break;
        }
    }

    private void notifyAdapter() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
//            if (btOk != null) {
//                Order data = Order.getOrder();
//                if (data.hasChecked() == -1) {
//                    btOk.setEnabled(false);
//                } else {
//                    btOk.setEnabled(true);
//                }
//            }
        }
    }


    private void printClientData(View v) {
        if (Order.getOrder() == null) {
            Order.loadUnsavedOrder(context);
        }
        Order data = Order.getOrder();
        if (data == null || data.getClient() == null) {
            GlobalProc.mToast(context, "Не получили данных о клиенте, завершаем OrderGoodsActivity");
            //TODO _setResult(RESULT_CANCELED);
            return;
        }

        Client client = data.getClient();
        try {
            TextView tvCliName = v.findViewById(R.id.og_lbName);
            TextView tvCliAddress = v.findViewById(R.id.og_lbAddress);

            TextView tvMaxCredit = v.findViewById(R.id.og_lbMaxCredit);
            TextView tvAVG = v.findViewById(R.id.og_lbAvg);
            TextView tvSKU = v.findViewById(R.id.og_lbSKU);
            TextView tvDiscount = v.findViewById(R.id.og_lbDiscount);
            tvCliName.setText(client.mName);
            tvCliAddress.setText(client.mPostAddress);
            tvCurCredit.setText(getString(R.string.debt, replaceCentDecimalValue(client.mCurArrear)));
            tvMaxCredit.setText(replaceCentDecimalValue(client.mMaxArrear));
            tvAVG.setText(replaceCentDecimalValue(GlobalProc.formatMoney(client.mAverageOrder)));
            tvSKU.setText(String.valueOf(client.mSKU));
            String discountText = client.mDiscount == 0 ? "Нет" : Byte.toString(client.mDiscount);
            tvDiscount.setText(discountText);


        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private String replaceCentDecimalValue(String value) {
        value = value.replaceAll("\\s+","");
        if (value.contains(",")) {
            int index = value.indexOf(",");
            boolean isForeign = Order.getOrder().getClient().isForeignAgent();
            float v = Float.parseFloat(value.substring(0, index));
            return GlobalProc.toForeignPrice(v, isForeign);
        }
        return value;
    }

    private ArrayList<GoodsInDocument> createGoodsInDocList() {
        ArrayList<GoodsInDocument> result = new ArrayList<>();
        for (OrderItem item : Order.getOrder().getItems()) {
            GoodsInDocument gid = new GoodsInDocument(item.getCodeGoods(),
                    item.inPack() ? item.getCountPack() : item.getCount(),
                    (byte) (item.inPack() ? 1 : 0),
                    item.getQuant(), item.getPrice(), item.getWeight());
            gid.isSpecPrice(item.mIsSpecialPrice);
            gid.isSertificate(item.isSertificateNeed());
            gid.isQualityDoc(item.isQualityDocNeed());
            result.add(gid);
        }
        return result;
    }

    @Override
    public void onTaskComplete(AutoOrderTask task) {
        waitCursor(false);
        boolean result = false;
        boolean timeout = false;
        try {
            result = task.get();
            // isCancelled = task.isCancelled();
            timeout = task.isTimeout();
        } catch (Exception e) {
            result = false;
        }

        if (result | timeout) {
            Order data = Order.getOrder();
            if (data == null) {
                Order.loadUnsavedOrder(context);
                data = Order.getOrder();
            }

            data.wasAutoOrder(timeout ? Order.AUTOORDER_OFFLINE
                    : Order.AUTOORDER_ONLINE);

            ArrayList<OrderItem> items = task.getItems();
            if (items != null) {
                //data.clear();
                data.addItems(items);

                Order.updateUnsavedOrder(context);
            } else {
                GlobalProc.mToast(context, "Ошибка при передаче параметров");
            }
        } else {
            GlobalProc.mToast(context, task.getMessage());
        }

        mAdapter.notifyDataSetChanged();
    }


    private void waitCursor(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onTaskComplete(AskDiscountTask task) {
        int result = AskDiscountTask.RES_SUCCESS;
        try {
            result = task.get();
        } catch (Exception e) {
            result = AskDiscountTask.RES_FAIL;
        }
        boolean res = true;
        String msg = "Цены рассчитаны. Весь товар в наличии";
        if (result == AskDiscountTask.RES_SUCCESS) {
            if (Order.getOrder() != null) {
                Order.getOrder().wasCheck(true);
                Order.updateUnsavedOrder(context);
                for (OrderItem oi : Order.getOrder().getItems()) {
                    if (oi.notInStock()) {
                        msg = "Цены рассчитаны. Красным цветом обозначены позиции с недостаточным количеством на складе";
                        res = false;
                        btOk.setEnabled(true);
                        break;
                    }
                }
            }
            notifyAdapter();
        } else {
            msg = task.getMessage();
        }
        GlobalProc.mToast(context, msg);
        //Snackbar.make(getView(), msg, Snackbar.LENGTH_SHORT).show();
        waitCursor(false);
        flagForCompleteAskDiscount = true;
        if (res && !task.isStartIsMenu()) {
            SQLiteDatabase db;
            try {
                MyApp app = (MyApp) context.getApplicationContext();
                db = app.getDB();
                Order data = Order.getOrder();
                saveOrder(data, db);
            } catch (Exception e) {
                Log.e(TAG + ".saveOrder", e.toString());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskComplete(CheckRestTask task) {
        waitCursor(false);
        String text = "";
        byte result = CheckRestTask.RES_ERR;
        try {
            result = task.get();
        } catch (Exception e) {
            text = e.toString();
            result = CheckRestTask.RES_ERR;
        }
        switch (result) {
            case CheckRestTask.RES_ENOUGH:
                text = "Весь товар в требуемом количестве имеется на складе";
                if (Order.getOrder() != null) {
                    Order.getOrder().wasCheck(true);
                }
                break;
            case CheckRestTask.RES_NOT_ENOUGH:
                text = "Красным цветом обозначены позиции с недостаточным количеством на складе";
                if (Order.getOrder() != null) {
                    Order.getOrder().wasCheck(true);
                }
                notifyAdapter();
                break;
            case CheckRestTask.RES_ERR:
                text = task.getMessage();
                break;
        }

        GlobalProc.mToast(context, text);
    }

    @Override
    public void onTaskComplete(GetLimitsTask task) {
        try {
            String s = task.get();
            if (s != null)
                tvLimit.setText(s);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onTaskComplete(GetMinAmountOrder task) {
        Drawable green = ContextCompat.getDrawable(context, R.drawable.traffic_light_green);
        Drawable red = ContextCompat.getDrawable(context, R.drawable.traffic_light_red);
        boolean foreignAgent = Order.getOrder().getClient().isForeignAgent();
        try {
            String minAmountOrderResult = task.get();
            if (minAmountOrderResult != null) {
                String[] dataArray = minAmountOrderResult.split(";");
                String amount = dataArray[0].isEmpty() || dataArray[0].equals("") ? "0" : dataArray[0].replace(",", ".");
                String amount_order_today = dataArray[1].isEmpty() || dataArray[1].equals("") ? "0" : dataArray[1].replace(",", ".");
                int trafficLightFlag = Integer.parseInt(dataArray[2]);
                txt_min_amount_order.setText(GlobalProc.toForeignPrice(Float.parseFloat(amount), foreignAgent));
                txt_amount_order_today.setText(GlobalProc.toForeignPrice(Float.parseFloat(amount_order_today), foreignAgent));
                if (trafficLightFlag == 0) {
                    setBackgroudView(red);
                } else {
                    setBackgroudView(green);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
            txt_min_amount_order.setText(GlobalProc.toForeignPrice(0, foreignAgent));
            txt_amount_order_today.setText(GlobalProc.toForeignPrice(0, foreignAgent));
        }
    }

    private void setBackgroudView(Drawable color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view_traffic_light.setBackground(color);
        } else {
            view_traffic_light.setBackgroundDrawable(color);
        }
    }

    @Override
    public void onCompleteGetSizePromotionGoodsTask(Integer integer) {
        waitCursor(false);
        //m_goods_filter(integer).show();
    }

    @Override
    public void onTaskComplete(SetReserveTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            txt = getString(R.string.mes_reserv_task_success);
        } else {
            txt = getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getmUnswer();
        }
        GlobalProc.mToast(context, txt);
        waitCursor(false);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment targetFragment = getTargetFragment();
        targetFragment.onActivityResult(getTargetRequestCode(), RESULT_OK, new Intent());
        boolean tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            fragmentManager.popBackStack();
        } else {
            if (fragmentManager.getBackStackEntryCount() == 3) {
                fragmentManager.popBackStack();
                fragmentManager.popBackStack();
            } else {
                fragmentManager.popBackStack();
            }
        }
    }
}
