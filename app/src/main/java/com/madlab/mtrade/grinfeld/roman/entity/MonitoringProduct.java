package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRA
 */
public class MonitoringProduct {

    private int idMonitoring;
    private String codeProduct;
    private int check;

    public MonitoringProduct(int idMonitoring, String codeProduct, int check) {
        this.idMonitoring = idMonitoring;
        this.codeProduct = codeProduct;
        this.check = check;
    }

    public int getIdMonitoring() {
        return idMonitoring;
    }

    public String getCodeProduct() {
        return codeProduct;
    }

    public int getCheck() {
        return check;
    }


    public void setIdMonitoring(int idMonitoring) {
        this.idMonitoring = idMonitoring;
    }

    public void setCodeProduct(String codeProduct) {
        this.codeProduct = codeProduct;
    }

    public void setCheck(int check) {
        this.check = check;
    }
}
