package com.madlab.mtrade.grinfeld.roman.tasks;

import android.location.Location;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;

import java.util.Locale;

import static com.madlab.mtrade.grinfeld.roman.Const.COMMAND;

/**
 * Created by GrinfeldRA
 */

public class SetReserveAll {

    private String mCodeAgent;
    private static final String VISIT = "СформироватьВизит";
    private final static String ORDER = "СформироватьДокументыФормат";
    private final static String PAIMENT = "СформироватьФайлОплат";
    private final static String RETURN = "СформироватьДокументыВозврата";

    String commandVisit = String.format("%s;%s;%s;", COMMAND, mCodeAgent, VISIT);



    private String getVisitQuery(Visit visit){
        Location loc = visit.getLocation();
        return String.format(Locale.getDefault(),"%d;%s;%s;" + "%.6f;%.6f;%.2f;" + "%d;%d;%d;"
                        + "%s;%d;%d;" + "%.2f;%.2f;%s;",
                visit.cnt(),
                mCodeAgent,
                visit.client().getCode(),
                loc.getLatitude(), loc.getLongitude(), loc.getAccuracy(),
                visit.hasOrder() ? 1 : 0, visit.hasReturns() ? 1 : 0,
                visit.hasPayments() ? 1 : 0,
                TimeFormatter.formatTo1CDateTimeGMT(visit.getStartTime()),
                visit.client().mSKU,
                visit.orderSKU(), visit.client().mAverageOrder,
                visit.orderSum(), visit.result());
    }


    private String getOrderQuery(Order data){
        String codeCli = data.getClient() != null ? data.getClient().getCode() : "";
        String orderCommon = String.format(Locale.ENGLISH,
                "%s;%s;%s;%s;%s;%d;%d;%d;%d;%d;"
                        + "%s;%s;%d;%d;%s;%s;%s;%s;%d;%.2f;" + "%d;%d;%d;%03d;"
                        + "%s;%s;%s;%s;%s;%s", data.getNumOrder(), // 0 ID документа
                (data.mState == Order.State.Normal) ? Order.STATE_NORMAL
                        : Order.STATE_DELETED, // 1 Статус (пометка удалаения)
                data.mCodeAgent, // 2 Код менеджера
                codeCli, // 3 Код контрагента
                TimeFormatter.sdf1C.format(data.getDateShip()), // 4 Дата поставки
                data.mTypePay, // 5 дополнительные указания
                data.mPrintBill, // 6 Признак документа
                data.mTypeMoney, // 7 Вид оплаты
                data.mTakeMoney ? 1 : 0, // 8 Забрать возврат
                data.mTypeVisit, // 9 тип визита
                data.mNote, // 10 Указания по доставке
                data.getOrderTime(), // 11 Время принятия заявки
                data.mVetSpr ? 1 : 0, // 12 Ветеринарное удостоверение
                data.wasAutoOrder(), // 13 Был автозаказ
                GlobalProc.getVersionName(MyApp.getContext()), // 14 Версия программы
                "0.0", // 15 широта
                "0.0", // 16 Долгота
                "0.0", // 17 Погрешность
                data.mWarranty ? 1 : 0, // 18
                data.sumToGet(), // 19
                data.selfDelivery() ? 1 : 0,// 20 Везу сам
                data.quality() == Order.QUALITY_NORMAL ? 0 : 1, // Качество
                data.wasCheck() ? 1 : 0, // 22 была ли проверка остатков
                data.firmFK(), // 23 ссылка на фирму
                "", // 24 Резервное поле
                "", // 25 Резервное поле
                "", // 26 Резервное поле
                "", // 27 Резервное поле
                "", // 28 Резервное поле
                "" // 29 Резервное поле
        );

        StringBuilder sb = new StringBuilder(String.format("%s;", orderCommon));

        for (OrderItem item : data.getItems()) {
            //Так как количество упаковок - множитель, вставляем костыль
            int countPack = item.getCountPack() == 0 ? 1 : item.getCountPack();

            //Теперь колдуем с количеством
            float count = 1f;
            if (item.mIsWeightGoods) {
                if (item.mIsHalfHead)//Всего полголовки
                    count = 0.5f;//item.getWeight() / 2;
                else//Весовой товар передаём в головках
                    count = item.getCount();
            } else {
                //количеством будет кол-во в упаковке, кол-во упаковок - множитель
                if (item.inPack()) {
                    count = item.numInPack();
                } else {
                    count = item.getCount() * item.getQuant();
                }
            }
            sb.append(String.format(Locale.ENGLISH,
                    "%s|%d|%.2f|%d|%.2f|%.2f|%d|%d|;", item.getCodeGoods(), // 0
                    countPack, // 1
                    count, // 2
                    item.mIsSpecialPrice ? 1 : 0, // 3
                    item.getPrice(), // 4
                    item.getAmount(), // 5
                    item.isSertificateNeed() ? 1 : 0, // 6
                    item.isQualityDocNeed() ? 1 : 0 // 7
            ));
        }
        return sb.toString();
    }

}
