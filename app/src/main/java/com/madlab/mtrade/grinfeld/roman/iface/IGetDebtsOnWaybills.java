package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.GetDebtsOnWaybills;

/**
 * Created by GrinfeldRA on 15.06.2018.
 */

public interface IGetDebtsOnWaybills {

    public void onCompleteGetDebtsOnWaybills(GetDebtsOnWaybills task);

}
