package com.madlab.mtrade.grinfeld.roman.entity;


import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitReservedMetadata;

public class Visit implements Parcelable {
    private static final String TAG = "!->Visit";
    public static final String KEY = "visit";

    public static final byte STATE_NORMAL = 0;
    public static final byte STATE_SENDED = 1;
    public static final byte STATE_DELETED = 2;
    public static final byte STATE_ALL = 3;

    private int mCNT;
    private UUID mID;
    private Location mLocation;
    private int mLocationID;
    private long mStartTime;
    private long mEndTime;
    private Client mClient;
    private short mOrderSKU;
    private float mOrderSum;
    private String mResult;

    private boolean mHasOrder;
    private boolean mHasReturns;
    private boolean mHasPayments;

    private ArrayList<PaybackPhoto> paybackPhotos;

    /**
     * Состояние документа, 0 - создан, 1 - отправлен, 2 помечен на удаление
     */
    private byte mState;

    public Visit(UUID iD, Location location, long startTime, long endTime,
                 Client client) {
        mCNT = -1;
        mID = iD;
        mLocation = location;
        mStartTime = startTime;
        mEndTime = endTime;
        mClient = client;
        mState = 0;
        mResult = "";

        mHasOrder = false;
        mHasReturns = false;
        mHasPayments = false;
        paybackPhotos = new ArrayList<>();
    }

    public Visit(UUID iD, Location location, long startTime,
                 Client client) {
        mCNT = -1;
        mID = iD;
        mLocation = location;
        mStartTime = startTime;
        mClient = client;
        mState = 0;
        mResult = "";

        mHasOrder = false;
        mHasReturns = false;
        mHasPayments = false;
        paybackPhotos = new ArrayList<>();
    }

    public int cnt() {
        return mCNT;
    }

    public void cnt(int counter) {
        mCNT = counter;
    }

    public UUID getID() {
        return mID;
    }

    public void setID(UUID iD) {
        mID = iD;
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public int locationID() {
        return mLocationID;
    }

    public void locationID(int locationID) {
        mLocationID = locationID;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public long getEndTime() {
        return mEndTime;
    }

    public void setEndTime(long endTime) {
        this.mEndTime = endTime;
    }

    public void client(Client client) {
        mClient = client;
    }

    public Client client() {
        return mClient;
    }

    public byte state() {
        return mState;
    }

    public void state(byte value) {
        mState = value;
    }

    public boolean hasOrder() {
        return mHasOrder;
    }

    public void hasOrder(boolean hasOrder) {
        mHasOrder = hasOrder;
    }

    public boolean hasReturns() {
        return mHasReturns;
    }

    public void hasReturns(boolean hasReturns) {
        mHasReturns = hasReturns;
    }

    public boolean hasPayments() {
        return mHasPayments;
    }

    public void hasPayments(boolean hasPayments) {
        mHasPayments = hasPayments;
    }

    public short orderSKU() {
        return mOrderSKU;
    }

    public void orderSKU(short count) {
        mOrderSKU = count;
    }

    public float orderSum() {
        return mOrderSum;
    }

    public void orderSum(float sum) {
        mOrderSum = sum;
    }

    public String result() {
        return mResult;
    }

    public void result(String result) {
        mResult = result;
    }

    /**
     * Сохраняем в БД
     *
     * @param ctx
     */
    public void insert(Context ctx) {
        try {
            MyApp app = (MyApp) ctx;
            insert(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void insert(SQLiteDatabase db) {
        ContentValues visitData = new ContentValues();

        visitData.put(VisitMetaData.FIELD_KEY, mID.toString());
        visitData.put(VisitMetaData.FIELD_CLIENT_CODE, mClient.getCode());
        visitData.put(VisitMetaData.FIELD_LOCATION_FK, mLocationID);
        visitData.put(VisitMetaData.FIELD_START_TIME,
                TimeFormatter.formatSQLWithTime(mStartTime));
        visitData.put(VisitMetaData.FIELD_END_TIME, mEndTime == 0 ? "" : TimeFormatter.formatSQLWithTime(mEndTime));
        visitData.put(VisitMetaData.FIELD_ORDER_SKU, mOrderSKU);
        visitData.put(VisitMetaData.FIELD_ORDER_SUM, mOrderSum);
        visitData.put(VisitMetaData.FIELD_RESULT, mResult);
        ContentValues cv = new ContentValues();
        cv.put(VisitReservedMetadata.FIELD_UUID, mID.toString());
        db.beginTransaction();
        try {
            long index = db.insert(VisitMetaData.TABLE_NAME, null, visitData);
            db.insert(VisitReservedMetadata.TABLE_NAME, null, cv);
            mCNT = (int) index;
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG + ".insert", e.toString());
        } finally {
            db.endTransaction();
        }


    }

    public void update(Context ctx) {
        try {
            MyApp app = (MyApp) ctx;
            update(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void update(SQLiteDatabase db) {
        if (mCNT == -1) {
            insert(db);
            return;
        }

        String where = String.format("%s = %d", VisitMetaData._ID, mCNT);

        ContentValues visitData = new ContentValues();

        visitData.put(VisitMetaData.FIELD_KEY, mID.toString());
        visitData.put(VisitMetaData.FIELD_CLIENT_CODE, mClient.getCode());
        visitData.put(VisitMetaData.FIELD_LOCATION_FK, mLocationID);
        visitData.put(VisitMetaData.FIELD_START_TIME,
                TimeFormatter.formatSQLWithTime(mStartTime));
        visitData.put(VisitMetaData.FIELD_END_TIME, mEndTime == 0 ? "" : TimeFormatter.formatSQLWithTime(mEndTime));

        visitData.put(VisitMetaData.FIELD_ORDER_SKU, mOrderSKU);
        visitData.put(VisitMetaData.FIELD_ORDER_SUM, mOrderSum);

        visitData.put(VisitMetaData.FIELD_RESULT, mResult);

        db.beginTransaction();
        try {
            db.update(VisitMetaData.TABLE_NAME, visitData, where, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG + ".update", e.toString());
        } finally {
            db.endTransaction();
        }
    }

    public void updateState(Context ctx, byte newState, byte typeReserveServer, boolean isReserved) {
        ContentValues values = new ContentValues();
        if (isReserved) {
            values.put(VisitMetaData.FIELD_STATE, newState);
            values.put(VisitMetaData.FIELD_TYPE_SERVER, typeReserveServer);
        }
        values.put(VisitMetaData.FIELD_IS_RESERVED, newState);
        String where = String.format("%s = '%s'", VisitMetaData.FIELD_KEY, mID);

        MyApp app = (MyApp) ctx.getApplicationContext();
        SQLiteDatabase db = app.getDB();
        try {
            db.beginTransaction();
            int res = db.update(VisitMetaData.TABLE_NAME, values, where, null);
            if (res > 0)
                db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            db.endTransaction();
        }
    }


    public static Visit load(Context ctx, short id) {
        Visit result = null;
        try {
            MyApp app = (MyApp) ctx;
            result = load(app.getDB(), id);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return result;
    }

    /**
     * Загружаем визит по коду (автоинкременту)
     *
     * @param db ссылка на базу данных
     * @param id позиция в базе данных
     * @return
     */
    public static Visit load(SQLiteDatabase db, short id) {
        Visit result = null;
        String sql = String.format("SELECT * FROM %s WHERE %s = %d",
                VisitMetaData.TABLE_NAME, VisitMetaData._ID, id);
        Cursor rows = db.rawQuery(sql, null);
        boolean success = false;
        if (rows.moveToFirst()) {
            success = true;

            int number = rows.getInt(VisitMetaData.FIELD_ID_INDEX);
            UUID iD = UUID.fromString(rows
                    .getString(VisitMetaData.FIELD_KEY_INDEX));
            int locID = rows.getInt(VisitMetaData.FIELD_LOCATION_FK_INDEX);
            long startTime = TimeFormatter.parseSqlDateWithTimeToLong(rows.getString(VisitMetaData.FIELD_START_TIME_INDEX));
            long endTime = TimeFormatter.parseSqlDateWithTimeToLong(rows.getString(VisitMetaData.FIELD_END_TIME_INDEX));

            Client client = new Client();
            client.setCode(rows
                    .getString(VisitMetaData.FIELD_CLIENT_CODE_INDEX));

            result = new Visit(iD, null, startTime, endTime, client);
            result.cnt(number);

            // нужен для загрузки координаты из БД
            result.locationID(locID);

            // Состояние документа
            result.state((byte) rows.getInt(VisitMetaData.FIELD_STATE_INDEX));

            // Данные заявки
            result.orderSKU(rows.getShort(VisitMetaData.FIELD_ORDER_SKU_INDEX));
            result.orderSum(rows.getFloat(VisitMetaData.FIELD_ORDER_SUM_INDEX));

            //Результат визита
            String r = rows.getString(VisitMetaData.FIELD_RESULT_INDEX);
            result.result(r);
        }
        rows.close();

        if (success) {
            // Теперь подгружаем клиента
            Client client = Client.load(db, result.client().getCode());
            result.client(client);

            // Теперь подгружаем координату
            Location loc = MyGPS.load(db, result.locationID());
            result.setLocation(loc);
        }

        return result;
    }

    /**
     * Загружаем список визитов.
     *
     * @param ctx   Контекст нужен для получения ссылки на базу данных
     * @param state признак STATE STATE_NORMAL - созданные STATE_SENDED -
     *              отправленные STATE_DELETED - помеченные на удаление STATE_ALL
     *              - все
     * @return
     */
    public static ArrayList<Visit> loadList(Context ctx, byte state) {
        ArrayList<Visit> result = new ArrayList<Visit>();
        String sql = String.format("SELECT * FROM %s ",
                VisitMetaData.TABLE_NAME);
        if (state != STATE_ALL) {
            sql += String.format("WHERE %s = %d", VisitMetaData.FIELD_STATE,
                    state);
        }

        try {
            MyApp app = (MyApp) ctx;
            SQLiteDatabase db = app.getDB();

            Cursor rows = db.rawQuery(sql, null);
            while (rows.moveToNext()) {
                int number = rows.getInt(VisitMetaData.FIELD_ID_INDEX);
                UUID iD = UUID.fromString(rows
                        .getString(VisitMetaData.FIELD_KEY_INDEX));
                int locID = rows.getInt(VisitMetaData.FIELD_LOCATION_FK_INDEX);
                long startTime = TimeFormatter.parseSqlDateWithTimeToLong(rows
                        .getString(VisitMetaData.FIELD_START_TIME_INDEX));
                long endTime = TimeFormatter.parseSqlDateWithTimeToLong(rows
                        .getString(VisitMetaData.FIELD_END_TIME_INDEX));

                Client client = new Client();
                client.setCode(rows
                        .getString(VisitMetaData.FIELD_CLIENT_CODE_INDEX));

                Visit newItem = new Visit(iD, null, startTime, endTime, client);
                newItem.cnt(number);
                // нужен для загрузки координаты из БД
                newItem.locationID(locID);

                // Состояние документа
                newItem.state((byte) rows
                        .getInt(VisitMetaData.FIELD_STATE_INDEX));

                // Данные заявки
                newItem.orderSKU(rows
                        .getShort(VisitMetaData.FIELD_ORDER_SKU_INDEX));
                newItem.orderSum(rows
                        .getFloat(VisitMetaData.FIELD_ORDER_SUM_INDEX));

                //Результат визита
                String r = rows.getString(VisitMetaData.FIELD_RESULT_INDEX);
                newItem.result(r);
                result.add(newItem);
            }
            rows.close();

            // Теперь подгружаем клиентов и координаты
            for (Visit visit : result) {
                visit.client(Client.load(db, visit.client().getCode()));
                visit.setLocation(MyGPS.load(db, visit.locationID()));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return result;
    }


    public ArrayList<PaybackPhoto> getPaybackPhotos() {
        return paybackPhotos;
    }

    public void addPaybackPhotos(PaybackPhoto paybackPhotos) {
        this.paybackPhotos.add(paybackPhotos);
    }

    public static boolean isReserved(UUID id) {
        boolean res = false;
        String sql = String.format("SELECT %s FROM %s WHERE %s = '%s' and %s = 1",
                VisitMetaData._ID, VisitMetaData.TABLE_NAME,
                VisitMetaData.FIELD_KEY, id, VisitMetaData.FIELD_IS_RESERVED);
        Cursor rows = null;
        try {
            Context context = MyApp.getContext();
            MyApp app = (MyApp) context;
            SQLiteDatabase db = app.getDB();
            rows = db.rawQuery(sql, null);
            if (rows.moveToFirst()) {
                res = true;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mCNT);
        dest.writeSerializable(mID);
        dest.writeInt(mLocationID);
        dest.writeLong(mStartTime);
        dest.writeLong(mEndTime);
        dest.writeParcelable(mClient, flags);
        dest.writeInt(mOrderSKU);
        dest.writeFloat(mOrderSum);
        dest.writeByte(mState);
        dest.writeString(mResult);

        boolean[] b = {mHasOrder, mHasReturns, mHasPayments};

        dest.writeBooleanArray(b);
    }

    public Visit(Parcel source) {
        mCNT = source.readInt();
        mID = (UUID) source.readSerializable();
        mLocationID = source.readInt();
        mStartTime = source.readLong();
        mEndTime = source.readLong();
        mClient = source.readParcelable(Client.class.getClassLoader());
        mOrderSKU = (short) source.readInt();
        mOrderSum = source.readFloat();
        mState = source.readByte();
        mResult = source.readString();

        boolean[] b = new boolean[3];

        source.readBooleanArray(b);

        mHasOrder = b[0];
        mHasReturns = b[1];
        mHasPayments = b[2];
    }

    public static final Parcelable.Creator<Visit> CREATOR = new Parcelable.Creator<Visit>() {

        public Visit createFromParcel(Parcel in) {
            return new Visit(in);
        }

        public Visit[] newArray(int size) {
            return new Visit[size];
        }
    };
}
