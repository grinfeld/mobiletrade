package com.madlab.mtrade.grinfeld.roman.tasks;

import java.util.Locale;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.Payment;
import com.madlab.mtrade.grinfeld.roman.entity.request.PaymentRequest;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;


public class SetReservPaymentTask extends AsyncTask<Short, Void, Boolean> {
    private final static String PROC_NAME = "TAKE_PAYMENT";
    private final static String PROC_NAME_IIS = "СформироватьФайлОплат";

    public final static String SUCCESS = "OK";
    public final static String FAILURE = "ERR";

    private final static String WAIT_MESSAGE = "Связь с сервером";

    private Credentials connectInfo;

    private String mUnswer;

    private Context baseContext;
    private ProgressDialog mProgress;

    private ISetReservComplete mTaskCompleteListener;

    public SetReservPaymentTask(Context data,
                                ISetReservComplete taskCompleteListener) {
        baseContext = data;
        mTaskCompleteListener = taskCompleteListener;
        mUnswer = "";

        connectInfo = Credentials.load(baseContext);
    }

    public String getMessage() {
        return mUnswer;
    }

    @Override
    protected void onPreExecute() {
        mProgress = new ProgressDialog(baseContext);
        mProgress.setMessage(WAIT_MESSAGE);
        mProgress.setCancelable(true);
        mProgress.show();
    }

    @Override
    protected Boolean doInBackground(Short... numbers) {
        // Если будет ошибка в catch - выставим флаг
        boolean wasError = false;

        SQLiteDatabase db = null;
        DalimoClient myClient = null;

        try {
            MyApp app = (MyApp) baseContext.getApplicationContext();
            db = app.getDB();

//            StringBuilder sb = new StringBuilder();
//            sb.append(String.format("%s;%s;%s", PROC_NAME, App.get(baseContext).getCodeAgent(), Const.NewLine));

            for (Short current : numbers) {
                Payment data = Payment.load(db, current);


                //sb.append(generateDataStringForReserv(data));
                //sb.append(Const.CRLR);
            }

//            if (1020 < sb.toString().length() && sb.toString().length() < 1024) {
//                sb.append(";;;;;;");
//            }

            myClient = new DalimoClient(connectInfo);

            if (!myClient.connect()) {
                mUnswer = "Не удалось подключиться\r\n";
                return false;
            }

            //myClient.send(sb.toString() + Const.END_MESSAGE);

            String _1cUnswer = myClient.receive();

            myClient.disconnect();

            // А теперь обрабатываем полученное сообщение
            if (_1cUnswer.contains(FAILURE)) {
                wasError = true;
                mUnswer = _1cUnswer + Const.NewLine;
            } else {
                mUnswer = _1cUnswer;
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            wasError = true;
        }
        return !wasError;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dismiss();

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress.hide();
        }
    }

    protected String generateDataStringForReserv(Payment data) {
        // Формируем строку с данными


        String dataString = String.format(Locale.ENGLISH,
                "%s;%s;%s;%s;%.2f;%s;",
                data.getCodeAgent(), // 0
                data.getCodeClient(), // 1
                data.getDocNum(), // 2
                TimeFormatter.sdf1C.format(data.getDateDoc()), // 3
                data.payment(), // 4
                //data.visitFK() != null ? data.visitFK().toString() : ""
                "" // 5
        );

        return dataString;
    }

}