package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.VpnService;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.TaskAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.fragments.TaskAddEditFragment;
import com.madlab.mtrade.grinfeld.roman.iface.IGetTasksProgress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by GrinfeldRA on 20.03.2018.
 */

public class GetTasksTask extends AsyncTask<String, Void, List<Task>> {

    private static final String TAG = "#GetTasksTask";
    public static final String PROC_NAME = "ПолучитьЗадачиПоМенеджеруМтрейд";
    public static final String PROC_NAME_MERCH = "СписокЗадачСупервайзера";
    private IGetTasksProgress iGetTasksProgress;
    private DalimoClient mClient;

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;

    private byte status = STATUS_OK;
    private String mMessage = "";
    private ArrayList<Task> mResult = null;
    private String codeClient, fromDate, toDate;

    public GetTasksTask(String codeClient, IGetTasksProgress iGetTasksProgress, String fromDate, String toDate) {
        this.codeClient = codeClient;
        this.iGetTasksProgress = iGetTasksProgress;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }


    @Override
    protected List<Task> doInBackground(String... strings) {
        if (strings!=null){
            mMessage = strings[0];
        }
        if (mMessage == null){
            App settings = App.get(MyApp.getContext());

            StringBuilder data = App.isMerch() ? new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s;",
                    Const.COMMAND, settings.getCodeAgent(), PROC_NAME_MERCH, settings.getCodeAgent(), fromDate, toDate)) :
                    new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;",
                    Const.COMMAND, settings.getCodeAgent(), PROC_NAME, settings.getCodeAgent(), codeClient));

            mClient = new DalimoClient(Credentials.load(MyApp.getContext()));

            if (mClient.connect()) {
                mClient.send(data + Const.END_MESSAGE);

                mMessage = mClient.receive();
                status = STATUS_OK;
            } else {
                status = STATUS_ERROR;
                mMessage = mClient.LastError();
            }

            mClient.disconnect();
        }
        //Список задач нам нужно распарсить здесь, чтобы не зависал основной поток
        //Если ошибок нет
        //Ссылка на БД нужна для загрузки данных о товарах
        if (!mMessage.contains(DalimoClient.ERROR)){
            MyApp app = (MyApp)MyApp.getContext();
            mResult = new ArrayList<>();
            String[] stringsList = mMessage.split(";");
            for (String current : stringsList) {
                Task t = parseLine(app.getDB(), current);
                if (t != null){
                    mResult.add(t);
                }
            }
        }
        return mResult;
    }


    @Override
    protected void onPostExecute(List<Task> task) {
        super.onPostExecute(task);
        iGetTasksProgress.onGetTasksProgress(task);
    }



    private Task parseLine(SQLiteDatabase db, String line){
        Task t;
        try {
            ArrayList<GoodsInTask> list = new ArrayList<>();
            String[] lineSplit = line.split("[|]");
            String uuid = lineSplit[0];
            String client = lineSplit[1];
            String descr = lineSplit[2];
            String dateStr = lineSplit[3];
            byte status = 0;
                try {
                    status = Byte.parseByte(lineSplit[5]);
                }catch (NumberFormatException e){
                    String[] task_statuses = MyApp.getContext().getResources().getStringArray(R.array.task_statuses);
                    for (int i = 0; i < task_statuses.length; i++){
                        if (task_statuses[i].toUpperCase().contains(lineSplit[5].toUpperCase())){
                            status = (byte) i;
                            break;
                        }
                    }
            }
            Date due;
            try {
                due = TimeFormatter.sdf1C.parse(dateStr);
            } catch (Exception e) {
                due = new Date(0);
            }
            byte percent = (byte) GlobalProc.parseInt(lineSplit[4], 0);
            if (lineSplit.length==7){
                String table  = lineSplit[6];
                if (table!=null&&!table.isEmpty()){
                    list = parseTablePart(db, table);
                }
            }
            Client c = Client.load(db, client);

            t = new Task(UUID.fromString(uuid), App.get(MyApp.getContext()).getCodeAgent(), client, descr, due.getTime(),status, Task.SAVED_TASK, true);
            t.avgPercent(percent);
            if (c != null){
                if (c.mName != null && c.mName.length() > 0)
                    t.clientName(c.mName);
                else
                    t.clientName(c.mNameFull);
            }

            t.goodsList(list);
        }
        catch (Exception e) {
            e.printStackTrace();
            t = null;
        }
        return t;
    }

    private ArrayList<GoodsInTask> parseTablePart(SQLiteDatabase db, String tablePart){
        ArrayList<GoodsInTask> result = new ArrayList<>();
        try {
            StringTokenizer token = new StringTokenizer(tablePart, "&");
            do{
                try {
                    String item = token.nextToken();
                    String[] goods = item.split(Pattern.quote("*"));

                    String code = "";
                    short totalCount = 0;
                    short sold = 0;
                    byte percent = 0;

                    for (int i = 0; i < goods.length; i++) {
                        switch (i) {
                            case 0:
                                code = goods[i];
                                break;
                            case 1:
                                totalCount = (short)GlobalProc.parseInt(goods[i], 0);
                                break;
                            case 2:
                                sold = (short) GlobalProc.parseInt(goods[i], 0);
                                break;
                            case 3:
                                percent = (byte)GlobalProc.parseInt(goods[i], 0);
                                break;
                        }
                    }

                    Goods g = Goods.load(db, code);
                    //Если товар есть в БД - добавляем
                    if (g != null){
                        GoodsInTask git = new GoodsInTask(code, g.getName(), percent, totalCount, sold, 1);
                        result.add(git);
                    }
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }while (token.hasMoreTokens());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }

}
