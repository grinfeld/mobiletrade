package com.madlab.mtrade.grinfeld.roman.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.iface.ICheckRestComplete;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by GrinfeldRA on 26.12.2017.
 */

public class CheckRestTask extends AsyncTask<Void, Integer, Byte> {
    private final String COMMAND_CHECK_REST = "003";
    private final String SUCCESS = "OK";

    public final static byte RES_ENOUGH = 60;
    public final static byte RES_NOT_ENOUGH = 61;
    public final static byte RES_ERR = 62;

    private ProgressDialog progress;
    private Context baseContext;

    private String codeCli;
    private ArrayList<OrderItem> mItems;
    private ICheckRestComplete mTaskCompleteListener;

    private DalimoClient mClient;
    private Credentials connectInfo;

    private String mUnswer = "";

    public String getMessage() {
        return mUnswer;
    }

    public CheckRestTask(Context data, String codeCli,
                         ArrayList<OrderItem> items, ICheckRestComplete taskCompleteListener) {
        baseContext = data;
        this.codeCli = codeCli;
        mItems = items;
        mTaskCompleteListener = taskCompleteListener;
        connectInfo = Credentials.load(data);
    }

    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(baseContext);
        progress.setMessage(baseContext.getString(R.string.mes_wait_for_load));
        progress.setCancelable(true);
        progress.show();
    }

    @Override
    protected Byte doInBackground(Void... params) {
        progress.setMax(params.length);

        StringBuilder data = new StringBuilder(String.format("%s;%s;%s;",
                COMMAND_CHECK_REST, App.get(baseContext).getCodeAgent(), codeCli));

        for (OrderItem oi : mItems) {
            float cnt = 1;

            // Если товар весовой, то переводим в кг
            // если штучный, переводим коробки в штуки
            if (oi.mIsWeightGoods) {
                cnt = oi.getTotalWeight();
            } else {
                cnt = oi.getCountInPieces();
            }

            data.append(String.format(Locale.ENGLISH, "%s|%.2f;",
                    oi.getCodeGoods(), cnt));
        }

        // А теперь всё это дело запускаем
        mClient = new DalimoClient(connectInfo);

        if (!mClient.connect()) {
            return RES_ERR;
        }

        mClient.send(data + Const.END_MESSAGE);

        String message = mClient.receive();

        mClient.disconnect();

        // А теперь обрабатываем полученное сообщение
        if (message.contains(SUCCESS)) {
            return RES_ENOUGH;
        }

        if (message.contains(DalimoClient.ERROR)) {
            int index = message.indexOf(DalimoClient.ERROR);
            if (index > 0) {
                mUnswer = message.substring(0, index);
            }
            return RES_ERR;
        }

        parseMessage(message);

        return RES_NOT_ENOUGH;
    }

    @Override
    protected void onPostExecute(Byte result) {
        dismiss();

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (progress != null) {
            progress.hide();
            progress.dismiss();
        }
    }

    private void parseMessage(String data) {
        String[] blackList = data.split(";");

        for (String item : blackList) {
            int ind = item.indexOf('|');

            if (ind == -1)
                continue;

            String code = (item.substring(0, ind));

            if (code.length() == 0)
                continue;

            // теперь надо обновить данные в списке товаров
            for (OrderItem oi : mItems) {
                if (oi.getCodeGoods().equalsIgnoreCase(code)) {
                    oi.notInStock(true);
                    break;
                }
            }
        }
    }
}
