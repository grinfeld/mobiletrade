package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */

public class PhotoFilterMetaData {

    public static final String TABLE_NAME = "PhotoFilter";

    public static final DBField FIELD_CODE = new DBField("fieldCode", 0);
    public static final DBField FIELD_VALUE = new DBField("fieldValue", 1);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(20)	    NOT NULL DEFAULT '')",TABLE_NAME, FIELD_CODE, FIELD_VALUE);

    public static String insertQuery(String code, String value) {
        return String.format(Locale.ENGLISH,
                "INSERT INTO %s " + "(%s, %s) VALUES ('%s', '%s')", TABLE_NAME,FIELD_CODE,FIELD_VALUE,code,value);
    }


}
