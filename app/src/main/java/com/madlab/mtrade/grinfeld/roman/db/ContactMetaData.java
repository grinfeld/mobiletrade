package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by grinfeldra
 */
public class ContactMetaData {

    public static final String TABLE_NAME = "Contacts";

    public static final DBField FIELD_ID = new DBField("id", 0);
    public static final DBField FIELD_CODE_AGENT = new DBField("codeAgent", 1);
    public static final DBField FIELD_CODE_CLIENT = new DBField("codeClient", 2);
    public static final DBField FIELD_POST = new DBField("post", 3);
    public static final DBField FIELD_FIO = new DBField("fio", 4);
    public static final DBField FIELD_TEL = new DBField("tel", 5);
    public static final DBField FIELD_EMAIL = new DBField("email", 6);
    public static final DBField FIELD_RESPONSIBLE = new DBField("responsible", 7);
    public static final DBField FIELD_DATE_BIRTH = new DBField("dateBirth", 8);
    public static final DBField FIELD_NOTE = new DBField("note", 9);
    public static final DBField FIELD_RESERVED = new DBField("reserved", 10);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	integer	    NOT NULL DEFAULT 0,"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	text	    NOT NULL DEFAULT '',"
                    + "%s	integer	    NOT NULL DEFAULT 0)", TABLE_NAME, FIELD_ID, FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_POST, FIELD_FIO,
            FIELD_TEL, FIELD_EMAIL, FIELD_RESPONSIBLE, FIELD_DATE_BIRTH, FIELD_NOTE, FIELD_RESERVED);

    public static String insertQuery(String codeAgent, String codeClient, String post, String fio, String tel, String email,
                                     int responsible, String dateBirth, String note) {
        return String.format(Locale.ENGLISH,
                "INSERT INTO %s " + "(%s, %s, %s, %s, %s, %s, %s, %s, %s) " +
                        "VALUES " +
                        "('%s','%s', '%s', '%s', '%s', '%s', %d, '%s', '%s')",
                TABLE_NAME, FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_POST, FIELD_FIO, FIELD_TEL, FIELD_EMAIL, FIELD_RESPONSIBLE, FIELD_DATE_BIRTH, FIELD_NOTE,
                codeAgent, codeClient, post, fio, tel, email, responsible, dateBirth, note);
    }


}
