package com.madlab.mtrade.grinfeld.roman.db;

/**
 * Goods In Task MetaData
 * @author Konovaloff
 *
 */
public final class TaskItemMetaData{

    private TaskItemMetaData() {}

	public static final String TABLE_NAME = "TaskItem";

	public static final String FIELD_GOODS = "TaskItem_CodeGoods";
	public static final String FIELD_PERCENT = "TaskItem_Percent";
	public static final String FIELD_TOTAL_COUNT = "TaskItem_TotalCount";
	public static final String FIELD_SOLD = "TaskItem_Sold";
	public static final String FIELD_TASK_FK = "task_fk";
	public static final String FIELD_QUANT = "quant";

	public static final byte FIELD_GOODS_INDEX = 0;
	public static final byte FIELD_PERCENT_INDEX = 1;
	public static final byte FIELD_TOTAL_COUNT_INDEX = 2;
	public static final byte FIELD_SOLD_INDEX = 3;
	public static final byte FIELD_TASK_FK_INDEX = 4;
	public static final int FIELD_QUANT_INDEX = 5;
	
	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s ("
					+ "%s nvarchar(6)	NOT NULL DEFAULT '',"
					+ "%s INTEGER		NOT NULL DEFAULT 0,"
					+ "%s INTEGER		NOT NULL DEFAULT 0,"
					+ "%s INTEGER		NOT NULL DEFAULT 0,"
					+ "%s nvarchar(36)	NOT NULL DEFAULT '',"
					+ "%s INTEGER		NOT NULL DEFAULT 1)",
					TABLE_NAME,
					FIELD_GOODS,
					FIELD_PERCENT,
					FIELD_TOTAL_COUNT,
					FIELD_SOLD,
					FIELD_TASK_FK,
				    FIELD_QUANT);

	public static final String INDEX = String.format(
			"CREATE INDEX idxTaskItem_TaskFK ON %s ( %s )", TABLE_NAME,
			FIELD_TASK_FK);
}
