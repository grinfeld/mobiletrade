package com.madlab.mtrade.grinfeld.roman.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import com.madlab.mtrade.grinfeld.roman.entity.Geolocation;

import java.util.ArrayList;
import java.util.List;


public final class GeolocationEntityMapper {
    private GeolocationEntityMapper(){
    }

    public static long insert(SQLiteDatabase db, Geolocation entity) throws SQLException {
        ContentValues values = new ContentValues(7);
        values.put(GeolocationMetaData.COLUMN_PROVIDER, entity.getProvider());
        values.put(GeolocationMetaData.COLUMN_TIME, entity.getTime());
        values.put(GeolocationMetaData.COLUMN_LATITUDE, entity.getLatitude());
        values.put(GeolocationMetaData.COLUMN_LONGITUDE, entity.getLongitude());
        values.put(GeolocationMetaData.COLUMN_SPEED, entity.getSpeed());
        values.put(GeolocationMetaData.COLUMN_ACCURACY, entity.getAccuracy());
        values.put(GeolocationMetaData.COLUMN_SEND, entity.isSend() ? 1 : 0);

        return db.insertOrThrow(GeolocationMetaData.TABLE_NAME, null, values);
    }

    public static void delete(SQLiteDatabase database, int id){

    }

    public static List<Geolocation> get(SQLiteDatabase db, String where, String[] args,
                                        String orderBy, String limit) throws SQLException {
        List<Geolocation> result = new ArrayList<>();

        Cursor cursor = db.query(GeolocationMetaData.TABLE_NAME, null, where, args, null,
                null, orderBy, limit);

        try {

            if (cursor.getCount() <= 0) return result;

            int providerIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_PROVIDER);
            int timeIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_TIME);
            int latitudeIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_LATITUDE);
            int longitudeIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_LONGITUDE);
            int speedIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_SPEED);
            int accuracyIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_ACCURACY);
            int sendIdx = cursor.getColumnIndex(GeolocationMetaData.COLUMN_SEND);

            while (cursor.moveToNext()) {
                Geolocation geolocation = new Geolocation();
                geolocation.setProvider(cursor.getString(providerIdx));
                geolocation.setTime(cursor.getLong(timeIdx));
                geolocation.setLatitude(cursor.getDouble(latitudeIdx));
                geolocation.setLongitude(cursor.getDouble(longitudeIdx));
                geolocation.setSpeed(cursor.getFloat(speedIdx));
                geolocation.setAccuracy(cursor.getFloat(accuracyIdx));
                geolocation.setSend(cursor.getInt(sendIdx) > 0);
                result.add(geolocation);
            }

        } finally {
            cursor.close();
        }

        return result;
    }

    public static Geolocation getLastPosition(SQLiteDatabase db){
        List<Geolocation> res = get(db, null, null, GeolocationMetaData.COLUMN_TIME+" DESC", "1");
        if (res.size() > 0) return res.get(0);

        return  null;
    }
}
