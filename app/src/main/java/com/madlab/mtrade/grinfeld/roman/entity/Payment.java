package com.madlab.mtrade.grinfeld.roman.entity;



import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.PaymentsMetaData;

public class Payment {
    private static String TAG = "!->Payment";

    private int id;
    private String codeAgent;
    private String codeClient;
    private String docNum;
    private Date dateDoc;
    private float paymentSum;
    private boolean reserved;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCodeAgent(String codeAgent) {
        this.codeAgent = codeAgent;
    }

    public String getCodeAgent() {
        return codeAgent;
    }

    public void setCodeClient(String codeClient) {
        this.codeClient = codeClient;
    }

    public String getCodeClient() {
        return codeClient;
    }

    public void setDocNum(String docNum) {
        this.docNum = docNum;
    }

    public String getDocNum() {
        return docNum;
    }

    public void setDateDoc(Date dateDoc) {
        this.dateDoc = dateDoc;
    }

    public Date getDateDoc() {
        return dateDoc;
    }

    public void payment(float sumDoc) {
        this.paymentSum = sumDoc;
    }

    public float payment() {
        return paymentSum;
    }

    public void isReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public boolean isReserved() {
        return reserved;
    }

    /**
     * Ссылка на визит
     */
    private UUID mVisit_fk;

    public void visitFK(UUID id) {
        mVisit_fk = id;
    }

    public UUID visitFK() {
        return mVisit_fk;
    }

    public Payment(int _id, String agent, String client, String docNumber,
                   Date docDate, float docSum, boolean isReserved) {
        id = _id;
        codeAgent = agent;
        codeClient = client;
        docNum = docNumber;
        dateDoc = docDate;
        paymentSum = docSum;
        reserved = isReserved;
    }

    public Payment() {
        this(-1, "", "", "", Calendar.getInstance().getTime(), 0f, false);
    }

    public void updateReservedSign(SQLiteDatabase db) {
        String where = String.format("%s = %d", PaymentsMetaData._ID, id);
        try {
            ContentValues cv = new ContentValues();
            cv.put(PaymentsMetaData.FIELD_SENDED, reserved);

            db.update(PaymentsMetaData.TABLE_NAME, cv, where, null);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {

        }
    }

    /**
     * Загружает оплату с нужным ID из БД
     *
     * @param db
     *            ссылка на БД
     * @param id
     *            ИД документа
     * @return документ оплату
     */
    public static Payment load(SQLiteDatabase db, int id) {
        String sql = String.format("SELECT * FROM %s WHERE %s = %d",
                PaymentsMetaData.TABLE_NAME, PaymentsMetaData._ID, id);
        Payment result = null;
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, null);

            if (rows.moveToFirst()) {
                result = new Payment();
                // ID
                result.setId(id);
                // CodeAgent
                result.setCodeAgent(rows
                        .getString(PaymentsMetaData.FIELD_CODE_AGENT_INDEX));
                // CodeClient
                result.setCodeClient(rows
                        .getString(PaymentsMetaData.FIELD_CODE_CLIENT_INDEX));
                // docNumber
                result.setDocNum(rows
                        .getString(PaymentsMetaData.FIELD_DOC_NUMBER_INDEX));
                // doc date
                String dtString = rows
                        .getString(PaymentsMetaData.FIELD_DOC_DATE_INDEX);
                result.setDateDoc(TimeFormatter.parseSqlDate(dtString));
                // payment
                result.payment(rows
                        .getFloat(PaymentsMetaData.FIELD_AMOUNT_INDEX));
                // sign, is payment sended
                result.isReserved(rows
                        .getShort(PaymentsMetaData.FIELD_SENDED_INDEX) == 1);
                // Visit UUID FK
                try {
                    result.visitFK(UUID.fromString(rows
                            .getString(PaymentsMetaData.FIELD_VISIT_FK_INDEX)));
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }
}
