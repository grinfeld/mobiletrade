package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.components.WheelView;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.wheel.OnWheelChangedListener;
import com.madlab.mtrade.grinfeld.roman.wheel.adapters.ArrayWheelAdapter;

import java.util.Locale;

import static android.app.Activity.RESULT_OK;

/**
 * Created by GrinfeldRA on 13.11.2017.
 */

public class GoodsQuantityFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "#GoodsQuantityFragment";
    //Текст с итоговой суммой
    private TextView tvTotalSum;

    //Текст с итоговым весом
    private TextView tvTotalWeight;

    //Текст с количеством
    private EditText etCount;

    //Текст с ценой
    private EditText etPrice;

    private final String HEAD = "гол.";
    private final String BOX = "кор.";

    //Макс скидка
    private final static byte MAX_DISCOUNT = 0;
    //Макс наценка
    private final static byte MAX_MARGIN = 20;

    private Goods mGoods;
    private OrderItem mOrderItem;

    //Базовый уровень цен. Ставим Стандарт
    private byte mPriceLvl = 0;

    //Цена от которой считаем скидку
    private float mBasePrice;

    //Цена, ниже которой скидку нельзя ставить
    private float mContractPrice;

    private float price = 0;

    private WheelView wvMeash;

    private TextWatcher mEditCountListener = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void afterTextChanged(Editable arg0) {
            if (mOrderItem != null) {
                if (mOrderItem.inPack())
                    mOrderItem.setCountPack(GlobalProc.parseInt(arg0.toString()));
                else {
                    if (!mOrderItem.mIsWeightGoods && mOrderItem.getQuant() > 1) {
                        int enteredValue = GlobalProc.parseInt(arg0.toString());
                        int realCnt = (int) Math.ceil(enteredValue / mOrderItem.getQuant());
                        mOrderItem.setCount(realCnt);
                    } else
                        mOrderItem.setCount(GlobalProc.parseInt(arg0.toString()));
                }
                printTotalWeight();
                printAmount();
            }
        }
    };

    private TextWatcher mEditPriceListener = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void afterTextChanged(Editable arg0) {
            if (mOrderItem != null) {
                mOrderItem.setPrice(GlobalProc.parseFloat(arg0.toString()));
                printAmount();
            }
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mGoods = getArguments().getParcelable(Goods.KEY);
        mOrderItem = getArguments().getParcelable(OrderItem.KEY);
        if (mOrderItem != null) {
            if (mGoods == null) {
                MyApp app = (MyApp) getActivity().getApplication();
                mGoods = Goods.load(app.getDB(), mOrderItem.getCodeGoods());
            }
        }

        //надо рассчитать цену товара, ещё раз. Чтоб всё было корректно
//        if (Order.getOrder() != null && Order.getOrder().getClient() != null){
//            Client client = Order.getOrder().getClient();
//            mPriceLvl = client.mPriceType;
//            mGoods.calcVisiblePrice(mPriceLvl, client.mDiscount);
//        }
        //только теперь, после расчёта цен, можно создать элементы заявки, если его сюда не передали
        if (mOrderItem == null && mGoods != null) {
            mOrderItem = new OrderItem(mGoods);
            //Проставляем признак серт. и кач.
            if (Order.getOrder() != null) {
                mOrderItem.isSertificateNeed(Order.getOrder().mSertificates);
                mOrderItem.isQualityDocNeed(Order.getOrder().mQualityDocs);
            }
        }
        price = mGoods.price();
        if (price == 0) {
            Client client;
            if (App.isMerch()) {
                client = Task.getCurrentTask().getClient();
            } else {
                client = Order.getOrder().getClient();
            }
            mPriceLvl = client.mPriceType;
            float discount = (mGoods.getPrice(mPriceLvl) / 100) * (client.mDiscount * -1);
            float price = mGoods.getPrice(mPriceLvl) + discount;
            Log.d(TAG, "discount " + discount + ", client.mPriceType " + price);
            this.price = price;
        }
        mBasePrice = mGoods.getPrice(mPriceLvl);
        mContractPrice = price;
//        if (mBasePrice != mOrderItem.getPrice()){
//            mOrderItem.mIsSpecialPrice = true;
//        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            if (App.isMerch()) {
                supportActionBar.setTitle("Новая задача");
            } else {
                supportActionBar.setTitle("Новая заявка");
            }
            supportActionBar.setSubtitle("Добавление товара");

        }
        return inflater.inflate(R.layout.fragment_goods_quantity, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        printGoodsInfo(view);
        printOrderItemInfo(view);
        initCheckBoxes(view);
        initButtons(view);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gq_btOK:
                String s = etPrice.getText().toString();
                boolean foreignAgent = Order.getOrder().getClient().isForeignAgent();
                String substring = s.substring(0, s.length() - 1);
                float price = Float.parseFloat(substring.contains(",")
                        ? substring.replaceAll(",", ".") : substring) *
                        (foreignAgent ? Float.parseFloat(GlobalProc.getPref(getActivity(), R.string.pref_foreign_price, "1")) : 1f);
                if (price < mContractPrice) {
                    mOrderItem.setPrice(mContractPrice);
                } else {
                    mOrderItem.setPrice(price);
                }
                Intent extras = new Intent();
                extras.putExtra(OrderItem.KEY, mOrderItem);
                Fragment targetFragment = getTargetFragment();
                int targetRequestCode = getTargetRequestCode();
                targetFragment.onActivityResult(targetRequestCode, Activity.RESULT_OK, extras);
                getFragmentManager().popBackStack();
                break;
            case R.id.gq_btCancel:
                getFragmentManager().popBackStack();
                break;
        }
    }


    /**
     * Выводим информацию о товаре на экран
     */
    private void printGoodsInfo(View v) {
        // Имя товара
        TextView tvName = v.findViewById(R.id.gq_tvNameGoods);
        tvName.setText(mGoods.getNameFull());

        // Количество в упаковке
        TextView tvNumInPack = v.findViewById(R.id.gq_tvNumInPack);
        tvNumInPack.setText(String.format("%.2f %s", mGoods.getNumInPack(),
                mGoods.getBaseMeash()));

        // Квант
        TextView tvQuant = v.findViewById(R.id.gq_tvQuant);
        tvQuant.setText(Float.toString(mGoods.getQuant()) + " "
                + mGoods.getBaseMeash());

        // Остаток на складе
        TextView tvRest = v.findViewById(R.id.gq_tvRest);
        tvRest.setText(Float.toString(mGoods.getRestOnStore()) + " "
                + mGoods.getBaseMeash());

        // Базовая цена, от которой рассчитывется скидка
        TextView tvPriceStandart = v.findViewById(R.id.gq_tvPriceStandart);
        tvPriceStandart.setText(GlobalProc.toForeignPrice(mBasePrice, Order.getOrder().getClient().isForeignAgent()));

        initWheels(v);
    }

    private void initWheels(View v) {
        wvMeash = v.findViewById(R.id.gq_wheelMeash);
        // Настраиваем колесо ед. изм.
        if (wvMeash != null) {
            String list[];
            if (!mGoods.isWeightGood()) {
                if (mGoods.getBaseMeash().equalsIgnoreCase(
                        mGoods.getPackMeash())) {
                    wvMeash.setEnabled(false);
                    list = new String[]{mGoods.getBaseMeash()};
                } else {
                    list = new String[]{mGoods.getBaseMeash(),
                            mGoods.getPackMeash()};
                }
            } else {
                list = new String[]{HEAD, BOX};
            }
            ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<>(getActivity(), list);
            wvMeash.setViewAdapter(awa);
            wvMeash.setVisibleItems(App.get(getActivity()).getVisibleWheelItems());
            if (mOrderItem != null) {
                wvMeash.setCurrentItem(mOrderItem.inPack() ? 1 : 0);
            } else {
                wvMeash.setCurrentItem(0);
            }
            // Слушатель изменения штуки/упаковки
            wvMeash.addChangingListener(new OnWheelChangedListener() {
                @Override
                public void onChanged(WheelView wheel, int oldValue, int newValue) {
                    mOrderItem.changePack(newValue == 1);
                    // теперь надо обновить информацию на экране
                    printAmount();
                    printCount();
                    printTotalWeight();
                }
            });
        }
        WheelView wvDiscount = v.findViewById(R.id.gq_wheelDiscount);
        // Настраиваем колесо со скидкой
        if (wvDiscount != null) {
            int total = MAX_DISCOUNT + MAX_MARGIN + 1;
            String list[] = new String[total];
            for (int i = 0; i < total; i++) {
                list[i] = Integer.toString(i - MAX_DISCOUNT);
            }
            ArrayWheelAdapter<String> awa = new ArrayWheelAdapter<>(getActivity(), list);
            wvDiscount.setViewAdapter(awa);
            wvDiscount.setVisibleItems(App.get(getActivity()).getVisibleWheelItems());
            if (mOrderItem.getPrice() != price) {
                float onePercent = (price / 100);
                float disc = ((price - mOrderItem.getPrice()) / onePercent);
                //int discount = (int)Math.floor(((mBasePrice - mOrderItem.getPrice())/mBasePrice)*100);
                int pos = MAX_DISCOUNT - Math.round(disc);
                //Надо скорректировать положение колеса из-за наличия нуля
//				if (discount < 0) pos++;
                //           if (discount > 0) pos--;
                //            if (pos >= 24) pos = 23;
                wvDiscount.setCurrentItem(pos);
            } else {
                wvDiscount.setCurrentItem(MAX_DISCOUNT);
            }
            // Слушатель изменения
            wvDiscount.addChangingListener(new OnWheelChangedListener() {
                @Override
                public void onChanged(WheelView wheel, int oldValue, int newValue) {
                    int discount = newValue - MAX_DISCOUNT;
                    float onePercent = (price / 100);
                    float delta = GlobalProc.round(onePercent * discount, 2);
                    float newPrice = price + delta;
                    if (newPrice < mContractPrice) newPrice = mContractPrice;
                    mOrderItem.setPrice(newPrice);
                    // теперь надо обновить информацию на экране
                    printPrice();
                    printAmount();
                }
            });
        }
    }


    /**
     * Выводим информацию об элементе заявки, добавляем слушатели для изменения.
     * Цена, количество, итоги.
     */
    private void printOrderItemInfo(View v) {
        // Итоговая сумма
        tvTotalSum = v.findViewById(R.id.gq_tvTotalSum);
        printAmount();
        // Вес
        tvTotalWeight = v.findViewById(R.id.gq_tvWeight);
        printTotalWeight();
        // Количество
        etCount = v.findViewById(R.id.gq_etCount);
        printCount();
        if (etCount != null) {
            etCount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    EditText etCount = (EditText) v;
                    if (hasFocus) {
                        etCount.addTextChangedListener(mEditCountListener);
                        int ln;
                        try {
                            ln = etCount.getText().toString().length();
                        } catch (Exception e) {
                            ln = 0;
                        }
                        etCount.setSelection(ln, ln);
                    } else {
                        etCount.removeTextChangedListener(mEditCountListener);
                    }

                }
            });
        }
        // Цена
        etPrice = v.findViewById(R.id.gq_etPrice);
        printPrice();
        //etPrice.setText(GlobalProc.formatMoney(mBasePrice));
    }

    private void initButtons(View v) {
        View btOK = v.findViewById(R.id.gq_btOK);
        View btCancel = v.findViewById(R.id.gq_btCancel);
        btOK.setOnClickListener(this);
        btCancel.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GlobalProc.closeKeyboard(getActivity());
    }

    private void initCheckBoxes(View v) {
        if (mOrderItem == null) return;
        CheckBox cbHalfHead = v.findViewById(R.id.gq_cbHalfHead);
        if (cbHalfHead != null) {
            cbHalfHead.setVisibility(mGoods.isHalfHead() ? View.VISIBLE : View.GONE);
            cbHalfHead.setChecked(mOrderItem.mIsHalfHead);
            cbHalfHead.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton source, boolean value) {
                    mOrderItem.mIsHalfHead = value;
                    //Блокируем ввод количества, меняем сумму и вес
                    if (value) {
                        etCount.setEnabled(false);
                        wvMeash.setEnabled(false);
                        mOrderItem.recalcAmount();
                    } else {
                        etCount.setEnabled(true);
                        wvMeash.setEnabled(true);
                        mOrderItem.recalcAmount();
                    }
                }
            });
        }
        CheckBox cbSpecPrice = v.findViewById(R.id.gq_cbSpecialPrice);
        if (cbSpecPrice != null) {
            cbSpecPrice.setChecked(mOrderItem.mIsSpecialPrice);
            View rlPrice = getView().findViewById(R.id.gq_rlPrice);
            if (rlPrice != null)
                rlPrice.setVisibility(mOrderItem.mIsSpecialPrice ? View.VISIBLE : View.GONE);
            cbSpecPrice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton source, boolean value) {
                    if (!value) {
                        mOrderItem.setPrice(mGoods.price());
                    }
                    View rlPrice = getView().findViewById(R.id.gq_rlPrice);
                    if (rlPrice != null)
                        rlPrice.setVisibility(value ? View.VISIBLE : View.GONE);
                    mOrderItem.mIsSpecialPrice = value;
                }
            });
        }
        CheckBox cbSertificat = v.findViewById(R.id.gq_cbSertificat);
        if (cbSertificat != null) {
            cbSertificat.setChecked(mOrderItem.isSertificateNeed());
            cbSertificat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton source, boolean value) {
                    mOrderItem.isSertificateNeed(value);
                }
            });
        }
//        CheckBox cbQuality = v.findViewById(R.id.gq_cbQualityDoc);
//        if (cbQuality != null){
//            cbQuality.setChecked(mOrderItem.isSertificateNeed());
//            cbQuality.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton source, boolean value) {
//                    mOrderItem.isQualityDocNeed(value);
//                }
//            });
//        }
    }

    private void printAmount() {
        if (mOrderItem != null && tvTotalSum != null) {
            tvTotalSum.setText(GlobalProc.toForeignPrice(mOrderItem.getAmount(), Order.getOrder().getClient().isForeignAgent()));
        }
    }

    private void printTotalWeight() {
        if (mOrderItem != null) {
            tvTotalWeight.setText(GlobalProc.formatWeight(mOrderItem.getTotalWeight()));
        }
    }

    private void printCount() {
        if (mOrderItem != null && etCount != null) {
            String format = "%d";
            String cnt;
            if (mOrderItem.mIsWeightGoods) {
                if (mOrderItem.isFractional()) {
                    format = "%d";
                    if (mOrderItem.inPack()) {
                        cnt = String.format(Locale.ENGLISH, format, mOrderItem.getCountPack());
                    } else {
                        cnt = String.format(Locale.ENGLISH, format, mOrderItem.getCount());
                    }
                } else {
                    format = "%d";
                    if (mOrderItem.inPack()) {
                        cnt = String.format(Locale.ENGLISH, format, mOrderItem.getCountPack());
                    } else {
                        cnt = String.format(Locale.ENGLISH, format, mOrderItem.getCount());
                    }
                }
            } else {
                if (mOrderItem.inPack()) {
                    cnt = String.format(Locale.ENGLISH, format, mOrderItem.getCountPack());
                } else {
                    int count = mOrderItem.getCount() * (int) mOrderItem.getQuant();
                    cnt = String.format(Locale.ENGLISH, format, count);
                }
            }
            etCount.setText(cnt);
        }
    }

    private void printPrice() {
        if (etPrice != null) {
            etPrice.setText(GlobalProc.toForeignPrice(mOrderItem.getPrice(), Order.getOrder().getClient().isForeignAgent()));
            etPrice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        etPrice.addTextChangedListener(mEditPriceListener);
                    } else {
                        etPrice.removeTextChangedListener(mEditPriceListener);
                    }
                }
            });
        }
    }

}
