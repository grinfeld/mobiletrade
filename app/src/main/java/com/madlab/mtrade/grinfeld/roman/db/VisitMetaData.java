package com.madlab.mtrade.grinfeld.roman.db;


import android.provider.BaseColumns;

public class VisitMetaData implements BaseColumns {
    public static final String TABLE_NAME = "Visit";

    public static final String FIELD_KEY = "Key";
    public static final String FIELD_LOCATION_FK = "VisitLocation";
    public static final String FIELD_START_TIME = "StartTime";
    public static final String FIELD_END_TIME = "EndTime";
    public static final String FIELD_CLIENT_CODE = "VisitClientCode";
    public static final String FIELD_ORDER_SKU = "OrderSKU";
    public static final String FIELD_ORDER_SUM = "OrderSum";
    public static final String FIELD_STATE = "VisitState";
    public static final String FIELD_RESULT = "VisitResult";
    public static final String FIELD_TYPE_SERVER = "VisitTypeServer";
    public static final String FIELD_IS_RESERVED = "isReserved";

    public static final byte FIELD_ID_INDEX = 0;
    public static final byte FIELD_KEY_INDEX = 1;
    public static final byte FIELD_LOCATION_FK_INDEX = 2;
    public static final byte FIELD_START_TIME_INDEX = 3;
    public static final byte FIELD_END_TIME_INDEX = 4;
    public static final byte FIELD_CLIENT_CODE_INDEX = 5;
    public static final byte FIELD_ORDER_SKU_INDEX = 6;
    public static final byte FIELD_ORDER_SUM_INDEX = 7;
    public static final byte FIELD_STATE_INDEX = 8;
    public static final byte FIELD_RESULT_INDEX = 9;
    public static final byte FIELD_TYPE_SERVER_INDEX = 10;
    public static final byte FIELD_IS_RESERVED_INDEX = 11;


    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	INTEGER			PRIMARY KEY AUTOINCREMENT, "
                    + "%s	NVARCHAR(36)    NOT NULL DEFAULT '', "
                    + "%s	INTEGER			NOT NULL DEFAULT -1, "
                    + "%s	DATETIME		NOT NULL DEFAULT 'now', "
                    + "%s	DATETIME		NOT NULL DEFAULT 'now', "
                    + "%s	NVARCHAR(6)		NOT NULL DEFAULT '', "
                    + "%s	INTEGER			NOT NULL DEFAULT 0, "
                    + "%s	MONEY			NOT NULL DEFAULT 0, "
                    + "%s	INTEGER			NOT NULL DEFAULT 0, "
                    + "%s	TEXT		    NOT NULL DEFAULT '',"
                    + "%s	INTEGER			NOT NULL DEFAULT 0,"
                    + "%s	INTEGER			NOT NULL DEFAULT 0)", TABLE_NAME, _ID,
            FIELD_KEY, FIELD_LOCATION_FK, FIELD_START_TIME, FIELD_END_TIME,
            FIELD_CLIENT_CODE, FIELD_ORDER_SKU, FIELD_ORDER_SUM, FIELD_STATE, FIELD_RESULT, FIELD_TYPE_SERVER,
            FIELD_IS_RESERVED);

}
