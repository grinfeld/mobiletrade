package com.madlab.mtrade.grinfeld.roman.tasks;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.connectivity.ApiPromotions;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.PromotionBonusesMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionCheckedMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionItemMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionProductMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.Datum;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsBean;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsItemBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by grinfeldra
 */
public class GetPromotionRx {

    String TAG = "GetPromotionRx";

    public interface IPromotionEvent {
        void onNext(int page);

        void onError(Throwable t);

        void onComplete();
    }

    private IPromotionEvent promotionEvent;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private ApiPromotions api = MyApp.getApi();
    private String regionId;
    private MyApp myApp;

    public GetPromotionRx(IPromotionEvent promotionEvent) {
        this.promotionEvent = promotionEvent;
        regionId = App.getRegionId(MyApp.getContext());
        myApp = (MyApp) MyApp.getContext();
    }

    public void onStart(int page) {
        final int[] lastPage = new int[1];
        Map<String, String> param = new LinkedHashMap<>();
        param.put("city_id", regionId);
        param.put("sort", "-weight");
        param.put("page", String.valueOf(page));
        mCompositeDisposable.add(api.getPromotions(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap((Function<PromotionsBean, Observable<Datum>>) promotions -> {
                    lastPage[0] = promotions.getMeta().getLastPage();
                    return Observable.fromIterable(promotions.getData());
                })
                .flatMap((Function<Datum, Observable<PromotionsItemBean>>) datum -> {
                    writeDBPromotion(datum);
                    return api.getPromotionItem(datum.getId(), param)
                            .subscribeOn(Schedulers.io());
                })
                .doOnComplete(() -> {
                    if (page < lastPage[0]) {
                        promotionEvent.onNext(page + 1);
                    } else {
                        promotionEvent.onComplete();
                    }
                })
                .subscribe(this::writeDBPromotionItem,
                        throwable -> promotionEvent.onError(throwable))
        );
    }

    private void writeDBPromotion(Datum datum) {
        SimpleDateFormat pattern = new SimpleDateFormat("dd.MM.yyyy hh:mm", Locale.getDefault());
        String timeStamp = pattern.format(new Date());
        String sql = PromotionMetaData.insertQuery(datum.getId(), datum.getTitle(),
                datum.getText().replaceAll("'", "’"), datum.getCover(), timeStamp);
        DBHelper.execMultipleSQLTXN(myApp.getDB(), new String[]{sql,
                PromotionCheckedMetaData.insertQuery(datum.getId(), Integer.parseInt(regionId))});
    }

    private void writeDBPromotionItem(PromotionsItemBean promotionsItemBean) {
        List<String> sqlCommandList = new ArrayList<>();
        PromotionsItemBean.Data data = promotionsItemBean.getData();
        sqlCommandList.add(PromotionItemMetaData.insertQuery(data.getId(), data.getTitle(),
                data.getText().replaceAll("'", "’"), data.getCover(), data.getQuantum(),
                data.getStartAt().getDate(), data.getCloseAt().getDate(), data.getCounts()));
        for (PromotionsItemBean.Product_ product : data.getProducts()) {
            if (product != null) {
                String originId = "";
                int id = -1;
                if (product.getOriginId() != null) {
                    originId = product.getOriginId();
                }
                if (data.getId() != null) {
                    id = data.getId();
                }
                sqlCommandList.add(PromotionProductMetaData.insertQuery(originId, id));
            }
        }
        for (PromotionsItemBean.Bonuse bonuse : data.getBonuses()) {
            if (bonuse != null) {
                String bonuseID = "";
                String quntum = "";
                int count = 0;
                int id = -1;
                if (bonuse.getProduct() != null && bonuse.getProduct().getOriginId() != null) {
                    bonuseID = bonuse.getProduct().getOriginId();
                }
                if (bonuse.getQuantum() != null) {
                    quntum = bonuse.getQuantum();
                }
                if (bonuse.getCounts() != null) {
                    count = bonuse.getCounts();
                }
                if (data.getId() != null) {
                    id = data.getId();
                }
                sqlCommandList.add(PromotionBonusesMetaData.insertQuery(bonuseID, quntum, count, id));
            }
        }
        String[] sql = new String[sqlCommandList.size()];
        sql = sqlCommandList.toArray(sql);
        DBHelper.execMultipleSQLTXN(myApp.getDB(), sql);
    }


}
