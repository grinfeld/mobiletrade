package com.madlab.mtrade.grinfeld.roman.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.PlanSales;
import java.util.List;

/**
 * Created by GrinfeldRa on 16.04.2018.
 */

public class PlanSalesAdapter extends BaseAdapter {

    private Activity activity;
    private List<PlanSales> planSalesArrayList;

    public PlanSalesAdapter(Activity activity, List<PlanSales> planSalesArrayList) {
        this.activity = activity;
        this.planSalesArrayList = planSalesArrayList;
    }

    @Override
    public int getCount() {
        return planSalesArrayList.size();
    }

    @Override
    public PlanSales getItem(int i) {
        return planSalesArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = view;
        PlanSalesAdapter.ViewHolder holder;
        if (rowView == null) {
            LayoutInflater vi = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.item_plan_sales, null);
            holder = new PlanSalesAdapter.ViewHolder();
            holder.txtGroupGoodsName = rowView.findViewById(R.id.txt_goods_group_name);
            holder.txtSku = rowView.findViewById(R.id.txt_sku);
            holder.txtPercentage = rowView.findViewById(R.id.txt_percentage);
            rowView.setTag(holder);
        } else {
            holder = (PlanSalesAdapter.ViewHolder)rowView.getTag();
        }
        PlanSales item = getItem(i);
        holder.txtGroupGoodsName.setText(item.getGroupGoodsName());
        holder.txtSku.setText(String.valueOf(item.getSkuFact()+"/"+item.getSkuPlan()));
        int percentage = item.getPercentageCompletion();
        holder.txtPercentage.setText(" вып: "+String.valueOf(percentage+"%"));
        if (percentage < 80){
            holder.txtPercentage.setTextColor(Color.RED);
        }else if (percentage < 90 && percentage > 79){
            holder.txtPercentage.setTextColor(Color.YELLOW);
        }else {
            holder.txtPercentage.setTextColor(Color.GREEN);
        }
        return rowView;
    }

    private static class ViewHolder {
        TextView txtGroupGoodsName;
        TextView txtSku;
        TextView txtPercentage;
    }

}