package com.madlab.mtrade.grinfeld.roman;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.util.Log;

public class TimeFormatter {
    private static String TAG = "!->TimeFormatter";

    public static final String FORMAT_DATE_SQL = "yyyy-MM-dd";
    public static final String FORMAT_DATE_AND_TIME_SQL = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE_1C = "dd.MM.yy";
    public static final String FORMAT_DATE_AND_TIME = "dd.MM.yyyy HH:mm";
    public static final String FORMAT_DATE = "dd.MM.yyyy";
    private static final String TIME_FORMAT = "HH:mm:ss";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT, Locale.getDefault());


    public static TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone("GMT+0");

    public static SimpleDateFormat sdfTaskMerch = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());

    public static SimpleDateFormat sdf1C = new SimpleDateFormat(FORMAT_DATE_1C, Locale.getDefault());

    public static SimpleDateFormat sdfSQL = new SimpleDateFormat(
            FORMAT_DATE_SQL, Locale.getDefault());

    private static SimpleDateFormat sdfSQLWithTime = new SimpleDateFormat(
            FORMAT_DATE_AND_TIME_SQL, Locale.getDefault());


    public static SimpleDateFormat sdfDT = new SimpleDateFormat(
            FORMAT_DATE_AND_TIME, Locale.getDefault());


    public static SimpleDateFormat sdfDate = new SimpleDateFormat(
            FORMAT_DATE, Locale.getDefault());

    public static Date parseSqlDate(String sqlDate) {
        return parseSqlDate(sqlDate, FORMAT_DATE_SQL);
    }

    public static Date parseSqlDateWithTime(String sqlDate) {
        return parseSqlDate(sqlDate, FORMAT_DATE_AND_TIME_SQL);
    }

    public static String formatSQLWithTime(long time) {
        //sdfSQLWithTime.setTimeZone(TimeZone.getTimeZone("GMT+0"));
        //return sdfSQLWithTime.format(time);
        return new SimpleDateFormat(FORMAT_DATE_AND_TIME_SQL, Locale.getDefault()).format(new Date(time));
    }

    public static String formatTo1CDateTimeGMT(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE_AND_TIME, Locale.getDefault());
        simpleDateFormat.setTimeZone(DEFAULT_TIMEZONE);
        return simpleDateFormat.format(time);
    }

    public static String formatTo1CDate(Date time) {
        return sdfDT.format(time);
    }

    /**
     * Преобразует строку с датой, возвращаемую SQLite в строку, понятную для 1С
     * "yyyy-MM-dd" в "dd.MM.yy" Если входную строку преобразовать не
     * получается, возвращает текущую дату
     *
     * @param sqlDate Строка в формате yyyy-MM-dd
     * @return String, дата в формате dd.MM.yy
     */
    public static String parseSqlTo1CDate(String sqlDate) {
        return sdf1C.format(parseSqlDate(sqlDate, FORMAT_DATE_SQL));
    }

    public static String convertDate1CToSQL(String dateCandidat) {
        SimpleDateFormat sdfSQL = new SimpleDateFormat(FORMAT_DATE_SQL,
                Locale.getDefault());
        String res = "";
        Date date = Calendar.getInstance().getTime();

        if (dateCandidat.length() < FORMAT_DATE_1C.length()) {
            res = sdfSQL.format(date);
        } else {
            try {
                Date tmp = sdf1C.parse(dateCandidat);
                res = sdfSQL.format(tmp);
            } catch (Exception e) {
                // Log.e(TAG, e.toString());
                res = sdfSQL.format(date);
            }
        }
        return res;
    }

    public static Date parseSqlDate(String sqlDate, String pattern) {
        return new Date(parseSqlDateToLong(sqlDate, pattern));
    }

    public static long parseSqlDateWithTimeToLong(String sqlDate) {
        return parseSqlDateToLong(sqlDate, FORMAT_DATE_AND_TIME_SQL);
    }

    public static long parseSqlDateToLong(String sqlDate, String pattern) {
        long res;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdfSQL = new SimpleDateFormat(pattern);
        try {
            res = sdfSQL.parse(sqlDate).getTime();
        } catch (Exception e) {
            res = System.currentTimeMillis();
            Log.e(TAG + ".parseSqlDateToLong", e.toString());
        }
        return res;
    }

    /**
     * Парсим дату в формате dd.MM.yyyy
     */
    public static Date parse1CDate(String candidat) {
        Date result;
        try {
            result = sdfDate.parse(candidat);
        } catch (ParseException e) {
            Log.e(TAG, e.toString());
            result = new Date(System.currentTimeMillis());
        }
        return result;
    }

    public static String formatDateTime(long time) {
        DATE_TIME_FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
        return DATE_TIME_FORMATTER.format(time);
    }
}
