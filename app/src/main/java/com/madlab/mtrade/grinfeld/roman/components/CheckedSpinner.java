package com.madlab.mtrade.grinfeld.roman.components;

import java.util.ArrayList;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.view.View.OnClickListener;

import com.madlab.mtrade.grinfeld.roman.R;

public class CheckedSpinner extends RelativeLayout implements OnClickListener {
	CheckBox checkBox;
	Spinner spinner;

	ArrayAdapter<String> adapter = null;

	public CheckedSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		ctor();
	}

	public CheckedSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);

		ctor();
	}

	public CheckedSpinner(Context context) {
		super(context);

		ctor();
	}

	private void ctor() {
		String infService = Context.LAYOUT_INFLATER_SERVICE;
		LayoutInflater li;
		li = (LayoutInflater) getContext().getSystemService(infService);
		li.inflate(R.layout.checked_spinner, this, true);

		checkBox = (CheckBox) findViewById(R.id.checkBox);
		spinner = (Spinner) findViewById(R.id.spinner);

		hookupButton();
	}

	private void hookupButton() {
		if (checkBox != null) {
			checkBox.setOnClickListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		if (spinner != null) {
			spinner.setEnabled(checkBox.isChecked());
		}
	}

	public void setChecked(boolean value) {
		if (checkBox != null) {
			checkBox.setChecked(value);
			spinner.setEnabled(value);
		}
	}

	public void setSpinnerItems(ArrayList<String> items) {
		if (spinner != null && items != null) {
			int spinner_dd_item = android.R.layout.simple_spinner_dropdown_item;
			adapter = new ArrayAdapter<String>(getContext(),
					android.R.layout.simple_spinner_item, items);
			adapter.setDropDownViewResource(spinner_dd_item);
			spinner.setAdapter(adapter);
		}
	}

	public String getSelected() {

		if (checkBox != null && checkBox.isChecked() && spinner != null) {
			String item = null;
			if (spinner.getSelectedItem() != null) {
				item = spinner.getSelectedItem().toString();
			}
			return item;
		}

		return null;
	}

	public void setSelected(String value) {
		if (adapter != null) {
			int position = adapter.getPosition(value);
			if (spinner != null) {
				spinner.setSelection(position);
			}
		}
	}
}
