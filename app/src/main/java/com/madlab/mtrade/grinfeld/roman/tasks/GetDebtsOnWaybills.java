package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.iface.IGetDebtsOnWaybills;

import java.util.Locale;

/**
 * Created by GrinfeldRA on 06.06.2018.
 */

public class GetDebtsOnWaybills extends AsyncTask<Void,Void,String> {


    private static final String TAG = "#GetDebtsOnWaybills";
    private DalimoClient mClient;
    private Context context;
    private String PROC_NAME = "ДолгиПоНакладным";
    private String codeClient;

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;
    private String mMessage;
    private byte status;
    private  IGetDebtsOnWaybills callback;


    public GetDebtsOnWaybills(Context context, IGetDebtsOnWaybills callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(Void... voids) {
        App settings = App.get(context);
        StringBuilder data = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s",
                Const.COMMAND, settings.getCodeAgent(), PROC_NAME, settings.getCodeAgent(), Const.END_MESSAGE));

        mClient = new DalimoClient(Credentials.load(context));

        if (mClient.connect()) {
            mClient.send(data.toString());
            mMessage = mClient.receive();
            status = STATUS_OK;
        } else {
            status = STATUS_ERROR;
            mMessage = mClient.LastError();
        }
        Log.d(TAG, mMessage);
        mClient.disconnect();

        return mMessage;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callback.onCompleteGetDebtsOnWaybills(this);
    }
}
