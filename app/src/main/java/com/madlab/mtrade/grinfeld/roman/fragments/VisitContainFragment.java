package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.CamActivity;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.components.DatePickerFragment;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.db.LocationMetadata;
import com.madlab.mtrade.grinfeld.roman.db.MonitoringMetaData;
import com.madlab.mtrade.grinfeld.roman.db.TaskLab;
import com.madlab.mtrade.grinfeld.roman.db.TaskMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;
import com.madlab.mtrade.grinfeld.roman.entity.Monitoring;
import com.madlab.mtrade.grinfeld.roman.entity.MonitoringProduct;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.eventbus.DateChanged;
import com.madlab.mtrade.grinfeld.roman.fragments.contacts.ContactListFragment;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IFetchLocationTask;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveTaskComplete;
import com.madlab.mtrade.grinfeld.roman.services.SendTasksIntentService;
import com.madlab.mtrade.grinfeld.roman.services.UploadFileIntentService;
import com.madlab.mtrade.grinfeld.roman.tasks.DocumentsReserver;
import com.madlab.mtrade.grinfeld.roman.tasks.FetchLocationTask;
import com.madlab.mtrade.grinfeld.roman.tasks.InitTime;
import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by GrinfeldRA on 24.10.2017.
 */

public class VisitContainFragment extends Fragment implements View.OnClickListener, IFetchLocationTask, ISetReserveTaskComplete {


    public final static byte IDD_NEW_ORDER = 10;
    public final static byte IDD_NEW_RETURN = 11;
    public final static byte IDD_NEW_PAYMENT = 12;
    public final static byte IDD_NEW_PHOTO = 13;
    public final static byte IDD_NEW_RESULT = 14;
    private static final int MESSAGE_UPDATE_LIST = 0;
    public final static byte IDD_EDIT_ORDER = 15;
    public final static byte IDD_EDIT_RETURN = 16;

    public final static byte IDD_NEW_MONITORING = 17;
    public final static byte IDD_EDIT_MONITORING = 18;
    public static final int IDD_NEW_TASK = 19;
    public static final int IDD_EDIT_TASK = 20;


    private static final String TAG = "#VisitFragmentContain";
    private static final String ADDRESS_KEY = "address";
    public static final String UUID_KEY = "uuid_key";
    public static final String LOCATION_ID = "location_id";
    public static final String CLIENT_CODE = "client_code";
    private ImageButton btOrder;
    private ImageButton btReturn;
    private ImageButton btPayment;
    private ImageButton btPhoto;
    private ImageButton btResult;
    private ImageButton btContacts;
    private ImageButton bt_create_task, imgBtnUpdateTask;
    private TextView txtAddress;
    private Button btn_from_date, btn_to_date;
    private Client mClient;
    private UUID mID;
    private Visit mVisit;
    private String mCurrentPhotoPath;
    private View error_locationView;
    private View viewBtnRepeatLocation;
    private int locationID;
    private ProgressBar progressBar, progressBarPhoto;
    Date fromDate, toDate;
    private State mOrder;
    private State mReturn;
    private State mMonitoring;
    private Activity context;

    private BroadcastReceiver sendTask, uploadTask;

    public static VisitContainFragment newInstance(Client client, Visit mVisit, String address, UUID mID, int locationID) {
        Bundle args = new Bundle();
        args.putParcelable(Client.KEY, client);
        args.putParcelable(Visit.KEY, mVisit);
        args.putString(ADDRESS_KEY, address);
        args.putSerializable(UUID_KEY, mID);
        args.putInt(LOCATION_ID, locationID);
        VisitContainFragment fragment = new VisitContainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        EventBus.getDefault().unregister(this);
        super.onPause();
    }


    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        EventBus.getDefault().register(this);
        super.onResume();
    }

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        setHasOptionsMenu(true);
        String address = getArguments().getString(ADDRESS_KEY);
        mClient = getArguments().getParcelable(Client.KEY);
        mID = (UUID) getArguments().getSerializable(UUID_KEY);
        locationID = getArguments().getInt(LOCATION_ID);
        mVisit = getArguments().getParcelable(Visit.KEY);
        if (rootView == null) {
            if (App.isMerch()) {
                rootView = inflater.inflate(R.layout.fragment_visit_contain_mmerch, container, false);
                btOrder = rootView.findViewById(R.id.visit_btOrder);
                bt_create_task = rootView.findViewById(R.id.visit_bt_create_task);
                btn_from_date = rootView.findViewById(R.id.btn_from_date);
                btn_to_date = rootView.findViewById(R.id.btn_to_date);
                btPhoto = rootView.findViewById(R.id.visit_btPhoto);
                imgBtnUpdateTask = rootView.findViewById(R.id.imgBtnUpdateTask);
                btResult = rootView.findViewById(R.id.visit_btVisitResult);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                fromDate = calendar.getTime();
                calendar = Calendar.getInstance();
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                toDate = calendar.getTime();
                btn_from_date.setText(TimeFormatter.sdf1C.format(fromDate));
                btn_to_date.setText(TimeFormatter.sdf1C.format(toDate));
                btn_from_date.setOnClickListener(this);
                btn_to_date.setOnClickListener(this);
                btOrder.setOnClickListener(this);
                bt_create_task.setOnClickListener(this);
                imgBtnUpdateTask.setOnClickListener(this);
                btPhoto.setOnClickListener(this);
                btResult.setOnClickListener(this);
            } else {
                rootView = inflater.inflate(R.layout.fragment_visit_contain, container, false);
                ImageView icon = new ImageView(context);
                icon.setImageResource(R.mipmap.add_new);
                btOrder = rootView.findViewById(R.id.visit_btOrder);
                btReturn = rootView.findViewById(R.id.visit_btReturn);
                btPayment = rootView.findViewById(R.id.visit_btMoney);
                btPhoto = rootView.findViewById(R.id.visit_btPhoto);
                btResult = rootView.findViewById(R.id.visit_btVisitResult);
                btContacts = rootView.findViewById(R.id.visit_btContact);
                progressBar = rootView.findViewById(R.id.progress_bar_visitContain);
                progressBarPhoto = rootView.findViewById(R.id.progress_bar_photo_task);
                viewBtnRepeatLocation = rootView.findViewById(R.id.viewBtnRepeatLocation);
                error_locationView = rootView.findViewById(R.id.error_locationView);
                setButtonsListener();
                rootView.findViewById(R.id.txt_visit_result_marquee).setSelected(true);
            }
            txtAddress = rootView.findViewById(R.id.txt_address);
            TextView txtClient = rootView.findViewById(R.id.txt_client);
            if (mClient != null) {
                txtClient.setText(String.format("%s\n%s", mClient.mName, mClient.mPostAddress));
            }
            setupReceivers();
            if (address == null) {
                error_locationView.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, address);
                txtAddress.setText("Визит засчитан");
            }
            startTaskFragment();
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (mClient != null) {
            String code = mClient.getCode();
            List<Contact> contacts = loadContactList(code);
            if (contacts.size() == 0) {
                startContactsFragment();
            } else if (isNotMainContactFace(contacts)) {
                startContactsFragment();
            } else {
                String dateLastUpdateContact = Client.getLastUpdateContact(context, code);
                if (!dateLastUpdateContact.isEmpty()) {
                    if (contactLastUpdate(dateLastUpdateContact)) {
                        startContactsFragment();
                    }
                }
            }
        }
    }

    private boolean isNotMainContactFace(List<Contact> contacts) {
        for (Contact contact : contacts) {
            if (contact.getResponsible() == 1) {
                return false;
            }
        }
        return true;
    }

    private List<Contact> loadContactList(String codeClient) {
        List<Contact> mFullClientList = new ArrayList<>();
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            db = app.getDB();
            mFullClientList = Contact.loadContacts(db, codeClient, App.get(getActivity()).getCodeAgent());
        } catch (Exception e) {
            GlobalProc.mToast(context, e.toString());
        }
        return mFullClientList;
    }


    private boolean contactLastUpdate(String date) {
        try {
            if (date.length() == "27.01.20".length()) {
                date = date.substring(0, 6).concat("20").concat(date.substring(6, 8));
            }
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            Date lastModify = format.parse(date);
            Date referenceDate = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(referenceDate);
            c.add(Calendar.MONTH, -6);
            Date timeSixMonthsAgo = c.getTime();
            if (lastModify.getTime() <= timeSixMonthsAgo.getTime()) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void showDatePickerDialog(View view, Date start) {
        DialogFragment newFragment = new DatePickerFragment(view, start);
        newFragment.show(getFragmentManager(), "datePicker");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateChanged(DateChanged dateChanged) {
        int year = dateChanged.getYear();
        int month = dateChanged.getMonth();
        int day = dateChanged.getDay();
        Calendar dateShip = Calendar.getInstance();
        dateShip.set(Calendar.YEAR, year);
        dateShip.set(Calendar.MONTH, month);
        dateShip.set(Calendar.DAY_OF_MONTH, day);
        dateShip.set(Calendar.HOUR, 0);
        dateShip.set(Calendar.MINUTE, 0);
        ((Button) dateChanged.getView()).setText(TimeFormatter.sdf1C.format(dateShip.getTime()));
        switch (dateChanged.getView().getId()) {
            case R.id.btn_from_date:
                fromDate = dateShip.getTime();
                break;
            case R.id.btn_to_date:
                toDate = dateShip.getTime();
                break;
        }
    }

    private void setupReceivers() {
        sendTask = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte status = intent.getByteExtra(
                        SendTasksIntentService.EXTRA_STATUS, (byte) 0);
                switch (status) {
                    case SendTasksIntentService.STATUS_ERROR:
                        GlobalProc.mToast(context, "Не удалось отправить задачу");
                        break;
                    case SendTasksIntentService.STATUS_OK:
                        GlobalProc.mToast(context, "Задача была успешно отправлена");
                        break;
                    default:
                        break;
                }
                FragmentManager fragmentManager;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    fragmentManager = getChildFragmentManager();
                } else {
                    fragmentManager = getFragmentManager();
                }
                TasksFragment task = (TasksFragment) fragmentManager.findFragmentById(R.id.visit_bottom);
                task.fillList(task.cb_show_complete_task.isChecked());
            }
        };
        uploadTask = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                byte status = intent.getByteExtra(
                        UploadFileIntentService.EXTRA_STATUS, (byte) 0);
                switch (status) {
                    case UploadFileIntentService.STATUS_ERROR:
                        GlobalProc.mToast(context, "Не удалось отправить");
                        break;
                    case UploadFileIntentService.STATUS_OK:
                        GlobalProc.mToast(context, "Успешно отправлено");
                        break;
                }
            }
        };
        IntentFilter filterSend = new IntentFilter(SendTasksIntentService.BROADCAST_ACTION);
        IntentFilter filterUpload = new IntentFilter(UploadFileIntentService.BROADCAST_ACTION);
        context.registerReceiver(uploadTask, filterUpload);
        context.registerReceiver(sendTask, filterSend);
    }

    @Override
    public void onDestroyView() {
        if (rootView.getParent() != null) {
            ((ViewGroup) rootView.getParent()).removeView(rootView);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            context.unregisterReceiver(sendTask);
            context.unregisterReceiver(uploadTask);
        } catch (Exception e) {

        }
    }

    private void waitCursor(boolean b) {
        if (b) {
            progressBar.setVisibility(View.VISIBLE);
            error_locationView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }


    private void startTaskFragment() {
        FragmentManager fragmentManager;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            fragmentManager = getChildFragmentManager();
        } else {
            fragmentManager = getFragmentManager();
        }
        if (mClient != null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.visit_bottom, TasksFragment.newInstance(mClient.getCode(), fromDate, toDate)).commitAllowingStateLoss();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener((v, keyCode, event) -> {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    completeVisitDialog();
                    return true;
                }
                return false;
            });
        }
    }

    private void setButtonsListener() {
        if (btOrder != null) {
            btOrder.setOnClickListener(this);
        }
        if (btReturn != null) {
            btReturn.setOnClickListener(this);
        }
        if (btPayment != null) {
            btPayment.setOnClickListener(this);
        }
        if (btPhoto != null) {
            btPhoto.setOnClickListener(this);
        }
        if (btResult != null) {
            btResult.setOnClickListener(this);
        }
        if (viewBtnRepeatLocation != null) {
            viewBtnRepeatLocation.setOnClickListener(this);
        }
        if (btContacts != null) {
            btContacts.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.visit_btOrder:
                checkExistOrderDoc();
                break;
            case R.id.visit_btMoney:
                PaymentClientListFragment fragment = PaymentClientListFragment.newInstance(mClient, mVisit.getID());
                getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.contentMain, fragment)
                        .commit();
                break;
            case R.id.visit_btPhoto:
                launchCamera();
                break;
            case R.id.visit_btReturn:
                if (mReturn == null || !mReturn.isExist()) {
                    Returns.newReturn(App.get(context).getCodeAgent(), mVisit.getID());
                    startNewReturnFragment(IDD_NEW_RETURN);
                } else {
                    startNewReturnFragment(IDD_EDIT_RETURN);
                }
                break;
            case R.id.visit_btVisitResult:
                showVisitResultDialog();
                break;
            case R.id.viewBtnRepeatLocation:
                FetchLocationTask task = new FetchLocationTask(context, this);
                task.execute();
                waitCursor(true);
                break;
            case R.id.visit_bt_create_task:
                checkExistTaskDoc();
                break;
            case R.id.btn_to_date:
                showDatePickerDialog(v, toDate);
                break;
            case R.id.btn_from_date:
                showDatePickerDialog(v, fromDate);
                break;
            case R.id.imgBtnUpdateTask:
                startTaskFragment();
                break;
            case R.id.visit_btContact:
                startContactsFragment();
                break;
        }
    }



    private void showVisitResultDialog() {
        final Dialog builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_visit_result);
        final EditText editText = builder.findViewById(R.id.et_dialog_visit_result);
        Button imageButton = builder.findViewById(R.id.btn_cancel_dialog_visit_result);
        Button btn_ok_visit_result = builder.findViewById(R.id.btn_ok_visit_result);
        btn_ok_visit_result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mVisit.result(editText.getText().toString());
                mVisit.update(context.getApplicationContext());
                builder.dismiss();
            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                builder.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.visit_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_complete_visit:
                // Завершение визита
                completeVisitDialog();
                break;
            case R.id.action_tickets_client:
                getActivity().getFragmentManager().beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.contentMain, TicketClientFragment.newInstance(mClient))
                        .commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void launchCamera() {
        Intent intent = new Intent(getActivity(), CamActivity.class);
        intent.putExtra(CLIENT_CODE, mClient.getCode());
        intent.putExtra(UUID_KEY, mID.toString());
//        File file = createImageFile();
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
//        } else {
//            Uri photoUri = FileProvider.getUriForFile(context,context.getApplicationContext().getPackageName() + ".provider", file);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(context.getApplicationContext().getPackageManager()) != null) {
            startActivityForResult(intent, IDD_NEW_PHOTO);
        }
    }

    private void checkExistTaskDoc() {
        if (TaskLab.get(context).getUnconfirmedTask() != null) {
            loadBackupDlg().show();
        } else {
            checkMonitoringOfTask();
        }
    }

    private void checkMonitoringOfTask() {
        try {
            MyApp myApp = (MyApp) context.getApplicationContext();
            SQLiteDatabase db = myApp.getDB();
            List<Monitoring> monitoringList = Monitoring.load(db, String.format(Locale.getDefault(),
                    "%s = %d", MonitoringMetaData.FIELD_UNCONFIRMED_MONITORING, 1));
            if (monitoringList.size() > 0) {
                loadTaskOfMonitoringDlg(monitoringList).show();
            } else {
                Task unconfirmedTask = TaskLab.get(context).getUnconfirmedTask();
                if (unconfirmedTask != null) {
                    TaskLab.get(context).deleteTask(unconfirmedTask);
                }
                startNewTaskFragment(IDD_NEW_TASK, null);
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private void checkExistOrderDoc() {
        if (App.isMerch()) {
            if (Monitoring.isUnsavedMonitoringExists(context, mID.toString())) {
                startNewMonitoringFragment(IDD_EDIT_MONITORING);
            } else {
                Monitoring.newMonitoring(context);
                startNewMonitoringFragment(IDD_NEW_MONITORING);
            }
        } else {
            if (Order.isUnsavedOrderExists(context)) {
                loadBackupDlg().show();
            } else {
                if (mOrder == null || !mOrder.isExist()) {
                    Order.newOrder(App.get(context).getCodeAgent());
                    startNewOrderFragment(IDD_NEW_ORDER);
                } else {
                    startNewOrderFragment(IDD_EDIT_ORDER);
                }
            }
        }
    }

    private Dialog loadTaskOfMonitoringDlg(List<Monitoring> monitoringList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(context.getString(R.string.mes_load_task_of_monitoring));
        ArrayAdapter<Monitoring> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(monitoringList);
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startNewTaskFragment(IDD_NEW_TASK, arrayAdapter.getItem(i));
            }
        });
        builder.setNegativeButton(getActivity().getString(R.string.bt_no),
                (dialog, which) -> {
                    Task unconfirmedTask = TaskLab.get(context).getUnconfirmedTask();
                    if (unconfirmedTask != null) {
                        TaskLab.get(context).deleteTask(unconfirmedTask);
                    }
                    startNewTaskFragment(IDD_NEW_TASK, null);
                });

        builder.setCancelable(true);
        return builder.create();
    }

    private Dialog loadBackupDlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getActivity().getString(R.string.cap_confirm));
        if (App.isMerch()) {
            builder.setMessage(getActivity().getString(
                    R.string.mes_load_task_from_backup));
        } else {
            builder.setMessage(getActivity().getString(
                    R.string.mes_load_order_from_backup));
        }

        builder.setPositiveButton(getActivity().getString(R.string.bt_yes),
                (dialog, which) -> {
                    if (App.isMerch()) {
                        startNewTaskFragment(IDD_EDIT_TASK, null);
                    } else {
                        Order.loadUnsavedOrder(context);
                        startNewOrderFragment(IDD_NEW_ORDER);
                    }
                });
        builder.setNegativeButton(getActivity().getString(R.string.bt_no),
                (dialog, which) -> {
                    if (App.isMerch()) {
                        checkMonitoringOfTask();
                    } else {
                        Order.deleteUnsavedOrder(context
                                .getApplicationContext());
                        Order.newOrder(App.get(context).getCodeAgent());
                        startNewOrderFragment(IDD_NEW_ORDER);
                    }
                });

        builder.setCancelable(true);
        return builder.create();
    }

    public void completeVisitDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.mipmap.main_icon);
        builder.setTitle(getString(R.string.complete_visit_title));
        builder.setMessage(getString(R.string.complete_visit));
        builder.setPositiveButton(getActivity().getString(R.string.bt_yes),
                (dialog, which) -> saveVisitCompleteDate());
        builder.setNegativeButton(getActivity().getString(R.string.bt_no),null);
        builder.setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void saveVisitCompleteDate() {
        try {
            Date noReallyThisIsTheTrueDateAndTime = null;
            try {
                noReallyThisIsTheTrueDateAndTime = new InitTime().execute().get();
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }

            long time = new Date().getTime();
            if (noReallyThisIsTheTrueDateAndTime != null && noReallyThisIsTheTrueDateAndTime.getTime() != 0) {
                time = noReallyThisIsTheTrueDateAndTime.getTime();
            }
            mVisit.setEndTime(time);
            MyApp app = (MyApp) context.getApplication();
            mVisit.locationID(locationID);
            mVisit.update(app.getDB());
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().remove(this).commit();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction()
                    .replace(R.id.contentMain, new StatisticsFragment())
                    .commit();
        } catch (Exception e) {
            Log.e(TAG, "Не удалось сохранить визит");
        }
    }

    /**
     * Запускаем фрагмент с общими параметрами возврата
     */
    private void startNewReturnFragment(int mode) {
        Returns data = Returns.getReturn();
        if (data != null) {
            data.setClient(mClient);
            data.visitFK(mID);
            ReturnCommonFragment fragment = new ReturnCommonFragment();
            fragment.setTargetFragment(this, mode);
            getFragmentManager().beginTransaction()
                    .replace(R.id.contentMain, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    private void startContactsFragment() {
        getFragmentManager().beginTransaction()
                .replace(R.id.contentMain, ContactListFragment.newInstance(mClient.getCode()))
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

    /**
     * Запускаем фрагмент с общими параметрами заявки
     */
    private void startNewOrderFragment(int mode) {
        Order data = Order.getOrder();
        if (data != null) {
            if (mode == IDD_NEW_ORDER) {
                data.setClient(mClient);
                data.mPrintBill = data.getClient().mTypeDoc;
                data.visitFK(mID);
            }
            OrderCommonFragment fragment = OrderCommonFragment.newInstance(mClient);
            fragment.setTargetFragment(this, mode);
            getFragmentManager().beginTransaction()
                    .replace(R.id.contentMain, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void startNewMonitoringFragment(int mode) {
        Monitoring data = Monitoring.getCurrentMonitoring();
        if (data != null) {
            if (mode == IDD_NEW_MONITORING) {
                data.setCodeClient(mClient.getCode());
                data.setVisit(mID);
                data.setCodeAgent(App.get(context).getCodeAgent());
                MyApp myApp = (MyApp) context.getApplicationContext();
                data.insert(myApp.getDB());
            }
            data.setClient(mClient);
            FragmentManager fragmentManager = getFragmentManager();
            Fragment fragment = new OrderGoodsFragment();
            fragment.setTargetFragment(this, mode);
            fragmentManager.beginTransaction()
                    .replace(R.id.contentMain, fragment, OrderGoodsFragment.TAG)
                    .addToBackStack(null)
                    .commit();
        }
    }


    private void startNewTaskFragment(int mode, Monitoring item) {
        Task currentTask = null;
        if (mode == IDD_EDIT_TASK) {
            currentTask = TaskLab.get(context).getUnconfirmedTask();
        }
        if (currentTask == null) {
            currentTask = new Task();
            currentTask.agent(item == null ? App.get(context).getCodeAgent() : item.getCodeAgent());
            currentTask.clientCode(item == null ? mClient.getCode() : item.getCodeClient());
            if (item != null) {
                ArrayList<GoodsInTask> goodsInTaskList = new ArrayList<>();
                for (MonitoringProduct monitoringProduct : item.getItemsList()) {
                    goodsInTaskList.add(new GoodsInTask(monitoringProduct.getCodeProduct(), "", (byte) 0, (short) 1, (short) 0, 1));
                }
                currentTask.goodsList(goodsInTaskList);
            }
            TaskLab.get(context).addTask(currentTask);
        }
        MyApp myApp = (MyApp) context.getApplicationContext();
        currentTask.setClient(Client.load(myApp.getDB(), currentTask.clientCode()));
        Task.setCurrentTask(currentTask);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = new OrderGoodsFragment();
        fragment.setTargetFragment(this, IDD_NEW_TASK);
        fragmentManager.beginTransaction()
                .replace(R.id.contentMain, fragment, OrderGoodsFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (mVisit == null) {
            GlobalProc.mToast(context, "Ошибка при получении документа");
            return;
        }
        switch (requestCode) {
            case IDD_NEW_ORDER:
                if (resultCode == Activity.RESULT_OK) {
                    Order order = Order.getOrder();
                    if (order != null) {
                        mVisit.orderSKU(order.getItemsCount());
                        mVisit.orderSum(order.getTotalAmount());

                        mOrder = new State();
                        mOrder.isExist(true);
                        mOrder.Count(order.getItemsCount());
                        mOrder.Amount(order.getTotalAmount());
                    }
                    //mVisit.update(context.getApplicationContext());
                    //Order.setOrder(null);
                }
                break;
            case IDD_NEW_RETURN:
                if (resultCode == Activity.RESULT_OK) {
                    Returns retData = Returns.getReturn();
                    if (retData != null) {
                        mReturn = new State();
                        mReturn.isExist(false);
                        mReturn.Count(retData.getItemsCount());
                        mReturn.Amount(retData.getTotalAmount());
                    }
                    //Returns.setReturn(null);
                }
                break;
            case IDD_NEW_TASK:
                startTaskFragment();
                break;
            case IDD_NEW_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    if (mClient == null) return;

                    //Привязываемся к хост-активности
                    //Вместо mCurrentPhotoPath надо собрать список всех фотографий в папке
                    Log.d(TAG, App.get(context).getPhotosPath());
                    List<String> imagesArray = getImagesInFolder(App.get(context).getPhotosPath());
                    if (imagesArray != null && imagesArray.size() > 0) {
                        String[] files = new String[imagesArray.size()];
                        imagesArray.toArray(files);
                        // Запускаем процесс отправки
                        progressBarPhoto.setProgress(0);
                        progressBarPhoto.setVisibility(View.VISIBLE);
                        Intent startUpload = new Intent(context, UploadFileIntentService.class);
                        startUpload.putExtra(UploadFileIntentService.EXTRA_FILES_LIST, files);
                        startUpload.putExtra("receiver", new DownloadReceiver(new Handler()));
                        context.startService(startUpload);
                    }
                    //Ставим картинку на кнопку
                    setPic();
                }
                //Костыль для удаления файлов с 0 размером
//                File f = new File(mCurrentPhotoPath);
//                if (f.exists() && f.length() == 0)
//                    f.delete();
                break;
            default:
                break;
        }
    }


    private void enableButtons(boolean state) {
        if (btOrder != null) {
            if (mOrder != null) {
                btOrder.setEnabled(!mOrder.isExist());
                if (mOrder.isExist()) {

                }
            } else {
                btOrder.setEnabled(state);
            }
        }
        if (btReturn != null) {
            if (mReturn != null) {
                btReturn.setEnabled(!mReturn.isExist());
                if (mReturn.isExist()) {

                }
            } else {
                btReturn.setEnabled(state);
            }
        }
        btPhoto.setEnabled(state);
        //btResult.setEnabled(state);
    }

    public List<String> getImagesInFolder(String photosPath) {
        File parentDir = new File(photosPath);
        if (!parentDir.exists() || !parentDir.isDirectory()) {
            throw new IllegalArgumentException("Parent directory cannot exist.");
        }
        List<String> imagesPath = new ArrayList<>();
        for (String path : parentDir.list()) {
            if (path.substring(path.lastIndexOf('.') + 1).equals("jpg")) {
                imagesPath.add(parentDir.getAbsolutePath() + "/" + path);
            }
        }
        return imagesPath;
    }


    private void setPic() {
        int targetW = btPhoto.getHeight();
        int targetH = btPhoto.getHeight();
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        btPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void onLocation(FetchLocationTask task) {
        Location currentLocation;
        String address = null;
        try {
            currentLocation = task.get();
        } catch (Exception e) {
            currentLocation = null;
        }
        if (currentLocation == null) {
            GlobalProc.mToast(context, "Не удалось определить координату");
            currentLocation = new Location(LocationFragment.FAIL);
            currentLocation.setLatitude(0f);
            currentLocation.setLongitude(0f);
            currentLocation.setAccuracy(0f);
        } else {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(currentLocation.getLatitude(),
                        currentLocation.getLongitude(), 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    address = listAddresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Date noReallyThisIsTheTrueDateAndTime = null;
        try {
            noReallyThisIsTheTrueDateAndTime = new InitTime().execute().get();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        long time = new Date().getTime();
        if (noReallyThisIsTheTrueDateAndTime != null && noReallyThisIsTheTrueDateAndTime.getTime() != 0) {
            time = noReallyThisIsTheTrueDateAndTime.getTime();
        } else if (currentLocation.getTime() != 0) {
            time = currentLocation.getTime();
        }

        mVisit = new Visit(mID, currentLocation, time, mClient);
        // Сохраняем координату и привязываем к визиту
        try {
            MyApp app = (MyApp) context.getApplication();
            LocationMetadata.updateLocation(app.getDB(), locationID, System.currentTimeMillis(),
                    (float) currentLocation.getLatitude(),
                    (float) currentLocation.getLongitude(),
                    currentLocation.getAccuracy(),
                    currentLocation.getSpeed(),
                    currentLocation.getProvider());
            mVisit.locationID(locationID);
            mVisit.insert(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, "Не удалось сохранить визит");
        }
        waitCursor(false);
        if (currentLocation.getProvider().equals(LocationFragment.FAIL)) {
            error_locationView.setVisibility(View.VISIBLE);
        } else if (address == null || address.equals("")) {
            txtAddress.setText("Визит засчитан");
        }
    }








    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTaskUpdated(Task task) {
        if (App.isMerch()) {
            SetReserveTask mTask = new SetReserveTask(MyApp.getContext(), this, task);
            mTask.execute();
        } else {
            Intent startSend = new Intent(context.getApplicationContext(), SendTasksIntentService.class);
            startSend.putExtra(TaskMetaData.TABLE_NAME, task);
            context.startService(startSend);
        }
    }

    @Override
    public void onTaskComplete(SetReserveTask task) {
        boolean result;
        try {
            result = task.get();
        } catch (Exception e) {
            result = false;
        }
        String txt;
        if (result) {
            startTaskFragment();
            txt = getString(R.string.mes_reserv_task_success);
        } else {
            txt = getString(R.string.mes_reserv_error)
                    + Const.NewLine + task.getmUnswer();
        }
        GlobalProc.mToast(context, txt);
    }

    private class DownloadReceiver extends ResultReceiver {

        DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        public void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            switch (resultCode) {
                case 1:
                    progressBarPhoto.setProgress(resultData.getInt("progress"));
                    break;
                case 2:
                    progressBarPhoto.setVisibility(View.INVISIBLE);
                    break;
            }

        }
    }

    private class State implements Parcelable {
        protected boolean isExist;
        protected int count;
        protected float amount;

        public State() {
            isExist = false;
            count = 0;
            amount = 0f;
        }


        public final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        public boolean isExist() {
            return isExist;
        }

        public void isExist(boolean isExist) {
            this.isExist = isExist;
        }

        public void Count(int count) {
            this.count = count;
        }


        public void Amount(float amount) {
            this.amount = amount;
        }

        @SuppressWarnings("unused")
        public State(Parcel parcel) {
            isExist = parcel.readByte() == 1;
            count = parcel.readInt();
            amount = parcel.readFloat();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isExist ? 1 : 0));
            dest.writeInt(count);
            dest.writeFloat(amount);
        }
    }
}
