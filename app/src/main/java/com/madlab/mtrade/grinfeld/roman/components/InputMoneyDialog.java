package com.madlab.mtrade.grinfeld.roman.components;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;

import com.madlab.mtrade.grinfeld.roman.entity.Arrear;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;

public class InputMoneyDialog extends android.app.DialogFragment {
	public static String EXTRA_RESULT = "extra_credit";
	private Arrear mResult;

	public static InputMoneyDialog newInstance(Arrear current) {
		Bundle args = new Bundle();
		args.putParcelable(EXTRA_RESULT, current);

		InputMoneyDialog fragment = new InputMoneyDialog();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mResult = getArguments().getParcelable(EXTRA_RESULT);

		final EditText input = new EditText(getActivity());
		input.setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		input.setText(Float.toString(mResult.getDocPayment()));
		input.selectAll();
		input.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				mResult.setDocPayment(GlobalProc.parseFloat(s.toString()));
			}
		});

		return new AlertDialog.Builder(getActivity()).setTitle("Введите сумму")
				// .setMessage("Введите сумму")
				.setView(input)
				.setPositiveButton(
						getActivity().getString(android.R.string.yes),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
												int whichButton) {
								setResult(Activity.RESULT_OK);
							}
						}).create();
	}

	private void setResult(int resultCode) {
		if (getTargetFragment() == null)
			return;

		Intent i = new Intent();
		i.putExtra(EXTRA_RESULT, mResult);

		getTargetFragment().onActivityResult(getTargetRequestCode(),
				resultCode, i);
	}
}
