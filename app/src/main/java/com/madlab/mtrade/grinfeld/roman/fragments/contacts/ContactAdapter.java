package com.madlab.mtrade.grinfeld.roman.fragments.contacts;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.Contact;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {
    private List<Contact> mItems;
    private int mSelected = -1;
    private IOnClickItem iOnClickItem;


    public ContactAdapter(Context context, List<Contact> items, IOnClickItem iOnClickItem) {
        super(context, R.layout.client_editor_list_item, items);
        this.mItems = items;
        this.iOnClickItem = iOnClickItem;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.client_editor_list_item, null);
        }

        if (mItems == null)
            return v;

        Contact c = mItems.get(position);
        if (c != null) {
            TextView itemId = v.findViewById(R.id.tv_cliName);

            if (itemId != null) {
                String s = c.getFio();
                itemId.setText(s);
            }

            TextView itemLabel = v.findViewById(R.id.tv_cliPost);
            if (itemLabel != null) {
                itemLabel.setText(c.getPost());
            }
            v.setOnClickListener(v12 -> iOnClickItem.onClick(c));
            ImageButton imgBtnDel = v.findViewById(R.id.img_del);
            imgBtnDel.setOnClickListener(v1 -> iOnClickItem.onClickDel(c));

        }
        return v;
    }

    public void setSelected(int index) {
        mSelected = index;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }
}
