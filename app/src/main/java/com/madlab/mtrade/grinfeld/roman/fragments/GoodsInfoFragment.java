package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.Security;
import com.madlab.mtrade.grinfeld.roman.adapters.TransitAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.Manager;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.squareup.picasso.Picasso;


/**
 * Created by GrinfeldRA on 04.10.2017.
 */

public class GoodsInfoFragment extends Fragment  {

    private static final String UNKNOWN = "unknown";
    private static final String TAG = "#GoodsInfoFragment";
    private Goods goods;
    private final static String POSTFIX_WEIGHT = "кг.";
    private Bitmap image;
    private Activity context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null){
            goods = bundle.getParcelable(Goods.KEY);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        context = getActivity();
        int positionViewPager = getArguments().getInt(GoodsInfoContainer.POSITION);
        switch (positionViewPager){
            case 0://информация
                view = inflater.inflate(R.layout.goods_info_fragment,container,false);
                if (goods!=null){
                    printData(goods,view);
                    SQLiteDatabase db = ((MyApp)context.getApplication()).getDB();
                    Manager mManager = Manager.load(db, goods.manager());
                    printManagerInfo(mManager,view);
                }
                break;
            case 1://изображение
                view = inflater.inflate(R.layout.fragment_goods_info_image, container,false);
                ImageView goodsInfo_ivPicture = view.findViewById(R.id.goodsInfo_ivPicture);
                setImage(goods, goodsInfo_ivPicture);
                break;
            case 2://в пути
                view = inflater.inflate(R.layout.fragment_transit_list,container,false);
                ListView transitList =  view.findViewById(android.R.id.list);
                if (transitList != null && goods.transitList() != null) {
                    TransitAdapter adapter = new TransitAdapter(context,
                            R.layout.transit_item, goods.transitList());
                    transitList.setAdapter(adapter);
                }
                break;
        }
        return view;
    }

    private void printData(Goods goods, View view) {
        if (goods != null) {
            TextView tvName = view.findViewById(R.id.goodsInfo_tvName);
            TextView tvCode = view.findViewById(R.id.goodsInfo_tvCode);
            TextView tvNumInPack = view.findViewById(R.id.goodsInfo_tvNumInPack);
            TextView tvQuant = view.findViewById(R.id.goodsInfo_tvMinShip);
            TextView tvRest =  view.findViewById(R.id.goodsInfo_tvRest);
            TextView tvBestBefore =  view.findViewById(R.id.goodsInfo_tvBestBefore);
            TextView tvWeight =  view.findViewById(R.id.goodsInfo_tvWeight);
            TextView tvPriceContract =  view.findViewById(R.id.goodsInfo_tvPriceContract);
            TextView tvPriceStandard =  view.findViewById(R.id.goodsInfo_tvPriceStandart);
            TextView tvPriceShop =  view.findViewById(R.id.goodsInfo_tvPriceShop);
            TextView tvPriceRegion =  view.findViewById(R.id.goodsInfo_tvPriceRegion);
            TextView tvPriceMarket =  view.findViewById(R.id.goodsInfo_tvPriceMarket);
            TextView tvPriceOpt =  view.findViewById(R.id.goodsInfo_tvPriceOptSection);
            TextView tvNDS =  view.findViewById(R.id.goodsInfo_tvNds);
            TextView tvCountry =  view.findViewById(R.id.goodsInfo_tvCountry);
            TextView tvInfo =  view.findViewById(R.id.goodsInfo_tvInfo);
            TextView tvPriceOptSectionPercent = view.findViewById(R.id.goodsInfo_tvPriceOptSectionPercent);
            TextView tvPriceContractPercent = view.findViewById(R.id.goodsInfo_tvPriceContractPercent);
            TextView tvPriceMarketPercent = view.findViewById(R.id.goodsInfo_tvPriceMarketPercent);
            TextView tvPriceRegionPercent = view.findViewById(R.id.goodsInfo_tvPriceRegionPercent);
            TextView tvPriceShopPercent = view.findViewById(R.id.goodsInfo_tvPriceShopPercent);
            TextView tvPriceStandartPercent = view.findViewById(R.id.goodsInfo_tvPriceStandartPercent);
            String txt = goods.getNameFull();
            if (Order.getOrder() != null
                    && Order.getOrder().quality() == Order.QUALITY_NON_LIQUID) {
                txt = goods.getNameFull() + " БРАК";
            }
            tvName.setText(txt);
            tvCode.setText(goods.getCode());
            tvNumInPack.setText(String.format("%.2f %s", goods.getNumInPack(), goods.getBaseMeash()));
            tvQuant.setText(Float.toString(goods.getQuant())
                    + goods.getBaseMeash());
            tvRest.setText(Float.toString(goods.getRestOnStore())
                    + goods.getBaseMeash());
            tvBestBefore.setText(goods.getBestBefore());
            tvWeight.setText(GlobalProc.formatWeight(goods.getWeight())
                    + POSTFIX_WEIGHT);

            float priceStandart = goods.getPriceStandart();
            float priceContract = goods.getPriceContract();
            float priceShop = goods.getPriceShop();
            float priceRegion = goods.getPriceRegion();
            float priceMarket = goods.getPriceMarket();
            float priceOptSection = goods.getPriceOptSection();

            boolean isForeign = getArguments().getBoolean(GoodsInfoContainer.KEY_FOREIGN);
            tvPriceContract.setText(GlobalProc.toForeignPrice(priceContract, isForeign));
            tvPriceStandard.setText(GlobalProc.toForeignPrice(priceStandart, isForeign));
            tvPriceShop.setText(GlobalProc.toForeignPrice(priceShop, isForeign));
            tvPriceRegion.setText(GlobalProc.toForeignPrice(priceRegion, isForeign));
            tvPriceMarket.setText(GlobalProc.toForeignPrice(priceMarket, isForeign));
            tvPriceOpt.setText(GlobalProc.toForeignPrice(priceOptSection, isForeign));

            float onePercent = (priceStandart / 100);
            float discContract =  ((priceContract - priceStandart) / onePercent);
            float discShop =  ((priceShop - priceStandart) / onePercent);
            float discRegion =  ((priceRegion - priceStandart) / onePercent);
            float discMarket = ((priceMarket - priceStandart) / onePercent);
            float discOptSection =  ((priceOptSection - priceStandart) / onePercent);

            tvPriceContractPercent.setText(String.format("%.1f",discContract)+"%");
            tvPriceShopPercent.setText(String.format("%.1f",discShop)+"%");
            tvPriceRegionPercent.setText(String.format("%.1f",discRegion)+"%");
            tvPriceMarketPercent.setText(String.format("%.1f",discMarket)+"%");
            tvPriceOptSectionPercent.setText(String.format("%.1f",discOptSection)+"%");
            tvNDS.setText(Byte.toString(goods.getNDS()) + '%');
            tvCountry.setText(goods.getCountry());
            tvInfo.setText(goods.getInfo());
        }
    }

    private void setImage(Goods goods,final ImageView iView){
        String code = goods.getCode();
        Log.d(TAG,"setImage");
        StringBuilder sb = new StringBuilder(GlobalProc.getPref(context,
                R.string.pref_catalog_url, Const.DEFAULT_CATALOG_URL));
        if (code.length() > 0) {
            String base = new String(Base64.encode(code.getBytes(), Base64.NO_WRAP));
            sb.append(String.format("%s&id=%s&is_iframe=1&size=320x320", base, code));

            String s = Security.imageURL(goods.getCode());
            Log.d(TAG, s);
            //Glide.with(context).load("https://www.dalimo.ru/system_files/files_old/h_33153_320x320").into(iView);
            Picasso.get().load(s).into(iView);
//            LoadImageTask loadImageTask = new LoadImageTask();
//            loadImageTask.setListener((thumbnail, error) -> {
//                if (thumbnail != null) {
//                    image = thumbnail;
//                    iView.setImageBitmap(image);
//                }else {
//                    Toast.makeText(MyApp.getContext(), error,Toast.LENGTH_LONG).show();
//                }
//            });
//            loadImageTask.execute(sb.toString());
        }
    }

    private void printManagerInfo(Manager man, View view){
        String fio = UNKNOWN;
        String phone = "";
        String email = "";
        TextView tvManagerFIO = view.findViewById(R.id.goodsInfo_tvManager);
        TextView tvManagerPhone = view.findViewById(R.id.goodsInfo_tvManagerPhone);
        TextView tvManagerEmail = view.findViewById(R.id.goodsInfo_tvManagerEmail);

        if (man != null){
            fio = man.Fio();
            phone = man.Phone();
            email = man.Email();

            tvManagerPhone.setTag(phone);
            if (phone != ""){
                tvManagerPhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View pArg0) {
                        String phone = (String)pArg0.getTag();
                        String uri = "tel:" + phone;
                        startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
                    }
                });
            }
            tvManagerEmail.setTag(email);
            if (email != ""){
                tvManagerEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View pArg0) {
                        String email = (String)pArg0.getTag();
                        startActivity(new Intent(
                                android.content.Intent.ACTION_SENDTO,
                                Uri.fromParts("mailto", email, null)));
                    }
                });
            }

        }
        tvManagerFIO.setText(fio);
        tvManagerPhone.setText(phone);
        tvManagerEmail.setText(email);
    }

}
