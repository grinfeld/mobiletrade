package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by GrinfeldRA on 11.12.2017.
 */

public class ClientViewPagerContainer extends Fragment {

    private static final String TAG = "#ClientViewPagerContainer";
    public static final String POSITION = "position";

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View _rootView;
    ClientListFragment fragment;


    @Override
    public void onDestroyView() {
        if (_rootView.getParent() != null) {
            ((ViewGroup) _rootView.getParent()).removeView(_rootView);
            ClientListFragment.searchFlag = false;
        }
        super.onDestroyView();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getFragmentManager().popBackStack();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.fragment_client_view_pager, container, false);
            viewPager = _rootView.findViewById(R.id.viewpager_visit);
            tabLayout = _rootView.findViewById(R.id.tabs_visit);
            setupViewPager(viewPager);
            viewPager.setOffscreenPageLimit(0);
            viewPager.setOnPageChangeListener(ClientListFragment.listener);
            tabLayout.setupWithViewPager(viewPager);
        }
        return _rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (fragment != null)
            fragment.onCreateOptionsMenu(menu, inflater);
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            adapter = new ViewPagerAdapter(getChildFragmentManager());
        } else {
            adapter = new ViewPagerAdapter(getFragmentManager());
        }
        adapter.addFragmentTitle("Сегодня");
        adapter.addFragmentTitle("Все");
        viewPager.setAdapter(adapter);
    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(POSITION, position);
            fragment = new ClientListFragment();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                fragment.setTargetFragment(getParentFragment(), getTargetRequestCode());
            } else {
                fragment.setTargetFragment(fragment, getTargetRequestCode());
            }
            fragment.setArguments(bundle);
            return fragment;
        }


        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        private void addFragmentTitle(String title) {
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
