package com.madlab.mtrade.grinfeld.roman.iface;


import com.madlab.mtrade.grinfeld.roman.tasks.GetTicketClientTask;

/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public interface IGetTicketClientTask {

    void onGetTicketClientTaskComplete(GetTicketClientTask task);

}
