package com.madlab.mtrade.grinfeld.roman.entity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.db.OrderStatusMetaData;

public class Status {

    public enum Status_type {
        Unknown, Delivery, Order, Return
    }

    private String clientName;

    private String secLineLeft;
    private String secLineRight;

    private String thirdLineLeft;
    private String thirdLineRight;

    boolean isOutOfStock;

    private Object tag;

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public Object getTag() {
        return tag;
    }

    public String getClientName() {
        return clientName;
    }

    public boolean isOutOfStock() {
        return isOutOfStock;
    }

    public String getLeft2Line() {
        return secLineLeft;
    }

    public String getRight2Line() {
        return secLineRight;
    }

    public String getLeft3Line() {
        return thirdLineLeft;
    }

    public String getRight3Line() {
        return thirdLineRight;
    }

    public Status() {
        this("", "", "", "", "", false);
    }

    public Status(String client, String left2Line, String right2Line,
                  String left3Line, String right3Line, boolean isOutOfStock) {
        clientName = client;
        this.secLineLeft = left2Line;
        this.secLineRight = right2Line;
        this.thirdLineLeft = left3Line;
        this.thirdLineRight = right3Line;
        this.tag = null;
        this.isOutOfStock = isOutOfStock;
    }

    public static ArrayList<Status> load(final SQLiteDatabase db,
                                         String selection, String[] selectionArgs) {
        ArrayList<Status> result = null;
        Cursor rows = null;
        try {
            // String sql = String.format("SELECT * FROM %s",
            // OrderStatusMetaData.TABLE_NAME);
            rows = db.query(OrderStatusMetaData.TABLE_NAME, null, selection,
                    selectionArgs, null, null, null);
            result = new ArrayList<>();

            while (rows.moveToNext()) {
                Status os = new Status();

                os.clientName = rows
                        .getString(OrderStatusMetaData.FIELD_CLIENT_NUM);
                os.secLineLeft = rows
                        .getString(OrderStatusMetaData.FIELD_EXPEDITOR_NUM);
                os.secLineRight = rows
                        .getString(OrderStatusMetaData.FIELD_PHONE_NUM);
                os.thirdLineLeft = rows
                        .getString(OrderStatusMetaData.FIELD_STATUS_NUM);
                os.thirdLineRight = rows
                        .getString(OrderStatusMetaData.FIELD_ORDER_TYPE_NUM);

                result.add(os);
            }
        } catch (Exception e) {
            result = null;
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    public static ArrayList<String> loadDistinct(final SQLiteDatabase db,
                                                 String field) {
        ArrayList<String> result = null;
        Cursor rows = null;
        try {
            String sql = String.format("SELECT DISTINCT %s FROM %s", field,
                    OrderStatusMetaData.TABLE_NAME);
            rows = db.rawQuery(sql, null);
            result = new ArrayList<String>();
            while (rows.moveToNext()) {
                String newItem = rows.getString(0);

                result.add(newItem);
            }
        } catch (Exception e) {
            result = null;
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    /**
     * Загружает список заказов из потока
     *
     * @param inputStream
     *            - входящий поток
     * @return - массив с информацией о текущих заказах
     */
    public static ArrayList<Status> loadFromStream(
            InputStreamReader inputStream, Status_type statusType) {
        ArrayList<Status> result = null;
        try {
            BufferedReader br = new BufferedReader(inputStream);
            String message = br.readLine();

            switch (statusType) {
                case Order:
                    result = loadOrdersFromString(message);
                    break;
                case Return:
                    result = loadReturnsFromString(message);
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            return null;
        }

        return result;
    }

    public static ArrayList<Status> loadOrdersFromString(String message) {
        ArrayList<Status> result = new ArrayList<Status>();

        String[] orders = message.split(";");
        StringTokenizer token;
        for (String order : orders) {
            token = new StringTokenizer(order, "|");
            if (token.countTokens() > 4) {
                String number = token.nextToken();
                String cli = token.nextToken();
                String orderType = token.nextToken();
                String sumStr = token.nextToken().replaceAll("\\s+", "")
                        .replace(',', '.');
                float sum = GlobalProc.parseFloat(sumStr);
                String way = "";
                if (token.hasMoreTokens()) {
                    way = token.nextToken();
                }
                String id = "";
                if (token.hasMoreTokens()) {
                    id = token.nextToken();
                }
                boolean isOutOfStock = false;
                if (token.hasMoreTokens()){
                    isOutOfStock = token.nextToken().equals("1");
                }

                Status status = new Status(cli, number, GlobalProc.formatMoney(sum),
                        orderType, way, isOutOfStock);
                status.setTag(id);

                result.add(status);
            }
        }

        if (result.size() == 0) {
            result = null;
        }

        return result;
    }

    public static ArrayList<Status> loadReturnsFromString(String message) {
        ArrayList<Status> result = new ArrayList<Status>();

        String[] returns = message.split(";");
        StringTokenizer token;
        for (String order : returns) {
            token = new StringTokenizer(order, "|");
            if (1 < token.countTokens() && token.countTokens() < 5) {
                String number = token.nextToken();
                String cli = token.nextToken();
                String sumStr = token.nextToken().replaceAll("\\s+", "")
                        .replace(',', '.');
                float sum = 0f;
                try {
                    sum = Float.parseFloat(sumStr);
                } catch (Exception e) {
                    sum = 0;
                }

                Status status = new Status(cli, number, GlobalProc.formatMoney(sum),
                        "", "", false);

                result.add(status);
            }
        }

        if (result.size() == 0) {
            result = null;
        }

        return result;
    }
}
