package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.MonitoringMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Monitoring;
import com.madlab.mtrade.grinfeld.roman.entity.MonitoringProduct;
import com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReserveMonitoringComplete;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import okhttp3.Response;

/**
 * Created by GrinfeldRa
 */
public class SetReserveMonitoring extends AsyncTask<Short, Void, Boolean> {

    private final static String TAG = "!->SetReserveMonitoring";

    private final static String PROC_NAME = "СоздатьДокументМониторингаФормат";

    private final static String SUCCESS = "OK";
    private final static String FAILURE = "ERR";

    private String mUnswer;
    private Credentials connectInfo;
    private Context baseContext;
    private ISetReserveMonitoringComplete monitoringComplete;

    public SetReserveMonitoring(Context baseContext, ISetReserveMonitoringComplete monitoringComplete) {
        this.baseContext = baseContext;
        this.monitoringComplete = monitoringComplete;
        connectInfo = Credentials.load(baseContext);
    }

    @Override
    protected Boolean doInBackground(Short... numbers) {
        SQLiteDatabase db = null;
        DalimoClient myClient = null;
        boolean success = true;
        try {
            App settings = App.get(baseContext);
            MyApp app = (MyApp) baseContext.getApplicationContext();
            db = app.getDB();
            for (Short current : numbers) {
                List<Monitoring> monitoringList = Monitoring.load(db,
                        String.format(Locale.getDefault(), "%s = %d", MonitoringMetaData.FIELD_ID, current));
                Monitoring monitoring = monitoringList.get(0);
                StringBuilder sb = new StringBuilder(String.format("%s;%s;%s;",
                        Const.COMMAND, settings.getCodeAgent(), PROC_NAME));
                String data = generateDataMonitoring(monitoring);
                String answer;
                int typeServer = 0;
                if (settings.connectionServer.equals("1")){
                    myClient = new DalimoClient(connectInfo);
                    if (!myClient.connect()){
                        success = false;
                        break;
                    }
                    String message = sb.append(data).append(Const.END_MESSAGE).toString();
                    Log.d(TAG, "send: "+ message);
                    myClient.send(message);
                    answer = myClient.receive();
                    typeServer = JournalFragment.APP_SERVER;
                    myClient.disconnect();
                } else {
                    String region = App.getRegion(MyApp.getContext());
                    try {
                        Response response = OkHttpClientBuilder.buildCall(PROC_NAME, data, region).execute();
                        if (response.isSuccessful()){
                            String strResponse = response.body().string();
                            JSONObject jsonObject = new JSONObject(strResponse);
                            answer = jsonObject.getString("data");
                            typeServer = JournalFragment.IIS_SERVER;
                        }else {
                            answer = FAILURE;
                        }
                    }catch (Exception e){
                        myClient = new DalimoClient(connectInfo);
                        if (!myClient.connect()){
                            success = false;
                            break;
                        }
                        String message = sb.append(data).append(Const.END_MESSAGE).toString();
                        Log.d(TAG, "send: "+ message);
                        myClient.send(message);
                        answer = myClient.receive();
                        typeServer = JournalFragment.APP_SERVER;
                        myClient.disconnect();
                    }
                }
                // А теперь обрабатываем полученное сообщение
                if (answer.contains(FAILURE)) {
                    success = false;
                    mUnswer += answer;
                } else {// В случае успеха - обновляем параметры
                    monitoring.setReserved(true);
                    monitoring.setTypeServer(typeServer);
                    StringTokenizer st = new StringTokenizer(answer, ";");
                    byte paramCount = 0;
                    success = false;
                    while (st.hasMoreElements()) {
                        String object = st.nextToken();
                        switch (paramCount) {
                            case 0:
                                success = object.contains(SUCCESS);
                                break;
                            default:
                                break;
                        }

                        paramCount++;
                    }
                    if (success)
                        monitoring.update(baseContext);
                }
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            success = false;
        } finally {
            if (myClient != null)
                myClient.disconnect();
        }
        return success;
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (monitoringComplete != null) {
            monitoringComplete.onTaskComplete(this);
        }
    }

    private String generateDataMonitoring(Monitoring monitoring) {
        String header = String.format(Locale.getDefault(), "%d;%s;%s;%s;%s;%s;",
                monitoring.getId(), TimeFormatter.sdf1C.format(monitoring.getDate()),
                monitoring.getCodeClient(), monitoring.getCodeManager(),
                monitoring.getCodeAgent(), TimeFormatter.sdf1C.format(monitoring.getDate()));
        StringBuffer sb = new StringBuffer(header);
        for (MonitoringProduct product : monitoring.getItemsList()){
            if (product.getCheck() == 1){
                sb.append(product.getCodeProduct()).append("|");
            }
        }
        sb.append(String.format(";%s;", GlobalProc.getPref(baseContext, R.string.pref_exclusive)));
        return sb.toString();
    }

    public String getMessage() {
        return mUnswer;
    }
}
