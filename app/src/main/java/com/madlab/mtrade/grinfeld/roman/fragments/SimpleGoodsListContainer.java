package com.madlab.mtrade.grinfeld.roman.fragments;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.db.ShipmentMustListMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.ShipmentMustList;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnCountChanged;
import com.madlab.mtrade.grinfeld.roman.eventbus.OnMeashChanged;
import com.madlab.mtrade.grinfeld.roman.iface.IGetShipmentMustList;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;
import com.madlab.mtrade.grinfeld.roman.services.LoadNonLiquidGoodsListIntentService;
import com.madlab.mtrade.grinfeld.roman.tasks.GetShipmentMustListTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsGroupTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsOnTreeClickTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGroupMatrixTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


public class SimpleGoodsListContainer extends android.app.Fragment implements ILoadGoodsComplete, IGetShipmentMustList {
	private final static String TAG = "!->SimpleGoodsListActivity";

	public final static String EXTRA_LOAD_GOODS = "STMorNonLiquid";

	public final static String EXTRA_VIEW_MODE = "GoodsAdapterViewMode";

	public final static String EXTRA_CLIENT = "Client";


	public final static byte LOAD_GOODS_STM = Byte.MIN_VALUE;

	public final static byte LOAD_GOODS_NON_LIQUID = Byte.MAX_VALUE;
	public static final byte LOAD_GOODS_MUST_LIST = 3;

	public ArrayList<GoodsInDocument> mExistedItems;
	private byte mViewMode;
	private byte mPriceLevel = 2;
	private byte mDiscount = 0;
	private Client client;
	byte load = LOAD_GOODS_STM;

	View _rootView;

	private BroadcastReceiver getListTask = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			byte status = intent.getByteExtra(
					LoadNonLiquidGoodsListIntentService.EXTRA_STATUS, (byte) 0);
			String message = intent.getStringExtra(LoadNonLiquidGoodsListIntentService.EXTRA_MESSAGE);
			switch (status) {
				case LoadNonLiquidGoodsListIntentService.STATUS_ERROR:
					if (context!=null)
					GlobalProc.mToast(context, message);
					break;
				case LoadNonLiquidGoodsListIntentService.STATUS_OK:
					ArrayList<Goods> goodsList = intent.getParcelableArrayListExtra(LoadNonLiquidGoodsListIntentService.EXTRA_GOODS);
					if (goodsList != null) {
						Fragment f = getFragmentManager().findFragmentById(R.id.fragmentContainer);
						if (f != null) {
							((SimpleGoodsListFragment) f).updateItems(goodsList, GoodsAdapter.MODE_NON_LIQUID, client.isForeignAgent());
						}
					}
					break;
				default:
					break;
			}
		}
	};
	private Activity context;


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		if (_rootView == null){
			// Inflate the layout for this fragment
			_rootView = inflater.inflate(R.layout.activity_fragment, container, false);
			//Если у нас есть фрагмент со списком товаров - рисуем, загружаем
//			if (_rootView.findViewById(R.id.detailFragmentContainer) != null) {
//				FragmentTransaction ft = getFragmentManager().beginTransaction();
//				Fragment newList = GoodsListFragment.newInstance(mListViewMode,
//						mSelectedNode, mType, mClient, mExistedItems,getArguments().getString(MainActivity.PRICE));
//				ft.replace(R.id.detailFragmentContainer, newList);
//				ft.commit();
//			}
		}
		_rootView.setFocusableInTouchMode(true);
		_rootView.requestFocus();
		_rootView.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
					Intent i = new Intent();
					i.putExtra(GoodsInDocument.KEY, mExistedItems);
					getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
					getFragmentManager().popBackStack();
					return true;
				}
				return false;
			}
		});
		setHasOptionsMenu(true);
		return _rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		super.onCreateOptionsMenu(menu, inflater);
	}

	protected Fragment createFragment() {
		// Параметры получаеми здесь, потому что вызывается раньше, чем onCreate
		Bundle args = getArguments();
		if (args != null) {
//			mType = args.getByte(GoodsTreeFragment.EXTRA_TYPE);
//			mSelectedNode = args.getParcelable(GoodsTreeFragment.EXTRA_NODE);
//			mListViewMode = args.getByte(GoodsTreeFragment.EXTRA_MODE);
			client = args.getParcelable(GoodsTreeFragment.EXTRA_CLIENT);
			mExistedItems = args.getParcelableArrayList(GoodsInDocument.KEY);
		}
		return SimpleGoodsListFragment.newInstance(mExistedItems, client);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		IntentFilter filter = new IntentFilter(LoadNonLiquidGoodsListIntentService.BROADCAST_ACTION);
		getActivity().registerReceiver(getListTask, filter);
		FragmentManager fm = getFragmentManager();
		Fragment fragment  = createFragment();//fm.findFragmentById(R.id.fragmentContainer);
		//if (fragment == null) {
		fm.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
		load = getArguments().getByte(EXTRA_LOAD_GOODS);
		mViewMode = GoodsAdapter.MODE_REFERENCE;
		mViewMode = getArguments().getByte(EXTRA_VIEW_MODE);
		client = getArguments().getParcelable(EXTRA_CLIENT);
		if (client != null){
			mPriceLevel = client.mPriceType;
			mDiscount = client.mDiscount;
		}
		mExistedItems = getArguments().getParcelableArrayList(GoodsInDocument.KEY);
		switch (load) {
			case LOAD_GOODS_STM:
				String[] prices = getResources().getStringArray(R.array.list_prices);
				String subtitle = "Цена: "+prices[mPriceLevel];
				if (mDiscount != 0) subtitle += String.format(". Скидка %d%%", mDiscount);
				android.support.v7.app.ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
				actionBar.setSubtitle(subtitle);
				startLoadSTMTask();
				break;
			case LOAD_GOODS_NON_LIQUID:
				getActivity().startService(new Intent(getActivity(), LoadNonLiquidGoodsListIntentService.class));
				break;
			case LOAD_GOODS_MUST_LIST:
				startLoadMustListTask(client);
				break;
			default:
				break;
			}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unregisterReceiver(getListTask);
	}

	@Override
	public void onTaskComplete(LoadGoodsGroupTask task) {

	}

	@Override
	public void onTaskComplete(LoadGoodsOnTreeClickTask task) {
		ArrayList<Goods> goodsList = task.getList();
		Fragment f = getFragmentManager().findFragmentById(R.id.fragmentContainer);
		if (f != null){
			((SimpleGoodsListFragment)f).updateItems(goodsList, mViewMode, client.isForeignAgent());
		}
		if (goodsList!=null&&goodsList.size()>0){
			List<String> isShipmentMustListCodeGoods = ShipmentMustList.loadCodeGoodsIsShipmentMustList(getActivity(), App.get(getActivity().getApplication()).getCodeAgent(), client.getCode());
			for (Goods goods : goodsList){
				if (isShipmentMustListCodeGoods != null) {
					for (String codeGoods  : isShipmentMustListCodeGoods){
                        if (goods.getCode().equals(codeGoods)){
                            goods.setSnipmentMustList(true);
                        }
                    }
				}
			}
			if ( f != null && ((SimpleGoodsListFragment) f).mAdapter != null) {
				((SimpleGoodsListFragment) f).mAdapter.notifyAfterExistedListChanged();
			}
			GetShipmentMustListTask getShipmentMustListTask =
					new GetShipmentMustListTask(getActivity(),this, client.getCode(),App.get(getActivity().getApplication()).getCodeAgent(),goodsList);
			getShipmentMustListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		}
	}

	@Override
	public void getShipmentMustListComplete(GetShipmentMustListTask task) {
		try {
			String result = task.get();
			LinkedHashMap<String,String> mapResult = new LinkedHashMap<>();
			if (result!=null && !result.isEmpty())
			for (String s : result.split(";")){
				String[] split = s.split("[|]");
				if (split.length==2)
				mapResult.put(split[0],split[1]);
			}

			for (Map.Entry<String,String> entry : mapResult.entrySet()){
				String query = ShipmentMustListMetaData.insertQuery(App.get(context.getApplication()).getCodeAgent(), client.getCode(), String.valueOf(entry.getValue()), entry.getKey());
				MyApp app = (MyApp) context.getApplicationContext();
				app.getDB().rawQuery(query,null);
			}

			Fragment f = context.getFragmentManager().findFragmentById(R.id.fragmentContainer);
			if (f != null){
				((SimpleGoodsListFragment)f).updateShipmentMustList(mapResult);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}catch (ClassCastException e){
			e.printStackTrace();
		}
	}


	private void startLoadSTMTask(){
		LoadGoodsOnTreeClickTask task = new LoadGoodsOnTreeClickTask(getActivity(), mExistedItems, this,
				GoodsList.MODE_STM, (byte)0, mPriceLevel, mDiscount, null);
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, App.get(getActivity().getApplication()).getRootCode());
	}

	private void startLoadMustListTask(Client client){
		LoadGoodsOnTreeClickTask task = new LoadGoodsOnTreeClickTask(getActivity(), mExistedItems, this,
				GoodsList.MODE_MUST_LIST, (byte)0, mPriceLevel, mDiscount, null);
		task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, App.get(getActivity().getApplication()).getRootCode());
		task.setMLCategory(client.Category());
		task.setClientCode(client.getCode());
	}


	@Override
	public void onPause() {
		super.onPause();
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onResume() {
		EventBus.getDefault().register(this);
		super.onResume();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onCountChanged(OnCountChanged data) {
		Goods goods = data.getGoods();
		int newCount = data.getNewCount();
		if (newCount > 0){
			goods.inDocument(true);
			
			GoodsInDocument gid = null;
			for (int i = 0; i < mExistedItems.size(); i++) {
				GoodsInDocument item = mExistedItems.get(i);
				
				if (goods.getCode().contains(item.getCode())){
					gid = item;
					if (gid.getMeashPosition() > 0){
						gid.setMeashPosition((byte)newCount);
					}else{
						gid.setCountPosition(newCount);
					}
					break;
				}
			}
			
			if (gid == null){
				gid = new GoodsInDocument(goods.getCode(), newCount, (byte)0, goods.getQuant(), goods.price(), goods.getWeight());
				if (load == LOAD_GOODS_NON_LIQUID){
					gid.isSpecPrice(true);
				}
				mExistedItems.add(gid);
			}
		} else {
			goods.inDocument(false);
			
			for (int i = 0; i < mExistedItems.size(); i++) {
				GoodsInDocument item = mExistedItems.get(i);
				
				if (goods.getCode().contains(item.getCode())){
					mExistedItems.remove(i);
					break;
				}
			}
		}
		
		updateGoodsAdapterAfterItemsAdd();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onMeashChanged(OnMeashChanged data) {
		Goods goods = data.getGoods();
		byte newValue = data.getNewValue();
		GoodsInDocument gid = null;
		if (mExistedItems == null)
		{
			mExistedItems = new ArrayList<>();
		}
		else {
			for (GoodsInDocument item : mExistedItems) {
				if (goods.getCode().contains(item.getCode())){
					gid = item;
					gid.setMeashPosition(newValue);
					break;
				}
			}
		}
		
		if (gid == null){
			goods.inDocument(true);
			
			gid = new GoodsInDocument(goods.getCode(), 1, newValue, goods.getQuant(), goods.price(), goods.getWeight());
			mExistedItems.add(gid);
		}
		
		updateGoodsAdapterAfterItemsAdd();
	}
	

	
	private void updateGoodsAdapterAfterItemsAdd(){
		try {
			Fragment f = getFragmentManager().findFragmentById(R.id.fragmentContainer);
			if (f != null){
				SimpleGoodsListFragment sgf = (SimpleGoodsListFragment)f;
				if (sgf != null){
					sgf.setExistedItems(mExistedItems);
					sgf.updateAdapterAfterAddItemsInOrder();
				}	
			}
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	@Override
	public void onTaskComplete(LoadGroupMatrixTask task) {
	}


//	@Override
//	public boolean onSearchRequested() {
//		return super.onSearchRequested();
//	}
//
//	@Override
//	public void onNewIntent(Intent intent) {
//		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
//			String query = intent.getStringExtra(SearchManager.QUERY);
//			PreferenceManager.getDefaultSharedPreferences(this).edit()
//					.putString(SimpleGoodsListFragment.PREF_SEARCH_QUERY, query)
//					.apply();
//			try {
//				Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
//				if (f != null)
//					((SimpleGoodsListFragment)f).startSearchGoods();
//			} catch (Exception e) {
//				Log.e(TAG, e.toString());
//			}
//		} else {
//			super.onNewIntent(intent);
//		}
//	}
}
