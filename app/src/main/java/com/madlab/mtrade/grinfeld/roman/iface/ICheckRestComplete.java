package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.CheckRestTask;

/**
 * Created by GrinfeldRA on 26.12.2017.
 */

public interface ICheckRestComplete {

    void onTaskComplete(CheckRestTask task);

}
