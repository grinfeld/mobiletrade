package com.madlab.mtrade.grinfeld.roman.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.ClientsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ContactMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsItemBean;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Client implements Parcelable {

    public static final String KEY = "client_key";
    private final static String TAG = "!->Client";

    // private static ArrayList<Client> mList = new ArrayList<Client>();
    // private static short mCount = 0;


    public boolean isRegMercury() {
        return isRegMercury;
    }

    public boolean isForeignAgent() {
        return isForeignAgent;
    }

    public void setForeignAgent(boolean foreignAgent) {
        isForeignAgent = foreignAgent;
    }

    private boolean isRegMercury;
    private boolean isForeignAgent;

    /**
     * Код клиента
     */
    private String mCode;// mCode

    public void setCode(String code) {
        mCode = code;
    }

    public String getCode() {
        return mCode;
    }

    /**
     * Краткое имя клиента (для списка)
     */
    public String mName;

    /**
     * Адрес
     */
    public String mPostAddress;

    /**
     * Полное наименование клиента
     */
    public String mNameFull;

    /**
     * Директор
     */
    public String mDirector;

    /**
     * Контактное лицо
     */
    public String mContact;

    /**
     * Номера телефонов
     */
    public String mPhones;

    /**
     * Максимальный долг
     */
    public String mMaxArrear;

    /**
     * Текущий долг
     */
    public String mCurArrear;

    /**
     * Средняя заявка
     */
    public float mAverageOrder;

    /**
     * Широта
     */
    public float mLatitude;

    /**
     * Долгота
     */
    public float mLongitude;

    /**
     * Тип ТТ
     */
    private byte mOutletType;

    public byte getOutletType() {
        return mOutletType;
    }

    /**
     * Тип цен
     */
    public byte mPriceType;

    /**
     * Скидка
     */
    public byte mDiscount;

    /**
     * Признак сегодняшнего посещения
     */
    public boolean mIsTodayVisit;

    /**
     * Признак стоп-отгрузки
     */
    public boolean mIsStop;

    /**
     * Признак П/Н (1 - П, 2 - Н)
     */
    public byte mTypeDoc;

    /**
     * Количество SKU
     */
    public short mSKU;

    /**
     * Признак посещения. Берется из таблицы с визитами
     */
    private boolean mVisited;

    public boolean visited() {
        return mVisited;
    }

    /**
     * План развоза продукции по маршрутам
     */
    public ArrayList<ShippingPlan> mShippingPlan;

    /**
     * План развоза продукции по маршрутам
     */
    private Map<String, String> mCurrentFirms;

    /**
     * Перечисление фирм, с которых производится отгузка
     */
    public void currentFirm(Map<String, String> value) {
        mCurrentFirms = value;
    }

    public Map<String, String> currentFirm() {
        return mCurrentFirms;
    }

    /**
     * Категория маст-листа
     */
    private String mCategory;

    public String Category() {
        return mCategory;
    }

    public int typeSalesPlan = 0;

    /**
     * дата последнего обновления контактной информации
     */
    public String dateLastUpdateContact;


    public void Category(String mustListCategory) {
        mCategory = mustListCategory;
    }

    public Client(String code, String name, String postAddr, String nameFull,
                  String director, String manager, String phones, String maxCredit,
                  String curCredit, byte outletType, byte priceType, byte discount,
                  byte typeDoc, boolean todayVisit, boolean isStop, float lat,
                  float lon, short sku, String mlCategory, boolean isRegMercury,
                  boolean isForeignAgent, String dateLastUpdateContact) {

        mCode = code;
        mName = name;
        mPostAddress = postAddr;
        mNameFull = nameFull;
        mDirector = director;
        mContact = manager;
        mPhones = phones;
        mMaxArrear = maxCredit;
        mCurArrear = curCredit;

        mLatitude = lat;
        mLongitude = lon;

        mOutletType = outletType;
        mPriceType = priceType;
        mDiscount = discount;
        mTypeDoc = typeDoc;

        mIsTodayVisit = todayVisit;
        mIsStop = isStop;
        mVisited = false;

        mSKU = sku;
        mCategory = mlCategory;
        this.isRegMercury = isRegMercury;
        this.isForeignAgent = isForeignAgent;
        this.dateLastUpdateContact = dateLastUpdateContact;
    }

    public Client() {
        this("", "", "", "", "", "", "", "", "", (byte) 0, (byte) 0, (byte) 0,
                (byte) 0, false, false, 0f, 0f, (short) 0, "", true, false, "");
    }

    @SuppressWarnings("unchecked")
    public Client(Parcel data) {
        mCode = data.readString();
        mName = data.readString();
        mPostAddress = data.readString();
        mNameFull = data.readString();
        mDirector = data.readString();
        mContact = data.readString();
        mPhones = data.readString();
        mMaxArrear = data.readString();
        mCurArrear = data.readString();
        mCategory = data.readString();

        mLatitude = data.readFloat();
        mLongitude = data.readFloat();

        mOutletType = data.readByte();
        mPriceType = data.readByte();
        mDiscount = data.readByte();
        mTypeDoc = data.readByte();
        mSKU = (short) data.readInt();

        typeSalesPlan = data.readInt();

//        byte size = data.readByte();
//        for(int i = 0; i < size; i++){
//            String KEY_TYPE = data.readString();
//            String value = data.readString();
//            if (KEY_TYPE != null && value != null){
//                mCurrentFirms.put(KEY_TYPE,value);
//            }
//        }
        mCurrentFirms = (LinkedHashMap<String, String>) data.readSerializable();
        mShippingPlan = data.readArrayList(getClass().getClassLoader());

        boolean[] bb = new boolean[3];
        data.readBooleanArray(bb);

        mIsTodayVisit = bb[0];
        mIsStop = bb[1];
        mVisited = bb[2];
        isRegMercury = data.readByte() == 1;
        isForeignAgent = data.readByte() == 1;
        dateLastUpdateContact = data.readString();
    }

    public static final Parcelable.Creator<Client> CREATOR = new Parcelable.Creator<Client>() {

        public Client createFromParcel(Parcel in) {
            return new Client(in);
        }

        public Client[] newArray(int size) {
            return new Client[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mCode);
        parcel.writeString(mName);
        parcel.writeString(mPostAddress);
        parcel.writeString(mNameFull);
        parcel.writeString(mDirector);
        parcel.writeString(mContact);
        parcel.writeString(mPhones);
        parcel.writeString(mMaxArrear);
        parcel.writeString(mCurArrear);
        parcel.writeString(mCategory);

        parcel.writeFloat(mLatitude);
        parcel.writeFloat(mLongitude);

        parcel.writeByte(mOutletType);
        parcel.writeByte(mPriceType);
        parcel.writeByte(mDiscount);
        parcel.writeByte(mTypeDoc);
        parcel.writeInt(mSKU);
        parcel.writeInt(typeSalesPlan);

//        if (mCurrentFirms != null){
//            parcel.writeByte((byte)mCurrentFirms.size());
//            for(Map.Entry<String,String> entry : mCurrentFirms.entrySet()){
//                parcel.writeString(entry.getKey());
//                parcel.writeString(entry.getValue());
//            }
//        }else{
//            parcel.writeByte((byte)0);
//        }
        parcel.writeSerializable((Serializable) mCurrentFirms);
        parcel.writeList(mShippingPlan);

        parcel.writeBooleanArray(new boolean[]{mIsTodayVisit, mIsStop, mVisited});
        parcel.writeByte((byte) (isRegMercury ? 1 : 0));
        parcel.writeByte((byte) (isForeignAgent ? 1 : 0));
        parcel.writeString(dateLastUpdateContact);
    }

    // ----------------------------------------------------------------------------------------------------
    // Список клиентов

    /*
     * Акссесор, дает доступ к списку клиентов
     */
    // public static ArrayList<Client> getList()
    // {
    // return mList;
    // }

    // public static ArrayList<String> getStringList()
    // {
    // ArrayList<String> list = new ArrayList<String>();
    //
    // for (Client cl : mList){
    // list.add(cl.mName);
    // }
    //
    // return list;
    // }

    /*
     * Добавляет клиента в конец списка
     */
    // private static void add(Client client)
    // {
    // if (client == null) return;
    //
    // if (mList.add(client)){
    // mCount++;
    // }
    // }

    /*
     * Возвращает количество записей в списке
     */
    // public static short getCount()
    // {
    // return mCount;
    // }

    /*
     * Получает элемент по индексу
     */
    // public static Client getItem(int index)
    // {
    // return mList.get(index);
    // }

    /**
     * Загружает информацию о клиенте из БД. Может вернуть null, если клиент не
     * найден.
     *
     * @param db      , база данных
     * @param codeCli , код клиента
     */
    public static Client load(SQLiteDatabase db, String codeCli) {
        if (codeCli != null && codeCli.length() < 0)
            return null;

        Client result = null;
        Cursor rows = null;
        try {
            rows = db
                    .rawQuery(ClientsMetaData.SELECT, new String[]{codeCli});

            if (rows.moveToFirst()) {
                result = new Client();

                result.setCode(rows.getString(ClientsMetaData.FIELD_CODE_INDEX));
                result.mName = rows.getString(ClientsMetaData.FIELD_NAME_INDEX);

                String tmp = rows
                        .getString(ClientsMetaData.FIELD_ADDRESS_INDEX);
                result.mPostAddress = tmp.length() == 0 ? "<Адрес не указан>"
                        : tmp;

                tmp = rows.getString(ClientsMetaData.FIELD_FULL_NAME_INDEX);
                result.mNameFull = tmp.length() == 0 ? "<Название не указано>"
                        : tmp;

                result.mDirector = rows
                        .getString(ClientsMetaData.FIELD_DIRECTOR_INDEX);
                result.mContact = rows
                        .getString(ClientsMetaData.FIELD_CONTACT_INDEX);
                result.mPhones = rows
                        .getString(ClientsMetaData.FIELD_PHONES_INDEX);
                result.mMaxArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_MAX_ARREAR_INDEX),
                        Locale.ENGLISH);
                result.mCurArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_CUR_ARREAR_INDEX),
                        Locale.ENGLISH);
                result.mAverageOrder = rows
                        .getFloat(ClientsMetaData.FIELD_AVG_ORDER_INDEX);
                result.mLatitude = rows
                        .getFloat(ClientsMetaData.FIELD_LATITUDE_INDEX);
                result.mLongitude = rows
                        .getFloat(ClientsMetaData.FIELD_LONGITUDE_INDEX);

                result.mPriceType = (byte) rows
                        .getShort(ClientsMetaData.FIELD_PRICE_TYPE_INDEX);
                result.mDiscount = (byte) rows
                        .getShort(ClientsMetaData.FIELD_DISCOUNT_INDEX);
                result.mTypeDoc = (byte) rows
                        .getShort(ClientsMetaData.FIELD_TYPE_PAY_INDEX);
                result.mSKU = rows.getShort(ClientsMetaData.FIELD_SKU_INDEX);

                result.mIsTodayVisit = (rows
                        .getShort(ClientsMetaData.FIELD_IS_TODAY_INDEX) == 1) ? true
                        : false;
                result.mIsStop = (rows
                        .getString(ClientsMetaData.FIELD_IS_STOP_INDEX)
                        .equalsIgnoreCase("X")) ? true : false;
                Map<String, String> firmsAndPayType = new LinkedHashMap<>();
                String[] arrayFirmsAndPayType = rows.getString(ClientsMetaData.FIELD_FIRMS_INDEX).split("[|]");
                for (String data : arrayFirmsAndPayType) {
                    String[] strings = data.split("#");
                    if (strings.length == 2) {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], strings[1]);
                        }
                    } else {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], "");
                        }
                    }
                }
                result.currentFirm(firmsAndPayType);
                result.mShippingPlan = ShippingPlan.Parse(rows
                        .getString(ClientsMetaData.FIELD_SHIPPINGPLAN_INDEX));
                //Категория маст-листа
                result.Category(rows.getString(ClientsMetaData.FIELD_CATEGORY_INDEX));
                result.typeSalesPlan = (rows.getInt(ClientsMetaData.FIELD_SALES_PLAN_INDEX));
                result.isRegMercury = rows.getShort(ClientsMetaData.FIELD_REG_MERCURY_INDEX) == 1;
                result.isForeignAgent = rows.getShort(ClientsMetaData.FIELD_FOREIGN_AGENT_INDEX) == 1;
                result.dateLastUpdateContact = rows.getString(ClientsMetaData.FIELD_LAST_UPDATE_CONTACT_INDEX);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }

        return result;
    }

    public static ArrayList<Client> loadList(SQLiteDatabase db, boolean today) {
        String where = null;
        if (today) {
            where = String.format(" WHERE %s = 1", ClientsMetaData.FIELD_IS_TODAY);
        }
        return loadList(db, where);
    }

    public static ArrayList<Client> loadList(SQLiteDatabase db, String where) {
        ArrayList<Client> result = null;

        String sql = String.format(Locale.ENGLISH, "SELECT * FROM %s LEFT JOIN %s ON %s = %s ",
                ClientsMetaData.TABLE_NAME,
                VisitMetaData.TABLE_NAME,
                ClientsMetaData.FIELD_CODE,
                VisitMetaData.FIELD_CLIENT_CODE);

        if (where != null) {
            sql += where + " ";
        }

        sql = String.format("%s GROUP BY %s", sql, ClientsMetaData.FIELD_CODE);
        sql = String.format("%s ORDER BY %s", sql, ClientsMetaData.FIELD_NAME);

        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, null);

            //Нужен для отметки посещенных клиентов
            int VisitRowIndex = rows.getColumnIndex(VisitMetaData._ID);

            result = new ArrayList<>();

            while (rows.moveToNext()) {
                Client cl = new Client();

                cl.setCode(rows.getString(ClientsMetaData.FIELD_CODE_INDEX));
                cl.mName = rows.getString(ClientsMetaData.FIELD_NAME_INDEX);

                String tmp = rows
                        .getString(ClientsMetaData.FIELD_ADDRESS_INDEX);
                cl.mPostAddress = tmp.length() == 0 ? "<Адрес не указан>" : tmp;

                tmp = rows.getString(ClientsMetaData.FIELD_FULL_NAME_INDEX);
                cl.mNameFull = tmp.length() == 0 ? "<Название не указано>"
                        : tmp;

                cl.mDirector = rows
                        .getString(ClientsMetaData.FIELD_DIRECTOR_INDEX);
                cl.mContact = rows
                        .getString(ClientsMetaData.FIELD_CONTACT_INDEX);
                cl.mPhones = rows.getString(ClientsMetaData.FIELD_PHONES_INDEX);
                cl.mMaxArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_MAX_ARREAR_INDEX),
                        Locale.ENGLISH);
                cl.mCurArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_CUR_ARREAR_INDEX),
                        Locale.ENGLISH);
                cl.mAverageOrder = rows
                        .getFloat(ClientsMetaData.FIELD_AVG_ORDER_INDEX);
                cl.mLatitude = rows
                        .getFloat(ClientsMetaData.FIELD_LATITUDE_INDEX);
                cl.mLongitude = rows
                        .getFloat(ClientsMetaData.FIELD_LONGITUDE_INDEX);

                cl.mPriceType = (byte) rows
                        .getShort(ClientsMetaData.FIELD_PRICE_TYPE_INDEX);
                cl.mDiscount = (byte) rows
                        .getShort(ClientsMetaData.FIELD_DISCOUNT_INDEX);
                cl.mTypeDoc = (byte) rows
                        .getShort(ClientsMetaData.FIELD_TYPE_PAY_INDEX);
                cl.mSKU = rows.getShort(ClientsMetaData.FIELD_SKU_INDEX);

                cl.mIsTodayVisit = (rows
                        .getShort(ClientsMetaData.FIELD_IS_TODAY_INDEX) == 1) ? true
                        : false;
                cl.mIsStop = (rows
                        .getString(ClientsMetaData.FIELD_IS_STOP_INDEX)
                        .equalsIgnoreCase("X")) ? true : false;
                cl.isRegMercury = rows.getShort(ClientsMetaData.FIELD_REG_MERCURY_INDEX) == 1;
                cl.isForeignAgent = rows.getShort(ClientsMetaData.FIELD_FOREIGN_AGENT_INDEX) == 1;
                cl.dateLastUpdateContact = rows.getString(ClientsMetaData.FIELD_LAST_UPDATE_CONTACT_INDEX);
                Map<String, String> firmsAndPayType = new LinkedHashMap<>();
                String[] arrayFirmsAndPayType = rows.getString(ClientsMetaData.FIELD_FIRMS_INDEX).split("[|]");
                for (String data : arrayFirmsAndPayType) {
                    String[] strings = data.split("#");
                    if (strings.length == 2) {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], strings[1]);
                        }
                    } else {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], "");
                        }
                    }
                }
                cl.currentFirm(firmsAndPayType);
                cl.mShippingPlan = ShippingPlan.Parse(rows
                        .getString(ClientsMetaData.FIELD_SHIPPINGPLAN_INDEX));

                cl.mVisited = rows.getInt(VisitRowIndex) > 0;

                //Категория маст-листа
                cl.Category(rows.getString(ClientsMetaData.FIELD_CATEGORY_INDEX));
                cl.typeSalesPlan = (rows.getInt(ClientsMetaData.FIELD_SALES_PLAN_INDEX));
                result.add(cl);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }

        return result;
    }


    public static void updateLastModifyContact(SQLiteDatabase db, String dateUpdate, String codeClient) {
        ContentValues values = new ContentValues();
        values.put(ClientsMetaData.FIELD_LAST_UPDATE_CONTACT, dateUpdate);
        db.update(ClientsMetaData.TABLE_NAME, values, ClientsMetaData.FIELD_CODE + " = ?", new String[]{codeClient});
    }

    public static String getLastUpdateContact(Activity context, String codeClient) {
        MyApp app = (MyApp) context.getApplication();
        SQLiteDatabase db = app.getDB();
        Client load = load(db, codeClient);
        if (load != null) {
            return load.dateLastUpdateContact;
        }
        return "";
    }

    /**
     * Загружает список клиентов из БД
     * SELECT *
     * FROM Clients
     * LEFT JOIN Visit ON CodeCli = VisitClientCode
     *
     * @param db    , база данных
     * @param where , признак необходимости посещения сегодня
     */
    public static ArrayList<Client> loadList(SQLiteDatabase db, String where, String[] selectionArgs) {
        ArrayList<Client> result = null;

        Cursor rows = null;
        try {
            rows = db.query(ClientsMetaData.TABLE_NAME,
                    null, where, selectionArgs, null, null, null);

            result = new ArrayList<Client>();

            while (rows.moveToNext()) {
                Client cl = new Client();

                cl.setCode(rows.getString(ClientsMetaData.FIELD_CODE_INDEX));
                cl.mName = rows.getString(ClientsMetaData.FIELD_NAME_INDEX);

                String tmp = rows
                        .getString(ClientsMetaData.FIELD_ADDRESS_INDEX);
                cl.mPostAddress = tmp.length() == 0 ? "<Адрес не указан>" : tmp;

                tmp = rows.getString(ClientsMetaData.FIELD_FULL_NAME_INDEX);
                cl.mNameFull = tmp.length() == 0 ? "<Название не указано>"
                        : tmp;

                cl.mDirector = rows
                        .getString(ClientsMetaData.FIELD_DIRECTOR_INDEX);
                cl.mContact = rows
                        .getString(ClientsMetaData.FIELD_CONTACT_INDEX);
                cl.mPhones = rows.getString(ClientsMetaData.FIELD_PHONES_INDEX);
                cl.mMaxArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_MAX_ARREAR_INDEX),
                        Locale.ENGLISH);
                cl.mCurArrear = String.format(Const.FORMAT_MONEY,
                        rows.getFloat(ClientsMetaData.FIELD_CUR_ARREAR_INDEX),
                        Locale.ENGLISH);
                cl.mAverageOrder = rows
                        .getFloat(ClientsMetaData.FIELD_AVG_ORDER_INDEX);
                cl.mLatitude = rows
                        .getFloat(ClientsMetaData.FIELD_LATITUDE_INDEX);
                cl.mLongitude = rows
                        .getFloat(ClientsMetaData.FIELD_LONGITUDE_INDEX);

                cl.mPriceType = (byte) rows
                        .getShort(ClientsMetaData.FIELD_PRICE_TYPE_INDEX);
                cl.mDiscount = (byte) rows
                        .getShort(ClientsMetaData.FIELD_DISCOUNT_INDEX);
                cl.mTypeDoc = (byte) rows
                        .getShort(ClientsMetaData.FIELD_TYPE_PAY_INDEX);
                cl.mSKU = rows.getShort(ClientsMetaData.FIELD_SKU_INDEX);

                cl.mIsTodayVisit = (rows
                        .getShort(ClientsMetaData.FIELD_IS_TODAY_INDEX) == 1) ? true
                        : false;
                cl.mIsStop = (rows
                        .getString(ClientsMetaData.FIELD_IS_STOP_INDEX)
                        .equalsIgnoreCase("X")) ? true : false;

                Map<String, String> firmsAndPayType = new LinkedHashMap<>();
                String[] arrayFirmsAndPayType = rows.getString(ClientsMetaData.FIELD_FIRMS_INDEX).split("[|]");
                for (String data : arrayFirmsAndPayType) {
                    String[] strings = data.split("#");
                    if (strings.length == 2) {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], strings[1]);
                        }
                    } else {
                        if (!strings[0].equals("")) {
                            firmsAndPayType.put(strings[0], "");
                        }
                    }
                }
                cl.currentFirm(firmsAndPayType);
                cl.mShippingPlan = ShippingPlan.Parse(rows
                        .getString(ClientsMetaData.FIELD_SHIPPINGPLAN_INDEX));

                //Категория маст-листа
                cl.Category(rows.getString(ClientsMetaData.FIELD_CATEGORY_INDEX));
                cl.typeSalesPlan = (rows.getInt(ClientsMetaData.FIELD_SALES_PLAN_INDEX));
                cl.isRegMercury = rows.getShort(ClientsMetaData.FIELD_REG_MERCURY_INDEX) == 1;
                cl.isForeignAgent = rows.getShort(ClientsMetaData.FIELD_FOREIGN_AGENT_INDEX) == 1;
                cl.dateLastUpdateContact = rows.getString(ClientsMetaData.FIELD_LAST_UPDATE_CONTACT_INDEX);
                result.add(cl);
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }

        return result;
    }

    /**
     * Ищет клиента в списке
     *
     * @param client искомый клиент
     * @return индекс клиента в списке, если не найден, то -1
     */
    public static int find(ArrayList<Client> source, Client client) {
        if (client == null || source == null)
            return -1;

        int result = -1;
        try {
            for (int i = 0; i < source.size(); i++) {
                Client current = source.get(i);
                if (current != null
                        && current.getCode().equalsIgnoreCase(client.getCode())) {
                    result = i;
                    break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            result = -1;
        }
        return result;
    }

    // public static void setChecked(Client client)
    // {
    // if (client == null) return;
    //
    // for (Client current : mList) {
    // if (current.getCode().equalsIgnoreCase(client.getCode())){
    // current.mSelected = true;
    // break;
    // }
    // }
    // }
}
