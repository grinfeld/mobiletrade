package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class ReturnMetaData implements BaseColumns {

    public static final String TABLE_NAME = "Returns";

    public static final String FIELD_NUM = "NumReturn";
    public static final String FIELD_STATE = "ReturnState";
    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CLIENT = "CodeCli";
    public static final String FIELD_DATE_SHIP = "DateReturn";
    public static final String FIELD_DOC_NUM = "DocNum";
    public static final String FIELD_DOC_DATE = "DocDate";
    public static final String FIELD_TYPE_MONEY = "TypePay1";
    public static final String FIELD_TYPE_DOC = "TypePay2";
    public static final String FIELD_TOTAL_SUM = "TotalSum";
    public static final String FIELD_TOTAL_WEIGHT = "TotalWeight";
    public static final String FIELD_NOTE = "Note";
    public static final String FIELD_GET_RETURN_TIME = "GetOrderTime";
    public static final String FIELD_SENDED = "WasSended";
    public static final String FIELD_INVOICE = "InvoiceNum";

    public static final String FIELD_DATE_GET_GOODS = "DateGetGoods";
    public static final String FIELD_PICKUP = "Pickup";
    public static final String FIELD_VISIT_FK = "VisitFK";
    public static final String FIELD_FIRM_FK = "FirmFK";
    public static final String FIELD_TYPE_ACT = "TypeAct";
    public static final String FIELD_TYPE_SERVER = "ReturnTypeServer";
    public static final String FIELD_REPAYMENT = "ReturnRepayment";
    public static final String FIELD_PHOTO_IS_EXIST = "photoIsExist";
    public static final String FIELD_UUID_DOC = "uuid_doc";
    public static final String FIELD_IS_RESERVED = "is_reserved";


    public static final byte FIELD_NUM_INDEX = 0;

    public static final String INDEX = String.format(
            "CREATE INDEX idxReturnClient ON %s (%s, %s)", TABLE_NAME,
            FIELD_CODE_AGENT, FIELD_CLIENT);

    public static final String CREATE_COMMAND = String
            .format("CREATE TABLE %s ("
                            + "%s	int             NOT NULL CONSTRAINT PK_RETURNS PRIMARY KEY,"
                            + "%s	nchar(1)		NOT NULL DEFAULT 'V',"
                            + "%s	nvarchar(6)		NOT NULL DEFAULT '',"
                            + "%s	nvarchar(6)		NOT NULL DEFAULT '',"
                            + "%s	datetime    	NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(20)   	NOT NULL DEFAULT '',"
                            + "%s	datetime    	NOT NULL DEFAULT 0,"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(50)   	NOT NULL DEFAULT '',"
                            + "%s	nvarchar(5)		NOT NULL DEFAULT '00:00',"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(12)   	NOT NULL DEFAULT '',"
                            + "%s	datetime    	NOT NULL DEFAULT 0,"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(36)	NOT NULL DEFAULT '',"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	smallint    	NOT NULL DEFAULT 0,"
                            + "%s	INTEGER			NOT NULL DEFAULT 0,"
                            + "%s	smallint    	NOT NULL DEFAULT 0, "
                            + "%s	smallint    	NOT NULL DEFAULT 0, "
                            + "%s	nvarchar(36)	NOT NULL DEFAULT '',"
                            + "%s	INTEGER			NOT NULL DEFAULT 0)", TABLE_NAME,
                    FIELD_NUM, FIELD_STATE, FIELD_CODE_AGENT, FIELD_CLIENT,
                    FIELD_DATE_SHIP, FIELD_DOC_NUM, FIELD_DOC_DATE,
                    FIELD_TYPE_MONEY, FIELD_TYPE_DOC, FIELD_TOTAL_WEIGHT,
                    FIELD_TOTAL_SUM, FIELD_NOTE, FIELD_GET_RETURN_TIME,
                    FIELD_SENDED, FIELD_INVOICE, FIELD_DATE_GET_GOODS,
                    FIELD_PICKUP, FIELD_VISIT_FK, FIELD_FIRM_FK, FIELD_TYPE_ACT, FIELD_TYPE_SERVER,
                    FIELD_REPAYMENT, FIELD_PHOTO_IS_EXIST, FIELD_UUID_DOC, FIELD_IS_RESERVED);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

}
