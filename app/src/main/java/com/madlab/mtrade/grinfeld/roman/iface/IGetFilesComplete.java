package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.GetFilesFTPTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetFilesFromServerTask;

public interface IGetFilesComplete {

    void onTaskComplete(GetFilesFromServerTask serverTask);
    void onTaskComplete(GetFilesFTPTask ftpTask);

}