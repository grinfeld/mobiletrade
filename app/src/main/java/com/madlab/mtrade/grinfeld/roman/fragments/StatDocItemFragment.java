package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListFragment;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.StatusAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.OrderStatusMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Status;
import com.madlab.mtrade.grinfeld.roman.iface.IGetDebtsOnWaybills;
import com.madlab.mtrade.grinfeld.roman.iface.IGetStatus;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadTablePartCompleteListener;
import com.madlab.mtrade.grinfeld.roman.tasks.GetDebtsOnWaybills;
import com.madlab.mtrade.grinfeld.roman.tasks.GetOrderListTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetOrderStatusTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetReturnsListTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadTablePartTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.madlab.mtrade.grinfeld.roman.tasks.LoadTablePartTask.IS_EXIST;

/**
 * Created by GrinfeldRA on 10.12.2017.
 */

public class StatDocItemFragment extends ListFragment implements IGetStatus, View.OnClickListener, IGetDebtsOnWaybills {

    private static final String TAG = "#StatDocItemFragment";
    private StatusAdapter adapter;
    private int positionViewPager;
    private ArrayList<Status> mItemsList;
    private ArrayList<String> orderStatusItems = null;
    private ArrayList<String> orderTypeItems = null;
    private ListView listView;
    private ProgressBar progressBar;
    private Date requestDate = Calendar.getInstance().getTime();
    private Button btShipDate;
    private TextView txt_not_data;
    private Activity mActivity;
    private GetOrderStatusTask orderStatusTask;
    private LoadTablePartTask tablePartTask;
    private GetOrderListTask orderListTask;

    private final String[] from = {LoadTablePartTask.GOODS_NAME,
            LoadTablePartTask.COUNT, LoadTablePartTask.AMOUNT};
    private final int[] to = {R.id.tp_name, R.id.tp_count, R.id.tp_amount};

    private short ordersFromSiteCount = 0;

    /**
     * Признак того, что после изменения даты было обновление данных
     */
    private boolean wasUpdate = false;
    private Handler mHandler;


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (positionViewPager == 1) {
            Status current = adapter.getItem(position);
            showDialogTicketInfo(current);
        }
    }


    public void showDialogTicketInfo(Status status) {
        String docId = (String) status.getTag();
        String info = String.format("%s - %s", status.getLeft2Line(), status.getRight2Line());
        final Dialog builder = new Dialog(mActivity);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_stat_doc_ticket);
        final TextView iView = builder.findViewById(R.id.tp_header);
        iView.setText(info);
        final ImageView imageButton = builder.findViewById(R.id.img_btn_dialog_cancel);
        final ProgressBar progressBarDialog = builder.findViewById(R.id.pg_dialog);
        progressBarDialog.setVisibility(View.VISIBLE);
        final ListView listView = builder.findViewById(android.R.id.list);
        final TextView empty = builder.findViewById(R.id.empty);
        imageButton.setOnClickListener(view -> builder.dismiss());
        tablePartTask = new LoadTablePartTask(mActivity, Credentials.load(mActivity), task -> {
            boolean res;
            try {
                res = task.get();
            } catch (Exception e) {
                res = false;
            }
            if (res) {
                List<Map<String, String>> list = task.getList();
                if (list != null) {
                    if (list.size() > 0) {
                        SimpleAdapter adapter = new SimpleAdapter(mActivity, list,
                                R.layout.dialog_item_stat_doc_ticket, from, to) {
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView tv_name = view.findViewById(R.id.tp_name);
                                String isExist = list.get(position).get(IS_EXIST);
                                if (isExist != null && !isExist.equals("0")) {
                                    tv_name.setTextColor(Color.RED);
                                } else {
                                    tv_name.setTextColor(Color.WHITE);
                                }
                                return view;
                            }
                        };
                        listView.setAdapter(adapter);
                    } else {
                        empty.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                GlobalProc.mToast(mActivity, task.getLastError());
            }
            progressBarDialog.setVisibility(View.GONE);
        });
        tablePartTask.execute(docId);
        builder.show();
    }

    private DatePickerDialog.OnDateSetListener dcListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            wasUpdate = false;
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            requestDate = cal.getTime();
            setDate(requestDate);
            //обновление данных при выборе даты
            switch (positionViewPager) {
                //доставка
                case 0:
                    prepareDeliveryTask(requestDate);
                    break;
                //заявки
                case 1:
                    prepareOrdersTask(requestDate);
                    break;
                //возвраты
                case 2:
                    prepareReturnsTask(requestDate);
                    break;
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_stat_doc, container, false);
        mActivity = getActivity();
        listView = rootView.findViewById(android.R.id.list);
        mHandler = new Handler(Looper.getMainLooper());
        progressBar = rootView.findViewById(R.id.progress_bar_stat);
        btShipDate = rootView.findViewById(R.id.btShipDate);
        txt_not_data = rootView.findViewById(R.id.txt_not_data);
        btShipDate.setOnClickListener(this);
        setDate(requestDate);
        positionViewPager = getArguments().getInt(WorkModeFragment.POSITION);
        initPage(positionViewPager);
        return rootView;
    }


    private Date loadDate() {
        long ld = GlobalProc.getPrefLong(mActivity,
                R.string.pref_lastOrdersStatusUpdate);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(ld);

        Calendar max = Calendar.getInstance();
        max.add(Calendar.YEAR, 1);

        Calendar min = Calendar.getInstance();
        min.add(Calendar.YEAR, -1);

        if (cal.before(min) || cal.after(max)) {
            cal = Calendar.getInstance();
        }

        return cal.getTime();
    }


    private void setDate(Date current) {
        btShipDate.setText("Дата: " + TimeFormatter.sdf1C.format(current));
    }

    private void initPage(int positionViewPager) {
        //requestDate = loadDate();
        switch (positionViewPager) {
            //доставка
            case 0:
                btShipDate.setVisibility(View.VISIBLE);
                prepareDeliveryTask(requestDate);
                break;
            //заявки
            case 1:
                btShipDate.setVisibility(View.VISIBLE);
                prepareOrdersTask(requestDate);
                break;
            //возвраты
            case 2:
                btShipDate.setVisibility(View.VISIBLE);
                prepareReturnsTask(requestDate);
                break;
            //долги по накладным
            case 3:
                btShipDate.setVisibility(View.GONE);
                startDebtTask();
                break;
            default:
                break;
        }
    }

    private void prepareDeliveryTask(Date requestDate) {
        String codeAgent = App.get(mActivity).getCodeAgent();
        String curDate = TimeFormatter.sdf1C.format(requestDate);
        waitCursor(true);
        String region = App.getRegion(mActivity);
        if (App.get(mActivity).connectionServer.equals("1")) {
            startDeliveryTask(requestDate, null);
        } else {
            OkHttpClientBuilder.buildCall("ПолучитьМаршруты", codeAgent + ";" + curDate, region).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    mHandler.post(() -> {
                        startDeliveryTask(requestDate, null);
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    try {
                        String strResponse = response.body().string();
                        Log.d(TAG, "ПолучитьМаршруты " + strResponse);
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String data = jsonObject.getString("data");
                        mHandler.post(() -> {
                            startDeliveryTask(requestDate, new String[]{data});
                        });
                    } catch (JSONException e) {
                        mHandler.post(() -> {
                            startDeliveryTask(requestDate, null);
                        });
                    }
                }
            });
        }
    }

    private void startDebtTask() {
        App settings = App.get(mActivity);
        if (settings.getCodeAgent() != null) {
            waitCursor(true);
            String region = App.getRegion(mActivity);
            if (settings.connectionServer.equals("1")) {
                new GetDebtsOnWaybills(mActivity, this).execute();
            } else {
                OkHttpClientBuilder.buildCall("ДолгиПоНакладным", settings.getCodeAgent(), region).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        mHandler.post(() -> new GetDebtsOnWaybills(mActivity, StatDocItemFragment.this).execute());
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        try {
                            final ArrayList<LinkedHashMap<String, String>> arrayList = new ArrayList<>();
                            LinkedHashMap<String, String> map;
                            String strResponse = response.body().string();
                            Log.d(TAG, "ДолгиПоНакладным " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            if (jsonObject.getString("data").contains(";")) {
                                String[] data = jsonObject.getString("data").split(";");
                                for (String debtsOnWaybill : data) {
                                    String title = debtsOnWaybill.split("[|]")[0];
                                    String description = debtsOnWaybill.split("[|]")[1];
                                    map = new LinkedHashMap<>();
                                    map.put("title", title);
                                    map.put("description", description);
                                    Log.d(TAG, title);
                                    arrayList.add(map);
                                }
                            }
                            mHandler.post(() -> {
                                listView.setVisibility(View.VISIBLE);
                                listView.setAdapter(new SimpleAdapter(mActivity, arrayList,
                                        R.layout.item_debts_on_waybills, new String[]{"title", "description"},
                                        new int[]{R.id.txt_title, R.id.txt_description}));
                                waitCursor(false);
                            });
                        } catch (JSONException e) {
                            mHandler.post(() -> new GetDebtsOnWaybills(mActivity, StatDocItemFragment.this).execute());
                        }
                    }
                });
            }
        }
    }

    private void prepareOrdersTask(Date requestDate) {
        App settings = App.get(mActivity);
        if (settings.getCodeAgent() != null) {
            waitCursor(true);
            String region = App.getRegion(mActivity);
            if (settings.connectionServer.equals("1")) {
                startOrdersTask(requestDate, null);
            } else {
                OkHttpClientBuilder.buildCall("ЗаказыВОперативке", settings.getCodeAgent() + ";" + TimeFormatter.sdf1C.format(requestDate), region).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        startOrdersTask(requestDate, null);
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        try {
                            String strResponse = response.body().string();
                            Log.d(TAG, "ЗаказыВОперативке " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String args = jsonObject.getString("data");
                            startOrdersTask(requestDate, new String[]{args});
                        } catch (JSONException e) {
                            startOrdersTask(requestDate, null);
                        }
                    }
                });
            }
        }
    }

    private void startOrdersTask(Date requestDate, String[] args) {
        Credentials connectInfo = Credentials.load(mActivity);
        orderListTask = new GetOrderListTask(connectInfo,
                App.get(mActivity).getCodeAgent(),
                requestDate,
                App.get(mActivity).getImpExpPath() + File.separator + Const.OrdersListFileName,
                this);
        orderListTask.execute(args);
    }

    private void prepareReturnsTask(Date requestDate) {
        App settings = App.get(mActivity);
        if (settings.getCodeAgent() != null) {
            waitCursor(true);
            String region = App.getRegion(mActivity);
            if (settings.connectionServer.equals("1")) {
                startReturnsTask(requestDate, null);
            } else {
                OkHttpClientBuilder.buildCall("ВозвратыВОперативке", settings.getCodeAgent() + ";" + TimeFormatter.sdf1C.format(requestDate), region).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        startReturnsTask(requestDate, null);
                    }

                    @Override
                    public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                        try {
                            String strResponse = response.body().string();
                            Log.d(TAG, "ВозвратыВОперативке " + strResponse);
                            JSONObject jsonObject = new JSONObject(strResponse);
                            String args = jsonObject.getString("data");
                            startReturnsTask(requestDate, new String[]{args});
                        } catch (JSONException e) {
                            startReturnsTask(requestDate, null);
                        }
                    }
                });
            }
        }
    }

    private void startReturnsTask(Date requestDate, String[] args) {
        waitCursor(true);

        Credentials connectInfo = Credentials.load(mActivity);
        GetReturnsListTask task = new GetReturnsListTask(connectInfo,
                App.get(mActivity).getCodeAgent(), requestDate,
                App.get(mActivity).getImpExpPath() + File.separator + Const.ReturnsListFileName,
                this);
        task.execute(args);
    }

    @Override
    public void onCompleteGetDebtsOnWaybills(GetDebtsOnWaybills task) {
        try {
            final ArrayList<LinkedHashMap<String, String>> arrayList = new ArrayList<>();
            LinkedHashMap<String, String> map;
            String result = task.get();
            if (result.contains(";")) {
                String[] data = result.split(";");
                for (String debtsOnWaybill : data) {
                    String title = debtsOnWaybill.split("[|]")[0];
                    String description = debtsOnWaybill.split("[|]")[1];
                    map = new LinkedHashMap<>();
                    map.put("title", title);
                    map.put("description", description);
                    Log.d(TAG, title);
                    arrayList.add(map);
                }
                listView.setVisibility(View.VISIBLE);
                listView.setAdapter(new SimpleAdapter(mActivity, arrayList,
                        R.layout.item_debts_on_waybills, new String[]{"title", "description"},
                        new int[]{R.id.txt_title, R.id.txt_description}));
                waitCursor(false);
            }
        } catch (Exception e) {
            //Log.d(TAG, e.getMessage());
            waitCursor(false);
        }
    }


    private void loadfromFile(File returns, Status.Status_type listType) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(returns);
            InputStreamReader isr = new InputStreamReader(fis);
            mItemsList = Status.loadFromStream(isr, listType);
            if (mItemsList != null)
                recalcFromSiteOrders(mItemsList);
            isr.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private void recalcFromSiteOrders(final ArrayList<Status> itemsList) {
        ordersFromSiteCount = 0;
        String fromSite = getString(R.string.from_site);
        for (Status stat : itemsList) {
            if (stat.getRight3Line().contains(fromSite)) {
                ordersFromSiteCount++;
            }
        }
    }


    private void startDeliveryTask(Date requestDate, String[] args) {
        orderStatusTask = new GetOrderStatusTask(mActivity, this, requestDate);
        orderStatusTask.execute(args);
    }


    @Override
    public void onStop() {
        if (orderStatusTask != null && orderStatusTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            orderStatusTask.cancel(true);
        }
        if (tablePartTask != null && tablePartTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            tablePartTask.cancel(true);
        }
        if (orderListTask != null && orderListTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            orderListTask.cancel(true);
        }
        super.onStop();
    }

    private void setAdapter() {
        if (mItemsList != null && mItemsList.size() > 0) {
            //btFilter.setEnabled((mItemsList.size() > 0));
            adapter = new StatusAdapter(mActivity, R.layout.status_item, mItemsList);
            listView.setAdapter(adapter);
            txt_not_data.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.GONE);
            txt_not_data.setVisibility(View.VISIBLE);
        }

    }


    private void waitCursor(boolean value) {
        if (value) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }


    private void loadList(String where, String[] params) {
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) mActivity.getApplication();
            db = app.getDB();
            mItemsList = Status.load(db, where, params);
            orderStatusItems = Status.loadDistinct(db, OrderStatusMetaData.FIELD_STATUS);
            orderTypeItems = Status.loadDistinct(db, OrderStatusMetaData.FIELD_ORDER_TYPE);
        } catch (Exception e) {
            mItemsList = null;
            orderStatusItems = null;
            orderTypeItems = null;
        }
    }

    @Override
    public void onTaskComplete(GetOrderStatusTask orderStatusTask) {
        boolean res;
        try {
            res = orderStatusTask.get();
        } catch (Exception e) {
            res = false;
        }

        if (res) {
            // Записываем дату, загружаем список из БД и выводим на экран
            saveDate();
            loadList(null, null);
            setAdapter();
        } else {
            GlobalProc.mToast(mActivity, "Ошибка загрузки данных");
        }

        waitCursor(false);
    }


    private void saveDate() {
        if (!wasUpdate && requestDate != null) {
            GlobalProc.setPref(mActivity, R.string.pref_lastOrdersStatusUpdate, requestDate.getTime());
        }
    }

    @Override
    public void onTaskComplete(GetOrderListTask orderListTask) {
        if (isAdded()) {
            boolean res;
            try {
                res = orderListTask.get();
            } catch (Exception e) {
                res = false;
            }

            if (res) {
                mItemsList = orderListTask.getItems();
                if (mItemsList != null)
                    recalcFromSiteOrders(mItemsList);
                setAdapter();
            }

            //setFooter(Status.Status_type.Order);
            waitCursor(false);
        }
    }

    @Override
    public void onTaskComplete(GetReturnsListTask returnListTask) {
        if (isAdded()) {
            boolean res;
            try {
                res = returnListTask.get();
            } catch (Exception e) {
                res = false;
            }

            if (res) {
                mItemsList = returnListTask.getItems();
                setAdapter();
            } else {
                mItemsList = null;
                setAdapter();
            }
            //setFooter(Status.Status_type.Return);
            waitCursor(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btShipDate:
                Calendar cal = Calendar.getInstance();
                cal.setTime(requestDate);
                if (requestDate != null) {
                    DatePickerDialog dlg = new DatePickerDialog(mActivity, dcListener,
                            cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH));
                    dlg.show();
                }
                break;
        }
    }


}
