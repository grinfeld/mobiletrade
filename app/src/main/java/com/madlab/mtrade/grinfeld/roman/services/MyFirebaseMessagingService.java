package com.madlab.mtrade.grinfeld.roman.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by GrinfeldRA on 14.02.2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "#MyFirebaseMessagingService";
    public static final String ID_MESSAGE = "id";
    public static final String TAG_MESSAGE = "tag";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        showNotification(remoteMessage);
    }

    private void showNotification(RemoteMessage remoteMessage){
        String title = remoteMessage.getNotification().getTitle();
        String message = remoteMessage.getNotification().getBody();
        String tag = remoteMessage.getNotification().getTag();
        String click_action = remoteMessage.getNotification().getClickAction();
        Intent intent = new Intent(click_action);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Map<String, String> data = remoteMessage.getData();
        intent.putExtra(ID_MESSAGE, data.get("id"));
        intent.putExtra(TAG_MESSAGE, tag);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, MyApp.NOTIFICATION_CHANNEL_ID_INFO)
                //.setSmallIcon(R.mipmap.main_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.ic_stat_name);
            mBuilder.setColor(getResources().getColor(R.color.colorBlue));
        } else {
            mBuilder.setSmallIcon(R.mipmap.main_icon);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0, mBuilder.build());
        }
    }
}
