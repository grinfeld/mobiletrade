package com.madlab.mtrade.grinfeld.roman.entity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.FirmsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.RemainderMetaData;
import com.madlab.mtrade.grinfeld.roman.fragments.GoodsListFragment;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by GrinfeldRA on 28.12.2017.
 */

public class Remainder {

    private static final String TAG = "#Remainder";
    String code;
    String sclad;
    String remainder;
    String reserve;

    public Remainder(String code, String sclad, String remainder, String reserve) {
        this.code = code;
        this.sclad = sclad;
        this.remainder = remainder;
        this.reserve = reserve;
    }


    public String getCode() {
        return code;
    }

    public String getSclad() {
        return sclad;
    }

    public String getRemainder() {
        return remainder;
    }

    public String getReserve() {
        return reserve;
    }

    public static List<String> loadCodeGoodsToSclad(Context ctx, String sclad){
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return loadCodeGoodsToSclad(app.getDB(),sclad);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    private static List<String> loadCodeGoodsToSclad(SQLiteDatabase db, String sclad) {
        List<String> result = new ArrayList<>();
        String where = String.format("%s = '%s'", RemainderMetaData.FIELD_SCLAD, sclad);
        Cursor cursor = null;
        try {
            cursor = db.query(RemainderMetaData.TABLE_NAME, null, where, null, null,
                    null, null);
            while (cursor.moveToNext()){
                String code = cursor.getString(RemainderMetaData.INDEX_FIELD_CODE);
                result.add(code);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public static Set<String> loadScladName(Context ctx) {
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return loadScladName(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    private static Set<String> loadScladName(SQLiteDatabase db) {
        Set<String> result = new LinkedHashSet<>();
        Cursor cursor = null;
        try {
            cursor = db.query(RemainderMetaData.TABLE_NAME, null, null, null, null,
                    null, null);
            while (cursor.moveToNext()){
                String sclad = cursor.getString(RemainderMetaData.INDEX_FIELD_SCLAD);
                result.add(sclad);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }


    public static List<Remainder> load(Context ctx, String id, List<GoodsListFragment.MenuItemObject> menuItemObjects) {
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return load(app.getDB(), id, menuItemObjects);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }


    private static List<Remainder> load(SQLiteDatabase db, String id, List<GoodsListFragment.MenuItemObject> menuItemObjects) {
        List<Remainder> res = new ArrayList<>();
        Cursor rows = null;
        try {
            for (GoodsListFragment.MenuItemObject itemObject : menuItemObjects){
                if (itemObject.isChecked()){
                    String where = String.format("%s = '%s' AND %s = '%s'", RemainderMetaData.FIELD_CODE, id, RemainderMetaData.FIELD_SCLAD, itemObject.getScladName());
                    rows = db.query(RemainderMetaData.TABLE_NAME, null, where, null, null, null, null);
                    Remainder r;
                    while (rows.moveToNext()) {
                        String code = rows.getString(RemainderMetaData.INDEX_FIELD_CODE);
                        String sclad = rows.getString(RemainderMetaData.INDEX_FIELD_SCLAD);
                        String remainder = rows.getString(RemainderMetaData.INDEX_FIELD_REMAINDER);
                        String reserve = rows.getString(RemainderMetaData.INDEX_FIELD_RESERVE);
                        r = new Remainder(code,sclad,remainder,reserve);
                        res.add(r);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }
}
