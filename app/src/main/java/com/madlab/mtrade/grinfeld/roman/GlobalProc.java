package com.madlab.mtrade.grinfeld.roman;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.madlab.mtrade.grinfeld.roman.Const.Theme;

public class GlobalProc {
    public enum WarnTypes {
        Error, Info, Warn
    }

    public static String toForeignPrice(float price, boolean isForeign) {
        try {
            float foreign_price = Float.parseFloat(getPref(MyApp.getContext(), R.string.pref_foreign_price));
            price = isForeign ? price * foreign_price : price;
            return isForeign ? String.format(Locale.US,
                    "%.2f₸", price) : String.format(Locale.US,
                    "%.2fР", price);
        } catch (Exception e) {
            return formatMoney(price);
        }

    }

    private static boolean showDebugInfo = false;

    public static String getPref(Context context, int prefID) {
        return getPref(context, prefID, "");
    }

    public static String getPref(Context context, int prefID,
                                 String defaultValue) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String tmp = context.getString(prefID, "");
        if (tmp.equalsIgnoreCase("")) {
            return "";
        }

        return settings.getString(tmp, defaultValue);
    }

    public static long getPrefLong(Context context, int prefID) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        String section = context.getString(prefID, "");
        long res = 0;
        try {
            res = settings.getLong(section, 0);
        } catch (Exception e) {
            res = 0;
        }

        return res;
    }

    public static String getTimeZone(Context context) {
        String region = App.getRegion(context);
        String timeZone = "GMT+03:00";
        if (region != null) {
            switch (region) {
                case "NV":
                    return "GMT+07:00";
                case "OR":
                    return "GMT+05:00";
                case "SM":
                    return "GMT+04:00";
                case "SR":
                    return "GMT+04:00";
                case "TL":
                    return "GMT+04:00";
                case "UL":
                    return "GMT+04:00";
                case "UF":
                    return "GMT+05:00";
                case "KH":
                    return "GMT+10:00";
                case "CH":
                    return "GMT+05:00";
                default:
                    return timeZone;
            }
        }
        return timeZone;
    }

    public static void closeKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


    public static int getPrefInt(Context context, int prefID, int defaultValue) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        String section = context.getString(prefID, "");
        int res = 0;
        try {
            res = Integer.parseInt(getPref(context, prefID));
        } catch (Exception e) {
            res = defaultValue;
        }

        return res;
    }

    public static byte getPrefByte(Context context, int prefID) {
        byte res = 0;
        try {
            res = Byte.parseByte(getPref(context, prefID));
        } catch (Exception e) {
            // Log.e(TAG, e.toString());
            res = 0;
        }

        return res;
    }

    public static short getPrefShort(Context context, int prefID) {
        short res = 0;
        try {
            res = Short.parseShort(getPref(context, prefID));
        } catch (Exception e) {
            // Log.e(TAG, e.toString());
            res = 0;
        }

        return res;
    }

    public static void setPref(Context context, int prefID, String currentValue) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();

        String currentSection = context.getString(prefID, "");

        if (currentSection != null && currentSection.length() > 0) {
            editor.putString(currentSection, currentValue);

            editor.apply();
        }
    }

    public static void setPref(Context context, int prefID, int value) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        String currentSection = context.getString(prefID, "");
        if (currentSection != null && currentSection.length() > 0) {
            editor.putInt(currentSection, value);

            editor.commit();
        }
    }


    private static final String DEFAULT_IMEI = "0123456789ABCDEFFFFF";
    private static final String DEFAULT_PHONE_NUMBER = "+01234567890";

    @SuppressLint("HardwareIds")
    public static String getIMEI() {
        String IMEI = null;
        Context context = MyApp.getContext();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            try {
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    IMEI = telephonyManager.getDeviceId();
                    return IMEI;
                }
                IMEI = telephonyManager.getDeviceId();
            } catch (Exception e) {
                // do nothing
            }
        }
        return IMEI == null ? DEFAULT_IMEI : IMEI;
    }

    @SuppressLint("HardwareIds")
    public static String getPhoneNumber() {
        String phoneNumber = null;
        Context context = MyApp.getContext();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            try {
                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    phoneNumber = telephonyManager.getLine1Number();
                    return phoneNumber;
                }
                phoneNumber = telephonyManager.getLine1Number();
            } catch (Exception e) {
                // do nothing
            }
        }
        return phoneNumber == null ? DEFAULT_PHONE_NUMBER : phoneNumber;
    }

    public static void setPref(Context context, int prefID, long value) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        String currentSection = context.getString(prefID, "");
        if (currentSection != null && currentSection.length() > 0) {
            editor.putLong(currentSection, value);

            editor.commit();
        }
    }

    public static void setPref(Context context, int prefID, Boolean value) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        String currentSection = context.getString(prefID, "");
        if (currentSection != null && currentSection.length() > 0) {
            editor.putBoolean(currentSection, value);

            editor.commit();
        }
    }

    /**
     * Расшифровывает входную строку
     *
     * @param inStr строка для расшифровки
     * @return расшифрованная строка
     */
    public static String deCode(String inStr) {
        String res = "";
        String[] tmp = inStr.split(" ");

        boolean bit = false;
        byte i = 1;

        if (tmp.length <= 1)
            return inStr;

        for (String item : tmp) {
            if (item.length() < 1)
                continue;

            if (bit) {
                res += (char) (Short.parseShort(item) - i / 2);
            } else {
                res += (char) (Short.parseShort(item) - i);
            }
            i++;
            bit = !bit;
        }

        return res;
    }

    /**
     * Функция распаковки архива
     *
     * @param sourcePath  путь к архиву
     * @param archiveName имя файла для распаковки
     * @return TRUE в случае успеха, False - иначе
     */
    public static boolean unpack(String sourcePath, String destinationPath,
                                 String archiveName) {
        ZipInputStream zis = null;
        InputStream istr = null;
        FileOutputStream fos = null;

        try {
            istr = new FileInputStream(sourcePath + File.separator
                    + archiveName);

            zis = new ZipInputStream(new BufferedInputStream(istr));

            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                }

                String filename = ze.getName();
                byte[] bytes = baos.toByteArray();

                fos = new FileOutputStream(destinationPath + File.separator
                        + filename);
                fos.write(bytes);
            }
        } catch (Exception e) {
            // Log.e(TAG, e.getMessage());
            return false;
        }

        try {
            if (zis != null)
                zis.close();
            if (istr != null)
                istr.close();
            if (fos != null)
                fos.close();
        } catch (Exception e) {
            // return false;
        }

        return true;
    }

    /**
     * Функция создания архива
     *
     * @param sourceDir   папка с файлами
     * @param archiveName имя файла с архивом
     * @return TRUE в случае успеха, False - иначе
     */
    public static boolean addFilesToArchive(String sourceDir, String[] files,
                                            String archiveName) {
        OutputStream ostr = null;
        ZipOutputStream zos = null;
        try {
            ostr = new FileOutputStream(sourceDir + File.separator
                    + archiveName);
            zos = new ZipOutputStream(new BufferedOutputStream(ostr));
            ZipEntry ze;

            for (String current : files) {
                File file = new File(sourceDir, current);
                if (!file.exists())
                    continue;

                FileInputStream fis = new FileInputStream(file);

                byte[] buffer = new byte[1024];
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int count;
                while ((count = fis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                }

                byte[] bytes = baos.toByteArray();
                ze = new ZipEntry(current);
                zos.putNextEntry(ze);
                zos.write(bytes);
                zos.closeEntry();

                fis.close();
            }
        } catch (Exception e) {
            Log.e("!->GlobalProc.PACK()", e.getMessage());
            return false;
        } finally {
            if (zos != null)
                try {
                    zos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return true;
    }

    public static float round(float input, int newScale) {
        return new BigDecimal(input).setScale(newScale, RoundingMode.UP)
                .floatValue();
    }

    // public static void writeLog(String text, boolean append){
    // File errorFile = null;
    // FileWriter writer = null;
    //
    // try{
    // errorFile = new File(mImpExpPath + File.separator + ERROR_FILE_NAME);
    // writer = new FileWriter(errorFile, append);
    // writer.write(text + NewLine);
    // }
    // catch(Exception e){
    // //Log.e(TAG+".writeLog", e.toString());
    // }
    //
    // try {
    // if (writer != null){
    // writer.close();
    // }
    // } catch (Exception e2) {
    // // TODO: handle exception
    // }
    // }

    public static String getVersionName(Context ctx) {
        PackageManager pMan = ctx.getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo = pMan.getPackageInfo(ctx.getPackageName(), 0);
        } catch (Exception e) {
            return "Unknown";
        }
        return pInfo.versionName;
    }

    static int getVersionCode(Context ctx) {
        PackageManager pMan = ctx.getPackageManager();
        PackageInfo pInfo;
        try {
            pInfo = pMan.getPackageInfo(ctx.getPackageName(), 0);
        } catch (Exception e) {
            return 0;
        }

        return pInfo.versionCode;
    }

    public static void mToast(Context context, String text) {
        if (text != null) {
            Toast toast = Toast.makeText(MyApp.getContext(), text, Toast.LENGTH_LONG);
            View view = toast.getView();
            TextView textView = view.findViewById(android.R.id.message);
            textView.setTextColor(Color.WHITE);
            toast.show();
        }
    }

    /**
     * Преобразует строку в число. В случае неудачи возвращает defValue
     *
     * @param candidat строка для преобразования
     * @param defValue значение по-умолчанию
     * @return число
     */
    public static int parseInt(String candidat, int defValue) {
        int res = 0;
        try {
            res = Integer.parseInt(candidat);
        } catch (Exception e) {
            // Log.e(TAG, e.toString());
            res = 0;
        }

        return res;
    }

    /**
     * Преобразует строку в число. В случае неудачи возвращает 0
     *
     * @param candidat строка для преобразования
     * @return число
     */
    public static int parseInt(String candidat) {
        return parseInt(candidat, 0);
    }

    public static float parseFloat(String inputString) {
        float tmp = 0f;
        try {
            tmp = Float.parseFloat(inputString.replace(',', '.'));//deleteSpaces(inputString.replace(',', '.'))
        } catch (Exception e) {
            // Log.e(TAG, e.toString());
            tmp = 0f;
        }
        return tmp;
    }

    /**
     * Нормализует строку удаляя лишние пробелы
     *
     * @param inputString исходная строка
     * @return нормализованная строка
     */
    public static String deleteSpaces(String inputString) {
        return inputString.replaceAll("\\s+", "");
    }

    /**
     * Нормализует строку, которая может быть номером телефона
     *
     * @param phones исходная строка
     * @return нормализованная строка
     */
    public static String normalizePhones(String phones) {
        String res = phones.replaceAll("[^0-9,]", "");
        return res;
    }

    /**
     * Нормализует строку удаляя переносы, кавычки и т. п.
     *
     * @param inputString
     * @return
     */
    public static String normalizeString(String inputString) {
        String res = inputString.replaceAll("[\r\n\'\";%]", "");
        return res;
    }

    public static int getScreenWidth(Activity act) {
        android.util.DisplayMetrics displaymetrics = new android.util.DisplayMetrics();
        if (act != null) {
            act.getWindowManager().getDefaultDisplay()
                    .getMetrics(displaymetrics);
            return displaymetrics.widthPixels;
        } else {
            return 0;
        }
    }

    /**
     * Кодирует входящую строку
     *
     * @param inStr входная строка
     * @return закодированная строка
     */
    public static String enCode(String inStr) {
        String res = "";
        int tmp = 0;

        for (int i = 0; i < inStr.length(); i++) {
            if ((i + 1) % 2 == 0) {
                tmp = ((i + 1) / 2 + (int) inStr.charAt(i));
                res += Integer.toString(tmp);
                res += ' ';
            } else {
                tmp = ((i + 1) + (int) inStr.charAt(i));
                res += Integer.toString(tmp);
                res += ' ';
            }
        }

        return res;
    }

    /**
     * Загружаем нумерацию документов, а заодно проверяем наличие не сохраненной
     * заявки
     *
     * @param - контекст для БД
     */
//    public static void loadDocNumeration(Context context) {
//        SQLiteDatabase db = null;
//        try {
//            MyApp app = (MyApp) context.getApplicationContext();
//            db = app.getDB();
//
//            DBProp.LoadDBProp(db);
//        } catch (Exception e) {
//            Log.e("App.loadDocNumeration", e.getMessage());
//        }
//    }
    public static int getDocsCount(SQLiteDatabase db, String tableName) {
        int result = 0;
        Cursor rows = null;
        String sql = String.format("SELECT count(*) FROM %s", tableName);
        rows = db.rawQuery(sql, null);

        rows.moveToFirst();

        result = rows.getInt(0);

        rows.close();

        return result;
    }

    public static boolean isLocationEnabled(Context ctx) {
        boolean res = false;
        // Проверяем, чтобы оба были включены
        LocationManager lm = (LocationManager) ctx
                .getSystemService(Context.LOCATION_SERVICE);
        boolean gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // Проверяем, существует ли поставщик
        if (!gps) {
            List<String> providers = lm.getProviders(false);
            if (!providers.contains(LocationManager.GPS_PROVIDER)) {
                res = lm.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
            }
            // else{
            // res = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            // }
        } else
            res = gps;
        return res;
    }

    // public static boolean isGPSEnabled(Context ctx) {
    // // Проверяем, если оба выключены - пробуем включить
    // LocationManager lm = (LocationManager) ctx
    // .getSystemService(Context.LOCATION_SERVICE);
    // return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    // }

    public static void showLocationSettings(Activity activity, int dialog_id) {
        // GlobalProc.mToast(OrderGoodsActivity.this,
        // getString(R.string.mes_switch_on_GPS));
        GlobalProc.mToast(activity,
                activity.getString(R.string.mes_switch_on_GPS));

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        activity.startActivityForResult(intent, dialog_id);
    }

//    public static void mNotify(Context ctx, String text) {
//        // NotificationManager manager = (NotificationManager) ctx
//        // .getSystemService(Context.NOTIFICATION_SERVICE);
//        //
//        // String title = ctx.getString(R.string.app_name);
//        // Notification notification = new Notification(R.drawable.dalimo, text,
//        // System.currentTimeMillis());
//        //
//        // if (contentIntent == null) {
//        // Intent mtrade = new Intent(ctx, MTradeActivity.class);
//        // contentIntent = PendingIntent.getActivity(ctx, 1, mtrade, 0);
//        // }
//        //
//        // notification.setLatestEventInfo(ctx, title, text, contentIntent);
//        // manager.notify(1, notification);
//
//        new NotificationCompat.Builder(ctx)
//                .setContentTitle(ctx.getText(R.string.app_label))
//                .setContentText(text).setSmallIcon(R.drawable.dalimo).build();
//    }

//    public static Dialog loadBackupDlg(final Activity ctx, final int id) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
//        builder.setIcon(R.drawable.dalimo);
//        builder.setTitle(ctx.getString(R.string.cap_confirm));
//        builder.setMessage(ctx.getString(R.string.mes_load_order_from_backup));
//
//        builder.setPositiveButton(ctx.getString(R.string.bt_yes),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClickDel(DialogInterface dialog, int which) {
//                        Order.loadUnsavedOrder(ctx);
//
//                        startNewOrderActivity(ctx, id);
//                    }
//                });
//
//        builder.setNegativeButton(ctx.getString(R.string.bt_no),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClickDel(DialogInterface dialog, int which) {
//                        // Удаляем старый документ
//                        Order.deleteUnsavedOrder(ctx.getApplicationContext());
//
//                        // Запускаем процесс формирования новой заявки
//                        // Создаем новый документ и отправляем его на заполнение
//                        Order.newOrder(App.get(ctx).getCodeAgent());
//
//                        startNewOrderActivity(ctx, id);
//                    }
//                });
//
//        builder.setCancelable(true);
//        return builder.create();
//    }
//
//    /**
//     *
//     */
//    public static void startNewOrderActivity(Activity ctx, int id) {
//        Intent clients = new Intent(ctx, ClientListActivity.class);
//        clients.setAction(ctx.getString(R.string.intent_new_order_client));
//        ctx.startActivityForResult(clients, id);
//    }
//
//    public static String md5(String s) {
//        try {
//            // Create MD5 Hash
//            MessageDigest digest = java.security.MessageDigest
//                    .getInstance("MD5");
//            digest.update(s.getBytes());
//            byte messageDigest[] = digest.digest();
//
//            // Create Hex String
//            StringBuilder hexString = new StringBuilder();
//            for (byte aMessageDigest : messageDigest) {
//                String h = Integer.toHexString(0xFF & aMessageDigest);
//                while (h.length() < 2)
//                    h = "0" + h;
//                hexString.append(h);
//            }
//            return hexString.toString();
//        } catch (Exception e) {
//            Log.e("!->md5", e.toString());
//        }
//        return "";
//
//    }

    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

//    public void applyTheme(Activity current, Theme theme) {
//        int androidVer = android.os.Build.VERSION.SDK_INT;
//
//        int res = android.R.style.Theme;
//        switch (theme) {
//            case Default:
//                if (androidVer < 11)
//                    res = android.R.style.Theme;// 16973829
//                else if (androidVer >= 11 && androidVer < 14)
//                    res = 16973931;// Theme_Holo
//                else if (androidVer >= 14) {
//                    res = 16974120;// Theme_DeviceDefault
//                }
//                break;
//            case Dark:
//                if (androidVer < 11)
//                    res = android.R.style.Theme;// 16973829
//                else if (androidVer >= 11 && androidVer < 14)
//                    res = 16973931;// Theme_Holo
//                else if (androidVer >= 14) {
//                    res = 16974120;// Theme_DeviceDefault
//                }
//                break;
//            case Light:
//                if (androidVer <= 10)
//                    res = android.R.style.Theme_Light;// 16973836
//                else if (androidVer > 10 && androidVer < 14)
//                    res = 16973934;// Theme_Holo_Light
//                else if (androidVer >= 14) {
//                    res = 16974123;// Theme_DeviceDefault_Light
//                }
//                break;
//            case Transparent:
//                if (androidVer <= 10)
//                    res = android.R.style.Theme_Wallpaper;// 16973829
//                else if (androidVer > 10 && androidVer < 14)
//                    res = 16973949;// Theme_Holo_Wallpaper
//                else if (androidVer >= 14) {
//                    res = 16974140;// Theme_DeviceDefault_Wallpaper
//                }
//                break;
//        }
//        current.setTheme(res);
//    }

    public static String formatMoney(float money) {
        return Const.formatMoney.format(money);
    }

    public static String formatWeight(float weight) {
        return formatFloat(Const.FORMAT_WEIGHT, weight);
    }

    public static String formatFloat(String format, float floatValue) {
        return String.format(Locale.ENGLISH, format, floatValue);
    }

    @SuppressLint("NewApi")
    public static void setUpAlarm(final Context context, final Intent intent, final int timeInterval) {
        final AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        final PendingIntent pi = PendingIntent.getBroadcast(context, timeInterval, intent, 0);
        am.cancel(pi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(System.currentTimeMillis() + timeInterval, pi);
            am.setAlarmClock(alarmClockInfo, pi);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeInterval, pi);
        else
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + timeInterval, pi);
    }

    public static void log_e(String tag, String msg) {
        log(WarnTypes.Error, tag, msg);
    }

    public static void log_w(String tag, String msg) {
        log(WarnTypes.Warn, tag, msg);
    }

    public static void log_i(String tag, String msg) {
        log(WarnTypes.Info, tag, msg);
    }

    private static void log(WarnTypes wtype, String tag, String msg) {
        if (showDebugInfo)
            switch (wtype) {
                case Error:
                    Log.e(tag, msg);
                    break;
                case Info:
                    Log.i(tag, msg);
                    break;
                case Warn:
                    Log.w(tag, msg);
                    break;
                default:
                    Log.d(tag, msg);
                    break;
            }
    }
}
