package com.madlab.mtrade.grinfeld.roman.entity;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderItemMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;

public class Order {

    public static final String TAG = "!->Order";
    private static final String DEF_CLIENT_CODE = "XXXXXX";

    private static byte zero = (byte) 0;

    public final static byte AUTOORDER_NOT_USED = 0;
    public final static byte AUTOORDER_ONLINE = 1;
    public final static byte AUTOORDER_OFFLINE = 2;

    private ArrayList<OrderItem> mItemsList;

    private float mTotalWeight = 0f;
    private float mTotalSum = 0f;

    private float mTotalWeightUnchecked = 0f;
    private float mTotalSumUnchecked = 0f;
    private short mUncheckedCount = 0;

    //


    public  short getUncheckedCount() {
        return mUncheckedCount;
    }

    public void removeMercuryItems() {
        if (mItemsList != null) {
            for (Iterator<OrderItem> it = mItemsList.iterator(); it.hasNext(); ) {
                OrderItem orderItem = it.next();
                if (orderItem.getIsMercury() == 1) {
                    if (!orderItem.isChecked()) {
                        mUncheckedCount -= 1;
                        mTotalSumUnchecked -= orderItem.getAmount();
                        mTotalWeightUnchecked -= orderItem.getTotalWeight();
                    }
                    it.remove();
                }
            }
            notifyListener();
        }
    }


    public void removeNonCashItems() {
        if (mItemsList != null) {
            for (Iterator<OrderItem> it = mItemsList.iterator(); it.hasNext(); ) {
                OrderItem orderItem = it.next();
                if (orderItem.isNonCash()) {
                    if (!orderItem.isChecked()) {
                        mUncheckedCount -= 1;
                        mTotalSumUnchecked -= orderItem.getAmount();
                        mTotalWeightUnchecked -= orderItem.getTotalWeight();
                    }
                    it.remove();
                }
            }
            notifyListener();
        }
    }



    public static enum State {
        Normal, Deleted
    }


    // public static enum Quality {
    // Normal, NonLiquid
    // };

    /**
     * Номер для неподтвержденной заявки
     */
    public static short UNCONFIRMED_ORDER_NUMBER = 999;

    public final static String STATE_NORMAL = "V";
    public final static String STATE_DELETED = "X";

    public final static String FORMAT_TIME = "HH:mm";
    // public final static String FORMAT_DATE = "yyyy-MM-dd";

    public final static byte QUALITY_NORMAL = 0;
    public final static byte QUALITY_NON_LIQUID = 1;

    private static Handler mHandler;

    /**
     * @author Konovaloff
     * Событие, генерируемое при изменении итогов
     */
    public interface ITotalsChangedListener {
        void onTotalsChanged(short count, float totalSum, float totalWeight,
                             float totalSumUncheked, float totalWeightUncheked);
    }

    private ITotalsChangedListener mItemAddListener;

    /**
     * Уведомляет об изменении количества элементов в заявке, веса, суммы
     */
    private void notifyListener() {
        if (mItemAddListener != null) {
            mItemAddListener.onTotalsChanged(mUncheckedCount, mTotalSum,
                    mTotalWeight, mTotalSumUnchecked, mTotalWeightUnchecked);
        }
    }


//    private void notifyListener() {
//        if (mAddItemListener != null) {
//            mAddItemListener
//                    .onItemAdd(mTotalSum, getItemsCount(), mTotalWeight);
//        }
//        if (mRemoveItemListener != null) {
//            mAddItemListener
//                    .onItemAdd(mTotalSum, getItemsCount(), mTotalWeight);
//        }
//    }


    public void setTotalsChangedListener(ITotalsChangedListener listener) {
        mItemAddListener = listener;
    }

    /**
     * Возвращает сумму
     */
    public float getTotalAmount() {
        return mTotalSum;
    }

    /**
     * Возвращает вес
     */
    public float getTotalWeight() {
        return mTotalWeight;
    }

    /**
     * Возвращает количество элементов
     */
    public short getItemsCount() {
        if (mItemsList != null)
            return (short) mItemsList.size();
        else
            return 0;
    }

    public float getTotalWeightUnchecked() {
        return mTotalWeightUnchecked;
    }

    public void setTotalWeightUnchecked(float totalWeightUnchecked) {
        mTotalWeightUnchecked = totalWeightUnchecked;
    }

    public float getTotalSumUnchecked() {
        return mTotalSumUnchecked;
    }

    public void setTotalSumUnchecked(float totalSumUnchecked) {
        mTotalSumUnchecked = totalSumUnchecked;
    }

    /**
     * Номер заявки (идентификатор)
     */
    private short mNumOrder;

    public short getNumOrder() {
        return mNumOrder;
    }

    public void setNumOrder(short numOrder) {
        mNumOrder = numOrder;
        if (mItemsList != null) {
            for (OrderItem item : mItemsList) {
                item.setNumOrder(numOrder);
            }
        }
    }

    /**
     * Код менеджера
     */
    public String mCodeAgent;

    public int typeServer;


    /**
     * Клиент
     */
    private Client mClient;

    public void setClient(Client value) {
        // При изменении клиента нужно убрать признак
        if (mClient != null && value != null
                && !mClient.getCode().equalsIgnoreCase(value.getCode())) {
            mWasPhotoReport = false;
        }
        mClient = value;
    }

    public Client getClient() {
        return mClient;
    }

    /**
     * Комментарий к заявке
     */
    public String mNote;

    /**
     * Дата поставки
     */
    private Date mDateShip;

    public void setDateShip(Date data) {
        mDateShip = data;
    }

    public void setDateShip(long data) {
        mDateShip = new Date(data);
    }

    public Date getDateShip() {
        return mDateShip;
    }

    /**
     * Время принятия заявки
     */
    private Date mGetOrderTime;

    public Date getGetOrderTime() {
        return mGetOrderTime;
    }

    /**
     * Возвращает время принятия заявки в формате FORMAT_TIME
     *
     * @return
     */
    public String getOrderTime() {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIME,
                Locale.getDefault());
        return sdf.format(mGetOrderTime);
    }

    /**
     * Состояние заявки: V - нормальное Х - помечена на удаление
     */
    public State mState;

    /**
     * Тип оплаты. {1 - "На реализацию", 2 - "Предоплата", 3 -
     * "Оплата при отгрузке", 4 - "Бонус"}
     */
    public byte mTypePay;

    /**
     * Цель визита. {1 - "Получить деньги", 2 - "Записаться на оплату", 3 -
     * "Без оплаты не отгружать"}
     */
    public byte mTypeVisit;

    /**
     * Забрать деньги
     */
    public boolean mTakeMoney;

    /**
     * Признак необходимости ветеринарного удостоверения.
     */
    public boolean mVetSpr;

    /**
     * Тип оплаты (нал = 1/безнал = 2)
     */
    public byte mTypeMoney;

    /**
     * Печатать чек (да = 1/нет =2)
     */
    public byte mPrintBill;


    public static void changeTypeMoney(Context context) {
        currentOrder.mPrintBill = 2;
        currentOrder.mTypeMoney = 1;
        updateUnsavedOrder(context);
    }

    /**
     * Качество
     */
    private byte mQuality;

    public byte quality() {
        return mQuality;
    }

    public void quality(byte value) {
        mQuality = value;
    }

    /**
     * Резерв
     */
    public boolean mReserved;

    /**
     * Сертификат
     */
    public boolean mSertificates;

    /**
     * Качественник
     */
    public boolean mQualityDocs;

    /**
     * Признак использования автозаказа
     */
    private byte mWasAutoOrder;

    public void wasAutoOrder(byte newValue) {
        mWasAutoOrder = newValue;
    }

    public byte wasAutoOrder() {
        return mWasAutoOrder;
    }

    /**
     * Признак проверки остатков
     */
    private boolean mWasCheck;

    public void wasCheck(boolean newValue) {
        mWasCheck = newValue;
    }

    public boolean wasCheck() {
        return mWasCheck;
    }

    /**
     * Признак об успешном выполнении фотоотчета
     */
    public boolean mWasPhotoReport;

    /**
     * Признак ознакомления с новинками
     */
    public boolean mWasNewbeeShown;

    /**
     * Признак необходимости доверенности
     */
    public boolean mWarranty;

    /**
     * Сумма, которую надо забрать
     */
    private float mSumToGet;

    public void sumToGet(float sumToGet) {
        this.mSumToGet = sumToGet;
    }

    public float sumToGet() {
        return mSumToGet;
    }

    /**
     * Признак "Везу сам"
     */
    private boolean mSelfDelivery;

    public boolean selfDelivery() {
        return mSelfDelivery;
    }

    public void selfDelivery(boolean value) {
        mSelfDelivery = value;
    }

    /**
     * Ссылка на визит
     */
    private UUID mVisit_fk;

    public void visitFK(UUID id) {
        mVisit_fk = id;
    }

    public UUID visitFK() {
        return mVisit_fk;
    }

    public short mFirmFK;

    public void firmFK(short value) {
        mFirmFK = value;
    }

    public short firmFK() {
        return mFirmFK;
    }

    /**
     * Номера документов в программе 1С
     */
    private String mDocNums;

    public void DocNums(String nums) {
        mDocNums = nums;
    }

    public String DocNums() {
        return mDocNums;
    }

    private OrderItem.ICheckedChanged listener = item -> {
        if (item.isChecked()) {
            mTotalSumUnchecked -= item.getAmount();
            mTotalWeightUnchecked -= item.getTotalWeight();
            mUncheckedCount--;
        } else {
            mTotalSumUnchecked += item.getAmount();
            mTotalWeightUnchecked += item.getTotalWeight();
            mUncheckedCount++;
        }
        notifyListener();
    };


    /**
     * Конструктор
     */
    public Order(short num, String codeAgent, Client client, Date dateShip,
                 Date getOrderTime, State orderState, byte typePay, byte typeVisit,
                 float sumToGet, byte typeMoney, byte typeDoc, boolean getMoney,
                 boolean vetSpravka, boolean sertificates, boolean qualityDocs,
                 byte quality, byte wasAutoOrder, boolean warranty,
                 boolean selfDelivery, boolean wasCheck, byte firmFK, int typeServer) {

        OrderItem.setCheckedListener(listener);

        mNumOrder = num;
        mCodeAgent = codeAgent;
        mClient = client;

        Calendar now = Calendar.getInstance();

        Calendar tommorow = Calendar.getInstance();
        tommorow.add(Calendar.DATE, 1);

        if (dateShip == null) {
            mDateShip = tommorow.getTime();
        } else
            mDateShip = dateShip;

        if (mGetOrderTime == null) {
            mGetOrderTime = now.getTime();
        } else {
            mGetOrderTime = getOrderTime;
        }

        mState = orderState;
        mTypePay = typePay;
        mTypeVisit = typeVisit;

        mSumToGet = sumToGet;

        mTypeMoney = typeMoney;
        mPrintBill = typeDoc;

        mTakeMoney = getMoney;
        mVetSpr = vetSpravka;

        mSertificates = sertificates;
        mQualityDocs = qualityDocs;

        mQuality = quality;
        mWasAutoOrder = wasAutoOrder;
        mWarranty = warranty;
        mSelfDelivery = selfDelivery;
        mWasCheck = wasCheck;

        mWasPhotoReport = false;
        mWasNewbeeShown = false;

        mFirmFK = firmFK;

        mDocNums = "";

        mItemsList = new ArrayList<OrderItem>();

        this.typeServer = typeServer;
    }

    public Order(short numOrder) {
        this(numOrder, "", null, null, null, State.Normal, zero, zero, 0f,
                zero, zero, false, false, false, false, QUALITY_NORMAL,
                AUTOORDER_NOT_USED, false, false, false, zero, 0);
    }

    /**
     * Добавляет элемент в конец списка
     */
    public boolean addItem(OrderItem item) {
        if (mItemsList != null && item != null) {
            item.setNumOrder(mNumOrder);

            mTotalSum += item.getAmount();
            mTotalWeight += item.getTotalWeight();

            if (!item.isChecked()) {
                mTotalSumUnchecked += item.getAmount();
                mTotalWeightUnchecked += item.getTotalWeight();
                mUncheckedCount++;
            }

            mItemsList.add(item);
            notifyListener();
        } else {
            return false;
        }

        return true;
    }

    /**
     * Ищет элемент по коду товара. Если не находит, возвращает null
     *
     * @param code код товара
     * @return OrderItem if found, else null
     */
    public OrderItem find(String code) {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).getCodeGoods().equalsIgnoreCase(code)) {
                return mItemsList.get(i);
            }
        }

        return null;
    }

    /**
     * Ищет элемент в заявке. Возвращает индекс элемента
     *
     * @param item заявки
     * @return -1 если не найден
     */
    public int getIndex(OrderItem item) {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).equals(item)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Возвращает индекс первого найденного элемента с заданным кодом
     *
     * @param code Код товара
     * @return индекс элемента в случае успеха, -1 иначе
     */
    public short getIndex(String code) {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).getCodeGoods().equalsIgnoreCase(code)) {
                return (short) i;
            }
        }
        return -1;
    }

    /**
     * Добавляет группу элементов
     *
     * @param items
     */
    public void addItems(ArrayList<OrderItem> items) {
        if (mItemsList == null)
            mItemsList = new ArrayList<OrderItem>();

        for (OrderItem oi : items) {
            if (oi == null)
                continue;

            if (find(oi.getCodeGoods()) != null) {
                continue;
            }

            oi.setNumOrder(mNumOrder);

            mTotalSum += oi.getAmount();
            mTotalWeight += oi.getTotalWeight();

            if (!oi.isChecked()) {
                mTotalSumUnchecked += oi.getAmount();
                mTotalWeightUnchecked += oi.getTotalWeight();
                mUncheckedCount++;
            }

            mItemsList.add(oi);
        }
        notifyListener();
    }

    /**
     * пересчитывaет сумму и вес элементов
     */
    public void recalcAmount(boolean isSave) {
        mTotalSum = 0f;
        mTotalWeight = 0f;
        mUncheckedCount = 0;
        mTotalSumUnchecked = 0f;
        mTotalWeightUnchecked = 0f;
        for (OrderItem item : mItemsList) {
            mTotalSum += item.getAmount();
            mTotalWeight += item.getTotalWeight();
            if (!item.isChecked()) {
                mTotalSumUnchecked += item.getAmount();
                mTotalWeightUnchecked += item.getTotalWeight();
                mUncheckedCount++;
            }
        }
        if (!isSave){
            notifyListener();
        }
    }


    /**
     * Удаляет элемент из списка по его индексу
     */
    public void removeItem(int index) {
        OrderItem oi = mItemsList.get(index);

        mTotalSum -= oi.getAmount();
        mTotalWeight -= oi.getTotalWeight();

        if (!oi.isChecked()) {
            mTotalSumUnchecked -= oi.getAmount();
            mTotalWeightUnchecked -= oi.getTotalWeight();
            mUncheckedCount--;
        }

        mItemsList.remove(index);
        notifyListener();
    }

    /**
     * Удаляет все элементы табличной части
     */
    public void clear() {
        mTotalSum = 0f;
        mTotalWeight = 0f;

        mTotalSumUnchecked = 0f;
        mTotalWeightUnchecked = 0f;
        mUncheckedCount = 0;

        mItemsList.clear();
        notifyListener();
    }

    /**
     * Обновляет данные элемента с номером index в соответствии с данными oi
     */
    public void updateItem(int index, OrderItem oi) {
        OrderItem old = mItemsList.get(index);
        if (old != null) {
            mTotalSum -= old.getAmount();
            mTotalWeight -= old.getTotalWeight();

            if (!old.isChecked()) {
                mTotalSumUnchecked -= old.getAmount();
                mTotalWeightUnchecked -= old.getTotalWeight();
            }

            mItemsList.set(index, oi);

            mTotalSum += oi.getAmount();
            mTotalWeight += oi.getTotalWeight();

            if (!oi.isChecked()) {
                mTotalSumUnchecked += oi.getAmount();
                mTotalWeightUnchecked += oi.getTotalWeight();
                mUncheckedCount++;
            }

            notifyListener();
            old = null;
        }
    }

    /**
     * Возвращает элемент по индексу
     */
    public OrderItem getItem(int index) {
        if (mItemsList != null) {
            if (-1 < index && index < mItemsList.size()) {
                return mItemsList.get(index);
            }
        }

        return null;
    }

    /**
     * Аксессор для списка товаров
     */
    public ArrayList<OrderItem> getItems() {
        return mItemsList;
    }

    /**
     * Снимает пометки для признака отсутствия на складе
     */
    public void clearMark() {
        for (OrderItem item : mItemsList) {
            item.notInStock(false);
        }
    }

    /**
     * Снимает пометки с отмеченных для удаления
     */
    public void clearChecked() {
        for (OrderItem item : mItemsList) {
            item.isChecked(false);
        }
    }

    /**
     * Проверяет наличие отмеченных для удаления товаров в заявке
     */
    public int hasChecked() {
        for (int i = 0; i < mItemsList.size(); i++) {
            if (mItemsList.get(i).isChecked()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Загружает сведения о заявке из БД по номеру
     */
    public static Order load(final SQLiteDatabase db, short index) {
        try {
            return load(db, String.format("%s = %d", OrderMetaData.FIELD_NUM, index));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Загружает сведения о заявке из БД по условию
     */
    public static Order load(final SQLiteDatabase db, String where)
            throws Exception {
        Cursor results = db.query(OrderMetaData.TABLE_NAME, null, where, null,
                null, null, null);

        if (!results.moveToFirst()) {
            results.close();
            return null;
        }

        // 0 FIELD_NUM
        short index = results.getShort(OrderMetaData.FIELD_NUM_INDEX);
        Order res = new Order(index);

        // 1 - State
        res.mState = (results.getString(OrderMetaData.FIELD_STATE_INDEX)
                .contains(Order.STATE_NORMAL)) ? State.Normal : State.Deleted;
        // 2 - CodeAgent
        res.mCodeAgent = results
                .getString(OrderMetaData.FIELD_CODE_AGENT_INDEX);
        // 3 - CodeCli
        res.mClient = Client.load(db,
                results.getString(OrderMetaData.FIELD_CLIENT_INDEX));
        // 4 - DateShip
        String ds = results.getString(OrderMetaData.FIELD_DATE_SHIP_INDEX);
        res.mDateShip = TimeFormatter.parseSqlDate(ds);
        // 5 - TypePay
        res.mTypePay = (byte) results
                .getShort(OrderMetaData.FIELD_TYPE_PAY_INDEX);
        // 6 - Type P or N
        res.mPrintBill = (byte) results
                .getShort(OrderMetaData.FIELD_TYPE_DOC_INDEX);
        // 7 - Type Nal or Beznal
        res.mTypeMoney = (byte) results
                .getShort(OrderMetaData.FIELD_TYPE_MONEY_INDEX);
        // 8 - TypeDo
        res.mTypeVisit = (byte) results
                .getShort(OrderMetaData.FIELD_TYPE_VISIT_INDEX);
        // 9 - GetReturn
        res.mTakeMoney = (results.getShort(OrderMetaData.FIELD_GET_MONEY_INDEX) == 1) ? true
                : false;
        // 10 - Veterinar
        res.mVetSpr = (results.getShort(OrderMetaData.FIELD_VETERINAR_INDEX) == 1) ? true
                : false;
        // 11 - TotalWeight
//		res.mTotalWeight =
//				 results.getFloat(OrderMetaData.FIELD_TOTAL_WEIGHT_INDEX);
        // 12 - TotalSum
        // res.mTotalSum =
        // results.getFloat(OrderMetaData.FIELD_TOTAL_SUM_INDEX);
        // 13 - Note
        res.mNote = results.getString(OrderMetaData.FIELD_NOTE_INDEX);
        // 14 - Признак резерва
        res.mReserved = results.getShort(OrderMetaData.FIELD_RESERVED_INDEX) == 1;
        // 15 - Качество
        res.mQuality = (byte) results
                .getShort(OrderMetaData.FIELD_QUALITY_INDEX);
        // 16 - была ли проверка остатков
        res.mWasCheck = results
                .getShort(OrderMetaData.FIELD_WAS_REST_CHECK_INDEX) == 1;

        // 17 - нужна ли доверенность
        res.mWarranty = results.getShort(OrderMetaData.FIELD_WARRANTY_INDEX) == 1;

        // 18 - сумма, которую надо забрать
        res.mSumToGet = results.getFloat(OrderMetaData.FIELD_SUM_TO_GET_INDEX);

        // 19 - нужна ли доверенность
        res.selfDelivery(results
                .getShort(OrderMetaData.FIELD_SELF_DELIVERY_INDEX) == 1);

        // 20 - тип автозаказа
        res.wasAutoOrder((byte) results
                .getShort(OrderMetaData.FIELD_WAS_AUTOORDER_INDEX));


        // 21 - ссылка на визит
        try {
            String s = results.getString(OrderMetaData.FIELD_VISIT_FK_INDEX);
            UUID id = UUID.fromString(s);
            res.visitFK(id);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        // 22 ссылка на выбранную фирму
        res.firmFK(results.getShort(OrderMetaData.FIELD_FIRM_FK_INDEX));

        // Преобразуем время принятия заявки из строки во время
        String tmp = results
                .getString(OrderMetaData.FIELD_GET_ORDER_TIME_INDEX);
        int hour;
        int minute;
        try {
            String h = tmp.substring(0, 2);
            String m = tmp.substring(3, 5);
            hour = Integer.parseInt(h);
            minute = Integer.parseInt(m);
        } catch (Exception e) {
            hour = 0;
            minute = 0;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        res.mGetOrderTime = cal.getTime();


        //24 подгружаем номера документов
        res.DocNums(results.getString(OrderMetaData.FIELD_DOC_NUMS_INDEX));

        res.typeServer = results.getInt(OrderMetaData.FIELD_TYPE_SERVER_INDEX);

        //Закрываем запрос
        results.close();

        // А теперь табличная часть
        String sql = String
                .format("SELECT * FROM %s "
                                + "INNER JOIN %s ON (%s.%s = %s.%s) "
                                + "WHERE %s = %d",
                        // FROM
                        GoodsMetaData.TABLE_NAME,
                        // INNER JOIN
                        OrderItemMetaData.TABLE_NAME,
                        // ON
                        GoodsMetaData.TABLE_NAME,
                        GoodsMetaData.FIELD_CODE.Name,
                        OrderItemMetaData.TABLE_NAME,
                        OrderItemMetaData.FIELD_CODE,
                        // WHERE
                        // OrderItemMetaData.TABLE_NAME,
                        OrderItemMetaData.FIELD_NUM, index);

        results = db.rawQuery(sql, null);
        int fieldIndex = -1;
        while (results.moveToNext()) {
            OrderItem item = new OrderItem();// index, results.getString(3)

            item.setNumOrder(index);
            // Название товара - 0
            fieldIndex = results
                    .getColumnIndex(GoodsMetaData.FIELD_NAME_FULL.Name);
            item.setNameGoods(results.getString(fieldIndex));

            // Признак весового товара - 1
            fieldIndex = results
                    .getColumnIndex(GoodsMetaData.FIELD_IS_WEIGHT.Name);
            item.mIsWeightGoods = (results.getShort(fieldIndex) == 1);

            // Quant - 2
            fieldIndex = results.getColumnIndex(GoodsMetaData.FIELD_QUANT.Name);
            if (res.mQuality == QUALITY_NORMAL) {
                item.setQuant(results.getFloat(fieldIndex));
            }

            // Вес - 3
            fieldIndex = results
                    .getColumnIndex(GoodsMetaData.FIELD_WEIGHT.Name);
            item.setWeight(results.getFloat(fieldIndex));

            // Остаток на складе
            fieldIndex = results
                    .getColumnIndex(GoodsMetaData.FIELD_REST.Name);
            item.restOnStore(results.getFloat(fieldIndex));

            // Количество в упаковке (Справочно).
            // Для количества упаковок есть mCountPack
            fieldIndex = results.getColumnIndex(GoodsMetaData.FIELD_NUM_IN_PACK.Name);
            item.numInPack(results.getFloat(fieldIndex));

            // Код товара
            fieldIndex = results.getColumnIndex(OrderItemMetaData.FIELD_CODE);
            item.setCodeGoods(results.getString(fieldIndex));

            // Количество
            fieldIndex = results.getColumnIndex(OrderItemMetaData.FIELD_COUNT);
            item.setCount(results.getInt(fieldIndex));

            // Множитель, для упаковок. Справочник загружается отдельно numInPack
            //  fieldIndex = results.getColumnIndex(OrderItemMetaData.TABLE_NAME+"."+OrderItemMetaData.FIELD_NUM_IN_PACK);
            fieldIndex = results.getColumnIndex(OrderItemMetaData.FIELD_NUM_IN_PACK);
            int mCountPack = results.getInt(fieldIndex);
            item.setCountPack(mCountPack);

            // Признак счета в упаковках
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IN_PACK);
            if (fieldIndex > 0) {
                item.inPack(results.getShort(fieldIndex) == 1);
            }

            // Цена
            fieldIndex = results.getColumnIndex(OrderItemMetaData.FIELD_PRICE);
            item.setPrice(results.getFloat(fieldIndex));

            // Признак спеццены
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IS_SPEC_PRICE);
            item.mIsSpecialPrice = results.getShort(fieldIndex) == 1 ? true
                    : false;

            // Необходимость сертификата
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IS_SERT);
            item.isSertificateNeed(results.getShort(fieldIndex) == 1 ? true
                    : false);

            // Необходимость качественного удостоверения
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IS_QUALITY);
            item.isQualityDocNeed(results.getShort(fieldIndex) == 1 ? true
                    : false);

            // Полголовки
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IS_HALF_HEAD);
            item.mIsHalfHead = results.getShort(fieldIndex) == 1;

            // Выделение
            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_HIGHLIGHT);
            item.mHighLight = (byte) results.getShort(fieldIndex);


            fieldIndex = results
                    .getColumnIndex(OrderItemMetaData.FIELD_IS_CHECK);
            item.isChecked(results.getInt(fieldIndex) == 1);

            Goods goods = Goods.load(db, item.getCodeGoods());

            item.setIsMercury(goods.getIsMercury());

            item.setNonCash(goods.isNonCash());

            item.recalcAmount();

            res.addItem(item);
        }

        results.close();

        res.checkAllItemsHaveSertificatesChecked();

        return res;
    }

    /**
     * Проверяем, у всех ли товаров проставлен признак с сертификатами
     */
    public void checkAllItemsHaveSertificatesChecked() {
        if (mItemsList.size() < 1) return;

        for (OrderItem item : mItemsList) {
            if (!item.isSertificateNeed()) {
                mSertificates = false;
                return;
            }
        }

        mSertificates = true;
    }

    public void setSertificateSignForAllItems(boolean newValue) {
        for (OrderItem item : mItemsList) {
            item.isSertificateNeed(newValue);
        }
    }

    public void setQualityDocSignForAllItems(boolean newValue) {
        for (OrderItem item : mItemsList) {
            item.isQualityDocNeed(newValue);
        }
    }

    /**
     * Insert
     *
     * @param db
     * @return
     */
    public boolean insert(SQLiteDatabase db) {
        // Общая часть
        ContentValues common = new ContentValues();

        common.put(OrderMetaData.FIELD_NUM, mNumOrder);
        common.put(OrderMetaData.FIELD_STATE,
                (mState == State.Normal) ? STATE_NORMAL : STATE_DELETED);
        common.put(OrderMetaData.FIELD_CODE_AGENT, mCodeAgent);
        common.put(OrderMetaData.FIELD_CLIENT,
                mClient == null ? DEF_CLIENT_CODE : mClient.getCode());
        common.put(OrderMetaData.FIELD_DATE_SHIP, TimeFormatter.sdfSQL.format(mDateShip));
        common.put(OrderMetaData.FIELD_TYPE_PAY, mTypePay);
        common.put(OrderMetaData.FIELD_TYPE_DOC, mPrintBill);
        common.put(OrderMetaData.FIELD_TYPE_MONEY, mTypeMoney);

        common.put(OrderMetaData.FIELD_TYPE_VISIT, mTypeVisit);

        common.put(OrderMetaData.FIELD_GET_MONEY, mTakeMoney ? 1 : 0);
        common.put(OrderMetaData.FIELD_VETERINAR, mVetSpr ? 1 : 0);
        common.put(OrderMetaData.FIELD_NOTE, mNote == null ? " " : mNote);

        common.put(OrderMetaData.FIELD_TOTAL_WEIGHT, mTotalWeight);
        common.put(OrderMetaData.FIELD_TOTAL_SUM, mTotalSum);
        common.put(OrderMetaData.FIELD_GET_ORDER_TIME, getOrderTime());

        common.put(OrderMetaData.FIELD_RESERVED, mReserved ? 1 : 0);
        common.put(OrderMetaData.FIELD_QUALITY, mQuality);
        common.put(OrderMetaData.FIELD_WAS_REST_CHECK, mWasCheck ? 1 : 0);
        common.put(OrderMetaData.FIELD_WARRANTY, mWarranty);
        common.put(OrderMetaData.FIELD_SUM_TO_GET, mSumToGet);
        common.put(OrderMetaData.FIELD_SELF_DELIVERY, mSelfDelivery);
        common.put(OrderMetaData.FIELD_WAS_AUTOORDER, mWasAutoOrder);
        if (mVisit_fk != null) {
            common.put(OrderMetaData.FIELD_VISIT_FK, mVisit_fk.toString());
        }
        common.put(OrderMetaData.FIELD_FIRM_FK, mFirmFK);
        common.put(OrderMetaData.FIELD_DOC_NUMS, mDocNums);

        db.beginTransaction();

        try {
            if (db.insert(OrderMetaData.TABLE_NAME, null, common) == -1) {
                db.endTransaction();
                return false;
            }
        } catch (Exception e) {
            db.endTransaction();
            Log.e("OrderInsertError", e.toString());
            // App.writeLog(e.toString(), true);
            return false;
        }
        // А теперь табличная часть
        for (OrderItem oi : mItemsList) {
            ContentValues item = new ContentValues();
            item.put(OrderItemMetaData.FIELD_NUM, oi.getNumOrder());
            item.put(OrderItemMetaData.FIELD_CODE, oi.getCodeGoods());
            item.put(OrderItemMetaData.FIELD_NUM_IN_PACK, oi.getCountPack());
            item.put(OrderItemMetaData.FIELD_COUNT, oi.getCount());
            item.put(OrderItemMetaData.FIELD_PRICE, oi.getPrice());
            item.put(OrderItemMetaData.FIELD_AMOUNT, oi.getAmount());
            item.put(OrderItemMetaData.FIELD_WEIGHT, oi.getTotalWeight());
            item.put(OrderItemMetaData.FIELD_IS_SERT,
                    oi.isSertificateNeed() ? 1 : 0);
            item.put(OrderItemMetaData.FIELD_IS_QUALITY,
                    oi.isQualityDocNeed() ? 1 : 0);
            item.put(OrderItemMetaData.FIELD_IS_SPEC_PRICE,
                    oi.mIsSpecialPrice ? 1 : 0);
            item.put(OrderItemMetaData.FIELD_IS_HALF_HEAD, oi.mIsHalfHead ? 1
                    : 0);
            item.put(OrderItemMetaData.FIELD_HIGHLIGHT, oi.mHighLight);
            item.put(OrderItemMetaData.FIELD_IN_PACK, oi.inPack());

            try {
                if (db.insert(OrderItemMetaData.TABLE_NAME, null, item) == -1) {
                    db.endTransaction();
                    return false;
                }
            } catch (Exception e) {
                db.endTransaction();
                Log.e("OrderItemInsertError", "item: " + oi.getCodeGoods()
                        + " " + e.toString());
                // App.writeLog(e.toString(), true);
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    /**
     * Update
     *
     * @param db
     * @return
     */
    @SuppressLint("LongLogTag")
    public boolean update(SQLiteDatabase db) {
        // Общая часть
        ContentValues common = new ContentValues();

        common.put(OrderMetaData.FIELD_NUM, mNumOrder);
        common.put(OrderMetaData.FIELD_STATE,
                (mState == Order.State.Normal) ? STATE_NORMAL : STATE_DELETED);
        common.put(OrderMetaData.FIELD_CODE_AGENT, mCodeAgent);
        common.put(OrderMetaData.FIELD_CLIENT,
                mClient == null ? DEF_CLIENT_CODE : mClient.getCode());
        common.put(OrderMetaData.FIELD_DATE_SHIP, TimeFormatter.sdfSQL.format(mDateShip));
        common.put(OrderMetaData.FIELD_TYPE_PAY, mTypePay);
        common.put(OrderMetaData.FIELD_TYPE_DOC, mPrintBill);
        common.put(OrderMetaData.FIELD_TYPE_MONEY, mTypeMoney);

        common.put(OrderMetaData.FIELD_TYPE_VISIT, mTypeVisit);
        //Если стоит без оплаты не отгружать
        if (mTypeVisit == 3) {
            mSumToGet = mTotalSum;
        }
        common.put(OrderMetaData.FIELD_GET_MONEY, mTakeMoney ? 1 : 0);
        common.put(OrderMetaData.FIELD_VETERINAR, mVetSpr ? 1 : 0);
        common.put(OrderMetaData.FIELD_NOTE, mNote == null ? " " : mNote);

        common.put(OrderMetaData.FIELD_TOTAL_WEIGHT, mTotalWeight);
        common.put(OrderMetaData.FIELD_TOTAL_SUM, mTotalSum);
        // Время обновления нам не нужно
        // common.put(OrderMetaData.FIELD_GET_ORDER_TIME, getOrderTime());

        common.put(OrderMetaData.FIELD_RESERVED, mReserved ? 1 : 0);
        common.put(OrderMetaData.FIELD_QUALITY, mQuality);
        common.put(OrderMetaData.FIELD_WAS_REST_CHECK, mWasCheck ? 1 : 0);
        common.put(OrderMetaData.FIELD_WARRANTY, mWarranty);
        common.put(OrderMetaData.FIELD_SUM_TO_GET, mSumToGet);
        common.put(OrderMetaData.FIELD_SELF_DELIVERY, mSelfDelivery);
        common.put(OrderMetaData.FIELD_WAS_AUTOORDER, mWasAutoOrder);
        if (mVisit_fk != null) {
            common.put(OrderMetaData.FIELD_VISIT_FK, mVisit_fk.toString());
        }
        common.put(OrderMetaData.FIELD_FIRM_FK, mFirmFK);
        common.put(OrderMetaData.FIELD_DOC_NUMS, mDocNums);
        common.put(OrderMetaData.FIELD_TYPE_SERVER, typeServer);
        db.beginTransaction();

        String where = String.format("%s = %d", OrderMetaData.FIELD_NUM,
                mNumOrder);
        try {
            if (db.update(OrderMetaData.TABLE_NAME, common, where, null) == -1) {
                db.endTransaction();
                return false;
            }

        } catch (Exception e) {
            Log.e("!->OrderUpdateError", e.toString());
            db.endTransaction();
            return false;
        }

        // А теперь табличная часть
        if (mItemsList != null && mItemsList.size() > 0) {
            // Удаляем табличную часть
            String query = OrderItemMetaData.Clear(mNumOrder);
            try {
                db.execSQL(query);
            } catch (Exception e) {
                Log.e(TAG + ".clear table part", e.toString());
            }

            for (OrderItem oi : mItemsList) {
                ContentValues item = new ContentValues();

                item.put(OrderItemMetaData.FIELD_NUM, oi.getNumOrder());
                item.put(OrderItemMetaData.FIELD_CODE, oi.getCodeGoods());
                item.put(OrderItemMetaData.FIELD_NUM_IN_PACK, oi.getCountPack());
                item.put(OrderItemMetaData.FIELD_COUNT, oi.getCount());
                item.put(OrderItemMetaData.FIELD_PRICE, oi.getPrice());
                item.put(OrderItemMetaData.FIELD_AMOUNT, oi.getAmount());
                item.put(OrderItemMetaData.FIELD_WEIGHT, oi.getTotalWeight());
                item.put(OrderItemMetaData.FIELD_IS_SERT,
                        oi.isSertificateNeed() ? 1 : 0);
                item.put(OrderItemMetaData.FIELD_IS_QUALITY,
                        oi.isQualityDocNeed() ? 1 : 0);
                item.put(OrderItemMetaData.FIELD_IS_SPEC_PRICE,
                        oi.mIsSpecialPrice ? 1 : 0);
                item.put(OrderItemMetaData.FIELD_IS_HALF_HEAD,
                        oi.mIsHalfHead ? 1 : 0);
                item.put(OrderItemMetaData.FIELD_HIGHLIGHT, oi.mHighLight);
                item.put(OrderItemMetaData.FIELD_IN_PACK, oi.inPack());
                item.put(OrderItemMetaData.FIELD_IS_CHECK, oi.isChecked() ? 1 : 0);
                try {
                    if (db.insert(OrderItemMetaData.TABLE_NAME, null, item) == -1) {
                        db.endTransaction();
                        return false;
                    }
                } catch (Exception e) {
                    Log.e("!->OrderItemInsertError",
                            "item: " + oi.getCodeGoods() + " " + e.toString());
                    db.endTransaction();
                }
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    /**
     * Удаляет из базы данных документ с указанным номером (orderID)
     *
     * @param db      - ссылка на подключение к БД
     * @param orderID - номер документа
     */
    public static void deleteOrder(SQLiteDatabase db, short orderID) {
        // Создаем два запроса, один на удаление табличной части, второй - для
        // общей
        String[] sql = {OrderItemMetaData.Clear(orderID),
                OrderMetaData.Clear(orderID)};

        DBHelper.execMultipleSQLTXN(db, sql);
    }

    public static boolean isExist(SQLiteDatabase db, short orderID) {
        boolean res = false;
        Cursor rows = null;
        try {
            String sql = String.format(Locale.ENGLISH,
                    "SELECT * FROM %s WHERE %s = %d", OrderMetaData.TABLE_NAME,
                    OrderMetaData.FIELD_NUM, orderID);
            rows = db.rawQuery(sql, null);
            res = rows.moveToFirst();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            rows.close();
        }

        return res;
    }

    // changeDeleteMark
    public static boolean changeDeleteMark(SQLiteDatabase db, short orderID) {
        String sql = String.format("SELECT %s " + "FROM %s " + "WHERE %s = %d",
                OrderMetaData.FIELD_STATE, OrderMetaData.TABLE_NAME,
                OrderMetaData.FIELD_NUM, orderID);

        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);

            if (!cursor.moveToFirst()) {
                return false;
            }

            State lastState = cursor.getString(0).contains(STATE_NORMAL) ? State.Normal
                    : State.Deleted;

            ContentValues common = new ContentValues();

            common.put(OrderMetaData.FIELD_STATE,
                    (lastState == State.Normal) ? STATE_DELETED : STATE_NORMAL);

            String where = String.format("%s = %d", OrderMetaData.FIELD_NUM,
                    orderID);

            try {
                db.update(OrderMetaData.TABLE_NAME, common, where, null);
            } catch (Exception e) {
                Log.e("!->OrderMarkError", e.toString());
                return false;
            }

            if (mHandler != null) {
                mHandler.sendEmptyMessage(2);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return true;
    }

    public void deleteChecked(boolean isSave) {
        if (mItemsList == null)
            return;

        ArrayList<OrderItem> candidat = new ArrayList<OrderItem>();

        for (int i = 0; i < mItemsList.size(); i++) {
            OrderItem item = mItemsList.get(i);
            if (item != null && item.isChecked()) {
                candidat.add(item);
            }
        }

        if (candidat != null && candidat.size() > 0) {
            boolean res = mItemsList.removeAll(candidat);
            if (res) {
                Log.i(TAG, "Группа элементов успешно удалена");
            } else {
                Log.i(TAG, "Ошибка при удалении группы элементов");
            }
        }
        recalcAmount(isSave);
    }

    public static boolean isUnsavedOrderExists(Context ctx) {
        boolean res = false;
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            res = isExist(app.getDB(), UNCONFIRMED_ORDER_NUMBER);
        } catch (Exception e) {
            Log.e("isUnsavedOrderExists", e.toString());
            res = false;
        }
        return res;
    }

    /**
     * Записываем данные не сохраненной заявки. Если существует - обновляем
     *
     * @param context
     */
    @SuppressLint("LongLogTag")
    public static void saveUnconfirmedOrder(Context context) {
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            if (isExist(app.getDB(), UNCONFIRMED_ORDER_NUMBER)) {
                currentOrder.update(app.getDB());
            } else {
                currentOrder.insert(app.getDB());
            }
        } catch (Exception e) {
            Log.e(TAG + ".saveUnconfirmedMonitoring", e.toString());
            currentOrder = null;
        }
    }

    /**
     * Загружает несохраненную заявку из БД. При ошибке загрузки - возвращает
     * null
     *
     * @param context контекст
     */
    public static void loadUnsavedOrder(Context context) {
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            currentOrder = Order.load(app.getDB(), Order.UNCONFIRMED_ORDER_NUMBER);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            currentOrder = null;
        }
    }

    public static void deleteUnsavedOrder(Context context) {
        try {
            MyApp app = (MyApp) context.getApplicationContext();

            deleteUnsavedOrder(app.getDB());
        } catch (Exception e) {

        }
    }

    public static void deleteUnsavedOrder(SQLiteDatabase db) {
        // Удаляем заявку
        Order.deleteOrder(db, Order.UNCONFIRMED_ORDER_NUMBER);
    }

    public static void updateUnsavedOrder(Context context) {
        try {
            MyApp app = (MyApp) context.getApplicationContext();

            currentOrder.update(app.getDB());
        } catch (Exception e) {
            currentOrder = null;
        }
    }

    // Заявка
    private static Order currentOrder;

    public static Order getOrder() {
        return currentOrder;
    }

    public static void setOrder(Order value) {
        currentOrder = value;
    }

    public static void newOrder(String codeAgent) {
        currentOrder = new Order(Order.UNCONFIRMED_ORDER_NUMBER);
        currentOrder.mCodeAgent = codeAgent;
    }

    /**
     * Ищем элемент в списке, если находим - заменяем, если не находим - добавляем
     *
     * @param newItem
     */
    public void updateItem(OrderItem newItem) {
        boolean wasFind = false;
        for (int i = 0; i < mItemsList.size(); i++) {
            OrderItem existed = mItemsList.get(i);
            if (existed.getCodeGoods().equalsIgnoreCase(newItem.getCodeGoods())) {
                mTotalSum -= existed.getAmount();
                mTotalWeight -= existed.getTotalWeight();

                if (!existed.isChecked()) {
                    mTotalSumUnchecked -= existed.getAmount();
                    mTotalWeightUnchecked -= existed.getTotalWeight();
                }

                mItemsList.set(i, newItem);

                mTotalSum += newItem.getAmount();
                mTotalWeight += newItem.getTotalWeight();

                if (!newItem.isChecked()) {
                    mTotalSumUnchecked += newItem.getAmount();
                    mTotalWeightUnchecked += newItem.getTotalWeight();
                }

                wasFind = true;
                break;
            }
        }

        if (!wasFind) {
            mItemsList.add(newItem);
        }
    }
}
