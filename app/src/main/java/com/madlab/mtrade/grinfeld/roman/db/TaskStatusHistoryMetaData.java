package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class TaskStatusHistoryMetaData implements BaseColumns {
	private TaskStatusHistoryMetaData() {}

	public static final String TABLE_NAME = "TaskStatusHistory";

	public static final String FIELD_ID = "rowid";
	
	public static final String FIELD_STATUS_FK = "StatusFK";
	public static final String FIELD_DATE_OF_CHANGE = "DateOfChange";
	public static final String FIELD_TASK_FK = "taskFK";

	public static final byte FIELD_ROWID_INDEX = 0;
	
	public static final byte FIELD_STATUS_FK_INDEX = 1;
	public static final byte FIELD_DATE_OF_CHANGE_INDEX = 2;
	public static final byte FIELD_TASK_FK_INDEX = 3;
	
	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s ("
					+ "%s	INTEGER		NOT NULL DEFAULT 0,"
					+ "%s	datetime	NOT NULL DEFAULT 'now',"
					+ "%s	INTEGER		NOT NULL DEFAULT 0)",
					TABLE_NAME,
					FIELD_STATUS_FK,
					FIELD_DATE_OF_CHANGE,
					FIELD_TASK_FK);

	public static String insertQuery(int status_id, String sqlDateDue, int taskID) {
		return String.format("INSERT INTO %s ("
				+ "%s, %s, %s) VALUES ("
				+ "%d, %s, %d)",
				TABLE_NAME,
				FIELD_STATUS_FK, FIELD_DATE_OF_CHANGE, FIELD_TASK_FK,
				status_id, sqlDateDue, taskID);
	}
}
