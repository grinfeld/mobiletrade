package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class PaymentsMetaData implements BaseColumns {
	public static final String TABLE_NAME = "Payment";

	public static final String FIELD_CODE_AGENT = "CodeAgent";
	public static final String FIELD_CODE_CLIENT = "CodeCli";
	public static final String FIELD_DOC_NUMBER = "NumDoc";
	public static final String FIELD_DOC_DATE = "DateDoc";
	public static final String FIELD_DOC_SUM = "SumDoc";
	public static final String FIELD_AMOUNT = "Amount";
	public static final String FIELD_SENDED = "SendedSign";
	public static final String FIELD_VISIT_FK = "VisitFK";
	public static final String FIELD_REC_TYPE = "RecType";

	public static final byte FIELD_ID_INDEX = 0;
	public static final byte FIELD_CODE_AGENT_INDEX = 1;
	public static final byte FIELD_CODE_CLIENT_INDEX = 2;
	public static final byte FIELD_DOC_NUMBER_INDEX = 3;
	public static final byte FIELD_DOC_DATE_INDEX = 4;
	public static final byte FIELD_DOC_SUM_INDEX = 5;
	public static final byte FIELD_AMOUNT_INDEX = 6;
	public static final byte FIELD_SENDED_INDEX = 7;
	public static final byte FIELD_VISIT_FK_INDEX = 8;
	public static final byte FIELD_REC_TYPE_INDEX = 9;

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s ("
					+ "%s	INTEGER	PRIMARY KEY AUTOINCREMENT,"
					+ "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
					+ "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
					+ "%s	nvarchar(10)	NOT NULL DEFAULT '',"
					+ "%s	datetime		NOT NULL DEFAULT 0,"
					+ "%s	nvarchar(8)		NOT NULL DEFAULT '',"
					+ "%s	money			NOT NULL DEFAULT 0,"
					+ "%s	smallint        NOT NULL DEFAULT 0,"
					+ "%s	nvarchar(36)	NOT NULL DEFAULT '',"
					+ "%s	smallint        NOT NULL DEFAULT 0)", TABLE_NAME,
			_ID, FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_DOC_NUMBER,
			FIELD_DOC_DATE, FIELD_DOC_SUM, FIELD_AMOUNT, FIELD_SENDED,
			FIELD_VISIT_FK, FIELD_REC_TYPE);

	public static final String INDEX = String.format(
			"CREATE INDEX idxPaymentClient ON %s ( %s, %s, %s, %s )",
			TABLE_NAME, FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_DOC_NUMBER,
			FIELD_DOC_DATE);

	public static final String INDEX_SENDED = String.format(
			"CREATE INDEX idxPaymentSended ON %s ( %s )", TABLE_NAME,
			FIELD_SENDED);

	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;
}
