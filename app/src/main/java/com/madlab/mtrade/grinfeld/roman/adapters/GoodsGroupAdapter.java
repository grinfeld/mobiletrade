package com.madlab.mtrade.grinfeld.roman.adapters;


import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.R;

public class GoodsGroupAdapter extends ArrayAdapter<Node> {
    private final static String TAG = "!->GoodsGroupAdapter";
    private final static int TAB = 20;

    private Context cntx;
    private ArrayList<Node> mNodes;
    private Drawable collapsed;
    private Drawable expanded;
    private Drawable discount;

    @SuppressLint("NewApi")
    public GoodsGroupAdapter(Context context, ArrayList<Node> nodes) {
        super(context, R.layout.goods_group_item, nodes);
        cntx = context;
        mNodes = nodes;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            collapsed = context.getDrawable(R.mipmap.add);
            expanded = context.getDrawable(R.mipmap.remove);
            discount = context.getDrawable(R.mipmap.discount);
        } else {
            collapsed = ContextCompat.getDrawable(context, R.mipmap.add);
            expanded = ContextCompat.getDrawable(context, R.mipmap.remove);
            discount = ContextCompat.getDrawable(context, R.mipmap.discount);
        }
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder = null;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) cntx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.goods_group_item, null);

            holder = new ViewHolder();
            holder.tvTitle = (TextView) v.findViewById(R.id.ggi_title);
            //holder.ibIcon = (ImageButton) v.findViewById(R.id.ggi_image);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Node node = null;
        try {
            node = mNodes.get(position);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            node = null;
        }

        if (node != null) {
            if (holder.tvTitle != null) {
                Drawable left = null;
                Drawable right = null;

                left = node.isExpanded() ? expanded : collapsed;
                right = node.discount() > 0 ? discount : null;
                holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);

                if (node.isSelected()) {
                    v.setBackgroundColor(Color.GRAY);
                } else {
                    v.setBackgroundColor(Color.TRANSPARENT);
                }

                holder.tvTitle.setPadding(node.getLevel() * TAB, 0, 0, 0);

                int cnt = node.getChild() != null ? node.getChild().size() : 0;
                String txt = cnt > 0 ? String.format("%s (%d)", node.getName(), cnt) : node.getName();
                holder.tvTitle.setText(txt);
            }
        }

        return v;
    }

    private static class ViewHolder {
        public TextView tvTitle;
    }

    @Override
    public int getCount() {
        return mNodes == null ? 0 : mNodes.size();
    }
}
