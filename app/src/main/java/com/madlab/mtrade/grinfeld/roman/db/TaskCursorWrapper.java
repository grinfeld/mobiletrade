package com.madlab.mtrade.grinfeld.roman.db;


import java.util.UUID;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;
import com.madlab.mtrade.grinfeld.roman.entity.Task;

public class TaskCursorWrapper extends CursorWrapper {
    private static final String TAG = "!->TaskCursorWrapper";

    public TaskCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Task getTask() {
        UUID uuid = null;
        try {
            uuid = UUID.fromString(getString(TaskMetaData.FIELD_UUID_INDEX));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        String title = getString(TaskMetaData.FIELD_TITLE_INDEX);
        String ac = getString(TaskMetaData.FIELD_AGENT_INDEX);
        String cc = getString(TaskMetaData.FIELD_CLIENT_INDEX);
        long crDate = getLong(TaskMetaData.FIELD_DATE_CREATION_INDEX);
        long dueDate = getLong(TaskMetaData.FIELD_DATE_DUE_INDEX);
        byte curState = (byte)getInt(TaskMetaData.FIELD_STATE_INDEX);
        int unconfirmed = getInt(TaskMetaData.FIELD_UNCONFIRMED_TASK_INDEX);
        boolean reserved = getInt(TaskMetaData.FIELD_SENDED_INDEX) == 1;
        Task result = new Task(uuid, ac, cc, title, dueDate, curState, unconfirmed, reserved);
        result.addCreationDate(crDate);
        return result;
    }

}
