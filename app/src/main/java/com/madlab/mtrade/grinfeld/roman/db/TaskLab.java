package com.madlab.mtrade.grinfeld.roman.db;


import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInTask;
import com.madlab.mtrade.grinfeld.roman.entity.Task;


public class TaskLab {
    private final static String TAG = "!->TaskLab";

    private static TaskLab sTask;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    private TaskLab(Context context) {
        mContext = MyApp.getContext();
        mDatabase = ((MyApp) mContext).getDB();
    }

    public static TaskLab get(Context context) {
        if (sTask == null) {
            sTask = new TaskLab(context);
        }
        return sTask;
    }


    public void deleteTask(Task t) {
        try {
            String where = String.format("%s = '%s'", TaskMetaData.FIELD_UUID, t.UUID());
            String where2 = String.format("%s = '%s'", TaskItemMetaData.FIELD_TASK_FK, t.UUID());
            mDatabase.beginTransaction();
            mDatabase.delete(TaskMetaData.TABLE_NAME, where, null);
            mDatabase.delete(TaskItemMetaData.TABLE_NAME, where2, null);
            mDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            mDatabase.endTransaction();
        }
    }

    /**
     * Сохраняем в БД
     *
     * @param t
     */
    public void addTask(Task t) {
        ContentValues common = getContentValues(t);
        ArrayList<ContentValues> tablePart = getContentValuesTablePart(t.UUID(), t.goodsList());
        try {
            mDatabase.beginTransaction();
            mDatabase.insert(TaskMetaData.TABLE_NAME, null, common);
            if (tablePart != null) {
                for (ContentValues contentValues : tablePart) {
                    mDatabase.insert(TaskItemMetaData.TABLE_NAME, null,
                            contentValues);
                }
            }
            mDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            mDatabase.endTransaction();
        }
    }

    public ArrayList<Task> getUnsendedTasks(String codeAgent) {
        ArrayList<Task> tasks = null;
        String where = String.format("%s = ?", TaskMetaData.FIELD_AGENT);
        TaskCursorWrapper cursor = queryTask(where, new String[]{codeAgent});
        try {
            if (cursor.moveToFirst()) {
                tasks = new ArrayList<>();
                do {
                    Task t = cursor.getTask();
                    //Добавляем клиента
                    Client c = Client.load(mDatabase, t.mClientCode);
                    String cliName = "";
                    if (c != null) {
                        if (c.mName != null && c.mName.length() > 0)
                            cliName = c.mName;
                        else
                            cliName = c.mNameFull;
                    }
                    t.clientName(cliName);

                    //Добавляем список товаров
                    ArrayList<GoodsInTask> list = GoodsInTask.loadList(mDatabase, t.UUID());
                    t.goodsList(list);

                    tasks.add(t);
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
        } finally {
            cursor.close();
        }
        return tasks;
    }

    public ArrayList<Task> getTasksMerch(String codeAgent) {
        ArrayList<Task> tasks = null;
         String where = String.format(Locale.getDefault(),"%s = ? AND %s = %d",
                TaskMetaData.FIELD_AGENT, TaskMetaData.FIELD_UNCONFIRMED_TASK, Task.SAVED_TASK);
        TaskCursorWrapper cursor = null;
        try {
            cursor = queryTask(where, new String[]{codeAgent});
            if (cursor.moveToFirst()) {
                //Так как мы выбираем заявки по одному клиенту, то данные можно загрузить один раз
                tasks = new ArrayList<>();
                do {
                    Task t = cursor.getTask();
                    // заполняем товарами
                    ArrayList<GoodsInTask> list = GoodsInTask.loadList(mDatabase, t.UUID());
                    t.goodsList(list);
                    tasks.add(t);
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return tasks;
    }

    public ArrayList<Task> getTasksOnClient(String codeAgent, String codeClient) {
        ArrayList<Task> tasks = null;
        String where = String.format("%s = ? AND %s = ?",
                TaskMetaData.FIELD_AGENT, TaskMetaData.FIELD_CLIENT);
        TaskCursorWrapper cursor = null;
        try {
            cursor = queryTask(where, new String[]{codeAgent, codeClient});
            if (cursor.moveToFirst()) {
                //Так как мы выбираем заявки по одному клиенту, то данные можно загрузить один раз
                Client c = Client.load(mDatabase, codeClient);
                String cliName = "";
                if (c != null) {
                    if (c.mName != null && c.mName.length() > 0)
                        cliName = c.mName;
                    else
                        cliName = c.mNameFull;
                }

                tasks = new ArrayList<Task>();

                do {
                    Task t = cursor.getTask();

                    t.clientName(cliName);

                    // заполняем товарами
                    ArrayList<GoodsInTask> list = GoodsInTask.loadList(mDatabase, t.UUID());
                    t.goodsList(list);

                    tasks.add(t);
                    cursor.moveToNext();
                } while (!cursor.isAfterLast());
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return tasks;
    }

    public Task getUnconfirmedTask() {
        Task result = null;
        String sql = String.format(Locale.getDefault(), "select * from %s where %s = %d",
                TaskMetaData.TABLE_NAME,TaskMetaData.FIELD_UNCONFIRMED_TASK, Task.UNCONFIRMED_TASK);
        Cursor cursor = null;
        try {
            cursor = mDatabase.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                UUID uuid = UUID.fromString(cursor.getString(TaskMetaData.FIELD_UUID_INDEX));
                String title = cursor.getString(TaskMetaData.FIELD_TITLE_INDEX);
                String ac = cursor.getString(TaskMetaData.FIELD_AGENT_INDEX);
                String cc = cursor.getString(TaskMetaData.FIELD_CLIENT_INDEX);
                long crDate = cursor.getLong(TaskMetaData.FIELD_DATE_CREATION_INDEX);
                long dueDate = cursor.getLong(TaskMetaData.FIELD_DATE_DUE_INDEX);
                byte curState = (byte) cursor.getInt(TaskMetaData.FIELD_STATE_INDEX);
                int unconfirmed = cursor.getInt(TaskMetaData.FIELD_UNCONFIRMED_TASK_INDEX);
                boolean reserved = cursor.getInt(TaskMetaData.FIELD_SENDED_INDEX) == 1;
                result = new Task(uuid, ac, cc, title, dueDate, curState, unconfirmed, reserved);
                result.addCreationDate(crDate);
                // заполняем товарами
                ArrayList<GoodsInTask> list = GoodsInTask.loadList(mDatabase, result.UUID());
                if (list == null) {
                    list = new ArrayList<>();
                }
                result.goodsList(list);

            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) cursor.close();
        }
        return result;
    }

    public Task getTask(String uuid) {
        Task result = null;
        String whereString = String.format(Locale.getDefault(), "%s = '%s'",
                TaskMetaData.FIELD_UUID, uuid);
        TaskCursorWrapper cursor = null;
        try {
            cursor = queryTask(whereString, null);
            if (cursor.moveToFirst()) {
                result = cursor.getTask();

                // заполняем товарами
                ArrayList<GoodsInTask> list = GoodsInTask.loadList(mDatabase,
                        uuid);
                result.goodsList(list);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (cursor != null) cursor.close();
        }
        return result;
    }

    private static ContentValues getContentValues(Task task) {
        ContentValues values = new ContentValues();

        values.put(TaskMetaData.FIELD_UUID, task.mUUID.toString());
        values.put(TaskMetaData.FIELD_AGENT, task.mAgent);
        values.put(TaskMetaData.FIELD_CLIENT, task.mClientCode);
        values.put(TaskMetaData.FIELD_TITLE, task.mDescription);
        values.put(TaskMetaData.FIELD_DATE_DUE, task.mDateDue);
        values.put(TaskMetaData.FIELD_DATE_CREATION, task.mCreationDate);
        values.put(TaskMetaData.FIELD_STATE, task.mCurrentStatus);
        values.put(TaskMetaData.FIELD_CODE_MANAGER, task.getManagerCode());
        values.put(TaskMetaData.FIELD_SENDED, task.isReserved() ? 1 : 0);
        values.put(TaskMetaData.FIELD_UNCONFIRMED_TASK, task.getUnconfirmed());
        return values;
    }

    private static ArrayList<ContentValues> getContentValuesTablePart(String fk, ArrayList<GoodsInTask> list) {
        if (list == null || list.size() < 1)
            return null;
        ArrayList<ContentValues> result = new ArrayList<ContentValues>();

        for (GoodsInTask git : list) {
            ContentValues values = new ContentValues();

            values.put(TaskItemMetaData.FIELD_GOODS, git.code());
            values.put(TaskItemMetaData.FIELD_PERCENT, git.percent());
            values.put(TaskItemMetaData.FIELD_TOTAL_COUNT, git.count());
            values.put(TaskItemMetaData.FIELD_SOLD, git.sold());
            values.put(TaskItemMetaData.FIELD_TASK_FK, fk);

            result.add(values);
        }

        return result;
    }

    public boolean updateTask(Task task) {
        String whereString = String.format(Locale.getDefault(), "%s = '%s'",
                TaskMetaData.FIELD_UUID, task.mUUID);
        String whereTablePart = String.format(Locale.getDefault(), "%s = '%s'",
                TaskItemMetaData.FIELD_TASK_FK, task.mUUID);

        ContentValues common = getContentValues(task);
        ArrayList<ContentValues> tablePart = getContentValuesTablePart(
                task.UUID(), task.goodsList());

        int itemsUpdatesCount = 0;
        try {
            mDatabase.beginTransaction();

            itemsUpdatesCount = mDatabase.update(TaskMetaData.TABLE_NAME,
                    common, whereString, null);

            // Удялем табличную часть
            mDatabase.delete(TaskItemMetaData.TABLE_NAME, whereTablePart, null);

            // Вставляем табличную часть (если она есть)
            if (tablePart != null && itemsUpdatesCount > 0)
                for (ContentValues values : tablePart) {
                    mDatabase.insert(TaskItemMetaData.TABLE_NAME, null, values);
                }

            mDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            mDatabase.endTransaction();
        }

        return itemsUpdatesCount > 0;
    }

    private TaskCursorWrapper queryTask(String where, String[] selectionArgs) {
        Cursor cursor = mDatabase.query(TaskMetaData.TABLE_NAME, null, where,
                selectionArgs, null, null, null);
        return new TaskCursorWrapper(cursor);
    }
}
