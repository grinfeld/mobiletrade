package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */
public class MonitoringMetaData {

    public static final String TABLE_NAME = "Monitoring";

    public static final String FIELD_ID = "_id";
    public static final String FIELD_CODE_CLIENT = "CodeCli";
    public static final String FIELD_CODE_MANAGER = "CodeManager";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_VISIT_FK = "VisitFk";
    public static final String FIELD_TYPE_SERVER = "MonitoringTypeServer";
    public static final String FIELD_STATE = "MonitoringState";
    public static final String FIELD_UNCONFIRMED_MONITORING = "UNCONFIRMED_MONITORING";
    public static final String FIELD_RESERVED = "Reserved";
    public static final String FIELD_CODE_AGENT = "CodeAgent";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	integer primary KEY_TYPE autoincrement,"
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(6)		NOT NULL DEFAULT 0,"
                    + "%s	DATETIME		NOT NULL DEFAULT 'now',"
                    + "%s	nvarchar(36)	NOT NULL DEFAULT '',"
                    + "%s	nchar(1)		NOT NULL DEFAULT 'V',"
                    + "%s	INTEGER			NOT NULL DEFAULT 0,"
                    + "%s	INTEGER			NOT NULL DEFAULT 0,"
                    + "%s	smallint		NOT NULL DEFAULT 0,"
                    + "%s	nvarchar(6)		NOT NULL DEFAULT '')", TABLE_NAME,
            FIELD_ID, FIELD_CODE_CLIENT, FIELD_CODE_MANAGER, FIELD_DATE,
            FIELD_VISIT_FK, FIELD_STATE, FIELD_TYPE_SERVER, FIELD_UNCONFIRMED_MONITORING,
            FIELD_RESERVED, FIELD_CODE_AGENT);

    public static String Clear(short numOrder) {
        return String.format(Locale.getDefault(),"DELETE FROM %s WHERE %s = %d", TABLE_NAME,
                FIELD_UNCONFIRMED_MONITORING, numOrder);
    }

}
