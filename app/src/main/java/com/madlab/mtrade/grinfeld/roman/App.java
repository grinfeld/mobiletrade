package com.madlab.mtrade.grinfeld.roman;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;
import com.madlab.mtrade.grinfeld.roman.Const.Theme;
import com.madlab.mtrade.grinfeld.roman.iface.IOnChangeSetting;

import org.json.JSONObject;

import static android.content.Context.MODE_APPEND;
import static android.content.Context.MODE_PRIVATE;

/**
 * Настройки приложения
 *
 * @author Konovaloff
 */
public class App {
    private static final String TAG = "!->App";

    private static final String REPORT_FILENAME_FORMAT = "%s_dolgi.html";
    private static final String DEFAULT_ROOT_CODE = "00000";

    private static byte mVisibleWheelItems = 3;

    private static App sAppSettings;
    private static Context sContext;

    private String mCodeAgent = null;
    private String mImpExpPath = null;
    private String mPhotosPath = null;

    // FTP Settings
    private String ftpServer = null;
    private String ftpServerA = null;
    private String ftpLogin = null;
    private String ftpPassword = null;

    private static int serverPort = 11000;
    // Photo server settings
    private static int serverPortPhoto = 10999;
    private static byte serverTimeout = 60;

    private String mRoot = "00000";

    private short mMaxWheelItemsCount = 30;

    private String mBaseMeash = "Штуки";

    public String[] mAutoOrder = null;
    public byte mAutoOrderSelectedIndex = -1;

    private PhoneInfo mPhoneInfo = null;

    public Theme theme = Theme.Default;

    public String connectionServer;

    private int GPSPort = 11001;

    public PhoneInfo getPhoneInfo() {
        return mPhoneInfo;
    }

    /**
     * Код, сохраненный в настройках
     */
    protected String mUnswerCode;

    public static IOnChangeSetting onChangeSetting;

    private static FirebaseDatabase firebaseDatabase;

    private App(Context ctx) {
        sContext = ctx.getApplicationContext();
    }

    public static App get(Context ctx) {
        if (sContext == null)
            sContext = ctx.getApplicationContext();
        if (sAppSettings == null)
            sAppSettings = loadPref(ctx);//new App(ctx);
        return sAppSettings;
    }


    public static FirebaseDatabase getDatabase() {
        if (firebaseDatabase == null) {
            firebaseDatabase = FirebaseDatabase.getInstance();
        }
        return firebaseDatabase;
    }


    static void setRegion(Context context, String region) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("region", region);
        editor.apply();
    }


    public static boolean isMerch(){
//        int tag;
//        try {
//            tag = Integer.parseInt(GlobalProc.getPref(sContext, R.string.pref_is_merch));
//        }catch (NumberFormatException e){
//            return false;
//        }
//        if (tag == 1){
//            return true;
//        }else if (tag == 2){
//            return  false;
//        }else {
//            return false;
//        }
        return false;
    }


    public static String getRegion(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
       // String region = sp.getString("region", null);
        //if (region == null) {
            String serverValue = sp.getString(context.getString(R.string.pref_ftp_server), "-1");
            String[] key_servers_array = context.getResources().getStringArray(R.array.key_servers);
            String[] val_servers_array = context.getResources().getStringArray(R.array.values_servers);
            for (int i = 0; i < val_servers_array.length; i++) {
                if (val_servers_array[i].equals(serverValue)) {
                    String region = key_servers_array[i];
                    return region.substring(region.length() - 3, region.length() - 1);
                }
            }
//            if (region != null) {
//                return region.substring(region.length() - 3, region.length() - 1);
//            }
        //} else {
            //return region;
        //}
        return null;
    }

    public static String getRegionId(Context context) {
        String region_id = "0";
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String serverValue = sp.getString(context.getString(R.string.pref_ftp_server), "-1");
        String[] id_servers_array = context.getResources().getStringArray(R.array.id_servers);
        String[] val_servers_array = context.getResources().getStringArray(R.array.values_servers);
        for (int i = 0; i < val_servers_array.length; i++) {
            if (val_servers_array[i].equals(serverValue)) {
                region_id = id_servers_array[i];
                break;
            }
        }
        return region_id;
    }


    public static String getDomain(Context context) {
        String domain = null;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String serverValue = sp.getString(context.getString(R.string.pref_ftp_server), "-1");
        String[] domain_help_desk = context.getResources().getStringArray(R.array.domain_help_desk);
        String[] val_servers_array = context.getResources().getStringArray(R.array.values_servers);
        for (int i = 0; i < val_servers_array.length; i++) {
            if (val_servers_array[i].equals(serverValue)) {
                domain = domain_help_desk[i];
                break;
            }
        }
        return domain;
    }

    public static String getCatIdHelpDesk(Context context){
        String catId = null;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String serverValue = sp.getString(context.getString(R.string.pref_ftp_server), "-1");
        String[] id_cat_help_desk = context.getResources().getStringArray(R.array.id_cat_help_desk);
        String[] val_servers_array = context.getResources().getStringArray(R.array.values_servers);
        for (int i = 0; i < val_servers_array.length; i++) {
            if (val_servers_array[i].equals(serverValue)) {
                catId = id_cat_help_desk[i];
                break;
            }
        }
        return catId;
    }


    /**
     * Перезагружаем настройки
     *
     * @param ctx
     */
    public static void Reload(Context ctx) {
        sAppSettings = loadPref(ctx);
    }

    /**
     * Возвращает код подтверждения
     *
     * @return строка, 20 символов
     */
    public String getUnswerCode() {
        return mUnswerCode;
    }

    /**
     * Устанавливает код подтверждения
     *
     * @return строка, 20 символов
     */
    public void setUnswerCode(String value) {
        mUnswerCode = value;
    }

    /**
     * Признак необходимости фотоотчета при формировании заявки
     */
    public static boolean mPhotoReport;

    /**
     * Количество фотографий для одного фотоотчета
     */
    private static int mPhotosCount;

    public static void photosCount(int count) {
        if (count > 0) {
            mPhotosCount = count;
        }
    }

    public static int photosCount() {
        return mPhotosCount;
    }

    public String getCodeAgent() {
        return mCodeAgent;
    }


    public String reportFileName() {
        return String.format(Locale.getDefault(),
                REPORT_FILENAME_FORMAT, getCodeAgent());
    }

    public String fullPathOfReport() {
        return String.format("%s%s%s", getImpExpPath(), File.separator, reportFileName());
    }

    public String getImpExpPath() {
        return mImpExpPath;
    }

    public String getPhotosPath() {
        return mPhotosPath;
    }

    public String getFTPServer() {
        return ftpServer;
    }

    public String getFTPServerA() {
        return ftpServerA;
    }

    public String getFTPLogin() {
        return ftpLogin;
    }

    public String getFTPPassword() {
        return GlobalProc.deCode(ftpPassword);
    }

    public void setServerPort(int candidat) {
        if (candidat > 0 && candidat < 65535) {
            serverPort = candidat;
        }
    }

    public int getServerPort() {
        return serverPort;
    }

    public void serverPortPhoto(int candidat) {
        if (candidat > 0 && candidat < 65535) {
            serverPortPhoto = candidat;
        }
    }

    public int serverPortPhoto() {
        return serverPortPhoto;
    }

    public void setServerTimeout(byte timeout) {
        if (timeout > 0) {
            serverTimeout = timeout;
        }
    }

    public byte getServerTimeout() {
        return serverTimeout;
    }

    public String getOrderFileName() {
        return Const.ORDER_FILE_PREFIX + mCodeAgent + Const.EXPORT_FILE_EXT;
    }

    public String getReturnFileName() {
        return Const.RETURN_FILE_PREFIX + mCodeAgent + Const.EXPORT_FILE_EXT;
    }

    public String getPaymentFileName() {
        return Const.PAY_FILE_PREFIX + mCodeAgent + Const.EXPORT_FILE_EXT;
    }

    public String getRootCode() {
        return mRoot;
    }

    //
    public String getMeash() {
        return mBaseMeash;
    }

    public short getMaxWheelItemsCount() {
        return mMaxWheelItemsCount;
    }

    private static App loadPref(Context cnt) {
        App result = new App(cnt);
        try {
            SharedPreferences settings = PreferenceManager
                    .getDefaultSharedPreferences(cnt);

            // Код менеджера
            String prefName = cnt.getString(R.string.pref_codeManager, Const.DEFAULT_CODE_AGENT);
            String tmp = settings.getString(prefName, "");
            result.mCodeAgent = tmp;

            // Код подтверждения
            prefName = cnt.getString(R.string.pref_unswerCode);
            // tmp = cnt.getString(R.string.defaultDeviceCode);
            result.mUnswerCode = settings.getString(prefName, "");

            // Уровень цен
            String[] prices = cnt.getResources().getStringArray(
                    R.array.list_prices);
            prefName = cnt.getString(R.string.pref_priceLevel);
            tmp = settings.getString(prefName, prices[2]);

            int i = 0;
            for (String str : prices) {
                if (str.compareToIgnoreCase(tmp) == 0) {
                    break;
                }
                i++;
            }
            //mPriceLevel = (byte) i;

            // Код корневой группы
            prefName = cnt.getString(R.string.pref_root_code);
            String root = settings.getString(prefName, DEFAULT_ROOT_CODE);
            result.mRoot = root;

            // ед. изм
            prefName = cnt.getString(R.string.pref_meash);
            result.mBaseMeash = settings.getString(prefName, "");

            // Кол-во элементов в колесиках
            prefName = cnt.getString(R.string.pref_maxWheelItemscount);
            short value = (short) settings.getInt(prefName, 30);
            result.mMaxWheelItemsCount = value;

            // Список вариантов автозаказа
            try {
                tmp = settings.getString(cnt.getString(R.string.pref_autoOrder), "AutoOrder");
                if (tmp != null) {
                    StringTokenizer token = new StringTokenizer(tmp, Const.DELIMITTER);

                    byte j = 0;
                    result.mAutoOrder = new String[token.countTokens()];
                    while (token.hasMoreElements()) {
                        result.mAutoOrder[j] = token.nextToken();
                        j++;
                    }
                }
            } catch (Exception e) {
                Log.e(TAG + ".parseAutoOrder", e.toString());
            }

            // Выбранный тип автозаказа
            prefName = cnt.getString(R.string.pref_autoOrderSelectedIndex);
            result.mAutoOrderSelectedIndex = (byte) settings.getInt(prefName, -1);

            // --------------------------------------------FTP
            result.ftpServer = GlobalProc.getPref(cnt, R.string.pref_ftp_server);
            result.ftpLogin = GlobalProc.getPref(cnt, R.string.pref_ftp_login);
            result.ftpPassword = GlobalProc.getPref(cnt, R.string.pref_ftp_password);
            result.ftpServerA = GlobalProc.getPref(cnt, R.string.pref_ftp_serverA);
            result.setGPSPort(GlobalProc.getPrefInt(cnt, R.string.pref_gpsPort, 11001));
            // --------------------------------------------Server
            result.setServerPort(GlobalProc
                    .getPrefInt(cnt, R.string.pref_serverPort, 0));
            result.serverPortPhoto(GlobalProc.getPrefShort(cnt,
                    R.string.pref_serverPortPhoto));
            result.setServerTimeout(GlobalProc.getPrefByte(cnt,
                    R.string.pref_server_timeout));
            result.connectionServer = GlobalProc.getPref(cnt, R.string.pref_connection_server);
            // --------------------------------------------PATH
            PhoneInfo pi = null;
            try {
                pi = PhoneInfo.getInfo(cnt);
            } catch (Exception ignored) {
            }

            result.mPhoneInfo = pi;
            result.mImpExpPath = getExternalStoragePath(pi);

            //После
            result.mPhotosPath = result.mImpExpPath + File.separator + "Photos";
            CreateFolder(result.mPhotosPath);
        } catch (Exception e) {
            Log.e(TAG + ".loadPref", e.toString());
        }
        return result;
    }

    private static String getExternalStoragePath(PhoneInfo pi) {
        String path = Environment.getExternalStorageDirectory().getPath();

        //Если есть доступ к информации о телефоне
        if (pi != null) {
            // сначала получаем версию системы
            float ver = 0;

            try {
                if (pi.getOSVersion().length() <= 3) {
                    ver = Float.valueOf(pi.getOSVersion());
                } else {
                    ver = Float.valueOf(pi.getOSVersion().substring(0, 3));
                }
            } catch (Exception e) {
                ver = 0;
            }

            // Обрабатываем производителя устройства
            if (pi.getManufacturer().contains("samsung")) {
                if (ver >= 4) {
                    path = "//mnt" + File.separator + "extSdCard";
                } else {
                    path += File.separator + "external_sd";
                }
            } else {
                if (pi.getManufacturer().contains("digma") ||
                        pi.getManufacturer().toLowerCase().contains("zte") ||
                        pi.getManufacturer().toLowerCase().contains("irbis")) {
                    path = "//storage/emulated/0";
                } else {
                    path = "//mnt" + File.separator + "sdcard2";
                }
            }
        }

        // Теперь вычисленный путь, если не существует, то оставляем
        // по-умолчанию
        path += File.separator + Const.IMPEXP_FOLDER;
        File tmp = new File(path);
        if (tmp.exists()) {
            return path;
        } else {
            path = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + File.separator + Const.IMPEXP_FOLDER;
            CreateFolder(path);
//
        }

        return path;
    }

    private static void CreateFolder(String fullPath) {
        File folder = new File(fullPath);
        if (!folder.exists()) {
            try {
                if (folder.mkdir()) {
                    Log.d(TAG, "Каталог создан - " + fullPath);
                }
            } catch (Exception e) {
                Log.e(TAG, "Не удалось создать каталог");
            }
        }
    }

    public final byte getVisibleWheelItems() {
        return mVisibleWheelItems;
    }

    private static String replaceLatinLetter(String inStr) {
        String result = "";
        inStr = inStr.trim();

        for (int i = 0; i < inStr.length(); i++) {
            char ch = inStr.charAt(i);
            switch (ch) {
                case 'B':
                    ch = 'В';
                    break;
                case 'K':
                    ch = 'К';
                    break;
                case 'O':
                    ch = 'О';
                    break;
                case 'C':
                    ch = 'С';
                    break;
            }
            result += ch;
        }

        return result;
    }

    public Activity getActivity() {

        return null;
    }

    public static SharedPreferences.OnSharedPreferenceChangeListener listener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    if (!key.equals("TimeZone")) {
                        sAppSettings = loadPref(sContext);
                    }
                    if (key.equals("ftpServer") || key.equals("ManagerCode")) {
                        if (onChangeSetting != null)
                            onChangeSetting.onChangeSetting();
                    }
                }
            };

    public void setGPSPort(int GPSPort) {
        this.GPSPort = GPSPort;
    }

    public void setReserveServer(String server) {
        this.ftpServerA = server;
    }

    public int getGPSPort() {
        return GPSPort;
    }
}
