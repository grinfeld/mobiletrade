package com.madlab.mtrade.grinfeld.roman.entity;

/**
 * Created by GrinfeldRA on 30.06.2018.
 */

public class PaymentTable {

    String nameFirma;
    float payNal;
    float payBezNal;


    public PaymentTable(String nameFirma, float payNal, float payBezNal) {
        this.nameFirma = nameFirma;
        this.payNal = payNal;
        this.payBezNal = payBezNal;
    }

    public String getNameFirma() {
        return nameFirma;
    }

    public float getPayNal() {
        return payNal;
    }

    public float getPayBezNal() {
        return payBezNal;
    }

    public void setNameFirma(String nameFirma) {
        this.nameFirma = nameFirma;
    }

    public void setPayNal(float payNal) {
        this.payNal = payNal;
    }

    public void setPayBezNal(float payBezNal) {
        this.payBezNal = payBezNal;
    }

}
