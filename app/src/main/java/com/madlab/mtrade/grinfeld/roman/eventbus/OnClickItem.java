package com.madlab.mtrade.grinfeld.roman.eventbus;

import com.madlab.mtrade.grinfeld.roman.entity.Goods;

/**
 * Created by GrinfeldRa
 */
public class OnClickItem {

    private Goods goods;
    private int position;

    public OnClickItem(Goods goods, int position) {
        this.goods = goods;
        this.position = position;
    }

    public Goods getGoods() {
        return goods;
    }

    public int getPosition() {
        return position;
    }
}
