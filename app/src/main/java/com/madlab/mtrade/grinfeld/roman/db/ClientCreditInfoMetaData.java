package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class ClientCreditInfoMetaData implements BaseColumns {
    private ClientCreditInfoMetaData() {
    }

    public static final String TABLE_NAME = "ClientCreditInfo";

    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CLIENT = "CodeCli";
    public static final String FIELD_NUMBER = "NumDoc";
    public static final String FIELD_DATE = "DateDoc";
    public static final String FIELD_FIO = "FIO";
    public static final String FIELD_SUM = "SumDoc";
    public static final String FIELD_CREDIT = "CreditDoc";
    public static final String FIELD_EXPEDITOR = "Expeditor";
    public static final String FIELD_EXP_PHONE = "ExpeditorPhone";
    public static final String FIELD_DOC_TYPE = "DocType";
    public static final String FIELD_FIRMA= "Firma";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s (" + "%s	nvarchar(6)	 NOT NULL DEFAULT '',"
                    + "%s	nvarchar(6)	 NOT NULL DEFAULT '',"
                    + "%s	nvarchar(10) NOT NULL DEFAULT '',"
                    + "%s	datetime	 NOT NULL DEFAULT 0,"
                    + "%s	nvarchar(20) NOT NULL DEFAULT '',"
                    + "%s	money		 NOT NULL DEFAULT 0,"
                    + "%s	money		 NOT NULL DEFAULT 0,"
                    + "%s	nvarchar(30) NOT NULL DEFAULT '',"
                    + "%s	nvarchar(30) NOT NULL DEFAULT '',"
                    + "%s	nvarchar(1) NOT NULL DEFAULT '',"
                    + "%s	nvarchar(30) NOT NULL DEFAULT '')", TABLE_NAME,
            FIELD_CODE_AGENT, FIELD_CLIENT, FIELD_NUMBER, FIELD_DATE,
            FIELD_FIO, FIELD_SUM, FIELD_CREDIT, FIELD_EXPEDITOR,
            FIELD_EXP_PHONE, FIELD_DOC_TYPE, FIELD_FIRMA);

    public static final String INDEX = String.format(
            "CREATE INDEX idxClientCreditInfo ON %s ( %s, %s, %s, %s )",
            TABLE_NAME, FIELD_CODE_AGENT, FIELD_CLIENT, FIELD_NUMBER,
            FIELD_DATE);

    public static String insertQuery(String codeAgent, String codeCli,
                                     String numDoc, String dateDoc, String sumDoc, String creditDoc,
                                     String manager, String expeditor, String phones, char type, String firma) {
        return String.format("INSERT INTO %s (" + "%s, %s, %s, %s, %s, "
                        + "%s, %s, %s, %s, %s, %s" + ") VALUES ("
                        + "'%s', '%s', '%s', '%s', %s, "
                        + "%s, '%s', '%s', '%s', '%s', '%s')", TABLE_NAME, FIELD_CODE_AGENT,
                FIELD_CLIENT, FIELD_NUMBER, FIELD_DATE, FIELD_SUM,
                FIELD_CREDIT, FIELD_FIO, FIELD_EXPEDITOR, FIELD_EXP_PHONE,
                FIELD_DOC_TYPE, FIELD_FIRMA, codeAgent, codeCli, numDoc, dateDoc, sumDoc,
                creditDoc, manager, expeditor, phones, type, firma);
    }
}
