package com.madlab.mtrade.grinfeld.roman.entity.promotions;

/**
 * Created by GrinfeldRA
 */

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.PromotionCheckedMetaData;
import com.madlab.mtrade.grinfeld.roman.db.PromotionMetaData;

public class PromotionsBean {

    private static final String TAG = "#Promotions";

    public PromotionsBean() {
    }

    public PromotionsBean(List<Datum> data) {
        this.data = data;
    }

    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<>();

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    @SerializedName("meta")
    @Expose
    private Meta meta;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }


    public static PromotionsBean load() {
        MyApp app = (MyApp) MyApp.getContext();
        return load(app.getDB());
    }

    private static PromotionsBean load(SQLiteDatabase db) {
        PromotionsBean res = null;
        Cursor rows = null;
        try {
            rows = db.query(PromotionMetaData.TABLE_NAME, null, null, null, null, null, null);
            List<Datum> datum = new ArrayList<>();
            while (rows.moveToNext()) {
                int id = rows.getInt(0);
                String title = rows.getString(1);
                String text = rows.getString(2);
                String cover = rows.getString(3);
                String timeStamp = rows.getString(4);
                Cursor cursor = db.rawQuery("select * from " + PromotionCheckedMetaData.TABLE_NAME + " where " + PromotionCheckedMetaData.FIELD_ID + " = " + id, null);
                cursor.moveToFirst();
                int isRead = cursor.getInt(1);
                datum.add(new Datum(id, title, text, cover, timeStamp, false, isRead != 0));
                cursor.close();
            }
            res = new PromotionsBean(datum);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

}