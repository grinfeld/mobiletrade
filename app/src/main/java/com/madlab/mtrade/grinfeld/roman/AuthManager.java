package com.madlab.mtrade.grinfeld.roman;

import android.content.Context;

import com.madlab.mtrade.grinfeld.roman.iface.AuthStateListener;

import java.util.HashSet;
import java.util.Set;


public class AuthManager {

    private static AuthManager INSTANCE;
    private Context context;

    public static void initialize(Context context) {
        if (INSTANCE != null) throw new IllegalStateException("AuthManager already initialized.");
        INSTANCE = new AuthManager(context);
    }

    public static AuthManager getInstance() {
        if (INSTANCE == null) throw new IllegalStateException("AuthManager doesn't initialized.");
        return INSTANCE;
    }


    private final Object lock = new Object();
    private String employeeId;

    private final Set<AuthStateListener> stateListeners = new HashSet<>();

    private AuthManager(Context context) {
        this.context = context;
    }


    public void addAuthStateListener(AuthStateListener stateListener) {
        if (stateListener == null) return;
        stateListeners.add(stateListener);
        stateListener.onAuthStateChanged(employeeId);
    }

    public void removeAuthStateListener(AuthStateListener stateListener) {
        if (stateListener == null) {
            stateListeners.clear();
        } else {
            stateListeners.remove(stateListener);
        }
    }

    public boolean isAuthenticate() {
        synchronized (lock) {
//            return true;
            return employeeId != null;
        }
    }

    public String getEmployeeId() {
        synchronized (lock) {
//            return new AuthData(Branch.SMR, "С03103");
            return employeeId;
        }
    }

    public String signInExplicitly(String employeeId){
        synchronized (lock){
            this.employeeId = employeeId;
            notifyOnAuthStateChanged(employeeId);
            return employeeId;
        }
    }

    public void signOutExplicitly(){
        synchronized (lock){
            employeeId = null;
            notifyOnAuthStateChanged(employeeId);
        }
    }

    private void notifyOnAuthStateChanged(String employeeId){
        for (AuthStateListener stateListener : stateListeners){
            stateListener.onAuthStateChanged(employeeId);
        }
    }
}
