package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class OrderItemMetaData implements BaseColumns {
	private OrderItemMetaData() {}

	public static final String TABLE_NAME = "OrderItem";
	public static final String FIELD_NUM = "NumOrderFK";
	public static final String FIELD_CODE = "OICodeGoods";
	public static final String FIELD_NUM_IN_PACK = "OINumInPack";
	public static final String FIELD_COUNT = "OINumItem";
	public static final String FIELD_PRICE = "OIPrice";
	public static final String FIELD_AMOUNT = "OIAmount";
	public static final String FIELD_WEIGHT = "OIWeight";
	public static final String FIELD_IS_SERT = "OIisSertificate";
	public static final String FIELD_IS_QUALITY = "OIisQualityDoc";
	public static final String FIELD_IS_SPEC_PRICE = "OIisSpecialPrice";
	public static final String FIELD_IS_WEIGHT_GOODS = "OIisWeightGoods";
	public static final String FIELD_IS_HALF_HEAD = "OIisHalfHead";
	public static final String FIELD_INFO = "OIInfo";
	public static final String FIELD_HIGHLIGHT = "OIHighLight";
	public static final String FIELD_IN_PACK = "OIInPack";
	public static final String FIELD_REST_ON_STOR = "OIRestOnStore";
	public static final String FIELD_IS_CHECK = "IsChecked";

	public static final String INDEX = String.format(
			"CREATE INDEX idxNumOrder ON %s ( %s )", TABLE_NAME, FIELD_NUM);

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s ("
					+ "%s	int			NOT NULL DEFAULT 0,"
					+ // FIELD_NUM
					"%s	nvarchar(6)	NOT NULL DEFAULT '',"
					+ // Code
					"%s	smallint	NOT NULL DEFAULT 0,"
					+ // NumInPack
					"%s	integer		NOT NULL DEFAULT 0,"
					+ // count
					"%s	money		NOT NULL DEFAULT 0,"
					+ // price
					"%s	money		NOT NULL DEFAULT 0,"
					+ // amount
					"%s	money		NOT NULL DEFAULT 0,"
					+ // weight
					"%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	smallint	NOT NULL DEFAULT 0,"
					+ "%s	integer		NOT NULL DEFAULT 0,"
					+ "%s	integer		NOT NULL DEFAULT 0)", // restOnStore
			TABLE_NAME, FIELD_NUM, FIELD_CODE, FIELD_NUM_IN_PACK, FIELD_COUNT,
			FIELD_PRICE, FIELD_AMOUNT, FIELD_WEIGHT, FIELD_IS_SERT,
			FIELD_IS_QUALITY, FIELD_IS_SPEC_PRICE, FIELD_IS_HALF_HEAD,
			FIELD_HIGHLIGHT, FIELD_IN_PACK, FIELD_REST_ON_STOR, FIELD_IS_CHECK);

	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	public static String Clear(short numOrder) {
		return String.format("DELETE FROM %s WHERE %s = %d", TABLE_NAME,
				FIELD_NUM, numOrder);
	}
}
