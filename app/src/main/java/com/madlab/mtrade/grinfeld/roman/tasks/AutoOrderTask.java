package com.madlab.mtrade.grinfeld.roman.tasks;


import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.db.AutoorderMetaData;
import com.madlab.mtrade.grinfeld.roman.db.DiscountsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Goods;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.iface.IAutoOrderComplete;


public class AutoOrderTask extends AsyncTask<String, Void, Boolean> {

    public final static byte RESULT_SUCESS = 0;
    public final static byte RESULT_FAILURE = -1;

    private final static String TAG = "!->AutoOrderTask";

    private final static String PROC_NAME = "ПолучитьАвтозаказ";

    // private final static String SUCCESS = "OK";
    private final static String FAILURE = "ERR";

    private DalimoClient myClient = null;

    private Credentials connectInfo;
    private String codeAgent;
    private String codeCli;
    private String autoOrderType;
    private boolean mSignSertificate;
    private boolean mSignQualityDoc;

    private String mUnswer;
    private Context mBaseContext;
    private ProgressDialog mProgress;
    private ArrayList<OrderItem> resultList;
    private IAutoOrderComplete mTaskCompleteListener;

    private ArrayList<String> mExcludeList;
    private ArrayList<OrderItem> mOfflineAutoorder;
    private short delay = 60;
    private Timer mTimer;
    private boolean mTimeout = false;

    private String mesAdded;
    private String mesCancelled;

    public boolean isTimeout() {
        return mTimeout;
    }

    /**
     * Устанавливает список групп для исключения
     *
     * @param excludeList
     */
    public void setExcludeList(final ArrayList<String> excludeList) {
        this.mExcludeList = excludeList;
    }

    public String getMessage() {
        return mUnswer;
    }

    public ArrayList<OrderItem> getItems() {
        return resultList;
    }

    public AutoOrderTask(Context data, String agent, String client,
                         String orderType, boolean isSertificateNeed, boolean isQualityDocNeed) {
        mBaseContext = data;
        codeAgent = agent;
        codeCli = client;
        autoOrderType = orderType;
        mSignSertificate = isSertificateNeed;
        mSignQualityDoc = isQualityDocNeed;

        mUnswer = "";
        mesAdded = mBaseContext.getString(R.string.mes_oi_added);
        mesCancelled = mBaseContext.getString(R.string.mes_cancelled_by_user);
        connectInfo = Credentials.load(data);
    }

    public void setListener(IAutoOrderComplete taskCompleteListener) {
        mTaskCompleteListener = taskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        mTimer = new Timer();
//        mProgress = new ProgressDialog(mBaseContext);
//        mProgress.setMessage(mBaseContext.getString(R.string.mes_wait_for_load));
//        mProgress.setCancelable(true);
//        if (!((Activity) mBaseContext).isFinishing()) {
//            mProgress.show();
//        }

    }

    @Override
    protected Boolean doInBackground(String... strings) {
        if (Order.getOrder() == null || Order.getOrder().getClient() == null) {
            return false;
        }

        boolean result = true;
        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) mBaseContext
                    .getApplicationContext();
            db = app.getDB();

            Client mClient = Client.load(db, codeCli);

            //Загружаем офлайнАвтозаказ
            loadOfflineAutoOrder(db);

            try {
                TimerTask task = new TimerTask() {
                    @Override
                    public void run() {
                        mTimer.cancel();
                        mTimeout = true;
                        resultList = mOfflineAutoorder;
                        normalizeOrder(Order.AUTOORDER_OFFLINE);
                        cancel();
                    }
                };
                mTimer.schedule(task, delay * 1000);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }

            if (strings != null && strings[0] != null) {
                mUnswer = strings[0];
            } else {
                myClient = new DalimoClient(connectInfo);
                if (!myClient.connect()) {
                    mUnswer = myClient.LastError();
                    result = false;
                }

                String command = String.format("%s;%s;%s;%s;%s;%s;%s",
                        Const.COMMAND, codeAgent, PROC_NAME, codeAgent, codeCli.toUpperCase(), autoOrderType,
                        Const.END_MESSAGE);
                myClient.send(command);
                mUnswer = myClient.receive();

                if (isCancelled()) {
                    mUnswer = mesCancelled;
                    result = false;
                }
                // больше они нам не нужен
                myClient.disconnect();
                mTimer.cancel();
            }
            // Если 1С вернула ошибку - заканчиваем
            if (mUnswer != null && mUnswer.contains(FAILURE)) {
                mUnswer = mUnswer
                        .substring(0, mUnswer.indexOf(FAILURE));
                result = false;
            } else {


                String[] goodsList = mUnswer.split(";");

                if (isCancelled()) {
                    mUnswer = mesCancelled;
                    result = false;
                }

                StringTokenizer token;
                resultList = new ArrayList<>();
                byte mPriceLevel = mClient == null ? 2 : mClient.mPriceType;
                byte discount = mClient == null ? 0 : mClient.mDiscount;
                for (String current : goodsList) {
                    if (isCancelled()) {
                        mUnswer = mesCancelled;
                        result = false;
                        break;
                    }
                    token = new StringTokenizer(current, "|");

                    if (token.countTokens() > 1) {
                        //Параметр 0, код товара
                        String codeGoods = token.nextToken();

                        if (isExclude(codeGoods, mExcludeList)) {
                            continue;
                        }

                        //Параметр 1, количество
                        int count = 1;
                        try {
                            String cnt = token.nextToken();
                            if (cnt.length() > 0) {
                                count = GlobalProc.parseInt(cnt);
                            }
                        } catch (NoSuchElementException e) {
                            count = 1;
                        }

                        //Параметр 2, Маст-лист
                        boolean must = false;
                        try {
                            must = token.nextToken().equalsIgnoreCase("1");
                        } catch (NoSuchElementException e) {

                        }

                        //Параметр 3, Новинка
//								boolean newbee = false;
//								try {
//									newbee = token.nextToken() == "1";
//								} catch (NoSuchElementException e) {
//
//								}

                        //Цены пока нет, надо рассчитывать
                        Goods goods = Goods.load(db, codeGoods);
                        if (goods != null) {
                            //Ищем индивидуальную скидку в БД
                            byte individualDiscount =
                                    loadDiscount(db, App.get(mBaseContext).getCodeAgent(), codeCli, codeGoods);
                            if (individualDiscount != 0)
                                discount = individualDiscount;

                            //Рассчитываем цену
                            goods.calcVisiblePrice(mPriceLevel, discount);
                            OrderItem item = new OrderItem(goods);
                            boolean regMercury = Order.getOrder().getClient().isRegMercury();
                            byte mTypeMoney = Order.getOrder().mTypeMoney;
                            if (!regMercury && mTypeMoney == 2) {
                                item.setIsMercury(goods.getIsMercury());
                            }
                            item.setNonCash(goods.isNonCash());
                            if (goods.isWeightGood()) {
                                item.setCount(count);
                            } else {
                                int cnt = (int) (count / goods.getQuant());
                                if (cnt < 1) cnt = 1;
                                item.setCount(cnt);
                            }
                            if (must) {
                                item.mHighLight = OrderItem.H_MUSTLIST;
                            } else {
                                item.mHighLight = OrderItem.H_NOTHING;
                            }

                            item.isChecked(true);
                            item.isSertificateNeed(mSignSertificate);
                            item.isQualityDocNeed(mSignQualityDoc);

                            resultList.add(item);
                        }
                    }
                }
                //Теперь надо проставить цены индивидуально
                mUnswer = mesAdded;
            }

        } catch (Exception e) {
            mUnswer = e.toString();
            Log.e(TAG, mUnswer);
            result = false;
        } finally {
            if (myClient != null) {
                myClient.disconnect();
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mTimer != null) {
            mTimer.cancel();
        }

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }

        //dismiss();
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.hide();
            mProgress.dismiss();
        }

    }

    public void detach() {
        dismiss();
        mBaseContext = null;
    }

    /**
     * Проверяет, находится ли товар в списке на исключение
     *
     * @param codeGoods Код товара
     * @return TRUE Если есть в списке, FALSE иначе
     */
    private boolean isExclude(String codeGoods, ArrayList<String> codesList) {
        if (codesList == null)
            return false;

        int pos = codesList.indexOf(codeGoods);
        boolean res = pos > -1;
//        if (res) {
//            codesList.remove(pos);
//        }

        return res;
    }

    private void loadOfflineAutoOrder(SQLiteDatabase db) {
        String agent = Order.getOrder().mCodeAgent;
        String client = Order.getOrder().getClient().getCode();
        boolean qualityDoc = Order.getOrder().mQualityDocs;
        boolean sertDoc = Order.getOrder().mSertificates;

        String sql = String
                .format("SELECT * FROM %s INNER JOIN %s ON %s.%s = %s.%s WHERE %s = '%s' AND %s = '%s'",
                        // FROM
                        GoodsMetaData.TABLE_NAME,
                        // INNER JOIN
                        AutoorderMetaData.TABLE_NAME,
                        // ON
                        GoodsMetaData.TABLE_NAME,
                        GoodsMetaData.FIELD_CODE.Name,
                        AutoorderMetaData.TABLE_NAME,
                        AutoorderMetaData.FIELD_CODE_GOODS,
                        // WHERE
                        AutoorderMetaData.FIELD_CODE_AGENT, agent,
                        AutoorderMetaData.FIELD_CODE_CLIENT, client);

        ArrayList<OrderItem> result = null;
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, null);
            if (rows.getCount() > 0) {
                result = new ArrayList<OrderItem>();
            }

            while (rows.moveToNext()) {
                OrderItem newItem = new OrderItem();
                newItem.setCodeGoods(rows
                        .getString(GoodsMetaData.FIELD_CODE.Index));
                newItem.isQualityDocNeed(qualityDoc);
                newItem.isSertificateNeed(sertDoc);

                newItem.setNameGoods(rows
                        .getString(GoodsMetaData.FIELD_NAME_FULL.Index));
                newItem.mIsWeightGoods = rows
                        .getInt(GoodsMetaData.FIELD_NAME_FULL.Index) == 1;
                newItem.setQuant(rows
                        .getFloat(GoodsMetaData.FIELD_QUANT.Index));
                newItem.numInPack(rows
                        .getFloat(GoodsMetaData.FIELD_NUM_IN_PACK.Index));

                float price = rows.getFloat(GoodsMetaData.FIELD_PRICE_SHOP.Index);
                ;
                newItem.setPrice(price);

                int columnCountIndex = rows
                        .getColumnIndex(AutoorderMetaData.FIELD_COUNT);
                int columnMLIndex = rows
                        .getColumnIndex(AutoorderMetaData.FIELD_ML_SIGN);
                int columnNewbeeIndex = rows
                        .getColumnIndex(AutoorderMetaData.FIELD_NEWBEE_SIGN);

                int cnt = (int) rows.getFloat(columnCountIndex);
                newItem.setCount(cnt);
                newItem.setWeight(cnt
                        * rows.getFloat(GoodsMetaData.FIELD_WEIGHT.Index));

                if (rows.getInt(columnNewbeeIndex) == 1) {
                    newItem.mHighLight = OrderItem.H_NEWBEE;
                }

                if (rows.getInt(columnMLIndex) == 1) {
                    newItem.mHighLight = OrderItem.H_MUSTLIST;
                }

                newItem.isChecked(true);

                result.add(newItem);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        mOfflineAutoorder = result;
    }

    @SuppressWarnings("unchecked")
    private void normalizeOrder(byte mode) {
        try {
            Order data = Order.getOrder();
            if (data != null && mOfflineAutoorder != null) {
                ArrayList<OrderItem> offlineAO = (ArrayList<OrderItem>) mOfflineAutoorder.clone();
                ArrayList<String> exclusion = (ArrayList<String>) mExcludeList
                        .clone();

                if (exclusion != null) {
                    for (int i = 0; i < offlineAO.size(); i++) {
                        OrderItem item = offlineAO.get(i);
                        String codeGoods = item.getCodeGoods();
                        int pos = exclusion.indexOf(codeGoods);
                        if (pos > -1) {
                            offlineAO.remove(i);
                            exclusion.remove(pos);
                        }
                    }
                }

                data.clear();
                data.addItems(offlineAO);
                data.wasAutoOrder(mode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private byte loadDiscount(SQLiteDatabase db, String agent, String client, String codeGoods) {
        byte result = 0;

        //Загружаем список товаров с ценами
        String sql = String.format("SELECT * FROM %s WHERE %s = 0 AND %s = ? AND %s = ? AND %s = ?",
                DiscountsMetaData.TABLE_NAME,
                DiscountsMetaData.FIELD_IS_GROUP,
                DiscountsMetaData.FIELD_CODE_AGENT,
                DiscountsMetaData.FIELD_CODE_CLIENT,
                DiscountsMetaData.FIELD_CODE_GOODS);

        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, new String[]{agent, client, codeGoods});

            int columnDiscount = rows.getColumnIndex(DiscountsMetaData.FIELD_DISCOUNT);

            if (rows.moveToFirst()) {
                do {
                    result = (byte) rows.getShort(columnDiscount);
                    rows.moveToNext();
                } while (!rows.isAfterLast());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) rows.close();
        }

        return result;
    }
}
