package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.entity.Task;


public class LocationMetadata implements BaseColumns {
	// private LocationMetaData() {}
	public static final String TABLE_NAME = "Location";


	public static final String FIELD_AGENT = "Agent";
	public static final String FIELD_DATE = "DateStamp";
	public static final String FIELD_LATITUDE = "Latitude";
	public static final String FIELD_LONGITUDE = "Longitude";
	public static final String FIELD_ACCURACY = "Accuracy";
	public static final String FIELD_SPEED = "Speed";
	public static final String FIELD_SENDED = "SendedSign";
	public static final String FIELD_PROVIDER = "GPSProvider";
	public static final String FIELD_CLIENT = "ClientCode";

	public static final byte FIELD_ID_NUM = 0;
	public static final byte FIELD_AGENT_NUM = 1;
	public static final byte FIELD_DATE_NUM = 2;
	public static final byte FIELD_LATITUDE_NUM = 3;
	public static final byte FIELD_LONGITUDE_NUM = 4;
	public static final byte FIELD_ACCURACY_NUM = 5;
	public static final byte FIELD_SPEED_NUM = 6;
	public static final byte FIELD_SENDED_NUM = 7;
	public static final byte FIELD_PROVIDER_NUM = 8;
	public static final byte FIELD_CLIENT_NUM = 9;

	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s (" + "%s	INTEGER		PRIMARY KEY AUTOINCREMENT, "
					+ "%s	NVARCHAR(6)	NOT NULL DEFAULT '', "
					+ "%s	DATETIME	NOT NULL DEFAULT 'now',"
					+ "%s	REAL	    NOT NULL DEFAULT 0,"
					+ "%s	REAL	    NOT NULL DEFAULT 0,"
					+ "%s	REAL		NOT NULL DEFAULT 0,"
					+ "%s	REAL		NOT NULL DEFAULT 0,"
					+ "%s	SMALLINT	NOT NULL DEFAULT 0,"
					+ "%s	NVARCHAR(15) NOT NULL DEFAULT '',"
					+ "%s	NVARCHAR(6) NOT NULL DEFAULT '')", TABLE_NAME, _ID,
			FIELD_AGENT, FIELD_DATE, FIELD_LATITUDE, FIELD_LONGITUDE,
			FIELD_ACCURACY, FIELD_SPEED, FIELD_SENDED, FIELD_PROVIDER,
			FIELD_CLIENT);

	public static final String INDEX_SENDED = String.format(
			"CREATE INDEX idxLocationSendedSign ON %s ( %s )", TABLE_NAME,
			FIELD_SENDED);

	public static final String INDEX_AGENT_SENDED = String.format(
			"CREATE INDEX idxLocationAgent_Sended ON %s ( %s, %s )",
			TABLE_NAME, FIELD_AGENT, FIELD_SENDED);

	/**
	 * Возвращает текст запроса для вставки значения в БД
	 *
	 * @param timeStamp
	 *            время
	 * @param latitude
	 *            широта
	 * @param longitude
	 *            долготоа
	 * @param accuracy
	 *            точность
	 * @return текст запроса, String
	 */
	public static String insertQuery(String agentCode, long timeStamp,
									 float latitude, float longitude, float accuracy, float speed,
									 String provider, String codeCli) {
		return String.format(Locale.ENGLISH, "INSERT INTO %s ("
						+ "%s, %s, %s, %s, %s, %s) VALUES ("
						+ "%s, %s, %f, %f, %s, %s)", TABLE_NAME, FIELD_AGENT,
				FIELD_DATE, FIELD_LATITUDE, FIELD_LONGITUDE, FIELD_ACCURACY,
				FIELD_SPEED, FIELD_PROVIDER,
				// VALUES
				agentCode, TimeFormatter.formatSQLWithTime(timeStamp), latitude,
				longitude, accuracy, speed, provider, codeCli);
	}

	public static int insert(SQLiteDatabase db, String codeAgent,
							 long timeStamp, float latitude, float longitude, float accuracy,
							 float speed, String provider, String codeCli, boolean sended) {

		ContentValues values = new ContentValues();
		values.put(FIELD_AGENT, codeAgent);
		values.put(FIELD_DATE, TimeFormatter.formatSQLWithTime(timeStamp));
		values.put(FIELD_LATITUDE, latitude);
		values.put(FIELD_LONGITUDE, longitude);
		values.put(FIELD_ACCURACY, accuracy);
		values.put(FIELD_SPEED, speed);
		values.put(FIELD_PROVIDER, provider);
		values.put(FIELD_CLIENT, codeCli);
		values.put(FIELD_SENDED, sended);

		int id = -1;
		try {
			id = (int) db.insert(TABLE_NAME, null, values);
		} catch (Exception e) {
			id = -1;
		}
		return id;
	}



	public static boolean insert(SQLiteDatabase db, String agent,
								 long timeStamp, float latitude, float longitude, float accuracy,
								 float speed, String provider, String codeCli) {
		return insert(db, agent, timeStamp, latitude, longitude, accuracy,
				speed, provider, codeCli, false) > -1;
	}

	public static void updateLocation(SQLiteDatabase db, int id, long timeStamp, float latitude, float longitude, float accuracy,
									  float speed, String provider){
		ContentValues values = new ContentValues();
		values.put(FIELD_DATE, TimeFormatter.formatSQLWithTime(timeStamp));
		values.put(FIELD_LATITUDE, latitude);
		values.put(FIELD_LONGITUDE, longitude);
		values.put(FIELD_ACCURACY, accuracy);
		values.put(FIELD_SPEED, speed);
		values.put(FIELD_PROVIDER, provider);
		int i = db.update(TABLE_NAME, values, "_id = ?", new String[]{String.valueOf(id)});
		Log.d("#LocationMetadata", String.valueOf(i));

	}

	public static void updateSendedSign(SQLiteDatabase db, int[] indexes) {
		if (indexes != null && indexes.length > 0) {
			String where = String.format("%s = ?", _ID);
			ContentValues values = new ContentValues();
			for (int i : indexes) {
				values.put(FIELD_SENDED, true);
				String[] whereArgs = { Integer.toString(indexes[i]) };
				db.update(TABLE_NAME, values, where, whereArgs);
			}
		}
	}
}
