package com.madlab.mtrade.grinfeld.roman.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.iface.IUpdateComplete;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by GrinfeldRA on 14.02.2018.
 */

public class UpdateRestFrom1CTask extends AsyncTask<String, Integer, Boolean> {
    // private final String SUCCESS = "OK";
    private final String FAILURE = "ERR";

    private final String TAG = "!->UpdateRestFrom1C";
    private final String WAIT_MESSAGE = "Связь с 1С";

    private final String COMMAND_UPDATE = "005";

    private final String GOODS_DIVIDER = ";";
    private final String GOODS_SUB_DIVIDER = "|";

    public static final byte RES_OK = 60;
    public static final byte RES_ERR = 61;

    private String mUnswer = "Ошибка";
    private String mCodeAgent = "XXXXXX";
    private ProgressDialog mProgress;
    private Context baseContext;
    private IUpdateComplete mUpdateCompleteListener;
    private Credentials connectInfo;

    public boolean needWaitingCursor = true;

    public String getMessage() {
        return mUnswer;
    }

    public UpdateRestFrom1CTask(Context data,
                                IUpdateComplete updateCompleteListener) {
        baseContext = data;
        mUpdateCompleteListener = updateCompleteListener;
        connectInfo = Credentials.load(baseContext);
        mCodeAgent = GlobalProc.getPref(baseContext, R.string.pref_codeManager);
    }

    @Override
    protected void onPreExecute() {
        if (needWaitingCursor) {
            mProgress = new ProgressDialog(baseContext);
            mProgress.setMessage(WAIT_MESSAGE);
            mProgress.setCancelable(true);
            mProgress.show();
        }
    }

    @Override
    protected Boolean doInBackground(String... params) {

        Calendar min = Calendar.getInstance();
        min.set(Calendar.HOUR_OF_DAY, 7);
        min.set(Calendar.MINUTE, 0);
        min.set(Calendar.SECOND, 0);
        Calendar max = Calendar.getInstance();
        max.set(Calendar.HOUR_OF_DAY, 19);
        max.set(Calendar.MINUTE, 0);
        max.set(Calendar.SECOND, 0);

        Calendar now = Calendar.getInstance();

        if (now.before(min) || now.after(max)) {
            mUnswer = "Рабочий день окончен. Время работы с 07 до 19 часов";
            return false;
        }
        String message = null;
        if (params != null){
            message = params[0];
        }
        if (message != null){
            if (!message.contains(FAILURE)) {
                if (!parseMessage(message)) {
                    mUnswer = "Ошибка при обработке входящего сообщения";
                    return false;
                }
            } else {
                int index = message.indexOf(FAILURE);
                if (index > 0) {
                    mUnswer = message.substring(0, index);
                }
                return false;
            }
        }else {
            String data = String.format("%s;%s;", COMMAND_UPDATE, mCodeAgent);
            DalimoClient myClient = new DalimoClient(connectInfo);
            if (!myClient.connect()) {
                mUnswer = "Не удалось подключиться";
                return false;
            }
            myClient.send(data + Const.END_MESSAGE);
            message = myClient.receive();
            if (!message.contains(FAILURE)) {
                if (!parseMessage(message)) {
                    mUnswer = "Ошибка при обработке входящего сообщения";
                    return false;
                }
            } else {
                int index = message.indexOf(FAILURE);
                if (index > 0) {
                    mUnswer = message.substring(0, index);
                }
                return false;
            }
            myClient.disconnect();
        }
        return true;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        // this.progress.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dismiss();

        if (result) {
            mUnswer = "Остатки товаров успешно обновлены";
        }

        if (mUpdateCompleteListener != null) {
            mUpdateCompleteListener.onTaskComplete(this);
        }
    }

    private boolean parseMessage(String data) {
        String[] goodsList = data.split(GOODS_DIVIDER);

        StringBuilder sb = new StringBuilder();

        sb.append(String.format("UPDATE %s SET %s = 0" + Const.NewLine,
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_REST.Name));

        for (String item : goodsList) {
            StringTokenizer st = new StringTokenizer(item, GOODS_SUB_DIVIDER);

            if (st.countTokens() < 2)
                continue;

            String codeGoods = st.nextToken();
            String count = st.nextToken().replace(',', '.');

            sb.append(String.format("UPDATE %s " + "SET %s = %s "
                            + "WHERE %s = '%s'" + Const.NewLine,
                    GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_REST.Name,
                    count, GoodsMetaData.FIELD_CODE.Name, codeGoods));
        }

        String[] querys = sb.toString().split(Const.NewLine);

        Log.i(TAG, String.format("Updating DB, %d rows", querys.length));

        SQLiteDatabase db;
        try {
            MyApp app = (MyApp) baseContext.getApplicationContext();
            db = app.getDB();

            DBHelper.execMultipleSQL(db, querys);
        } catch (Exception e) {
            Log.i(TAG, e.getMessage());
        }

        return true;
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.hide();
            mProgress.dismiss();
        }
    }
}
