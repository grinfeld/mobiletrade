package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.adapters.TicketClientListAdapter;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.TicketClient;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IGetTicketClientTask;
import com.madlab.mtrade.grinfeld.roman.tasks.GetTicketClientTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public class TicketClientFragment extends Fragment implements View.OnClickListener, IGetTicketClientTask{


    private static final String TAG = "#TicketClientFragment";
    private Button btShipDate;
    private Date requestDate = Calendar.getInstance().getTime();
    private Client client;
    private ListView listView;
    private ProgressDialog progress;
    private TextView txt_all_amount;
    private Handler mHandler;

    private DatePickerDialog.OnDateSetListener dcListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, monthOfYear);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            requestDate = cal.getTime();
            setDate(requestDate);
            //обновление данных при выборе даты
            SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy.MM.dd");
            String strDt = simpleDate.format(requestDate);
            prepareTicketClientTask(strDt);
        }
    };

    public static TicketClientFragment newInstance(Client curClient) {
        TicketClientFragment fragment = new TicketClientFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Client.KEY, curClient);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ticket_client, container, false);
        setHasOptionsMenu(true);
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        client = getArguments().getParcelable(Client.KEY);
        btShipDate = rootView.findViewById(R.id.btShipDate);
        listView = rootView.findViewById(android.R.id.list);
        txt_all_amount = rootView.findViewById(R.id.txt_all_amount);
        TextView txt_client = rootView.findViewById(R.id.txt_client);
        txt_client.setText(client.mName+"\n"+client.mPostAddress);
        btShipDate.setOnClickListener(this);
        setDate(requestDate);
        progress = new ProgressDialog(getActivity());
        progress.setMessage(getActivity().getString(R.string.mes_wait_for_load));
        progress.setCancelable(true);
        mHandler = new Handler(Looper.getMainLooper());
        SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy.MM.dd");
        String strDt = simpleDate.format(requestDate);
        prepareTicketClientTask(strDt);
        return rootView;
    }

    private void prepareTicketClientTask(String strDt){
        progress.show();
        if (App.get(getActivity()).connectionServer.equals("1")) {
            startTicketClientTask(strDt, null);
        }else {
            String region = App.getRegion(getActivity());
            OkHttpClientBuilder.buildCall("ПолучитьЗаявкиПоКлиенту",
                    String.valueOf(client.getCode()+";"+strDt+";"+ App.get(getActivity()).getCodeAgent()),region).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    mHandler.post(() ->
                            startTicketClientTask(strDt, null));
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    try {
                        String strResponse = response.body().string();
                        JSONObject jsonObject = new JSONObject(strResponse);
                        final String data = jsonObject.getString("data");
                        mHandler.post(() -> startTicketClientTask(strDt, new String[]{data}));
                    } catch (JSONException e) {
                        startTicketClientTask(strDt, null);
                        e.printStackTrace();
                    }

                }
            });

        }
    }

    private void startTicketClientTask(String strDt, String[] args){
        GetTicketClientTask task = new GetTicketClientTask(getActivity(),client.getCode(),strDt, this);
        task.execute(args);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setDate(Date current) {
        btShipDate.setText("Дата: "+ TimeFormatter.sdf1C.format(current));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btShipDate:
                Calendar cal = Calendar.getInstance();
                cal.setTime(requestDate);
                if (requestDate != null) {
                    DatePickerDialog dlg = new DatePickerDialog(getActivity(), dcListener,
                            cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                            cal.get(Calendar.DAY_OF_MONTH));
                    dlg.show();
                }
                break;
        }
    }



    @Override
    public void onGetTicketClientTaskComplete(GetTicketClientTask task) {
        ArrayList<TicketClient> ticketClients;
        try {
            ticketClients = task.get();
        } catch (Exception e){
            ticketClients = null;
        }
        if (ticketClients != null){
            double allAmount = 0.0;
            for (TicketClient ticketClient : ticketClients){
                try {
                    String amountStr = ticketClient.getAmount().replaceAll("\\s","");
                    allAmount += Double.parseDouble(amountStr.replace(",","."));
                }catch (Exception e){
                    allAmount = 0.0;
                }
            }
            txt_all_amount.setText(String.format("%.2f", allAmount)+" Р");
            listView.setAdapter(new TicketClientListAdapter(getActivity(),ticketClients));
        }
        progress.dismiss();
    }
}
