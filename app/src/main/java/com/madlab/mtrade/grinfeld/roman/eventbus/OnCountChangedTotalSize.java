package com.madlab.mtrade.grinfeld.roman.eventbus;

/**
 * Created by GrinfeldRA
 */
public class OnCountChangedTotalSize {

    private int size;

    public OnCountChangedTotalSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
