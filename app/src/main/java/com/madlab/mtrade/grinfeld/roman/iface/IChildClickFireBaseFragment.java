package com.madlab.mtrade.grinfeld.roman.iface;

import android.widget.ExpandableListView;

import com.madlab.mtrade.grinfeld.roman.adapters.FireBaseMessageExpAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.FireBaseNotification;

/**
 * Created by GrinfeldRA on 26.02.2018.
 */

public interface IChildClickFireBaseFragment {

    public void onClick(FireBaseNotification fireBaseNotification, String token, FireBaseMessageExpAdapter adapter);

}
