package com.madlab.mtrade.grinfeld.roman.iface;

import android.location.Location;

import com.madlab.mtrade.grinfeld.roman.tasks.FetchLocationTask;

/**
 * Created by GrinfeldRA on 22.12.2017.
 */

public interface IFetchLocationTask {

    void onLocation(FetchLocationTask task);

}
