package com.madlab.mtrade.grinfeld.roman.connectivity;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsBean;
import com.madlab.mtrade.grinfeld.roman.entity.promotions.PromotionsItemBean;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by GrinfeldRA
 */

public interface ApiPromotions {

    @GET("promotions/{id}")
    Observable<PromotionsItemBean> getPromotionItem(@Path("id") int id, @QueryMap Map<String, String> params);

    @GET("promotions")
    Observable<PromotionsBean> getPromotions(@QueryMap Map<String, String> params);

}
