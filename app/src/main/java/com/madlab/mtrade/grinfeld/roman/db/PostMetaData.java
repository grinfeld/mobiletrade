package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by grinfeldra
 */
public class PostMetaData {

    public static final String TABLE_NAME = "Post";

    public static final DBField FIELD_ID = new DBField("fieldId", 0);
    public static final DBField FIELD_VALUE = new DBField("fieldValue", 1);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	integer	    NOT NULL DEFAULT 0,"
                    + "%s	nvarchar(20)	    NOT NULL DEFAULT '')", TABLE_NAME, FIELD_ID, FIELD_VALUE);

    public static String insertQuery(int id, String value) {
        return String.format(Locale.ENGLISH,
                "INSERT INTO %s " + "(%s, %s) VALUES (%d, '%s')", TABLE_NAME, FIELD_ID, FIELD_VALUE, id, value);
    }


}
