package com.madlab.mtrade.grinfeld.roman.help;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.fragments.ClientViewPagerContainer;
import com.madlab.mtrade.grinfeld.roman.fragments.PDFViewerFragment;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.ArrayList;

/**
 * Created by grinfeldra
 */
public class HelpFragment extends Fragment {

    public static final int IDD_NEW_CLAIM = 20;
    private HelpAndSupportAdapter adapter;
    private Context context;

    public enum Actions {
        CALL, INSTRUCTIONS, CLAIM, WHATSAPP, VIBER, TELEGRAM, TELEGRAM_CHANNEL, STANDART_JOB
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApp myApp = (MyApp) getActivity().getApplication();
        Tracker defaultTracker = myApp.getDefaultTracker();
        defaultTracker.setScreenName(this.getClass().getSimpleName());
        defaultTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        return inflater.inflate(R.layout.help_and_support, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView1 = view.findViewById(R.id.recycler);
        adapter = new HelpAndSupportAdapter();
        recyclerView1.setLayoutManager(new LinearLayoutManager(recyclerView1.getContext()));
        recyclerView1.addItemDecoration(new DividerItemDecoration(recyclerView1.getContext(), DividerItemDecoration.VERTICAL));
        adapter.setData(items);
        recyclerView1.setAdapter(adapter);
        ItemClickSupport.addTo(recyclerView1).setOnItemClickListener((recyclerView, position, v) -> {
            if (adapter.getItemByPosition(position) instanceof HelpAndSupportItem) {
                itemClick(((HelpAndSupportItem) adapter.getItemByPosition(position)).getActions());
            }
        });
    }

    private final ArrayList<HelpAndSupportTypes> items = new ArrayList<HelpAndSupportTypes>() {
        {
            add(new HelpAndSupportItem(R.drawable.ic_call_black_24dp, "Позвонить в службу поддержки", Actions.CALL));
            add(new HelpAndSupportHeader("Стандарты работы"));
            add(new HelpAndSupportItem(R.mipmap.help, "Инструкция пользователя", Actions.INSTRUCTIONS));
            add(new HelpAndSupportItem(R.mipmap.help, "Стандарты работы с сетями", Actions.STANDART_JOB));
            add(new HelpAndSupportItem(R.drawable.ic_claim, "Претензия", Actions.CLAIM));
            add(new HelpAndSupportHeader("Задать вопрос"));
            add(new HelpAndSupportItem(R.drawable.ic_whatsapp2, "Написать в WhatsApp", Actions.WHATSAPP));
            add(new HelpAndSupportItem(R.drawable.ic_viber2, "Написать в Viber", Actions.VIBER));
            add(new HelpAndSupportItem(R.drawable.ic_telegram, "Написать в Telegram", Actions.TELEGRAM));
            add(new HelpAndSupportHeader("Новости"));
            add(new HelpAndSupportItem(R.drawable.ic_new_releases_black_24dp, "Подписаться на канал в Telegram", Actions.TELEGRAM_CHANNEL));
        }
    };

    public void itemClick(Actions actions) {
        switch (actions) {
            case CALL:
                call();
                break;
            case WHATSAPP:
                openWhatsApp();
                break;
            case VIBER:
                openViber();
                break;
            case TELEGRAM:
                openTelegram();
                break;
            case INSTRUCTIONS:
                openInstructions();
                break;
            case CLAIM:
                openClaimScreen();
                break;
            case TELEGRAM_CHANNEL:
                telegramSubscribe();
                break;
            case STANDART_JOB:
                openStandartJob();
                break;
        }
    }


    private void openStandartJob() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(R.id.contentMain, PDFViewerFragment.newInstance(PDFViewerFragment.NETWORKING_STANDARD))
                .addToBackStack(null)
                .commit();
    }


    public void call() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + getString(R.string.phone_tech_support)));
        Permissions.check(context, Manifest.permission.CALL_PHONE, null, new PermissionHandler() {
            @Override
            public void onGranted() {
                startActivity(intent);
            }
        });
    }


    public void openInstructions() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction()
                .replace(R.id.contentMain, PDFViewerFragment.newInstance(PDFViewerFragment.FAQ))
                .addToBackStack(null)
                .commit();
    }


    public void openClaimScreen() {

        ClientViewPagerContainer fragment = new ClientViewPagerContainer();
        FragmentTransaction fragmentTransaction;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            fragment.setTargetFragment(getParentFragment(), IDD_NEW_CLAIM);
            fragmentTransaction = getActivity().getFragmentManager().beginTransaction();
        } else {
            fragment.setTargetFragment(this, IDD_NEW_CLAIM);
            fragmentTransaction = getFragmentManager().beginTransaction();
        }
        fragmentTransaction.addToBackStack(null).replace(R.id.contentMain, fragment).commit();
    }


    public void openWhatsApp() {
        String fio = GlobalProc.getPref(MyApp.getContext(), R.string.pref_nameManager);
        String region = App.getRegion(MyApp.getContext());
        String description = "";
        if (fio != null && !fio.isEmpty() && region != null && !region.isEmpty()) {
            description = String.format("Здравствуйте! Вас беспокоит " + fio + " с региона " + region);
        }
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        String whatsapprequest = String.format(getString(R.string.whatsapp_api), getString(R.string.phone_tech_support), description);
        sendIntent.setData(Uri.parse(whatsapprequest));
        startActivity(sendIntent);
    }


    public void openViber() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            PackageInfo info = pm.getPackageInfo("com.viber.voip", PackageManager.GET_META_DATA);
            Uri uri = Uri.parse("tel:" + Uri.encode("+79879118324"));
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setClassName("com.viber.voip", "com.viber.voip.WelcomeActivity");
            intent.setData(uri);
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getActivity(), "Viber не установлен", Toast.LENGTH_SHORT).show();
        }
    }


    public void openTelegram() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            PackageInfo info = pm.getPackageInfo("org.telegram.messenger", PackageManager.GET_META_DATA);
            Uri uri = Uri.parse(String.format("tg://resolve?domain=%s", "DalimoMobile"));
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(uri);
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(getActivity(), "Telegram не установлен", Toast.LENGTH_SHORT).show();
        }
    }


    public void telegramSubscribe() {
        Uri uri = Uri.parse("https://t.me/joinchat/AAAAAFWGjzLqLGYIV5KiPg");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


}
