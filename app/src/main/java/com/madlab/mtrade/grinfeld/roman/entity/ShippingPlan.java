package com.madlab.mtrade.grinfeld.roman.entity;


import java.util.ArrayList;
import java.util.StringTokenizer;
import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;

public class ShippingPlan implements Parcelable {
    public static String ROUTE_COMMON = "Общий прайс";
    public static String ROUTE_PERISHABLE = "Скоропорт";
    public static String ROUTE_FREEZE = "Заморозка";
    public static String ROUTE_MARS = "Бакалея";

    /**
     * Разделитель элементов в передаваемой строке
     */
    public static String sDelimitterItem = "|";

    /**
     * Разделитель полей в передаваемой строке
     */
    public static String sDelimitterSubItem = "*";

    public byte mCode;
    public String mPlan;

    public static ArrayList<ShippingPlan> Parse(String value) {
        ArrayList<ShippingPlan> result = null;

        StringTokenizer st = new StringTokenizer(value, sDelimitterItem);
        if (st.countTokens() > 0) {
            result = new ArrayList<ShippingPlan>();
        }
        // Перебираем элементы
        while (st.hasMoreElements()) {
            String item = st.nextToken();
            if (item != null && item.length() > 0) {

                StringTokenizer st1 = new StringTokenizer(item,
                        sDelimitterSubItem);
                byte key = 0;
                byte routeCode = 0;
                // Перебираем поля
                while (st1.hasMoreElements()) {
                    String _field = st1.nextToken();
                    // Если элемент пустой - пропускаем
                    if (_field.length() < 1)
                        continue;

                    switch (key) {
                        case 0:// Название маршрута
                            routeCode = (byte) GlobalProc.parseInt(_field);
                            break;
                        case 1:// Признак видимости, если не равен 1 - выходим из
                            // цикла
                            if (!_field.equalsIgnoreCase("1")) {
                                break;
                            }
                            break;
                        case 2:// План развоза
                            ShippingPlan newItem = new ShippingPlan();
                            newItem.mCode = routeCode;
                            newItem.mPlan = _field;
                            result.add(newItem);
                            break;
                    }
                    key++;
                }
            }
        }
        return result;
    }

    public ShippingPlan() {
        mCode = 0;
        mPlan = "";
    }

    public ShippingPlan(Parcel parcel) {
        mCode = parcel.readByte();
        mPlan = parcel.readString();
    }

    public static final Parcelable.Creator<ShippingPlan> CREATOR = new Parcelable.Creator<ShippingPlan>() {

        public ShippingPlan createFromParcel(Parcel in) {
            return new ShippingPlan(in);
        }

        public ShippingPlan[] newArray(int size) {
            return new ShippingPlan[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(mCode);
        dest.writeString(mPlan);
    }
}
