package com.madlab.mtrade.grinfeld.roman.iface;


import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveTask;

/**
 * Created by GrinfeldRa
 */
public interface ISetReserveTaskComplete {

    void onTaskComplete(SetReserveTask task);

}
