package com.madlab.mtrade.grinfeld.roman.connectivity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;

public class DalimoClient {
    private final String TAG = "!->DalimoClient";

    private static final char CR = 0x0D;
    private static final char LR = 0x0A;
    private static final String CRLR = new String(new char[]{CR, LR});

    public static final String ERROR = "<ERR>";

    private short timeout = 30;

    private Socket _socket = null;

    private OutputStream _out = null;
    private InputStream _in = null;
    private BufferedReader in = null;

    private Credentials info = null;

    private String mLastError;

    public String LastError() {
        return mLastError;
    }

    public DalimoClient(Credentials credentials) {
        info = credentials;
    }

    private boolean createSocket(InetAddress inetAddress, int port) {
        try {
            _socket = new Socket();

            InetSocketAddress sockaddr = new InetSocketAddress(inetAddress,
                    port);
            timeout = App.get(MyApp.getContext()).getServerTimeout();
            _socket.connect(sockaddr, timeout * 1000);
        } catch (Exception e) {
            mLastError = e.toString();
            mLastError += ERROR;

            Log.e(TAG, mLastError);
            return false;
        }
        return true;
    }

    public boolean connect() {
        if (info == null)
            return false;

        try {
            if (info.getServer1() != null && info.getServer1().length() < 1) {
                return false;
            }
            InetAddress mServer = null;
            try {
                mServer = InetAddress.getByName(info.getServer1());
            } catch (UnknownHostException e) {

            }
            Log.i(TAG, String.format("Try to connect to %s:%d",
                    info.getServer1(), info.getPort()));

            if (!createSocket(mServer, info.getPort())) {
                String s = info.getServer2();
                if (s != null && s.length() > 0) {
                    mServer = InetAddress.getByName(info.getServer2());
                    Log.i(TAG, String.format("Try to connect to %s:%d", info.getServer2(), info.getPort()));
                    createSocket(mServer, info.getPort());
                }
            }

            if (_socket.isConnected()) {
                _in = _socket.getInputStream();
                in = new BufferedReader(new InputStreamReader(_socket.getInputStream(), Const.WIN1251));
                _out = _socket.getOutputStream();
            } else {
                mLastError = "Could not connect to server" + ERROR;
                Log.e(TAG, mLastError);
                return false;
            }
        } catch (Exception e) {
            mLastError = e.toString();
            mLastError += ERROR;
            Log.e(TAG, mLastError);
            disconnect();
            return false;
        }

        Log.i(TAG, "Connection sucessfull");
        return true;
    }

    public void disconnect() {
        try {
            if (_in != null)
                _in.close();
            if (in != null)
                in.close();
            if (_out != null)
                _out.close();
            if (_socket != null)
                _socket.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        Log.i(TAG, "Disconnect");
    }

    public void send(String message) {
        send(message, Const.WIN1251);
    }

    public void send(String message, String codePageName) {
        try {
            _out.write(message.getBytes(codePageName));
        } catch (Exception e) {
            mLastError = e.toString();
            mLastError += ERROR;
            Log.e(TAG, mLastError);
            disconnect();
        }
    }

    public void sendLine(String message) {
        send(message + CRLR, Const.WIN1251);
    }

    public String receive() {
        StringBuilder res = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(_in));
            String line;
            while ((line = br.readLine()) != null) {
                int pos = line.indexOf(Const.END_MESSAGE);
                if (pos != -1) {
                    line = line.substring(0, pos);
                }
                res.append(line);
            }
        } catch (Exception e) {
            mLastError = e.toString() + ERROR;
            Log.e(TAG, mLastError);
            return mLastError;
        }

        return res.toString();
    }

    /**
     * Получаем входящий файл
     *
     * @param fileName - полное имя файла для сохранения
     * @return True, если всё хорошо, False - иначе
     */

    public boolean receiveFile(String fileName) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = _socket.getInputStream();
            out = new FileOutputStream(fileName);
            byte[] buf = new byte[8192]; //buffer
            int len;
            String message = new String(buf, Const.UTF8);
            while ((len = in.read(buf)) != -1) {
                if (message.contains(ERROR)) {
                    int pos = message.indexOf(ERROR);
                    if (pos > 1)
                        mLastError = message.substring(0, pos - 1);
                    return false;
                }
                out.write(buf, 0, len); //write buffer
            }
            return true;
        } catch (Exception e) {
            mLastError = e.toString();
        } finally {
            try {
                out.close();
                in.close();
            } catch (Exception e) {

            }
        }
        return false;
    }

//    public boolean receiveFile(String destinationFilePath) {
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(destinationFilePath, false);
//            byte[] buf = new byte[1000];
//            int count = _in.read(buf, 0, 1000);
//            String message = new String(buf, Const.UTF8);
//            if (message.contains(ERROR)) {
//                int pos = message.indexOf(ERROR);
//                if (pos > 1)
//                    mLastError = message.substring(0, pos - 1);
//                return false;
//            } else {
//                fos.write(buf, 0, count);
//            }
//
//            while ((count = _in.read(buf)) > 0) {
//                fos.write(buf, 0, count);
//            }
//
//            return true;
//        } catch (Exception e) {
//            mLastError = e.toString();
//            Log.e(TAG, mLastError);
//        } finally {
//            try {
//                fos.close();
//            } catch (Exception e2) {
//
//            }
//        }
//        return false;
//    }

    /**
     * Отправляем файл
     *
     * @param sourceFilePath - полное имя файла для отправки
     * @return True, если всё хорошо, False - иначе
     */
    public boolean sendFile(String sourceFilePath) {

        File file = new File(sourceFilePath);
        if (!file.exists())
            return false;

        boolean wasError = false;

        FileInputStream fis = null;
        try {
            byte[] buf = new byte[(int) file.length()];

            fis = new FileInputStream(file);

            BufferedInputStream bis = new BufferedInputStream(fis);
            bis.read(buf);
            _out.write(buf);
            bis.close();
        } catch (Exception e) {
            mLastError = e.toString();
            Log.e(TAG, mLastError);
            wasError = true;
        } finally {
            try {
                fis.close();
            } catch (Exception e1) {

            }
//			try {
//				bis.close();
//			} catch (Exception e2) {
//
//			}
        }
        return !wasError;
    }

    public void flushOutput() {
        if (_out != null) {
            try {
                _out.flush();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public InputStream getInputStream() {
        return _in;
    }

    private static final int DEFAULT_CONNECTION_TIMEOUT = 20000;

    private static int connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;


    /* package */
    static <Response extends DalimoResponse> Response execute(DalimoRequest<Response> request)
            throws DalimoClientException {
        Socket socket = new Socket();
        try {
            socket.connect(request.getAddress(), connectionTimeout);
            request.onRequest(socket.getOutputStream());

            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // do nothing
            }

            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            try {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    int endLine = line.indexOf("<EOF>");
                    if (endLine != -1) {
                        line = line.substring(0, endLine);
                    }
                    builder.append(line);
                }
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    // do nothing
                }
            }

            return request.onReceive(builder.toString());
        } catch (Exception e) {
            throw new DalimoClientException(e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                // do nothing
            }
        }
    }
}
