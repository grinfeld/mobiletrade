package com.madlab.mtrade.grinfeld.roman.fragments.contacts;


import com.madlab.mtrade.grinfeld.roman.entity.Contact;

/**
 * Created by grinfeldra
 */
public interface IOnClickItem {

    void onClickDel(Contact client);
    void onClick(Contact client);

}
