package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MatrixMetaData;

public class GoodsMatrix {
    private static String TAG = "!->GoodsMatrix";

    // Первый уровень иерархии
    private static List<Map<String, Node>> mNodes = null;

    public static List<Map<String, Node>> getNodes() {
        return mNodes;
    }

    // Второй уровень иерархии
    private static List<List<Map<String, Node>>> mSubNodes = null;

    public static List<List<Map<String, Node>>> getSubNodes() {
        return mSubNodes;
    }

    public static void loadGroup(SQLiteDatabase db, String rootCode, String clientCode)
    {
        mNodes = new ArrayList<Map<String, Node>>();
        mSubNodes = new ArrayList<List<Map<String, Node>>>();

        Cursor result = null;
        try{
            //Выбираем матрицу
//			String sql = String.format("SELECT * FROM %s WHERE %s = ?",
//					//FROM
//					MatrixMetaData.TABLE_NAME,
//					//WHERE
//					MatrixMetaData.FIELD_CODE_CLIENT);
//
//			result = db.rawQuery(sql, new String[] {clientCode});
//			//Список товаров в Матрице
//			ArrayList<String> goodsList = new ArrayList<String>();
//			int codeGoods = result.getColumnIndex(MatrixMetaData.FIELD_CODE_GOODS);
//			while (result.moveToNext())
//			{
//				goodsList.add(result.getString(codeGoods));
//			}
//			result.close();

            //Выбираем коды родителей (группы), второй уровень
            String sql = String.format("SELECT DISTINCT %s FROM %s INNER JOIN %s ON %s.%s=%s.%s WHERE %s = ?",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //INNER JOIN
                    MatrixMetaData.TABLE_NAME,
                    //ON
                    GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
                    MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
                    //WHERE
                    MatrixMetaData.FIELD_CODE_CLIENT);

            result = db.rawQuery(sql, new String[] {clientCode});
            ArrayList<String> groupsList = new ArrayList<String>();
            while (result.moveToNext())
            {
                groupsList.add(result.getString(0));
            }
            result.close();

            //формируем секцию WHERE
            String where = "";
            for (int i = 0; i < groupsList.size()-1; i++){
                where += String.format("%s = '%s' OR ", GoodsMetaData.FIELD_CODE.Name, groupsList.get(i));
            }
            where += String.format("%s = '%s'", GoodsMetaData.FIELD_CODE.Name, groupsList.get(groupsList.size()-1));

            sql = String.format("SELECT %s, %s, %s FROM %s WHERE %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_NAME.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where);

            result = db.rawQuery(sql, null);
            ArrayList<Node> secondLevel = new ArrayList<Node>();
            while (result.moveToNext()){
                Node node = new Node(result.getString(0), result.getString(1), result.getString(2));
                secondLevel.add(node);
            }

            sql = String.format("SELECT DISTINCT %s FROM %s WHERE %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where);

            result = db.rawQuery(sql, null);

            groupsList = new ArrayList<String>();
            while (result.moveToNext())
            {
                groupsList.add(result.getString(0));
            }
            result.close();

            //формируем секцию WHERE
            where = "";
            for (int i = 0; i < groupsList.size()-1; i++){
                where += String.format("%s = '%s' OR ", GoodsMetaData.FIELD_CODE.Name, groupsList.get(i));
            }
            where += String.format("%s = '%s'", GoodsMetaData.FIELD_CODE.Name, groupsList.get(groupsList.size()-1));

            sql = String.format("SELECT %s, %s, %s FROM %s WHERE %s ORDER BY %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_NAME.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where,
                    //ORDER BY
                    GoodsMetaData.FIELD_SORT_ORDER.Name);

            result = db.rawQuery(sql, null);

            //Загружаем информацию об узлах первого уровня
            ArrayList<Node> firstLevel = new ArrayList<Node>();
            while (result.moveToNext()){
                Node item = new Node(result.getString(0), result.getString(1), result.getString(2));
                firstLevel.add(item);
            }
            result.close();

//			ArrayList<Node> controlList = (ArrayList<Node>)secondLevel.clone();

            //карта элементов первого уровня
            for (Node node : firstLevel) {
                //карта элементов второго уровня
                ArrayList<Map<String, Node>> subList = new ArrayList<Map<String, Node>>();
                for (Node subNode : secondLevel) {
                    if (subNode.getParentCode().equalsIgnoreCase(node.getCode())){

//						controlList.remove(subNode);

                        Cursor subRes = db.rawQuery(GoodsMetaData.QuerySubItemsCountMatrix, new String[] {subNode.getCode(), clientCode});
                        if (subRes.moveToFirst()){
                            subNode.childCount(subRes.getInt(0));
                        }
                        subRes.close();

                        Map<String, Node> sn = new HashMap<String, Node>();
                        sn.put(Node.SUBNODE_KEY, subNode);
                        subList.add(sn);
                    }
                }
                if (!subList.isEmpty()){
                    mSubNodes.add(subList);
                    node.childCount(subList.size());
                }

                Map<String, Node> m = new HashMap<String, Node>();
                m.put(Node.NODE_KEY, node);
                mNodes.add(m);
            }
//			if (controlList.size() > 0){
//				Log.e(TAG, "Не все группы были обработаны");
//			}
        }
        catch(Exception e)
        {
            Log.e(TAG, e.toString());
        }
        finally{
            if (result != null){
                result.close();
            }
        }
    }

    public static void loadGroupSTM(SQLiteDatabase db, String rootCode, String clientCode)
    {
        mNodes = new ArrayList<Map<String, Node>>();
        mSubNodes = new ArrayList<List<Map<String, Node>>>();

        Cursor result = null;
        try{
            //Выбираем коды родителей (группы), второй уровень
            String sql = String.format("SELECT DISTINCT %s FROM %s WHERE %s = 1",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,

                    GoodsMetaData.FIELD_STM.Name);

            result = db.rawQuery(sql, null);
            ArrayList<String> groupsList = new ArrayList<String>();
            while (result.moveToNext())
            {
                groupsList.add(result.getString(0));
            }
            result.close();

            //формируем секцию WHERE
            String where = "";
            for (int i = 0; i < groupsList.size()-1; i++){
                where += String.format("%s = '%s' OR ", GoodsMetaData.FIELD_CODE.Name, groupsList.get(i));
            }
            where += String.format("%s = '%s'", GoodsMetaData.FIELD_CODE.Name, groupsList.get(groupsList.size()-1));

            sql = String.format("SELECT %s, %s, %s FROM %s WHERE %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_NAME.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where);

            result = db.rawQuery(sql, null);
            ArrayList<Node> secondLevel = new ArrayList<Node>();
            while (result.moveToNext()){
                Node node = new Node(result.getString(0), result.getString(1), result.getString(2));
                secondLevel.add(node);
            }

            sql = String.format("SELECT DISTINCT %s FROM %s WHERE %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where);

            result = db.rawQuery(sql, null);

            groupsList = new ArrayList<String>();
            while (result.moveToNext())
            {
                groupsList.add(result.getString(0));
            }
            result.close();

            //формируем секцию WHERE
            where = "";
            for (int i = 0; i < groupsList.size()-1; i++){
                where += String.format("%s = '%s' OR ", GoodsMetaData.FIELD_CODE.Name, groupsList.get(i));
            }
            where += String.format("%s = '%s'", GoodsMetaData.FIELD_CODE.Name, groupsList.get(groupsList.size()-1));

            sql = String.format("SELECT %s, %s, %s FROM %s WHERE %s ORDER BY %s",
                    //SELECT
                    GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_NAME.Name,
                    //FROM
                    GoodsMetaData.TABLE_NAME,
                    //WHERE
                    where,
                    //ORDER BY
                    GoodsMetaData.FIELD_SORT_ORDER.Name);

            result = db.rawQuery(sql, null);

            //Загружаем информацию об узлах первого уровня
            ArrayList<Node> firstLevel = new ArrayList<Node>();
            while (result.moveToNext()){
                Node item = new Node(result.getString(0), result.getString(1), result.getString(2));
                firstLevel.add(item);
            }
            result.close();

//			ArrayList<Node> controlList = (ArrayList<Node>)secondLevel.clone();

            //карта элементов первого уровня
            for (Node node : firstLevel) {
                //карта элементов второго уровня
                ArrayList<Map<String, Node>> subList = new ArrayList<Map<String, Node>>();
                for (Node subNode : secondLevel) {
                    if (subNode.getParentCode().equalsIgnoreCase(node.getCode())){

//						controlList.remove(subNode);

                        Cursor subRes = db.rawQuery(GoodsMetaData.QuerySubItemsCount, new String[] {subNode.getCode()});
                        if (subRes.moveToFirst()){
                            subNode.childCount(subRes.getInt(0));
                        }
                        subRes.close();

                        Map<String, Node> sn = new HashMap<String, Node>();
                        sn.put(Node.SUBNODE_KEY, subNode);
                        subList.add(sn);
                    }
                }
                if (!subList.isEmpty()){
                    mSubNodes.add(subList);
                    node.childCount(subList.size());
                }

                Map<String, Node> m = new HashMap<String, Node>();
                m.put(Node.NODE_KEY, node);
                mNodes.add(m);
            }
//			if (controlList.size() > 0){
//				Log.e(TAG, "Не все группы были обработаны");
//			}
        }
        catch(Exception e)
        {
            Log.e(TAG, e.toString());
        }
        finally{
            if (result != null){
                result.close();
            }
        }
    }
}
