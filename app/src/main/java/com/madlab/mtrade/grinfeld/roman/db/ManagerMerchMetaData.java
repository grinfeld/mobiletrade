package com.madlab.mtrade.grinfeld.roman.db;

import android.content.ContentValues;
import android.util.Log;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */
public class ManagerMerchMetaData {

    private static final String TAG = "--->ManagerMetaData";

    public static final String TABLE_NAME = "managers_table";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CODE_AGENT = "code_agent";
    public static final String COLUMN_CODE_CLIENT = "code_client";
    public static final String COLUMN_CODE_MANAGER = "code_manager";
    public static final String COLUMN_FIO = "fio";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND =
            "create table " + TABLE_NAME + "(" +
                    COLUMN_ID + " integer primary KEY_TYPE autoincrement, " +
                    COLUMN_CODE_AGENT + " text, " +
                    COLUMN_CODE_CLIENT + " text, " +
                    COLUMN_CODE_MANAGER + " text, " +
                    COLUMN_FIO + " text" +
                    ");";

    public static String insertQuery(String code_agent, String code_client, String code_manager, String fio) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s) "
                        + "VALUES "
                        + "('%s', '%s', '%s', '%s')\n",
                TABLE_NAME,
                COLUMN_CODE_AGENT, COLUMN_CODE_CLIENT, COLUMN_CODE_MANAGER, COLUMN_FIO,
                code_agent, code_client, code_manager, fio);
    }

}
