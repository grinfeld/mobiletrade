package com.madlab.mtrade.grinfeld.roman.components;

import android.content.Context;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.wheel.adapters.ArrayWheelAdapter;

/**
 * Класс, описывающий WheelView с единицами измерения
 *
 */
public class WheelMeash extends WheelView {
	private String[] listMeash = new String[] { "шт.", "уп." };
	private ArrayWheelAdapter<String> adapter;

	// private LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
	// LayoutParams.WRAP_CONTENT);

	public WheelMeash(Context context) {
		super(context);
		initWheel(context);
	}

	/**
	 * Здесь мы будем инициализировать наш элемент управления
	 */
	public void initWheel(Context context) {
		// setLayoutParams(params);

		adapter = new ArrayWheelAdapter<String>(context, listMeash);
		adapter.setTextSize(16);
		setVisibleItems(App.get(getContext()).getVisibleWheelItems());
	}

	/**
	 * Сеттер для списка единиц измерения
	 */
	public void setListMeash(String[] newListMeashValue) {
		listMeash = newListMeashValue;
	}
}
