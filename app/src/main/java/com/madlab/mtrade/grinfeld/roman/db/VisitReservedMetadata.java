package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by grinfeldra
 */
public class VisitReservedMetadata {


    public static final String TABLE_NAME = "visitReserved";

    public static final String FIELD_ID = "id";
    public static final String FIELD_UUID = "uuid";
    public static final String FIELD_IS_RESERVED= "is_reserved";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;


    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "%s	TEXT	NOT NULL DEFAULT '' unique,"
                    + "%s	INTEGER	NOT NULL DEFAULT 0)",
            TABLE_NAME, FIELD_ID, FIELD_UUID, FIELD_IS_RESERVED);


    public static String insertQuery(String uuid) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s) "
                        + "VALUES "
                        + "('%s')",
                TABLE_NAME,  FIELD_UUID,  uuid);
    }


}
