package com.madlab.mtrade.grinfeld.roman.db;

public class LastSalesMetaData {
	/**
	 * ��� �������
	 */
	public static final String TABLE_NAME = "LastSale";

	/**
	 * �������� ���� � �� � ����� ������
	 */
	public static final String FIELD_CLIENT = "CodeCli";

	/**
	 * �������� ���� � �� � ����� ��������
	 */
	public static final String FIELD_GOODS = "CodeGoods";

	/**
	 * �������� ���� � �� � ��������� �����������
	 */
	public static final String FIELD_INFO = "Info";

	/**
	 * �������� ���� � �� � ��������� �����������
	 */
	public static final String FIELD_SORT_ORDER = "SortOrder";

	/**
	 * ���������� ������ ��� �������� �������
	 */
	public static final String CREATE_COMMAND = String.format(
			"CREATE TABLE %s (" + "%s		nvarchar(6)	    NOT NULL DEFAULT '',"
					+ "%s		nvarchar(6)	    NOT NULL DEFAULT '',"
					+ "%s		nvarchar(50)	NOT NULL DEFAULT '',"
					+ "%s		int             NOT NULL DEFAULT 0)", TABLE_NAME,
			FIELD_CLIENT, FIELD_GOODS, FIELD_INFO, FIELD_SORT_ORDER);

	/**
	 * ���������� ������ ��� ������� �������
	 */
	public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

	/**
	 * ��������� ����
	 */
	public static final String PRIMARY_KEY = String.format(
			"ALTER TABLE %s ADD finalRAINT PK_LastSale PRIMARY KEY ( %s, %s )",
			TABLE_NAME, FIELD_CLIENT, FIELD_GOODS);

	/**
	 * ������
	 */
	public static final String INDEX = String.format(
			"CREATE INDEX idxLastSaleSortOrder ON %s ( %s )", TABLE_NAME,
			FIELD_SORT_ORDER);

	/**
	 * ���������� ������ �� ������� �������� � ��
	 * 
	 * @param clientCode
	 *            , ��� �������
	 * @param goodsCode
	 *            , ��� ������
	 * @param salesString
	 *            , ������ � ���������� ���������
	 * @param sortOrder
	 *            , ����������
	 * @return ����� �������
	 */
	public static String insertQuery(String clientCode, String goodsCode,
			String salesString, int sortOrder) {
		return String.format("INSERT INTO %s (" + "%s, %s, %s, %s) VALUES ("
				+ "'%s', '%s', '%s', %d)", TABLE_NAME, FIELD_CLIENT,
				FIELD_GOODS, FIELD_INFO, FIELD_SORT_ORDER, clientCode,
				goodsCode, salesString, sortOrder);
	}
}
