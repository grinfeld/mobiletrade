package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;

public class Shipment implements Parcelable {
    private Date mDateShip;
    private int mCount;

    public Shipment(Date date, int count) {
        mDateShip = date;
        mCount = count;
    }

    public Shipment() {
        this(new Date(System.currentTimeMillis()), 0);
    }

    public Shipment(Parcel parcel) {
        mDateShip = new Date(parcel.readLong());
        mCount = parcel.readInt();
    }

    public Date getDateShip() {
        return mDateShip;
    }

    public int getCount() {
        return mCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mDateShip.getTime());
        dest.writeInt(mCount);
    }

    public static final Parcelable.Creator<Shipment> CREATOR = new Parcelable.Creator<Shipment>() {

        public Shipment createFromParcel(Parcel in) {
            return new Shipment(in);
        }

        public Shipment[] newArray(int size) {
            return new Shipment[size];
        }
    };
}
