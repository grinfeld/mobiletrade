package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.AskDiscountTask;

/**
 * Created by GrinfeldRA on 18.12.2017.
 */

public interface IAskDiscountComplete {

    void onTaskComplete(AskDiscountTask task);

}
