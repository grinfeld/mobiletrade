package com.madlab.mtrade.grinfeld.roman.eventbus;

/**
 * Created by GrinfeldRA
 */

public class GoodsListEventFilter {

    byte newValue;

    public GoodsListEventFilter(byte newValue) {
        this.newValue = newValue;
    }

    public byte getNewValue() {
        return newValue;
    }
}
