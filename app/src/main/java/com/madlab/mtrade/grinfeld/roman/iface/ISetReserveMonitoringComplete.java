package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.SetReserveMonitoring;

/**
 * Created by GrinfeldRa
 */
public interface ISetReserveMonitoringComplete {

    void onTaskComplete(SetReserveMonitoring task);

}
