package com.madlab.mtrade.grinfeld.roman.help;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.madlab.mtrade.grinfeld.roman.R;

import java.util.ArrayList;
import java.util.List;

public class HelpAndSupportAdapter extends RecyclerView.Adapter {
    private final ArrayList<HelpAndSupportTypes> items = new ArrayList<>();


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType== HelpAndSupportTypes.HEADER){
            return new HelpAndSupportViewHolderHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.help_and_support_header, parent, false));
        }
        else if (viewType== HelpAndSupportTypes.ITEM){
            return new HelpAndSupportViewHolderItem(LayoutInflater.from(parent.getContext()).inflate(R.layout.help_and_support_item, parent, false));
        }
        else{
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HelpAndSupportViewHolderHeader){
            ((HelpAndSupportViewHolderHeader)holder).bind((HelpAndSupportHeader)items.get(position));
        }
        if(holder instanceof HelpAndSupportViewHolderItem){
            ((HelpAndSupportViewHolderItem)holder).bind((HelpAndSupportItem)items.get(position));
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public HelpAndSupportTypes getItemByPosition(int position){
        return items.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        if(items.get(position) instanceof HelpAndSupportHeader){
            return HelpAndSupportTypes.HEADER;
        }
        else if(items.get(position) instanceof HelpAndSupportItem){
            return HelpAndSupportTypes.ITEM;
        }
        else {
            return -1;
        }
    }

    public void setData(List<HelpAndSupportTypes> items){
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    class HelpAndSupportViewHolderItem extends RecyclerView.ViewHolder{
        TextView title;
        ImageView icon;

        public HelpAndSupportViewHolderItem(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            icon = itemView.findViewById(R.id.item_icon);
        }

        void bind(HelpAndSupportItem item){
            title.setText(item.getTitle());
            icon.setImageResource(item.getDrawable());
        }
    }

    class HelpAndSupportViewHolderHeader extends RecyclerView.ViewHolder{
        TextView title;

        public HelpAndSupportViewHolderHeader(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.header_title);
        }

        public void bind(HelpAndSupportHeader item){
            title.setText(item.getTitle());
        }

    }
}
