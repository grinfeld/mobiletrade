package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.madlab.mtrade.grinfeld.roman.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA on 08.12.2017.
 */

public class StatDocFragment extends Fragment{

    private static final String TAG = "#StatDocFragment";
    public static final String POSITION = "position";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View _rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        supportActionBar.setSubtitle("");
        supportActionBar.setTitle("Статус документов");
        if (_rootView == null){
            _rootView = inflater.inflate(R.layout.fragment_stat_doc,container,false);
            viewPager = _rootView.findViewById(R.id.viewpager);
            viewPager.setOffscreenPageLimit(0);
            tabLayout = _rootView.findViewById(R.id.tabs);
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
        }
        return _rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupViewPager(ViewPager viewPager) {
        StatDocFragment.ViewPagerAdapter adapter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            adapter = new StatDocFragment.ViewPagerAdapter(getChildFragmentManager());
        } else{
            adapter = new StatDocFragment.ViewPagerAdapter(getFragmentManager());
        }
        adapter.addFragmentTitle("Доставка");
        adapter.addFragmentTitle("Заявки");
        adapter.addFragmentTitle("Возвраты");
        adapter.addFragmentTitle("Долги по накладным");
        viewPager.setAdapter(adapter);
    }


    private class ViewPagerAdapter extends FragmentStatePagerAdapter {

        private final List<String> mFragmentTitleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(POSITION, position);
            StatDocItemFragment fragment =  new StatDocItemFragment();
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        private void addFragmentTitle(String title) {
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
