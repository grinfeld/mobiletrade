package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.components.DatePickerFragment;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Firm;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.eventbus.DateChanged;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment.IDD_EDIT_RETURN;


/**
 * Created by GrinfeldRA on 21.11.2017.
 */

public class ReturnCommonFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "#ReturnCommonFragment";

    private final static byte DOC_NEW = 21;
    private final static byte DOC_EDIT = 22;
    private final static byte STEP_RETURN_GOODS = 11;
    public final static String RETURN_MODE = "return_mode";

    byte currentDocType = DOC_NEW;

    TextView tvName;
    TextView tvAddress;
    EditText tvDocNum;
    EditText etInvoiceNumber;
    RadioButton rbNal;
    RadioButton rbBezNal;
    EditText etInfo;
    CheckBox cbPickup;
    Button btDateDoc;
    Button btDateGetBack;
    RadioButton rb_act_return, rb_repayment;
    RadioButton rb_act_loss;
    Returns data;
    private Activity context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_return_common, container, false);
        setHasOptionsMenu(true);
        context = getActivity();
        tvName = rootView.findViewById(R.id.rc_lbName);
        tvAddress = rootView.findViewById(R.id.rc_lbAddress);
        tvDocNum = rootView.findViewById(R.id.rc_etDocNum);
        etInvoiceNumber = rootView.findViewById(R.id.rc_invoice);
        rbNal = rootView.findViewById(R.id.rc_radioNal);
        rbBezNal = rootView.findViewById(R.id.rc_radioBeznal);
        rb_act_loss = rootView.findViewById(R.id.rb_act_loss);
        rb_act_return = rootView.findViewById(R.id.rb_act_return);
        etInfo = rootView.findViewById(R.id.rc_etInfo);
        cbPickup = rootView.findViewById(R.id.rc_pickup);
        rb_repayment = rootView.findViewById(R.id.rb_repayment);
        btDateDoc = rootView.findViewById(R.id.rc_btDateDoc);
        btDateGetBack = rootView.findViewById(R.id.rc_btShipDate);
        ActionBar supportActionBar = ((AppCompatActivity) context).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("");
            supportActionBar.setTitle("Новый возврат");
        }
        setMode();
        initButtons(rootView);
        initLists(rootView);
        updateData(false);
        return rootView;
    }

    @Override
    public void onResume() {
        ActionBar supportActionBar = ((AppCompatActivity) context).getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setSubtitle("");
            supportActionBar.setTitle("Новый возврат");
        }
        EventBus.getDefault().register(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }


    private void setMode() {
        if (getTargetRequestCode() == IDD_EDIT_RETURN) {
            currentDocType = DOC_EDIT;
        } else {
            currentDocType = DOC_NEW;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case STEP_RETURN_GOODS:
                if (resultCode == RESULT_OK) {
                    getTargetFragment().onActivityResult(requestCode, resultCode, data);
                    getFragmentManager().popBackStack();
                }
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void updateData(boolean direction) {
        Returns returnData = Returns.getReturn();
        if (returnData == null) {
            return;
        }
        try {
            if (direction) {
                String txt = GlobalProc.normalizeString(tvDocNum.getText().toString());
                returnData.mDocNum = txt;
                txt = GlobalProc.normalizeString(etInvoiceNumber.getText().toString());
                returnData.setInvoice(txt);
                returnData.mTypeMoney = (byte) (rbNal.isChecked() ? 2 : 1);
                if (rb_act_return.isChecked()) {
                    returnData.mTypeAct = (byte) 0;
                } else if (rb_act_loss.isChecked()) {
                    returnData.mTypeAct = (byte) 1;
                } else {
                    returnData.mTypeAct = (byte) 2;
                }
                returnData.mNote = GlobalProc.normalizeString(etInfo.getText().toString());
                if (cbPickup != null) {
                    returnData.setPickup(cbPickup.isChecked());
                }
            } else {
                if (currentDocType == DOC_NEW) {
                    returnData.mDocDate.setTime(Calendar.getInstance().getTimeInMillis());
                }
                Client client = returnData.getClient();
                if (client != null) {
                    tvName.setText(client.mNameFull);
                    tvAddress.setText(client.mPostAddress);
                    String typePay = client.currentFirm().get("0" + data.firmFK());
                    if (currentDocType == DOC_NEW) {
                        if (typePay != null && !typePay.equals("")) {
                            if (typePay.equals("П")) {
                                rbBezNal.setChecked(true);
                            } else {
                                rbNal.setChecked(true);
                            }
                        } else {
                            //если клиент по умолчанию работает по безналу
                            if (Returns.getReturn().getClient().mTypeDoc == 1) {
                                rbBezNal.setChecked(true);
                            } else {
                                rbNal.setChecked(true);
                            }
                        }
                    } else {
                        if (returnData.mTypeMoney == 2) {
                            rbNal.setChecked(true);
                        } else {
                            rbBezNal.setChecked(true);
                        }
                    }
                }
                tvDocNum.setText(returnData.mDocNum);
                if (returnData.mTypeAct == 0) {
                    rb_act_return.setChecked(true);
                } else if (returnData.mTypeAct == 1) {
                    rb_act_loss.setChecked(true);
                } else {
                    rb_repayment.setChecked(true);
                }


                btDateDoc.setText(getString(R.string.lb_client_doc_num)
                        + ":" + TimeFormatter.sdfDate.format(returnData.mDocDate));
                btDateGetBack.setText(getString(R.string.lb_return_date)
                        + ":" + TimeFormatter.sdfDate.format(returnData.getDateGoodsBack()));
                etInvoiceNumber.setText(returnData.getInvoice());
                // rbNo.setEnabled(rbNal.isChecked());
                etInfo.setText(returnData.mNote);
                if (cbPickup != null) {
                    cbPickup.setChecked(returnData.getPickup());
                }
            }
        }// try
        catch (Exception e) {
            GlobalProc.mToast(context, e.getMessage());
        }
    }

    private void initLists(View rootView) {
        Spinner listFirms = rootView.findViewById(R.id.rc_listFirms);
        if (listFirms != null) {
            data = Returns.getReturn();
            // Если в списке фирм у клиента находится более двух возможных
            // вариантов - показываем элемент управления
            // и создаем слушатель
            if (data != null && data.getClient() != null
                    && data.getClient().currentFirm() != null
                    && data.getClient().currentFirm().size() > 1) {
                listFirms.setVisibility(View.VISIBLE);
                ArrayList<Firm> list = new ArrayList<>();
                // Заполняем
                for (Map.Entry<String, String> entry : data.getClient().currentFirm().entrySet()) {
                    short i = -1;
                    try {
                        i = Short.parseShort(entry.getKey());
                    } catch (Exception e) {

                    }
                    Firm item = Firm.load(context, i);
                    if (item != null)
                        list.add(item);
                }
                ArrayAdapter<Firm> adapter = new ArrayAdapter<>(context,
                        R.layout.spinner_item, list);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
                listFirms.setAdapter(adapter);

                // Если мы редактируем документ - ставим прошлое значение
                if (currentDocType == DOC_EDIT) {
                    int pos = -1;
                    for (int i = 0; i < list.size(); i++) {
                        Firm firm = list.get(i);
                        pos = i;
                        if (firm.getCode() == data.firmFK())
                            break;
                    }
                    if (pos > -1) {
                        listFirms.setSelection(pos);
                    }
                    if (list.size() > 0)
                        data.firmFK(list.get(pos).getCode());
                } else {
                    if (list.size() > 0)
                        data.firmFK(list.get(0).getCode());
                }

                // Создаем слушатель
                listFirms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        try {
                            Firm selected = (Firm) parent.getAdapter()
                                    .getItem(position);
                            Returns.getReturn().firmFK(
                                    selected.getCode());
                            updateData(false);
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        }
    }

    private void initButtons(View rootView) {
        setListener(rootView.findViewById(R.id.rc_btCancel));
        setListener(rootView.findViewById(R.id.rc_btOk));
        setListener(rootView.findViewById(R.id.rc_btDateDoc));
        setListener(rootView.findViewById(R.id.rc_btShipDate));
    }

    private void setListener(View button) {
        if (button != null) {
            button.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rc_btCancel:
                getFragmentManager().popBackStack();
                break;
            case R.id.rc_btOk://mDocDate
                updateData(true);
                Returns returnData = Returns.getReturn();
                Log.d(TAG, String.valueOf(returnData.mTypeMoney));
                Bundle bundle = new Bundle();
                if (currentDocType == DOC_NEW) {
                    bundle.putString(RETURN_MODE, getString(R.string.intent_new_return_goods));
                } else {
                    bundle.putString(RETURN_MODE, getString(R.string.intent_edit_return_goods));
                }
                ReturnGoodsFragment fragment = new ReturnGoodsFragment();
                fragment.setTargetFragment(this, STEP_RETURN_GOODS);
                fragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.contentMain, fragment, ReturnGoodsFragment.TAG)
                        .addToBackStack(null)
                        .commit();
                break;
            case R.id.rc_btDateDoc:
                showDatePickerDialog(v, Returns.getReturn().mDocDate);
                break;
            case R.id.rc_btShipDate:
                showDatePickerDialog(v, Returns.getReturn().getDateGoodsBack());
                break;
        }
    }

    private void showDatePickerDialog(View view, Date mDocDate) {
        DialogFragment newFragment = new DatePickerFragment(view, mDocDate);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        GlobalProc.closeKeyboard(context);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDateChanged(DateChanged dateChanged) {
        int year = dateChanged.getYear();
        int month = dateChanged.getMonth();
        int day = dateChanged.getDay();
        View v = dateChanged.getView();
        Calendar dateShip = new GregorianCalendar(year, month, day);
        switch (v.getId()) {
            case R.id.rc_btDateDoc:
                Returns.getReturn().mDocDate = dateShip.getTime();
                ((Button) v).setText(getString(R.string.lb_client_doc_num)
                        + ":" + String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year));
                break;
            case R.id.rc_btShipDate:
                Returns.getReturn().setDateGetGoods(dateShip.getTime());
                ((Button) v).setText(getString(R.string.lb_return_date)
                        + ":" + String.format(Locale.ENGLISH, "%02d.%02d.%d", day, month + 1, year));
                break;
        }
    }

}
