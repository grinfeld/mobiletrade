package com.madlab.mtrade.grinfeld.roman.help;


public class HelpAndSupportHeader implements HelpAndSupportTypes {
    private String title;

    public HelpAndSupportHeader(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
