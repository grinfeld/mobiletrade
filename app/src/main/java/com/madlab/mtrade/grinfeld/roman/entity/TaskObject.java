package com.madlab.mtrade.grinfeld.roman.entity;



import java.util.ArrayList;
import java.util.Calendar;
import android.os.Parcel;
import android.os.Parcelable;

public abstract class TaskObject {
    public int mDBID;
    public String mDescription;
    public long mDateDue;
    public byte mImportance;
    public ArrayList<StatusHistory> mStatusHistory;

    public TaskObject(int id, String description, long dateDue) {
        mDBID = id;

        mDescription = description;
        mDateDue = dateDue;

        mImportance = (byte)1;

        mStatusHistory = new ArrayList<TaskObject.StatusHistory>();
    }

    public TaskObject() {
        this(0, "", Calendar.getInstance().getTimeInMillis());
    }

    /**
     * История изменения статуса
     * @author Konovaloff
     *
     */
    public static class StatusHistory implements Parcelable{
        public static final byte ZERO = (byte)0;
        public long mChangeDate;
        public byte mStatusIndex;

        public StatusHistory(long changeDate, byte index) {
            mChangeDate = changeDate;
            mStatusIndex = index;
        }

        public StatusHistory() {
            this(Calendar.getInstance().getTimeInMillis(), ZERO);
        }

        public StatusHistory(Parcel parcel) {
            mChangeDate = parcel.readLong();
            mStatusIndex = parcel.readByte();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int arg1) {
            parcel.writeLong(mChangeDate);
            parcel.writeByte(mStatusIndex);
        }

        public static final Parcelable.Creator<StatusHistory> CREATOR = new Parcelable.Creator<StatusHistory>() {

            public StatusHistory createFromParcel(Parcel in) {
                return new StatusHistory(in);
            }

            public StatusHistory[] newArray(int size) {
                return new StatusHistory[size];
            }
        };
    }
}
