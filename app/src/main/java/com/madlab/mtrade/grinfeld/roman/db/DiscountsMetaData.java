package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

public final class DiscountsMetaData{
    private DiscountsMetaData() {}

    public static final String TABLE_NAME = "Discounts";

    public static final String FIELD_CODE_AGENT = "CodeAgentPrice";
    public static final String FIELD_CODE_CLIENT = "CodeClientPrice";
    public static final String FIELD_CODE_GOODS = "CodeGoodsPrice";
    public static final String FIELD_IS_GROUP = "IsGroup";
    public static final String FIELD_DISCOUNT = "DiscountSize";

    public static final byte FIELD_CODE_AGENT_INDEX = 0;
    public static final byte FIELD_CODE_CLIENT_INDEX = 1;
    public static final byte FIELD_CODE_GOODS_INDEX = 2;
    public static final byte FIELD_IS_GROUP_INDEX = 3;
    public static final byte FIELD_DISCOUNT_INDEX = 4;

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)	NOT NULL DEFAULT '', "
                    + "%s	nvarchar(6)	NOT NULL DEFAULT '', "
                    + "%s	nvarchar(5)	NOT NULL DEFAULT 0, "
                    + "%s	smallint	NOT NULL DEFAULT 0, "
                    + "%s	money		NOT NULL DEFAULT 0)",
            TABLE_NAME,
            FIELD_CODE_AGENT,
            FIELD_CODE_CLIENT,
            FIELD_CODE_GOODS,
            FIELD_IS_GROUP,
            FIELD_DISCOUNT
    );

    public static final String INDEX = String.format(
            "CREATE INDEX idxDiscountAgentClient ON %s ( %s, %s )",
            TABLE_NAME, FIELD_CODE_CLIENT, FIELD_CODE_GOODS);

    public static String insertQuery(String agentCode, String clientCode, String goodsCode, int isGroup, float discount) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s ("
                        + "%s, %s, %s, %s, %s) VALUES ("
                        + "'%s', '%s', '%s', " +
                        "%d, %.2f)", TABLE_NAME,
                FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_CODE_GOODS,
                FIELD_IS_GROUP, FIELD_DISCOUNT,
                agentCode, clientCode, goodsCode,
                isGroup, discount);
    }
}
