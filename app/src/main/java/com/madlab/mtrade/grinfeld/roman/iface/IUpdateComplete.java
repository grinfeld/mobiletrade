package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.UpdateProgrammTask;
import com.madlab.mtrade.grinfeld.roman.tasks.UpdateRestFrom1CTask;

/**
 * Created by GrinfeldRA on 20.12.2017.
 */

public interface IUpdateComplete {

    //void onTaskComplete(UpdateRestFrom1CTask task);

    void onTaskComplete(UpdateProgrammTask updateTask);

    void onTaskComplete(UpdateRestFrom1CTask updateRestFrom1CTask);
}
