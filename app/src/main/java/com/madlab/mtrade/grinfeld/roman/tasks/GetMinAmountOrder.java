package com.madlab.mtrade.grinfeld.roman.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.iface.IGetMinAmountOrder;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by GrinfeldRA on 23.04.2018.
 */

public class GetMinAmountOrder extends AsyncTask<String, Void, String>{

    private final static String COMMAND = "COMMAND";
    private final static String FUNCTION = "МинимальнаяСуммаЗаказа";

    private DalimoClient client;
    private Credentials connectInfo;

    private String codeClient;
    private String codeAgent;
    private String mUnswer = null;
    private String _1CUnswer = null;
    private Context context;
    private IGetMinAmountOrder callback;

    public GetMinAmountOrder(Context context, IGetMinAmountOrder callback, String codeClient, String codeAgent){
        this.context = context;
        this.callback = callback;
        this.codeClient = codeClient;
        this.codeAgent = codeAgent;
    }

    @Override
    protected String doInBackground(String... strings) {
        if (strings != null){
            _1CUnswer = strings[0];
        }
        if (_1CUnswer == null){
            Date orderTime = Order.getOrder().getDateShip();
            SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
            String date = format.format(orderTime);
            String query = String
                    .format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s", COMMAND, codeAgent, FUNCTION, date, codeClient, Const.END_MESSAGE);
            connectInfo = Credentials.load(context);
            client = new DalimoClient(connectInfo);
            if (!client.connect()) {
                mUnswer = "Не удалось подключиться";
                return null;
            }

            client.send(query);

            _1CUnswer = client.receive();

            client.disconnect();
        }

        if (_1CUnswer == null || _1CUnswer.contains(DalimoClient.ERROR)) {
            mUnswer = _1CUnswer;
            return null;
        }
        Log.d("#OrderGoodsFragment", String.valueOf(_1CUnswer));
        return _1CUnswer;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        callback.onTaskComplete(this);
    }
}
