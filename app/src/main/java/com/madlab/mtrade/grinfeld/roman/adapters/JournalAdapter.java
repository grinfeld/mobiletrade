package com.madlab.mtrade.grinfeld.roman.adapters;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.db.ClientCreditInfoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.RemainderMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.Document;
import com.madlab.mtrade.grinfeld.roman.entity.Firm;
import com.madlab.mtrade.grinfeld.roman.entity.Remainder;
import com.madlab.mtrade.grinfeld.roman.fragments.GoodsListFragment;

public class JournalAdapter extends ArrayAdapter<Document> {
    private static final String TAG = "#JournalAdapter";
    private ArrayList<Document> items;

    Drawable checkGreen = null;
    Drawable delRed = null;
    Drawable checkYellow = null;
    ViewHolder holder;
    Context context;
    int YELLOW = Color.YELLOW;
    Const.DOCUMENTS_TYPE typeDoc;

    public JournalAdapter(Context context, int textViewResourceId,
                          ArrayList<Document> items, Const.DOCUMENTS_TYPE typeDoc) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
        checkGreen = ContextCompat.getDrawable(getContext(), R.mipmap.check_g);
        delRed = ContextCompat.getDrawable(getContext(), R.mipmap.del_r);
        checkYellow = ContextCompat.getDrawable(getContext(), R.mipmap.check_y);
        // Меняем на более золотистый
        YELLOW = Color.parseColor("#DAA520");
        this.typeDoc = typeDoc;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = vi.inflate(R.layout.item_journal, null);
            holder = new ViewHolder();
            holder.tvName = rowView.findViewById(R.id.jitem_title);
            holder.tvDate = rowView.findViewById(R.id.jitem_date);
            holder.tvSum = rowView.findViewById(R.id.jitem_sum);
            holder.tvWeight = rowView.findViewById(R.id.jitem_weight);
            holder.imageState = rowView.findViewById(R.id.jitem_imageState);
            holder.imageWeight = rowView.findViewById(R.id.jitem_imageWeight);
            holder.txt_type_server = rowView.findViewById(R.id.txt_type_server);
            holder.txt_type_pay = rowView.findViewById(R.id.txt_type_pay);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Document doc = items.get(position);
        if (doc != null) {
            // Заголовок
            if (holder.tvName != null) {
                int color = YELLOW;
                switch (doc.isHighLight) {
                    case Document.RESERVED:
                        color = Color.CYAN;
                        break;
                    case Document.DELETED:
                        color = Color.RED;
                        break;
                }
                holder.tvName.setText(doc.getCaption());
                holder.tvName.setTextColor(color);
                // tvName.setTextSize(titleSize);
            }
            if (typeDoc == Const.DOCUMENTS_TYPE.Payment) {
                holder.tvDate.setText(doc.getFieldLeft());
                holder.tvSum.setText(doc.getFieldCenter());
                ArrayList<String> arrayList = getFirmAndTypePay(doc.mTag);
                if (arrayList != null && arrayList.size() > 0) {
                    String textFirmAndTypePay = "";
                    if (!arrayList.get(0).equals("")) {
                        Firm item = Firm.load(context, Short.parseShort(arrayList.get(0)));
                        if (item != null) {
                            textFirmAndTypePay = " | " + item.getName();
                        }
                    }
                    textFirmAndTypePay += " | Тип оплаты: " + arrayList.get(1);
                    holder.tvWeight.setText(textFirmAndTypePay);
                }
                holder.imageWeight.setVisibility(View.GONE);
            } else {
                if (typeDoc == Const.DOCUMENTS_TYPE.Order) {
                    //Печатать чек (да = 1/нет =2)
                    String typeMoney = "";
                    if (doc.getTypeMoney() == 1) {
                        typeMoney += "| П";
                    } else if (doc.getTypeMoney() == 2) {
                        typeMoney += "| Н";
                    }
                    holder.txt_type_pay.setText(typeMoney);
                }
                switch (doc.getTypeReserveServer()) {
                    case 0:
                        holder.txt_type_server.setVisibility(View.GONE);
                        break;
                    case 1:
                        holder.txt_type_server.setVisibility(View.VISIBLE);
                        holder.txt_type_server.setText("| APP");
                        break;
                    case 2:
                        holder.txt_type_server.setVisibility(View.VISIBLE);
                        holder.txt_type_server.setText("| IIS");
                        break;
                }
                holder.imageWeight.setVisibility(View.VISIBLE);
                holder.tvDate.setText(doc.mTag);
                // Левая колонка
                if (holder.tvSum != null) {
                    // tvSum.setTextSize(labelSize);
                    holder.tvSum.setText(doc.getFieldLeft());
                }

                // Центр (вес)
                if (holder.tvWeight != null) {
                    // tvWeight.setTextSize(labelSize);
                    holder.tvWeight.setText(doc.getFieldCenter());
                }
            }


            // Правая колонка (Состояние)
            if (holder.imageState != null) {
                Drawable draw = null;

                switch (doc.isHighLight) {
                    case Document.RESERVED:// Резерв
                        draw = checkGreen;
                        break;
                    case Document.DELETED:// Удалена
                        draw = delRed;
                        break;
                    default:// Обычное состояние
                        draw = checkYellow;
                        break;
                }

                if (draw != null) {
                    holder.imageState.setImageDrawable(draw);
                }
            }
        }
        return rowView;
    }

    private ArrayList<String> getFirmAndTypePay(String number) {
        Cursor rows = null;
        ArrayList<String> result = new ArrayList<>();
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            String where = String.format("%s = '%s'", ClientCreditInfoMetaData.FIELD_NUMBER, number);
            rows = app.getDB().query(ClientCreditInfoMetaData.TABLE_NAME, null, where, null, null, null, null);
            while (rows.moveToNext()) {
                String firma = rows.getString(rows.getColumnIndex(ClientCreditInfoMetaData.FIELD_FIRMA));
                String typePay = rows.getString(rows.getColumnIndex(ClientCreditInfoMetaData.FIELD_DOC_TYPE));
                result.add(firma);
                result.add(typePay);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    private static class ViewHolder {
        TextView tvName;
        TextView tvDate;
        TextView tvSum;
        TextView tvWeight;
        ImageView imageState;
        ImageView imageWeight;
        TextView txt_type_server;
        TextView txt_type_pay;
    }

}