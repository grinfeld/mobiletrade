package com.madlab.mtrade.grinfeld.roman.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = "!->DatabaseHelper";

    public static final String DATABASE_NAME = "mtrade.db";
    public static final int DATABASE_VERSION = 62;

    public static final String AUTHORITY = "com.dalimo.provider.MTradeDB";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        // mContext = context;
    }

    /**
     * Список запросов для очистки таблиц при импорте В комментариях указаны
     * сопоставления из файла array.xml
     */
    public final static String[] CLEAR_TABLES_LIST = {ClientsMetaData.CLEAR, // file_clients
            GoodsMetaData.CLEAR, // file_goods
            ClientCreditInfoMetaData.CLEAR, // file_credit
            FirmsMetaData.CLEAR, // file_firma
            MatrixMetaData.CLEAR, // file_matrix
            MustListMetaData.CLEAR, // file_mustlist
            GoodsNLMetaData.CLEAR, // file_smtp
            TransitMetaData.CLEAR, // file_transit
            ExcludedMetaData.CLEAR, // file_excluded
            AutoorderMetaData.CLEAR, // file_autoorder
            DiscountsMetaData.CLEAR, // file_discounts
            ManagerMetaData.CLEAR, // file_managers
            RemainderMetaData.CLEAR, //file_ostatky
            PhotoFilterMetaData.CLEAR,
            PostMetaData.CLEAR
    };

    /**
     * Список запросов для удаления документов
     */
    public final static String[] CLEAR_DOC_QUERYS_LIST = {
            OrderItemMetaData.CLEAR, OrderMetaData.CLEAR,
            ReturnItemMetaData.CLEAR, ReturnMetaData.CLEAR,
            PaymentsMetaData.CLEAR, VisitMetaData.CLEAR,
            LocationMetadata.CLEAR, ShipmentMustListMetaData.CLEAR,
            TaskMetaData.CLEAR, TaskItemMetaData.CLEAR, TaskStatusHistoryMetaData.CLEAR,
            MonitoringMetaData.CLEAR, MonitoringProductMetaData.CLEAR, PaybackPhotoMetaData.CLEAR};

    public final static String MATRIX_QUERY = String.format(
            "SELECT * " + "FROM %s " + "INNER JOIN %s ON (" + "%s.%s=%s.%s) "
                    + "WHERE %s.%s=? ",// + "ORDER BY %s.%s",
            // From
            GoodsMetaData.TABLE_NAME,
            // InnerJoin
            MatrixMetaData.TABLE_NAME,
            // On
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
            // Where
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_CLIENT
            // ORDER BY
            // MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_SORT_ORDER
    );

    public final static String MATRIX_QUERY_M = String.format("SELECT * "
                    + "FROM %s " + "INNER JOIN %s ON (" + "%s.%s=%s.%s) "
                    + "WHERE %s.%s=? ",
            // From
            GoodsMetaData.TABLE_NAME,
            // InnerJoin
            MatrixMetaData.TABLE_NAME,
            // On
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
            // Where
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_CLIENT);


    public final static String MATRIX_QUERY_GROUP = String.format("SELECT * "
                    + "FROM %s " + "INNER JOIN %s ON (" + "%s.%s=%s.%s) "
                    + "WHERE %s.%s=? AND %s.%s=? ",
            // From
            GoodsMetaData.TABLE_NAME,
            // InnerJoin
            MatrixMetaData.TABLE_NAME,
            // On
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
            // Where
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_PARENT.Name,
            // AND
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_CLIENT);

    public final static String MATRIX_QUERY_GROUP_REST = String.format(
            "SELECT * " + "FROM %s " + "INNER JOIN %s ON (" + "%s.%s=%s.%s) "
                    + "WHERE %s.%s=? AND %s.%s>0 AND %s.%s=? ",
            // From
            GoodsMetaData.TABLE_NAME,
            // InnerJoin
            MatrixMetaData.TABLE_NAME,
            // On
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
            // Where
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_PARENT.Name,
            // AND
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_REST.Name,
            // AND
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_CLIENT);

    @Override
    public void onCreate(SQLiteDatabase db) {
        String[] tables = new String[]{
                DBPropMetaData.CREATE_TABLE, // Номера документов
                "INSERT INTO dbProp (numDoc, numOrder) VALUES (1, 1)",
                ClientsMetaData.CREATE_COMMAND, // таблица с клиентами
                GoodsMetaData.CREATE_COMMAND, // таблица с товарами
                OrderMetaData.CREATE_COMMAND, // таблица с заявками
                OrderItemMetaData.CREATE_COMMAND, // таблица с заявками
                ClientCreditInfoMetaData.CREATE_COMMAND,// таблица с долгами
                MatrixMetaData.CREATE_COMMAND, // матрица
                FirmsMetaData.CREATE_COMMAND, // Наши реквизиты
                PaymentsMetaData.CREATE_COMMAND, // Платежи
                ReturnMetaData.CREATE_COMMAND, // Возвраты
                ReturnItemMetaData.CREATE_COMMAND, // Элемент возврата
                TaskMetaData.CREATE_COMMAND, // задачи
                TransitMetaData.CREATE_COMMAND, // Товары в пути
                LastSalesMetaData.CREATE_COMMAND, // Последние продажи
                ExcludedMetaData.CREATE_COMMAND, // исключения
                OrderStatusMetaData.CREATE_COMMAND, // статус заказа
                GoodsNLMetaData.CREATE_COMMAND, // Не ликвид
                AutoorderMetaData.CREATE_COMMAND, // Автозаказ offline
                VisitMetaData.CREATE_COMMAND, // данные визита
                LocationMetadata.CREATE_COMMAND, // данные о местоположениях
                MustListMetaData.CREATE_COMMAND, // данные Must-листа
                TaskItemMetaData.CREATE_COMMAND, // подзадачи
                TaskStatusHistoryMetaData.CREATE_COMMAND, // история изменения
                DiscountsMetaData.CREATE_COMMAND, // скидки
                ManagerMetaData.CREATE_COMMAND, // ответственные менеджеры
                RemainderMetaData.CREATE_COMMAND, //остатки на складах
                ShipmentMustListMetaData.CREATE_COMMAND,
                GeolocationMetaData.CREATE_COMMAND,
                PhotoFilterMetaData.CREATE_COMMAND,
                ContactMetaData.CREATE_COMMAND,
                PostMetaData.CREATE_COMMAND,

                PromotionMetaData.CREATE_COMMAND,//Акции
                PromotionCheckedMetaData.CREATE_COMMAND,
                PromotionItemMetaData.CREATE_COMMAND,
                PromotionProductMetaData.CREATE_COMMAND,
                PromotionBonusesMetaData.CREATE_COMMAND,
                ManagerMerchMetaData.CREATE_COMMAND,
                MonitoringMetaData.CREATE_COMMAND,
                MonitoringProductMetaData.CREATE_COMMAND,

                PaybackPhotoMetaData.CREATE_COMMAND,
                VisitReservedMetadata.CREATE_COMMAND
        };

        // Теперь индексы
        String[] indexes = new String[]{GoodsMetaData.INDEX,
                GoodsMetaData.INDEX_SORT_ORDER, GoodsMetaData.INDEX_HIT,
                GoodsMetaData.INDEX_STM, ClientsMetaData.INDEX_SORT_ORDER,
                ClientsMetaData.INDEX_TODAY_VISIT, OrderMetaData.INDEX,
                OrderItemMetaData.INDEX, ClientCreditInfoMetaData.INDEX,
                MatrixMetaData.INDEX, PaymentsMetaData.INDEX,
                ReturnMetaData.INDEX, ReturnItemMetaData.INDEX,
                LastSalesMetaData.INDEX, ExcludedMetaData.INDEX,
                OrderStatusMetaData.INDEX_STATUS,
                OrderStatusMetaData.INDEX_TYPE, GoodsNLMetaData.INDEX,
                AutoorderMetaData.INDEX, LocationMetadata.INDEX_SENDED,
                LocationMetadata.INDEX_AGENT_SENDED, MustListMetaData.INDEX,
                TaskItemMetaData.INDEX, DiscountsMetaData.INDEX, MonitoringProductMetaData.INDEX};

        // Создаем таблицы
        execMultipleSQL(db, tables);
        // Теперь индексы
        execMultipleSQL(db, indexes);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion <= oldVersion) {
            return;
        }
        boolean success = false;
        switch (oldVersion) {
            case 39:
                success = update39to40(db) && update40to41(db) && update41to42(db) && update42to43(db) && update43to44(db) && update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 40:
                success = update40to41(db) && update41to42(db) && update42to43(db) && update43to44(db) && update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 41:
                success = update41to42(db) && update42to43(db) && update43to44(db) && update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 42:
                success = update42to43(db) && update43to44(db) && update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 43:
                success = update43to44(db) && update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 44:
                success = update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 45:
                success = update44to45(db) && update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update61to62(db) && update60to61(db);
                break;
            case 46:
                success = update45to46(db) && update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update61to62(db) && update60to61(db);
                break;
            case 47:
                success = update46to47(db) && update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update60to61(db) && update61to62(db);
                break;
            case 48:
                success = update47to48(db) && update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update60to61(db) && update61to62(db);
                break;
            case 49:
                success = update48to49(db) && update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update60to61(db) && update61to62(db);
                break;
            case 50:
                success = update49to50(db) && update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update60to61(db) && update61to62(db);
                break;
            case 51:
                success = update50to51(db) && update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 52:
                success = update51to52(db) && update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 53:
                success = update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 54:
                success = update52to53(db) && update53to54(db) && update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update61to62(db) && update60to61(db);
                break;
            case 55:
                success = update54to55(db) && update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 56:
                success = update55to56(db) && update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 57:
                success = update56to57(db) && update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 58:
                success = update57to58(db) && update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 59:
                success = update58to59(db) && update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 60:
                success = update59to60(db) && update60to61(db) && update61to62(db);
                break;
            case 61:
                success = update60to61(db) && update61to62(db);
                break;
            case 62:
                success = update61to62(db);
                break;
        }
        if (success) {
            Log.i(TAG, "Successful complete");
        } else {
            Log.e(TAG, "Update failed");
        }
    }
    private boolean update61to62(SQLiteDatabase db) {
        String[] sql = {VisitReservedMetadata.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update60to61(SQLiteDatabase db) {
        String[] sql = {PaybackPhotoMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update59to60(SQLiteDatabase db) {
        String[] sql = {String.format(
                "ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                VisitMetaData.TABLE_NAME, VisitMetaData.FIELD_IS_RESERVED),
                String.format("ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                        ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_IS_RESERVED)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update58to59(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s text NOT NULL DEFAULT ''",
                ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_LAST_UPDATE_CONTACT),
                PostMetaData.CREATE_COMMAND, ContactMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update57to58(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s nvarchar(36) NOT NULL DEFAULT ''",
                ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_UUID_DOC)};
        execMultipleSQL(db, sql);
        return true;
    }


    private boolean update56to57(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s integer NOT NULL DEFAULT 0",
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_IS_NON_CASH)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update55to56(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s integer NOT NULL DEFAULT 0",
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_IS_CUSTOM),
                String.format("ALTER TABLE %s ADD %s smallint NOT NULL DEFAULT 0",
                        ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_FOREIGN_AGENT)};
        execMultipleSQL(db, sql);
        return true;
    }


    private boolean update54to55(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s integer NOT NULL DEFAULT 0",
                OrderItemMetaData.TABLE_NAME, OrderItemMetaData.FIELD_IS_CHECK)};
        execMultipleSQL(db, sql);
        return true;
    }


    private boolean update53to54(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s smallint",
                ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_REPAYMENT),
                String.format("ALTER TABLE %s ADD %s smallint",
                        ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_PHOTO_IS_EXIST)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update52to53(SQLiteDatabase db) {
        String[] sql = {PromotionCheckedMetaData.DROP_TABLE, PromotionCheckedMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update51to52(SQLiteDatabase db) {
        String[] sql = {PromotionCheckedMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update50to51(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s int",
                GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_IS_MERCURY)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update49to50(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s smallint NOT NULL DEFAULT 1",
                ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_REG_MERCURY),
                String.format("ALTER TABLE %s ADD %s text ''",
                        TaskMetaData.TABLE_NAME, TaskMetaData.FIELD_CODE_MANAGER),
                String.format("ALTER TABLE %s ADD %s short NOT NULL DEFAULT 0",
                        TaskMetaData.TABLE_NAME, TaskMetaData.FIELD_UNCONFIRMED_TASK)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update48to49(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s nvarchar(32) NOT NULL DEFAULT ''",
                ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_UPPER_NAME)};
        execMultipleSQL(db, sql);
        return true;
    }


    private boolean update47to48(SQLiteDatabase db) {
        String[] sql = {ManagerMerchMetaData.CREATE_COMMAND,
                MonitoringMetaData.CREATE_COMMAND,
                MonitoringProductMetaData.CREATE_COMMAND,
                MonitoringProductMetaData.INDEX};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update46to47(SQLiteDatabase db) {
        String[] sql = {String.format("ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                OrderMetaData.TABLE_NAME, OrderMetaData.FIELD_TYPE_SERVER),
                String.format("ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                        ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_TYPE_SERVER)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update45to46(SQLiteDatabase db) {
        String[] sql = {PromotionMetaData.CREATE_COMMAND,//Акции
                PromotionItemMetaData.CREATE_COMMAND,
                PromotionProductMetaData.CREATE_COMMAND,
                PromotionBonusesMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update44to45(SQLiteDatabase db) {
        String[] sql = {String.format(
                "ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                VisitMetaData.TABLE_NAME, VisitMetaData.FIELD_TYPE_SERVER)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update43to44(SQLiteDatabase db) {
        String[] sql = {PhotoFilterMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update42to43(SQLiteDatabase db) {
        String[] sql = {GeolocationMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update41to42(SQLiteDatabase db) {
        String[] sql = {String.format(
                "ALTER TABLE %s	ADD %s smallint NOT NULL DEFAULT 0",
                ReturnMetaData.TABLE_NAME, ReturnMetaData.FIELD_TYPE_ACT),
                String.format("ALTER TABLE %s	ADD %s	nvarchar(30) NOT NULL DEFAULT ''",
                        ClientCreditInfoMetaData.TABLE_NAME, ClientCreditInfoMetaData.FIELD_FIRMA)};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update40to41(SQLiteDatabase db) {
        String[] sql = {ShipmentMustListMetaData.CREATE_COMMAND};
        execMultipleSQL(db, sql);
        return true;
    }

    private boolean update39to40(SQLiteDatabase db) {
        String[] sql = {String.format(
                "ALTER TABLE %s ADD %s int NOT NULL DEFAULT 0",
                ClientsMetaData.TABLE_NAME, ClientsMetaData.FIELD_TYPE_SALES_PLAN)};
        execMultipleSQL(db, sql);
        return true;
    }


    @SuppressLint("LongLogTag")
    public static void execMultipleSQL(SQLiteDatabase db, String[] sql) {
        for (String s : sql) {
            if (s.trim().length() > 0) {
                try {
                    db.execSQL(s);
                } catch (SQLException e) {
                    Log.e(TAG + ".execMultipleSQL", s);
                }
            }
        }
    }

    @SuppressLint("LongLogTag")
    public static void execMultipleSQLTXN(SQLiteDatabase db, String[] sql) {
        db.beginTransaction();
        for (String s : sql) {
            try {
                db.execSQL(s);
            } catch (SQLException e) {
                Log.e(TAG + " " + e.getMessage() + "|||.execMultipleSQLTXN", s);
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }

}