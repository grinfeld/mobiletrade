package com.madlab.mtrade.grinfeld.roman.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;

import org.json.JSONObject;

import java.util.Locale;
import java.util.StringTokenizer;

import okhttp3.Response;

/**
 * Created by GrinfeldRA on 14.11.2017.
 */

public class SetReservOrderTask extends AsyncTask<Short, Void, Boolean> {
    private final static String TAG = "!->SetReservTask";

    private final static String PROC_NAME = "СформироватьДокументыФормат";

    private final static String SUCCESS = "OK";
    private final static String FAILURE = "ERR";

    private final static String WAIT_MESSAGE = "Связь с сервером";

    private String mUnswer;
    private String mVersion;

    private Context baseContext;
    private ProgressDialog mProgress;

    private Credentials connectInfo;

    private ISetReservComplete mTaskCompleteListener;

    public SetReservOrderTask(Context data, ISetReservComplete taskCompleteListener) {
        baseContext = data;
        mTaskCompleteListener = taskCompleteListener;
        mUnswer = "";

        mVersion = GlobalProc.getVersionName(baseContext);
        connectInfo = Credentials.load(baseContext);
    }

    public String getMessage() {
        return mUnswer;
    }

    @Override
    protected void onPreExecute() {
        // showDlg();
    }

    @Override
    protected Boolean doInBackground(Short... numbers) {
        SQLiteDatabase db = null;
        DalimoClient myClient = null;

        boolean success = true;
        try {
            MyApp app = (MyApp) baseContext.getApplicationContext();
            db = app.getDB();
            App settings = App.get(baseContext);
            for (Short current : numbers) {
                String data;
                Order order;
                int typeServer = 0;
                order = Order.load(db, current);

                if (order.mState == Order.State.Deleted) {
                    continue;
                }

                if (order.mReserved) {
                    continue;
                }
                StringBuilder sb = new StringBuilder(String.format("%s;%s;%s;",
                        Const.COMMAND, order.mCodeAgent, PROC_NAME));
                //COMMAND;С01201;СформироватьДокументыФормат;1;V;С01201;С14906;
                // 23.08.18;0;1;2;0;0;;16:47;0;1;1.0.61;0.0;0.0;0.0;0;0.00;0;0;1;038;;;;;;;69794|1|1.00|0|492.96|887.33|0|0|;31883|1|26.00|0|24.53|637.78|0|0|;32031|1|1.00|0|240.33|240.33|0|0|;36335|1|22.00|0|49.68|1092.96|0|0|;55261|1|24.00|0|47.92|1150.08|0|0|;13181|1|20.00|0|37.36|747.20|0|0|;13518|1|20.00|0|37.36|747.20|0|0|;42517|1|24.00|0|36.12|866.88|0|0|;38988|1|44.00|0|22.05|970.20|0|0|;32922|1|24.00|0|58.79|1410.96|0|0|;46643|1|28.00|0|51.96|1454.88|0|0|;32633|1|28.00|0|51.96|1454.88|0|0|;32194|1|28.00|0|44.30|1240.40|0|0|;<EOF>
                String answer;
                if (order.getItems().size() > 0){
                    data = generateDataStringForReserv(order);
                    sb.append(data);
                    sb.append(Const.END_MESSAGE);
                }else {
                    mUnswer = baseContext.getString(R.string.err_load_doc_table);
                    return false;
                }

                if (settings.connectionServer.equals("1")) {
                    myClient = new DalimoClient(connectInfo);
                    if (!myClient.connect()) {
                        success = false;
                        break;
                    }
                    myClient.send(sb.toString());
                    answer = myClient.receive();
                    typeServer = JournalFragment.APP_SERVER;
                    myClient.disconnect();
                } else {
                    String region = App.getRegion(MyApp.getContext());
                    StringBuilder stringBuilder = new StringBuilder(data);
                    data = stringBuilder.replace(stringBuilder.lastIndexOf(";"), stringBuilder.lastIndexOf(";") + 1, "").toString();
                    try {
                        Response response = OkHttpClientBuilder.buildCall(PROC_NAME, data, region).execute();
                        if (response.isSuccessful()) {
                            String strResponse = response.body().string();
                            JSONObject jsonObject = new JSONObject(strResponse);
                            answer = jsonObject.getString("data");
                            typeServer = JournalFragment.IIS_SERVER;
                        } else {
                            answer = FAILURE;
                        }
                    } catch (Exception e) {
                        myClient = new DalimoClient(connectInfo);
                        if (!myClient.connect()) {
                            success = false;
                            break;
                        }
                        myClient.send(sb.toString());
                        answer = myClient.receive();
                        typeServer = JournalFragment.APP_SERVER;
                        myClient.disconnect();
                    }
                }

                // А теперь обрабатываем полученное сообщение
                if (answer.contains(FAILURE)) {
                    success = false;
                    mUnswer += answer;
                } else {// В случае успеха - обновляем параметры
                    order.mReserved = true;
                    order.typeServer = typeServer;
                    StringTokenizer st = new StringTokenizer(answer, ";");
                    byte paramCount = 0;
                    success = false;
                    while (st.hasMoreElements()) {
                        String object = st.nextToken();
                        switch (paramCount) {
                            case 0:
                                success = object.contains(SUCCESS);
                                break;
                            case 1://составной код из программы 1С
                                Log.i(TAG, object);
                                break;
                            case 2://номера электронных заявок в 1С
                                order.DocNums(object);
                                break;
                            default:
                                break;
                        }

                        paramCount++;
                    }
                    if (success)
                        order.update(db);
                }

                myClient = null;
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            // Log.e(TAG, e.toString());
            success = false;
        } finally {
            if (myClient != null)
                myClient.disconnect();
        }

        return success;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        // dismiss();

        // mUnswer = result ? "Заявка(и) успешно отправлены" :
        // "Одна или несколько заявок не были отправлены";

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress.hide();
        }
    }

    private void showDlg() {
        mProgress = new ProgressDialog(baseContext);
        mProgress.setMessage(WAIT_MESSAGE);
        mProgress.setCancelable(true);
        mProgress.show();
    }

    public void attach(Activity activity, ISetReservComplete listener) {
        baseContext = activity;
        mTaskCompleteListener = listener;
        showDlg();
    }

    public void detach() {
        dismiss();

        baseContext = null;
        mTaskCompleteListener = null;
    }

    protected String generateDataStringForReserv(Order data) {
        String codeCli = data.getClient() != null ? data.getClient().getCode() : "";
        String orderCommon = String.format(Locale.ENGLISH,
                "%s;%s;%s;%s;%s;%d;%d;%d;%d;%d;"
                        + "%s;%s;%d;%d;%s;%s;%s;%s;%d;%.2f;" + "%d;%d;%d;%03d;"
                        + "%s;%s;%s;%s;%s;%s", data.getNumOrder(), // 0 ID документа
                (data.mState == Order.State.Normal) ? Order.STATE_NORMAL
                        : Order.STATE_DELETED, // 1 Статус (пометка удалаения)
                data.mCodeAgent, // 2 Код менеджера
                codeCli, // 3 Код контрагента
                TimeFormatter.sdf1C.format(data.getDateShip()), // 4 Дата поставки
                data.mTypePay, // 5 дополнительные указания
                data.mPrintBill, // 6 Признак документа
                data.mTypeMoney, // 7 Вид оплаты
                data.mTakeMoney ? 1 : 0, // 8 Забрать возврат
                data.mTypeVisit, // 9 тип визита
                data.mNote, // 10 Указания по доставке
                data.getOrderTime(), // 11 Время принятия заявки
                data.mVetSpr ? 1 : 0, // 12 Ветеринарное удостоверение
                data.wasAutoOrder(), // 13 Был автозаказ
                mVersion, // 14 Версия программы
                "0.0", // 15 широта
                "0.0", // 16 Долгота
                "0.0", // 17 Погрешность
                data.mWarranty ? 1 : 0, // 18
                data.sumToGet(), // 19
                data.selfDelivery() ? 1 : 0,// 20 Везу сам
                data.quality() == Order.QUALITY_NORMAL ? 0 : 1, // Качество
                data.wasCheck() ? 1 : 0, // 22 была ли проверка остатков
                data.firmFK(), // 23 ссылка на фирму
                "", // 24 Резервное поле
                "", // 25 Резервное поле
                "", // 26 Резервное поле
                "", // 27 Резервное поле
                "", // 28 Резервное поле
                "" // 29 Резервное поле
        );

        StringBuilder sb = new StringBuilder(String.format("%s;", orderCommon));
        for (OrderItem item : data.getItems()) {
            //Так как количество упаковок - множитель, вставляем костыль
            int countPack = item.getCountPack() == 0 ? 1 : item.getCountPack();

            //Теперь колдуем с количеством
            float count = 1f;
            if (item.mIsWeightGoods) {
                if (item.mIsHalfHead)//Всего полголовки
                    count = 0.5f;//item.getWeight() / 2;
                else//Весовой товар передаём в головках
                    count = item.getCount();
            } else {
                //количеством будет кол-во в упаковке, кол-во упаковок - множитель
                if (item.inPack()) {
                    count = item.numInPack();
                } else {
                    count = item.getCount() * item.getQuant();
                }
            }
            sb.append(String.format(Locale.ENGLISH,
                    "%s|%d|%.2f|%d|%.2f|%.2f|%d|%d|;", item.getCodeGoods(), // 0
                    countPack, // 1
                    count, // 2
                    item.mIsSpecialPrice ? 1 : 0, // 3
                    item.getPrice(), // 4
                    item.getAmount(), // 5
                    item.isSertificateNeed() ? 1 : 0, // 6
                    item.isQualityDocNeed() ? 1 : 0 // 7
            ));
        }
        return sb.toString();
    }

}