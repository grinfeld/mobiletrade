package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRA
 */

public class PromotionProductMetaData {

    public static final String TABLE_NAME = "PromotionProduct";

    public static final String FIELD_ORIGIN_ID = "origin_id";
    public static final String FIELD_ID_PROMOTION = "id_promotion";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s text NOT NULL DEFAULT '',"
                    + "%s integer NOT NULL DEFAULT 0)",
            TABLE_NAME,
            FIELD_ORIGIN_ID,FIELD_ID_PROMOTION);

    public static String insertQuery(String origin_id, int id_promotions) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s) "
                        + "VALUES "
                        + "('%s', %d)",
                TABLE_NAME, FIELD_ORIGIN_ID, FIELD_ID_PROMOTION,
                origin_id, id_promotions);
    }

}
