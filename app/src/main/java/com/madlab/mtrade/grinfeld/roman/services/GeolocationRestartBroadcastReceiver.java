package com.madlab.mtrade.grinfeld.roman.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by GrinfeldRA
 */

public class GeolocationRestartBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("#GeolocationService", "restart");
        context.startService(new Intent(context, GeolocationService.class));
    }
}
