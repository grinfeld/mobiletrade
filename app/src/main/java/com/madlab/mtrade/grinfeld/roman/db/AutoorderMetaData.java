package com.madlab.mtrade.grinfeld.roman.db;


import java.util.Locale;

import android.provider.BaseColumns;

public final class AutoorderMetaData implements BaseColumns {
    private AutoorderMetaData() {
    }

    public static final String TABLE_NAME = "AutoOrder";

    public static final String DEFAULT_SORT_ORDER = "modified DESC";

    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CODE_CLIENT = "CodeCli";
    public static final String FIELD_CODE_GOODS = "CodeGoods";
    public static final String FIELD_COUNT = "Count";
    public static final String FIELD_ML_SIGN = "MLSign";
    public static final String FIELD_NEWBEE_SIGN = "NewbeeSign";

    public static final byte FIELD_CODE_AGENT_INDEX = 0;
    public static final byte FIELD_CODE_CLIENT_INDEX = 1;
    public static final byte FIELD_CODE_GOODS_INDEX = 2;
    public static final byte FIELD_COUNT_INDEX = 3;
    public static final byte FIELD_ML_SIGN_INDEX = 4;
    public static final byte FIELD_NEWBEE_SIGN_INDEX = 5;

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)	NOT NULL DEFAULT '', "
                    + "%s	nvarchar(6)	NOT NULL DEFAULT '', "
                    + "%s	nvarchar(5)	NOT NULL DEFAULT '', "
                    + "%s	money		NOT NULL DEFAULT 0, "
                    + "%s	smallint	NOT NULL DEFAULT 0, "
                    + "%s	smallint	NOT NULL DEFAULT 0)", TABLE_NAME,
            FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_CODE_GOODS, FIELD_COUNT,
            FIELD_ML_SIGN, FIELD_NEWBEE_SIGN);

    public static final String INDEX = String.format(
            "CREATE INDEX idxAutoOrderManagerClient ON %s ( %s, %s )",
            TABLE_NAME, FIELD_CODE_AGENT, FIELD_CODE_CLIENT);

    public static String insertQuery(String agentCode, String clientCode,
                                     String goodsCode, String count, String isMustList, String isNewbee) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s ("
                        + "%s, %s, %s, %s, %s, %s) VALUES ("
                        + "'%s', '%s', '%s', %s, %s, %s)", TABLE_NAME,
                FIELD_CODE_AGENT, FIELD_CODE_CLIENT, FIELD_CODE_GOODS,
                FIELD_COUNT, FIELD_ML_SIGN, FIELD_NEWBEE_SIGN, agentCode,
                clientCode, goodsCode, count, isMustList, isNewbee);
    }
}
