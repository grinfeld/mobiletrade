package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.ChangeBounds;
import android.support.transition.Fade;
import android.support.transition.Scene;
import android.support.transition.Transition;
import android.support.transition.TransitionManager;
import android.support.transition.TransitionSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.db.LocationMetadata;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.IFetchLocationTask;
import com.madlab.mtrade.grinfeld.roman.tasks.FetchLocationTask;
import com.madlab.mtrade.grinfeld.roman.tasks.InitTime;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


/**
 * Created by GrinfeldRA on 18.10.2017.
 * Класс по определению геолокации
 */

public class LocationFragment extends Fragment implements IFetchLocationTask {

    private static final String TAG = "#LocationFragment";
    private View locationImg;
    private Visit mVisit;
    private Client mClient;
    private FetchLocationTask getLocation;
    private UUID mID;
    public final static String FAIL = "FAIL";
    private AlertDialog alertDialogLocationSettings;
    private Activity context;


    public static LocationFragment newInstance(Client client) {
        Bundle args = new Bundle();
        args.putParcelable(Client.KEY, client);
        LocationFragment fragment = new LocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        context = getActivity();
        ((IDynamicToolbar) getActivity()).onChangeToolbar(null);
        View rootView = inflater.inflate(R.layout.fragment_location_contain, container, false);
        mClient = getArguments().getParcelable(Client.KEY);
        View circle = rootView.findViewById(R.id.viewT);
        View wave1 = rootView.findViewById(R.id.viewWave1);
        View wave2 = rootView.findViewById(R.id.viewWave2);
        View wave3 = rootView.findViewById(R.id.viewWave3);
        locationImg = rootView.findViewById(R.id.img_location);
        mID = UUID.randomUUID();
        final List<AnimationView> myViews =
                Arrays.asList(new AnimationView(circle, 800), new AnimationView(wave1, 1200),
                        new AnimationView(wave2, 1500), new AnimationView(wave3, 1800));
        startAnimation(myViews);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                        //block back button
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView");
        if (getLocation != null && getLocation.getStatus() == AsyncTask.Status.RUNNING) {
            getLocation.cancel(false);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isLocationDisabled()) {
            getLocationSettingsDialog();
            if (getLocation != null && getLocation.getStatus() == AsyncTask.Status.RUNNING) {
                getLocation.cancel(false);
            }
        } else {
            if (alertDialogLocationSettings != null) {
                alertDialogLocationSettings.dismiss();
            }
            if (getLocation == null || getLocation.getStatus() != AsyncTask.Status.RUNNING) {
                getLocation = new FetchLocationTask(getActivity(), this);
                getLocation.execute();
            }
        }
    }

    private void startAnimation(List<AnimationView> viewList) {
        int i = 0;
        for (AnimationView myView : viewList) {
            View view = myView.getView();
            ScaleAnimation fade_in = new ScaleAnimation(0f, 1f, 0f, 1f,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            fade_in.setDuration(myView.getDuration());
            fade_in.setFillAfter(true);
            view.setVisibility(View.VISIBLE);
            view.startAnimation(fade_in);
            if (i == 0) {
                fade_in.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Animation anim = AnimationUtils.loadAnimation(getActivity(),
                                R.anim.anim_location_img);
                        locationImg.setVisibility(View.VISIBLE);
                        locationImg.startAnimation(anim);
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                Animation anim = AnimationUtils.loadAnimation(getActivity(),
                                        R.anim.anim_location_img2);
                                locationImg.startAnimation(anim);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            } else {
                fade_in.setAnimationListener(null);
            }
            i++;
        }
    }


    private void startVisitFragment(final String address, final int locationId) {
        ViewGroup sceneRoot = getActivity().findViewById(R.id.scene_root);
        int resId;
        if (App.isMerch()) {
            resId = R.layout.fragment_visit_contain_mmerch;
        } else {
            resId = R.layout.fragment_visit_contain;
        }
        Scene visitScene = Scene.getSceneForLayout(sceneRoot, resId, getActivity());
        TransitionSet set = new TransitionSet();
        set.addTransition(new Fade());
        set.addTransition(new ChangeBounds());
        set.setOrdering(TransitionSet.ORDERING_SEQUENTIAL);
        set.setDuration(1000);
        set.setInterpolator(new AccelerateInterpolator());
        set.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionEnd(@NonNull Transition transition) {
                FragmentManager fragmentManager = context.getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.contentMain, VisitContainFragment.newInstance(mClient, mVisit, address, mID, locationId))
                        .commitAllowingStateLoss();
            }

            @Override
            public void onTransitionCancel(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionPause(@NonNull Transition transition) {
            }

            @Override
            public void onTransitionResume(@NonNull Transition transition) {
            }
        });
        TransitionManager.go(visitScene, set);
    }

    private boolean isLocationDisabled() {
        final Context context = getActivity();
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return (!gps_enabled && !network_enabled);
    }

    private void getLocationSettingsDialog() {
        final Context context = getActivity();
        // notify user
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        alertDialogLocationSettings = dialog.create();
        dialog.setTitle("Внимание");
        dialog.setMessage(getActivity().getString(R.string.mes_switch_on_GPS));
        dialog.setIcon(R.mipmap.main_icon);
        dialog.setCancelable(false);
        dialog.setPositiveButton("Настройки", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(myIntent);
                //get gps
            }
        });
//        dialog.setNegativeButton("Выйти", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClickDel(DialogInterface paramDialogInterface, int paramInt) {
//                paramDialogInterface.cancel();
//            }
//        });
        dialog.show();
    }

    @Override
    public void onLocation(FetchLocationTask task) {
        Location currentLocation;
        String address = null;
        int res = -1;
        try {
            currentLocation = task.get();
        } catch (Exception e) {
            currentLocation = null;
        }
        if (currentLocation == null) {
            GlobalProc.mToast(context, "Не удалось определить координату");
            currentLocation = new Location("FAIL");
            currentLocation.setLatitude(0f);
            currentLocation.setLongitude(0f);
            currentLocation.setAccuracy(0f);
            currentLocation.setSpeed(0f);
        } else {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(currentLocation.getLatitude(),
                        currentLocation.getLongitude(), 1);
                if (null != listAddresses && listAddresses.size() > 0) {
                    address = listAddresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Date noReallyThisIsTheTrueDateAndTime = null;
        try {
            noReallyThisIsTheTrueDateAndTime = new InitTime().execute().get();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        long time = new Date().getTime();
        if (noReallyThisIsTheTrueDateAndTime != null && noReallyThisIsTheTrueDateAndTime.getTime() != 0) {
            time = noReallyThisIsTheTrueDateAndTime.getTime();
        } else if (currentLocation.getTime() != 0) {
            time = currentLocation.getTime();
        }

        mVisit = new Visit(mID, currentLocation, time,
                0, mClient);
        // Сохраняем координату и привязываем к визиту
        try {
            MyApp app = (MyApp) getActivity().getApplication();
            res = LocationMetadata.insert(app.getDB(), App.get(getActivity()).getCodeAgent(),
                    System.currentTimeMillis(),
                    (float) currentLocation.getLatitude(),
                    (float) currentLocation.getLongitude(),
                    currentLocation.getAccuracy(),
                    currentLocation.getSpeed(),
                    currentLocation.getProvider(),
                    mClient.getCode(), false);
            mVisit.locationID(res);
            mVisit.insert(app.getDB());
        } catch (Exception e) {
            Log.e(TAG, "Не удалось сохранить визит");
        }
        if (currentLocation.getProvider().equals(FAIL)) {
            startVisitFragment(null, res);
        } else if (address == null || address.equals("")) {
            startVisitFragment("Широта: " + currentLocation.getLatitude() + ", " +
                    "Долгота: " + currentLocation.getLongitude(), res);
        } else {
            startVisitFragment(address, res);
        }
    }


    private class AnimationView {

        private View view;
        private int duration;

        AnimationView(View view, int duration) {
            this.view = view;
            this.duration = duration;
        }

        View getView() {
            return view;
        }

        int getDuration() {
            return duration;
        }
    }

}
