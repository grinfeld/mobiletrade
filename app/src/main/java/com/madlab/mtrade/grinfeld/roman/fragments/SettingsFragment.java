package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.Locale;


public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "#SettingsFragment";
    private EditTextPreference editTextPreferencePort;
    private EditTextPreference editTextPreferencePhotoPort;
    private EditTextPreference editTextPreferenceGPSPort;
    private EditTextPreference editTextPreferenceReservServer;
    private Preference export_data;
    private Context sContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        editTextPreferencePort = (EditTextPreference) getPreferenceManager().findPreference(getString(R.string.pref_serverPort));
        editTextPreferencePhotoPort = (EditTextPreference) getPreferenceManager().findPreference(getString(R.string.pref_serverPortPhoto));
        editTextPreferenceGPSPort = (EditTextPreference) getPreferenceManager().findPreference(getString(R.string.pref_gpsPort));
        editTextPreferenceReservServer = (EditTextPreference) getPreferenceManager().findPreference(getString(R.string.pref_ftp_serverA));
        export_data = getPreferenceManager().findPreference(getString(R.string.pref_export_data));
        if (export_data != null) {
            export_data.setOnPreferenceClickListener(preference -> {
                alertForExportData();
                return true;
            });
        }
    }


    private void alertForExportData() {
        final AlertDialog.Builder alert = new AlertDialog.Builder(sContext);
        alert.setTitle("Внимание");
        alert.setMessage("Экспорт базы данных необходим только по просьбе технической поддержки");
        alert.setNegativeButton("Отмена", (dialogInterface, i) -> dialogInterface.dismiss());
        alert.setPositiveButton("Экспорт", (dialog, which) -> copyDatabaseSdcardAndExportToMail(sContext));
        alert.show();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sContext = getActivity();
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        setHasOptionsMenu(true);
        if (view != null) {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener((v, keyCode, event) -> {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.beginTransaction()
                            .replace(R.id.contentMain, new StatisticsFragment())
                            .commit();
                    return true;
                }
                return false;
            });
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_setting, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_redo_request:
                if (App.onChangeSetting != null)
                    App.onChangeSetting.onChangeSetting();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        App.get(getActivity());
        SharedPreferences sharedPreferences = getPreferenceManager().getSharedPreferences();
        sharedPreferences.registerOnSharedPreferenceChangeListener(App.listener);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        App.get(getActivity());
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(App.listener);
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals("ftpServer")) {
            switch (prefs.getString("ftpServer", "")) {
                case "mtrade.spb.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.spb.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.khv.lazomilk.ru":
                    setReserveServer(prefs, "mtrade1.khv.lazomilk.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.ufa.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.ufa.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.ul.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.ul.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.tol.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.tol.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.srt.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.srt.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.pnz.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.pnz.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.oren.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.oren.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.msk.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.msk.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.kd.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.kd.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.kz.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.kz.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.vlg.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.vlg.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
                case "mtrade.nvs.dalimo.ru":
                    setPort(prefs, 51000);
                    setPhotoPort(prefs, 50999);
                    setGPSPort(prefs, 51001);
                    setReserveServer(prefs, "mtrade1.nvs.dalimo.ru");
                    break;
                case "mtrade.chel.dalimo.ru":
                    setPort(prefs, 41000);
                    setPhotoPort(prefs, 40999);
                    setGPSPort(prefs, 41001);
                    setReserveServer(prefs, "mtrade1.chel.dalimo.ru");
                    break;
                case "mtrade.vrn.dalimo.ru":
                    setPort(prefs, 21000);
                    setPhotoPort(prefs, 20999);
                    setGPSPort(prefs, 21001);
                    setReserveServer(prefs, "mtrade1.vrn.dalimo.ru");
                    break;
                case "mtrade.simf.dalimo.ru":
                    setPort(prefs, 31000);
                    setPhotoPort(prefs, 30999);
                    setGPSPort(prefs, 31001);
                    setReserveServer(prefs, "mtrade1.simf.dalimo.ru");
                    break;
                case "mtrade.dalimo.ru":
                    setReserveServer(prefs, "mtrade1.dalimo.ru");
                    setDefaultPort(prefs);
                    break;
            }
        }
    }

    private void setDefaultPort(SharedPreferences prefs) {
        setPort(prefs, 11000);
        setPhotoPort(prefs, 10999);
        setGPSPort(prefs, 11001);
    }


    private void setReserveServer(SharedPreferences prefs, String value) {
        prefs.edit().putString(sContext.getString(R.string.pref_ftp_serverA), value).apply();
        App.get(sContext).setReserveServer(value);
        editTextPreferenceReservServer.setText(value);
    }

    private void setPort(SharedPreferences prefs, int port) {
        prefs.edit().putString(sContext.getString(R.string.pref_serverPort), String.valueOf(port)).apply();
        App.get(sContext).setServerPort(port);
        editTextPreferencePort.setText(String.valueOf(port));
    }

    private void setPhotoPort(SharedPreferences prefs, int port) {
        prefs.edit().putString(sContext.getString(R.string.pref_serverPortPhoto), String.valueOf(port)).apply();
        App.get(sContext).serverPortPhoto(port);
        editTextPreferencePhotoPort.setText(String.valueOf(port));
    }

    private void setGPSPort(SharedPreferences prefs, int port) {
        prefs.edit().putString(sContext.getString(R.string.pref_gpsPort), String.valueOf(port)).apply();
        App.get(sContext).setGPSPort(port);
        editTextPreferenceGPSPort.setText(String.valueOf(port));
    }

    public void copyDatabaseSdcardAndExportToMail(Context c) {
        String databasePath = c.getDatabasePath("mtrade.db").getPath();
        File f = new File(databasePath);
        OutputStream myOutput = null;
        InputStream myInput = null;
        if (f.exists()) {
            try {
                File directory = new File("/mnt/sdcard");
                if (!directory.exists())
                    directory.mkdir();
                myOutput = new FileOutputStream(directory.getAbsolutePath()
                        + "/" + "mtrade.db");
                myInput = new FileInputStream(databasePath);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }

                myOutput.flush();
            } catch (Exception e) {
            } finally {
                try {
                    if (myOutput != null) {
                        myOutput.close();
                    }
                    if (myInput != null) {
                        myInput.close();
                    }
                    File file = new File("/mnt/sdcard/mtrade.db");
                    Uri path = Uri.fromFile(file);
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("vnd.android.cursor.dir/email");
                    String to[] = {"mdev@mad-lab.pro"};
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
                    emailIntent.putExtra(Intent.EXTRA_STREAM, path);
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));
                } catch (Exception e) {
                }
            }
        }
    }
}

