package com.madlab.mtrade.grinfeld.roman.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.FireBaseMessageExpAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.FireBaseNotification;
import com.madlab.mtrade.grinfeld.roman.iface.IChildClickFireBaseFragment;
import com.madlab.mtrade.grinfeld.roman.services.MyFirebaseMessagingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder.buildLogin;
import static com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder.buildNotifications;

/**
 * Created by GrinfeldRA on 26.02.2018.
 */

public class FragmentFireBaseMessageTreeView extends Fragment implements  ExpandableListView.OnChildClickListener {

    public static final String WEB = "web";
    public static final String ODIN_C = "1c";
    public static final String MEscort = "MEscort";
    public static final String DALIMO = "dalimo";
    private static final String TAG = "#FragmentFireBaseMessageTreeView";

    private ExpandableListView expandableListView;
    private Map<String, List<FireBaseNotification>> notificationsGroup;
    private FireBaseMessageExpAdapter adapter;
    private ArrayList<FireBaseNotification> notificationsListWeb = new ArrayList<>();
    private ArrayList<FireBaseNotification> notificationsList1c = new ArrayList<>();
    private ArrayList<FireBaseNotification> notificationsListMEscort = new ArrayList<>();
    private ArrayList<FireBaseNotification> notificationsListDalimo = new ArrayList<>();
    private IChildClickFireBaseFragment iChildClickFireBaseFragment;
    private ProgressDialog dialog;
    private String region;
    private String code;
    private Handler mHandler;
    private String token;
    private String id = null;
    private String tag = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        iChildClickFireBaseFragment = (IChildClickFireBaseFragment)getFragmentManager().findFragmentById(R.id.contentMain);
        View rootView = inflater.inflate(R.layout.fragment_firebase_message,container,false);

        if (getArguments()!=null){
            id = (String) getArguments().get(MyFirebaseMessagingService.ID_MESSAGE);
            tag = (String) getArguments().get(MyFirebaseMessagingService.TAG_MESSAGE);
        }

        expandableListView = rootView.findViewById(R.id.exListView);
        notificationsGroup = new LinkedHashMap<>();
        notificationsGroup.put(WEB, notificationsListWeb);
        notificationsGroup.put(ODIN_C, notificationsList1c);
        notificationsGroup.put(MEscort, notificationsListMEscort);
        notificationsGroup.put(DALIMO, notificationsListDalimo);



        adapter = new FireBaseMessageExpAdapter(getActivity(),notificationsGroup);
        expandableListView.setAdapter(adapter);

        region = App.getRegion(getActivity());
        code = App.get(getActivity()).getCodeAgent();

        mHandler = new Handler(Looper.getMainLooper());

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.show();

        buildLogin().enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(TAG, e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String strResponse = response.body().string();
                    JSONObject jsonObject = new JSONObject(strResponse);
                    token = jsonObject.getString("token");
                    Log.d(TAG, token+" "+code+" "+region);
                    buildNotifications(token,code, region).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            mHandler.post(() -> dialog.dismiss());
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            try {
                                final List<FireBaseNotification> fireBaseNotifications = new ArrayList<>();
                                String strResponse = response.body().string();
                                JSONObject jsonObject = new JSONObject(strResponse);
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for (int i = 0; i < jsonArray.length(); i ++) {
                                    JSONObject json = jsonArray.getJSONObject(i);
                                    int id = json.getInt("id");
                                    String title = json.getString("title");
                                    String body = json.getString("body");
                                    String tag = json.getString("tag");
                                    int read = json.getInt("read");
                                    long date = json.getLong("date");
                                    fireBaseNotifications.add(new FireBaseNotification(id, title, body, tag, read, date));
                                }
                                mHandler.post(() -> {
                                    addNotificationForAdapter(fireBaseNotifications);
                                    dialog.dismiss();

                                });
                            } catch (JSONException e) {
                                Log.d(TAG, e.getMessage());
                                dialog.dismiss();
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        expandableListView.setOnChildClickListener(this);
        return rootView;
    }


    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
        FireBaseNotification notification = notificationsGroup.get(getKey(i)).get(i1);
        iChildClickFireBaseFragment.onClick(notification, token, adapter);
        int index = expandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForChild(i, i1));
        expandableListView.setItemChecked(index, true);
        return false;
    }


    private void addNotificationForAdapter(List<FireBaseNotification> notifications){
        FireBaseNotification baseNotification = null;
        if (notifications != null) {
            for (FireBaseNotification notification : notifications){
                if (id!=null&&!id.equals("0")){
                    if (id.equals(String.valueOf(notification.getId()))){
                        baseNotification = notification;
                    }
                }
                switch (notification.getTag()){
                    case WEB:
                        notificationsListWeb.add(notification);
                        notificationsGroup.put(WEB, notificationsListWeb);
                        break;
                    case ODIN_C:
                        notificationsList1c.add(notification);
                        notificationsGroup.put(ODIN_C, notificationsList1c);
                        break;
                    case MEscort:
                        notificationsListMEscort.add(notification);
                        notificationsGroup.put(MEscort, notificationsListMEscort);
                        break;
                    case DALIMO:
                        notificationsListDalimo.add(notification);
                        notificationsGroup.put(DALIMO, notificationsListDalimo);
                        break;
                }
            }
            sortList(notificationsListWeb);
            sortList(notificationsList1c);
            sortList(notificationsListMEscort);
            sortList(notificationsListDalimo);
            adapter.notifyDataSetChanged();
            if (baseNotification!=null || (id!=null&&id.equals("0"))){
                int positionGroup = 0;
                if (tag!=null){
                    positionGroup = getIntPositionFromTag(tag);
                }
                expandableListView.expandGroup(positionGroup);
                int index = expandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForChild(positionGroup, 0));
                expandableListView.setItemChecked(index, true);

                if (id.equals("0")){
                    baseNotification = notificationsGroup.get(getKey(positionGroup)).get(0);
                }

                iChildClickFireBaseFragment.onClick(baseNotification, token, adapter);
            }
        }
    }

    private void sortList(ArrayList<FireBaseNotification> list){
        Collections.sort(list, (fireBaseNotification, t1) -> Long.valueOf(t1.getDate()).compareTo(fireBaseNotification.getDate()));
    }

    private int getIntPositionFromTag(String tag){
        switch (tag){
            case WEB:
                return 0;
            case ODIN_C:
                return  1;
            case MEscort:
                return 2;
            case DALIMO:
                return 3;
            default:
                return 0;
        }
    }

    private String getKey(int position){
        String key = null;
        switch (position){
            case 0:
                key = WEB;
                break;
            case 1:
                key = ODIN_C;
                break;
            case 2:
                key = MEscort;
                break;
            case 3:
                key = DALIMO;
        }
        return key;
    }


}
