package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.iface.IGetStatus;


public class GetOrderListTask extends AsyncTask<String, Integer, Boolean> {
	private final static String TAG = "!->GetOrderListTask";
	private final static String COMMAND = "ЗаказыВОперативке";//"ORDERS_LIST";

	// private Context mContext;
	private String mAgentCode;
	private Date mRequestDate;
	private IGetStatus taskCompleteListener;
	private ArrayList<com.madlab.mtrade.grinfeld.roman.entity.Status> mItems;

	private DalimoClient client;
	private Credentials connectInfo;
	private String mFilePath;

	private String _1CUnswer = null;
	private String mUnswer = "";

	public GetOrderListTask(Credentials info, String agent, Date requestDate, String orderListFilePath, IGetStatus listener) {
		connectInfo = info;
		mAgentCode = agent;
		if (requestDate != null) {
			mRequestDate = requestDate;
		} else {
			mRequestDate = new Date(System.currentTimeMillis());
		}
		mFilePath = orderListFilePath;
		taskCompleteListener = listener;
	}

	@Override
	protected void onPreExecute() {
		// Тут нам надо создать прогрессДиалог
	}

	@Override
	protected Boolean doInBackground(String... params) {

		if (params!=null){
			_1CUnswer = params[0];
		}
		if (_1CUnswer == null){
			String query = String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s","COMMAND", mAgentCode,COMMAND, mAgentCode,
					TimeFormatter.sdf1C.format(mRequestDate), Const.END_MESSAGE);
			client = new DalimoClient(connectInfo);
			if (!client.connect()) {
				mUnswer = "Не удалось подключиться";
				return false;
			}

			client.send(query);

			_1CUnswer = client.receive();

			client.disconnect();
		}


		if (_1CUnswer == null || _1CUnswer.contains(DalimoClient.ERROR)) {
			mUnswer = _1CUnswer;
			return false;
		}

		//// Во втором потоке запишем в файл
		new Runnable() {
			@Override
			public void run() {
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(mFilePath);
					OutputStreamWriter writer = new OutputStreamWriter(fos);
					writer.write(_1CUnswer);
					writer.close();
				} catch (Exception e) {
					Log.e(TAG, e.toString());
				} finally {
					if (fos != null) {
						try {
							fos.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}.run();

		try {
			mItems = com.madlab.mtrade.grinfeld.roman.entity.Status.loadOrdersFromString(_1CUnswer);
		} catch (Exception e) {
			mItems = null;
			mUnswer = e.toString();
			return false;
		}

		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (taskCompleteListener != null) {
			taskCompleteListener.onTaskComplete(this);
		}
	}

	public String getUnswer() {
		return mUnswer;
	}

	public ArrayList<com.madlab.mtrade.grinfeld.roman.entity.Status> getItems() {
		return mItems;
	}

}
