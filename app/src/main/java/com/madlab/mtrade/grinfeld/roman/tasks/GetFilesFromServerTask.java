package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.iface.IGetFilesComplete;

import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

public class GetFilesFromServerTask extends AsyncTask<String, Integer, Boolean> {
    private final String TAG = "!->GetFilesFromServer";
    private static final String ARCH_EXT = ".zip";

    // public
    public static final byte RESULT_SUCCESS = 10;
    public static final byte RESULT_FAIL = 11;

    // private
    private ProgressDialog mProgressDlg;
    private Context baseContext;
    private IGetFilesComplete mListener;

    private short delay = 60;
    private Timer mTimer;
    private boolean mTimeout = false;

    public boolean isTimeout() {
        return mTimeout;
    }

    private String mUnswer;

    public String getMessage() {
        return mUnswer;
    }

    public GetFilesFromServerTask(Context context, IGetFilesComplete handler) {
        baseContext = context;
        mListener = handler;
    }

    @Override
    protected void onPreExecute() {
        mProgressDlg = new ProgressDialog(baseContext);
        mProgressDlg.setMessage("Дождитесь окончания загрузки");
        mProgressDlg.setCancelable(false);
        mProgressDlg.show();
        mTimer = new Timer();
    }

    @Override
    protected Boolean doInBackground(String... files) {
//         progress.setMax(files.length);
//         int mProgress = 0;
//         publishProgress(mProgress);
        mUnswer = "";
        boolean success = true;
        for (String item : files) {
            App app = App.get(baseContext);
            String path = String.format("%s%s%s", App.get(baseContext).getImpExpPath(), File.separator, item + ARCH_EXT);
            if (app.connectionServer.equals("1")) {
                try {
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                            mTimer.cancel();
                            mTimeout = true;
                            mUnswer = "Превышено время ожидания. Повторите попытку";
                            cancel();
                        }
                    };
                    mTimer.schedule(task, delay * 1000);
                } catch (Exception e) {
                    Log.e(TAG, e.toString());
                }

                // Коннектимся
                Credentials connectInfo = Credentials.load(baseContext);
                DalimoClient client = new DalimoClient(connectInfo);

                if (!client.connect()) {
                    mUnswer = "Не удалось подключиться";
                    success = false;
                    break;
                }

                // item = FILES1
                // Получаем
                String sectionEC = baseContext.getString(R.string.pref_exclusive);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext.getApplicationContext());
                String exclusive = sharedPreferences.getString(sectionEC, "");
                String message = String.format("%s;%s;%s;%s", item, App.get(baseContext).getCodeAgent(), exclusive, Const.END_MESSAGE);
                client.send(message);
                Log.d(TAG, "message: " + message);

                boolean resGetFile = client.receiveFile(path);
                Log.d(TAG, "resGetFile: " + resGetFile);
                // отключаемся
                client.disconnect();
                if (!resGetFile) {
                    mUnswer += client.LastError() + Const.NewLine;
                    success = false;
                    break;
                } else {
                    mUnswer += "Файл успешно получен" + Const.NewLine;
                }

                if (mTimer != null) mTimer.cancel();
                // mProgress++;
                // publishProgress(mProgress);
            } else {
                try {
                    Response response = OkHttpClientBuilder.getFiles(App.getRegion(baseContext),App.get(baseContext).getCodeAgent()).execute();
                    if (response.isSuccessful()){
                        File downloadedFile = new File(path);
                        BufferedSink sink = Okio.buffer(Okio.sink(downloadedFile));
                        sink.writeAll(response.body().source());
                        sink.close();
                    }else {
                        try {
                            TimerTask task = new TimerTask() {
                                @Override
                                public void run() {
                                    mTimer.cancel();
                                    mTimeout = true;
                                    mUnswer = "Превышено время ожидания. Повторите попытку";
                                    cancel();
                                }
                            };
                            mTimer.schedule(task, delay * 1000);
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }

                        // Коннектимся
                        Credentials connectInfo = Credentials.load(baseContext);
                        DalimoClient client = new DalimoClient(connectInfo);

                        if (!client.connect()) {
                            mUnswer = "Не удалось подключиться";
                            success = false;
                            break;
                        }

                        // item = FILES1
                        // Получаем
                        String sectionEC = baseContext.getString(R.string.pref_exclusive);
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(baseContext.getApplicationContext());
                        String exclusive = sharedPreferences.getString(sectionEC, "");
                        String message = String.format("%s;%s;%s;%s", item, App.get(baseContext).getCodeAgent(), exclusive, Const.END_MESSAGE);
                        client.send(message);
                        Log.d(TAG, "message: " + message);

                        boolean resGetFile = client.receiveFile(path);
                        Log.d(TAG, "resGetFile: " + resGetFile);
                        // отключаемся
                        client.disconnect();
                        if (!resGetFile) {
                            mUnswer += client.LastError() + Const.NewLine;
                            success = false;
                            break;
                        } else {
                            mUnswer += "Файл успешно получен" + Const.NewLine;
                        }

                        if (mTimer != null) mTimer.cancel();
                    }
                } catch (IOException e) {
                    mUnswer = "Ошибка " + e.getMessage();
                    return false;
                }
            }
        }
        return success;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (mProgressDlg != null) {
            mProgressDlg.setProgress(values[0]);
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mTimer != null) mTimer.cancel();
        dismiss();
        if (mListener != null) {
            mListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (mProgressDlg != null) {
            mProgressDlg.hide();
            mProgressDlg.dismiss();
        }
    }

}