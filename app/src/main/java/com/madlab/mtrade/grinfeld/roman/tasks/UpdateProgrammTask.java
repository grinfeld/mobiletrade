package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.StringTokenizer;

import org.xmlpull.v1.XmlPullParser;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.FTP;
import com.madlab.mtrade.grinfeld.roman.iface.IUpdateComplete;

public class UpdateProgrammTask extends AsyncTask<Void, String, Byte> {
    private static final String TAG = "!->UpdateProgrammTask";

    private static final String TXT = "Settings2.TXT";
    public static final byte ID = 1;

    /**
     * Работает
     */
    public static final byte IN_PROGRESS = 0;

    /**
     * Произошла ошибка при проверке, требуется повтор
     */
    public static final byte FAIL = 1;

    /**
     * Версия актуальная, нужно сохранить дату проверки
     */
    public static final byte ACTUAL = 2;

    /**
     * Загружена новая версия, нужно запустить обновление
     */
    public static final byte DOWNLOADED = 3;

    public static final String RESULT = "result";
    public static final String MESSAGE = "message";

    private final Resources mResources;
    private FTP mFtp;
    private int mCurrentVersion = 0;
    private final IUpdateComplete mTaskCompleteListener;
    private ProgressBar mProgressBar;

    private Context mBaseContext;
    private ProgressDialog progress;
    // private String mWaitString;

    private String impexpPath;

    private String mResultMessage;

    public UpdateProgrammTask(Context context, IUpdateComplete completeListener, int currentVersion) {
        mTaskCompleteListener = completeListener;
        mBaseContext = context;
        mResources = mBaseContext.getResources();
        // mWaitString = mBaseContext.getString(R.string.mes_wait_for_load);
        mCurrentVersion = currentVersion;

        impexpPath = "/storage/emulated/0/IMPEXP";
    }

    public void setProgressBar(ProgressBar progressBar) {
        mProgressBar = progressBar;
    }

    public String getResultMessage() {
        return mResultMessage;
    }

    @Override
    protected void onPreExecute() {
//        if (mProgressBar != null) {
//            showProgress(true);
//        } else {
             progress = new ProgressDialog(mBaseContext);
             progress.setMessage("Пожалуйста подождите");
             progress.setCancelable(false);
             progress.show();
        //}
    }

    @Override
    protected Byte doInBackground(Void... params) {
        // Настройки нужно брать из файла XML
        XmlPullParser parser = null;
        boolean connected = false;

        try {
            String server = null;
            String login = null;
            String passw = null;

            parser = mResources.getXml(R.xml.internet_settings);
            while (parser.getEventType() != XmlPullParser.END_DOCUMENT
                    && !connected) {
                switch (parser.getEventType()) {
                    case XmlPullParser.START_TAG:
                        // Перебираем узлы с настройками
                        if (parser.getName().equals("setting")) {
                            // Если узел самарский, то пробуем подключиться
                            if (parser.getAttributeValue(0).contains("samara")) {
                                server = parser.getAttributeValue(1);
                                login = parser.getAttributeValue(2);
                                passw = GlobalProc.deCode(parser
                                        .getAttributeValue(3));
                                // mFtp = new FTP(server, login,
                                // "ugolochek",FTP.DEFAULT_PORT);
                                mFtp = new FTP(server, login, passw);
                                Log.d(TAG, server +", "+login+", "+passw);
                                connected = mFtp.connect();
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:

                        break;
                    case XmlPullParser.TEXT:

                        break;
                }

                parser.next();
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        if (!connected) {
            if (mFtp != null) {
                mFtp.disconnect();
            }
            mResultMessage = "Не удалось подключиться к интернет для проверки обновления";
            return FAIL;
        }

        if (!mFtp.login()) {
            mFtp.disconnect();
            mResultMessage = "Не удалось подключиться к серверу обновлений";
            return FAIL;
        }

        // Теперь надо получить файл
        String path = App.get(mBaseContext).getImpExpPath() + File.separator + TXT;

        OutputStream os = null;
        try {
            os = new FileOutputStream(path);

            if (!mFtp.retrieveFile(TXT, os)) {
                mResultMessage = "Ошибка при получении файла";
                return FAIL;
            }
        } catch (Exception e) {
            mResultMessage = "Ошибка: " + e.getMessage();
            return FAIL;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                mResultMessage = "Ошибка: " + e.getMessage();
                return FAIL;
            }
        }

        // Читаем
        FileInputStream fis = null;
        InputStreamReader iStr = null;
        int version = 0;
        try {
            fis = new FileInputStream(path);
            iStr = new InputStreamReader(fis, Const.WIN1251);
            final BufferedReader br = new BufferedReader(iStr);
            String line = br.readLine();

            StringTokenizer token = new StringTokenizer(line, ";");
            byte i = 0;
            while (token.hasMoreTokens()) {
                switch (i) {
                    case 0:
                        GlobalProc.setPref(mBaseContext, R.string.pref_catalog_url,
                                token.nextToken());
                        break;
                    case 1:
                        String ver = token.nextToken();
                        try {
                            version = Integer.parseInt(ver
                                    .replaceAll("[^0-9,]", ""));
                        } catch (Exception e) {
                            version = 0;
                        }
                        break;
                }
                ++i;
            }
        } catch (Exception e) {
            if (mFtp != null) {
                mFtp.disconnect();
            }
        } finally {
            try {
                if (iStr != null) {
                    iStr.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {

            }
        }

        if (version <= mCurrentVersion) {
            mResultMessage = "Нет доступных обновлений";
            return ACTUAL;
        }

        // Качаем обновление
        path = App.get(mBaseContext).getImpExpPath() + File.separator + Const.ZIP;
        os = null;
        try {
            os = new FileOutputStream(path);

            if (!mFtp.retrieveFile(Const.ZIP, os)) {
                mResultMessage = "Ошибка при получении файла обновления";
                return FAIL;
            }

            if (!GlobalProc.unpack(impexpPath, impexpPath, Const.ZIP)) {
                mResultMessage = "Ошибка при распаковке архива";
                return FAIL;
            }
        } catch (Exception e) {
            mResultMessage = "Ошибка: " + e.getMessage();
            return FAIL;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                mResultMessage = "Ошибка: " + e.getMessage();
                return FAIL;
            }
        }

        return DOWNLOADED;
    }

    @Override
    protected void onPostExecute(Byte result) {
        if (mFtp != null) {
            mFtp.disconnect();
        }
        dismiss();
        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (progress != null) {
            progress.dismiss();
            progress.hide();
        }
    }

    private void showProgress(boolean value) {
        if (mProgressBar != null) {
            mProgressBar.setVisibility(value ? View.VISIBLE : View.INVISIBLE);
        }
    }
}