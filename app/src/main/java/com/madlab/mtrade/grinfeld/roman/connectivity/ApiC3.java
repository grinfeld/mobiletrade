package com.madlab.mtrade.grinfeld.roman.connectivity;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;

/**
 * Created by grinfeldra
 */
public interface ApiC3 {

    @POST("s3")
    Call<ResponseBody> sendPhoto(@QueryMap Map<String, String> params, @Body RequestBody body);

    @PUT("s3")
    Call<ResponseBody> changeAccess(@QueryMap Map<String, String> params, @Body Map<String, String> body);

}
