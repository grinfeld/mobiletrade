package com.madlab.mtrade.grinfeld.roman.connectivity;

/**
 * Created by GrinfeldRA on 12.07.2018.
 */

public class DalimoClientException extends Exception {
    public DalimoClientException(Throwable cause) {
        super(cause);
    }
}
