package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class OrderMetaData implements BaseColumns {
    private OrderMetaData() {
    }

    public static final String TABLE_NAME = "Orders";

    public static final String DEFAULT_SORT_ORDER = "modified DESC";

    public static final String FIELD_NUM = "NumOrder";
    public static final String FIELD_STATE = "OrderState";
    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CLIENT = "CodeCli";
    public static final String FIELD_DATE_SHIP = "DateShip";
    public static final String FIELD_TYPE_PAY = "TypePay1";
    public static final String FIELD_TYPE_DOC = "TypePay2";
    public static final String FIELD_TYPE_MONEY = "TypePay3";
    public static final String FIELD_TYPE_VISIT = "TypeDo";
    public static final String FIELD_GET_MONEY = "GetReturn";
    public static final String FIELD_VETERINAR = "Veterinar";
    public static final String FIELD_TOTAL_WEIGHT = "TotalWeight";
    public static final String FIELD_TOTAL_SUM = "TotalSum";
    public static final String FIELD_NOTE = "Note";
    public static final String FIELD_RESERVED = "Reserved";
    public static final String FIELD_QUALITY = "NonLiquid";
    public static final String FIELD_GET_ORDER_TIME = "GetOrderTime";
    public static final String FIELD_WAS_REST_CHECK = "WasCheck";
    public static final String FIELD_WARRANTY = "Warranty";//
    public static final String FIELD_SUM_TO_GET = "SumToGet"; //
    //
    public static final String FIELD_SELF_DELIVERY = "SelfDelivery";
    public static final String FIELD_WAS_AUTOORDER = "WasAutoOrder";//


    public static final String FIELD_VISIT_FK = "VisitFK";
    public static final String FIELD_FIRM_FK = "FirmFK";
    public static final String FIELD_DOC_NUMS = "DocNums";

    public static final String FIELD_TYPE_SERVER = "OrderTypeServer";

    public static final byte FIELD_NUM_INDEX = 0;
    public static final byte FIELD_STATE_INDEX = 1;
    public static final byte FIELD_CODE_AGENT_INDEX = 2;
    public static final byte FIELD_CLIENT_INDEX = 3;
    public static final byte FIELD_DATE_SHIP_INDEX = 4;
    public static final byte FIELD_TYPE_PAY_INDEX = 5;
    public static final byte FIELD_TYPE_DOC_INDEX = 6;
    public static final byte FIELD_TYPE_MONEY_INDEX = 7;
    public static final byte FIELD_TYPE_VISIT_INDEX = 8;
    public static final byte FIELD_GET_MONEY_INDEX = 9;
    public static final byte FIELD_VETERINAR_INDEX = 10;
    public static final byte FIELD_TOTAL_WEIGHT_INDEX = 11;
    public static final byte FIELD_TOTAL_SUM_INDEX = 12;
    public static final byte FIELD_NOTE_INDEX = 13;
    public static final byte FIELD_RESERVED_INDEX = 14;
    public static final byte FIELD_QUALITY_INDEX = 15;
    public static final byte FIELD_GET_ORDER_TIME_INDEX = 16;
    public static final byte FIELD_WAS_REST_CHECK_INDEX = 17;
    public static final byte FIELD_WARRANTY_INDEX = 18;
    public static final byte FIELD_SUM_TO_GET_INDEX = 19;
    public static final byte FIELD_SELF_DELIVERY_INDEX = 20;
    public static final byte FIELD_WAS_AUTOORDER_INDEX = 21;
    public static final byte FIELD_VISIT_FK_INDEX = 22;
    public static final byte FIELD_FIRM_FK_INDEX = 23;
    public static final byte FIELD_DOC_NUMS_INDEX = 24;
    public static final byte FIELD_TYPE_SERVER_INDEX = 25;

    public static final String INDEX = String.format(
            "CREATE INDEX idxOrderClient ON %s (%s, %s)", TABLE_NAME,
            FIELD_CODE_AGENT, FIELD_CLIENT);

    public static final String CREATE_COMMAND = String
            .format("CREATE TABLE %s ("
                            + "%s	int             NOT NULL CONSTRAINT PK_Orders PRIMARY KEY,"
                            + "%s	nchar(1)		NOT NULL DEFAULT 'V',"
                            + "%s	nvarchar(6)		NOT NULL DEFAULT '',"
                            + "%s	nvarchar(6)		NOT NULL DEFAULT '',"
                            + "%s	datetime    	NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(50)	NOT NULL DEFAULT '',"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(5)		NOT NULL DEFAULT '00:00',"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	money			NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	nvarchar(36)	NOT NULL DEFAULT '',"
                            + "%s	smallint		NOT NULL DEFAULT 0,"
                            + "%s	TEXT			NOT NULL DEFAULT '',"
                            + "%s	INTEGER			NOT NULL DEFAULT 0)", TABLE_NAME,
                    FIELD_NUM, FIELD_STATE, FIELD_CODE_AGENT, FIELD_CLIENT,
                    FIELD_DATE_SHIP, FIELD_TYPE_PAY, FIELD_TYPE_DOC,
                    FIELD_TYPE_MONEY, FIELD_TYPE_VISIT, FIELD_GET_MONEY,
                    FIELD_VETERINAR, FIELD_TOTAL_WEIGHT, FIELD_TOTAL_SUM,
                    FIELD_NOTE, FIELD_RESERVED, FIELD_QUALITY,
                    FIELD_GET_ORDER_TIME, FIELD_WAS_REST_CHECK, FIELD_WARRANTY,
                    FIELD_SUM_TO_GET, FIELD_SELF_DELIVERY, FIELD_WAS_AUTOORDER,
                    FIELD_VISIT_FK, FIELD_FIRM_FK, FIELD_DOC_NUMS, FIELD_TYPE_SERVER);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;


    public static String Clear(short numOrder) {
        return String.format("DELETE FROM %s WHERE %s = %d", TABLE_NAME,
                FIELD_NUM, numOrder);
    }

}