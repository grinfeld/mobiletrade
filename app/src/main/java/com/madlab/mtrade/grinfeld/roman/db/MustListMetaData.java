package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class MustListMetaData implements BaseColumns {
    private MustListMetaData() {}

    public static final String TABLE_NAME = "MustList";

    public static final String DEFAULT_SORT_ORDER = "modified DESC";

    public static final String FIELD_CODE_AGENT = "CodeAgent";
    public static final String FIELD_CATEGORY = "Category";
    public static final String FIELD_CODE_GOODS = "CodeGoods";
//	public static final String FIELD_SORT_ORDER = "SortOrder";

    public static final byte FIELD_CODE_AGENT_INDEX = 0;
    public static final byte FIELD_CATEGORY_INDEX = 1;
    public static final byte FIELD_CODE_GOODS_INDEX = 2;
//	public static final byte FIELD_SORT_ORDER_INDEX = 3;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(30)	NOT NULL DEFAULT '',"
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '')",
            TABLE_NAME,//+ "%s int NOT NULL DEFAULT 0
            FIELD_CODE_AGENT,
            FIELD_CATEGORY,
            FIELD_CODE_GOODS);//, FIELD_SORT_ORDER

    public static final String INDEX = String.format(
            "CREATE INDEX idxMustListCategoryCodeGoods ON %s ( %s, %s )",
            TABLE_NAME,
            FIELD_CATEGORY,
            FIELD_CODE_GOODS);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static String insertQuery(String agent, String category, String goods) {
        return String.format("INSERT INTO %s ("
                        + "%s, %s, %s) VALUES ("
                        + "'%s', '%s', '%s')",
                TABLE_NAME,
                FIELD_CODE_AGENT, FIELD_CATEGORY, FIELD_CODE_GOODS,
                agent, category, goods);
    }
}
