package com.madlab.mtrade.grinfeld.roman.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.entity.Shipment;

import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 30.01.2018.
 */

public class TransitAdapter extends ArrayAdapter<Shipment> {
    // private Context mContext;
    private ArrayList<Shipment> mList;

    public TransitAdapter(Context context, int textViewResourceId,
                          ArrayList<Shipment> objects) {
        super(context, textViewResourceId, objects);
        // mContext = context;
        mList = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.transit_item, null);
        }

        TextView tvDate = v.findViewById(R.id.tv_left);// R.id.tv_left
        TextView tvCount = v.findViewById(R.id.tv_right);// R.id.tv_right
        Shipment item = mList.get(position);
        if (item != null) {
            if (tvDate != null)
                tvDate.setText(String.format("%d%s", item.getCount(),
                        Const.POSTFIX_PIECE_GOODS));
            if (tvCount != null)
                tvCount.setText(TimeFormatter.sdf1C.format(item.getDateShip()));
        }

        return v;
    }

    @Override
    public Shipment getItem(int position) {
        return mList.get(position);
    }
}