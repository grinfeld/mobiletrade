package com.madlab.mtrade.grinfeld.roman.entity;


import java.util.ArrayList;
import java.util.StringTokenizer;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.FirmsMetaData;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Firm {
    private final static String TAG = "!->Firm";

    short mCode;
    String mName;
    String mINN;
    String mAddress;
    String mAddressFull;
    String mHead;
    String mAccountant;
    String mBank;
    String mPhones;

    public Firm(short code, String name, String iNN, String address,
                String addressFull, String head, String accountant, String bank,
                String phones) {
        mCode = code;
        mName = name;
        mINN = iNN;
        mAddress = address;
        mAddressFull = addressFull;
        mHead = head;
        mAccountant = accountant;
        mBank = bank;
        mPhones = phones;
    }

    public short getCode() {
        return mCode;
    }

    public void setCode(short code) {
        mCode = code;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getINN() {
        return mINN;
    }

    public void setINN(String iNN) {
        mINN = iNN;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getAddressFull() {
        return mAddressFull;
    }

    public void setAddressFull(String addressFull) {
        mAddressFull = addressFull;
    }

    public String getHead() {
        return mHead;
    }

    public void setHead(String head) {
        mHead = head;
    }

    public String getAccountant() {
        return mAccountant;
    }

    public void setAccountant(String accountant) {
        mAccountant = accountant;
    }

    public String getBank() {
        return mBank;
    }

    public void setBank(String bank) {
        mBank = bank;
    }

    public String getPhones() {
        return mPhones;
    }

    public void setPhones(String phones) {
        mPhones = phones;
    }

    @Override
    public String toString() {
        return mName;
    }

    public static Firm load(Context ctx, short id) {
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            return load(app.getDB(), id);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return null;
    }

    public static Firm load(SQLiteDatabase db, short id) {
        String where = String.format("%s = '%d'", FirmsMetaData.FIELD_CODE, id);
        return load(db, where);
    }

    public static Firm load(SQLiteDatabase db, String where) {
        Firm res = null;

        Cursor rows = null;
        try {
            rows = db.query(FirmsMetaData.TABLE_NAME, null, where, null, null,
                    null, null);
            if (rows.moveToFirst()) {
                short code = (short) GlobalProc.parseInt(rows
                        .getString(FirmsMetaData.FIELD_CODE_INDEX));
                String name = rows.getString(FirmsMetaData.FIELD_NAME_INDEX);
                if (name == null || name.length() < 1) name = "<Неизвестный>";

                res = new Firm(code,
                        name,
                        rows.getString(FirmsMetaData.FIELD_INN_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ADDRESS_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ADDRESS_FULL_INDEX),
                        rows.getString(FirmsMetaData.FIELD_CHIEF_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ACCOUNTER_INDEX),
                        rows.getString(FirmsMetaData.FIELD_BANK_INDEX),
                        rows.getString(FirmsMetaData.FIELD_PHONE_INDEX));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return res;
    }

    public static ArrayList<Firm> loadList(SQLiteDatabase db) {
        ArrayList<Firm> result = null;

        Cursor rows = null;
        try {
            rows = db.query(FirmsMetaData.TABLE_NAME, null, null, null, null,
                    null, null);
            if (rows.getCount() > 0) {
                result = new ArrayList<Firm>();
            }
            while (rows.moveToNext()) {
                short code = (short) GlobalProc.parseInt(rows
                        .getString(FirmsMetaData.FIELD_CODE_INDEX));

                Firm newItem = new Firm(code,
                        rows.getString(FirmsMetaData.FIELD_NAME_INDEX),
                        rows.getString(FirmsMetaData.FIELD_INN_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ADDRESS_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ADDRESS_FULL_INDEX),
                        rows.getString(FirmsMetaData.FIELD_CHIEF_INDEX),
                        rows.getString(FirmsMetaData.FIELD_ACCOUNTER_INDEX),
                        rows.getString(FirmsMetaData.FIELD_BANK_INDEX),
                        rows.getString(FirmsMetaData.FIELD_PHONE_INDEX));

                result.add(newItem);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            if (rows != null) {
                rows.close();
            }
        }
        return result;
    }

    public static int[] Parse(String value) {
        int[] result = null;
        StringTokenizer st = new StringTokenizer(value, Const.DELIMITTER);
        if (st.countTokens() > 0) {
            result = new int[st.countTokens()];
            byte counter = 0;
            while (st.hasMoreElements()) {
                int newItem = GlobalProc.parseInt(st.nextElement().toString());
                result[counter] = newItem;
                counter++;
            }
        }

        return result;
    }

}
