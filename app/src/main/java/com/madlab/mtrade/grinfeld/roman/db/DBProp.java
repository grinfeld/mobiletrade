package com.madlab.mtrade.grinfeld.roman.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBProp {

    private static final String TAG = "!->DBProp";

    private static volatile short m_numDoc = 1;
    private static volatile short m_numOrder = 1;

    public static void IncNumDoc() {
        m_numDoc++;
    }

    public static void IncNumOrder() {
        m_numOrder++;
    }

    public static short GetNumDoc() {
        return m_numDoc;
    }

    public static short GetNumOrder() {
        return m_numOrder;
    }

    public static boolean DropNumeration(SQLiteDatabase db) {
        m_numDoc = 1;
        m_numOrder = 1;
        return UpdateDBProp(db);
    }

    public static boolean UpdateDBProp(SQLiteDatabase db) {
        int res = 0;
        try {
			/*
			 * db.execSQL(String.format( "UPDATE %s "+ "SET %s = %d, %s = %d",
			 * DBPropMetaData.TABLE_NAME, DBPropMetaData.FIELD_NUM_DOC,
			 * m_numDoc, DBPropMetaData.FIELD_NUM_ORDER, m_numOrder));
			 */
            ContentValues cv = new ContentValues();

            cv.put(DBPropMetaData.FIELD_NUM_DOC, m_numDoc);
            cv.put(DBPropMetaData.FIELD_NUM_ORDER, m_numOrder);

            res = db.update(DBPropMetaData.TABLE_NAME, cv, null, null);
        } catch (Exception e) {
            return false;
        }

        return res > 0;
    }

    public static boolean LoadDBProp(SQLiteDatabase db) {
        Cursor cursor = null;
        try {
            cursor = db.query(DBPropMetaData.TABLE_NAME, new String[] {
                            DBPropMetaData.FIELD_NUM_DOC,
                            DBPropMetaData.FIELD_NUM_ORDER }, null, null, null, null,
                    null);

            if (cursor != null) {
                cursor.moveToFirst();
            } else {
                return false;
            }

            m_numDoc = cursor.getShort(0);
            m_numOrder = cursor.getShort(1);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return true;
    }

}
