package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.tasks.LoadTablePartTask;

/**
 * Created by GrinfeldRA on 27.12.2017.
 */

public interface ILoadTablePartCompleteListener {

    void onTaskComplete(LoadTablePartTask task);

}
