package com.madlab.mtrade.grinfeld.roman.fragments;


import java.util.ArrayList;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsAdapter;
import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsInDocument;
import com.madlab.mtrade.grinfeld.roman.entity.GoodsList;
import com.madlab.mtrade.grinfeld.roman.entity.Node;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.adapters.GoodsGroupAdapter;
import com.madlab.mtrade.grinfeld.roman.iface.IDynamicToolbar;
import com.madlab.mtrade.grinfeld.roman.iface.ILoadGoodsComplete;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsGroupTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGoodsOnTreeClickTask;
import com.madlab.mtrade.grinfeld.roman.tasks.LoadGroupMatrixTask;

/**
 * Класс, описывающий дерево с группами товаров
 *
 * @author Konovaloff
 */
public class GoodsGroupListFragment extends ListFragment implements ILoadGoodsComplete {
    public final static String TAG = "!->GoodsGroupListFragment";

    public final static String EXTRA_MODE_LOAD = "LoadMode";
    public final static String EXTRA_GROUP_SELECTED = "GoodsGroupSelected";
    public final static String EXTRA_CLIENT = "ClientSelected";
    public final static String EXTRA_EXISTED_ITEMS = "mExistedItems";
    private static final String EXTRA_LIST_VIEW_MODE = "listViewMode";

    //	private GoodsList mGoodsList;
    // private OnNodeSelected mCallbacks;
    private ArrayList<Node> mGoodsTree;

    /**
     * Режим загрузки товаров ВСЕ, МАТРИЦА, НЕЛИКВИД
     */
    private byte mModeLoadGoods = GoodsList.MODE_ALL;
    private Node mLastSelected = null;
    private Client mClient = null;
    private ArrayList<GoodsInDocument> mExistedItems;
    private byte mListViewMode = GoodsAdapter.MODE_REFERENCE;
    /**
     * Адаптер для дерева
     */
    private GoodsGroupAdapter mNodesAdapter;
    private ArrayList<Node> mFlatList;
    private String errorLoad;

    private boolean isTablet = false;

    //Сюда нужно передать выбранный ранее товар для того, чтобы развернуть группу
    //в которой он находится
    //Ещё либо передавать список групп (предпочтителен), либо передавать параметры для загрузки
    //этого списка. Нам понадобятся:
    //1. Код клиента для загрузки матрицы,
    //2. Режим загрузки (матрица-не матрица)
    //А пока оставим вариант с загрузкой списка групп во фрагменте
    //Мне не нравится, хочу перенести загрузку в хост-активность
    public static GoodsGroupListFragment newInstance(byte loadGoodsTask_MODE, Node node, Client client, ArrayList<GoodsInDocument> mExistedItems, byte mListViewMode, String tagPrice) {
        Bundle args = new Bundle();
        GoodsGroupListFragment fragment = new GoodsGroupListFragment();
        args.putByte(EXTRA_MODE_LOAD, loadGoodsTask_MODE);
        args.putParcelable(EXTRA_GROUP_SELECTED, node);
        args.putParcelable(EXTRA_CLIENT, client);
        args.putParcelableArrayList(EXTRA_EXISTED_ITEMS, mExistedItems);
        args.putByte(EXTRA_LIST_VIEW_MODE, mListViewMode);
        args.putString(MainActivity.PRICE, tagPrice);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
        //mCallbacks = (OnNodeSelected)getFragmentManager().findFragmentById(R.id.contentMain);

        // Показываем стрелку для возврата сверху, если есть родительская
        // активность
        errorLoad = getString(R.string.err_load_metadata);

        mModeLoadGoods = getArguments().getByte(EXTRA_MODE_LOAD);

        //Смотрим, передавалась ли сюда группа, которую надо выделить
        if (getArguments().containsKey(EXTRA_GROUP_SELECTED)) {
            mLastSelected = getArguments().getParcelable(EXTRA_GROUP_SELECTED);
        }
//
        // Смотрим, передавалась ли сюда клиент
        if (getArguments().containsKey(EXTRA_CLIENT)) {
            mClient = getArguments().getParcelable(EXTRA_CLIENT);
        }
        if (getArguments().containsKey(EXTRA_EXISTED_ITEMS)) {
            mExistedItems = getArguments().getParcelableArrayList(EXTRA_EXISTED_ITEMS);
        }
        if (getArguments().containsKey(EXTRA_LIST_VIEW_MODE)) {
            mListViewMode = getArguments().getByte(EXTRA_LIST_VIEW_MODE);
        }
        // Если мы тут в первый раз
        if (savedInstanceState == null) {
            startLoadGoodsGroupTask(mModeLoadGoods, mClient, mLastSelected);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        menu.clear();
        ((IDynamicToolbar)getActivity()).onChangeToolbar(null);
    }


    public void startLoadGoodsGroupTask(byte mode, Client client, Node selected) {
        String codeClient = client == null ? null : client.getCode();
        LoadGoodsGroupTask loadGoodsTask = new LoadGoodsGroupTask(getActivity(), this, mode, selected, codeClient);
        loadGoodsTask.setTree(mGoodsTree);
        loadGoodsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void notifyTreeAdapter() {
        if (mNodesAdapter != null) {
            mNodesAdapter.notifyDataSetChanged();
        }
    }

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = super.onCreateView(inflater, container, savedInstanceState);
        if (container != null && container.getParent() != null) {
            ViewGroup viewGroup = (ViewGroup) container.getParent();
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View view = viewGroup.getChildAt(i);
                if (view.getId() == R.id.detailFragmentContainer) {
                    isTablet = true;
                }
            }
        }
        return v;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mNodesAdapter == null)
            waitCursor(true);
        else {
            waitCursor(false);
            setListAdapter(mNodesAdapter);
        }
    }

    private void waitCursor(boolean value) {
        setListShown(!value);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Node selected = mNodesAdapter.getItem(position);
        if (selected == null) {
            return;
        }
        selected.toggle();
        selected.setSelection();
        mLastSelected = selected;

        //При сворачивании узла надо перестроить список
        //Разворачивание обрабатываем в хост-активности
        if (!selected.isExpanded()) {
            updateAdapter();
        }
        if (GoodsListFragment.searchItem != null) {
            if (GoodsListFragment.searchItem.isActionViewExpanded()) {
                GoodsListFragment.searchItem.collapseActionView();
            }
        }
        //mSelectedNode = node;
        GoodsGroupListFragment gglf;
        gglf = (GoodsGroupListFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentContainer);
        // Если мы разворачиваем узел и там нет загруженных дочерних элементов
        if (selected.isExpanded()) {
            // загружаем список групп
            if (selected.getChild() == null || selected.getChild().size() < 1 && gglf != null) {
                Log.d(TAG, "startLoadGoodsGroupTask2");
                gglf.startLoadGoodsGroupTask(mModeLoadGoods, mClient, selected);
            } else {// Обновляем картинку
                if (gglf != null) {
                    gglf.updateAdapter();
                }
            }
        }
        if (!isTablet) {
            // Если группа содержит товары, то создаём фрагмент со списком.
            // Если фрагмент существует - обновляем контент
            MyApp app = (MyApp) getActivity().getApplicationContext();
            if (GoodsList.CountInGroup(app.getDB(), selected.getCode()) > 0) {
                // Заменяем фрагмент списка групп на список товаров
                GoodsListFragment glf = GoodsListFragment.newInstance(
                        mListViewMode, (byte) 0, mLastSelected, mModeLoadGoods, mClient, mExistedItems, getArguments().getString(MainActivity.PRICE));
                FragmentManager fm = getFragmentManager();
                fm.beginTransaction().replace(R.id.fragmentContainer, glf, GoodsListFragment.TAG)
                        .addToBackStack(GoodsListFragment.TAG).commit();
            }
        } else {
            startLoadGoodsInGoodsListFragment(mModeLoadGoods, mLastSelected);
        }
        mNodesAdapter.notifyDataSetChanged();
    }


    private void startLoadGoodsInGoodsListFragment(byte mType, Node mSelectedNode) {
        byte modeLoadGoods = GoodsList.MODE_ALL;
        switch (mType) {
            case GoodsList.MODE_MATRIX:
                modeLoadGoods = GoodsList.MODE_MATRIX;
                break;
        }
        Fragment old = getFragmentManager().findFragmentById(R.id.detailFragmentContainer);
        String codeCli = mClient != null ? mClient.getCode() : "";
        String mlCat = mClient != null ? mClient.Category() : "";
        try {
            if (old != null)
                ((GoodsListFragment) old).updateItems(mSelectedNode, codeCli,
                        mlCat, GoodsListFragment.modeRadio, modeLoadGoods);
            else {
                old = getFragmentManager().findFragmentById(R.id.fragmentContainer);
                ((GoodsListFragment) old).updateItems(mSelectedNode, codeCli,
                        mlCat, GoodsListFragment.modeRadio, modeLoadGoods);
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private ArrayList<Node> createFlatList(ArrayList<Node> goodsTree) {
        if (goodsTree == null) return null;

        ArrayList<Node> result = new ArrayList<Node>();
        for (Node node : goodsTree) {
            result.add(node);
            if (node.isExpanded()) {
                result.addAll(createFlatList(node.getChild()));
            }
        }
        return result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    /**
     * Меняет видимость дерева групп товаров
     * /@param makeVisible - если true, делает видимым, иначе - прячет
     */
//	private void ChangeTreeVisibility(boolean makeVisible){
//		if (makeVisible) {
//			clearGoods();
//			lvTreeList.setVisibility(View.VISIBLE);
//		} else {
//			setListAdapter(mListViewMode);
//			lvTreeList.setVisibility(View.GONE);
//		}
//	}

    // Контекстное меню
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

//	/**
//	 * разворачиваем все группы
//	 */
//	private void expand(boolean expandAll) {
//		notifyTreeAdapter();
//	}


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

//	private int findAndSelect(ArrayList<Node> goodsTree, String code) {
//		for (int i = 0; i < goodsTree.size(); i++) {
//			Node node = goodsTree.get(i);
//			if (node.getCode().equalsIgnoreCase(code)) {
//				node.setSelection();
//				return i;
//			}
//		}
//		return -1;
//	}

//	private void setTreeAdapter(ListView lv, ArrayList<Node> tree) {
//		if (lv != null && tree != null) {
//			mNodesAdapter = new GoodsGroupAdapter(getActivity(), tree);
//			lv.setAdapter(mNodesAdapter);
//		}
//	}

    @SuppressLint("LongLogTag")
    @Override
    public void onTaskComplete(LoadGoodsGroupTask task) {
        boolean res;
        try {
            res = task.get();
            if (res) {
                mGoodsTree = task.getGoodsTree();

                updateAdapter();

                //Выделяем узел
                if (mLastSelected != null) {
                    int index = -1;
                    for (Node node : mFlatList) {
                        if (node.getCode().equalsIgnoreCase(mLastSelected.getCode())) {
                            ++index;
                            break;
                        } else ++index;
                    }
                    if (-1 < index && index < mFlatList.size()) {
                        setSelection(index);
                    }
                }
            } else {
                //GlobalProc.mToast(getActivity(), errorLoad);
                Log.e(TAG, errorLoad);
            }
            waitCursor(false);
        } catch (Exception e) {
            res = false;
        }
    }

    @Override
    public void onTaskComplete(LoadGoodsOnTreeClickTask task) {
    }

    public void updateAdapter() {
        mFlatList = createFlatList(mGoodsTree);
        if (mNodesAdapter == null) {
            mNodesAdapter = new GoodsGroupAdapter(getActivity(), mFlatList);
            setListAdapter(mNodesAdapter);
        } else {
            mNodesAdapter.clear();
            mNodesAdapter.addAll(mFlatList);
        }
    }

    @Override
    public void onTaskComplete(LoadGroupMatrixTask task) {
        // TODO Auto-generated method stub

    }
}
