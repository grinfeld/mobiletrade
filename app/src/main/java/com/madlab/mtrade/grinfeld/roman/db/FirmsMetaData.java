package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class FirmsMetaData implements BaseColumns {
    public static final String TABLE_NAME = "OurInfo";

    public static final String FIELD_CODE = "Code";
    public static final String FIELD_NAME = "Name";
    public static final String FIELD_INN = "Inn";
    public static final String FIELD_ADDRESS = "LowAddress";
    public static final String FIELD_ADDRESS_FULL = "PostAddress";
    public static final String FIELD_CHIEF = "Chief";
    public static final String FIELD_ACCOUNTER = "Accounter";
    public static final String FIELD_BANK = "Bank";
    public static final String FIELD_PHONE = "Phone";

    public static final byte FIELD_CODE_INDEX = 0;
    public static final byte FIELD_NAME_INDEX = 1;
    public static final byte FIELD_INN_INDEX = 2;
    public static final byte FIELD_ADDRESS_INDEX = 3;
    public static final byte FIELD_ADDRESS_FULL_INDEX = 4;
    public static final byte FIELD_CHIEF_INDEX = 5;
    public static final byte FIELD_ACCOUNTER_INDEX = 6;
    public static final byte FIELD_BANK_INDEX = 7;
    public static final byte FIELD_PHONE_INDEX = 8;

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s("
                    + "%s	nvarchar(3)     NOT NULL DEFAULT '',"
                    + "%s	nvarchar(50)    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(30)    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(85)    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(135)	NOT NULL DEFAULT '',"
                    + "%s	nvarchar(40)    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(40)    NOT NULL DEFAULT '',"
                    + "%s	nvarchar(200)   NOT NULL DEFAULT '',"
                    + "%s	nvarchar(100)   NOT NULL DEFAULT '')", TABLE_NAME,
            FIELD_CODE, FIELD_NAME, FIELD_INN, FIELD_ADDRESS,
            FIELD_ADDRESS_FULL, FIELD_CHIEF, FIELD_ACCOUNTER, FIELD_BANK,
            FIELD_PHONE);

    public static String insertQuery(String code, String name, String inn,
                                     String address, String postAddr, String chief, String account,
                                     String bank, String phones) {
        return String.format("INSERT INTO %s ("
                        + "%s, %s, %s, %s, %s, %s, %s, %s, %s" + ") VALUES ("
                        + "%s, '%s', '%s', '%s', '%s', " + "'%s', '%s', '%s', '%s')",
                TABLE_NAME, FIELD_CODE, FIELD_NAME, FIELD_INN, FIELD_ADDRESS,
                FIELD_ADDRESS_FULL, FIELD_CHIEF, FIELD_ACCOUNTER, FIELD_BANK,
                FIELD_PHONE, code, name, inn, address, postAddr, chief,
                account, bank, phones);
    }
}
