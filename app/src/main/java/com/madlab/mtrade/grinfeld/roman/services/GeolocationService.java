package com.madlab.mtrade.grinfeld.roman.services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.AuthManager;
import com.madlab.mtrade.grinfeld.roman.MainActivity;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.DatabaseHelperManager;
import com.madlab.mtrade.grinfeld.roman.db.GeolocationEntityMapper;
import com.madlab.mtrade.grinfeld.roman.entity.Geolocation;
import com.madlab.mtrade.grinfeld.roman.iface.AuthStateListener;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class GeolocationService extends Service implements LocationListener, AuthStateListener {

    private static final String TAG = "#GeolocationService";
    public static final String ACTION_STOP_SERVICE = "com.madlab.mtrade.grinfeld.roman.services.GeolocationService.STOP_SERVICE";
    private static final int ALARM_TYPE_ELAPSED = 10;

    private DBHelper db;

    public enum State {
        RUN, STOP
    }

    public interface OnServiceStateChangedListener {
        void onServiceStateChanged(State newState);
    }

    private static State serviceState = State.STOP;
    private static Set<OnServiceStateChangedListener> listeners = new HashSet<>();

    private String employeeId;
    private AuthManager authManager = AuthManager.getInstance();
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private String action;

    public static State getServiceState() {
        return serviceState;
    }

    public static void addOnServiceStateChangedListener(OnServiceStateChangedListener listener) {
        listeners.add(listener);
        listener.onServiceStateChanged(serviceState);
    }

    public static void removeOnServiceStateChangedListener(OnServiceStateChangedListener listener) {
        listeners.remove(listener);
    }

    private void notifyOnServiceStateChangedListeners(State newState) {
        serviceState = newState;

        for (OnServiceStateChangedListener listener : listeners) {
            listener.onServiceStateChanged(serviceState);
        }
    }


    private final Object lock = new Object();
    private Geolocation bestGeolocation = null;

    private final Timer timer = new Timer();


    @Override
    public void onCreate() {
        db = DatabaseHelperManager.getDatabaseHelper(this);
        super.onCreate();

        if (!authManager.isAuthenticate()) {
            stopSelf();
            return;
        }
        authManager.addAuthStateListener(this);

        scheduleRepeatingElapsedNotification(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_DENIED) {

            Notification notification = new NotificationCompat.BigTextStyle(new NotificationCompat.Builder(this, MyApp.NOTIFICATION_CHANNEL_ID_INFO)
                    .setSmallIcon(R.mipmap.main_icon)
                    .setContentTitle("MTRADE")
                    .setContentText(getString(R.string.error_starting_geolocation_service))
                    .setPriority(2)
                    .setDefaults(Notification.DEFAULT_ALL))
                    .bigText(getString(R.string.no_access_rights_to_the_data_of_the_device_location))
                    .build();
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.notify(R.id.notifications_geolocation_service_error, notification);

            stopSelf();
            return;
        }
        Log.d(TAG, getString(R.string.geolocation_service_started));
        // dbHelper.log(new Event(Log.INFO, getString(R.string.geolocation_service_started)));
        notifyOnServiceStateChangedListeners(State.RUN);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, MyApp.NOTIFICATION_CHANNEL_ID_INFO);
        Notification notification = builder.setContentIntent(
                PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0))
                .setSmallIcon(R.drawable.ic_satellite_white_24dp)
                .setContentTitle("MTRADE")
                .setContentText(getString(R.string.warning_writing_device_geolocation))
                .build();
        startForeground(R.id.notifications_geolocation_service, notification);

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
//            dbHelper.log(new Event(Log.INFO,
//                    getString(R.string.geolocation_providers_disabled)));
            Log.d(TAG, getString(R.string.geolocation_providers_disabled));
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Geolocation geolocation;

                synchronized (lock) {
                    geolocation = bestGeolocation;
                    bestGeolocation = null;
                }

                if (geolocation == null) {
                    Log.d(TAG, getString(R.string.geolocation_is_not_defined));
                    //dbHelper.log(new Event(Log.INFO, getString(R.string.geolocation_is_not_defined)));
                } else if (geolocation.getAccuracy() > 100) {
                    Log.d(TAG, getString(R.string.geolocation_does_not_meet_the_accuracy_requirements));
                    // dbHelper.log(new Event(Log.INFO,getString(R.string.geolocation_does_not_meet_the_accuracy_requirements)));
                } else {
                    try {
                        Log.d(TAG, String.valueOf(geolocation.getLatitude() + " " + geolocation.getLongitude()));
                        GeolocationEntityMapper.insert(db.getWritableDatabase(), geolocation);
                    } catch (SQLException e) {
                        Log.d(TAG, e.getMessage());
                        //dbHelper.log(new Event(Log.ERROR, e.getMessage()));
                    }
                }
            }
        }, 10000, 10000);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (alarmManager != null) {
            pendingIntent = PendingIntent.getService(this, 0, new Intent(this, ReportingService.class), 0);
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(),
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                    pendingIntent);
        }
    }


    public void scheduleRepeatingElapsedNotification(Context context) {
        Intent intent = new Intent(context, AlarmReceiver.class);

        PendingIntent alarmIntentElapsed = PendingIntent.getBroadcast(context, ALARM_TYPE_ELAPSED, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManagerElapsed = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        //Неточный ежедневный сигнал от момента загрузки устройства. Это наилучший выбор и
        //в этом случае он автоматически изменяется при смене локали/часового пояса
        //Настроим сигнал получения уведомления 15 минут после перезагрузки в каждые 15 минут
        if (alarmManagerElapsed != null) {
            alarmManagerElapsed.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + 150000,
                    150000, alarmIntentElapsed);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getStringExtra("COMMAND") != null) {
            action = intent.getStringExtra("COMMAND");
            if (action != null && action.equals("STOP")) {
                stopSelf();
            }
        }
        return START_STICKY;
    }

    @Override
    public void onAuthStateChanged(String employeeId) {
        if (employeeId == null) {
            stopSelf();
            return;
        }
        this.employeeId = employeeId;
    }

    @Override
    public void onLocationChanged(Location location) {
        synchronized (lock) {
            if (bestGeolocation == null || bestGeolocation.getAccuracy() > location.getAccuracy()) {
                bestGeolocation = new Geolocation(location);
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // do nothing
    }

    @Override
    public void onProviderEnabled(String provider) {
        //dbHelper.log(new Event(Log.INFO, provider + getString(R.string._provider_enabled)));
        Log.d(TAG, provider + getString(R.string._provider_enabled));
    }

    @Override
    public void onProviderDisabled(String provider) {
        //dbHelper.log(new Event(Log.INFO, provider + getString(R.string._provider_disabled)));
        Log.d(TAG, provider + getString(R.string._provider_disabled));
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
//        timer.cancel();
//
//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
//                PackageManager.PERMISSION_GRANTED) {
//            locationManager.removeUpdates(this);
//        }
//
//        stopForeground(true);
//        if (alarmManager!=null){
//            if (pendingIntent!=null){
//                alarmManager.cancel(pendingIntent);
//            }
//        }
//        //dbHelper.log(new Event(authData.getEmployeeId(), Log.INFO,getString(R.string.geolocations_service_stoped)));
//        notifyOnServiceStateChangedListeners(State.STOP);
//        db = null;
//        DatabaseHelperManager.releaseDatabaseHelper();
//            if (action==null){
//                Intent broadcastIntent = new Intent("com.madlab.mtrade.grinfeld.roman.services.GeolocationRestartBroadcastReceiver");
//                sendBroadcast(broadcastIntent);
//            }else {
//                Log.d(TAG, getString(R.string.geolocations_service_stoped));
//                action = null;
//            }
//        super.onDestroy();
        timer.cancel();

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            }
        }

        stopForeground(true);

        notifyOnServiceStateChangedListeners(State.STOP);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(PendingIntent.getService(this, 0, new Intent(this, ReportingService.class), 0));
        }

        startService(new Intent(this, ReportingService.class));

        super.onDestroy();
        db = null;
        DatabaseHelperManager.releaseDatabaseHelper();
    }
}
