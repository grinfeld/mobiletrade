package com.madlab.mtrade.grinfeld.roman.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class GoodsInDocument implements Parcelable{
    public static String KEY = "GoodsInDocumentExtras";

    /**
     * Код товара
     */
    private String mCode;

    /**
     * Позиция переключателя штук/упаковок
     */
    private byte mMeashPosition;

    /**
     * Позиция штук
     */
    private int mCountPosition;

    /**
     * Вес
     */
    private float mWeight;

    /**
     * Цена
     */
    private float mPrice;

    /**
     * Квант
     */
    private float mQuant;

    /**
     * Признак спеццены
     */
    private boolean mSpecPrice;

    /**
     * Признак сертификата
     */
    private boolean mSertificate;

    /**
     * Признак качественного удостоверения
     */
    private boolean mQualityDoc;

    /**
     * Признак половины головки
     */
    private boolean mIsHalfHead;


    public float getWeight() {
        return mWeight;
    }

    public void setWeight(float mWeight) {
        this.mWeight = mWeight;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String pCode) {
        mCode = pCode;
    }

    public int getMeashPosition() {
        return mMeashPosition;
    }

    public void setMeashPosition(byte pMeashPosition) {
        mMeashPosition = pMeashPosition;
    }

    public int getCountPosition() {
        return mCountPosition;
    }

    public void setCountPosition(int pCountPosition) {
        mCountPosition = pCountPosition;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float pPrice) {
        mPrice = pPrice;
    }

    public float getQuant() {
        return mQuant;
    }

    public void setQuant(float pQuant) {
        mQuant = pQuant;
    }

    public boolean isSpecPrice() {
        return mSpecPrice;
    }

    public void isSpecPrice(boolean pSpecPrice) {
        mSpecPrice = pSpecPrice;
    }

    public boolean isSertificate() {
        return mSertificate;
    }

    public void isSertificate(boolean pSertificate) {
        mSertificate = pSertificate;
    }

    public boolean isQualityDoc() {
        return mQualityDoc;
    }

    public void isQualityDoc(boolean pQualityDoc) {
        mQualityDoc = pQualityDoc;
    }

    /**
     * Конструктор
     * @param code Код товара
     * @param count Позиция правого колеса (кол-во штук или упаковок)
     * @param meash Позиция левого колеса ( 0 или 1)
     * @param quant Квант
     * @param price Цена
     */
    public GoodsInDocument(String code, int count, byte meash, float quant, float price, float weight){
        mCode = code;
        mCountPosition = count;
        mMeashPosition = meash;
        mQuant = quant;
        mPrice = price;
        mWeight = weight;

        mSpecPrice = false;
        mSertificate = false;
        mQualityDoc = false;
    }

    public GoodsInDocument(Parcel parcel){
        mCode = parcel.readString();
        mCountPosition = parcel.readInt();
        mMeashPosition = parcel.readByte();
        mQuant = parcel.readFloat();
        mPrice = parcel.readFloat();
        mWeight = parcel.readFloat();

        boolean[] arr = new boolean[3];
        parcel.readBooleanArray(arr);

        mSpecPrice = arr[0];
        mSertificate = arr[1];
        mQualityDoc = arr[2];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int arg1) {
        parcel.writeString(mCode);
        parcel.writeInt(mCountPosition);
        parcel.writeByte(mMeashPosition);
        parcel.writeFloat(mQuant);
        parcel.writeFloat(mPrice);
        parcel.writeFloat(mWeight);

        boolean[] signs = new boolean[] {mSpecPrice, mSertificate, mQualityDoc};
        parcel.writeBooleanArray(signs);
    }

    public static final Parcelable.Creator<GoodsInDocument> CREATOR = new Parcelable.Creator<GoodsInDocument>() {

        public GoodsInDocument createFromParcel(Parcel in) {
            return new GoodsInDocument(in);
        }

        public GoodsInDocument[] newArray(int size) {
            return new GoodsInDocument[size];
        }
    };

    public boolean ismIsHalfHead() {
        return mIsHalfHead;
    }

    public void setmIsHalfHead(boolean mIsHalfHead) {
        this.mIsHalfHead = mIsHalfHead;
    }
}
