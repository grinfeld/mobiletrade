package com.madlab.mtrade.grinfeld.roman;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.connectivity.ReportRequest;
import com.madlab.mtrade.grinfeld.roman.fragments.FireBaseMessageContainer;
import com.madlab.mtrade.grinfeld.roman.services.MyFirebaseMessagingService;

public class NotificationMessageActivity extends AppCompatActivity {


    private static final String TAG = "#NotificationMessageActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_message);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FireBaseMessageContainer fireBaseMessageContainer = new FireBaseMessageContainer();
        if (getIntent().getAction() != null) {
            if (getIntent().getExtras()!=null && getIntent().getExtras().get(MyFirebaseMessagingService.ID_MESSAGE)!=null){
                String id = (String) getIntent().getExtras().get(MyFirebaseMessagingService.ID_MESSAGE);
                String tag = (String) getIntent().getExtras().get(MyFirebaseMessagingService.TAG_MESSAGE);
                Bundle bundle = new Bundle();
                bundle.putString(MyFirebaseMessagingService.ID_MESSAGE, id);
                bundle.putString(MyFirebaseMessagingService.TAG_MESSAGE, tag);
                fireBaseMessageContainer.setArguments(bundle);
            }else if (getIntent().getExtras().get(MyFirebaseMessagingService.TAG_MESSAGE)!=null){
                String tag = (String) getIntent().getExtras().get(MyFirebaseMessagingService.TAG_MESSAGE);
                Bundle bundle = new Bundle();
                bundle.putString(MyFirebaseMessagingService.ID_MESSAGE, "0");
                bundle.putString(MyFirebaseMessagingService.TAG_MESSAGE, tag);
                fireBaseMessageContainer.setArguments(bundle);
            }
        }
        fragmentManager.beginTransaction()
                .replace(R.id.contentMain, fireBaseMessageContainer)
                .commit();

    }


    @Override
    public boolean onSupportNavigateUp() {
        int backStackCount = getFragmentManager().getBackStackEntryCount();
        if (backStackCount > 0){
            getFragmentManager().popBackStack();
        }else {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        return true;
    }

}
