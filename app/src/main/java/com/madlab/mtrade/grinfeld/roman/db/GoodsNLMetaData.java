package com.madlab.mtrade.grinfeld.roman.db;

import android.provider.BaseColumns;

public final class GoodsNLMetaData implements BaseColumns {
    private GoodsNLMetaData() {
    }

    public static final String TABLE_NAME = "GoodsNonLiquid";

    public static final String FIELD_CODE = "CodeGoods";
    public static final String FIELD_REST = "RestOnStor";
    public static final String FIELD_PRICE = "Price";
    public static final String FIELD_DATE_OF_CHANGE = "DateOfChange";
    public static final String FIELD_COMMENT = "Comment";

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String
            .format("CREATE TABLE %s ("
                            + "%s	nvarchar(6)		NOT NULL CONSTRAINT PK_GoodsInStor PRIMARY KEY, "
                            + "%s	money			NOT NULL DEFAULT 0, "
                            + "%s	money			NOT NULL DEFAULT 0, "
                            + "%s	datetime		NOT NULL DEFAULT 0, "
                            + "%s	nvarchar(100)	NOT NULL DEFAULT '')", TABLE_NAME,
                    FIELD_CODE, FIELD_REST, FIELD_PRICE, FIELD_DATE_OF_CHANGE,
                    FIELD_COMMENT);

    public static final String INDEX = String.format(
            "CREATE INDEX idxGoodsNLCodeGoods ON %s (%s)", TABLE_NAME,
            FIELD_CODE);
}
