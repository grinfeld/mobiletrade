package com.madlab.mtrade.grinfeld.roman.entity;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;

import com.madlab.mtrade.grinfeld.roman.db.GoodsMetaData;

public class Node implements Parcelable {
    public static final String KEY = "GoodsGroupNode";
    public final static String NODE_KEY = "node";
    public final static String SUBNODE_KEY = "subNode";

    private String mParentCode;
    private String mCodeGroup;
    private String mNameGroup;
    private int mChildCount;
    private byte mLevel;
    private boolean mIsExpanded;
    private boolean mIsSelected;
    private byte mDiscount;
    private ArrayList<Node> mChild;

    private static Node mLastSelected;

    public Node(String parent, String code, String name) {
        mParentCode = parent;
        mCodeGroup = code;
        mNameGroup = name;
        mChildCount = 0;
        mLevel = 0;
        mIsExpanded = false;
        mIsSelected = false;
        mDiscount = 0;
        mChild = new ArrayList<Node>();
    }

    public String getName() {
        return mNameGroup;
    }

    public void setName(String n) {
        mNameGroup = n;
    }

    public String getParentCode() {
        return mParentCode;
    }

    public void setParentCode(String p) {
        mParentCode = p;
    }

    public String getCode() {
        return mCodeGroup;
    }

    public void setCode(String c) {
        mCodeGroup = c;
    }

    public int childCount() {
        return mChildCount;
    }

    public void childCount(int count) {
        mChildCount = count;
    }

    public boolean isExpanded() {
        return mIsExpanded;
    }

    public void isExpanded(boolean isExpanded) {
        mIsExpanded = isExpanded;
    }

    public void toggle() {
        mIsExpanded = !mIsExpanded;
    }

    public boolean isSelected() {
        return mIsSelected;
    }

    public void isSelected(boolean isSelected) {
        mIsSelected = isSelected;
    }

    public void setSelection() {
        if (mLastSelected != null && mLastSelected != this) {
            // Убираем пометку с предыдущего
            mLastSelected.isSelected(false);
        }

        mIsSelected = true;
        // Сохраняем ссылку на текущий
        mLastSelected = this;
    }

    public byte getLevel() {
        return mLevel;
    }

    public void setLevel(byte level) {
        mLevel = level;
    }

    public ArrayList<Node> getChild() {
        return mChild;
    }

    public void setChild(ArrayList<Node> child) {
        if (child != null) {
            mChildCount = child.size();
        }
        byte nextLevel = (byte) (mLevel + 1);
        for (Node node : child) {
            node.setLevel(nextLevel);
        }
        mChild = child;
    }

    public void addChild(Node child) {
        if (mChild == null) {
            mChild = new ArrayList<Node>();
        }
        child.setLevel((byte) (mLevel + 1));
        mChild.add(child);
        if (mChild != null) {
            mChildCount = mChild.size();
        }
    }

    public byte discount() {
        return mDiscount;
    }

    public void discount(byte discount) {
        mDiscount = discount;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", mParentCode, mCodeGroup, mNameGroup);
    }

    ;

    /**
     * Загружает сведения об узле из БД
     *
     * @param db         - база данных
     * @param parentCode - код товара
     * @return элемент типа Node
     */
    public static Node load(SQLiteDatabase db, String parentCode) {
        String sql = String.format("SELECT %s, %s, %s FROM %s WHERE %s = ?",
                // Select
                GoodsMetaData.FIELD_CODE.Name, GoodsMetaData.FIELD_PARENT.Name,
                GoodsMetaData.FIELD_NAME.Name,
                // From
                GoodsMetaData.TABLE_NAME,
                // Where
                GoodsMetaData.FIELD_CODE.Name);
        Cursor results = db.rawQuery(sql, new String[]{parentCode});

        Node node = null;

        if (results.moveToFirst()) {
            node = new Node(results.getString(0), results.getString(1),
                    results.getString(2));
        }

        results.close();

        return node;
    }

    // -----------------------------------------------------------------------------------------------
    // List

    private static String[] fields = new String[]{
            GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name,
            GoodsMetaData.FIELD_NAME.Name};

    private static String where = String.format("%s = 1 AND %s = ?",
            GoodsMetaData.FIELD_RECORD_TYPE.Name,
            GoodsMetaData.FIELD_PARENT.Name);


    public static ArrayList<Node> loadSubGroupsSTM(SQLiteDatabase db, String rootCode, byte level) {
        ArrayList<Node> resultList = null;
        Cursor result = null;
        try {
            String[] fields = new String[]{
                    GoodsMetaData.FIELD_PARENT.Name, GoodsMetaData.FIELD_CODE.Name,
                    GoodsMetaData.FIELD_NAME.Name};
//AND %s = '%s'
            String where = String.format("%s = 0  AND %s = 1",
                    //
                    GoodsMetaData.FIELD_RECORD_TYPE.Name,
                    //AND
                    //GoodsMetaData.FIELD_PARENT.Name, rootCode,
                    //AND
                    GoodsMetaData.FIELD_STM);

            //String[] args = new String[]{rootCode};

            result = db.query(GoodsMetaData.TABLE_NAME, fields, where,
                    null, null, null,
                    GoodsMetaData.FIELD_SORT_ORDER.Name);
            if (result.getCount() > 0) {
                resultList = new ArrayList<>();
            }

            while (result.moveToNext()) {
                Node node = new Node(result.getString(0), result.getString(1),
                        result.getString(2));
                node.setLevel(level);
                if (resultList != null) {
                    resultList.add(node);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (result != null)
                result.close();
        }
        return resultList;
    }

    /**
     * Загружает подгруппы из БД
     *
     * @param db
     * @param rootCode
     * @return
     */
    public static ArrayList<Node> loadSubGroups(SQLiteDatabase db, String rootCode, byte level) {
        ArrayList<Node> resultList = null;

        Cursor results = null;
        try {
            results = db.query(GoodsMetaData.TABLE_NAME, fields, where,
                    new String[]{rootCode}, null, null,
                    GoodsMetaData.FIELD_SORT_ORDER.Name);
            if (results.getCount() > 0) {
                resultList = new ArrayList<>();
            }

            while (results.moveToNext()) {
                Node node = new Node(results.getString(0), results.getString(1),
                        results.getString(2));
                node.setLevel(level);
                resultList.add(node);
            }
        } catch (Exception e) {

        } finally {
            if (results != null)
                results.close();
        }

        return resultList;
    }

    /**
     * TODO: Надо переделать
     *
     * @param fullList
     * @param child
     * @param groupCode
     */
    public static void findAndExpand(ArrayList<Node> fullList, ArrayList<Node> child, String groupCode) {
        ArrayList<Node> list = child == null ? fullList : child;
        for (int i = 0; i < list.size(); i++) {
            Node node = list.get(i);
            if (node.getCode().equalsIgnoreCase(groupCode)) {
                node.isExpanded(true);
                if (node.getLevel() > 0)
                    findAndExpand(fullList, null, node.getParentCode());
                return;
            } else {
                findAndExpand(fullList, node.getChild(), groupCode);
            }
        }
    }

    public static final Parcelable.Creator<Node> CREATOR = new Parcelable.Creator<Node>() {

        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        public Node[] newArray(int size) {
            return new Node[size];
        }
    };

    @SuppressWarnings("unchecked")
    public Node(Parcel parcel) {
        mParentCode = parcel.readString();
        mCodeGroup = parcel.readString();
        mNameGroup = parcel.readString();
        mLevel = parcel.readByte();
        mDiscount = parcel.readByte();
        boolean[] boolArr = new boolean[2];
        parcel.readBooleanArray(boolArr);
        mIsExpanded = boolArr[0];
        mIsSelected = boolArr[1];

        mChild = parcel.readArrayList(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mParentCode);
        dest.writeString(mCodeGroup);
        dest.writeString(mNameGroup);
        dest.writeByte(mLevel);
        dest.writeByte(mDiscount);
        dest.writeBooleanArray(new boolean[]{mIsExpanded, mIsSelected});
        dest.writeList(mChild);
    }
}
