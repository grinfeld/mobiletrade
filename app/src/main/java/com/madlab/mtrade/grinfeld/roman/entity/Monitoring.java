package com.madlab.mtrade.grinfeld.roman.entity;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.MonitoringMetaData;
import com.madlab.mtrade.grinfeld.roman.db.MonitoringProductMetaData;
import com.madlab.mtrade.grinfeld.roman.db.OrderMetaData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by GrinfeldRA
 */

public class Monitoring {

    private static final String TAG = "#Monitoring";
    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private int id;
    private String codeClient;
    private String codeManager;
    private Date date;
    private UUID visit;
    private int Unconfirmed;
    private static Monitoring currentMonitoring;
    private String state;
    private ArrayList<MonitoringProduct> mItemsList;
    private int typeServer;
    public static int UNCONFIRMED_MONITORING_NUMBER = 0;
    private Client client;
    private String codeAgent;
    private boolean reserved;

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        MyApp myApp = (MyApp)MyApp.getContext().getApplicationContext();
        return Objects.requireNonNull(Client.load(myApp.getDB(), codeClient)).mName + ", выбрано товаров: "+mItemsList.size();
    }

    public Monitoring(String codeAgent, String codeClient, String codeManager, Date date, UUID visit, int unconfirmed, String state, int typeServer) {
        this.codeAgent = codeAgent;
        this.codeClient = codeClient;
        this.codeManager = codeManager;
        this.date = date;
        this.visit = visit;
        Unconfirmed = unconfirmed;
        this.state = state;
        this.typeServer = typeServer;
        mItemsList = new ArrayList<>();
    }

    public Monitoring() {
        this("", "", "", new Date(), null, UNCONFIRMED_MONITORING_NUMBER, "V", 0);
    }


    public static boolean isUnsavedMonitoringExists(Context ctx, String uuid) {
        boolean res;
        try {
            MyApp app = (MyApp) ctx.getApplicationContext();
            res = isExist(app.getDB(),uuid);
        } catch (Exception e) {
            Log.e("isUnsavedOrderExists", e.toString());
            res = false;
        }
        return res;
    }

    public static boolean isExist(SQLiteDatabase db,String uuid) {
        boolean res = false;
        Cursor rows = null;
        try {
            String sql = String.format(Locale.ENGLISH,
                    "SELECT * FROM %s WHERE %s = '%s'", MonitoringMetaData.TABLE_NAME,
                    MonitoringMetaData.FIELD_VISIT_FK, uuid);
            rows = db.rawQuery(sql, null);
            res = rows.moveToFirst();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        } finally {
            rows.close();
        }
        return res;
    }




    public static void newMonitoring(Context context) {
        currentMonitoring = new Monitoring();
    }


    public void addProductItemAll(List<MonitoringProduct> productList) {
        mItemsList = new ArrayList<>(productList);
    }


    public static void loadUnsavedMonitoring(Context context) {
        try {
            MyApp app = (MyApp) context.getApplicationContext();
            currentMonitoring = Objects.requireNonNull(loadUnsavedMonitoring(app.getDB(), UNCONFIRMED_MONITORING_NUMBER)).get(0);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            currentMonitoring = null;
        }
    }

    public static List<Monitoring> loadUnsavedMonitoring(final SQLiteDatabase db, int index) {
        try {
            return load(db, String.format(Locale.getDefault(), "%s = %d", MonitoringMetaData.FIELD_UNCONFIRMED_MONITORING, index));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<Monitoring> load(final SQLiteDatabase db, String where) throws Exception {
        List<Monitoring> monitoringList = new ArrayList<>();
        Cursor cursorMonitoring = db.query(MonitoringMetaData.TABLE_NAME, null, where, null,
                null, null, null);
        while (cursorMonitoring.moveToNext()) {
            int index = cursorMonitoring.getInt(0);
            Monitoring monitoring = new Monitoring();
            monitoring.id = index;
            monitoring.codeClient = cursorMonitoring.getString(1);
            monitoring.codeManager = cursorMonitoring.getString(2);
            SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN, Locale.getDefault());
            monitoring.date = format.parse(cursorMonitoring.getString(3));
            monitoring.visit = UUID.fromString(cursorMonitoring.getString(4));
            monitoring.state = cursorMonitoring.getString(5);
            monitoring.typeServer = cursorMonitoring.getInt(6);
            monitoring.Unconfirmed = cursorMonitoring.getInt(7);
            monitoring.reserved = cursorMonitoring.getShort(8) == 1;
            monitoring.codeAgent = cursorMonitoring.getString(9);
            String sql = String.format(Locale.getDefault(), "Select * from %s where %s = %d",
                    MonitoringProductMetaData.TABLE_NAME, MonitoringProductMetaData.FIELD_ID_MONITORING, index);
            Cursor cursorProduct = db.rawQuery(sql, null);
            List<MonitoringProduct> monitoringProducts = new ArrayList<>();
            while (cursorProduct.moveToNext()) {
                MonitoringProduct monitoringProduct =
                        new MonitoringProduct(cursorProduct.getInt(0), cursorProduct.getString(1), cursorProduct.getInt(2));
                monitoringProducts.add(monitoringProduct);
            }
            monitoring.addProductItemAll(monitoringProducts);
            monitoring.setClient(Client.load(db, monitoring.codeClient));
            monitoringList.add(monitoring);
            cursorProduct.close();
        }
        cursorMonitoring.close();
        return monitoringList;
    }

    public boolean insert(SQLiteDatabase db) {
        ContentValues common = new ContentValues();
        common.put(MonitoringMetaData.FIELD_CODE_CLIENT, codeClient);
        common.put(MonitoringMetaData.FIELD_CODE_MANAGER, codeManager);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN, Locale.getDefault());
        common.put(MonitoringMetaData.FIELD_DATE, dateFormat.format(date));
        common.put(MonitoringMetaData.FIELD_UNCONFIRMED_MONITORING, Unconfirmed);
        common.put(MonitoringMetaData.FIELD_VISIT_FK, visit.toString());
        common.put(MonitoringMetaData.FIELD_STATE, state);
        common.put(MonitoringMetaData.FIELD_TYPE_SERVER, typeServer);
        db.beginTransaction();
        try {
            long insert = db.insert(MonitoringMetaData.TABLE_NAME, null, common);
            if (insert == -1) {
                db.endTransaction();
                return false;
            }
            setId((int) insert);
        } catch (Exception e) {
            db.endTransaction();
            return false;
        }
        for (MonitoringProduct monitoringProduct : mItemsList) {
            ContentValues item = new ContentValues();
            item.put(MonitoringProductMetaData.FIELD_CHECK_PRODUCT, monitoringProduct.getCheck());
            item.put(MonitoringProductMetaData.FIELD_CODE_PRODUCT, monitoringProduct.getCodeProduct());
            item.put(MonitoringProductMetaData.FIELD_ID_MONITORING, monitoringProduct.getIdMonitoring());
            try {
                if (db.insert(MonitoringProductMetaData.TABLE_NAME, null, item) == -1) {
                    db.endTransaction();
                    return false;
                }
            } catch (Exception e) {
                db.endTransaction();
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    @SuppressLint("LongLogTag")
    public boolean update(Context context) {
        MyApp myApp = (MyApp)context.getApplicationContext();
        SQLiteDatabase db = myApp.getDB();
        String where = String.format(Locale.getDefault(),"%s = %d", MonitoringMetaData.FIELD_ID, id);
        ContentValues common = new ContentValues();
        common.put(MonitoringMetaData.FIELD_CODE_CLIENT, codeClient);
        common.put(MonitoringMetaData.FIELD_CODE_MANAGER, codeManager);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        common.put(MonitoringMetaData.FIELD_DATE, dateFormat.format(date));
        common.put(MonitoringMetaData.FIELD_UNCONFIRMED_MONITORING, Unconfirmed);
        common.put(MonitoringMetaData.FIELD_VISIT_FK, visit.toString());
        common.put(MonitoringMetaData.FIELD_STATE, state);
        common.put(MonitoringMetaData.FIELD_TYPE_SERVER, typeServer);
        common.put(MonitoringMetaData.FIELD_RESERVED, reserved ? 1 : 0);
        common.put(MonitoringMetaData.FIELD_CODE_AGENT, codeAgent);
        db.beginTransaction();
        try {
            if (db.update(MonitoringMetaData.TABLE_NAME, common, where, null) == -1) {
                db.endTransaction();
                return false;
            }

        } catch (Exception e) {
            Log.e("!->OrderUpdateError", e.toString());
            db.endTransaction();
            return false;
        }
        Log.d(TAG, "mItemsList.size : " + mItemsList.size());
        if (mItemsList != null && mItemsList.size() > 0) {
            String query = MonitoringProductMetaData.Clear(id);
            try {
                db.execSQL(query);
            } catch (Exception e) {
                Log.e(TAG + ".clear table part", e.toString());
            }
            for (MonitoringProduct monitoringProduct : mItemsList) {
                ContentValues item = new ContentValues();
                item.put(MonitoringProductMetaData.FIELD_CHECK_PRODUCT, monitoringProduct.getCheck());
                item.put(MonitoringProductMetaData.FIELD_CODE_PRODUCT, monitoringProduct.getCodeProduct());
                item.put(MonitoringProductMetaData.FIELD_ID_MONITORING, monitoringProduct.getIdMonitoring());
                try {
                    if (db.insert(MonitoringProductMetaData.TABLE_NAME, null, item) == -1) {
                        db.endTransaction();
                        return false;
                    }
                } catch (Exception e) {
                    db.endTransaction();
                }
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public static String getTAG() {
        return TAG;
    }

    public String getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(String codeClient) {
        this.codeClient = codeClient;
    }

    public String getCodeManager() {
        return codeManager;
    }

    public void setCodeManager(String codeManager) {
        this.codeManager = codeManager;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UUID getVisit() {
        return visit;
    }

    public void setVisit(UUID visit) {
        this.visit = visit;
    }

    public int getUnconfirmed() {
        return Unconfirmed;
    }

    public void setUnconfirmed(int unconfirmed) {
        Unconfirmed = unconfirmed;
    }

    public static Monitoring getCurrentMonitoring() {
        return currentMonitoring;
    }

    public static void setCurrentMonitoring(Monitoring currentMonitoring) {
        Monitoring.currentMonitoring = currentMonitoring;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<MonitoringProduct> getItemsList() {
        return mItemsList;
    }

    public void setmItemsList(ArrayList<MonitoringProduct> mItemsList) {
        this.mItemsList = mItemsList;
    }

    public int getTypeServer() {
        return typeServer;
    }

    public void setTypeServer(int typeServer) {
        this.typeServer = typeServer;
    }

    public static int getUnconfirmedMonitoringNumber() {
        return UNCONFIRMED_MONITORING_NUMBER;
    }

    public static void setUnconfirmedMonitoringNumber(int unconfirmedMonitoringNumber) {
        UNCONFIRMED_MONITORING_NUMBER = unconfirmedMonitoringNumber;
    }

    public int getId() {
        return id;
    }

    public String getCodeAgent() {
        return codeAgent;
    }

    public void setCodeAgent(String codeAgent) {
        this.codeAgent = codeAgent;
    }
}

