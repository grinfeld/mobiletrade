package com.madlab.mtrade.grinfeld.roman.tasks;

import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.iface.IAskDiscountComplete;


public class AskDiscountTask extends AsyncTask<String, Integer, Byte> {
    private static final String TAG = "#AskDiscountTask";
    private final String PROC_NAME = "РасчетСкидки";

    public final static byte RES_SUCCESS = 0;
    public final static byte RES_FAIL = -1;

    private ProgressDialog progress;
    private Context baseContext;

    private String codeCli;
    private ArrayList<OrderItem> mItems;

    private IAskDiscountComplete mTaskCompleteListener;
    //Сведения для подключения
    private DalimoClient mClient;
    private Credentials connectInfo;

    private String mUnswer = "";

    private boolean startIsMenu = false;

    public boolean isStartIsMenu() {
        return startIsMenu;
    }

    public void setStartIsMenu(boolean startIsMenu) {
        this.startIsMenu = startIsMenu;
    }

    public String getMessage() {
        return mUnswer;
    }

    public ArrayList<OrderItem> getItems() {
        return mItems;
    }

    public AskDiscountTask(Context data, String codeCli,
                           ArrayList<OrderItem> items, IAskDiscountComplete taskCompleteListener) {
        baseContext = data;
        this.codeCli = codeCli;
        mItems = items;
        mTaskCompleteListener = taskCompleteListener;

        connectInfo = Credentials.load(data);
    }

    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(baseContext);
        progress.setMessage(baseContext.getString(R.string.mes_wait_for_load));
        progress.setCancelable(true);
        progress.show();
    }

    @Override
    protected Byte doInBackground(String... params) {
        String message;
        message = params[0];
        if (message == null) {
            StringBuilder data = new StringBuilder(String.format(Locale.ENGLISH, "%s;%s;%s;%d;%s;",
                    Const.COMMAND, App.get(baseContext).getCodeAgent(), PROC_NAME, 999, codeCli));

            for (OrderItem oi : mItems) {

                float cnt;
                // Если товар весовой, то переводим в кг
                // если штучный, переводим коробки в штуки
                if (oi.mIsWeightGoods) {
                    cnt = oi.getTotalWeight();
                } else {
                    cnt = oi.getCountInPieces();
                }

                data.append(String.format(Locale.ENGLISH, "%s#%.2f|",
                        oi.getCodeGoods(), cnt));

            }
            data.append(";");

            // А теперь всё это дело запускаем
            mClient = new DalimoClient(connectInfo);

            if (!mClient.connect()) {
                return RES_FAIL;
            }

            mClient.send(data + Const.END_MESSAGE);

            message = mClient.receive();
            Log.d(TAG, "РасчетСкидки APP: " + message);
            mClient.disconnect();
        }
        // А теперь обрабатываем полученное сообщение
        if (message.contains(DalimoClient.ERROR)) {
            int index = message.indexOf(DalimoClient.ERROR);
            if (index > 0) {
                mUnswer = message.substring(0, index);
            }
            return RES_FAIL;
        }

        parseMessage(message);

        return RES_SUCCESS;
    }


    @Override
    protected void onPostExecute(Byte result) {
        dismiss();

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (progress != null) {
            progress.hide();
            progress.dismiss();
        }
    }

    private void parseMessage(String data) {
        String[] itemsWithNewPrice = data.split(";");
        String code;
        String price;
        String enough;

        StringTokenizer token;
        for (String item : itemsWithNewPrice) {
            try {
                token = new StringTokenizer(item, Const.DELIMITTER);
                if (token.countTokens() > 2) {
                    code = token.nextToken();
                    price = token.nextToken();
                    enough = token.nextToken();

                    if (code.length() == 0)
                        continue;

                    // теперь надо обновить данные в списке товаров
                    for (OrderItem oi : mItems) {
                        if (oi.getCodeGoods().equalsIgnoreCase(code)) {
                            if (!oi.mIsSpecialPrice)
                                oi.setPrice(GlobalProc.parseFloat(price));
                            if (enough.equalsIgnoreCase("0") && oi.getIsCustom() != 1
                                    && !oi.perishable()) {
                                oi.notInStock(true);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("Ошибка", e.toString());
            }
        }
    }
}
