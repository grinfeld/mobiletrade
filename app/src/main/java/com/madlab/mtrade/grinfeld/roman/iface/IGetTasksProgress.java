package com.madlab.mtrade.grinfeld.roman.iface;

import com.madlab.mtrade.grinfeld.roman.entity.Task;
import com.madlab.mtrade.grinfeld.roman.tasks.GetTasksTask;

import java.util.List;


/**
 * Created by GrinfeldRA on 20.03.2018.
 */

public interface IGetTasksProgress {

    void onGetTasksComplete(GetTasksTask task);

    void onGetTasksProgress(List<Task> progress);

}
