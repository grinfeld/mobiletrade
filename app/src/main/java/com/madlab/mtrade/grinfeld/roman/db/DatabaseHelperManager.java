package com.madlab.mtrade.grinfeld.roman.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MyApp;

/**
 * Created by GrinfeldRA on 12.07.2018.
 */

public class DatabaseHelperManager {

    private static final DatabaseHelperManager INSTANCE = new DatabaseHelperManager();

    public static DBHelper getDatabaseHelper(){
        return getDatabaseHelper(MyApp.getContext());
    }

    public static DBHelper getDatabaseHelper(Context context){
        return INSTANCE.get(context);
    }

    public static void releaseDatabaseHelper(){
        INSTANCE.release();
    }

    private final Object lock = new Object();
    private DBHelper dbHelper;
    private volatile int useCount = 0;

    private DatabaseHelperManager(){
    }

    private DBHelper get(Context context){
        synchronized (lock){
            if (useCount++ <= 0){
                dbHelper = new DBHelper(context);
            }

            return dbHelper;
        }
    }

    private void release(){
        synchronized (lock){
            if (--useCount <= 0 && dbHelper != null){
                dbHelper.close();
                dbHelper = null;
            }
        }
    }

}

