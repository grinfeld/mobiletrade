package com.madlab.mtrade.grinfeld.roman.entity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.db.ManagerMerchMetaData;
import com.madlab.mtrade.grinfeld.roman.db.ManagerMetaData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GrinfeldRA
 */
public class ManagersMerch {


    private static final String TAG = "#ManagersMerch";
    private String mCode;
    private String mFIO;

    public ManagersMerch(String mCode, String mFIO) {
        this.mCode = mCode;
        this.mFIO = mFIO;
    }

    public String getmCode() {
        return mCode;
    }

    public String getmFIO() {
        return mFIO;
    }

    @Override
    public String toString() {
        return mFIO;
    }

    public static List<ManagersMerch> load(SQLiteDatabase db, String code){
        List<ManagersMerch> result = new ArrayList<>();

        String sql = String.format("SELECT * FROM %s WHERE %s = ?", ManagerMerchMetaData.TABLE_NAME, ManagerMerchMetaData.COLUMN_CODE_CLIENT);
        Cursor rows = null;
        try {
            rows = db.rawQuery(sql, new String[] {code});
            while (rows.moveToNext()){
                result.add(new ManagersMerch(rows.getString(3),rows.getString(4)));
            }
        }
        catch (Exception e) {
            GlobalProc.log_e(TAG, e.toString());
        }finally{
            if (rows != null){
                rows.close();
            }
        }

        return result;
    }


}
