package com.madlab.mtrade.grinfeld.roman.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.components.WheelView;
import com.madlab.mtrade.grinfeld.roman.entity.Order;
import com.madlab.mtrade.grinfeld.roman.entity.OrderItem;
import com.madlab.mtrade.grinfeld.roman.wheel.OnWheelScrollListener;
import com.madlab.mtrade.grinfeld.roman.wheel.adapters.ArrayWheelAdapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 14.11.2017.
 */

public class OrderGoodsAdapter extends ArrayAdapter<OrderItem> {

    private final static String TAG = "!->OrderGoodsAdapter";

    private short MAX_ITEMS = 30;

    private ArrayList<OrderItem> items;

    private static Drawable imageMustList;
    private static Drawable imageNewbee;
    private static Drawable imageDiscount;

    private static int textColor = Color.WHITE;

    private Order orderData;
    private Context context;

    private final OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollingStarted(WheelView wheel) {
        }

        public void onScrollingFinished(WheelView wheel) {
            OrderItem item = (OrderItem) wheel.getTag();

            if (orderData == null || item == null) {
                return;
            }

            int newWheelValue = wheel.getCurrentItem();
            switch (wheel.getId()) {
                case R.id.ogi_wheelCount:
                    if (newWheelValue > 0) {
                        item.mIsHalfHead = false;
                        if (item.inPack()) {
                            item.setCountPack(newWheelValue);
                        } else {
                            item.setCount(newWheelValue);
                        }
                    } else {
                        int index = orderData.getIndex(item);
                        if (index > -1) {
                            orderData.removeItem(index);
                        }
                    }
                    break;
                case R.id.ogi_wheelMeash:
                    item.changePack(newWheelValue == 1);
                    break;
            }

            orderData.recalcAmount(false);

            notifyDataSetChanged();
        }
    };

    /**
     * TODO: Данные заявки передаются для управления элементами заявки через колесики. Надо переделать на ICallback
     *
     * @param context
     * @param textViewResourceId
     * @param order
     */
    public OrderGoodsAdapter(Context context, int textViewResourceId, Order order) {
        super(context, textViewResourceId, order.getItems());
        this.items = order.getItems();
        MAX_ITEMS = App.get(context).getMaxWheelItemsCount();
        this.context = context;
        imageMustList = ContextCompat.getDrawable(context, R.mipmap.m);
        imageNewbee = ContextCompat.getDrawable(context, R.mipmap.h);
        imageDiscount = ContextCompat.getDrawable(context, R.mipmap.flag);

        orderData = order;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.item_order_goods, null);
            holder = new ViewHolder();
            holder.cbCheck = view.findViewById(R.id.ogi_check);
            holder.ivPic = view.findViewById(R.id.ogi_image);
            holder.tvName = view.findViewById(R.id.ogi_tvName);
            holder.tvPrice = view.findViewById(R.id.ogi_tvPrice);
            holder.tvAmount = view.findViewById(R.id.ogi_tvAmount);
            holder.tvWeight = view.findViewById(R.id.ogi_tvWeight);
            holder.tvRestOnStore = view.findViewById(R.id.ogi_tvRest);
            holder.wheelMeash = view.findViewById(R.id.ogi_wheelMeash);
            holder.wheelCount = view.findViewById(R.id.ogi_wheelCount);
            holder.txt_mercury = view.findViewById(R.id.txt_mercury);
            holder.txt_non_cash = view.findViewById(R.id.txt_non_cash);
            holder.txt_is_custom = view.findViewById(R.id.txt_is_custom);
            holder.txt_perishable = view.findViewById(R.id.txt_perishable);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        OrderItem orderItem = null;
        if (items != null && position < items.size()) {
            orderItem = items.get(position);
        }
        if (orderItem != null) {
            holder.cbCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
                items.get(position).isChecked(isChecked);
                MyApp app = (MyApp) context.getApplicationContext();
                Order.getOrder().update(app.getDB());
            });

            // Отметка
            if (holder.cbCheck != null) {
                holder.cbCheck.setChecked(orderItem.isChecked());
            }
            // Имя товара
            holder.tvName.setText(orderItem.getNameGoods().toString());
            if (orderItem.notInStock()) {
                holder.tvName.setTextColor(Color.RED);
            } else {
                holder.tvName.setTextColor(textColor);
            }
            switch (orderItem.mHighLight) {
                case OrderItem.H_NOTHING:
                    // holder.ivPic.setImageDrawable(imageInfo);
                    holder.ivPic.setVisibility(View.INVISIBLE);
                    break;
                case OrderItem.H_MUSTLIST:
                    holder.ivPic.setImageDrawable(imageMustList);
                    holder.ivPic.setVisibility(View.VISIBLE);
                    break;
                case OrderItem.H_DISCOUNT:
                    holder.ivPic.setImageDrawable(imageDiscount);
                    holder.ivPic.setVisibility(View.VISIBLE);
                    break;
                case OrderItem.H_NEWBEE:
                    holder.ivPic.setImageDrawable(imageNewbee);
                    holder.ivPic.setVisibility(View.VISIBLE);
                    break;
                default:
                    holder.ivPic.setVisibility(View.INVISIBLE);
                    break;
            }
            // Цена
            if (holder.tvPrice != null) {
                //holder.tvPrice.setText(GlobalProc.formatMoney(orderItem.getPrice()));
                holder.tvPrice.setText(GlobalProc.toForeignPrice(orderItem.getPrice(), orderData.getClient().isForeignAgent()));
            }
            // Итог
            if (holder.tvAmount != null) {
                //holder.tvAmount.setText(GlobalProc.formatMoney(orderItem.getAmount()));
                holder.tvAmount.setText(GlobalProc.toForeignPrice(orderItem.getAmount(), orderData.getClient().isForeignAgent()));
            }
            // Вес
            if (holder.tvWeight != null) {
                holder.tvWeight
                        .setText(new DecimalFormat("#.###").format(orderItem.getTotalWeight()));
            }
            // Остаток
            if (holder.tvRestOnStore != null) {
                holder.tvRestOnStore
                        .setText(new DecimalFormat("#.###").format(orderItem.restOnStore()));
                //GlobalProc.formatWeight(orderItem.restOnStore()));
            }
            // Колесико с ед. изм.
            if (holder.wheelMeash != null
                    && Order.getOrder() != null
                    && Order.getOrder().quality() != Order.QUALITY_NON_LIQUID) {
                initWheelMeash(orderItem, holder.wheelMeash);
            }
            // Колесико с количеством
            if (holder.wheelCount != null) {
                initWheel(orderItem, holder.wheelCount);
            }
            if (orderItem.getIsMercury() == 1) {
                holder.txt_mercury.setVisibility(View.VISIBLE);
            } else {
                holder.txt_mercury.setVisibility(View.GONE);
            }
            if (orderItem.isNonCash()) {
                holder.txt_non_cash.setVisibility(View.VISIBLE);
            } else {
                holder.txt_non_cash.setVisibility(View.GONE);
            }
            if (orderItem.getIsCustom() == 1) {
                holder.txt_is_custom.setVisibility(View.VISIBLE);
            } else {
                holder.txt_is_custom.setVisibility(View.GONE);
            }
            if (orderItem.perishable()) {
                holder.txt_perishable.setVisibility(View.VISIBLE);
            } else {
                holder.txt_perishable.setVisibility(View.GONE);
            }
        }
        return view;
    }

    private void initWheel(final OrderItem oi, WheelView wheelView) {
        wheelView.setPadding(0, 0, 20, 0);

        wheelView.setCyclic(true);
        wheelView.setTag(oi);
        wheelView.setVisibleItems(App.get(getContext()).getVisibleWheelItems());
        int position = oi.inPack() ? oi.getCountPack() : oi.getCount();
        //Квант для адаптера весового товара должен быть равным 1 (головка).
        float quant = oi.inPack() ? 1 : oi.getQuant();
        if (oi.mIsWeightGoods) {
            quant = 1;
        }
        //Для сыров (весового товара) делаем исключение. Также при продаже в упаковках
        boolean isFractional = oi.mIsWeightGoods || oi.inPack() ? false : oi.isFractional();

        short max = MAX_ITEMS;
        if (position > MAX_ITEMS) {
            max = (short) (position + 1);
        }
        String[] items = createAdapter(quant, max, isFractional);
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(
                getContext(), items);
        adapter.setTextSize(16);
        wheelView.setViewAdapter(adapter);
        wheelView.setCurrentItem(position);
        wheelView.removeScrollingListener(scrolledListener);
        wheelView.addScrollingListener(scrolledListener);
    }

    private void initWheelMeash(final OrderItem oi, WheelView wheelMeash) {
        wheelMeash.setTag(oi);
        wheelMeash.setVisibleItems(App.get(getContext()).getVisibleWheelItems());
        if (oi.mIsWeightGoods) {
            if (oi.inPack()) {
                wheelMeash.setVisibility(View.VISIBLE);
            } else {
                wheelMeash.setVisibility(View.GONE);
                return;
            }
        } else {
            wheelMeash.setVisibility(View.VISIBLE);
        }
        String[] listMeash = oi.mIsWeightGoods ? new String[]{"гол.", "кор."} : new String[]{"шт.", "уп."};
        int startPosition = oi.inPack() ? 1 : 0;
        // Adapter
        ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<>(
                getContext(), listMeash);
        adapter.setTextSize(16);
        wheelMeash.setViewAdapter(adapter);
        wheelMeash.setCurrentItem(startPosition);
        // listeners
        wheelMeash.removeScrollingListener(scrolledListener);
        wheelMeash.addScrollingListener(scrolledListener);
    }

    private String[] createAdapter(float quant, short items_count,
                                   boolean isFractional) {
        String[] result;
        try {
            result = new String[items_count];
        } catch (NegativeArraySizeException e) {
            result = new String[MAX_ITEMS];
        }
        // Если штуки
        for (int j = 0; j < items_count; j++) {
            float res = j * quant;
            result[j] = isFractional ? new DecimalFormat("#.##").format(res).replace(",", ".") : Integer
                    .toString((int) res);
        }
        return result;
    }

    private static class ViewHolder {
        CheckBox cbCheck;
        ImageView ivPic;
        TextView tvName;
        TextView tvPrice;
        TextView tvAmount;
        TextView tvWeight;
        TextView tvRestOnStore;
        WheelView wheelMeash;
        WheelView wheelCount;
        TextView txt_mercury;
        TextView txt_non_cash;
        TextView txt_is_custom;
        TextView txt_perishable;
    }

}