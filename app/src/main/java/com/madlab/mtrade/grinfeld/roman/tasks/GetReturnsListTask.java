package com.madlab.mtrade.grinfeld.roman.tasks;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.os.AsyncTask;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.iface.IGetStatus;


public class GetReturnsListTask extends AsyncTask<String, Integer, Boolean> {
    private final static String TAG = "!->GetReturnsListTask";
    private final static String COMMAND = "ВозвратыВОперативке";//"RETURNS_LIST";

    // private Context mContext;
    private String mAgentCode;
    private Date mRequestDate = Calendar.getInstance().getTime();
    private IGetStatus taskCompleteListener;
    private ArrayList<com.madlab.mtrade.grinfeld.roman.entity.Status> mItems;

    private Credentials connectInfo;

    //	private ProgressDialog progress;
    private String _1CUnswer = null;
    private String mUnswer = "";
    private String mPath;

    public GetReturnsListTask(Credentials info, String agent, Date requestDate, String path,
                              IGetStatus listener) {
        connectInfo = info;
        mAgentCode = agent;
        if (requestDate != null)
            mRequestDate = requestDate;

        mPath = path;

        taskCompleteListener = listener;
    }

    @Override
    protected void onPreExecute() {
        // Тут нам надо создать прогрессДиалог
    }

    @Override
    protected Boolean doInBackground(String... params) {
        if (params != null){
            _1CUnswer = params[0];
        }

        if (_1CUnswer == null){
            String query = String.format(Locale.ENGLISH, "%s;%s;%s;%s;%s;%s", "COMMAND", mAgentCode, COMMAND, mAgentCode,
                    TimeFormatter.sdf1C.format(mRequestDate), Const.END_MESSAGE);
            DalimoClient client = new DalimoClient(connectInfo);
            if (!client.connect()) {
                mUnswer = "Не удалось подключиться";
                return false;
            }

            client.send(query);

            _1CUnswer = client.receive();

            client.disconnect();
        }

        if (_1CUnswer == null || _1CUnswer.contains(DalimoClient.ERROR)) {
            mUnswer = _1CUnswer;
            return false;
        }

        // Во втором потоке запишем в файл
        ((Runnable) () -> {
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mPath);
                OutputStreamWriter writer = new OutputStreamWriter(fos);
                writer.write(_1CUnswer);
                writer.close();
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }).run();

        try {
            mItems = com.madlab.mtrade.grinfeld.roman.entity.Status.loadReturnsFromString(_1CUnswer);
        } catch (Exception e) {
            mItems = null;
            mUnswer = _1CUnswer;
        }

        if (mItems == null) {
            mUnswer = _1CUnswer;
        }

        return mItems != null;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        _1CUnswer = null;

        dismiss();

        if (taskCompleteListener != null) {
            taskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
//		if (progress != null) {
//			progress.hide();
//			progress.dismiss();
//		}
    }

    public String getUnswer() {
        return mUnswer;
    }

    public ArrayList<com.madlab.mtrade.grinfeld.roman.entity.Status> getItems() {
        return mItems;
    }

}
