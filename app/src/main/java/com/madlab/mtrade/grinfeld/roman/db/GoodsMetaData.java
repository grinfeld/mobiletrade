package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

import android.provider.BaseColumns;

public final class GoodsMetaData implements BaseColumns {
    private GoodsMetaData() {
    }

    public static final String TABLE_NAME = "GoodsInStor";

    public static final DBField FIELD_RECORD_TYPE = new DBField("RecType", 0);
    public static final DBField FIELD_PARENT = new DBField("ParentCode", 1);
    public static final DBField FIELD_CODE = new DBField("CodeGoods", 2);
    public static final DBField FIELD_NAME = new DBField("Name", 3);

    public static final DBField FIELD_NAME_FULL = new DBField("NameFull", 4);
    public static final DBField FIELD_BASE_MEASH = new DBField("BaseMeash", 5);
    public static final DBField FIELD_PACK_MEASH = new DBField("PackMeash", 6);
    public static final DBField FIELD_NUM_IN_PACK = new DBField("NumInPack", 7);

    public static final DBField FIELD_QUANT = new DBField("MinShip", 8);
    public static final DBField FIELD_REST = new DBField("RestOnStor", 9);

    public static final DBField FIELD_PRICE_CONTRACT = new DBField(
            "PriceContract", 10);
    public static final DBField FIELD_PRICE_STANDART = new DBField(
            "PriceStandart", 11);
    public static final DBField FIELD_PRICE_SHOP = new DBField("PriceShop", 12);
    public static final DBField FIELD_PRICE_REGION = new DBField("PriceRegion",
            13);
    public static final DBField FIELD_PRICE_MARKET = new DBField("PriceMarket",
            14);
    public static final DBField FIELD_PRICE_SECTION = new DBField(
            "PriceSection", 15);

    public static final DBField FIELD_BEST_BEFORE = new DBField("BestBefore",
            16);
    public static final DBField FIELD_NDS = new DBField("NDS", 17);
    public static final DBField FIELD_COUNTRY = new DBField("Country", 18);
    public static final DBField FIELD_WEIGHT = new DBField("WeightBrutto", 19);
    public static final DBField FIELD_HIGHLIGHT = new DBField("HighLight", 20);

    public static final DBField FIELD_IS_WEIGHT = new DBField("IsWeightGood", 21);
    public static final DBField FIELD_IS_HALFHEAD = new DBField("IsHalfHead", 22);
    public static final DBField FIELD_IS_PERISHABLE = new DBField("IsPerishable", 23);


    /**
     * Признак Хита продаж
     */
    public static final DBField FIELD_HIT = new DBField("Hit", 24);

    /**
     * Признак СТМ
     */
    public static final DBField FIELD_STM = new DBField("STM", 25);

    /**
     * Код ответственного менеджера
     */
    public static final DBField FIELD_MANAGER_FK = new DBField("GoodsManager_FK", 26);

    /**
     * Порядковый номер в файле
     */
    public static final DBField FIELD_SORT_ORDER = new DBField("SortOrder", 27);

    /**
     * Признак меркурия
     */
    public static final DBField FIELD_IS_MERCURY = new DBField("isMercury", 28);

    /**
     * Признак товар под заказ
     */
    public static final DBField FIELD_IS_CUSTOM = new DBField("isCustom", 29);

    /**
     * Признак "Запрещено Отписывать по безналу"
     */
    public static final DBField FIELD_IS_NON_CASH = new DBField("isNonCash", 30);

    /**
     * Не забываем увеличивать при добавлении новых полей, иначе умрет список
     * товаров
     */
    public static final byte FIELDS_COUNT = 27;

    public static final String INDEX = String.format(
            "CREATE INDEX idxGoodsInStoreBase ON %s ( %s, %s, %s )",
            TABLE_NAME, FIELD_RECORD_TYPE.Name, FIELD_PARENT.Name,
            FIELD_NAME.Name);

    public static final String INDEX_SORT_ORDER = String.format(
            "CREATE INDEX idxGoodsInStoreSortOrder ON %s (%s)", TABLE_NAME,
            FIELD_SORT_ORDER.Name);

    public static final String INDEX_HIT = String.format(
            "CREATE INDEX idxGoodsInStoreHit ON %s (%s)", TABLE_NAME,
            FIELD_HIT.Name);

    public static final String INDEX_STM = String.format(
            "CREATE INDEX idxGoodsInStoreSTM ON %s (%s)", TABLE_NAME,
            FIELD_STM.Name);

    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	smallint	    NOT NULL DEFAULT 0,"    // 0 - recordType
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '0',"    // 1 - FIELD_PARENT
                    + "%s	nvarchar(6)	    NOT NULL CONSTRAINT PK_GoodsInStor PRIMARY KEY," //2
                    + "%s	nvarchar(50)	NOT NULL DEFAULT '',"    // 3 - FIELD_NAME
                    + "%s	nvarchar(100)	NOT NULL DEFAULT '',"    // 4 - FIELD_NAME_FULL
                    + "%s	nvarchar(5)	    NOT NULL DEFAULT '',"    // 5 - FIELD_BASE_MEASH
                    + "%s	nvarchar(5)	    NOT NULL DEFAULT '',"    // 6 - FIELD_PACK_MEASH
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 7 - FIELD_NUM_IN_PACK
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 8 - FIELD_QUANT
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 9 - FIELD_REST
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 10 - FIELD_PRICE_CONTRACT
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 11 - FIELD_PRICE_STANDART
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 12 - FIELD_PRICE_SHOP
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 13 - FIELD_PRICE_REGION
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 14 - FIELD_PRICE_MARKET
                    + "%s	money		    NOT NULL DEFAULT 0,"    // 15 - FIELD_PRICE_SECTION
                    + "%s	nvarchar(60)	NOT NULL DEFAULT '',"    // 16 - FIELD_BEST_BEFORE
                    + "%s	smallint	    NOT NULL DEFAULT 0,"    // 17 - FIELD_BEST_NDS
                    + "%s	nvarchar(30)	NOT NULL DEFAULT '',"    // 18 - FIELD_COUNTRY
                    + "%s	money			NOT NULL DEFAULT 0,"    // 19 - FIELD_WEIGHT
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 20 - FIELD_HIGHLIGHT
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 21 - FIELD_IS_WEIGHT
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 22 - IsHalfHead
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 23 - Скоропорт
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 24 - признак Хит
                    + "%s	smallint		NOT NULL DEFAULT 0,"    // 25 - признак СТМ
                    + "%s	nvarchar(6)	    NOT NULL DEFAULT '',"    // 26 - FIELD_MANAGER_FK
                    + "%s	int				NOT NULL DEFAULT 0,"
                    + "%s   int, "
                    + "%s	int				NOT NULL DEFAULT 0,"
                    + "%s	int				NOT NULL DEFAULT 0)",    // 27 - Порядковый номер в файле
            TABLE_NAME,
            FIELD_RECORD_TYPE.Name, FIELD_PARENT.Name, FIELD_CODE.Name,
            FIELD_NAME.Name, FIELD_NAME_FULL.Name, FIELD_BASE_MEASH.Name,
            FIELD_PACK_MEASH.Name, FIELD_NUM_IN_PACK.Name, FIELD_QUANT.Name,
            FIELD_REST.Name, FIELD_PRICE_CONTRACT.Name,
            FIELD_PRICE_STANDART.Name, FIELD_PRICE_SHOP.Name,
            FIELD_PRICE_REGION.Name, FIELD_PRICE_MARKET.Name,
            FIELD_PRICE_SECTION.Name, FIELD_BEST_BEFORE.Name, FIELD_NDS.Name,
            FIELD_COUNTRY.Name, FIELD_WEIGHT.Name, FIELD_HIGHLIGHT.Name,
            FIELD_IS_WEIGHT.Name, FIELD_IS_HALFHEAD.Name,
            FIELD_IS_PERISHABLE.Name, FIELD_HIT.Name, FIELD_STM.Name,
            FIELD_MANAGER_FK.Name, FIELD_SORT_ORDER.Name, FIELD_IS_MERCURY.Name, FIELD_IS_CUSTOM.Name,
            FIELD_IS_NON_CASH.Name);

    public static String insertQuery(char recType, String parent, String code,
                                     String name, String baseMeash, String packMeash, String numInPack,
                                     String minShip, String rest, String pContract, String pStandart,
                                     String pShop, String pRegion, String pMarket, String pOpt,
                                     String bestBefore, String nds, String country, String weight,
                                     String fullName, String highLight, String isWeight,
                                     String halfHead, String isPerishable, String isHit,
                                     String isSTM, String manager_fk, int count, String isMercury,
                                     String isCustom, String isNonCash) {
        return String.format(Locale.ENGLISH,
                "INSERT INTO %s " + "(%s, %s, %s, " + // 1
                        "%s, %s, %s, " + // 2
                        "%s, %s, %s, " + // 3
                        "%s, %s, %s, " + // 4
                        "%s, %s, %s, " + // 5
                        "%s, %s, %s, " + // 6
                        "%s, %s, %s, " + // 7
                        "%s, %s, %s, " + // 8
                        "%s, %s, %s, " + // 9
                        "%s, %s, %s, %s" + //10
                        ") VALUES " +
                        "(%s, '%s', '%s', " + // 1
                        "'%s', '%s','%s', " + // 2
                        "%s, %s, %s, " + // 3
                        "%s, %s, %s, " + // 4
                        "%s, %s, %s, " + // 5
                        "'%s', %s, '%s', " + // 6
                        "%s, '%s', %s, " + // 7
                        "%s, %s, %s, " + // 8
                        "%s, %s, '%s', " + //9
                        "%d, %s, %s, %s)", // 10
                TABLE_NAME,
                // Fields
                FIELD_RECORD_TYPE.Name, FIELD_PARENT.Name, FIELD_CODE.Name, // 1
                FIELD_NAME.Name, FIELD_BASE_MEASH.Name, FIELD_PACK_MEASH.Name, // 2
                FIELD_NUM_IN_PACK.Name, FIELD_QUANT.Name, FIELD_REST.Name, // 3
                FIELD_PRICE_CONTRACT.Name, FIELD_PRICE_STANDART.Name, FIELD_PRICE_SHOP.Name, // 4
                FIELD_PRICE_REGION.Name, FIELD_PRICE_MARKET.Name, FIELD_PRICE_SECTION.Name, // 5
                FIELD_BEST_BEFORE.Name, FIELD_NDS.Name, FIELD_COUNTRY.Name, // 6
                FIELD_WEIGHT.Name, FIELD_NAME_FULL.Name, FIELD_HIGHLIGHT.Name,  // 7
                FIELD_IS_WEIGHT.Name, FIELD_IS_HALFHEAD.Name, FIELD_IS_PERISHABLE.Name, // 8
                FIELD_HIT.Name, FIELD_STM.Name, FIELD_MANAGER_FK.Name,//9
                FIELD_SORT_ORDER.Name, //10
                FIELD_IS_MERCURY.Name,
                FIELD_IS_CUSTOM.Name,
                FIELD_IS_NON_CASH.Name,
                // Values
                recType, parent, code, // 1
                name, baseMeash, packMeash, // 2
                numInPack, minShip, rest, // 3
                pContract, pStandart, pShop, // 4
                pRegion, pMarket, pOpt, // 5
                bestBefore, nds, country, // 6
                weight, fullName, highLight, // 7
                isWeight, halfHead, isPerishable, // 8
                isHit, isSTM, manager_fk,    //9
                count, isMercury, isCustom, isNonCash); // 10
    }

    /**
     * Текст запроса для выбора всех элементов таблицы
     */
    public final static String QuerySelectAll = String.format(
            "SELECT * FROM %s " + "WHERE %s = ? ORDER BY %s", TABLE_NAME,
            FIELD_PARENT.Name, FIELD_SORT_ORDER.Name);

    /**
     * Текст запроса для подсчета дочерних элементов
     */
    public final static String QuerySubItemsCount = String.format(
            "SELECT DISTINCT COUNT(%s) " + "FROM %s "
                    + "WHERE %s = ? AND %s = 0 AND %s = 1", GoodsMetaData.FIELD_CODE.Name,
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_PARENT.Name,
            GoodsMetaData.FIELD_RECORD_TYPE.Name, GoodsMetaData.FIELD_STM.Name);

    /**
     * Текст запроса для подсчета дочерних элементов, с объединением по матрице
     */
    public final static String QuerySubItemsCountMatrix = String.format(
            "SELECT Count(%s.%s) " + "FROM %s " + "JOIN %s "
                    + "ON %s.%s = %s.%s " + "WHERE %s.%s = ? AND " + "%s = ?",
            // SELECT
            MatrixMetaData.TABLE_NAME,
            MatrixMetaData.FIELD_CODE_GOODS,
            // FROM
            MatrixMetaData.TABLE_NAME,
            // JOIN
            GoodsMetaData.TABLE_NAME,
            // ON
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_CODE.Name,
            MatrixMetaData.TABLE_NAME, MatrixMetaData.FIELD_CODE_GOODS,
            // WHERE
            GoodsMetaData.TABLE_NAME, GoodsMetaData.FIELD_PARENT.Name,
            MatrixMetaData.FIELD_CODE_CLIENT);
}
