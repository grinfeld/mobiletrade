package com.madlab.mtrade.grinfeld.roman.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.GlobalProc;
import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;

import java.util.ArrayList;

/**
 * Created by GrinfeldRA on 22.11.2017.
 */

public class ReturnGoodsAdapter extends ArrayAdapter<ReturnItem> {
    private ArrayList<ReturnItem> mItems;
    private boolean isForeign;
    // private int mLastPosition = 0;

    public ReturnGoodsAdapter(Context context, int textViewResourceId, final ArrayList<ReturnItem> items, boolean isForeign) {
        super(context, textViewResourceId, items);
        mItems = items;
        this.isForeign = isForeign;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;

        ViewHolder holder;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.return_goods_item, null);

            holder = new ViewHolder();
            // holder.setLayout((RelativeLayout)v);

            holder.setName((TextView) view.findViewById(R.id.rgi_goodsName));
            holder.setCount((TextView) view.findViewById(R.id.rgi_goodsCount));
            holder.setPrice((TextView) view.findViewById(R.id.rgi_goodsPrice));
            holder.setAmount((TextView) view.findViewById(R.id.rgi_goodsAmount));
            holder.setWeight((TextView) view.findViewById(R.id.rgi_goodsWeight));
            holder.tvMercury = view.findViewById(R.id.txt_mercury);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        ReturnItem ri = null;
        if (mItems != null) {
            ri = mItems.get(position);
        }

        if (ri != null) {
            // holder.changeBackground(position % 2 == 0);
            // Имя товара
            String name = ri.getNameGoods();
            holder.setNameText(name);

            // Количество
            int count = ri.inPack() ? (int) (ri.getCountPack() * ri.numInPack()) : (int) ri.getCount();
            String meash = ri.mIsWeightGoods ? Const.POSTFIX_WEIGHT_GOODS : Const.POSTFIX_PIECE_GOODS;
            String txtCnt = String.format("%d%s", count, meash);

            holder.setCountText(txtCnt);

            // Цена
            holder.setPriceText(GlobalProc.toForeignPrice(ri.getPrice(), isForeign));

            // Итог
            holder.setAmountText(GlobalProc.toForeignPrice(ri.getAmount(), isForeign));

            // Вес

            holder.setWeightText(GlobalProc.formatWeight(ri.getWeight() * count)
                    + Const.POSTFIX_WEIGHT);

            if (ri.getIsMercury() == 1) {
                holder.tvMercury.setVisibility(View.VISIBLE);
            } else {
                holder.tvMercury.setVisibility(View.GONE);
            }
        } else {
            Log.w("ADAPTER", "ri == null");
        }
        return view;
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        private TextView tvName;
        private TextView tvCount;
        private TextView tvPrice;
        private TextView tvAmount;
        private TextView tvWeight;
        private TextView tvMercury;

        public void setName(TextView textView) {
            tvName = textView;
        }

        public void setCount(TextView textView) {
            tvCount = textView;
        }

        public void setPrice(TextView textView) {
            tvPrice = textView;
        }

        public void setAmount(TextView textView) {
            tvAmount = textView;
        }

        public void setWeight(TextView textView) {
            tvWeight = textView;
        }

        public void setNameText(String name) {
            if (tvName != null) {
                tvName.setText(name);
            }
        }

        public void setCountText(String count) {
            if (tvCount != null) {
                tvCount.setText(count);
            }
        }

        public void setPriceText(String price) {
            if (tvPrice != null) {
                tvPrice.setText(price);
            }
        }

        public void setAmountText(String amount) {
            if (tvAmount != null) {
                tvAmount.setText(amount);
            }
        }

        public void setWeightText(String weight) {
            if (tvWeight != null) {
                tvWeight.setText(weight);
            }
        }
    }
}