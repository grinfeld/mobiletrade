package com.madlab.mtrade.grinfeld.roman.entity;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.madlab.mtrade.grinfeld.roman.db.ManagerMetaData;

/**
 * Created by GrinfeldRA on 15.01.2018.
 */

public class ManagerCursorWrapper extends CursorWrapper {

    public ManagerCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Manager getManager() {
        Manager result;

        String code = getString(ManagerMetaData.FIELD_CODE_INDEX);
        String fio = getString(ManagerMetaData.FIELD_FIO_INDEX);
        String phone = getString(ManagerMetaData.FIELD_PHONE_INDEX);
        String email = getString(ManagerMetaData.FIELD_EMAIL_INDEX);

        result = new Manager(code, fio, phone, email);

        return result;
    }

}

