package com.madlab.mtrade.grinfeld.roman.db;

import java.util.Locale;

/**
 * Created by GrinfeldRa
 */
public class PromotionCheckedMetaData {

    public static final String TABLE_NAME = "PromotionChecked";
    public static final String FIELD_ID = "id_promotion";
    public static final String FIELD_CHECK = "promotion_check";
    public static final String FIELD_REGION = "promotion_region";

    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s integer,"
                    + "%s integer NOT NULL DEFAULT 0,"
                    + "%s integer, "
                    + "UNIQUE(%s, %s) ON CONFLICT IGNORE);",
            TABLE_NAME, FIELD_ID, FIELD_CHECK, FIELD_REGION, FIELD_ID, FIELD_REGION);


    public static String insertQuery(int id, int regionId) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s) "
                        + "VALUES "
                        + "(%d, %d)",
                TABLE_NAME, FIELD_ID, FIELD_REGION,
                id, regionId);
    }

}
