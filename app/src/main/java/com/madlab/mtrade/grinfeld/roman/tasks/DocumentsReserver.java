package com.madlab.mtrade.grinfeld.roman.tasks;

/**
 * Created by GrinfeldRA on 29.11.2017.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.db.VisitReservedMetadata;
import com.madlab.mtrade.grinfeld.roman.entity.PaybackPhoto;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;
import com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.madlab.mtrade.grinfeld.roman.Const.COMMAND;


/**
 * Класс для резервирования визитов
 *
 * @param <Token> Тип документа
 * @author Konovaloff
 */
public class DocumentsReserver<Token> extends HandlerThread {

    private final static String PROC_NAME = "СформироватьВизит";

    public static final byte RESULT_SUCCESS = 1;
    public static final byte RESULT_FAIL = 0;
    public static final byte RESULT_NO_CONNECT = 2;

    private static final String TAG = "DocumentsReserver";
    private static final int MESSAGE_DOWNLOAD = 0;


    Credentials mCredentials;
    Handler mResponseHandler;
    String mCodeAgent;
    Map<Visit, String> requestMap = Collections
            .synchronizedMap(new HashMap<Visit, String>());

    Listener<Token> mListener;

    public interface Listener<Token> {
        void onReserved(Visit token, byte result, byte typeReserveServer);
    }

    public void setListener(Listener<Token> listener) {
        mListener = listener;
    }

    /**
     * Здесь нам понадобятся данные для подключения
     *
     * @param responseHandler
     * @param credentials
     */
    public DocumentsReserver(String codeAgent, Handler responseHandler, Credentials credentials) {
        super(TAG);

        mCodeAgent = codeAgent;

        mResponseHandler = responseHandler;
        mCredentials = credentials;
    }

    @SuppressLint("HandlerLeak")
    @Override
    protected void onLooperPrepared() {
        mResponseHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == MESSAGE_DOWNLOAD) {
                    Visit token = (Visit) msg.obj;
                    Log.i(TAG, "Got a request for visit: " + requestMap.get(token));
                    handleRequest(token);
                }
            }
        };
    }

    public void queueVisit(Visit visit, String url) {
        requestMap.put(visit, url);
        mResponseHandler.obtainMessage(MESSAGE_DOWNLOAD, visit).sendToTarget();
    }


    /**
     * Запускаем процесс резервирования
     * <Менеджер>;<Контрагент>;<Широта>;<Долгота>;<Погрешность>;
     * <ФлагЗаявка>;<ФлагВозврат
     * >;<ФлагДеньги>;<Время>;<SKUср>;<SKUзаяв>;<СуммаСр>;<СуммаЗаяв>;
     * 12;С01201;С14906;53,202427;50,260990;22,30;0;0;0;20.08.2018 12:55;0;0;0,00;0,00;;
     * COMMAND;С01201;СформироватьВизит;1;С01201;С14906;37,421997;-122,084000;20,00;0;0;0;16.08.2018 07:17;0;0;0,00;0,00;;<EOF>
     *
     * @param visit
     */
    private void handleRequest(final Visit visit) {
        Log.d(TAG, "СформироватьВизит run");
        for (PaybackPhoto paybackPhoto : visit.getPaybackPhotos()) {
            Log.d(TAG, paybackPhoto.getUrl() + " " + visit.getID().toString());
        }
        final String uuid = requestMap.get(visit);
        if (uuid == null || visit == null || visit.getLocation() == null)
            return;
        final byte[] res = {RESULT_FAIL};
        Location loc = visit.getLocation();
        @SuppressLint("DefaultLocale")
        String command = String.format("%s;%s;%s;", COMMAND, mCodeAgent, PROC_NAME);
        String codeClient = "X00000";
        if (visit.client() != null && visit.client().getCode() != null) {
            codeClient = visit.client().getCode();
        }
        @SuppressLint("DefaultLocale")
        String query = String.format("%d;%s;%s;" + "%.6f;%.6f;%.2f;" + "%d;%d;%d;"
                        + "%s;%d;%d;" + "%.2f;%.2f;%s;%s;%s;",
                visit.cnt(),
                mCodeAgent,
                codeClient,
                loc.getLatitude(), loc.getLongitude(), loc.getAccuracy(),
                visit.hasOrder() ? 1 : 0, visit.hasReturns() ? 1 : 0,
                visit.hasPayments() ? 1 : 0,
                TimeFormatter.formatTo1CDateTimeGMT(visit.getStartTime()),
                visit.client().mSKU,
                visit.orderSKU(), visit.client().mAverageOrder,
                visit.orderSum(), visit.result(), uuid, visit.getEndTime() == 0 ? "" : TimeFormatter.formatTo1CDateTimeGMT(visit.getEndTime()));

        if (App.get(MyApp.getContext()).connectionServer.equals("1")) {
            // Подключаемся и пробуем зарезервировать /////////////10 мин до конца раб дня copy paste fixme
            DalimoClient client = new DalimoClient(mCredentials);
            if (client.connect()) {
                client.send(command + query + Const.END_MESSAGE);
                String unswer = client.receive();
                if (unswer != null && unswer.contains("OK")) {
                    res[0] = RESULT_SUCCESS;
                    // По идее, обновления признака в БД можно разместить здесь.
                }
            } else {
                res[0] = RESULT_NO_CONNECT;
            }
            onResult(res[0], visit, uuid, JournalFragment.APP_SERVER);
        } else {
            String region = App.getRegion(MyApp.getContext());
            OkHttpClientBuilder.buildCall("СформироватьВизит", query, region).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    DalimoClient client = new DalimoClient(mCredentials);
                    if (client.connect()) {
                        client.send(command + query + Const.END_MESSAGE);
                        String unswer = client.receive();
                        if (unswer != null && unswer.contains("OK")) {
                            res[0] = RESULT_SUCCESS;
                            // По идее, обновления признака в БД можно разместить здесь.
                        }
                    } else {
                        res[0] = RESULT_NO_CONNECT;
                    }
                    onResult(res[0], visit, uuid, JournalFragment.APP_SERVER);
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

                    try {
                        String strResponse = response.body().string();
                        Log.d(TAG, "СформироватьВизит " + strResponse);
                        JSONObject jsonObject = new JSONObject(strResponse);
                        String data = jsonObject.getString("data");
                        if (data.equals("OK<EOF>")) {
                            res[0] = RESULT_SUCCESS;
                            onResult(res[0], visit, uuid, JournalFragment.IIS_SERVER);
                        }
                    } catch (JSONException e) {
                        DalimoClient client = new DalimoClient(mCredentials);
                        if (client.connect()) {
                            client.send(command + query + Const.END_MESSAGE);
                            String unswer = client.receive();
                            if (unswer != null && unswer.contains("OK")) {
                                res[0] = RESULT_SUCCESS;
                                // По идее, обновления признака в БД можно разместить здесь.
                            }
                        } else {
                            res[0] = RESULT_NO_CONNECT;
                        }
                        onResult(res[0], visit, uuid, JournalFragment.APP_SERVER);
                    }
                }
            });
        }

    }


    private void onResult(byte res, Visit visit, String uuid, byte typeReserveServer) {
        try {
            MyApp app = (MyApp) MyApp.getContext().getApplicationContext();
            SQLiteDatabase db = app.getDB();
            // Удаляем документ из очереди и передаем сообщение в основной поток
            if (res == RESULT_SUCCESS) {
                String region = App.getRegion(MyApp.getContext());
                ArrayList<PaybackPhoto> paybackPhotos = visit.getPaybackPhotos();
                for (PaybackPhoto paybackPhoto : paybackPhotos) {
                    Response response = OkHttpClientBuilder.buildCall("ПривязатьФотоКВизиту", String.format("%s;%s;", visit.getID().toString(), paybackPhoto.getUrl()), region).execute();
                    if (response.isSuccessful()) {
                        ResponseBody responseBody = response.body();
                        if (responseBody != null) {
                            String string1 = responseBody.string();
                            if (string1.contains("OK<EOF>")) {
                                DBHelper.execMultipleSQL(db, new String[]{PaybackPhotoMetaData.deleteQuery(paybackPhoto.getId())});
                            } else if (string1.contains("не поддерживатеся [EREST]")) {
                                res = RESULT_FAIL;
                            } else {
                                res = RESULT_FAIL;
                            }
                        }
                    } else {
                        res = RESULT_FAIL;
                    }
                }
            }
            if (res == RESULT_SUCCESS) {
                ContentValues cv = new ContentValues();
                cv.put(VisitReservedMetadata.FIELD_IS_RESERVED, 1);
                db.update(VisitReservedMetadata.TABLE_NAME, cv,
                        VisitReservedMetadata.FIELD_UUID + "='" + visit.getID().toString()+"'",
                        null);
            }
            final byte result = res;
            mResponseHandler.post(() -> {
                if (requestMap.get(visit) != uuid)
                    return;
                requestMap.remove(visit);
                mListener.onReserved(visit, result, typeReserveServer);
            });
        } catch (Exception e) {

        }
    }


    public int getItemsCount() {
        if (requestMap != null) {
            return requestMap.size();
        }

        return 0;
    }

    public void clearQueue() {
        mResponseHandler.removeMessages(MESSAGE_DOWNLOAD);
        requestMap.clear();
    }
}