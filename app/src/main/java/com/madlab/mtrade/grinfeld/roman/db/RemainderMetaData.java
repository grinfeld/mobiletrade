package com.madlab.mtrade.grinfeld.roman.db;

import android.util.Log;

import java.util.Locale;

/**
 * Created by GrinfeldRA on 28.12.2017.
 */

public class RemainderMetaData {

    public static final String TABLE_NAME = "Remainder";

    /**
     * Код товара
     */
    public static final String FIELD_CODE = "GoodsCode";

    /**
     * № Склада
     */
    public static final String FIELD_SCLAD = "ScladName";

    /**
     * Остаток на сладе
     */
    public static final String FIELD_REMAINDER = "FieldRemainder";

    /**
     * Резерв
     */
    public static final String FIELD_RESERVE = "FieldReserve";


    public static final byte INDEX_FIELD_CODE = 0;
    public static final byte INDEX_FIELD_SCLAD = 1;
    public static final byte INDEX_FIELD_REMAINDER = 2;
    public static final byte INDEX_FIELD_RESERVE = 3;


    public static final String CLEAR = "DELETE FROM " + TABLE_NAME;

    public static final String CREATE_COMMAND = String.format(
            "CREATE TABLE %s ("
                    + "%s	nvarchar(25)	NOT NULL DEFAULT '',"
                    + "%s	nvarchar(25)	NOT NULL DEFAULT '',"
                    + "%s	nvarchar(25)	NOT NULL DEFAULT '',"
                    + "%s	nvarchar(20)	NOT NULL DEFAULT '')",
            TABLE_NAME,
            FIELD_CODE, FIELD_SCLAD, FIELD_REMAINDER, FIELD_RESERVE);

    public static String insertQuery(String code, String sclad, String remainder, String reserve) {
        return String.format(Locale.ENGLISH, "INSERT INTO %s "
                        + "(%s, %s, %s, %s) "
                        + "VALUES "
                        + "('%s', '%s', '%s', '%s')",
                TABLE_NAME, FIELD_CODE, FIELD_SCLAD, FIELD_REMAINDER, FIELD_RESERVE,
                code, sclad, remainder, reserve);
    }

}
