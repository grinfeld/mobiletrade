package com.madlab.mtrade.grinfeld.roman.adapters;


import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.entity.Client;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.R;

public class ClientAdapter extends ArrayAdapter<Client> {
    private ArrayList<Client> mItems;
    private int mSelected = -1;

    // private float titleSize;
    // private float addresSize;

    public ClientAdapter(Context context, ArrayList<Client> items) {
        super(context, R.layout.client_list_item, items);
        this.mItems = items;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.client_list_item, null);
        }

        if (mItems == null)
            return v;

        Client c = mItems.get(position);
        if (c != null) {
            RelativeLayout rl = (RelativeLayout)v;
            if (rl != null) {
                if (mSelected > -1 && mSelected == position) {
                    rl.setBackgroundColor(Color.DKGRAY);
                } else {
                    rl.setBackgroundColor(Color.TRANSPARENT);
                }
            }

            TextView itemId = v.findViewById(R.id.tv_cliName);
            View view = v.findViewById(R.id.img_type_sales_plan);
            if (c.typeSalesPlan==1){
                view.setVisibility(View.VISIBLE);
            }else {
                view.setVisibility(View.GONE);
            }
            View img_foreign_agent = v.findViewById(R.id.img_foreign_agent);
            if (c.isForeignAgent()){
                img_foreign_agent.setVisibility(View.VISIBLE);
            }else {
                img_foreign_agent.setVisibility(View.GONE);
            }
            if (itemId != null) {
                String s = c.mName;
                itemId.setText(s);
                if (c.mIsStop) {
                    itemId.setTextColor(Color.RED);
                } else if (c.visited()){
                    itemId.setTextColor(Color.CYAN);
                }else{
                    itemId.setTextColor(Const.COLOR_GOLD);
                }
            }

            TextView itemLabel = v.findViewById(R.id.tv_cliPost);
            if (itemLabel != null) {
                String s = c.mPostAddress;
                itemLabel.setText(s);
            }
        }
        return v;
    }

    public void setSelected(int index) {
        mSelected = index;
    }

    @Override
    public int getCount() {
        return mItems == null ? 0 : mItems.size();
    }
}
