package com.madlab.mtrade.grinfeld.roman.tasks;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.Const;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.TimeFormatter;
import com.madlab.mtrade.grinfeld.roman.connectivity.Credentials;
import com.madlab.mtrade.grinfeld.roman.connectivity.DalimoClient;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.PaybackPhoto;
import com.madlab.mtrade.grinfeld.roman.entity.ReturnItem;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.fragments.JournalFragment;
import com.madlab.mtrade.grinfeld.roman.iface.ISetReservComplete;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by GrinfeldRA on 24.11.2017.
 */

public class SetReservReturnTask extends AsyncTask<Short, Void, Boolean> {
    private final static String PROC_NAME = "СформироватьДокументыВозврата";

    private final static String SUCCESS = "OK";
    private final static String FAILURE = "ERR";

    private final static String WAIT_MESSAGE = "Связь с сервером";

    private Credentials connectInfo;

    private String mUnswer;

    private Context baseContext;
    private ProgressDialog mProgress;
    private Boolean isWasSended = true;
    private Returns returns;

    private ISetReservComplete mTaskCompleteListener;

    public SetReservReturnTask(Context data, ISetReservComplete taskCompleteListener) {
        baseContext = data;
        mTaskCompleteListener = taskCompleteListener;
        mUnswer = "";
        connectInfo = Credentials.load(baseContext);
    }

    public void setWasSended(boolean isWasSended) {
        this.isWasSended = isWasSended;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public String getMessage() {
        return mUnswer;
    }

    @Override
    protected void onPreExecute() {
        mProgress = new ProgressDialog(baseContext);
        mProgress.setMessage(WAIT_MESSAGE);
        mProgress.setCancelable(true);
        mProgress.show();
    }

    @Override
    protected Boolean doInBackground(Short... numbers) {
        // Если будет ошибка в catch - выставим флаг
        boolean wasError = false;

        SQLiteDatabase db;
        DalimoClient myClient;

        try {
            MyApp app = (MyApp) baseContext.getApplicationContext();
            String region = App.getRegion(MyApp.getContext());
            db = app.getDB();
            App settings = App.get(baseContext);
            for (Short current : numbers) {
                Returns data;
                data = Returns.load(db, current);
                if (data == null && returns != null) {
                    data = returns;
                }
                if (data.wasSended()) {
                    mUnswer += "Повторная отправка возврата недопустима\r\n";
                    continue;
                }
                StringBuilder res = new StringBuilder(Const.COMMAND + ";");
                res.append(data.getCodeAgent());
                res.append(";");
                res.append(PROC_NAME);
                res.append(";");
                String message = generateDataStringForReserv(data, settings.connectionServer);
                res.append(message);
                int typeServer = 0;
                String serverUnswer;
                if (settings.connectionServer.equals("1")) {
                    myClient = new DalimoClient(connectInfo);

                    if (!myClient.connect()) {
                        wasError = true;
                        mUnswer += "Не удалось подключиться\r\n";
                        continue;
                    }
                    myClient.send(res.toString());
                    serverUnswer = myClient.receive();
                    typeServer = JournalFragment.APP_SERVER;
                    myClient.disconnect();
                } else {

//                    StringBuilder stringBuilder = new StringBuilder(message);
//                    message = stringBuilder.replace(stringBuilder.lastIndexOf(";"), stringBuilder.lastIndexOf(";") + 1, "").toString();
                    try {
                        Response response = OkHttpClientBuilder.buildCall(PROC_NAME, message, region).execute();
                        if (response.isSuccessful()) {
                            String strResponse = response.body().string();
                            JSONObject jsonObject = new JSONObject(strResponse);
                            serverUnswer = jsonObject.getString("data");
                            typeServer = JournalFragment.IIS_SERVER;
                        } else {
                            serverUnswer = FAILURE;
                            wasError = true;
                        }
                    } catch (Exception e) {
                        myClient = new DalimoClient(connectInfo);

                        if (!myClient.connect()) {
                            wasError = true;
                            mUnswer += "Не удалось подключиться\r\n";
                            continue;
                        }
                        myClient.send(res.toString());
                        serverUnswer = myClient.receive();
                        typeServer = JournalFragment.APP_SERVER;
                        myClient.disconnect();
                    }
                }

                // А теперь обрабатываем полученное сообщение
                if (serverUnswer.contains(FAILURE)) {
                    wasError = true;
                    mUnswer += serverUnswer + Const.NewLine;
                    continue;
                }

                // В случае успеха - обновляем признак
                if (serverUnswer.contains(SUCCESS)) {
                    ArrayList<PaybackPhoto> paybackPhotos = data.getPaybackPhotos();
                    if (paybackPhotos.size() == 0) {
                        data.updateSendSign(db, true, typeServer);
                    } else {
                        boolean isComplete = false;
                        for (PaybackPhoto paybackPhoto : paybackPhotos) {
                            Response response = OkHttpClientBuilder.buildCall("ПривязатьФотоКВозврату", String.format("%s;%s;", data.getUuidDoc().toString(), paybackPhoto.getUrl()), region).execute();
                            if (response.isSuccessful()) {
                                ResponseBody responseBody = response.body();
                                if (responseBody != null) {
                                    String string1 = responseBody.string();
                                    if (string1.contains("OK<EOF>")) {
                                        DBHelper.execMultipleSQL(db, new String[]{PaybackPhotoMetaData.deleteQuery(paybackPhoto.getId())});
                                        isComplete = true;
                                    } else if (string1.contains("не поддерживатеся [EREST]")) {
                                        isComplete = false;
                                    } else {
                                        isComplete = false;
                                    }
                                }
                            } else {
                                isComplete = false;
                            }
                        }
                        if (isComplete) {
                            data.updateSendSign(db, true, typeServer);
                        }
                    }
                }
            }
        } catch (Exception e) {
            mUnswer = e.toString();
            wasError = true;
        }
        return !wasError;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        dismiss();

        if (mTaskCompleteListener != null) {
            mTaskCompleteListener.onTaskComplete(this);
        }
    }

    private void dismiss() {
        if (mProgress != null) {
            mProgress.dismiss();
            mProgress.hide();
        }
    }


    private String generateDataStringForReserv(Returns data, String type) {
        StringBuilder res = new StringBuilder();
        SimpleDateFormat sdf1C = new SimpleDateFormat("dd.MM.yy", Locale.getDefault());
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("НомерЗаявки", data.getNumReturn());
            jsonObject.put("СостояниеЗаявки", (data.mState == Returns.State.Normal) ? Returns.STATE_NORMAL : Returns.STATE_DELETED);
            jsonObject.put("КодМенеджера", data.getCodeAgent());
            jsonObject.put("Клиент", data.getClient().getCode());
            jsonObject.put("ТипОплаты", data.mTypeMoney);
            jsonObject.put("НомерДокументаПокупателя", data.mDocNum);
            jsonObject.put("ДатаСоздания", sdf1C.format(data.getDateReturn()));
            jsonObject.put("НомерНакладной", data.getInvoice());
            jsonObject.put("ДатаВозврата", sdf1C.format(data.getDateGoodsBack()));
            jsonObject.put("ВезуСам", data.getPickup() ? 1 : 0);
            jsonObject.put("Комментарий", data.mNote);
            jsonObject.put("КодФирмы", data.firmFK());
            jsonObject.put("ТипАкта", data.mTypeAct);
            if (data.getUuidDoc() != null && !data.getUuidDoc().toString().isEmpty()) {
                jsonObject.put("ИдентификаторВозврата", data.getUuidDoc().toString());
            }
            String orderCommon = String.format(Locale.US, "%d;%s;%s;%s;%s;%s;%s;%s;%s;%d;%s;%03d;"
                            + "%s;",// резервные поля
                    data.getNumReturn(), // 0
                    (data.mState == Returns.State.Normal) ? Returns.STATE_NORMAL
                            : Returns.STATE_DELETED, // 1
                    data.getCodeAgent(), // 2
                    data.getClient().getCode(), // 3
                    data.mTypeMoney, // 4 П/Н
                    data.mDocNum, // 5
                    sdf1C.format(data.getDateReturn()), // 6
                    data.getInvoice(), // 7 Номер нашего документа
                    sdf1C.format(data.getDateGoodsBack()), // 8 Когда забрать возврат
                    data.getPickup() ? 1 : 0, // 9
                    data.mNote, // 10
                    data.firmFK(), // 11 код фирмы
                    data.mTypeAct // 12 тип акта возврат\потеря\выкуп
            );


            res.append(orderCommon);

            String forma = "%s|%.3f|%.2f|;";
            JSONArray jsonArray = new JSONArray();
            for (ReturnItem item : data.getItems()) {
                //Если товар весовой - килограммы
                float count = item.getWeight();
                // Если в упаковках = количество коробок, иначе - штук
                if (!item.mIsWeightGoods) {
                    count = item.inPack() ? item.getCountPack() * item.numInPack() : item.getCount();
                }
                res.append(String.format(Locale.ENGLISH, forma,
                        item.getCodeGoods(), // 0
                        count, // 1
                        item.getPrice() // 2
                ));
                JSONObject object = new JSONObject();
                object.put("КодТовара", item.getCodeGoods());
                object.put("Количество", String.format(Locale.ENGLISH, "%.3f", count));
                object.put("Цена", String.format(Locale.ENGLISH, "%.2f", item.getPrice()));
                jsonArray.put(object);
            }
            jsonObject.put("ТабличнаяЧасть", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        res.append(Const.END_MESSAGE);
        return type.equals("1") ? res.toString() : jsonObject.toString();
    }

}