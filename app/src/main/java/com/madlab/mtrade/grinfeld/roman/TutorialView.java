//package com.madlab.mtrade.grinfeld.roman;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v4.content.ContextCompat;
//
//import com.hololo.tutorial.library.Step;
//import com.hololo.tutorial.library.TutorialActivity;
//
///**
// * Created by grinfeldra
// */
//public class TutorialView extends TutorialActivity {
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.pref_is_viewed_tutorial), false)) {
//            setPrevText("Назад");
//            setNextText("Далее");
//            setFinishText("Готово");
//            setCancelText("Выйти");
//            addFragment(new Step.Builder()
//                    //.setContent("Товар с признаком  \"Только Н\", продается только по непроводным документам")
//                    .setBackgroundColor(ContextCompat.getColor(this, R.color.colorMainBack))
//                    .setDrawable(R.drawable.tutorial1)
//                    .build());
//            addFragment(new Step.Builder()
//                    .setContent("При не правильном выборе оплаты, программа напомнит вам об этом.")
//                    .setBackgroundColor(ContextCompat.getColor(this, R.color.colorMainBack))
//                    .setDrawable(R.drawable.tutorial2)
//                    .build());
//        } else {
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
//            finish();
//        }
//    }
//
//
//    private void pref_viewed_tutorial() {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putBoolean(getString(R.string.pref_is_viewed_tutorial), true);
//        editor.apply();
//    }
//
//    @Override
//    public void currentFragmentPosition(int position) {
//
//    }
//
//    @Override
//    public void finishTutorial() {
//        pref_viewed_tutorial();
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//        finish();
//    }
//}
