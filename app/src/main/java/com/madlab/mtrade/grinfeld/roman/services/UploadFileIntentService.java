package com.madlab.mtrade.grinfeld.roman.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Base64;
import android.util.Log;

import com.madlab.mtrade.grinfeld.roman.App;
import com.madlab.mtrade.grinfeld.roman.MyApp;
import com.madlab.mtrade.grinfeld.roman.connectivity.ApiC3;
import com.madlab.mtrade.grinfeld.roman.connectivity.ApiPromotions;
import com.madlab.mtrade.grinfeld.roman.connectivity.OkHttpClientBuilder;
import com.madlab.mtrade.grinfeld.roman.db.VisitReservedMetadata;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.entity.Visit;

import org.json.JSONObject;

public class UploadFileIntentService extends IntentService {
    public static final String EXTRA_PAYBACK_RESERVED = "is_reserved";
    private static final String TAG = "!->UploadFileAsyncTask";

    public static final String EXTRA_FILES_LIST = "FILES_LIST";
    public static final String EXTRA_STATUS = "TaskStatus";
    public static final String EXTRA_MESSAGE = "ErrorMessage";

    public static final String BROADCAST_ACTION = "com.dalimo.konovalov.mtrade.uploadphotos";

    public static final String URL_C3 = "https://hb.bizmrg.com";

    private static final MediaType IMAGE_JPG = MediaType.parse("image/jpeg");

    final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");

    public static final byte STATUS_OK = Byte.MAX_VALUE;
    public static final byte STATUS_ERROR = Byte.MIN_VALUE;

    private byte status = STATUS_OK;
    private String mErrorText = "Не удалось отправить";

    private ArrayList<String> successful = new ArrayList<String>();

    public UploadFileIntentService() {
        super(BROADCAST_ACTION);
    }


    @Override
    protected void onHandleIntent(Intent extras) {
        boolean isMainServer = true;
        ResultReceiver receiver = extras.getParcelableExtra("receiver");
        String[] paths = extras.getStringArrayExtra(EXTRA_FILES_LIST);
        if (paths != null && paths.length > 0) {
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build();
            int i = 1;
            for (String path : paths) {
                File file = new File(path);
                String name = file.getName();
                String[] split = name.split("_");
                String codeAgent = split[0];
                String codeClient = split[1];
                String date = split[2];
                if (split[2].length() == 8) {
                    date = split[2].substring(0, 4).concat("-").concat(split[2].substring(4, 6)).concat("-").concat(split[2].substring(6, 8));
                }
                String timeStamp = split[3];
                String codeFirm = split[4];
                String uuid = "";
                String returnUUID = "";
                String fileName = file.getName();
                if (split.length == 8) {
                    uuid = split[6];
                    returnUUID = split[7].substring(0, split[7].indexOf("."));
                    int i1 = name.lastIndexOf("_", name.lastIndexOf("_") - 1);
                    fileName = name.substring(0, i1).concat(".jpg");
                } else if (split.length == 7) {
                    uuid = split[6];
                    int i1 = name.lastIndexOf("_");
                    fileName = name.substring(0, i1).concat(".jpg");
                }

                if (file.exists() && file.isFile()) {
                    RequestBody body = RequestBody.create(IMAGE_JPG, file);
                    App settings = App.get(getApplicationContext());
                    if (settings.connectionServer.equals("1")) {
                        int photoPort = App.get(getApplicationContext()).serverPortPhoto();
                        String api_url = String.format(Locale.ENGLISH, "http://%s:%d/api/photos/name?%s",
                                App.get(getApplicationContext()).getFTPServer(), photoPort,
                                Base64.encodeToString(fileName.getBytes(), Base64.DEFAULT));

                        Request request = new Request.Builder()
                                .url(api_url)
                                .post(body)
                                .build();

                        Response response = null;
                        try {
                            if (isMainServer) {
                                response = httpClient.newCall(request).execute();
                            } else {
                                try {
                                    api_url = String.format(Locale.ENGLISH, "http://%s:%d/api/photos/name?%s",
                                            App.get(getApplicationContext()).getFTPServerA(), photoPort,
                                            Base64.encodeToString(fileName.getBytes(), Base64.DEFAULT));
                                    request = new Request.Builder()
                                            .url(api_url)
                                            .post(body)
                                            .build();
                                    response = httpClient.newCall(request).execute();
                                } catch (IOException e1) {
                                    status = STATUS_ERROR;
                                    mErrorText = e1.getMessage();
                                }
                            }
                        } catch (IOException e) {
                            isMainServer = false;
                            try {
                                api_url = String.format(Locale.ENGLISH, "http://%s:%d/api/photos/name?%s",
                                        App.get(getApplicationContext()).getFTPServerA(), photoPort,
                                        Base64.encodeToString(fileName.getBytes(), Base64.DEFAULT));
                                request = new Request.Builder()
                                        .url(api_url)
                                        .post(body)
                                        .build();
                                response = httpClient.newCall(request).execute();
                            } catch (IOException e1) {
                                status = STATUS_ERROR;
                                mErrorText = e1.getMessage();
                            }
                        }
                        if (response != null && response.isSuccessful()) {
                            successful.add(path);
                            try {
                                new File(path).delete();
                                Bundle bundle = new Bundle();
                                bundle.putInt("progress", ((i * 100) / paths.length));
                                receiver.send(1, bundle);
                                i++;
                            } catch (Exception e) {
                                Log.e(TAG, e.toString());
                            }
                        } else {
                            Log.e(TAG, "response not isSuccessful");
                            status = STATUS_ERROR;
                            mErrorText = "response not isSuccessful";
                        }
                    } else {
                        try {
                            String region = App.getRegion(getApplicationContext());
                            String filePath;
                            if (!returnUUID.isEmpty()) {
                                if (uuid.contains("payback")) {
                                    filePath = String.format("%s/%s/%s/%s/%s/payback/%s/mtrade_%s.jpg", region, date, codeAgent, codeClient, codeFirm, returnUUID, timeStamp);
                                } else {
                                    filePath = String.format("%s/%s/%s/%s/%s/payback/%s/mtrade_%s_%s.jpg", region, date, codeAgent, codeClient, codeFirm, returnUUID, timeStamp, uuid);
                                }
                            } else {
                                if (uuid.contains("payback") || codeFirm.equals("000")) {
                                    filePath = String.format("%s/%s/%s/%s/%s/payback/mtrade_%s.jpg", region, date, codeAgent, codeClient, codeFirm, timeStamp);
                                } else {
                                    filePath = String.format("%s/%s/%s/%s/%s/mtrade_%s", region, date, codeAgent, codeClient, codeFirm, timeStamp);
                                    if (!uuid.isEmpty()) {
                                        filePath = filePath + "_" + uuid;
                                    } else {
                                        filePath = filePath + ".jpg";
                                    }
                                }
                            }
                            ApiC3 apiC3 = MyApp.getApiC3();
                            Map<String, String> param = new LinkedHashMap<>();
                            param.put("bucket", "dlm-vizit");
                            param.put("key", filePath);
                            param.put("public", "true");
                            Log.d(TAG, filePath);
                            retrofit2.Response<ResponseBody> execute = apiC3.sendPhoto(param, body).execute();
                            if (execute.isSuccessful()) {
                                //new File(path).delete();
                                try {
                                    String locationFile = "https://dlm-vizit.hb.bizmrg.com/" + filePath;
                                    if (execute.body() != null) {
                                        String string = execute.body().string();
                                        if (!string.contains("error")) {
                                            JSONObject jsonObject = new JSONObject(string);
                                            JSONObject data = jsonObject.getJSONObject("data");
                                            locationFile = data.getString("Location");
                                        }
                                    }
                                    if (!uuid.isEmpty()) {
                                        String function = null;
                                        String myUuid = null;
                                        boolean isReserved = false;
                                        if (!locationFile.contains("payback")) {
//                                            function = "ПривязатьФотоКВозврату";
//                                            myUuid = returnUUID;
//                                            if (!isReservedPayback) {
//                                                isReserved = Returns.isReserved(UUID.fromString(myUuid));
//                                            }
//                                        } else {
                                            function = "ПривязатьФотоКВизиту";
                                            myUuid = uuid.substring(0, uuid.lastIndexOf("."));
                                            MyApp app = (MyApp) getApplicationContext().getApplicationContext();
                                            Cursor query = app.getDB().query(VisitReservedMetadata.TABLE_NAME, new String[]{VisitReservedMetadata.FIELD_IS_RESERVED},
                                                    VisitReservedMetadata.FIELD_UUID + "=?",
                                                    new String[]{myUuid}, null, null, null);
                                            if (query.moveToFirst()){
                                                isReserved = query.getInt(query.getColumnIndex(VisitReservedMetadata.FIELD_IS_RESERVED)) == 1;
                                            }
                                            query.close();
                                            if (isReserved) {
                                                Response response = OkHttpClientBuilder.buildCall(function, String.format("%s;%s;", myUuid, locationFile), region).execute();
                                                if (response.isSuccessful()) {
                                                    ResponseBody responseBody = response.body();
                                                    if (responseBody != null) {
                                                        String string1 = responseBody.string();
                                                        if (string1.contains("OK<EOF>")) {
                                                            new File(path).delete();
                                                        } else if (string1.contains("не поддерживатеся [EREST]")) {
                                                            new File(path).delete();
                                                        } else {
                                                            status = STATUS_ERROR;
                                                        }
                                                    }
                                                }
                                            } else {
                                                new File(path).delete();
                                            }
                                        } else {
                                            new File(path).delete();
                                        }
                                    } else {
                                        new File(path).delete();
                                    }

                                    Bundle bundle = new Bundle();
                                    bundle.putInt("progress", ((i * 100) / paths.length));
                                    receiver.send(1, bundle);
                                    i++;
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                    status = STATUS_ERROR;
                                    mErrorText = e.toString();
                                }
                            } else {
                                status = STATUS_ERROR;
                            }
                        } catch (IOException e) {
                            status = STATUS_ERROR;
                            mErrorText = e.getMessage();
                        }
                    }
                }
            }
        } else {
            status = STATUS_ERROR;
            mErrorText = "";
        }
        Intent result = new Intent(BROADCAST_ACTION);
        result.putExtra(EXTRA_STATUS, status);
        result.putExtra(EXTRA_MESSAGE, mErrorText);
        receiver.send(2, new Bundle());
        sendBroadcast(result);
    }

}
