package com.madlab.mtrade.grinfeld.roman.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.madlab.mtrade.grinfeld.roman.R;
import com.madlab.mtrade.grinfeld.roman.entity.FireBaseNotification;
import com.madlab.mtrade.grinfeld.roman.fragments.FireBaseMessageContainer;
import com.madlab.mtrade.grinfeld.roman.fragments.FragmentFireBaseMessageTreeView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by GrinfeldRA on 21.02.2018.
 */

public class FireBaseMessageExpAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "#FireBaseMessageExpAdapter";
    private Map<String, List<FireBaseNotification>> notificationsListMap;
    private Context mContext;
    Drawable expanded;
    Drawable collapsed;

    public FireBaseMessageExpAdapter(Context context, Map<String, List<FireBaseNotification>> notificationsListMap){
        mContext = context;
        this.notificationsListMap = notificationsListMap;

        if (android.os.Build.VERSION.SDK_INT>=21) {
            collapsed = context.getDrawable(android.R.drawable.arrow_down_float);
            expanded = context.getDrawable(android.R.drawable.arrow_up_float);
        }else{
            collapsed = ContextCompat.getDrawable(context, android.R.drawable.arrow_down_float);
            expanded = ContextCompat.getDrawable(context, android.R.drawable.arrow_up_float);
        }
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        observer.onChanged();
        super.registerDataSetObserver(observer);
    }

    @Override
    public int getGroupCount() {
        return notificationsListMap.size();
    }

    private String getKey(int position){
        String key = null;
        switch (position){
            case 0:
                key = FragmentFireBaseMessageTreeView.WEB;
                break;
            case 1:
                key = FragmentFireBaseMessageTreeView.ODIN_C;
                break;
            case 2:
                key = FragmentFireBaseMessageTreeView.MEscort;
                break;
            case 3:
                key = FragmentFireBaseMessageTreeView.DALIMO;
        }
        return key;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return notificationsListMap.get(getKey(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return notificationsListMap.get(getKey(groupPosition));
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return notificationsListMap.get(getKey(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_group_view_firebase, null);
        }

        if (isExpanded){
            //Изменяем что-нибудь, если текущая Group раскрыта
        }
        else{
            //Изменяем что-нибудь, если текущая Group скрыта
        }
        int count_message = 0;
        TextView message_count = convertView.findViewById(R.id.message_count);
        for (FireBaseNotification notification : notificationsListMap.get(getKey(groupPosition))){
            if (notification.getRead()==0){
                count_message ++;
            }
        }

        TextView textGroup =  convertView.findViewById(R.id.textGroup);
        TextView tvTitle = convertView.findViewById(R.id.indicator);
        Drawable drawable = isExpanded ? expanded : collapsed;
        tvTitle.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        textGroup.setText(notificationsListMap.keySet().toArray()[groupPosition].toString().toUpperCase());
        message_count.setText(String.valueOf(count_message));
        return convertView;

    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exp_child_view_firebase, null);
        }

        TextView tvTitle = convertView.findViewById(R.id.textTitle);
        TextView tvMessage = convertView.findViewById(R.id.textMessage);
        TextView tvDate = convertView.findViewById(R.id.textDate);
        TextView view = convertView.findViewById(R.id.txt_read_state);
        FireBaseNotification notification = notificationsListMap.get(getKey(groupPosition)).get(childPosition);
        tvTitle.setText(notification.getTitle());
        tvMessage.setText(notification.getMessage());
        long dateLong = notification.getDate();
        Date date = new Date(dateLong*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+4"));
        String formattedDate = sdf.format(date);
        tvDate.setText(formattedDate);
        if (notification.getRead()==1){
            view.setVisibility(View.INVISIBLE);
        }else {
            view.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

