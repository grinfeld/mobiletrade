package com.madlab.mtrade.grinfeld.roman;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.madlab.mtrade.grinfeld.roman.db.DBHelper;
import com.madlab.mtrade.grinfeld.roman.db.PaybackPhotoMetaData;
import com.madlab.mtrade.grinfeld.roman.entity.PhotoFilter;
import com.madlab.mtrade.grinfeld.roman.entity.Returns;
import com.madlab.mtrade.grinfeld.roman.fragments.ReturnGoodsFragment;
import com.madlab.mtrade.grinfeld.roman.fragments.VisitContainFragment;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraOptions;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.madlab.mtrade.grinfeld.roman.fragments.ReturnGoodsFragment.RETURN_PHOTO_REPORT;
import static com.madlab.mtrade.grinfeld.roman.fragments.ReturnGoodsFragment.TYPE_PHOTO_REPORT;


/**
 * Created by GrinfeldRA
 */

public class CamActivity extends Activity {

    private static final String TAG = "#CamActivity";
    public static final String BITMAP_IMAGE = "BitmapImage";

    CameraView cameraView;
    FloatingActionButton floatingActionCapture, floatingActionDone;
    ImageView img_preview;
    Spinner spinner;
    String clientCode;
    String uuid;
    String uuidReturnDoc;
    int typePhotoReport = 0;
    private String mCurrentPhotoPath = "";
    private CameraHandlerThread mThread = null;
    private String pathFile = null;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.photo_view);
        clientCode = getIntent().getStringExtra(VisitContainFragment.CLIENT_CODE);
        uuid = getIntent().getStringExtra(VisitContainFragment.UUID_KEY);
        typePhotoReport = getIntent().getIntExtra(TYPE_PHOTO_REPORT, 0);
        uuidReturnDoc = getIntent().getStringExtra(ReturnGoodsFragment.KEY_UUID_DOC);
        img_preview = findViewById(R.id.img_preview);
        spinner = findViewById(R.id.spinner_photo_filter);
        floatingActionCapture = findViewById(R.id.action_capture);
        floatingActionDone = findViewById(R.id.action_done);
        cameraView = findViewById(R.id.camera);

        if (savedInstanceState != null && savedInstanceState.containsKey(BITMAP_IMAGE)) {
            mCurrentPhotoPath = savedInstanceState.getString(BITMAP_IMAGE);
            if (mCurrentPhotoPath != null && !mCurrentPhotoPath.isEmpty()) {
                File file = new File(mCurrentPhotoPath);
                if (file.exists()) {
                    img_preview.setVisibility(View.VISIBLE);
                    pathFile = file.getAbsolutePath();
                    Glide.with(CamActivity.this).load(pathFile).asBitmap().into(img_preview);
                    floatingActionDone.hide();
                }

            }
        }
        if (typePhotoReport != RETURN_PHOTO_REPORT) {
            List<PhotoFilter> filterList = PhotoFilter.load(this);
            if (filterList != null && filterList.size() > 0) {
                spinner.setVisibility(View.VISIBLE);
                ArrayAdapter<PhotoFilter> adapter = new ArrayAdapter<>(this, R.layout.spinner_item_camera, filterList);
                adapter.setDropDownViewResource(R.layout.spinner_dropdown_view);
                spinner.setAdapter(adapter);
            }
        }
        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                try {
                    File imageFile = createImageFile();
                    FileOutputStream fos = new FileOutputStream(imageFile);
                    fos.write(jpeg);
                    fos.close();
                    mCompositeDisposable.add(new Compressor(CamActivity.this)
                            .setQuality(75)
                            .setMaxHeight(1080)
                            .setMaxWidth(1920)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .compressToFileAsFlowable(imageFile)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .map(file -> {
                                copyFile(file.getAbsolutePath(), imageFile.getAbsolutePath(), true);
                                return file;
                            })
                            .subscribe(file -> {
                                floatingActionDone.show();
                                img_preview.setVisibility(View.VISIBLE);
                                pathFile = file.getAbsolutePath();
                                Glide.with(CamActivity.this).load(pathFile).asBitmap().into(img_preview);
                                Returns aReturn = Returns.getReturn();
                                if (aReturn != null) {
                                    aReturn.setPhotoIsExist((byte) 1);
                                }
                                String name = file.getName();
                                String[] split = name.split("_");
                                String codeAgent = split[0];
                                String codeClient = split[1];
                                String date = split[2];
                                if (split[2].length() == 8) {
                                    date = split[2].substring(0, 4).concat("-").concat(split[2].substring(4, 6)).concat("-").concat(split[2].substring(6, 8));
                                }
                                String timeStamp = split[3];
                                String codeFirm = split[4];
                                String uuid = "";
                                String returnUUID = "";
                                String fileName = file.getName();
                                if (split.length == 8) {
                                    uuid = split[6];
                                    returnUUID = split[7].substring(0, split[7].indexOf("."));
                                    int i1 = name.lastIndexOf("_", name.lastIndexOf("_") - 1);
                                    fileName = name.substring(0, i1).concat(".jpg");
                                } else if (split.length == 7) {
                                    uuid = split[6];
                                    int i1 = name.lastIndexOf("_");
                                    fileName = name.substring(0, i1).concat(".jpg");
                                }
                                String region = App.getRegion(getApplicationContext());
                                String filePath;
                                if (!returnUUID.isEmpty()) {
                                    if (uuid.contains("payback")) {
                                        filePath = String.format("%s/%s/%s/%s/%s/payback/%s/mtrade_%s.jpg", region, date, codeAgent, codeClient, codeFirm, returnUUID, timeStamp);
                                    } else {
                                        filePath = String.format("%s/%s/%s/%s/%s/payback/%s/mtrade_%s_%s.jpg", region, date, codeAgent, codeClient, codeFirm, returnUUID, timeStamp, uuid);
                                    }
                                } else {
                                    if (uuid.contains("payback") || codeFirm.equals("000")) {
                                        filePath = String.format("%s/%s/%s/%s/%s/payback/mtrade_%s.jpg", region, date, codeAgent, codeClient, codeFirm, timeStamp);
                                    } else {
                                        filePath = String.format("%s/%s/%s/%s/%s/mtrade_%s", region, date, codeAgent, codeClient, codeFirm, timeStamp);
                                        if (!uuid.isEmpty()) {
                                            filePath = filePath + "_" + uuid;
                                        } else {
                                            filePath = filePath + ".jpg";
                                        }
                                    }
                                }
                                String locationFile = "https://dlm-vizit.hb.bizmrg.com/" + filePath;
                                MyApp app = (MyApp) getApplication();
                                SQLiteDatabase db = app.getDB();
                                String sql = PaybackPhotoMetaData.insertQuery(locationFile, aReturn == null ? uuid.substring(0, uuid.lastIndexOf(".")) : uuidReturnDoc);
                                Log.d(TAG, sql);
                                DBHelper.execMultipleSQL(db, new String[]{sql});
                                floatingActionCapture.setEnabled(true);
                            }, throwable -> {
                                throwable.printStackTrace();
                                Toast.makeText(CamActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                floatingActionCapture.setEnabled(true);
                            }));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        CameraOptions cameraOptions = cameraView.getCameraOptions();
        if (cameraOptions != null && cameraOptions.isAutoFocusSupported()) {
            cameraView.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER);
        }
        floatingActionCapture.setOnClickListener(view -> {
            cameraView.capturePicture();
            floatingActionCapture.setEnabled(false);
        });
        floatingActionDone.setOnClickListener(view -> {
            Intent intent = new Intent();

            setResult(RESULT_OK, intent);
            finish();
        });
        img_preview.setOnClickListener(view -> {
            if (pathFile != null) {
                show(pathFile);
            }
        });
    }


    private void copyFile(String from, String to, Boolean overwrite) {

        try {
            File fromFile = new File(from);
            File toFile = new File(to);

            if (!fromFile.exists()) {
                throw new IOException("File not found: " + from);
            }
            if (!fromFile.isFile()) {
                throw new IOException("Can't copy directories: " + from);
            }
            if (!fromFile.canRead()) {
                throw new IOException("Can't read file: " + from);
            }

            if (toFile.isDirectory()) {
                toFile = new File(toFile, fromFile.getName());
            }

            if (toFile.exists() && !overwrite) {
                throw new IOException("File already exists.");
            } else {
                String parent = toFile.getParent();
                if (parent == null) {
                    parent = System.getProperty("user.dir");
                }
                File dir = new File(parent);
                if (!dir.exists()) {
                    throw new IOException("Destination directory does not exist: " + parent);
                }
                if (dir.isFile()) {
                    throw new IOException("Destination is not a valid directory: " + parent);
                }
                if (!dir.canWrite()) {
                    throw new IOException("Can't write on destination: " + parent);
                }
            }

            FileInputStream fis = null;
            FileOutputStream fos = null;
            try {

                fis = new FileInputStream(fromFile);
                fos = new FileOutputStream(toFile);
                byte[] buffer = new byte[4096];
                int bytesRead;

                while ((bytesRead = fis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }

            } finally {
                if (from != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
                if (to != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Problems when copying file.");
        }
    }


    public void show(String pathFile) {
        final Dialog builder = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        builder.setContentView(R.layout.dialog_cam_preview);
        ImageButton button_close = builder.findViewById(R.id.button_close);
        ImageView img_full_screen = builder.findViewById(R.id.img_full_screen);
        Glide.with(this).load(pathFile).asBitmap().into(img_full_screen);
        button_close.setOnClickListener(view -> builder.dismiss());
        builder.show();
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);//FIXME
        finish();
    }

    private File createImageFile() {
        String codePhotoFilter;
        try {
            codePhotoFilter = ((PhotoFilter) spinner.getSelectedItem()).getCode();
        } catch (Exception e) {
            codePhotoFilter = null;
        }
        File dir = new File(App.get(this).getPhotosPath());
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = String.format("%s_%s_%s", App.get(this).getCodeAgent(), clientCode, timeStamp);
        if (codePhotoFilter == null) {
            imageFileName += "_000_1";
        } else {
            imageFileName += "_" + codePhotoFilter + "_0";
        }
        imageFileName += "_" + uuid;
        if (uuidReturnDoc != null) {
            imageFileName += "_" + uuidReturnDoc;
        }
        File image = new File(dir + "/" + imageFileName + ".jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void oldOpenCamera() {
        try {
            cameraView.start();
        } catch (RuntimeException e) {
            Log.e(TAG, "failed to open front camera");
        }
    }

    private void newOpenCamera() {
        if (mThread == null) {
            mThread = new CameraHandlerThread();
        }
        synchronized (mThread) {
            mThread.openCamera();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (img_preview.getDrawable() != null) {
            outState.putString(BITMAP_IMAGE, mCurrentPhotoPath);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        newOpenCamera();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
        mCompositeDisposable.dispose();
    }

    private class CameraHandlerThread extends HandlerThread {
        Handler mHandler = null;

        CameraHandlerThread() {
            super("CameraHandlerThread");
            start();
            mHandler = new Handler(getLooper());
        }

        synchronized void notifyCameraOpened() {
            notify();
        }

        void openCamera() {
            mHandler.post(() -> {
                oldOpenCamera();
                notifyCameraOpened();
            });
            try {
                wait();
            } catch (InterruptedException e) {
                Log.w(TAG, "wait was interrupted");
            }
        }
    }

}
